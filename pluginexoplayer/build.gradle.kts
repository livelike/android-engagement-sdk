@Suppress("DSL_SCOPE_VIOLATION")

plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.detekt)
    `maven-publish`
}

group = "com.livelike"

android {
    namespace = "com.livelike.livelikepreintegrators"
    compileSdk = libs.versions.compile.sdk.version.get().toInt()

    defaultConfig {
        minSdk = libs.versions.min.sdk.version.get().toInt()
        targetSdk = libs.versions.target.sdk.version.get().toInt()

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    kotlinOptions {
        jvmTarget = "17"
    }
}

publishing {
    publications {
        // creating a release publication
        create<MavenPublication>("maven") {
            groupId = "com.livelike.android-engagement-sdk"
            artifactId = "pluginexoplayer"
            version = "0.0.1"

            afterEvaluate {
                from(components["release"])
            }
        }
    }
}

dependencies {
    implementation(libs.appcompat)
    compileOnly (libs.media3.exoplayer)
    compileOnly (libs.media3.exoplayer.dash)
    constraints {
        compileOnly("com.google.guava:guava:32.1.2-android")
    }
    compileOnly(project(":engagementsdk"))
    testImplementation(libs.junit)
}
