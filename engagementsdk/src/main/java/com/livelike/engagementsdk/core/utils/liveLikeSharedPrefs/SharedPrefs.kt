package com.livelike.engagementsdk.core.utils.liveLikeSharedPrefs

import android.content.Context
import android.content.SharedPreferences

private var mAppContext: Context? = null

internal fun initLiveLikeSharedPrefs(appContext: Context) {
    mAppContext = appContext
}

internal fun getSharedPreferences(): SharedPreferences {
    val packageName = mAppContext?.packageName ?: ""
    return mAppContext!!.getSharedPreferences("$packageName-livelike-sdk", Context.MODE_PRIVATE)
}

