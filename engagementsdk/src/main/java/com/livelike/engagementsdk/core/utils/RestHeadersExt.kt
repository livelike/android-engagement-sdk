package com.livelike.engagementsdk.core.utils

import android.os.Build
import com.livelike.engagementsdk.BuildConfig

internal val userAgent =
    "EngagementSDK/${BuildConfig.SDK_VERSION} Android/${Build.MODEL}/${Build.VERSION.SDK_INT}"
