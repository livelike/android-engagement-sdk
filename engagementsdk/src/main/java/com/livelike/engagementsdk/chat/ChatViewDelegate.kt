package com.livelike.engagementsdk.chat

import android.net.Uri
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintSet
import androidx.recyclerview.widget.RecyclerView
import com.livelike.engagementsdk.publicapis.ChatMessageType
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage

interface ChatViewDelegate {
    /**
     * Allow integrator to provide viewHolder class based on viewType, currently it supports only #ChatMessageType.CUSTOM_MESSAGE_CREATED
     */
    fun onCreateViewHolder(parent: ViewGroup, viewType: ChatMessageType): RecyclerView.ViewHolder

    /**
     * Allow integrator to bind data to the view elements inside the holder instance, right supports for only #ChatMessageType.CUSTOM_MESSAGE_CREATED
     */
    fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        liveLikeChatMessage: LiveLikeChatMessage,
        chatViewThemeAttributes: ChatViewThemeAttributes,
        showChatAvatar: Boolean
    )
}

interface ChatSnapToLiveDelegate {
    fun getSnapToLiveView(): View
    fun applyConstraintsOnChatViewForSnapToLiveView(
        constraintSet: ConstraintSet, rootViewId: Int
    )
}

interface ChatImagePickerDelegate {
    fun onImageButtonClicked()
   // fun onDocumentButtonClicked()
}

interface ChatImagePickerListener {
    fun onImagePick(uri: Uri)
  //  fun onDocumentPick(uri: Uri)

}

interface ChatViewSeparatorContentDelegate {
    fun getSeparatorView(
        messageTop: LiveLikeChatMessage?,
        messageBottom: LiveLikeChatMessage?
    ): View?
}