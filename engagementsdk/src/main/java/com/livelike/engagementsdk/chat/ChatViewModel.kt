package com.livelike.engagementsdk.chat

import com.livelike.chat.utils.EVENT_LOADING_COMPLETE
import com.livelike.chat.utils.EVENT_LOADING_STARTED
import com.livelike.chat.utils.EVENT_MESSAGE_CANNOT_SEND
import com.livelike.chat.utils.EVENT_MESSAGE_DELETED
import com.livelike.chat.utils.EVENT_NEW_MESSAGE
import com.livelike.chat.utils.MessageError
import com.livelike.common.DataStoreDelegate
import com.livelike.common.clients.LiveLikeProfileClient
import com.livelike.common.model.PubnubChatEventType
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.MessageWithReactionListener
import com.livelike.engagementsdk.ViewAnimationEvents
import com.livelike.engagementsdk.chat.chatreaction.Reaction
import com.livelike.engagementsdk.chat.data.remote.ChatRoom
import com.livelike.engagementsdk.chat.data.remote.PinMessageInfo
import com.livelike.engagementsdk.chat.stickerKeyboard.Sticker
import com.livelike.engagementsdk.chat.stickerKeyboard.StickerPack
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage
import com.livelike.engagementsdk.widget.repository.ProgramRepository
import com.livelike.engagementsdk.widget.viewModel.ViewModel
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import java.util.Collections
import java.util.Locale

internal class ChatViewModel(
    private val chatSession: LiveLikeChatSession,
    currentProfileId: String,
    viewModelDispatcher: CoroutineDispatcher = Dispatchers.Default,
    uiDispatcher: CoroutineDispatcher = Dispatchers.Main
) : ChatRenderer, ViewModel(viewModelDispatcher, uiDispatcher) {

    val currentProfileOnce = (chatSession as ChatSession).currentProfileOnce
    private var isPublicRoom: Boolean = (chatSession as ChatSession).isPublicRoom
    var animationEventsFlow: StateFlow<ViewAnimationEvents?>? = null
    var programRepository: ProgramRepository? = null

    //    private var dataClient: ChatDataClient = (chatSession as ChatSession).dataClient
    var errorDelegate: ErrorDelegate? = (chatSession as ChatSession).errorDelegate
    var dataStoreDelegate: DataStoreDelegate = (chatSession as ChatSession).dataStoreDelegate
    private var quoteChatMessageDeletedMessage: String =
        (chatSession as ChatSession).quoteChatMessageDeletedMessage
    private var chatMessageDeletedMessage: String =
        (chatSession as ChatSession).chatMessageDeletedMessage

    var analyticsService: AnalyticsService = (chatSession as ChatSession).analyticsService
        set(value) {
            field = value
            chatAdapter.analyticsService = value
        }
    internal var showChatAvatarLogo: Boolean = true
        set(value) {
            field = value
            chatAdapter.showChatAvatarLogo = value
        }

    var chatAdapter = ChatRecyclerAdapter(
        analyticsService, ::reportChatMessage, ::blockProfile,
        ::getSticker,
        { messageId, reactionId ->
            chatSession.sendMessageReaction(messageId, reactionId) { result, error ->
                error?.let {
                    logError { it }
                    (chatSession as ChatSession).errorDelegate?.onError(it)
                }
                result?.let { logDebug { "Send Message reaction Successfully" } }
            }
        },
        { messageId, reactionId ->
            chatSession.removeMessageReactions(messageId, reactionId) { result, error ->
                error?.let {
                    logError { it }
                    (chatSession as ChatSession).errorDelegate?.onError(it)
                }
                result?.let {
                    logDebug { "Remove Message reaction Successfully" }
                }
            }
        },
        { messageId ->
            chatSession.deleteMessage(messageId) { result, error ->
                result?.let { logDebug { "Successfully Deleted Message: $messageId" } }
                error?.let { logError { it } }
            }
        },
        currentProfileId
    )

    var enableQuoteMessage: Boolean = false
        set(value) {
            field = value
            chatAdapter.enableQuoteMessage = value
        }

     var hideDeletedMessage: Boolean = false
        set(value) {
            field = value
            chatAdapter.hideDeletedMessage = value
        }

    internal val eventFlow = MutableSharedFlow<String?>(replay = 10)
    var currentChatRoom: ChatRoom? = null
        set(value) {
            field = value
            chatAdapter.chatRoomId = value?.id
            chatAdapter.isPublicChat = isPublicRoom
            chatAdapter.chatRoomName = value?.title
        }

    private var liveLikeProfileClient: LiveLikeProfileClient =
        (chatSession as ChatSession).liveLikeProfileClient
    val stickerPackListFlow = MutableStateFlow<List<StickerPack>>(emptyList())

    fun getSticker(shortcode: String): Sticker? {
        return stickerPackListFlow.value.map { it.stickers }.flatten()
            .find { it.shortcode == shortcode }
    }

    var chatReactions: ArrayList<Reaction> = arrayListOf()
        set(value) {
            field = value
            chatAdapter.reactionList = value
        }

    var isLastItemVisible = false

    internal var chatLoaded = false
        set(value) {
            field = value
            logDebug { "chatLoad:$field" }
            eventFlow.tryEmit(
                if (field) {
                    EVENT_LOADING_COMPLETE
                } else {
                    EVENT_LOADING_STARTED
                }
            )
        }

    init {
        chatSession.setMessageWithReactionListener(object : MessageWithReactionListener {
            override fun addMessageReaction(
                messagePubnubToken: Long?,
                messageId: String?,
                chatMessageReaction: ChatMessageReaction
            ) {
                this@ChatViewModel.addMessageReaction(
                    messagePubnubToken,
                    messageId,
                    chatMessageReaction
                )
            }

            override fun removeMessageReaction(
                messagePubnubToken: Long?,
                messageId: String?,
                emojiId: String,
                userId: String?
            ) {
                this@ChatViewModel.removeMessageReaction(
                    messagePubnubToken,
                    messageId,
                    emojiId,
                    userId
                )
            }

            override fun onNewMessage(message: LiveLikeChatMessage) {
                val messageList = chatSession.getLoadedMessages()
                logDebug { "onNewMessage1:${messageList.size}" }
                uiScope.launch {
                    displayChatMessage(message, currentProfileOnce().id)
                }
                logDebug { "onNewMessage2:${messageList.size}" }
            }

            override fun onHistoryMessage(messages: List<LiveLikeChatMessage>) {
                logDebug { "onHistoryMessage : ${chatSession.getLoadedMessages().size}" }
                uiScope.launch {
                    displayChatMessages(emptyList(), currentProfileOnce().id)
                    loadingCompleted()
                }
            }

            override fun onDeleteMessage(messageId: String) {
                deleteChatMessage(messageId)
            }

            override fun onPinMessage(message: PinMessageInfo) {
                logDebug { "Pin Message :$message" }
            }

            override fun onUnPinMessage(pinMessageId: String) {
                logDebug { "UnPin Message :$pinMessageId" }
            }

            override fun onErrorMessage(error: String, clientMessageId: String?) {
                errorDelegate?.onError(error)
                if (clientMessageId != null) {
                    refreshWithDeletedMessage(clientMessageId)
                } else {
                    //added this code in case if loading is there and an error arises
                    eventFlow.tryEmit(EVENT_LOADING_COMPLETE)
                }
                logError { "$error for message with clientMessageId:$clientMessageId" }
            }
        })
    }

    override fun displayChatMessages(
        messages: List<LiveLikeChatMessage>,
        currentProfileId: String
    ) {
        val messageList = chatSession.getLoadedMessages()
        val deletedMessages = chatSession.getDeletedMessages()

        for (it in messageList) {
            replaceImageMessageContentWithImageUrl(it)
            it.quoteMessage?.apply {
                replaceImageMessageContentWithImageUrl(this)
            }
        }

        notifyNewChatMessages(messageList.filter {
            !deletedMessages.contains(it.id)
        }.filter {
            chatAdapter.chatViewDelegate != null || (chatAdapter.chatViewDelegate == null && it.messageEvent != PubnubChatEventType.CUSTOM_MESSAGE_CREATED)
        }.map {
            it.isFromMe = currentProfileId == it.senderId
            it.quoteMessage = it.quoteMessage?.apply {
                message = when (deletedMessages.contains(id)) {
                    true -> quoteChatMessageDeletedMessage
                    else -> message
                }
                isDeleted = deletedMessages.contains(id)
            }
            it
        })
    }

    override fun displayChatMessage(message: LiveLikeChatMessage, currentProfileId: String) {
        val messageList = Collections.synchronizedList(ArrayList(chatSession.getLoadedMessages()))
        //val messageList = chatSession.getLoadedMessages()
        val deletedMessages = chatSession.getDeletedMessages()
        logDebug {
            "Chat display message: ${message.message} check1:${
                message.chatRoomId != currentChatRoom?.id
            } check deleted:${deletedMessages.contains(message.id)} has Parent msg: ${message.quoteMessage != null}"
        }
        if (message.chatRoomId != currentChatRoom?.id) return

        // if custom message is received, ignore that, custom messages doesn't need to be shown in UI
        // if integrator provide the chatViewDelegate that means we are allowing to show the custom message
        if (chatAdapter.chatViewDelegate == null && message.messageEvent == PubnubChatEventType.CUSTOM_MESSAGE_CREATED) return


        if (deletedMessages.contains(message.id?.lowercase(Locale.getDefault()))) {
            logDebug { "the message is deleted by producer" }
            return
        }

        replaceImageMessageContentWithImageUrl(message)

        //prevent concurrent modification exception
        synchronized(messageList) {
            val messageFromList = messageList.find { it.clientMessageId == message.clientMessageId }
            messageFromList?.apply {
                logDebug { "updating existing message" }
                isFromMe = currentProfileId == senderId
                timetoken = message.timetoken
                createdAt = message.createdAt
                id = message.id
                imageHeight = message.imageHeight
                imageWidth = message.imageWidth
                quoteMessage = quoteMessage?.apply {
                    replaceImageMessageContentWithImageUrl(this)
                    this.message = when (deletedMessages.contains(id)) {
                        true -> quoteChatMessageDeletedMessage
                        else -> this.message
                    }
                    this.isDeleted = deletedMessages.contains(id)
                }
                parentMessageId = message.parentMessageId
            }?: messageList.add(message.apply {
                logDebug { "adding new message" }
                isFromMe = currentProfileId == senderId
                quoteMessage = quoteMessage?.apply {
                    replaceImageMessageContentWithImageUrl(this)
                    this.message = when (deletedMessages.contains(id)) {
                        true -> quoteChatMessageDeletedMessage
                        else -> this.message
                    }
                    this.isDeleted = deletedMessages.contains(id)
                }
            })
        }

       /* messageList.find { it.clientMessageId == message.clientMessageId }?.apply {
            logDebug { "updating existing message" }
            isFromMe = currentProfileId == senderId
            timetoken = message.timetoken
            createdAt = message.createdAt
            id = message.id
            imageHeight = message.imageHeight
            imageWidth = message.imageWidth
            quoteMessage = quoteMessage?.apply {
                replaceImageMessageContentWithImageUrl(this)
                this.message = when (deletedMessages.contains(id)) {
                    true -> quoteChatMessageDeletedMessage
                    else -> this.message
                }
                this.isDeleted = deletedMessages.contains(id)
            }
        } ?: messageList.add(message.apply {
            logDebug { "adding new message" }
            isFromMe = currentProfileId == senderId
            quoteMessage = quoteMessage?.apply {
                replaceImageMessageContentWithImageUrl(this)
                this.message = when (deletedMessages.contains(id)) {
                    true -> quoteChatMessageDeletedMessage
                    else -> this.message
                }
                this.isDeleted = deletedMessages.contains(id)
            }
        })*/

        notifyNewChatMessages(messageList)
    }

    private fun notifyNewChatMessages(messageList: List<LiveLikeChatMessage>) {
        if (chatLoaded) {
            chatAdapter.submitList(ArrayList(messageList))
            eventFlow.tryEmit(EVENT_NEW_MESSAGE)
        }
    }

    private fun replaceImageMessageContentWithImageUrl(message: LiveLikeChatMessage) {
        val imageUrl = message.imageUrl
        if (!imageUrl.isNullOrEmpty()) {
            message.message = CHAT_MESSAGE_IMAGE_TEMPLATE.replace("message", imageUrl)
        }
    }

    override fun removeMessageReaction(
        messagePubnubToken: Long?,
        messageId: String?,
        emojiId: String,
        profileId: String?
    ) {
        val messageList = chatSession.getLoadedMessages()
        val index =
            messageList.indexOfFirst { it.timetoken == messagePubnubToken || it.id == messageId }
        if (index > -1 && index < messageList.size) {
            uiScope.launch { chatAdapter.notifyItemChanged(index) }
        }
    }

    // need to check for error message to show in ChatView
    override fun errorSendingMessage(error: com.livelike.common.clients.Error) {
        val messageList = chatSession.getLoadedMessages()
        if (error.type == MessageError.DENIED_MESSAGE_PUBLISH.name) {
            val index = messageList.indexOfLast { it.clientMessageId == error.clientMessageId }
            if (index > -1) {
                messageList.removeAt(index)
                chatAdapter.submitList(messageList)
                eventFlow.tryEmit(EVENT_MESSAGE_CANNOT_SEND)
            } else {
                logError { "Unable to find the message based on client message id: ${error.clientMessageId}" }
            }
        }
    }

    override fun addMessageReaction(
        messagePubnubToken: Long?,
        messageId: String?,
        chatMessageReaction: ChatMessageReaction
    ) {
        val messageList = chatSession.getLoadedMessages()
        val index =
            messageList.indexOfFirst { it.timetoken == messagePubnubToken || it.id == messageId }
        if (index > -1 && index < messageList.size) {
            uiScope.launch { chatAdapter.notifyItemChanged(index) }
        }
    }

    override fun deleteChatMessage(messageId: String) {
        val messageList = Collections.synchronizedList(ArrayList(chatSession.getLoadedMessages()))
       // val messageList = chatSession.getLoadedMessages()
        val deletedMessages = chatSession.getDeletedMessages()
        logDebug { "delete Chat message: $messageId" }
        deletedMessages.add(messageId)
        if (chatLoaded) {
            logDebug { "message is deleted from producer so changing its text" }
            synchronized(messageList) {

                messageList.find {
                    it.id?.lowercase(Locale.getDefault()) == messageId
                }?.apply {
                    logDebug { "found deleted Message: $this" }
                    message = chatMessageDeletedMessage
                    isDeleted = true
                }
                for (it in messageList.filter {
                    it.quoteMessage != null && it.quoteMessage?.id?.lowercase(
                        Locale.getDefault()
                    ) == messageId
                }) {
                    if (it.quoteMessage != null && it.quoteMessage?.id?.lowercase(Locale.getDefault()) == messageId) {
                        it.apply {
                            quoteMessage = quoteMessage?.apply {
                                message = when (messageId == id) {
                                    true -> quoteChatMessageDeletedMessage
                                    else -> message
                                }
                                isDeleted = deletedMessages.contains(id)
                            }
                        }
                    }
                }
            }
            uiScope.launch {
                synchronized(messageList) {
                    logDebug { "Message List after delete: ${messageList.size}" }
                    chatAdapter.submitList(ArrayList(messageList.toSet()))
                    chatAdapter.currentChatReactionPopUpViewPos = -1
                    val index = messageList.indexOfFirst { it.id == messageId }
                    val indexList = messageList.getIndexList {
                        it.quoteMessage != null && it.quoteMessage?.id?.lowercase(Locale.getDefault()) == messageId
                    }
                    logDebug { "Deleted Index: $index, indexList: $indexList" }
                    notifyIndexUpdate(index)
                    for (it in indexList) {
                        notifyIndexUpdate(it)
                    }
                    eventFlow.tryEmit(EVENT_MESSAGE_DELETED)
                }
            }
        } else {
            logDebug { "Unable to perform delete as Chat is not loaded yet" }
        }
    }

    private fun notifyIndexUpdate(index: Int) {
        if (index != -1 && index < chatAdapter.itemCount) {
            chatAdapter.notifyItemChanged(index)
        }
    }

    override fun loadingCompleted() {
        logDebug { "Chat loading Completed : $chatLoaded" }
        val messageList = chatSession.getLoadedMessages()
        if (!chatLoaded) {
            chatLoaded = true
            chatAdapter.submitList(ArrayList(messageList.toSet()))
        } else {
            eventFlow.tryEmit(EVENT_LOADING_COMPLETE)
        }
    }

    private fun blockProfile(profileId: String) {
        liveLikeProfileClient.blockProfile(profileId) { result, error ->
            error?.let { errorDelegate?.onError(it) }
            result?.let {
                logDebug { "Block User: ${it.blockedProfileID} ,By User: ${it.blockedByProfileId}" }
                  chatSession.onBlockProfile(it)
            }
        }
    }

    private fun reportChatMessage(message: LiveLikeChatMessage) {
        chatSession.reportMessage(message) { result, error ->
            result?.let { logDebug { "Message reported Successfully :$message" } }
            error?.let { logError { it } }
        }
    }

    internal fun refreshWithDeletedMessage(clientMessageId: String? = null) {
        val messageList = chatSession.getLoadedMessages()
        val deletedMessages = chatSession.getDeletedMessages()
        messageList.removeAll { deletedMessages.contains(it.id?.lowercase(Locale.getDefault())) }
        clientMessageId?.let { messageList.removeAll { it.clientMessageId == clientMessageId } }
        uiScope.launch {
            chatAdapter.submitList(ArrayList(messageList))
        }
    }

    fun flushMessages() {
        chatSession.clearMessages()
        val messageList = chatSession.getLoadedMessages()
        chatAdapter.submitList(messageList)
    }

    fun loadPreviousMessages() {
        val messageList = chatSession.getLoadedMessages()
        val deletedMessages = chatSession.getDeletedMessages()
        logDebug { "Chat loading previous messages size:${messageList.size},all Message size:${messageList.size},deleted Message:${deletedMessages.size}," }
        chatSession.loadNextHistory()
    }

    private fun List<LiveLikeChatMessage>.getIndexList(predicate: (LiveLikeChatMessage) -> Boolean): List<Int> {
        val list = arrayListOf<Int>()
        this.forEachIndexed { index, chatMessage ->
            if (predicate(chatMessage)) {
                list.add(index)
            }
        }
        return list
    }
}
