package com.livelike.engagementsdk.chat

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.text.Editable
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.Spannable
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnLayoutChangeListener
import android.view.WindowManager
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.fragment.app.Fragment
import androidx.fragment.app.findFragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.livelike.chat.utils.EVENT_LOADING_COMPLETE
import com.livelike.chat.utils.EVENT_LOADING_STARTED
import com.livelike.chat.utils.EVENT_MESSAGE_CANNOT_SEND
import com.livelike.chat.utils.EVENT_NEW_MESSAGE
import com.livelike.common.model.PubnubChatEventType
import com.livelike.common.utils.safeCallBack
import com.livelike.engagementsdk.DEFAULT_CHAT_MESSAGE_DATE_TIME_FORMATTER
import com.livelike.engagementsdk.KeyboardType
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.R
import com.livelike.engagementsdk.ViewAnimationEvents
import com.livelike.engagementsdk.chat.stickerKeyboard.FragmentClickListener
import com.livelike.engagementsdk.chat.stickerKeyboard.Sticker
import com.livelike.engagementsdk.chat.stickerKeyboard.StickerKeyboardView
import com.livelike.engagementsdk.chat.stickerKeyboard.countMatches
import com.livelike.engagementsdk.chat.stickerKeyboard.findImages
import com.livelike.engagementsdk.chat.stickerKeyboard.getUrlFromMessageComponent
import com.livelike.engagementsdk.chat.stickerKeyboard.replaceWithImages
import com.livelike.engagementsdk.chat.stickerKeyboard.replaceWithStickers
import com.livelike.engagementsdk.chat.stickerKeyboard.targetByteArrays
import com.livelike.engagementsdk.chat.stickerKeyboard.targetDrawables
import com.livelike.engagementsdk.chat.utils.setTextOrImageToView
import com.livelike.engagementsdk.core.utils.AndroidResource
import com.livelike.engagementsdk.core.utils.AndroidResource.Companion.dpToPx
import com.livelike.engagementsdk.core.utils.animators.buildScaleAnimator
import com.livelike.engagementsdk.core.utils.scanForActivity
import com.livelike.engagementsdk.databinding.ChatViewBinding
import com.livelike.engagementsdk.databinding.ChatViewSnapToLiveBinding
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage
import com.livelike.engagementsdk.widget.view.loadImage
import com.livelike.utils.UNKNOWN_ERROR
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import pl.droidsonroids.gif.MultiCallback
import java.util.Date
import kotlin.math.max
import kotlin.math.min


/**
 *  ChatView will load and display a chat component. To use chat view
 *  ```
 *  <com.livelike.sdk.chat.ChatView
 *      android:id="@+id/chatView"
 *      android:layout_width="wrap_content"
 *      android:layout_height="wrap_content">
 *   </com.livelike.sdk.chat.ChatView>
 *  ```
 *
 */
open class ChatView(context: Context, private val attrs: AttributeSet?) :
    ConstraintLayout(context, attrs) {
    private lateinit var pickMedia: ActivityResultLauncher<PickVisualMediaRequest>
    private lateinit var binding: ChatViewBinding
    private var currentQuoteMessage: LiveLikeChatMessage? = null
        set(value) {
            field = value
            updateInputView()
        }

    /**
     * use this variable to hide message input to build use case like influencer chat
     **/
    var isChatInputVisible: Boolean = true
        set(value) {
            field = value
            logDebug { "ChatView: isChatInputVisible:$value" }
            if (this::binding.isInitialized) {
                if (value) {
                    binding.chatInput.root.visibility = View.VISIBLE
                } else {
                    binding.chatInput.root.visibility = View.GONE
                }
            }
        }

    private val chatAttribute = ChatViewThemeAttributes()
    var errorDelegate: ErrorDelegate? = null
    private var itemTouchHelper:ItemTouchHelper? = null
    private val handler = CoroutineExceptionHandler { _, exception ->
        exception.printStackTrace()
        logError { exception }
        errorDelegate?.onError(exception.message ?: UNKNOWN_ERROR)
    }
    private val uiScope = CoroutineScope(Dispatchers.Main.immediate + handler)

    private var session: LiveLikeChatSession? = null
    private var snapToLiveAnimation: AnimatorSet? = null
    private var showingSnapToLive: Boolean = false
    private var currentProfile: LiveLikeProfile? = null

    var allowMediaFromKeyboard: Boolean = true
        set(value) {
            field = value
            binding.chatInput.edittextChatMessage.allowMediaFromKeyboard = value
        }

    var emptyChatBackgroundView: View? = null
        set(view) {
            field = view
            if (binding.chatdisplayBack.childCount > 1) binding.chatdisplayBack.removeViewAt(1)
            initEmptyView()
        }

    /** Boolean option to enable / disable the profile display inside chat view */
    private var displayUserProfile: Boolean = false
        set(value) {
            field = value
            logDebug { "ChatView: displayUserProfile:$displayUserProfile" }
            if (this::binding.isInitialized) {
                binding.chatInput.userProfileDisplayLL.root.apply {
                    visibility = if (value) View.VISIBLE else View.GONE
                }
            }
        }

    var chatViewDelegate: ChatViewDelegate? = null
        set(value) {
            field = value
            viewModel?.chatAdapter?.chatViewDelegate = value
        }

    var hideDeletedMessage: Boolean = false
        set(value) {
            field = value
            viewModel?.hideDeletedMessage = value
        }

    var chatViewSeparatorContentDelegate: ChatViewSeparatorContentDelegate? = null
        set(value) {
            field = value
            viewModel?.chatAdapter?.chatViewSeparatorContentDelegate = value
        }

    var shouldDisplayAvatar: Boolean = true
        set(value) {
            field = value
            viewModel?.showChatAvatarLogo = value
        }

    private var reactionCountFormatter: ((count: Int) -> String)? = null

    private var chatInterceptor: ChatInterceptor? = null

    var enableDeleteMessage: Boolean = true
        set(value) {
            field = value
            viewModel?.chatAdapter?.enableDeleteMessage = value
        }

    private var deleteDialogInterceptor: DialogInterceptor? = object : DialogInterceptor() {
        override fun dialogToShow(message: LiveLikeChatMessage) {
            AlertDialog.Builder(this@ChatView.context).apply {
                setTitle(context.getString(R.string.delete_message_title))
                setMessage(context.getString(R.string.delete_message_desc))
                setPositiveButton(R.string.yes) { p0, _ ->
                    success()
                    p0.dismiss()
                }
                setNegativeButton(R.string.no) { p0, _ -> p0.dismiss() }
                create()
            }.show()
        }
    }
        set(value) {
            field = value
            viewModel?.chatAdapter?.deleteDialogInterceptor = value
        }


    private var viewModel: ChatViewModel? = null

    val callback = MultiCallback(true)
    private val layoutChangeListener =
        OnLayoutChangeListener { _, _, _, _, bottom, _, _, _, oldBottom ->
            if (bottom < oldBottom) {
                viewModel?.chatAdapter?.itemCount?.let {
                    if (it > 0) {
                        if (viewModel?.isLastItemVisible == true) {
                            binding.chatdisplay.post {
                                binding.chatdisplay.smoothScrollToPosition(it - 1)
                            }
                        }
                    }
                }
            }
        }

    //    private val pickDocument =
//        (context as AppCompatActivity).registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
//            // Handle the returned Uri
//            if (uri != null) {
//                logDebug { "pickDoc: $uri" }
//                chatImagePickerListener.onDocumentPick(uri)
//            } else {
//                logError { "No media doc Selected" }
//            }
//        }
    var supportImagePicker: Boolean = true
        set(value) {
            field = value
            binding.chatInput.buttonPicker.visibility = if (value) View.VISIBLE else View.GONE
        }

    var chatImagePickerDelegate: ChatImagePickerDelegate = object : ChatImagePickerDelegate {
        override fun onImageButtonClicked() {
            openPickerDialog()
        }

//        override fun onDocumentButtonClicked() {
//           pickDocument.launch("image/*")
//        }
    }

    val chatImagePickerListener: ChatImagePickerListener = object : ChatImagePickerListener {
        override fun onImagePick(uri: Uri) {
            binding.chatInput.edittextChatMessage.setText(":$uri:")
            binding.chatInput.buttonEmoji.visibility = View.GONE
        }

//        override fun onDocumentPick(uri: Uri) {
//            binding.chatInput.edittextChatMessage.setText(":$uri:")
//        }
    }

    private var chatSnapToLiveDelegate: ChatSnapToLiveDelegate = object : ChatSnapToLiveDelegate {
        private val chatViewSnapToLiveBinding =
            ChatViewSnapToLiveBinding.inflate(LayoutInflater.from(context), null, false)

        init {
            chatViewSnapToLiveBinding.root.layoutParams = LayoutParams(
                resources.getDimensionPixelSize(R.dimen.livelike_snap_live_width),
                resources.getDimensionPixelSize(R.dimen.livelike_snap_live_height)
            )
        }

        override fun getSnapToLiveView(): View {
            return chatViewSnapToLiveBinding.root
        }

        override fun applyConstraintsOnChatViewForSnapToLiveView(
            constraintSet: ConstraintSet,
            rootViewId: Int
        ) {
            constraintSet.connect(
                getSnapToLiveView().id,
                ConstraintSet.START,
                ConstraintSet.PARENT_ID,
                ConstraintSet.START
            )
            constraintSet.connect(
                getSnapToLiveView().id,
                ConstraintSet.BOTTOM,
                binding.chatInput.root.id,
                ConstraintSet.TOP
            )
            constraintSet.connect(
                getSnapToLiveView().id,
                ConstraintSet.END,
                binding.chatdisplayBack.id,
                ConstraintSet.END
            )
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                constraintSet.setHorizontalBias(
                    getSnapToLiveView().id,
                    resources.getFloat(R.dimen.livelike_snap_live_horizontal_bias)
                )
            } else {
                val outValue = TypedValue()
                resources.getValue(R.dimen.livelike_snap_live_horizontal_bias, outValue, true)
                constraintSet.setHorizontalBias(
                    getSnapToLiveView().id,
                    outValue.float
                )
            }
            constraintSet.setMargin(
                getSnapToLiveView().id,
                ConstraintSet.START,
                resources.getDimensionPixelSize(R.dimen.livelike_snap_live_margin_start)
            )
            constraintSet.setMargin(
                getSnapToLiveView().id,
                ConstraintSet.LEFT,
                resources.getDimensionPixelSize(R.dimen.livelike_snap_live_margin_left)
            )
            constraintSet.setMargin(
                getSnapToLiveView().id,
                ConstraintSet.END,
                resources.getDimensionPixelSize(R.dimen.livelike_snap_live_margin_end)
            )
            constraintSet.setMargin(
                getSnapToLiveView().id,
                ConstraintSet.RIGHT,
                resources.getDimensionPixelSize(R.dimen.livelike_snap_live_margin_right)
            )
            constraintSet.setMargin(
                getSnapToLiveView().id,
                ConstraintSet.BOTTOM,
                resources.getDimensionPixelSize(R.dimen.livelike_snap_live_margin_bottom)
            )
        }

    }

    fun setChatSnapToLiveDelegate(chatSnapToLiveDelegate: ChatSnapToLiveDelegate) {
        if (this.chatSnapToLiveDelegate != chatSnapToLiveDelegate) {
            val oldView = this.chatSnapToLiveDelegate.getSnapToLiveView()
            binding.root.removeView(oldView)
        }

        val snapToLiveView = chatSnapToLiveDelegate.getSnapToLiveView()
        binding.root.addView(snapToLiveView)
        val constraintSet = ConstraintSet()
        constraintSet.clone(binding.root)
        chatSnapToLiveDelegate.applyConstraintsOnChatViewForSnapToLiveView(
            constraintSet,
            binding.root.id
        )
        constraintSet.applyTo(binding.root)
        this.chatSnapToLiveDelegate = chatSnapToLiveDelegate
    }

    fun updateSnapToLiveBottomMargin(isStickerKeyboardVisible: Boolean) {
        val bottomMargin = if (isStickerKeyboardVisible) {
            resources.getDimensionPixelSize(R.dimen.bottom_margin_sticker_visible)
        } else {
            resources.getDimensionPixelSize(R.dimen.bottom_margin_sticker_hidden)
        }

        val constraintSet = ConstraintSet()
        constraintSet.clone(binding.root)
        chatSnapToLiveDelegate.applyConstraintsOnChatViewForSnapToLiveView(constraintSet,
            binding.root.id)
        constraintSet.setMargin(chatSnapToLiveDelegate.getSnapToLiveView().id, ConstraintSet.BOTTOM, bottomMargin)
        constraintSet.applyTo(binding.root)
    }

    init {
        context.scanForActivity()?.window?.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN or @Suppress("DEPRECATION") WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE //kept due to pre M support
        ) // INFO: Adjustresize doesn't work with Fullscreen app.. See issue https://stackoverflow.com/questions/7417123/android-how-to-adjust-layout-in-full-screen-mode-when-softkeyboard-is-visible
        context.obtainStyledAttributes(
            attrs, R.styleable.ChatView, 0, 0
        ).apply {
            try {
                displayUserProfile = getBoolean(R.styleable.ChatView_displayUserProfile, false)
                chatAttribute.initAttributes(context, this)
            } finally {
                recycle()
            }
        }
        try {
            pickMedia =
                (context as AppCompatActivity).registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri: Uri? ->
                    // Callback is invoked after the user selects a media item or closes the
                    // photo picker.
                    if (uri != null) {
                        logDebug { "pickMedia : $uri" }
                        chatImagePickerListener.onImagePick(uri)
                    } else {
                        logError { "No media Selected" }
                    }
                }
        } catch (exception: Exception) {
            logError { exception.message }
            exception.message?.let { (session as? ChatSession)?.errorDelegate?.onError(it) }
        }
        initView(context)
    }

    var enableQuoteMessage: Boolean = false
        set(value) {
            field = value
            viewModel?.enableQuoteMessage = value
        }

    var enableChatMessageURLs: Boolean = false
        set(value) {
            field = value
            viewModel?.chatAdapter?.showLinks = value
        }

    var chatMessageUrlPatterns: String? = null
        set(value) {
            field = value
            value?.let {
                if (value.isNotEmpty()) viewModel?.chatAdapter?.linksRegex = it.toRegex()
            }
        }

    private fun setBackButtonInterceptor(v: View) {
        v.isFocusableInTouchMode = true
        v.requestFocus()
        v.setOnKeyListener(object : OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (event?.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                    if (binding.stickerKeyboard.visibility == View.VISIBLE) {
                        hideStickerKeyboard()
                        return true
                    }
                }
                return false
            }
        })
    }

    private fun initView(context: Context) {
        binding = ChatViewBinding.inflate(LayoutInflater.from(context), this, true)
        setChatSnapToLiveDelegate(chatSnapToLiveDelegate)
        binding.chatInput.userProfileDisplayLL.root.visibility =
            if (displayUserProfile) View.VISIBLE else View.GONE
        chatAttribute.apply {
            binding.chatInput.apply {
                userProfileDisplayLL.rankValue.setTextColor(rankValueTextColor)
                binding.chatView.background = chatViewBackgroundRes
                chatDisplayBackgroundRes?.let {
                    binding.chatdisplay.background = it
                }
                changeColorOfProgressbar(chatProgressLoaderColor)
                chatInputBackground.background = chatInputViewBackgroundRes
                chatInputBorder.background = chatInputBackgroundRes
                edittextChatMessage.filters = arrayOf<InputFilter>(LengthFilter(chatInputCharLimit))
                edittextChatMessage.setTextColor(chatInputTextColor)
                edittextChatMessage.setHintTextColor(chatInputHintTextColor)
                edittextChatMessage.setTextSize(
                    TypedValue.COMPLEX_UNIT_PX, chatInputTextSize.toFloat()
                )
                buttonEmoji.setImageDrawable(chatStickerSendDrawable)
                buttonPicker.setImageDrawable(chatImagePickerDrawable)
                buttonEmoji.setColorFilter(
                    sendStickerTintColor, PorterDuff.Mode.MULTIPLY
                )
                buttonEmoji.visibility = when {
                    showStickerSend -> View.VISIBLE
                    else -> View.GONE
                }

                val layoutParams = buttonChatSend.layoutParams
                layoutParams.width = sendIconWidth
                layoutParams.height = sendIconHeight
                buttonChatSend.layoutParams = layoutParams
                buttonChatSend.setImageDrawable(chatSendDrawable)
                buttonChatSend.background = chatSendBackgroundDrawable
                buttonChatSend.setPadding(
                    chatSendPaddingLeft,
                    chatSendPaddingTop,
                    chatSendPaddingRight,
                    chatSendPaddingBottom
                )
                buttonPicker.setPadding(
                    chatImagePickerPaddingLeft,
                    chatImagePickerPaddingTop,
                    chatImagePickerPaddingRight,
                    chatImagePickerPaddingBottom
                )
                buttonChatSend.setColorFilter(
                    sendImageTintColor, PorterDuff.Mode.MULTIPLY
                )
                buttonPicker.background = chatImagePickerBackgroundDrawable
                imgQuoteMsgCancel.setOnClickListener {
                    currentQuoteMessage = null
                }
                initEmptyView()
            }
        }
        callback.addView(binding.chatInput.edittextChatMessage)

        binding.swipeToRefresh.setOnRefreshListener {
            if (viewModel?.chatLoaded == true) {
                viewModel?.loadPreviousMessages()
                hidePopUpReactionPanel()
            } else binding.swipeToRefresh.isRefreshing = false
        }
    }

    private fun changeColorOfProgressbar(chatProgressLoaderColor: Int) {
        val progressDrawable: Drawable = binding.loadingSpinner.indeterminateDrawable.mutate()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            progressDrawable.colorFilter =
                BlendModeColorFilter(chatProgressLoaderColor, BlendMode.SRC_ATOP)
        } else {
            progressDrawable.setColorFilter(chatProgressLoaderColor, PorterDuff.Mode.SRC_ATOP)
        }
        binding.loadingSpinner.progressDrawable = progressDrawable
    }

    private fun initEmptyView() {
        emptyChatBackgroundView?.let {
            if (binding.chatdisplayBack.childCount == 1) {
                val layoutParam = FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT
                )
                layoutParam.gravity = Gravity.CENTER
                binding.chatdisplayBack.addView(it, layoutParam)
            }
            it.visibility = View.GONE
        }
    }

    /**
     * unix timestamp is passed as param
     * returns the formatted string to display
     */
    open fun formatMessageDateTime(messageTimeStamp: Long?): String {
        if (messageTimeStamp == null || messageTimeStamp == 0L) {
            return ""
        }
        val dateTime = Date()
        dateTime.time = messageTimeStamp
        return DEFAULT_CHAT_MESSAGE_DATE_TIME_FORMATTER.format(dateTime)
    }

    /**
     * Scroll directly to the particular messageId mentioned
     */
    open fun scrollToMessage(messageId: String) {
        if (messageId.isEmpty()) {
            logError { "Not allowed empty Message ID" }
        } else {
            val index = session?.getLoadedMessages()?.indexOfFirst { it.id == messageId }
            index?.let {
                if (index > -1 && index < (binding.chatdisplay.adapter?.itemCount ?: 0)) {
                    binding.chatdisplay.postDelayed({
                        binding.chatdisplay.scrollToPosition(it)
                    }, 100)
                } else {
                    logDebug { "Message not found" }
                }
            }
        }
    }

    /**
     * Scroll the chat list to the bottom
     */
    open fun scrollChatToBottom() {
        snapToLive()
    }

    /**
     * chat session is loaded through this
     */
    fun setSession(session: LiveLikeChatSession) {
        if (this.session === session) return // setting it multiple times same view with same session have a weird behaviour will debug later.
        hideGamification()
        this.session = session.apply {
            (session as ChatSession).analyticsService.trackOrientationChange(resources.configuration.orientation == 1)
        }
        uiScope.launch {
            viewModel = ChatViewModel(session, (session as ChatSession).currentProfileOnce().id)
            viewModel?.apply {
                chatAdapter.enableDeleteMessage = enableDeleteMessage
                chatAdapter.deleteDialogInterceptor = deleteDialogInterceptor
                this@ChatView.uiScope.launch {
                    launch {
                        session.chatSessionIdleFlow.collect {
                            if (it) {
                                if (session.currentChatRoomFlow.value == null) {
                                    chatLoaded = false
                                } else {
                                    displayChatMessages(emptyList(), currentProfileOnce().id)
                                    loadingCompleted()
                                }
                            }
                        }
                    }
                    launch {
                        session.updateUrlFlow.collect { pair ->
                            if (pair != null) {
//                                viewModel!!.programIdForStickerPack = chatRoom.clientId

                                session.getStickerPacks { result, error ->
                                    result?.let { stickerPacks ->
                                        stickerPackListFlow.value = stickerPacks
                                        uiScope.launch {
                                            stickerPacks.onEach { stickerPack ->
                                                session.preLoadImage(stickerPack.file)
                                                stickerPack.stickers.onEach {
                                                    session.preLoadImage(it.file)
                                                }
                                            }
                                            if (chatLoaded) {
                                                chatAdapter.notifyDataSetChanged()
                                            }
                                        }
                                    }
                                }

                                if (pair.second) {
                                    session.getReactions { result, error ->
                                        result?.let {
                                            viewModel!!.chatReactions = ArrayList(it)
                                            it.onEach {
                                                safeCallBack(uiScope) {
                                                    session.preLoadImage(it.file)
                                                }
                                            }
                                        }
                                        error?.let { logError { it } }
                                    }
                                }
                            }
                        }
                    }
                }
                launch {
                    session.currentChatRoomFlow.collect {
                        currentChatRoom = it
                        if (it != null) {
                            initStickerKeyboard(binding.stickerKeyboard, this@apply)
                        }
                    }
                }
                launch {
                    session.chatLoadedFlow.collect {
                        if (it != null && currentChatRoom?.id != it) {
                            chatLoaded = false
                        }
                    }
                }
                launch {
                    session.flushMessagesFlow.collect {
                        logDebug { "flush message: $it ,currentChatRoom : ${currentChatRoom?.id}" }
                        if (it != null && currentChatRoom?.id != it) {
                            flushMessages()
                            session.flushMessagesFlow.value = null
                        }
                    }
                }
                this.enableQuoteMessage = this@ChatView.enableQuoteMessage
                this.hideDeletedMessage = this@ChatView.hideDeletedMessage
                chatAdapter.showLinks = enableChatMessageURLs
                chatMessageUrlPatterns?.let {
                    if (it.isNotEmpty()) chatAdapter.linksRegex = it.toRegex()
                }
                chatAdapter.currentChatReactionPopUpViewPos = -1
                chatAdapter.chatViewThemeAttribute = chatAttribute
                chatAdapter.messageTimeFormatter = { time ->
                    formatMessageDateTime(time)
                }
                chatAdapter.reactionCountFormatter = reactionCountFormatter

                refreshWithDeletedMessage()
                setDataSource(chatAdapter)
                if (chatLoaded) checkEmptyChat()
                logDebug { "SetSession ViewModel:$this , ChatView:${this@ChatView}" }

                this@ChatView.uiScope.launch {
                    launch {
                        eventFlow.collect {
                            logDebug { "Chat event flow : $it" }
                            when (it) {
                                EVENT_NEW_MESSAGE -> {
                                    // Auto scroll if user is looking at the latest messages
                                    autoScroll = true
                                    checkEmptyChat()
                                    if (viewModel?.isLastItemVisible == true && !binding.swipeToRefresh.isRefreshing && chatAdapter.isReactionPopUpShowing()
                                            .not()
                                    ) {
                                        snapToLive()
                                    } else if (chatAdapter.isReactionPopUpShowing() && (viewModel?.isLastItemVisible == true && chatAdapter.itemCount > 0)
                                        && !(binding.chatdisplay.canScrollVertically(-1)) && !(binding.chatdisplay.canScrollVertically(
                                            1
                                        ))
                                    ) {


                                        chatAdapter.notifyItemInserted(chatAdapter.currentList.size)
                                        binding.chatdisplay.smoothScrollToPosition(chatAdapter.itemCount)
                                    } else if (chatAdapter.isReactionPopUpShowing()) {
                                        showSnapToLive()
                                    }
                                }

                                EVENT_LOADING_COMPLETE -> {
                                    uiScope.launch {
                                        // Add delay to avoid UI glitch when recycler view is loading
                                        delay(500)
                                        hideLoadingSpinner()
                                        checkEmptyChat()
                                        logDebug { "loading complete: ${binding.swipeToRefresh.isRefreshing}" }
                                        if (!binding.swipeToRefresh.isRefreshing) snapToLive()
                                        binding.swipeToRefresh.isRefreshing = false
                                        eventFlow.tryEmit(null)
                                    }
                                }

                                EVENT_LOADING_STARTED -> {
                                    uiScope.launch {
                                        hideKeyboard()
                                        hideStickerKeyboard()
                                        initEmptyView()
                                        delay(400)
                                        showLoadingSpinner()
                                    }
                                }

                                EVENT_MESSAGE_CANNOT_SEND -> {
                                    uiScope.launch {
                                        AlertDialog.Builder(context).apply {
                                            setMessage(context.getString(R.string.send_message_failed_access_denied))
                                            setPositiveButton(context.getString(R.string.livelike_chat_report_message_confirm)) { _, _ ->
                                            }
                                            create()
                                        }.show()
                                    }
                                }
                            }
                        }
                    }
                    launch {
                        currentProfile = currentProfileOnce()
                        uiScope.launch {
                            binding.chatInput.userProfileDisplayLL.userProfileTv.visibility =
                                View.VISIBLE
                            binding.chatInput.userProfileDisplayLL.userProfileTv.text =
                                currentProfile?.nickname
                        }
                    }
                    launch {
                        programRepository?.programGamificationProfileFlow?.collect {
                            it?.let { programRank ->
                                binding.chatInput.userProfileDisplayLL.apply {
                                    if (programRank.newPoints == 0 || pointView.visibility == View.GONE) {
                                        pointView.showPoints(programRank.points)
                                        wouldShowBadge(programRank)
                                        showUserRank(programRank)
                                    } else if (programRank.points == programRank.newPoints) {
                                        pointView.apply {
                                            postDelayed(
                                                {
                                                    startAnimationFromTop(programRank.points)
                                                    showUserRank(programRank)
                                                }, 6300
                                            )
                                        }
                                    } else {
                                        pointView.apply {
                                            postDelayed(
                                                {
                                                    startAnimationFromTop(programRank.points)
                                                    showUserRank(programRank)
                                                }, 1000
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    }
                    launch {
                        animationEventsFlow?.collect {
                            if (it == ViewAnimationEvents.BADGE_COLLECTED) {
                                programRepository?.programGamificationProfileFlow?.value?.let { programGamificationProfile ->
                                    wouldShowBadge(programGamificationProfile, true)
                                }
                            }
                        }
                    }
                    launch {
                        delay(1000L)
                        chatAdapter.notifyDataSetChanged()
                    }
                }

                chatAdapter.checkListIsAtTop = lambda@{
                    val lm: LinearLayoutManager =
                        binding.chatdisplay.layoutManager as LinearLayoutManager
                    if (lm.findFirstVisibleItemPosition() == it) {
                        return@lambda true
                    }
                    return@lambda false
                }

                binding.chatInput.apply {
                    edittextChatMessage.addTextChangedListener(object : TextWatcher {
                        var containsImage = false
                        override fun afterTextChanged(s: Editable?) {
                            val matcher = s.toString().findImages()
                            if (matcher.find()) {
                                containsImage = true
                                replaceWithImages(
                                    s as Spannable, this@ChatView.context, callback, true
                                )
                                // cleanup before the image
                                if (matcher.start() > 0) edittextChatMessage.text?.delete(
                                    0, matcher.start()
                                )

                                // cleanup after the image
                                if (matcher.end() < s.length) edittextChatMessage.text?.delete(
                                    matcher.end(), s.length
                                )
                                // Move to end of line
                                edittextChatMessage.setSelection(
                                    edittextChatMessage.text?.length ?: 0
                                )
                                if (edittextChatMessage.text?.isNotEmpty() == true) wouldUpdateChatInputAccessibiltyFocus(
                                    100
                                )
                            } else if (containsImage) {
                                containsImage = false
                                s?.length?.let { edittextChatMessage.text?.delete(0, it) }
                            } else {
                                containsImage = false
                                replaceWithStickers(
                                    s as Spannable,
                                    this@ChatView.context,
                                    viewModel!!::getSticker,
                                    edittextChatMessage,
                                    null
                                )
                            }
                        }

                        override fun beforeTextChanged(
                            s: CharSequence?, start: Int, count: Int, after: Int
                        ) = Unit

                        override fun onTextChanged(
                            s: CharSequence?, start: Int, before: Int, count: Int
                        ) = Unit
                    })

                }
                /**
                 * stickers are loaded when emoji btn is clicked
                 * if sticker keyboard is already visible, then this image changes to qwerty button
                 * and on click of qwerty button normal keyboard opens
                 * and in case normal keyboard is already visible then this changes to emoji button
                 */
                binding.chatInput.buttonEmoji.setOnClickListener {
                    hidePopUpReactionPanel()
                    if (binding.stickerKeyboard.visibility == View.GONE) {
                        showStickerKeyboard()
                    } else {
                        hideStickerKeyboard()
                        showKeyboard()
                    }
                }
                if (supportImagePicker)
                    binding.chatInput.buttonPicker.setOnClickListener {
                        chatImagePickerDelegate.onImageButtonClicked()
                    }
                binding.chatdisplay.addOnLayoutChangeListener(layoutChangeListener)
            }
        }
    }


    /**
     * checks if any chats are available
     * if not/ empty list this shows a empty chat background
     */
    private fun checkEmptyChat() {
        emptyChatBackgroundView?.let {
            it.visibility = if ((session?.getLoadedMessages()?.size ?: 0) == 0) View.VISIBLE
            else View.GONE
        }
    }

    /**
     * opens up the gallery image picker dialog
     */
    private fun openPickerDialog() {
        val builder = AlertDialog.Builder(context)
        builder.setView(R.layout.bottom_sheet_layout)
        val dialog = builder.create()
        dialog.window?.decorView?.setBackgroundResource(R.drawable.dialog_background) // setting the background
        dialog.show()
        dialog.findViewById<TextView>(R.id.photo_tv)?.setOnClickListener {
            dialog.dismiss()
            pickMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
        }
    }

    /**
     * stickers keyboard initialization process
     */
    private fun initStickerKeyboard(
        stickerKeyboardView: StickerKeyboardView, chatViewModel: ChatViewModel
    ) {
        stickerKeyboardView.initTheme(chatAttribute)
        stickerKeyboardView.setChatRoom(
            chatViewModel.currentChatRoom?.id ?: "", chatViewModel.stickerPackListFlow
        ) {
            if (it.isNullOrEmpty()) {
                binding.chatInput.buttonEmoji.visibility = View.GONE
                binding.stickerKeyboard.visibility = View.GONE
            } else {
                binding.chatInput.buttonEmoji.visibility = View.VISIBLE
            }
            viewModel?.chatAdapter?.notifyDataSetChanged()
        }
        // used to pass the shortcode to the keyboard
        stickerKeyboardView.setOnClickListener(object : FragmentClickListener {
            override fun onClick(sticker: Sticker) {
                val textToInsert = ":${sticker.shortcode}:"
                val start = max(binding.chatInput.edittextChatMessage.selectionStart, 0)
                val end = max(binding.chatInput.edittextChatMessage.selectionEnd, 0)
                if (binding.chatInput.edittextChatMessage.text!!.length + textToInsert.length < 250) {
                    // replace selected text or start where the cursor is
                    binding.chatInput.edittextChatMessage.text?.replace(
                        min(start, end), max(start, end), textToInsert, 0, textToInsert.length
                    )
                }
            }
        })
    }

    private fun wouldShowBadge(
        programRank: com.livelike.engagementsdk.widget.data.models.ProgramGamificationProfile,
        animate: Boolean = false
    ) {
        var currentBadge = programRank.newBadges?.maxOrNull()
        if (currentBadge == null) {
            currentBadge = programRank.currentBadge
        }
        currentBadge?.let {
            binding.chatInput.userProfileDisplayLL.gamificationBadgeIv.visibility = View.VISIBLE
            binding.chatInput.userProfileDisplayLL.gamificationBadgeIv.loadImage(
                it.imageFile, dpToPx(14)
            )
            if (animate) {
                binding.chatInput.userProfileDisplayLL.gamificationBadgeIv.buildScaleAnimator(
                    0f, 1f, 1000
                ).start()
            }
        }
    }

    private fun hideGamification() {
        binding.chatInput.userProfileDisplayLL.pointView.visibility = View.GONE
        binding.chatInput.userProfileDisplayLL.rankLabel.visibility = View.GONE
        binding.chatInput.userProfileDisplayLL.rankValue.visibility = View.GONE
        binding.chatInput.userProfileDisplayLL.gamificationBadgeIv.visibility = View.GONE
    }

    private fun showUserRank(programGamificationProfile: com.livelike.engagementsdk.widget.data.models.ProgramGamificationProfile) {
        binding.chatInput.userProfileDisplayLL.apply {
            if (programGamificationProfile.points > 0) {
                rankLabel.visibility = View.VISIBLE
                rankValue.visibility = View.VISIBLE
                uiScope.launch {
                    delay(1000)
                    rankValue.text = "#${programGamificationProfile.rank}"
                }
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthDp = AndroidResource.pxToDp(width)
        if (widthDp < CHAT_MINIMUM_SIZE_DP && widthDp != 0) {
            logError { "[CONFIG ERROR] Current ChatView Width is $widthDp, it must be more than 292dp or won't display on the screen." }
            setMeasuredDimension(0, 0)
            return
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    /**
     * Hides keyboard on clicking outside of edittext
     */
    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        val v = context.scanForActivity()?.currentFocus
        if (v != null && (ev?.action == MotionEvent.ACTION_UP || ev?.action == MotionEvent.ACTION_MOVE) && (v is EditText || v is ChatView) && !v.javaClass.name.startsWith(
                "android.webkit."
            )
        ) {
            val scrcoords = IntArray(2)
            v.getLocationOnScreen(scrcoords)
            val y = ev.rawY + v.top - scrcoords[1]
            var outsideStickerKeyboardBound =
                (v.bottom - binding.stickerKeyboard.height - binding.chatInput.buttonChatSend.height - binding.chatInput.buttonEmoji.height)
            if (binding.chatInput.buttonChatSend.height == 0) {
                outsideStickerKeyboardBound -= chatAttribute.sendIconHeight
            }
            // Added check for image_height greater than 0 so bound position for touch should be above the send icon
            if (!binding.chatInput.edittextChatMessage.isTouching) {
                if (y < v.top || y > v.bottom || (y < outsideStickerKeyboardBound)) {
                    hideStickerKeyboard()
                    hideKeyboard()
                }
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    fun hidePopUpReactionPanel() {
        viewModel?.chatAdapter?.currentChatReactionPopUpViewPos?.let {
            (binding.chatdisplay.findViewHolderForAdapterPosition(it) as? ChatRecyclerAdapter.ViewHolder)?.hideFloatingUI()
        }
    }

    // private var isLastItemVisible = true
    private var autoScroll = false

    /**
     *  Sets the data source for this view.
     *  @param chatAdapter ChatAdapter used for creating this view.
     */
    private fun setDataSource(chatAdapter: ChatRecyclerAdapter) {
        binding.chatdisplay.let { rv ->
            chatAdapter.chatViewDelegate = chatViewDelegate
            chatAdapter.chatViewSeparatorContentDelegate = chatViewSeparatorContentDelegate
            rv.adapter = chatAdapter
            val lm = rv.layoutManager as LinearLayoutManager
            lm.recycleChildrenOnDetach = true
            rv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(
                    rv: RecyclerView, dx: Int, dy: Int
                ) {
                    if (((binding.chatdisplay.canScrollVertically(-1))
                                && (binding.chatdisplay.canScrollVertically(1)) && chatAdapter.isReactionPopUpShowing())
                    ) {
                        hidePopUpReactionPanel()
                    }
                    //hidePopUpReactionPanel() removed due to bug ESINTEG-404
                    val totalItemCount = lm.itemCount
                    val lastVisible = lm.findLastVisibleItemPosition()
                    val endHasBeenReached = lastVisible + 5 >= totalItemCount
                    if (!autoScroll) viewModel?.isLastItemVisible =
                        if (totalItemCount > 0 && endHasBeenReached) {
                            hideSnapToLive()
                            true
                        } else {
                            showSnapToLive()
                            false
                        }
                    if (endHasBeenReached) {
                        viewModel?.isLastItemVisible = true
                        autoScroll = false
                    }
                }
            })
            if (enableQuoteMessage && isChatInputVisible) {
                val messageSwipeController =
                    MessageSwipeController(context, object : SwipeControllerActions {
                        override fun showReplyUI(position: Int) {
                            val chatMessage = chatAdapter.getChatMessage(position)
                            if (chatMessage.messageEvent == PubnubChatEventType.CUSTOM_MESSAGE_CREATED) {
                                logDebug { "Not Allowed to quote message on Custom Message" }
                                (session as? ChatSession)?.errorDelegate?.onError("Not Allowed to quote message on Custom Message")
                            } else {
                                if (!chatMessage.isDeleted && chatMessage.getUnixTimeStamp() != null) currentQuoteMessage =
                                    chatMessage.copy()
                                else {
                                    logDebug { "Not Allowed to quote message because the chat message is deleted or not yet sent" }
                                    (session as? ChatSession)?.errorDelegate?.onError("Not Allowed to quote message")
                                }
                            }
                        }
                    })
                itemTouchHelper = ItemTouchHelper(messageSwipeController)
                itemTouchHelper?.attachToRecyclerView(rv)
            }
        }

        chatSnapToLiveDelegate.getSnapToLiveView().setOnClickListener {
            hidePopUpReactionPanel()
            autoScroll = true
            snapToLive()
        }

        binding.chatInput.buttonChatSend.let { buttonChat ->
            buttonChat.setOnClickListener { sendMessageNow() }

            if (binding.chatInput.edittextChatMessage.text.isNullOrEmpty()) {
                buttonChat.visibility = View.GONE
                buttonChat.isEnabled = false

            } else {
                buttonChat.isEnabled = true
                buttonChat.visibility = View.VISIBLE
            }

            binding.chatInput.edittextChatMessage.apply {
                addTextChangedListener(object : TextWatcher {
                    var previousText: CharSequence = ""
                    override fun beforeTextChanged(
                        s: CharSequence, start: Int, count: Int, after: Int
                    ) {
                        previousText = s
                    }

                    override fun onTextChanged(
                        s: CharSequence, start: Int, before: Int, count: Int
                    ) {
                    }

                    override fun afterTextChanged(s: Editable) {
                        if (s.isNotEmpty()) {
                            buttonChat.isEnabled = true
                            buttonChat.visibility = View.VISIBLE
                            if (supportImagePicker)
                                binding.chatInput.buttonPicker.visibility = View.GONE
                        } else {
                            buttonChat.isEnabled = false
                            buttonChat.visibility = View.GONE
                            if (supportImagePicker)
                                binding.chatInput.buttonPicker.visibility = View.VISIBLE
                            binding.chatInput.buttonEmoji.visibility = View.VISIBLE
                        }
                    }
                })

                setOnFocusChangeListener { _, hasFocus ->
                    if (hasFocus) {
                        hidePopUpReactionPanel()
                        (session as? ChatSession)?.analyticsService?.trackKeyboardOpen(
                            KeyboardType.STANDARD
                        )
                        hideStickerKeyboard()
                        viewModel?.chatAdapter?.isKeyboardOpen = true
                    }
                }
            }
        }
    }

    private fun updateInputView() {
        binding.chatInput.apply {
            layQuoteMessage.visibility =
                when (currentQuoteMessage != null && isChatInputVisible) {
                    true -> {
                        binding.chatInput.quoteChatMessageNickname.text =
                            currentQuoteMessage?.nickname
                        chatAttribute.apply {
                            var options = RequestOptions()
                            if (chatAvatarCircle) {
                                options = options.optionalCircleCrop()
                            }
                            if (chatAvatarRadius > 0) {
                                options = options.transform(
                                    CenterCrop(), RoundedCorners(chatAvatarRadius)
                                )
                            }
                            if (currentQuoteMessage!!.profilePic.isNullOrEmpty()) {
                                // load local image
                                Glide.with(context.applicationContext)
                                    .load(R.drawable.default_avatar)
                                    .apply(options).placeholder(chatUserPicDrawable)
                                    .into(imgQuoteChatAvatar)
                            } else {
                                Glide.with(context.applicationContext)
                                    .load(currentQuoteMessage!!.profilePic)
                                    .apply(options).placeholder(chatUserPicDrawable)
                                    .error(chatUserPicDrawable).into(imgQuoteChatAvatar)
                            }
                            viewModel?.showChatAvatarLogo?.let {
                                imgQuoteChatAvatar.apply {
                                    visibility = if (it) View.VISIBLE else View.GONE
                                }
                            }
                            if (enableChatMessageURLs) {
                                quoteChatMessage.apply {
                                    linksClickable = enableChatMessageURLs
                                    setLinkTextColor(quoteChatMessageLinkTextColor)
                                    movementMethod = LinkMovementMethod.getInstance()
                                }
                            }
                            currentQuoteMessage?.let {
                                viewModel?.chatAdapter?.linksRegex?.let {
                                    viewModel?.analyticsService?.let {
                                        setTextOrImageToView(
                                            currentQuoteMessage,
                                            quoteChatMessage,
                                            imgQuoteChatMessage,
                                            true,
                                            quoteChatMessageTextSize,
                                            viewModel!!::getSticker,
                                            enableChatMessageURLs,
                                            context.resources.displayMetrics.density,
                                            viewModel?.chatAdapter?.linksRegex!!,
                                            viewModel?.chatAdapter?.chatRoomId,
                                            viewModel?.chatAdapter?.chatRoomName,
                                            viewModel?.analyticsService!!,
                                            true,
                                            currentProfile?.id
                                        )
                                    }
                                }
                            }
                        }
                        layQuoteMessage.accessibilityDelegate =
                            object : AccessibilityDelegate() {
                                override fun onInitializeAccessibilityNodeInfo(
                                    host: View, info: AccessibilityNodeInfo
                                ) {
                                    super.onInitializeAccessibilityNodeInfo(host, info)
                                    val isExternalImage =
                                        currentQuoteMessage?.message?.findImages()?.matches()
                                            ?: false
                                    info.text = context.getString(
                                        R.string.livelike_send_quote_message_input_accessibility,
                                        when (isExternalImage) {
                                            true -> context.getString(R.string.image)
                                            false -> currentQuoteMessage?.message
                                        }
                                    )
                                }
                            }
                        View.VISIBLE
                    }

                    else -> View.GONE
                }
            if (currentQuoteMessage != null && isChatInputVisible) {
                layQuoteMessage.postDelayed(
                    {
                        layQuoteMessage.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
                    }, 100
                )
            }
        }
    }

    /**
     * used for hiding sticker keyboard / sticker view
     **/
    private fun hideStickerKeyboard(/*reason: KeyboardHideReason*/) {
        chatAttribute.apply {
            binding.chatInput.buttonEmoji.setImageDrawable(chatStickerSendDrawable)
        }

        findViewById<StickerKeyboardView>(R.id.sticker_keyboard)?.apply {
            //            if (visibility == View.VISIBLE) {
//                session?.analyticService?.trackKeyboardClose(KeyboardType.STICKER, reason)
//            }
            visibility = View.GONE
        }
        updateSnapToLiveBottomMargin(false)
    }

    /**
     * used for showing sticker keyboard / sticker view
     **/
    private fun showStickerKeyboard() {
        uiScope.launch {
            hideKeyboard()
            (session as? ChatSession)?.analyticsService?.trackKeyboardOpen(KeyboardType.STICKER)
            delay(200) // delay to make sure the keyboard is hidden
            findViewById<StickerKeyboardView>(R.id.sticker_keyboard)?.visibility = View.VISIBLE

            chatAttribute.apply {
                binding.chatInput.buttonEmoji.setImageDrawable(chatStickerKeyboardSendDrawable)
            }
            viewModel?.chatAdapter?.isKeyboardOpen = true
            updateSnapToLiveBottomMargin(true)
        }
    }

    private fun showLoadingSpinner() {
        binding.apply {
            loadingSpinner.visibility = View.VISIBLE
            chatInput.root.visibility = View.GONE
            chatdisplay.visibility = View.GONE
            chatSnapToLiveDelegate.getSnapToLiveView().visibility = View.GONE
        }
    }

    private fun hideLoadingSpinner() {
        logDebug { "ChatView: isChatInputVisible:$isChatInputVisible" }
        binding.apply {
            loadingSpinner.visibility = View.GONE
            if (isChatInputVisible) {
                chatInput.root.visibility = View.VISIBLE
            }
            chatdisplay.visibility = View.VISIBLE
            wouldUpdateChatInputAccessibiltyFocus()
        }
    }

    private fun wouldUpdateChatInputAccessibiltyFocus(time: Long = 500) {
        binding.chatInput.root.postDelayed(
            {
                binding.chatInput.edittextChatMessage.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
            }, time
        )
    }

    /**
     * this is used to hide default keyboard
     **/
    private fun hideKeyboard(/*reason: KeyboardHideReason*/) {
        val inputManager =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(
            binding.chatInput.edittextChatMessage.windowToken, 0
        )

//        session?.analyticService?.trackKeyboardClose(KeyboardType.STANDARD, reason)
        setBackButtonInterceptor(this)
    }

    /**
     * this is used to show default keyboard
     **/
    private fun showKeyboard() {
        binding.chatInput.edittextChatMessage.requestFocus()
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.toggleSoftInput(
            InputMethodManager.SHOW_FORCED,
            InputMethodManager.HIDE_IMPLICIT_ONLY
        )
        viewModel?.chatAdapter?.isKeyboardOpen = true
    }

    /**
     * use this to listen messages sent from this view
     **/
    var sentMessageListener: ((message: LiveLikeChatMessage) -> Unit)? = null

    /**
     * Use this function to hide any soft and sticker keyboards over the view.
     **/
    fun dismissKeyboard() {
        hideKeyboard()
        hideStickerKeyboard()
    }

    /**
     * This function is used to send message, that user enters
     **/
    private fun sendMessageNow() {
        if (binding.chatInput.edittextChatMessage.text.isNullOrBlank()) {
            // Do nothing if the message is blank or empty
            return
        }
        if (enableQuoteMessage) {
            if (currentQuoteMessage?.quoteMessage != null) {
                currentQuoteMessage?.quoteMessage = null
            }
        }
        val msg = binding.chatInput.edittextChatMessage.text.toString().trim()
        val hasExternalImage = msg.findImages().countMatches() > 0
        // add code to get the actual height of image to set in presend chat message
        val callback: com.livelike.common.LiveLikeCallback<LiveLikeChatMessage> = { result, error ->
            uiScope.launch {
                result?.let { chatMessage ->
                    sentMessageListener?.invoke(chatMessage)
                    viewModel?.apply {
                        displayChatMessage(chatMessage, currentProfileOnce().id)
                        binding.chatInput.edittextChatMessage.setText("")
                        currentQuoteMessage = null
                        snapToLive()
                    }
                }
                error?.let {
                    logError { it }
                    (session as? ChatSession)?.errorDelegate?.onError(it)
                }
            }
        }

        when {
            enableQuoteMessage && currentQuoteMessage != null -> session?.quoteMessage(
                message = when (hasExternalImage) {
                    false -> msg
                    else -> null
                },
                imageUrl = when (hasExternalImage) {
                    true -> msg.getUrlFromMessageComponent()
                    else -> null
                },
                imageWidth = null,
                imageHeight = null,
                quoteMessageId = currentQuoteMessage!!.id ?: "",
                quoteMessage = currentQuoteMessage!!,
                messageMetadata = null,
                liveLikePreCallback = callback,
                chatInterceptor = chatInterceptor
            )

            else -> session?.sendMessage(
                message = when (hasExternalImage) {
                    false -> msg
                    else -> null
                },
                imageUrl = when (hasExternalImage) {
                    true -> msg.getUrlFromMessageComponent()
                    else -> null
                },
                imageWidth = null,
                imageHeight = null,
                messageMetadata = null,
                liveLikePreCallback = callback,
                chatInterceptor = chatInterceptor
            )
        }
    }

    /**
     * used for hiding the Snap to live button
     * snap to love is mainly responsible for showing user the latest message
     * if user is already at the latest message,then usually this icon remain hidden
     **/
    private fun hideSnapToLive() {
        logDebug { "Chat hide Snap to Live: $showingSnapToLive" }
        if (!showingSnapToLive) return
        showingSnapToLive = false
        chatSnapToLiveDelegate.getSnapToLiveView().visibility = View.GONE
        animateSnapToLiveButton()
    }

    /**
     * used for showing the Snap to Live button
     **/
    private fun showSnapToLive() {
        logDebug { "Chat show Snap to Live: $showingSnapToLive" }
        if (showingSnapToLive) return
        showingSnapToLive = true
        chatSnapToLiveDelegate.getSnapToLiveView().visibility = View.VISIBLE
        animateSnapToLiveButton()
    }

    private fun animateSnapToLiveButton() {
        snapToLiveAnimation?.cancel()

        val translateAnimation = ObjectAnimator.ofFloat(
            chatSnapToLiveDelegate.getSnapToLiveView(),
            "translationY",
            if (showingSnapToLive) 0f else dpToPx(if (displayUserProfile) SNAP_TO_LIVE_ANIMATION_DESTINATION else 10).toFloat()
        )
        translateAnimation?.duration = SNAP_TO_LIVE_ANIMATION_DURATION.toLong()
        val alphaAnimation =
            ObjectAnimator.ofFloat(
                chatSnapToLiveDelegate.getSnapToLiveView(),
                "alpha",
                if (showingSnapToLive) 1f else 0f
            )
        alphaAnimation.duration = (SNAP_TO_LIVE_ALPHA_ANIMATION_DURATION).toLong()
        alphaAnimation.addListener(object : Animator.AnimatorListener {
            override fun onAnimationEnd(animation: Animator) {
                chatSnapToLiveDelegate.getSnapToLiveView().visibility =
                    if (showingSnapToLive) View.VISIBLE else View.GONE
            }

            override fun onAnimationStart(animation: Animator) {
                chatSnapToLiveDelegate.getSnapToLiveView().visibility =
                    if (showingSnapToLive) View.GONE else View.VISIBLE
            }

            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })

        snapToLiveAnimation = AnimatorSet()
        snapToLiveAnimation?.play(translateAnimation)?.with(alphaAnimation)
        snapToLiveAnimation?.start()
    }

    private fun snapToLive() {
        binding.chatdisplay.let { rv ->
            hideSnapToLive()
            session?.getLoadedMessages()?.size?.let {
                val lm = rv.layoutManager as LinearLayoutManager
                val lastVisiblePosition = lm.itemCount - lm.findLastVisibleItemPosition()
                if (lastVisiblePosition < SMOOTH_SCROLL_MESSAGE_COUNT_LIMIT) {
                    binding.chatdisplay.postDelayed(
                        {
                            rv.smoothScrollToPosition(it)
                        }, 200
                    )
                } else {
                    binding.chatdisplay.postDelayed(
                        {
                            rv.scrollToPosition(it - 1)
                        }, 200
                    )
                }
            }
        }
    }

    /**
     * used for customizing the scrollbar
     * @param isScrollBarNeeded to show if scrollbar needed to be shown or not
     * @param scrollbarDrawable customized drawable for scrollbar
     **/
    fun setScrollBarEnabled(
        isScrollBarNeeded: Boolean,
        scrollbarDrawable: Drawable? = AppCompatResources.getDrawable(
            context,
            android.R.color.darker_gray
        )
    ) {
        if (this::binding.isInitialized) {
            binding.chatdisplay.isVerticalScrollBarEnabled = isScrollBarNeeded
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                if (isScrollBarNeeded) {
                    scrollbarDrawable?.let {
                        binding.chatdisplay.isScrollbarFadingEnabled = false
                        binding.chatdisplay.scrollBarFadeDuration = 0
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                            binding.chatdisplay.verticalScrollbarThumbDrawable = it
                        }
                    }
                }
            }
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        logDebug { "exoplayer - on Attched called" }
        wouldUpdateChatInputAccessibiltyFocus()
        registerForResult()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        // added to dismiss popup reaction panel on fragment replace
        binding.chatdisplay.removeOnLayoutChangeListener(layoutChangeListener)
        viewModel?.chatAdapter?.chatPopUpView?.dismiss()
        viewModel?.chatAdapter?.chatViewSeparatorContentDelegate = null
        uiScope.cancel()
    }


    private fun registerForResult() {
        try {
            val fragment = findFragment<Fragment>()
            pickMedia =
                fragment.registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri: Uri? ->
                    // Callback is invoked after the user selects a media item or closes the
                    // photo picker.
                    if (uri != null) {
                        logDebug { "pickMedia : $uri" }
                        chatImagePickerListener.onImagePick(uri)
                    } else {
                        logError { "No media Selected" }
                    }
                }

        } catch (exception: Exception) {
            logError { exception.message }
            exception.message?.let { (session as? ChatSession)?.errorDelegate?.onError(it) }
        }
    }

    /**
     *This should generally be called to cleanup at time of onDestroy of activity and onDestroyView of fragment
     **/
    fun clearSession() {
        logDebug { "Clear Chat Session ViewModel:$viewModel ,ChatView:$this" }
        viewModel?.apply {
            chatAdapter.checkListIsAtTop = null
            chatAdapter.mRecyclerView = null
            chatAdapter.messageTimeFormatter = null
            chatAdapter.chatViewSeparatorContentDelegate = null
            chatAdapter.deleteDialogInterceptor = null
            session?.setMessageWithReactionListener(null)
            session?.setMessageListener(null)
        }
        binding.chatdisplay.adapter = null
        itemTouchHelper?.attachToRecyclerView(null)
        targetDrawables.clear()
        targetByteArrays.clear()
    }

    companion object {
        const val SNAP_TO_LIVE_ANIMATION_DURATION = 400F
        const val SNAP_TO_LIVE_ALPHA_ANIMATION_DURATION = 320F
        const val SNAP_TO_LIVE_ANIMATION_DESTINATION = 50
        private const val CHAT_MINIMUM_SIZE_DP = 292
        private const val SMOOTH_SCROLL_MESSAGE_COUNT_LIMIT = 100
    }
}
