package com.livelike.engagementsdk.chat.stickerKeyboard

import android.animation.LayoutTransition
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayout
import com.livelike.engagementsdk.R
import com.livelike.engagementsdk.chat.ChatViewThemeAttributes
import com.livelike.engagementsdk.chat.utils.liveLikeSharedPrefs.filterRecentStickers
import com.livelike.engagementsdk.core.utils.AndroidResource
import com.livelike.engagementsdk.databinding.LivelikeStickerKeyboardPagerBinding
import com.livelike.utils.logDebug
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch


class StickerKeyboardView(context: Context?, attributes: AttributeSet? = null) :
    ConstraintLayout(context!!, attributes) {
    private var chatViewThemeAttributes: ChatViewThemeAttributes? = null
    private var binding: LivelikeStickerKeyboardPagerBinding
    private val uiScope = MainScope()

    init {
        binding =
            LivelikeStickerKeyboardPagerBinding.inflate(LayoutInflater.from(context), this, true)
        layoutTransition = LayoutTransition()
    }

    fun initTheme(themeAttributes: ChatViewThemeAttributes) {
        chatViewThemeAttributes = themeAttributes
        themeAttributes.apply {
            binding.apply {
                pager.background = stickerBackground
                pagerTab.background = stickerTabBackground
                pagerTab.setSelectedTabIndicatorColor(stickerSelectedTabIndicatorColor)
            }
        }
    }

    override fun onVisibilityChanged(changedView: View, visibility: Int) {
        if (visibility == View.VISIBLE) {
            val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(changedView.windowToken, 0)
        }
        super.onVisibilityChanged(changedView, visibility)
    }

    private fun createTabItemView(stickerPack: StickerPack? = null): View {
        val imageView = ImageView(context)
        imageView.contentDescription =
            stickerPack?.name ?: context.getString(R.string.recent_sticker)
        chatViewThemeAttributes?.let {
            if (stickerPack?.file == null)
                imageView.setColorFilter(
                    it.stickerSelectedTabIndicatorColor,
                    android.graphics.PorterDuff.Mode.MULTIPLY
                )
        }
        imageView.layoutParams = ViewGroup.LayoutParams(
            AndroidResource.dpToPx(24),
            AndroidResource.dpToPx(24)
        )
        Glide.with(this).load((stickerPack?.file) ?: R.drawable.keyboard_ic_recent).into(imageView)
        return imageView
    }

    fun setChatRoom(
        chatRoomId: String,
        stickerPacksFlow: Flow<List<StickerPack>>,
        onLoaded: ((List<StickerPack>?) -> Unit)? = null
    ) {
        uiScope.launch {
            stickerPacksFlow.collect { stickerPacks ->
                onLoaded?.invoke(stickerPacks)
                binding.apply {
                    logDebug { "sticker pack: ${stickerPacks.size}" }
                    filterRecentStickers(chatRoomId, stickerPacks)
                    val stickerCollectionPagerAdapter = StickerCollectionAdapter(
                        stickerPacks, chatRoomId,
                        emptyRecentTextColor = chatViewThemeAttributes?.stickerRecentEmptyTextColor
                            ?: Color.WHITE
                    ) { s -> listener?.onClick(s) }
                    pagerTab.removeAllTabs()
                    pager.layoutManager =
                        LinearLayoutManager(
                            context,
                            LinearLayoutManager.HORIZONTAL,
                            false
                        )
                    pager.adapter = stickerCollectionPagerAdapter
                    val pageListener =
                        object : TabLayout.TabLayoutOnPageChangeListener(pagerTab) {}
                    RVPagerSnapHelperListenable().attachToRecyclerView(
                        pager,
                        object : RVPagerStateListener {
                            override fun onPageScroll(pagesState: List<VisiblePageState>) {
                                for (pos in pagesState.indices) {
                                    val state = pagesState[pos]
                                    var diff = (1.0f - state.distanceToSettled)
                                    if (diff < 0.0f) {
                                        diff = 0.0f
                                    } else if (diff > 1.0f) {
                                        diff = 1.0f
                                    }
                                    if (pos == 0) {
                                        pageListener.onPageScrolled(
                                            state.index,
                                            diff,
                                            state.distanceToSettledPixels
                                        )
                                    }
                                }
                            }

                            override fun onScrollStateChanged(state: RVPageScrollState) {
                                pageListener.onPageScrollStateChanged(
                                    when (state) {
                                        RVPageScrollState.Idle -> {
                                            pagerTab.getTabAt(pagerTab.selectedTabPosition)
                                                ?.select()
                                            ViewPager.SCROLL_STATE_IDLE
                                        }
                                        RVPageScrollState.Dragging -> ViewPager.SCROLL_STATE_DRAGGING
                                        RVPageScrollState.Settling -> ViewPager.SCROLL_STATE_SETTLING
                                    }
                                )
                            }

                            override fun onPageSelected(index: Int) {
                                pageListener.onPageSelected(index)
                            }
                        }
                    )

                    val listener = object : TabLayout.OnTabSelectedListener {
                        override fun onTabReselected(p0: TabLayout.Tab?) {
                        }

                        override fun onTabUnselected(p0: TabLayout.Tab?) {
                        }

                        override fun onTabSelected(p0: TabLayout.Tab?) {
                            logDebug { "Sticker Tab Selected :${p0?.position}" }
                            p0?.let {
                                pager.smoothScrollToPosition(p0.position)
                            }
                        }
                    }
                    pagerTab.addOnTabSelectedListener(listener)
                    for (i in 0 until stickerCollectionPagerAdapter.itemCount) {
                        val tab = pagerTab.newTab()
                        if (i == StickerCollectionAdapter.RECENT_STICKERS_POSITION) {
                            tab.customView = createTabItemView()
                        } else {
                            tab.customView = createTabItemView(stickerPacks[i - 1])
                        }
                        pagerTab.addTab(tab)
                    }
                }
                //viewModel?.preload(context.applicationContext)
            }
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        uiScope.cancel()
    }

    private var listener: FragmentClickListener? = null

    fun setOnClickListener(listener: FragmentClickListener) {
        this.listener = listener
    }
}
