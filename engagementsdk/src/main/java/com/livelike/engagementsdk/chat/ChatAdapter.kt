package com.livelike.engagementsdk.chat

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Patterns
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.livelike.common.model.PubnubChatEventType
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.R
import com.livelike.engagementsdk.chat.chatreaction.ChatActionsPopupView
import com.livelike.engagementsdk.chat.chatreaction.Reaction
import com.livelike.engagementsdk.chat.chatreaction.SelectReactionListener
import com.livelike.engagementsdk.chat.stickerKeyboard.Sticker
import com.livelike.engagementsdk.chat.utils.setTextOrImageToView
import com.livelike.engagementsdk.core.utils.AndroidResource
import com.livelike.engagementsdk.databinding.DefaultChatCellBinding
import com.livelike.engagementsdk.publicapis.ChatMessageType
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage
import com.livelike.engagementsdk.publicapis.toChatMessageType
import com.livelike.engagementsdk.widget.view.loadImage
import com.livelike.utils.logError
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.ln
import kotlin.math.pow

private val diffChatMessage: DiffUtil.ItemCallback<LiveLikeChatMessage> =
    object : DiffUtil.ItemCallback<LiveLikeChatMessage>() {
        override fun areItemsTheSame(p0: LiveLikeChatMessage, p1: LiveLikeChatMessage): Boolean {
            return p0.id == p1.id
        }

        //Do not add logs in these methods or remove logs after testing as it will affect he chatview functionality
        override fun areContentsTheSame(p0: LiveLikeChatMessage, p1: LiveLikeChatMessage): Boolean {
            return p0.message == p1.message && p0.nickname == p1.nickname
        }
    }

internal class ChatRecyclerAdapter(
    internal var analyticsService: AnalyticsService,
    private val reporter: (LiveLikeChatMessage) -> Unit,
    private val blockProfile: (String) -> Unit,
    private val getSticker: (String) -> Sticker?,
    private val sendMessageReaction: (String, String) -> Unit,
    private val removeMessageReaction: (String, String) -> Unit,
    private val deleteOwnMessage: (String) -> Unit,
    private val currentUserId: String
) : ListAdapter<LiveLikeChatMessage, RecyclerView.ViewHolder>(diffChatMessage) {
    var isKeyboardOpen: Boolean = false
    internal var chatRoomId: String? = null
    internal var showChatAvatarLogo: Boolean = true
    internal var chatRoomName: String? = null
    private var lastFloatingUiAnchorView: View? = null
//    var chatRepository: ChatRepository? = null

    internal var showLinks = false
    internal var linksRegex = Patterns.WEB_URL.toRegex()
    var checkListIsAtTop: (((position: Int) -> Boolean)?) = null
    internal var enableDeleteMessage: Boolean = true
    internal var deleteDialogInterceptor: DialogInterceptor? = null
    private val uiScope = MainScope()
    lateinit var chatViewThemeAttribute: ChatViewThemeAttributes
    var reactionList: List<Reaction> = emptyList()

    internal var isPublicChat: Boolean = true
    internal var mRecyclerView: RecyclerView? = null
    internal var messageTimeFormatter: ((time: Long?) -> String)? = null
    internal var reactionCountFormatter: ((count: Int) -> String)? = null
    var currentChatReactionPopUpViewPos: Int = -1
    internal var chatPopUpView: ChatActionsPopupView? = null

    var chatViewDelegate: ChatViewDelegate? = null
    var hideDeletedMessage:Boolean = false
    var chatViewSeparatorContentDelegate: ChatViewSeparatorContentDelegate? = null
    var enableQuoteMessage: Boolean = false

    override fun onCreateViewHolder(root: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (chatViewDelegate != null) {
            val messageType = ChatMessageType.values().find { it.ordinal == viewType }
            if (messageType == ChatMessageType.CUSTOM_MESSAGE_CREATED) {
                return chatViewDelegate!!.onCreateViewHolder(root, messageType)
            }
        }
        return ViewHolder(
            DefaultChatCellBinding.inflate(
                LayoutInflater.from(root.context),
                root,
                false
            )
        )
    }

    override fun getItemViewType(position: Int): Int {
        return if (chatViewDelegate != null)
            getItem(position).messageEvent.toChatMessageType()?.ordinal ?: -1
        else
            super.getItemViewType(position)
    }

    fun isReactionPopUpShowing(): Boolean {
        return chatPopUpView?.isShowing ?: false
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val chatMessage = getItem(position)
        if (chatViewDelegate != null && chatMessage.messageEvent == PubnubChatEventType.CUSTOM_MESSAGE_CREATED) {
            chatViewDelegate!!.onBindViewHolder(
                holder,
                chatMessage,
                chatViewThemeAttribute,
                showChatAvatarLogo
            )
        } else if (holder is ViewHolder) {
            holder.bindTo(
                chatMessage,
                chatViewSeparatorContentDelegate?.getSeparatorView(
                    when (position) {
                        0 -> null
                        else -> getItem(position - 1)
                    },
                    getItem(position)
                )
            )
        }
        holder.itemView.requestLayout()
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        mRecyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        mRecyclerView = null
    }

    fun getChatMessage(position: Int): LiveLikeChatMessage = getItem(position)

    /** Commenting this code for now so QA finalize whether old issues are coming or not
    Flowing code helps in accessbility related issues
     **/
//    override fun onViewDetachedFromWindow(holder: ViewHolder) {
//        holder.hideFloatingUI()
//        super.onViewDetachedFromWindow(holder)
//    }

    inner class ViewHolder(val binding: DefaultChatCellBinding) :
        RecyclerView.ViewHolder(binding.root),
        View.OnLongClickListener,
        View.OnClickListener {
        private var message: LiveLikeChatMessage? = null
        private val bounceAnimation: Animation =
            AnimationUtils.loadAnimation(binding.root.context, R.anim.bounce_animation)

        @SuppressLint("StringFormatInvalid")
        private val dialogOptions = listOf(
            binding.root.context.getString(R.string.flag_ui_blocking_title) to { msg: LiveLikeChatMessage ->
                AlertDialog.Builder(binding.root.context).apply {
                    setMessage(
                        context.getString(
                            R.string.flag_ui_blocking_message,
                            msg.nickname
                        )
                    )
                    setPositiveButton(context.getString(R.string.livelike_chat_alert_blocked_confirm)) { _, _ ->

                    }
                    create()
                }.show()
            },
            binding.root.context.getString(R.string.flag_ui_reporting_title) to { _: LiveLikeChatMessage ->
                AlertDialog.Builder(binding.root.context).apply {
                    setMessage(context.getString(R.string.flag_ui_reporting_message))
                    setPositiveButton(context.getString(R.string.livelike_chat_report_message_confirm)) { _, _ ->

                    }
                    create()
                }.show()
            }
        )

        init {
            chatViewThemeAttribute.chatBubbleBackgroundRes.let { res ->
                if (res < 0) {
                    binding.chatBubbleBackground.setBackgroundColor(res)
                } else {
                    val value = TypedValue()
                    binding.root.context.resources.getValue(res, value, true)
                    when {
                        value.type == TypedValue.TYPE_REFERENCE -> binding.chatBubbleBackground.setBackgroundResource(
                            res
                        )

                        value.type == TypedValue.TYPE_NULL -> binding.chatBubbleBackground.setBackgroundResource(
                            R.drawable.ic_chat_message_bubble_rounded_rectangle
                        )

                        value.type >= TypedValue.TYPE_FIRST_COLOR_INT && value.type <= TypedValue.TYPE_LAST_COLOR_INT -> ColorDrawable(
                            value.data
                        )

                        else -> binding.chatBubbleBackground.setBackgroundResource(R.drawable.ic_chat_message_bubble_rounded_rectangle)
                    }
                }
            }
            binding.root.setOnLongClickListener(this)
            binding.root.setOnClickListener(this)
        }

        override fun onLongClick(view: View?): Boolean {
            return true
        }

        private fun wouldShowFloatingUi(view: View?) {
            if (lastFloatingUiAnchorView == view) {
                lastFloatingUiAnchorView = null
                return
            }
            lastFloatingUiAnchorView = view
            val isOwnMessage = (view?.tag as LiveLikeChatMessage?)?.isFromMe ?: false
            val isDeletedMessage = (view?.tag as LiveLikeChatMessage?)?.isDeleted ?: false
            val reactionsAvailable = reactionList.isNotEmpty()
            if (!isDeletedMessage) {
                showFloatingUI(
                    isOwnMessage,
                    (checkListIsAtTop?.invoke(adapterPosition) ?: false) && itemCount > 1,
                    reactionsAvailable
                )
            }
        }

        override fun onClick(view: View?) {
            if (chatPopUpView?.isShowing == true) {
                hideFloatingUI()
            } else {
                if (!isKeyboardOpen)
                    wouldShowFloatingUi(view)
                else
                    isKeyboardOpen = false
            }
        }

        fun bindTo(item: LiveLikeChatMessage?, separatorView: View?) {
            binding.root.tag = item
            if(this@ChatRecyclerAdapter.hideDeletedMessage && item?.isDeleted == true){
                    separatorView?.visibility = View.GONE
                    // Set the height of the root view to 0
                    binding.root.layoutParams = ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        0
                    )

            }else{
                binding.root.layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                binding.root.visibility = View.VISIBLE
                setMessage(item)
            }


            val topChild = binding.layChatItem.getChildAt(0)
            if (topChild.tag != null) {
                binding.layChatItem.removeView(topChild)
            }
            separatorView?.let {
                it.tag = "separator-${item?.id}"
                it.visibility = View.VISIBLE
                binding.layChatItem.addView(it, 0)
            }
            updateBackground()

            if (item?.timetoken != 0L) {
                binding.root.postDelayed(
                    {
                        binding.root.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
                    },
                    100
                )
            }
            if (currentChatReactionPopUpViewPos > -1 && currentChatReactionPopUpViewPos == adapterPosition) {
                if (chatPopUpView?.isShowing == true) {
                    chatPopUpView?.updatePopView(
                        message?.profileReactionListForEmojiMap?.toMutableMap()
                    )
                }
            }

            item?.let { chatMessage ->
                chatMessage.id?.let {
                    analyticsService.trackMessageDisplayed(it, chatMessage.message)
                }
            }
        }

        private fun showFloatingUI(
            isOwnMessage: Boolean,
            checkItemIsAtTop: Boolean,
            reactionsAvailable: Boolean
        ) {
            currentChatReactionPopUpViewPos = adapterPosition
            updateBackground()
            if (chatPopUpView?.isShowing == true)
                chatPopUpView?.dismiss()

            var y = chatViewThemeAttribute.chatReactionY
            if (chatViewThemeAttribute.chatReactionPanelGravity == Gravity.TOP ||
                chatViewThemeAttribute.chatReactionPanelGravity == Gravity.TOP or Gravity.RIGHT ||
                chatViewThemeAttribute.chatReactionPanelGravity == Gravity.TOP or Gravity.LEFT ||
                chatViewThemeAttribute.chatReactionPanelGravity == Gravity.TOP or Gravity.CENTER ||
                chatViewThemeAttribute.chatReactionPanelGravity == Gravity.TOP or Gravity.START ||
                chatViewThemeAttribute.chatReactionPanelGravity == Gravity.TOP or Gravity.END
            ) {
                y -= binding.root.height
                if (checkItemIsAtTop) {
                    y += binding.root.height
                }
            }

            chatPopUpView = ChatActionsPopupView(
                binding.root.context,
                reactionList,
                { _ ->
                    analyticsService.trackFlagButtonPressed()
                    hideFloatingUI()
                    binding.root.context?.let { ctx ->
                        AlertDialog.Builder(ctx).apply {
                            setTitle(context.getString(R.string.flag_ui_title))
                            setItems(dialogOptions.map { it.first }.toTypedArray()) { _, which ->
                                message?.let {
                                    when (dialogOptions[which].first) {
                                        binding.root.context.getString(R.string.flag_ui_blocking_title) -> {
                                            analyticsService.trackBlockingUser()
                                            it.senderId?.let { it1 -> blockProfile(it1) }
                                            reporter(it)
                                        }

                                        binding.root.context.getString(R.string.flag_ui_reporting_title) -> {
                                            analyticsService.trackReportingMessage()
                                            reporter(it)
                                        }
                                    }
                                    dialogOptions[which].second(it)
                                }
                            }
                            setOnCancelListener { analyticsService.trackCancelFlagUi() }
                            create()
                        }.show()
                    }
                },
                ::hideFloatingUI,
                isOwnMessage,
                emojiCountMap = message?.profileReactionListForEmojiMap?.toMutableMap(),
                chatViewThemeAttributes = chatViewThemeAttribute,
                selectReactionListener = object : SelectReactionListener {
                    override fun onSelectReaction(reaction: Reaction, isRemoved: Boolean) {
                        if (currentChatReactionPopUpViewPos > -1 && currentChatReactionPopUpViewPos < itemCount) {
                            getItem(currentChatReactionPopUpViewPos)?.apply {
                                val reactionId: String?
                                val myChatMessageReactions =
                                    ArrayList(
                                        profileReactionListForEmojiMap[reaction.id] ?: emptyList()
                                    )
                                if (isRemoved) {
                                    val myReaction =
                                        myChatMessageReactions.find { it.emojiId == reaction.id && it.userId == currentUserId }
                                    reactionId = myReaction?.emojiId
                                    myReaction?.let {
                                        id?.let {
                                            removeMessageReaction(it, reaction.id)
                                        }
                                    }
                                } else {
                                    reactionId = reaction.id
                                    val chatMessageReaction = ChatMessageReaction(
                                        reaction.id,
                                        userReactionId = null,
                                        userId = currentUserId
                                    )
                                    myChatMessageReactions.add(chatMessageReaction)
                                    val reactionMap = profileReactionListForEmojiMap.toMutableMap()
                                    val userReactions =
                                        ArrayList(reactionMap[reaction.id] ?: emptyList())
                                    if (userReactions.any { it.emojiId == reactionId && it.userId == currentUserId }
                                            .not()) {
                                        userReactions.add(chatMessageReaction)
                                    }
                                    reactionMap[reaction.id] = userReactions
                                    profileReactionListForEmojiMap = reactionMap
                                    id?.let {
                                        sendMessageReaction(it, reaction.id)
                                    }
                                }
                                id?.let {
                                    reactionId?.let {
                                        chatRoomId?.let {
                                            analyticsService.trackChatReactionSelected(
                                                it,
                                                id!!,
                                                reactionId,
                                                isRemoved
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                deleteClick = {
                    if (isOwnMessage && enableDeleteMessage) {
                        hideFloatingUI()
                        if (deleteDialogInterceptor != null) {
                            deleteDialogInterceptor!!.dialogEvent.value=false
                            uiScope.launch {
                                deleteDialogInterceptor!!.dialogEvent.collect { decision ->
                                    when (decision) {
                                        true -> {
                                            message?.id?.let {
                                                deleteOwnMessage(it)
                                            } ?: logError { "Message id is Null" }
                                        }

                                        else -> {
                                            logError { "No Decision!!" }
                                        }
                                    }
                                }
                            }
                            deleteDialogInterceptor!!.dialogToShow(message!!)
                        } else {
                            message?.id?.let {
                                deleteOwnMessage(it)
                            } ?: logError { "Message id is Null" }
                        }
                    } else {
                        logError { "You are not allowed to delete other user's messages" }
                    }
                },
                reactionsAvailable,
                enableDeleteMessage,
                currentUserId
            ).apply {
                animationStyle = when {
                    checkItemIsAtTop -> R.style.ChatReactionAnimationReverse
                    else -> R.style.ChatReactionAnimation
                }
                // I had to specify the width and height in order to be shown on that version (which is still compatible with the rest of the versions as well).
                width = ViewGroup.LayoutParams.WRAP_CONTENT
                height = ViewGroup.LayoutParams.WRAP_CONTENT

                showAsDropDown(
                    binding.root,
                    chatViewThemeAttribute.chatReactionX,
                    y,
                    chatViewThemeAttribute.chatReactionPanelGravity
                )
                message?.id?.let {
                    analyticsService.trackChatReactionPanelOpen(it)
                }
            }
        }

        private fun updateBackground() {
            //Need to check before functionality make it in more proper way
            binding.root.apply {
                if (currentChatReactionPopUpViewPos > -1 && adapterPosition > -1 && currentChatReactionPopUpViewPos == adapterPosition) {
                    chatViewThemeAttribute.apply {
                        updateUI(
                            binding.chatBubbleBackground,
                            chatReactionMessageBubbleHighlightedBackground
                        )
                        chatReactionMessageBackHighlightedBackground?.let { res ->
                            updateUI(binding.chatBackground, res)
                        }
                        updateUI(
                            binding.layQuoteMessage,
                            quoteChatReactionMessageBackHighlightedBackground
                        )
                    }
                } else {
                    chatViewThemeAttribute.apply {
                        updateUI(binding.chatBubbleBackground, chatBubbleBackgroundRes)
                        chatBackgroundRes?.let { res ->
                            updateUI(binding.chatBackground, res)
                        }
                        updateUI(binding.layQuoteMessage, quoteChatBackgroundRes)
                    }
                }
            }
        }

        private fun updateUI(view: View, res: Int) {
            if (res < 0) {
                view.setBackgroundColor(res)
            } else {
                val value = TypedValue()
                try {
                    binding.root.context.resources.getValue(res, value, true)
                    when (value.type) {
                        TypedValue.TYPE_REFERENCE, TypedValue.TYPE_STRING -> view.setBackgroundResource(
                            res
                        )

                        TypedValue.TYPE_NULL -> view.setBackgroundColor(
                            Color.TRANSPARENT
                        )

                        else -> view.setBackgroundColor(Color.TRANSPARENT)
                    }
                } catch (_: Resources.NotFoundException) {
                    view.setBackgroundColor(res)
                }
            }
        }

        internal fun hideFloatingUI() {
            if (chatPopUpView?.isShowing == true)
                chatPopUpView?.dismiss()
            chatPopUpView = null
            lastFloatingUiAnchorView = null
            if (mRecyclerView?.isComputingLayout == false) {
                // Add check for checking computing and check with current adapter position
                if (currentChatReactionPopUpViewPos > -1 && currentChatReactionPopUpViewPos == adapterPosition) {
                    try {
                        notifyItemChanged(currentChatReactionPopUpViewPos)
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                        logError { e.message }
                    }
                }
                currentChatReactionPopUpViewPos = -1
                updateBackground()
            }
        }

        // HH:MM:SS eg 02:45:12
        private fun Long.toTimeString(): String =
            when (this) {
                0L -> ""
                else -> SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(
                    Date().apply {
                        time = this@toTimeString
                    }
                )
            }

        private fun setCustomFontWithTextStyle(
            textView: TextView,
            fontPath: String?,
            textStyle: Int
        ) {
            if (fontPath != null) {
                try {
                    val typeFace =
                        Typeface.createFromAsset(
                            textView.context.assets,
                            fontPath
                        )
                    textView.setTypeface(typeFace, textStyle)
                } catch (e: Exception) {
                    e.printStackTrace()
                    textView.setTypeface(null, textStyle)
                }
            } else {
                textView.setTypeface(null, textStyle)
            }
        }

        private fun setLetterSpacingForTextView(textView: TextView, letterSpacing: Float) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                textView.letterSpacing = letterSpacing
            }
        }

        @SuppressLint("SetTextI18n")
        private fun setMessage(
            message: LiveLikeChatMessage?
        ) {
            binding.apply {
                this@ViewHolder.message = message
                message?.apply {

                    // Cleanup the recycled item
                    chatMessage.text = ""
                    Glide.with(root.context.applicationContext).clear(chatMessage)

                    chatViewThemeAttribute.apply {
                        chatMessage.setTextColor(chatMessageColor)
                        quoteChatMessage.setTextColor(quoteChatMessageColor)
                        if (message.isFromMe) {
                            chatNickname.setTextColor(chatNickNameColor)
                            chatNickname.text =
                                message.nickname
                        } else {
                            chatNickname.setTextColor(chatOtherNickNameColor)
                            chatNickname.text = message.nickname
                        }
                        quoteChatMessageNickname.setTextColor(quoteChatNickNameColor)
                        chatNickname.setTextSize(TypedValue.COMPLEX_UNIT_PX, chatUserNameTextSize)
                        quoteChatMessageNickname.setTextSize(
                            TypedValue.COMPLEX_UNIT_PX,
                            quoteChatUserNameTextSize
                        )
                        chatNickname.isAllCaps = chatUserNameTextAllCaps
                        setCustomFontWithTextStyle(
                            chatNickname,
                            chatUserNameCustomFontPath,
                            chatUserNameTextStyle
                        )
                        setCustomFontWithTextStyle(
                            quoteChatMessageNickname,
                            quoteChatUserNameCustomFontPath,
                            quoteChatUserNameTextStyle
                        )
                        setLetterSpacingForTextView(chatNickname, chatUserNameTextLetterSpacing)
                        setLetterSpacingForTextView(
                            quoteChatMessageNickname,
                            quoteChatUserNameTextLetterSpacing
                        )
                        chatMessage.setTextSize(TypedValue.COMPLEX_UNIT_PX, chatMessageTextSize)
                        quoteChatMessage.setTextSize(
                            TypedValue.COMPLEX_UNIT_PX,
                            quoteChatMessageTextSize
                        )
                        if (showLinks) {
                            chatMessage.apply {
                                linksClickable = showLinks
                                setLinkTextColor(chatMessageLinkTextColor)
                                movementMethod = LinkMovementMethod.getInstance()
                            }
                            quoteChatMessage.apply {
                                linksClickable = showLinks
                                setLinkTextColor(quoteChatMessageLinkTextColor)
                                movementMethod = LinkMovementMethod.getInstance()
                            }
                        }
                        setCustomFontWithTextStyle(
                            chatMessage,
                            chatMessageCustomFontPath,
                            when (isDeleted) {
                                true -> Typeface.ITALIC
                                else -> chatMessageTextStyle
                            }
                        )
                        setCustomFontWithTextStyle(
                            quoteChatMessage,
                            quoteChatMessageCustomFontPath,
                            when (quoteMessage?.isDeleted) {
                                true -> Typeface.ITALIC
                                else -> quoteChatMessageTextStyle
                            }
                        )
                        setLetterSpacingForTextView(chatMessage, chatMessageTextLetterSpacing)
                        setLetterSpacingForTextView(
                            quoteChatMessage,
                            quoteChatMessageTextLetterSpacing
                        )
                        if (chatViewThemeAttribute.showMessageDateTime) {
                            messageDateTime.visibility = View.VISIBLE
                            if (EngagementSDK.enableDebug) {
                                val pdt = message.timeStamp?.toLong() ?: 0
                                val createdAt = message.getUnixTimeStamp()?.toTimeString() ?: ""
                                val syncedTime = pdt.toTimeString()

                                messageDateTime.text =
                                    "Created :  $createdAt | Synced : $syncedTime "
                            } else {
                                messageDateTime.text = messageTimeFormatter?.invoke(
                                    message.getUnixTimeStamp()
                                )
                            }
                            messageDateTime.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX,
                                chatMessageTimeTextSize
                            )
                            setCustomFontWithTextStyle(
                                messageDateTime,
                                chatMessageTimeCustomFontPath,
                                chatMessageTimeTextStyle
                            )
                            messageDateTime.isAllCaps = chatMessageTimeTextAllCaps
                            messageDateTime.setTextColor(chatMessageTimeTextColor)
                            setLetterSpacingForTextView(
                                messageDateTime,
                                chatMessageTimeTextLetterSpacing
                            )
                        } else {
                            messageDateTime.visibility = View.GONE
                        }

                        val topBorderLP = borderTop.layoutParams
                        topBorderLP.height = chatMessageTopBorderHeight
                        borderTop.layoutParams = topBorderLP

                        val bottomBorderLP = borderBottom.layoutParams
                        bottomBorderLP.height = chatMessageBottomBorderHeight
                        borderBottom.layoutParams = bottomBorderLP

                        borderTop.setBackgroundColor(chatMessageTopBorderColor)
                        borderBottom.setBackgroundColor(chatMessageBottomBorderColor)

                        val layoutParam =
                            chatBackground.layoutParams as ConstraintLayout.LayoutParams
                        layoutParam.setMargins(
                            chatMarginLeft,
                            chatMarginTop + AndroidResource.dpToPx(6),
                            chatMarginRight,
                            chatMarginBottom
                        )
                        layoutParam.width = chatBackgroundWidth
                        chatBackground.layoutParams = layoutParam
                        chatBubbleBackground.setPadding(
                            chatBubblePaddingLeft,
                            chatBubblePaddingTop,
                            chatBubblePaddingRight,
                            chatBubblePaddingBottom
                        )
                        layQuoteMessage.setPadding(
                            quoteChatBubblePaddingLeft,
                            quoteChatBubblePaddingTop,
                            quoteChatBubblePaddingRight,
                            quoteChatBubblePaddingBottom
                        )
                        val layoutParam1: LinearLayout.LayoutParams =
                            chatBubbleBackground.layoutParams as LinearLayout.LayoutParams
                        layoutParam1.setMargins(
                            chatBubbleMarginLeft,
                            chatBubbleMarginTop,
                            chatBubbleMarginRight,
                            chatBubbleMarginBottom
                        )
                        layoutParam1.width = chatBubbleWidth
                        chatBubbleBackground.layoutParams = layoutParam1
                        imgChatAvatar.visibility =
                            when (this@ChatRecyclerAdapter.showChatAvatarLogo) {
                                true -> View.VISIBLE
                                else -> View.GONE
                            }
                        val layoutParamAvatar = LinearLayout.LayoutParams(
                            chatAvatarWidth,
                            chatAvatarHeight
                        )
                        layoutParamAvatar.setMargins(
                            chatAvatarMarginLeft,
                            chatAvatarMarginTop,
                            chatAvatarMarginRight,
                            chatAvatarMarginBottom
                        )
                        layoutParamAvatar.gravity = chatAvatarGravity
                        imgChatAvatar.layoutParams = layoutParamAvatar

                        var options = RequestOptions()
                        if (chatAvatarCircle) {
                            options = options.optionalCircleCrop()
                        }
                        if (chatAvatarRadius > 0) {
                            options = options.transform(
                                CenterCrop(),
                                RoundedCorners(chatAvatarRadius)
                            )
                        }

                        // load local image with glide, so that (chatAvatarCircle and chatAvatarRadius) properties can be applied.
                        // more details on https://livelike.atlassian.net/browse/ES-1790
                        // replace context with applicationContext related to ES-2185
                        if (message.profilePic.isNullOrEmpty()) {
                            // load local image
                            Glide.with(root.context.applicationContext)
                                .load(R.drawable.default_avatar)
                                .apply(options)
                                .placeholder(chatUserPicDrawable)
                                .into(imgChatAvatar)
                        } else {
                            Glide.with(root.context.applicationContext).load(message.profilePic)
                                .apply(options)
                                .placeholder(chatUserPicDrawable)
                                .error(chatUserPicDrawable)
                                .into(imgChatAvatar)
                        }

                        setTextOrImageToView(
                            message,
                            chatMessage,
                            imgChatMessage,
                            false,
                            chatMessageTextSize,
                            getSticker,
                            showLinks,
                            root.context.resources.displayMetrics.density,
                            linksRegex,
                            chatRoomId,
                            chatRoomName,
                            analyticsService,
                            false,
                            currentUserId
                        )

                        var imageView: ImageView
                        relReactionsLay.removeAllViews()

                        val chatReactionParam =
                            relReactionsLay.layoutParams as ConstraintLayout.LayoutParams
                        chatReactionParam.leftMargin = chatReactionIconsMarginLeft
                        chatReactionParam.rightMargin = chatReactionIconsMarginRight
                        chatReactionParam.topMargin = chatReactionIconsMarginTop
                        chatReactionParam.bottomMargin = chatReactionIconsMarginBottom
                        chatReactionParam.height = chatReactionDisplaySize
                        if (chatReactionIconsPositionAtBottom) {
                            chatReactionParam.topToTop = ConstraintLayout.LayoutParams.UNSET
                            chatReactionParam.bottomToBottom = chatConstraintBox.id
                        }
                        relReactionsLay.layoutParams = chatReactionParam

                        val chatReactionCountParam =
                            txtChatReactionsCount.layoutParams as ConstraintLayout.LayoutParams
                        chatReactionCountParam.leftMargin = chatReactionCountMarginLeft
                        chatReactionCountParam.rightMargin = chatReactionCountMarginRight
                        chatReactionCountParam.topMargin = chatReactionCountMarginTop
                        chatReactionCountParam.bottomMargin = chatReactionCountMarginBottom
                        if (chatReactionCountPositionAtBottom) {
                            chatReactionCountParam.topToTop = ConstraintLayout.LayoutParams.UNSET
                            chatReactionCountParam.bottomToBottom = chatConstraintBox.id
                        }
                        txtChatReactionsCount.layoutParams = chatReactionCountParam

                        setCustomFontWithTextStyle(
                            txtChatReactionsCount,
                            chatReactionDisplayCountCustomFontPath,
                            chatReactionDisplayCountTextStyle
                        )
                        txtChatReactionsCount.setTextSize(
                            TypedValue.COMPLEX_UNIT_PX,
                            chatReactionDisplayCountTextSize
                        )
                        // TODO need to check for updating list and work on remove the reaction with animation
                        profileReactionListForEmojiMap.keys.filter {
                            return@filter (profileReactionListForEmojiMap[it]?.size ?: 0) > 0
                        }
                            .forEachIndexed { index, reactionId ->
                                val reactions =
                                    profileReactionListForEmojiMap[reactionId] ?: emptyList()
                                if (reactions.isNotEmpty()) {
                                    imageView = ImageView(root.context)
                                    val reaction = reactionList.firstOrNull { it.id == reactionId }
                                    reaction?.let {
                                        imageView.contentDescription = reaction.name
                                        imageView.loadImage(reaction.file, chatReactionDisplaySize)
                                        val paramsImage: FrameLayout.LayoutParams =
                                            FrameLayout.LayoutParams(
                                                chatReactionDisplaySize,
                                                chatReactionDisplaySize
                                            )
                                        paramsImage.gravity = Gravity.LEFT
                                        val left =
                                            ((chatReactionDisplaySize / chatReactionIconsFactor) * (index)).toInt()
                                        paramsImage.setMargins(left, 0, 0, 0)
                                        relReactionsLay.addView(imageView, paramsImage)
                                        imageView.bringToFront()
                                        imageView.invalidate()
                                        if (reactions.any { it.emojiId == reaction.id }) {
                                            imageView.startAnimation(bounceAnimation)
                                        }
                                    }
                                    /*This check has been added, so that if we reacted with any emoji and that emoji
                                    * has been deleted from reaction packs from cms, then wrong count use to come*/
                                    //Avoiding this removal as this remove from global object,instead for count we are filtering the emojimap and find count from filters
//                                    if (reaction == null) {
//                                        userReactionListForEmojiMap.remove(reactionId)
//                                    }
                                }
                            }

                        txtChatReactionsCount.setTextColor(chatReactionDisplayCountColor)
                        val sumCount =
                            profileReactionListForEmojiMap.filter { rec -> reactionList.firstOrNull { it.id == rec.key } != null }.values.sumOf { it.size }
                        val isReactionsAvailable = reactionList.isNotEmpty()
                        if (chatViewThemeAttribute.chatReactionHintEnable && sumCount == 0) {
                            val innerImageView = ImageView(root.context)
                            innerImageView.contentDescription =
                                root.context.getString(R.string.you_can_add_reaction_hint)
                            innerImageView.setImageResource(chatViewThemeAttribute.chatReactionHintIcon)
                            val params: FrameLayout.LayoutParams =
                                FrameLayout.LayoutParams(
                                    chatReactionDisplaySize,
                                    chatReactionDisplaySize
                                )
                            relReactionsLay.addView(innerImageView, params)
                        }

                        if (profileReactionListForEmojiMap.isNotEmpty() && sumCount > 0 && isReactionsAvailable) {
                            txtChatReactionsCount.visibility = View.VISIBLE
                            txtChatReactionsCount.text =
                                reactionCountFormatter?.invoke(sumCount) ?: formatReactionCount(
                                    sumCount
                                )
                        } else {
                            txtChatReactionsCount.visibility = View.INVISIBLE
                            txtChatReactionsCount.text = "  "
                        }
                    }
                    layQuoteMessage.visibility =
                        when (quoteMessage != null && enableQuoteMessage && !isDeleted) {
                            true -> View.VISIBLE
                            else -> View.GONE
                        }
                    quoteMessage?.let {
                        setTextOrImageToView(
                            it,
                            quoteChatMessage,
                            imgQuoteChatMessage,
                            true,
                            chatViewThemeAttribute.quoteChatMessageTextSize,
                            getSticker,
                            showLinks,
                            root.context.resources.displayMetrics.density,
                            linksRegex,
                            chatRoomId,
                            chatRoomName,
                            analyticsService,
                            false,
                            currentUserId
                        )
                        quoteChatMessageNickname.text = it.nickname
                    }
                }
            }
        }
    }
}

class InternalURLSpan(
    private var clickedSpan: String,
    private val messageId: String?,
    private val chatRoomId: String?,
    private val chatRoomName: String?,
    private val analyticsService: AnalyticsService?
) : ClickableSpan() {
    override fun onClick(textView: View) {
        val i = Intent(Intent.ACTION_VIEW)
        if (!clickedSpan.startsWith("http://") && !clickedSpan.startsWith("https://"))
            clickedSpan = "http://$clickedSpan"
        i.data = Uri.parse(clickedSpan)
        try {
            textView.context.startActivity(i)
        } catch (e: ActivityNotFoundException) {
            logError { e.message }
        }
        chatRoomId?.let {
            analyticsService?.trackMessageLinkClicked(
                chatRoomId,
                chatRoomName,
                messageId,
                clickedSpan
            )
        }
    }
}

internal fun formatReactionCount(count: Int): String {
    if (count < 1000) return "" + count
    val exp = (ln(count.toDouble()) / ln(1000.0)).toInt()
    return String.format(
        Locale.getDefault(),
        "%.1f%c",
        count / 1000.0.pow(exp.toDouble()),
        "kMGTPE"[exp - 1],
    )
}