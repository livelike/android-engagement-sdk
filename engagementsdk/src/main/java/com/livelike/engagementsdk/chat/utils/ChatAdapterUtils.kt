package com.livelike.engagementsdk.chat.utils

import android.os.Build
import android.text.*
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.R
import com.livelike.engagementsdk.chat.CHAT_MESSAGE_IMAGE_TEMPLATE
import com.livelike.engagementsdk.chat.InternalURLSpan
import com.livelike.engagementsdk.chat.stickerKeyboard.*
import com.livelike.engagementsdk.core.utils.AndroidResource
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage
import pl.droidsonroids.gif.MultiCallback
import java.util.regex.Matcher
import java.util.regex.Pattern


// const val should be in uppercase always
private const val LARGER_STICKER_SIZE = 100
private const val MEDIUM_STICKER_SIZE = 50
private const val SMALL_STICKER_SIZE = 28

internal fun setTextOrImageToView(
    chatMessage: LiveLikeChatMessage?,
    textView: TextView,
    imageView: ImageView,
    isParent: Boolean = false,
    textSize: Float,
    getSticker: (String) -> Sticker?,
    showLinks: Boolean,
    density: Float,
    linksRegex: Regex,
    chatRoomId: String?,
    chatRoomName: String?,
    analyticsService: AnalyticsService?,
    input: Boolean,
    userId: String?
) {
    val callback = MultiCallback(true)
    chatMessage?.apply {
        val tag = when (isParent) {
            true -> "parent_$id"
            else -> id
        }

        if(chatMessage.imageUrl!=null) {
            replaceImageMessageContentWithImageUrl(chatMessage)
        }
        val convertedMessage = convertedMessage(userId)

        textView.tag = tag
        val spaceRemover = Pattern.compile("[\\s]")
        val inputNoString =
            spaceRemover.matcher(convertedMessage).replaceAll(Matcher.quoteReplacement(""))
        val isOnlyStickers =
            inputNoString.findIsOnlyStickers().matches() || convertedMessage.findImages().matches()
        val atLeastOneSticker =
            inputNoString.findStickers().find() || convertedMessage.findImages().matches()
        val numberOfStickers = convertedMessage.findStickers().countMatches()
        val isExternalImage = convertedMessage.findImages().matches()

        val linkText = getTextWithCustomLinks(
            linksRegex,
            SpannableString(convertedMessage),
            chatMessage.id,
            chatRoomId,
            chatRoomName,
            analyticsService
        )
        textView.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        callback.addView(textView)
        textView.contentDescription = if (isExternalImage) {
            textView.context.getString(R.string.image)
        } else {
            convertedMessage
        }
        when {
            !isDeleted && isExternalImage && !isBlocked -> {
                imageView.contentDescription = if (isExternalImage) {
                    textView.context.getString(R.string.image)
                } else {
                    convertedMessage
                }
                textView.minHeight = 0
                textView.text = ""
                textView.visibility = View.GONE
                imageView.visibility = View.VISIBLE
                val size = when (isParent) {
                    true -> MEDIUM_STICKER_SIZE
                    else -> LARGER_STICKER_SIZE
                }
                val factor: Double = when (isParent) {
                    true -> when (input) {
                        true -> 2.2
                        else -> 1.75
                    }
                    else -> when ((imageHeight ?: 0) > size) {
                        true -> when ((imageHeight ?: 0) > 1000) {
                            true -> 3.0
                            else -> 1.0
                        }
                        else -> 1.0
                    }
                }
                imageView.minimumHeight = ((imageHeight ?: size) / factor).toInt()
                Glide.with(imageView.context)
                    .load(imageUrl ?: message?.substring(1, (message?.length ?: 0) - 1))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .apply(
                        RequestOptions().override(
                            ((imageWidth ?: size) / factor).toInt(),
                            ((imageHeight ?: size) / factor).toInt()
                        )
                    )
                    .into(imageView)
            }
            !isDeleted && (isOnlyStickers && numberOfStickers < 2) && !isBlocked -> {
                textView.visibility = View.VISIBLE
                imageView.visibility = View.GONE
                textView.minHeight = AndroidResource.dpToPx(MEDIUM_STICKER_SIZE)
                val s = when (showLinks) {
                    true -> linkText
                    else -> SpannableString(convertedMessage)
                }
                replaceWithStickers(
                    s,
                    textView.context.applicationContext,
                    getSticker,
                    null,
                    callback,
                    MEDIUM_STICKER_SIZE
                ) {
                    // TODO this might write to the wrong messageView on slow connection.
                    if (textView.tag == tag) {
                        textView.text = s
                    }
                }
            }
            !isDeleted && atLeastOneSticker && !isBlocked -> {
                textView.visibility = View.VISIBLE
                imageView.visibility = View.GONE
                var columnCount = numberOfStickers / 8
                val lines = convertedMessage.withoutStickers().getLinesCount(density, textSize)
                if (columnCount == 0) {
                    columnCount = 1
                }
                textView.minHeight =
                    (textSize.toInt() * columnCount) + when {
                        lines != columnCount -> (lines * textSize.toInt())
                        else -> 0
                    }
                val s = when (showLinks) {
                    true -> linkText
                    else -> SpannableString(convertedMessage)
                }
                replaceWithStickers(
                    s,
                    textView.context.applicationContext,
                    getSticker,
                    null,
                    callback,
                    SMALL_STICKER_SIZE
                ) {
                    // TODO this might write to the wrong messageView on slow connection.
                    if (textView.tag == tag) {
                        textView.text = s
                    }
                }
            }
            else -> {
                imageView.visibility = View.GONE
                textView.visibility = View.VISIBLE
                id?.let {
                    clearTarget(it, textView.context)
                }
                textView.minHeight = textSize.toInt()
                textView.text = when (isParent && isBlocked) {
                    true -> textView.context.getString(R.string.livelike_quote_blocked_message)
                    else -> when (showLinks) {
                        true -> linkText
                        else -> convertedMessage
                    }
                }
            }
        }
    }
}

private fun replaceImageMessageContentWithImageUrl(message: LiveLikeChatMessage) {
    val imageUrl = message.imageUrl
    if (!imageUrl.isNullOrEmpty()) {
        message.message = CHAT_MESSAGE_IMAGE_TEMPLATE.replace("message", imageUrl)
    }
}

private fun getTextWithCustomLinks(
    linksRegex: Regex,
    spannableString: SpannableString,
    messageId: String?,
    chatRoomId: String?,
    chatRoomName: String?,
    analyticsService: AnalyticsService?
): SpannableString {
    val result = linksRegex.toPattern().matcher(spannableString)
    while (result.find()) {
        val start = result.start()
        val end = result.end()
        spannableString.setSpan(
            InternalURLSpan(
                spannableString.subSequence(start, end).toString(),
                messageId,
                chatRoomId,
                chatRoomName,
                analyticsService
            ),
            start,
            end,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
    return spannableString
}

private val encodeMap = hashMapOf(
    "&amp;" to "&",
    "&lt;" to "<",
    "&gt;" to ">",
    "&quot;" to "\"",
    "&#47;" to "/"
)

internal fun String.decodeTextMessage(): String {
    return encodeMap.entries.fold(this) { acc, (key, value) -> acc.replace(key, value) }
}

/**
 * Creating this function to get line count of string assuming the width as some value
 * it is estimated not exact value
 */
private fun String.getLinesCount(density: Float, textSize: Float): Int {
    val paint = TextPaint()
    paint.textSize = textSize * density
    // Using static width for now ,can be replace with dynamic for later
    val width = (AndroidResource.dpToPx(300) * density).toInt()
    val alignment: Layout.Alignment = Layout.Alignment.ALIGN_NORMAL
    val layout = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        StaticLayout.Builder.obtain(this, 0, this.length, paint, width)
            .setAlignment(alignment)
            .setLineSpacing(0F, 1F)
            .setIncludePad(false)
            .build()
    } else {
        @Suppress("DEPRECATION") //suppressed as needed to support pre M
        (StaticLayout(this, paint, width, alignment, 1F, 0F, false))
    }
    return layout.lineCount
}

private fun String.withoutStickers(): String {
    var result = this
    for (it in this.findStickerCodes().allMatches()) {
        result = result.replace(it, "")
    }
    return result
}