package com.livelike.engagementsdk.chat.chatreaction

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.accessibility.AccessibilityEvent
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.livelike.engagementsdk.R
import com.livelike.engagementsdk.chat.ChatMessageReaction
import com.livelike.engagementsdk.chat.ChatViewThemeAttributes
import com.livelike.engagementsdk.chat.formatReactionCount
import com.livelike.engagementsdk.core.utils.AndroidResource
import com.livelike.engagementsdk.databinding.PopupChatReactionBinding
import com.livelike.engagementsdk.widget.view.loadImage

/**
 * Chat reactions and Chat moderation actions view that will popup when use long press chat
 */
internal class ChatActionsPopupView(
    val context: Context,
    private val reactionsList: List<Reaction>,
    flagClick: (View) -> Unit,
    hideFloatingUi: () -> Unit,
    isOwnMessage: Boolean,
    var emojiCountMap: MutableMap<String, List<ChatMessageReaction>>? = null,
    private val chatViewThemeAttributes: ChatViewThemeAttributes,
    val selectReactionListener: SelectReactionListener? = null,
    deleteClick: (View) -> Unit,
    reactionsAvailable: Boolean,
    enableDeleteMessage: Boolean,
    private val currentUserId: String
) : PopupWindow(context) {
    private var binding: PopupChatReactionBinding

    init {
        chatViewThemeAttributes.apply {
            binding = PopupChatReactionBinding.inflate(LayoutInflater.from(context))
            contentView = binding.root
            binding.chatReactionBackgroundCard.apply {
                setCardBackgroundColor(chatReactionPanelColor)
                cardElevation = chatReactionElevation
                radius = chatReactionRadius
                setContentPadding(
                    chatReactionPadding,
                    chatReactionPadding,
                    chatReactionPadding,
                    chatReactionPadding
                )
            }

            if (chatReactionModerationFlagVisible) {
                if (!isOwnMessage) {
                    binding.moderationFlagLay.apply {
                        visibility = View.VISIBLE
                        setOnClickListener {
                            dismiss()
                            flagClick(it)
                        }
                        radius = chatReactionRadius
                        setCardBackgroundColor(chatReactionPanelColor)
                    }
                    binding.moderationFlag.setColorFilter(
                        chatReactionFlagTintColor
                    )
                } else {
                    binding.moderationFlagLay.visibility = View.GONE
                }
            } else {
                binding.moderationFlagLay.visibility = View.GONE
            }
            if (isOwnMessage && enableDeleteMessage) {
                binding.moderationDeleteLay.apply {
                    visibility = View.VISIBLE
                    setOnClickListener {
                        dismiss()
                        deleteClick(it)
                    }
                    radius = chatReactionRadius
                    setCardBackgroundColor(chatReactionPanelColor)
                }
                binding.moderationDelete.setColorFilter(chatReactionFlagTintColor)
            } else {
                binding.moderationDeleteLay.visibility = View.GONE
            }

            setOnDismissListener(hideFloatingUi)
            isOutsideTouchable = false
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        if (reactionsAvailable) {
            binding.chatReactionBackgroundCard.visibility = View.GONE
            initReactions()
        } else {
            binding.chatReactionBackgroundCard.visibility = View.GONE
        }
    }

    private fun formattedReactionCount(count: Int): String {
        var c = count
        if (c < 0) {
            c = 0
        }
        return formatReactionCount(c)
    }

    fun updatePopView(
        emojiCountMap: MutableMap<String, List<ChatMessageReaction>>? = null,
    ) {
        this.emojiCountMap = emojiCountMap
        initReactions()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initReactions() {
        val reactionsBox = binding.reactionPanelInteractionBox
        reactionsBox.removeAllViews()
        for (reaction in reactionsList) {
            val cardView = CardView(context)
            cardView.cardElevation = 0f
            cardView.setContentPadding(5, 5, 8, 5)
            cardView.setCardBackgroundColor(Color.TRANSPARENT)
            val relativeLayout = RelativeLayout(context)
            val countView = TextView(context)
            val imageView = ImageView(context)
            imageView.id = View.generateViewId()
            imageView.isFocusable = true
            imageView.contentDescription = reaction.name
            imageView.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
            imageView.loadImage(
                reaction.file,
                context.resources.getDimensionPixelSize(R.dimen.livelike_chat_reaction_size)
            )
            val reactions = emojiCountMap?.get(reaction.id) ?: emptyList()
            for (it in reactions) {
                if (it.emojiId == reaction.id && it.userId == currentUserId) {
                    imageView.tag = it.emojiId
                    cardView.radius = chatViewThemeAttributes.chatSelectedReactionRadius
                    cardView.setCardBackgroundColor(
                        ContextCompat.getColor(
                            context, R.color.livelike_chat_reaction_selected_background_color
                        )
                    )
                    break
                }
            }
            // On Touch we are scaling and descaling the reaction imageview to show bounce feature
            imageView.setOnTouchListener { v, event ->
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        v.animate().scaleX(1.2f).scaleY(1.2f).setDuration(50).start()
                        return@setOnTouchListener true
                    }

                    MotionEvent.ACTION_UP -> {
                        v.animate().scaleX(1.0f).scaleY(1.0f).setDuration(50).start()
                        selectReactionListener?.let { listener ->
                            val index = reactions.indexOfFirst { v.tag == it.emojiId }
                            if (index > -1) {
                                listener.onSelectReaction(reaction, true) // Remove Same Reaction
                            } else {
                                listener.onSelectReaction(reaction, false) //Add any new reaction
                            }
                            dismiss()
                        }
                        return@setOnTouchListener true
                    }
                }
                return@setOnTouchListener false
            }

            imageView.scaleType = ImageView.ScaleType.CENTER

            val count = emojiCountMap?.get(reaction.id)?.size ?: 0
            countView.apply {
                gravity = Gravity.RIGHT
                text = formattedReactionCount(count)
                setTextColor(chatViewThemeAttributes.chatReactionPanelCountColor)
                if (chatViewThemeAttributes.chatReactionPanelCountCustomFontPath != null) {
                    try {
                        val typeFace = Typeface.createFromAsset(
                            context.assets,
                            chatViewThemeAttributes.chatReactionPanelCountCustomFontPath
                        )
                        setTypeface(typeFace, Typeface.BOLD)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        setTypeface(null, Typeface.BOLD)
                    }
                } else {
                    setTypeface(null, Typeface.BOLD)
                }
                setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    context.resources.getDimension(R.dimen.livelike_chat_reaction_popup_text_size)
                )
                visibility = if (!chatViewThemeAttributes.chatReactionPanelCountVisibleIfZero) {
                    if (count > 0) {
                        View.VISIBLE
                    } else {
                        View.INVISIBLE
                    }
                } else {
                    View.VISIBLE
                }
            }
            relativeLayout.addView(imageView, RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.MATCH_PARENT
            ).apply {
                addRule(RelativeLayout.CENTER_IN_PARENT)
            })
            relativeLayout.addView(countView, RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
            ).apply {
                addRule(RelativeLayout.ALIGN_TOP, imageView.id)
                addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
            })
            cardView.addView(relativeLayout)
            reactionsBox.addView(
                cardView,
                LinearLayout.LayoutParams(AndroidResource.dpToPx(35), AndroidResource.dpToPx(35))
            )
        }
        binding.chatReactionBackgroundCard.visibility =
            if (reactionsList.isNotEmpty()) {
                View.VISIBLE
            } else {
                View.INVISIBLE
            }
        binding.chatReactionBackgroundCard.postDelayed(
            {
                binding.chatReactionBackgroundCard.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
            }, 500
        )
    }
}

internal interface SelectReactionListener {
    fun onSelectReaction(reaction: Reaction, isRemoved: Boolean)
}
