package com.livelike.engagementsdk.widget.timeline

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.DividerItemDecoration.VERTICAL
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.livelike.engagementsdk.ContentSession
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.LiveLikeEngagementTheme
import com.livelike.engagementsdk.core.utils.AndroidResource
import com.livelike.engagementsdk.databinding.LivelikeTimelineViewBinding
import com.livelike.engagementsdk.widget.LiveLikeWidgetViewFactory
import com.livelike.engagementsdk.widget.data.models.WidgetKind
import com.livelike.engagementsdk.widget.data.models.WidgetUserInteractionBase
import com.livelike.engagementsdk.widget.util.SmoothScrollerLinearLayoutManager
import com.livelike.engagementsdk.widget.viewModel.WidgetStates
import com.livelike.utils.Result
import com.livelike.widget.parseDuration
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class WidgetsTimeLineView(
    context: Context,
    private val timeLineViewModel: WidgetTimeLineViewModel,
    sdk: EngagementSDK
) : FrameLayout(context) {

    private var binding: LivelikeTimelineViewBinding
    private var adapter: TimeLineViewAdapter
    private var snapToLiveAnimation: AnimatorSet? = null
    private var showingSnapToLive: Boolean = false
    private var isFirstItemVisible = false
    private var autoScrollTimeline = false
    private var separator: Drawable? = null

    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private val visibleThreshold = 2

    /**
     * For custom widgets to show on this timeline, set implementation of widget view factory
     * @see <a href="https://docs.livelike.com/docs/livelikewidgetviewfactory">Docs reference</a>
     **/
    var widgetViewFactory: LiveLikeWidgetViewFactory? = null
        set(value) {
            adapter.widgetViewFactory = value
            field = value
        }

    /**
     * configuring this controlled will allow to control the timer in widget
     * By default there will be no timer, interaction duration will be kept indefinite
     * to have cms defined interaction timer simple use:
     *  widgetsTimeLineView.widgetTimerController = CMSSpecifiedDurationTimer()
     **/
    var widgetTimerController: WidgetTimerController? = null
        set(value) {
            field = value
            adapter.widgetTimerController = field
        }

    /**
     * this will add custom separator/divider (drawables) between widgets in timeline
     * * @param Drawable
     **/
    fun setSeparator(customSeparator: Drawable?) {
        this.separator = customSeparator
        separator?.let {
            val itemDecoration = DividerItemDecoration(context, VERTICAL)
            itemDecoration.setDrawable(it)
            binding.timelineRv.addItemDecoration(itemDecoration)
        }
    }

    init {

        binding = LivelikeTimelineViewBinding.inflate(LayoutInflater.from(context), this, true)

        // added a check based on data, since this will be causing issue during rotation of device
        if (timeLineViewModel.timeLineWidgets.isEmpty()) {
            showLoadingSpinnerForTimeline()
        }
        adapter =
            TimeLineViewAdapter(
                sdk,
                timeLineViewModel
            )
        adapter.widgetTimerController = widgetTimerController
        adapter.addAll(timeLineViewModel.timeLineWidgets)
        binding.timelineRv.layoutManager = SmoothScrollerLinearLayoutManager(context)
        binding.timelineRv.adapter = adapter
        initListeners()
    }

//    /**
//     * use this function to set timeline view model for the timeline view
//     * make sure to clear this timeline model when scope destroys
//     */
//    fun setTimeLineViewModel(timeLineViewModel: WidgetTimeLineViewModel) {
//        timeLineViewModel.clear()
//        this.timeLineViewModel = timeLineViewModel
//
//    }

    /**
     * will update the value of theme to be applied for all widgets in timeline
     * This will update the theme on the current displayed widget as well
     **/
    fun applyTheme(theme: LiveLikeEngagementTheme) {
        this.adapter.liveLikeEngagementTheme = theme
    }

    /**
     * this method parse livelike theme from json object and apply if its a valid json
     * refer @applyTheme(theme)
     **/
    fun applyTheme(themeJson: JsonObject): Result<Boolean> {
        val themeResult = LiveLikeEngagementTheme.instanceFrom(themeJson)
        return if (themeResult is Result.Success) {
            applyTheme(themeResult.data)
            Result.Success(true)
        } else {
            themeResult as Result.Error
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        subscribeForTimelineWidgets()
    }

    private fun subscribeForTimelineWidgets() {
        timeLineViewModel.uiScope.launch {
            timeLineViewModel.timeLineWidgetsFlow.collect { pair ->
                pair?.let {
                    // lockInteracatedWidgetsWithoutPatchUrl(pair.second) // will remove this logic when backend adds patch_url
                    lockAlreadyInteractedQuizAndEmojiSlider(pair.second)
                    wouldLockPredictionWidgets(pair.second) // if follow up is received lock prediction interaction

                    // changing timeout value for widgets when widgetTimerController is configured
                    widgetTimerController?.run {
                        for (widget in it.second) {
                            if (widget.widgetState == WidgetStates.INTERACTING) {
                                widget.liveLikeWidget.timeout =
                                    this.timeValue(widget.liveLikeWidget)
                                timeLineViewModel.uiScope.launch {
                                    delay(
                                        parseDuration(
                                            pair.second[0].liveLikeWidget.timeout
                                        )
                                    )
                                    pair.second[0].widgetState = WidgetStates.RESULTS
                                    adapter.notifyItemChanged(adapter.list.indexOf(widget))
                                }
                            }
                        }
                    }
                    if (pair.first == WidgetApiSource.REALTIME_API) {
                        adapter.addAll(0, pair.second)
                        wouldRetreatToActiveWidgetPosition()
                    } else {
                        timeLineViewModel.uiScope.launch {
                            adapter.addAll(pair.second)
                            adapter.isLoadingInProgress = false
                        }
                    }
                }
            }
        }
    }

    private fun lockAlreadyInteractedQuizAndEmojiSlider(widgets: List<TimelineWidgetResource>) {
        for (it in widgets) {
            val kind = it.liveLikeWidget.kind
            if (kind == WidgetKind.IMAGE_SLIDER.event || kind?.contains(WidgetKind.QUIZ.event) == true ||
                kind?.contains(WidgetKind.TEXT_ASK.event) == true || kind?.contains(WidgetKind.NUMBER_PREDICTION.event) == true
            ) {
                if ((timeLineViewModel.contentSession as ContentSession).widgetInteractionRepository.getWidgetInteraction<WidgetUserInteractionBase>(
                        it.liveLikeWidget.id ?: ""
                    ) != null
                ) {
                    it.widgetState = WidgetStates.RESULTS
                }
            }
        }
    }

    /**
     * this locks the prediction widgets, when followup is received
     **/
    private fun wouldLockPredictionWidgets(widgets: List<TimelineWidgetResource>) {
        val followUpWidgetPredictionIds = widgets.filter {
            it.liveLikeWidget.kind?.contains("follow-up") ?: false
        }.map {
            it.liveLikeWidget.textPredictionId ?: it.liveLikeWidget.imagePredictionId
            ?: it.liveLikeWidget.textNumberPredictionId ?: it.liveLikeWidget.imageNumberPredictionId
        }

        for (widget in widgets) {
            if (followUpWidgetPredictionIds.contains(widget.liveLikeWidget.id)) {
                widget.widgetState = WidgetStates.RESULTS
            }
        }
        for (widget in adapter.list) {
            if (followUpWidgetPredictionIds.contains(widget.liveLikeWidget.id) && widget.widgetState == WidgetStates.INTERACTING) {
                widget.widgetState = WidgetStates.RESULTS
                adapter.notifyItemChanged(adapter.list.indexOf(widget))
            }
        }
    }


    /**
     *this will check for visible position, if it is 0 then it will scroll to top
     **/
    private fun wouldRetreatToActiveWidgetPosition() {
        val shouldRetreatToTopPosition =
            (binding.timelineRv.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition() == 0
        if (shouldRetreatToTopPosition) {
            binding.timelineRv.smoothScrollToPosition(0)
        }
    }

    /**
     * view click listeners
     * snap to live added
     **/
    private fun initListeners() {
        val lm = binding.timelineRv.layoutManager as LinearLayoutManager
        binding.timelineRv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(
                rv: RecyclerView,
                dx: Int,
                dy: Int
            ) {
                val firstVisible = lm.findFirstVisibleItemPosition()
                val topHasBeenReached = firstVisible == 0
                if (!autoScrollTimeline)
                    isFirstItemVisible = if (topHasBeenReached) {
                        hideSnapToLiveForWidgets()
                        true
                    } else {
                        showSnapToLiveForWidgets()
                        false
                    }
                if (topHasBeenReached) {
                    autoScrollTimeline = false
                }

                /**
                 * load more on scrolled (pagination)
                 **/
                if (!adapter.isLoadingInProgress && !adapter.isEndReached) {
                    val totalItemCount = lm.itemCount
                    val lastVisibleItem = lm.findLastVisibleItemPosition()
                    if (totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        timeLineViewModel.loadMore()
                        adapter.isLoadingInProgress = true
                    }
                }
            }
        })

        binding.timelineSnapLive.setOnClickListener {
            autoScrollTimeline = true
            snapToLiveForTimeline()
        }
        timeLineViewModel.uiScope.launch {
            timeLineViewModel.widgetEventFlow.collect {
                when (it) {

                    WidgetTimeLineViewModel.WIDGET_LOADING_COMPLETE -> {
                        timeLineViewModel.uiScope.launch {
                            hideLoadingSpinnerForTimeline()
                        }
                    }

                    WidgetTimeLineViewModel.WIDGET_TIMELINE_END -> {
                        timeLineViewModel.uiScope.launch {
                            adapter.isEndReached = true
                            adapter.notifyItemChanged(adapter.list.size - 1)
                            hideLoadingSpinnerForTimeline()
                        }
                    }

                    WidgetTimeLineViewModel.WIDGET_LOADING_STARTED -> {
                        // adding this line for case if in first page the filter widget data is empty and we are loading next page as automatically
                        if (adapter.itemCount == 0) {
                            timeLineViewModel.uiScope.launch {
                                showLoadingSpinnerForTimeline()
                            }
                        }
                    }
                }
            }
        }
    }

    private fun showLoadingSpinnerForTimeline() {
        binding.apply {
            loadingSpinnerTimeline.visibility = View.VISIBLE
            timelineRv.visibility = View.GONE
            timelineSnapLive.visibility = View.GONE
        }
    }

    private fun hideLoadingSpinnerForTimeline() {
        binding.apply {
            loadingSpinnerTimeline.visibility = View.GONE
            timelineRv.visibility = View.VISIBLE
        }
    }

    /**
     * used for hiding the Snap to live button
     * snap to live is mainly responsible for showing user the latest widget
     * if user is already at the latest widget,then usually this icon remain hidden
     **/
    private fun hideSnapToLiveForWidgets() {
        if (!showingSnapToLive)
            return
        showingSnapToLive = false
        binding.timelineSnapLive.visibility = View.GONE
        animateSnapToLiveButton()
    }

    /**
     * used for showing the Snap to Live button
     **/
    private fun showSnapToLiveForWidgets() {
        if (showingSnapToLive)
            return
        showingSnapToLive = true
        binding.timelineSnapLive.visibility = View.VISIBLE
        animateSnapToLiveButton()
    }

    private fun snapToLiveForTimeline() {
        binding.timelineRv.let { rv ->
            hideSnapToLiveForWidgets()
            timeLineViewModel.timeLineWidgets.size.let {
                rv.postDelayed(
                    {
                        rv.smoothScrollToPosition(0)
                    },
                    200
                )
            }
        }
    }

    private fun animateSnapToLiveButton() {
        snapToLiveAnimation?.cancel()

        val translateAnimation = ObjectAnimator.ofFloat(
            binding.timelineSnapLive,
            "translationY",
            if (showingSnapToLive) 0f else AndroidResource.dpToPx(
                TIMELINE_SNAP_TO_LIVE_ANIMATION_DESTINATION
            )
                .toFloat()
        )
        translateAnimation?.duration = TIMELINE_SNAP_TO_LIVE_ANIMATION_DURATION.toLong()
        val alphaAnimation =
            ObjectAnimator.ofFloat(
                binding.timelineSnapLive,
                "alpha",
                if (showingSnapToLive) 1f else 0f
            )
        alphaAnimation.duration = (TIMELINE_SNAP_TO_LIVE_ALPHA_ANIMATION_DURATION).toLong()
        alphaAnimation.addListener(object : Animator.AnimatorListener {
            override fun onAnimationEnd(animation: Animator) {
                binding.timelineSnapLive.visibility =
                    if (showingSnapToLive) View.VISIBLE else View.GONE
            }

            override fun onAnimationStart(animation: Animator) {
                binding.timelineSnapLive.visibility =
                    if (showingSnapToLive) View.GONE else View.VISIBLE
            }

            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })

        snapToLiveAnimation = AnimatorSet()
        snapToLiveAnimation?.play(translateAnimation)?.with(alphaAnimation)
        snapToLiveAnimation?.start()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        timeLineViewModel.clear()
    }

    companion object {
        const val TIMELINE_SNAP_TO_LIVE_ANIMATION_DURATION = 400F
        const val TIMELINE_SNAP_TO_LIVE_ALPHA_ANIMATION_DURATION = 320F
        const val TIMELINE_SNAP_TO_LIVE_ANIMATION_DESTINATION = 50
    }
}
