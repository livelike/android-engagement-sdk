package com.livelike.engagementsdk.widget.view

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat.startActivity
import com.bumptech.glide.Glide
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.R
import com.livelike.engagementsdk.core.utils.AndroidResource
import com.livelike.engagementsdk.databinding.WidgetAlertBinding
import com.livelike.engagementsdk.widget.SpecifiedWidgetView
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.WidgetsTheme
import com.livelike.engagementsdk.widget.model.Alert
import com.livelike.engagementsdk.widget.viewModel.AlertWidgetViewModel
import com.livelike.engagementsdk.widget.viewModel.BaseViewModel
import com.livelike.engagementsdk.widget.viewModel.WidgetStates
import kotlinx.coroutines.launch


internal class AlertWidgetView : SpecifiedWidgetView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    private var inflated = false
    private var binding: WidgetAlertBinding? = null

    var viewModel: AlertWidgetViewModel? = null

    override var dismissFunc: ((action: DismissAction) -> Unit)? =
        {
            viewModel?.dismissWidget(it)
            removeAllViews()
        }

    override var widgetViewModel: BaseViewModel? = null
        set(value) {
            field = value
            viewModel = value as AlertWidgetViewModel
        }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        viewModel?.dataFlow?.value?.let {
            inflate(context, it)
        }
        viewModel?.widgetStateFlow?.value?.let {
            interactionStateHandling(it)
        }
        uiScope.launch {
            /*launch {
                viewModel?.dataFlow?.collect {
                    logDebug { "showing the Alert WidgetView" }
                    it?.let {
                        inflate(context, it)
                    }
                }
            }*/
            launch {
                viewModel?.widgetStateFlow?.collect { widgetStates ->
                    widgetStates?.let {
                        interactionStateHandling(it)
                    }
                }
            }
        }
    }

    private fun interactionStateHandling(widgetStates: WidgetStates) {
        if (widgetStates == WidgetStates.INTERACTING && (!viewModel?.dataFlow?.value?.link_url.isNullOrEmpty())) {
            // will only be fired if link is available in alert widget
            viewModel?.markAsInteractive()
        }
        if (viewModel?.enableDefaultWidgetTransition == true) {
            defaultStateTransitionManager(widgetStates)
        }
    }

    private fun defaultStateTransitionManager(widgetStates: WidgetStates?) {
        when (widgetStates) {
            WidgetStates.READY -> {
                viewModel?.widgetStateFlow?.value = WidgetStates.INTERACTING
            }
            WidgetStates.INTERACTING -> {
                viewModel?.dataFlow?.value?.let {
                    viewModel?.startDismissTimout(it.timeout) {
                        viewModel?.widgetStateFlow?.value = WidgetStates.FINISHED
                    }
                }
            }
            WidgetStates.FINISHED -> {
                removeAllViews()
                parent?.let { (it as ViewGroup).removeAllViews() }
            }
            WidgetStates.RESULTS -> {
                // not needed presently
            }

            else -> {}
        }
    }

    override fun applyTheme(theme: WidgetsTheme) {
        super.applyTheme(theme)
        viewModel?.dataFlow?.value?.let { _ ->
            theme.getThemeLayoutComponent(WidgetType.ALERT)?.let { themeComponent ->
                //headers
                AndroidResource.updateThemeForView(
                    binding?.labelText,
                    themeComponent.title,
                    fontFamilyProvider
                )
                if (themeComponent.header?.background != null) {
                    binding?.labelText?.background =
                        AndroidResource.createDrawable(themeComponent.header)
                }
                themeComponent.header?.padding?.let {
                    AndroidResource.setPaddingForView(
                        binding?.labelText,
                        themeComponent.header.padding
                    )
                }

                //body
                binding?.bodyBackground?.background =
                    AndroidResource.createDrawable(themeComponent.body)
                AndroidResource.updateThemeForView(
                    binding?.bodyText,
                    themeComponent.body,
                    fontFamilyProvider
                )
                setThemeForFooter(theme)
            }
        }
    }

    private fun setThemeForFooter(theme: WidgetsTheme) {
        viewModel?.dataFlow?.value?.let { _ ->
            theme.getThemeLayoutComponent(WidgetType.ALERT)?.let { themeComponent ->
                if (themeComponent.footer?.background != null) {
                    binding?.linkBackground?.background =
                        AndroidResource.createDrawable(themeComponent.footer)
                }

                AndroidResource.updateThemeForView(
                    binding?.linkText,
                    themeComponent.footer,
                    fontFamilyProvider
                )
            }
        }
    }

    override fun moveToNextState() {
        super.moveToNextState()
        if (widgetViewModel?.widgetStateFlow?.value == WidgetStates.INTERACTING) {
            widgetViewModel?.widgetStateFlow?.value = WidgetStates.FINISHED
        } else {
            super.moveToNextState()
        }
    }

    private fun inflate(context: Context, resourceAlert: Alert) {
        if (!inflated) {
            inflated = true
            /* LayoutInflater.from(context)
                  .inflate(R.layout.widget_alert, this, true)*/
            binding =
                WidgetAlertBinding.inflate(LayoutInflater.from(context), this@AlertWidgetView, true)
        }

        binding?.apply {
            bodyText.text = resourceAlert.text
            labelText.text = resourceAlert.title
            linkText.text = resourceAlert.link_label
        }

        if (resourceAlert.link_url.isNullOrEmpty().not()) {
            binding?.linkBackground?.visibility = View.VISIBLE
            binding?.linkBackground?.setOnClickListener {
                openBrowser(context, resourceAlert.link_url)
            }
        } else {
            binding?.apply {
                linkArrow.visibility = View.GONE
                linkText.visibility = View.GONE
                linkBackground.visibility = View.GONE
                bodyBackground.setBackgroundResource(R.drawable.alert_rounded_corner_with_background)
            }
        }


        if (resourceAlert.image_url.isNullOrEmpty()) {
            binding?.bodyImage?.visibility = View.GONE
            val params = binding?.bodyText?.layoutParams as ConstraintLayout.LayoutParams
            params.rightMargin = AndroidResource.dpToPx(16)
            binding?.bodyText?.requestLayout()
        } else {
            resourceAlert.image_url.apply {
                Glide.with(context.applicationContext)
                    .load(resourceAlert.image_url)
                    .into(binding?.bodyImage!!)
            }
        }

        updateImagePosition(resourceAlert) // update image to center when text is not present

        if (resourceAlert.title.isNullOrEmpty()) {
            val params = binding?.labelText?.layoutParams as ConstraintLayout.LayoutParams
            params.topMargin = AndroidResource.dpToPx(0)
            binding?.labelText?.requestLayout()
        } else {
            binding?.labelText?.requestLayout()
        }

        widgetsTheme?.let {
            applyTheme(it)
        }
    }

    private fun updateImagePosition(resourceAlert: Alert){
        val constraintSet = ConstraintSet()
        constraintSet.clone(binding?.alertWidget)
        binding?.bodyImage?.id?.let { bodyImage ->
            if (resourceAlert.text.isEmpty()) {
                // Center bodyImage horizontally
                constraintSet.clear(bodyImage, ConstraintSet.START)
                constraintSet.clear(bodyImage, ConstraintSet.END)
                constraintSet.connect(
                    bodyImage,
                    ConstraintSet.START,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.START
                )
                constraintSet.connect(
                    bodyImage,
                    ConstraintSet.END,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.END
                )
                constraintSet.setHorizontalBias(bodyImage, 0.5f)


            } else {
                // Align bodyImage to the extreme right
                constraintSet.clear(bodyImage, ConstraintSet.START)
                constraintSet.connect(
                    bodyImage,
                    ConstraintSet.END,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.END
                )
            }

            constraintSet.applyTo(binding?.alertWidget)
        }
    }


    private fun openBrowser(context: Context, linkUrl: String) {
        viewModel?.onClickLink(linkUrl)
        val universalLinkIntent =
            Intent(Intent.ACTION_VIEW, Uri.parse(linkUrl)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        if (universalLinkIntent.resolveActivity(context.packageManager) != null) {
            startActivity(context, universalLinkIntent, Bundle.EMPTY)
        }
    }
}
