package com.livelike.engagementsdk.widget.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.R
import com.livelike.engagementsdk.core.utils.AndroidResource
import com.livelike.engagementsdk.databinding.WidgetTextOptionSelectionBinding
import com.livelike.engagementsdk.widget.OptionsWidgetThemeComponent
import com.livelike.engagementsdk.widget.SpecifiedWidgetView
import com.livelike.engagementsdk.widget.WidgetsTheme
import com.livelike.engagementsdk.widget.adapters.WidgetOptionsViewAdapter
import com.livelike.engagementsdk.widget.model.Resource
import com.livelike.engagementsdk.widget.viewModel.BaseViewModel
import com.livelike.engagementsdk.widget.viewModel.PollViewModel
import com.livelike.engagementsdk.widget.viewModel.PollWidget
import com.livelike.engagementsdk.widget.viewModel.WidgetStates
import com.livelike.utils.logDebug
import com.livelike.utils.parseISODateTime
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class PollView(context: Context, attr: AttributeSet? = null) : SpecifiedWidgetView(context, attr) {

    private var viewModel: PollViewModel? = null

    private var inflated = false
    private var binding: WidgetTextOptionSelectionBinding? = null
    private var adapter: WidgetOptionsViewAdapter? = null
    override var dismissFunc: ((DismissAction) -> Unit)? = { viewModel?.dismissWidget(it) }
    private var viewJob: Job? = null
    private val viewScope: CoroutineScope
        get() = CoroutineScope(Dispatchers.Main + (viewJob ?: Job()))


    private var dividerAdded:Boolean = false

    override var widgetViewModel: BaseViewModel? = null
        set(value) {
            field = value
            viewModel = value as PollViewModel
            uiScope.launch {
                launch { viewModel?.widgetStateFlow?.collect { stateObserver(it) } }
                launch { viewModel?.currentVoteIdFlow?.collect { clickedOptionObserver(it) } }
                launch { viewModel?.pointsFlow?.collect { rewardsObserver(it) } }
            }
        }

    private fun clickedOptionObserver(id: String?) {
        id?.let {
            adapter?.showPercentage = true
            adapter?.notifyDataSetChanged()
            viewModel?.onOptionClicked()
        }
    }

    // Refresh the view when re-attached to the activity
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        resourceObserver(viewModel?.dataFlow?.value)
        viewModel?.widgetStateFlow?.value.let { stateObserver(it) }

        viewScope.launch {
                viewModel?.resultsFlow?.collect {results->
                    if (isFirstInteraction)
                        resultsObserver(results)
                }
        }
    }

    private fun stateObserver(widgetStates: WidgetStates?) {
        when (widgetStates) {
            WidgetStates.READY -> {
                lockInteraction()
            }

            WidgetStates.INTERACTING -> {
                unLockInteraction()
                showResultAnimation = context.resources.getBoolean(R.bool.livelike_widget_show_animation)

                // show timer while widget interaction mode
                viewModel?.dataFlow?.value?.showTimer()
                viewScope.launch {
                        viewModel?.resultsFlow?.collect {results->
                            if (isFirstInteraction)
                                resultsObserver(results)
                        }
                }
            }

            WidgetStates.RESULTS, WidgetStates.FINISHED -> {
                lockInteraction()
                onWidgetInteractionCompleted()
                viewScope.launch {
                    viewModel?.resultsFlow?.collect { res ->
                        viewModel?.dataFlow?.value?.resource?.pubnubEnabled?.let {
                            resultsObserver(res)
                        }
                    }
                }
            }

            else -> {}
        }
        if (viewModel?.enableDefaultWidgetTransition == true) {
            defaultStateTransitionManager(widgetStates)
        }
    }

    private fun lockInteraction() {
        viewModel?.selectionLockedFlow?.value = true
    }

    private fun unLockInteraction() {
        viewModel?.selectionLockedFlow?.value = false
        viewModel?.markAsInteractive()
    }

    private fun defaultStateTransitionManager(widgetStates: WidgetStates?) {
        when (widgetStates) {
            WidgetStates.READY -> {
                moveToNextState()
            }

            WidgetStates.INTERACTING -> {
                viewModel?.dataFlow?.value?.let {
                    viewModel?.startDismissTimout(it.resource.timeout)
                }
            }

            WidgetStates.RESULTS -> {
                viewModel?.confirmationState()
            }

            WidgetStates.FINISHED -> {
                resourceObserver(null)
            }

            else -> {}
        }
    }

    private fun rewardsObserver(points: Int?) {
        points?.let {
            if (viewModel?.dataStoreDelegate?.shouldShowPointTutorial() != true && it > 0) {
                binding?.pointView?.startAnimation(it, true)
                wouldShowProgressionMeter(
                    viewModel?.rewardsType,
                    viewModel?.gamificationProfile?.value,
                    binding?.progressionMeterView!!
                )
            }
        }
    }

    override fun applyTheme(theme: WidgetsTheme) {
        super.applyTheme(theme)
        viewModel?.dataFlow?.value?.let { widget ->
            theme.getThemeLayoutComponent(widget.type)?.let { themeComponent ->
                if (themeComponent is OptionsWidgetThemeComponent) {
                    applyThemeOnTitleView(themeComponent)
                    applyThemeOnTagView(themeComponent)
                    adapter?.component = themeComponent
                    adapter?.fontFamilyProvider = fontFamilyProvider
                    adapter?.notifyDataSetChanged()
                    AndroidResource.createDrawable(themeComponent.body)?.let {
                        binding?.layTextRecyclerView?.background = it
                    }
                }
            }
        }
    }

    private var isFirstInteraction = false

    init {
        isFirstInteraction = viewModel?.getUserInteraction() != null
    }

    private fun resourceObserver(widget: PollWidget?) {
        widget?.apply {
            val optionList = resource.getMergedOptions() ?: return
            if (!inflated) {
                inflated = true
                binding = WidgetTextOptionSelectionBinding.inflate(
                    LayoutInflater.from(context),
                    this@PollView,
                    true
                )
                wouldInflateSponsorUi()
            }
            binding?.txtTitleBackground?.setBackgroundResource(R.drawable.header_rounded_corner_poll)
            binding?.layTextRecyclerView?.setBackgroundResource(R.drawable.body_rounded_corner_poll)

            setBodyBackgroundForSponsor()

            val horizontalDecoration =
                DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            val divider: Drawable? =
                ContextCompat.getDrawable(context, R.drawable.poll_list_item_separator)


            // Check if the divider is not null and if it hasn't been added before
            if (divider != null && !isDividerAdded()) {
                horizontalDecoration.setDrawable(divider)
                binding?.textRecyclerView?.addItemDecoration(horizontalDecoration)
                markDividerAdded()
            }


            // added tag as label for identification of widget (by default tag will be empty)
            setTagViewWithStyleChanges(context.resources.getString(R.string.livelike_poll_tag))
            binding?.titleView?.title = resource.question ?: ""
            //  update header background with margin or padding
            binding?.titleView?.titleViewBinding?.titleTextView?.gravity = Gravity.START
            adapter = adapter ?: WidgetOptionsViewAdapter(
                optionList,
                type,
                selectionLockedFlow = viewModel!!.selectionLockedFlow,
                selectedPositionFlow = viewModel!!.selectedPositionFlow,
                userSelectedOptionIdFlow = viewModel!!.userSelectedOptionIdFlow
            )
            adapter?.interacativeUntil = resource.interactiveUntil

            // set on click

            adapter?.onClick = {
                val selectedId = adapter?.myDataset?.get(
                    adapter?.selectedPositionFlow?.value ?: -1
                )?.id ?: ""
                viewModel?.selectedPositionFlow?.value = adapter?.selectedPositionFlow?.value ?: -1
                viewModel?.currentVoteIdFlow?.value = selectedId
                widgetLifeCycleEventsListener?.onUserInteract(widgetData)
                isFirstInteraction = true

                // analytics
                viewModel?.currentWidgetId?.let { widgetId ->
                    viewModel?.programId?.let { programId ->
                        viewModel?.trackWidgetEngagedAnalytics(
                            currentWidgetType = resource.getWidgetType(),
                            currentWidgetId = widgetId,
                            programId=programId,
                            widgetPrompt = resource.question ?: ""
                        )
                    }
                }
            }

            widgetsTheme?.let {
                applyTheme(it)
            }

            viewModel?.onWidgetInteractionCompleted = { onWidgetInteractionCompleted() }
            logDebug { "is Expired: ${viewModel?.isInteractivityExpired(resource.interactiveUntil)} " }
            if (viewModel?.isInteractivityExpired(resource.interactiveUntil) == false) {
                lockInteraction()
            }
            resource.interactiveUntil?.parseISODateTime()?.let {
                val epochTimeMs = it.toInstant().toEpochMilli()
                viewModel?.startInteractiveUntilTimeout(epochTimeMs)
            }

            uiScope.launch {
                viewModel?.disableInteractionFlow?.collect {
                    if (it == true) lockInteraction()
                }
            }

            binding?.textRecyclerView.apply {
                isFirstInteraction = viewModel?.getUserInteraction() != null
                if (viewModel?.getUserInteraction() != null) {
                    viewModel?.selectedOptionId = viewModel?.getUserInteraction()?.optionId
                }
                adapter?.restoreSelectedPosition(viewModel?.getUserInteraction()?.optionId)
                this?.adapter = this@PollView.adapter
            }
            if (widgetViewModel?.widgetStateFlow?.value == null || widgetViewModel?.widgetStateFlow?.value == WidgetStates.READY)
                widgetViewModel?.widgetStateFlow?.value = WidgetStates.READY
        }
        if (widget == null) {
            inflated = false
            removeAllViews()
            parent?.let { (it as ViewGroup).removeAllViews() }
        }
    }

    // Function to check if the divider has already been added
    private fun isDividerAdded(): Boolean {
        return dividerAdded
    }

    // Function to mark the divider as added
    private fun markDividerAdded() {
        dividerAdded = true
    }

    private fun PollWidget.showTimer() {
        showTimer(
            resource.timeout, binding?.textEggTimer,
            {
                viewModel?.animationEggTimerProgress = it
            },
            {
                viewModel?.dismissWidget(it)
            }
        )
    }

    private fun setBodyBackgroundForSponsor() {
        val bodyView = binding?.layTextRecyclerView ?: return
            val squareDrawable = ContextCompat.getDrawable(context, R.drawable.body_square_corner_poll)
            val roundDrawable = ContextCompat.getDrawable(context, R.drawable.body_rounded_corner_poll)

            if (squareDrawable != null && roundDrawable != null) {
                checkIfSponsorViewIsInflated(bodyView, squareDrawable, roundDrawable)
            }
    }


    private fun resultsObserver(resource: Resource?) {
        (resource ?: viewModel?.dataFlow?.value?.resource)?.apply {
            println("poll456-resltObserver ${resource?.kind} === ${resource?.id}")
            val optionResults = this.getMergedOptions() ?: return
            val totalVotes = optionResults.sumOf { it.getMergedVoteCount().toInt() }
            val options = viewModel?.dataFlow?.value?.resource?.getMergedOptions() ?: return
            for (opt in options) {
                optionResults.find {
                    it.id == opt.id
                }?.apply {
                    opt.updateCount(this)
                    opt.percentage = opt.getPercent(totalVotes.toFloat())
                }
            }

            adapter?.myDataset = options
            if (this.getMergedTotal() > 0) {
                adapter?.showPercentage = true
            }
            binding?.textRecyclerView?.swapAdapter(adapter, false)
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        viewScope.cancel()
    }
}
