package com.livelike.engagementsdk.widget.timeline.polling

import com.livelike.common.LiveLikeCallback
import com.livelike.engagementsdk.LiveLikeContentSession
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.WidgetStatus
import com.livelike.engagementsdk.WidgetsRequestOrdering
import com.livelike.engagementsdk.WidgetsRequestParameters
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.utils.isoDateTimeFormat
import com.livelike.utils.parseISODateTime
import kotlinx.coroutines.flow.MutableStateFlow
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.TimeZone
import java.util.Timer
import java.util.TimerTask

class LiveWidgetPollingData(
    val session : LiveLikeContentSession,
    val flow : MutableStateFlow<List<LiveLikeWidget>>,
    private val interval : Long = 30000
) {
    private var sinceLastPublishedTime : Long = 0
    private val utcDateFormat = "yyyy-mm-dd'T'HH:mm:ss.SSS'Z'";
    private val utcFormat = SimpleDateFormat(utcDateFormat, Locale.US).apply { timeZone = TimeZone.getTimeZone("UTC") }
    
    fun startPolling() {
        val date = utcFormat.format(Date())
        sinceLastPublishedTime = utcFormat.parse(date)!!.time
        Timer().schedule(task, 0, interval)
    }
    
    
    private val task = object : TimerTask() {
        override fun run() {
            session.getWidgets(
                LiveLikePagination.FIRST,
                WidgetsRequestParameters(
                    widgetStatus = WidgetStatus.PUBLISHED,
                    ordering = WidgetsRequestOrdering.RECENT,
                    since = ZonedDateTime.ofInstant(
                        Instant.ofEpochMilli(sinceLastPublishedTime),
                        ZoneId.of("UTC")
                    ).isoDateTimeFormat()
                ),
                
                liveLikeCallback = object : LiveLikeCallback<List<LiveLikeWidget>> {
                    override fun invoke(result : List<LiveLikeWidget>?, error : String?) {
                        if (result != null) {
                            processResult(result)
                        }
                    }

                })
        }
    }
    
    private fun processResult(result : List<LiveLikeWidget>) {
        val liveWidgetsList = result.filter { timePredicate(it.publishedAt) }
        if (liveWidgetsList.isNotEmpty()) {
            liveWidgetsList.get(0).publishedAt?.parseISODateTime()?.let {
                sinceLastPublishedTime = it.toInstant().toEpochMilli()
            }
            flow.tryEmit(liveWidgetsList)
        }
    }
    
    private fun timePredicate(timeUTC : String?) : Boolean {
        var publishedAtForWidget = 0L
        timeUTC?.parseISODateTime()?.let {
            publishedAtForWidget = it.toInstant().toEpochMilli()
        }
        return publishedAtForWidget > sinceLastPublishedTime
    }
    fun cancelPolling(){
        task.cancel()
    }
}