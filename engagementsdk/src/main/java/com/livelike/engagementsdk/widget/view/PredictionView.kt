@file:Suppress("UselessCallOnNotNull", "USELESS_ELVIS")

package com.livelike.engagementsdk.widget.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.R
import com.livelike.engagementsdk.core.utils.AndroidResource
import com.livelike.engagementsdk.databinding.WidgetTextOptionSelectionBinding
import com.livelike.engagementsdk.widget.OptionsWidgetThemeComponent
import com.livelike.engagementsdk.widget.SpecifiedWidgetView
import com.livelike.engagementsdk.widget.WidgetsTheme
import com.livelike.engagementsdk.widget.adapters.WidgetOptionsViewAdapter
import com.livelike.engagementsdk.widget.model.Resource
import com.livelike.engagementsdk.widget.viewModel.BaseViewModel
import com.livelike.engagementsdk.widget.viewModel.PredictionViewModel
import com.livelike.engagementsdk.widget.viewModel.PredictionWidget
import com.livelike.engagementsdk.widget.viewModel.WidgetStates
import com.livelike.utils.logDebug
import com.livelike.utils.parseISODateTime
import kotlinx.coroutines.launch
import java.lang.ref.WeakReference


class PredictionView(context: Context, attr: AttributeSet? = null) :
    SpecifiedWidgetView(context, attr) {

    private var viewModel: PredictionViewModel? = null

    private var inflated = false

    private var isFirstInteraction = false
    private var binding: WidgetTextOptionSelectionBinding? = null
    private var adapter: WidgetOptionsViewAdapter? = null
    override var dismissFunc: ((action: DismissAction) -> Unit)? = { viewModel?.dismissWidget(it) }

    override var widgetViewModel: BaseViewModel? = null
        get() = viewModel
        set(value) {
            field = value
            viewModel = value as PredictionViewModel
        }

    init {
        isFirstInteraction = viewModel?.getUserInteraction() != null
    }

    // Refresh the view when re-attached to the activity
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        widgetObserver(viewModel?.dataFlow?.value)
        viewModel?.widgetStateFlow?.value.let { widgetStateObserver(it) }
        uiScope.launch {
            viewModel?.widgetStateFlow?.collect { widgetStateObserver(it) }
        }
    }

    private fun widgetStateObserver(widgetStates: WidgetStates?) {
        when (widgetStates) {
            WidgetStates.READY -> {
                lockInteraction()
            }
            WidgetStates.INTERACTING -> {
                unLockInteraction()
                //showResultAnimation = true
                showResultAnimation = context.resources.getBoolean(R.bool.livelike_widget_show_animation)

                // show timer while widget interaction mode
                viewModel?.dataFlow?.value?.resource?.timeout?.let { timeout ->
                    showTimer(
                        timeout, binding?.textEggTimer,
                        {
                            viewModel?.animationEggTimerProgress = it
                        },
                        {
                            viewModel?.dismissWidget(it)
                        }
                    )
                }
            }
            WidgetStates.RESULTS, WidgetStates.FINISHED -> {
                lockInteraction()
                onWidgetInteractionCompleted()
                viewModel?.apply {
                    if (followUp) {
                        binding?.followupAnimation?.apply {
                            if (viewModel?.animationPath?.isNotEmpty() == true)
                                setAnimation(
                                    viewModel?.animationPath
                                )
                            progress = viewModel?.animationProgress!!
                            addAnimatorUpdateListener { valueAnimator ->
                                viewModel?.animationProgress = valueAnimator.animatedFraction
                            }
                            if (progress != 1f) {
                                resumeAnimation()
                            }
                            visibility = if (showResultAnimation) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }
                        }
                        viewModel?.points?.let {
                            if (viewModel?.dataStoreDelegate?.shouldShowPointTutorial() != true && it > 0) {
                                binding?.pointView?.startAnimation(it, true)
                                wouldShowProgressionMeter(
                                    viewModel?.rewardsType,
                                    viewModel?.gamificationProfile?.value,
                                    binding?.progressionMeterView!!
                                )
                            }
                        }
                    } else {
                        uiScope.launch {
                            viewModel?.resultsFlow?.collect {
                                if (isFirstInteraction)
                                    resultsObserver(it)
                            }
                        }

                        binding?.confirmationMessage?.apply {
                            if (isFirstInteraction) {
                                text =
                                    viewModel?.dataFlow?.value?.resource?.confirmationMessage ?: ""
                                viewModel?.animationPath?.let {
                                    if (it.isNotEmpty()) {
                                        viewModel?.animationProgress?.let { it1 ->
                                            startAnimation(
                                                it,
                                                it1
                                            )
                                        }
                                    }
                                }
                                subscribeToAnimationUpdates { value ->
                                    viewModel?.animationProgress = value
                                }
                                visibility = if (showResultAnimation) {
                                    View.VISIBLE
                                } else {
                                    View.GONE
                                }
                            }
                        }

                        viewModel?.points?.let {
                            if (viewModel?.dataStoreDelegate?.shouldShowPointTutorial() != true && it > 0) {
                                binding?.pointView?.startAnimation(it, true)
                                wouldShowProgressionMeter(
                                    viewModel?.rewardsType,
                                    viewModel?.gamificationProfile?.value,
                                    binding?.progressionMeterView!!
                                )
                            }
                        }
                    }
                }
            }
            else -> {}
        }
        if (viewModel?.enableDefaultWidgetTransition == true) {
            defaultStateTransitionManager(widgetStates)
        }
    }

    private fun lockInteraction() {
        viewModel?.selectionLockedFlow?.value = true
        viewModel?.dataFlow?.value?.let {
            val isFollowUp = it.resource.kind?.contains("follow-up") ?: false
            if (isFollowUp) {
                binding?.textEggTimer?.showCloseButton { viewModel?.dismissWidget(DismissAction.TIMEOUT) }

            }
        }
    }

    private fun unLockInteraction() {
        viewModel?.dataFlow?.value?.let {
            val isFollowUp = it.resource.kind?.contains("follow-up") ?: false
            if (!isFollowUp) {
                viewModel?.selectionLockedFlow?.value = false
                viewModel?.markAsInteractive()
            }
        }
    }

    private fun defaultStateTransitionManager(widgetStates: WidgetStates?) {
        when (widgetStates) {
            WidgetStates.READY -> {
                moveToNextState()
            }
            WidgetStates.INTERACTING -> {
                viewModel?.dataFlow?.value?.let {
                    val isFollowUp = it.resource.kind?.contains("follow-up") ?: false
                    viewModel?.startDismissTimout(
                        it.resource.timeout,
                        isFollowUp
                    )
                }
            }
            WidgetStates.RESULTS -> {

            }
            WidgetStates.FINISHED -> {
                widgetObserver(null)
            }
            else -> {}
        }
    }

    private fun resultsObserver(resource: Resource?) {
        (resource ?: viewModel?.dataFlow?.value?.resource)?.apply {
            val optionResults = this.getMergedOptions() ?: return
            val totalVotes = optionResults.sumOf { it.getMergedVoteCount().toInt() ?: 0 }
            val options = viewModel?.dataFlow?.value?.resource?.getMergedOptions() ?: return
            for (opt in options) {
                optionResults.find {
                    it.id == opt.id
                }?.apply {
                    opt.updateCount(this)
                    opt.percentage = opt.getPercent(totalVotes.toFloat())
                }
            }
            adapter?.myDataset = options
            adapter?.showPercentage = true
            binding?.textRecyclerView?.swapAdapter(adapter, false)
        }
    }

    override fun applyTheme(theme: WidgetsTheme) {
        super.applyTheme(theme)
        viewModel?.dataFlow?.value?.let { widget ->
            theme.getThemeLayoutComponent(widget.type)?.let { themeComponent ->
                if (themeComponent is OptionsWidgetThemeComponent) {
                    applyThemeOnTitleView(themeComponent)
                    applyThemeOnTagView(themeComponent)
                    adapter?.component = themeComponent
                    adapter?.notifyDataSetChanged()
                    AndroidResource.createDrawable(themeComponent.body)?.let {
                        binding?.layTextRecyclerView?.background = it
                    }
                }
            }
        }
    }

    private fun setBodyBackgroundForSponsor(){
        val bodyView = binding?.layTextRecyclerView ?: return
        val squareDrawable = ContextCompat.getDrawable(context, R.drawable.body_square_corner_prediction)
        val roundDrawable = ContextCompat.getDrawable(context, R.drawable.body_rounded_corner_prediction)

        if (squareDrawable != null && roundDrawable != null) {
            checkIfSponsorViewIsInflated(bodyView, squareDrawable, roundDrawable)
        }
    }

    private fun widgetObserver(widget: PredictionWidget?) {
        widget?.apply {
            val optionList = resource.getMergedOptions() ?: return
            if (!inflated) {
                inflated = true
                binding = WidgetTextOptionSelectionBinding.inflate(
                    LayoutInflater.from(context),
                    this@PredictionView,
                    true
                )
                wouldInflateSponsorUi()
            }

            val isFollowUp = resource.kind?.contains("follow-up") ?: false

            // added tag for identification of widget (by default will be empty)
            if (isFollowUp) {
                setTagViewWithStyleChanges(context.resources.getString(R.string.livelike_follow_up_tag))
            } else {
                setTagViewWithStyleChanges(context.resources.getString(R.string.livelike_prediction_tag))
            }

            binding?.apply {
                titleView.title = resource.question ?: ""
                txtTitleBackground.setBackgroundResource(R.drawable.header_rounded_corner_prediciton)
                layTextRecyclerView.setBackgroundResource(R.drawable.body_rounded_corner_prediction)
                setBodyBackgroundForSponsor()

                val horizontalDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
                val divider: Drawable? = ContextCompat.getDrawable(context, R.drawable.prediction_list_item_separator)
                divider?.let { horizontalDecoration.setDrawable(it) }
                textRecyclerView.addItemDecoration(horizontalDecoration)

                titleView.titleViewBinding.titleTextView.gravity = Gravity.START
            }
            viewModel!!.userSelectedOptionIdFlow.value =
                (if (resource.textPredictionId.isNullOrEmpty()) resource.imagePredictionId else resource.textPredictionId)
                    ?: ""
            adapter = adapter ?: WidgetOptionsViewAdapter(
                optionList,
                widget.type,
                resource.correctOptionId,
                selectionLockedFlow = viewModel!!.selectionLockedFlow,
                selectedPositionFlow = viewModel!!.selectedPositionFlow,
                userSelectedOptionIdFlow = viewModel!!.userSelectedOptionIdFlow
            )
            adapter?.interacativeUntil = resource.interactiveUntil

            // set on click
            adapter?.onClick = {
                adapter?.notifyDataSetChanged()
                viewModel?.onOptionClicked()
                isFirstInteraction = true
                widgetLifeCycleEventsListener?.onUserInteract(widgetData)
            }

            widgetsTheme?.let {
                applyTheme(it)
            }

            logDebug { "is Expired: ${viewModel?.isInteractivityExpired(resource.interactiveUntil)} " }
            if (viewModel?.isInteractivityExpired(resource.interactiveUntil) == false) {
                lockInteraction()
            }
            resource.interactiveUntil?.parseISODateTime()?.let {
                val epochTimeMs = it.toInstant().toEpochMilli()
                viewModel?.startInteractiveUntilTimeout(epochTimeMs)
            }

            uiScope.launch {
                viewModel?.disableInteractionFlow?.collect {
                    logDebug { "disable interaction: $it" }
                    if (it == true) lockInteraction()
                }
            }

            if (!isFollowUp)
                viewModel?.apply {
                    val rootPath = widgetViewThemeAttributes.stayTunedAnimation
                    val weakContext = WeakReference(context.applicationContext)
                    animationPath = AndroidResource.selectRandomLottieAnimation(
                        rootPath,
                        weakContext
                    ) ?: ""
                }

            binding?.textRecyclerView?.apply {
                isFirstInteraction = viewModel?.getUserInteraction() != null
                if (viewModel?.getUserInteraction() != null) {
                    viewModel?.selectedOptionId = viewModel?.getUserInteraction()?.optionId
                }
                this@PredictionView.adapter?.restoreSelectedPosition(viewModel?.getUserInteraction()?.optionId)
                this.adapter = this@PredictionView.adapter
                setHasFixedSize(true)
            }

            if (isFollowUp) {
                val selectedPredictionId =
                    viewModel?.dataStoreDelegate?.getWidgetPredictionVotedAnswerIdOrEmpty(if (resource.textPredictionId.isNullOrEmpty()) resource.imagePredictionId else resource.textPredictionId)
                selectedPredictionId?.let {
                    viewModel?.followupState(
                        selectedPredictionId,
                        resource.correctOptionId,
                        widgetViewThemeAttributes.widgetWinAnimation,
                        widgetViewThemeAttributes.widgetLoseAnimation
                    )
                }
            }
            if (widgetViewModel?.widgetStateFlow?.value == null || widgetViewModel?.widgetStateFlow?.value == WidgetStates.READY)
                widgetViewModel?.widgetStateFlow?.value = WidgetStates.READY
        }

        if (widget == null) {
            inflated = false
            removeAllViews()
            parent?.let { (it as ViewGroup).removeAllViews() }
        }
    }
}
