@file:Suppress("UNNECESSARY_SAFE_CALL", "UNCHECKED_CAST")

package com.livelike.engagementsdk.widget.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.R
import com.livelike.engagementsdk.widget.SpecifiedWidgetView
import com.livelike.engagementsdk.widget.model.Resource
import com.livelike.engagementsdk.widget.view.components.PointView
import com.livelike.engagementsdk.widget.viewModel.BaseViewModel
import com.livelike.engagementsdk.widget.viewModel.WidgetState
import com.livelike.engagementsdk.widget.viewModel.WidgetStates
import com.livelike.engagementsdk.widget.viewModel.WidgetViewModel
import com.livelike.widget.parseDuration
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * For now creating separate class, will mere it with specified widget view after full assessment of other widget views and then move all widget views to inherit this
 * Also For now Doing minimal refactor to expedite image slider delivery.
 */

internal abstract class GenericSpecifiedWidgetView<Entity : Resource, T : WidgetViewModel<Entity>>(
    context: Context,
    attr: AttributeSet? = null
) : SpecifiedWidgetView(context, attr) {

    // Move viewmodel to constructor
    lateinit var viewModel: T

    override var dismissFunc: ((DismissAction) -> Unit)? = { viewModel?.dismissWidget(it) }

    var isViewInflated = false
    var isFirstInteraction = false

    override var widgetViewModel: BaseViewModel? = null
        get() = viewModel
        set(value) {
            field = value
            viewModel = value as T
//            subscribeCalls()
        }

    protected open fun stateObserver(widgetState: WidgetState) {
        when (widgetState) {
            WidgetState.LOCK_INTERACTION -> confirmInteraction()
            WidgetState.SHOW_RESULTS -> showResults()
            WidgetState.SHOW_GAMIFICATION -> {
                rewardsObserver()
                if (viewModel?.enableDefaultWidgetTransition == true) {
                    viewModel?.uiScope?.launch {
                        delay(2000)
                        viewModel?.dismissWidget(DismissAction.TIMEOUT)
                    }
                }
            }
            WidgetState.DISMISS -> {
                dataModelObserver(null)
            }
        }
    }

    protected abstract fun showResults()

    protected abstract fun confirmInteraction()

    protected open fun dataModelObserver(entity: Entity?) {
        entity?.let { _ ->
            if (!isViewInflated) {
                isViewInflated = true
                if (widgetViewModel?.widgetStateFlow?.value == null)
                    widgetViewModel?.widgetStateFlow?.value = WidgetStates.READY
            }
        }
        if (entity == null) {
            isViewInflated = false
            removeAllViews()
            parent?.let { (it as ViewGroup).removeAllViews() }
        }
    }

    private fun rewardsObserver() {
        viewModel.gamificationProfile?.value?.let {
            if (viewModel?.dataStoreDelegate?.shouldShowPointTutorial() != true && it.newPoints > 0) {
                findViewById<PointView>(R.id.pointView).startAnimation(it.newPoints, true)
                wouldShowProgressionMeter(
                    viewModel?.rewardsType,
                    it,
                    findViewById(R.id.progressionMeterView)
                )
            }
        }
    }

    protected open fun subscribeCalls() {
        dataModelObserver(viewModel.dataFlow.value)
        viewModel?.widgetStateFlow?.value?.let { widgetStateObserver(it) }
        uiScope.launch {
            /*launch {
                viewModel.dataFlow.collect {
                    dataModelObserver(it)
                }
            }*/
            launch {
                viewModel.stateFlow.collect {
                    it?.let { stateObserver(it) }
                }
            }
            launch {
                widgetViewModel?.widgetStateFlow?.collect {
                    widgetStateObserver(it)
                }
            }
        }
    }

    private fun widgetStateObserver(widgetState: WidgetStates?){
        when (widgetState) {
            WidgetStates.READY -> {
                isFirstInteraction = false
                lockInteraction()
            }
            WidgetStates.INTERACTING -> {
                unLockInteraction()
                //showResultAnimation = true
                showResultAnimation = context.resources.getBoolean(R.bool.livelike_widget_show_animation)
                viewModel?.dataFlow?.value?.timeout?.let { timeout ->
                    showTimer(
                        timeout, findViewById(R.id.textEggTimer),
                        {
                            viewModel?.animationEggTimerProgress = it
                        },
                        {
                            viewModel?.dismissWidget(it)
                        }
                    )
                }
                findViewById<LinearLayout>(R.id.lay_lock).visibility = View.VISIBLE
                uiScope.launch {
                    viewModel?.resultsFlow?.collect {
                        if (isFirstInteraction)
                            showResults()
                    }
                }
            }
            WidgetStates.RESULTS, WidgetStates.FINISHED -> {
                lockInteraction()
                onWidgetInteractionCompleted()
                uiScope.launch {
                    viewModel?.resultsFlow?.collect { showResults() }
                }
                // showResults()
                viewModel.confirmInteraction()
            }
            else -> {}
        }

        if (viewModel?.enableDefaultWidgetTransition == true) {
            defaultStateTransitionManager(widgetState)
        }
    }

    internal abstract fun lockInteraction()

    internal abstract fun unLockInteraction()

    private fun defaultStateTransitionManager(widgetStates: WidgetStates?) {
        when (widgetStates) {
            WidgetStates.READY -> {
                moveToNextState()
            }
            WidgetStates.INTERACTING -> {
                viewModel.dataFlow.value?.let { entity ->
                    val timeout = parseDuration(entity.timeout)
                    viewModel.startInteractionTimeout(timeout)
                }
//            viewModel?.data?.latest()?.let {
//                viewModel?.startDismissTimout(it.resource.timeout)
//            }
            }
            WidgetStates.RESULTS -> {
//            viewModel?.confirmationState()
            }
            WidgetStates.FINISHED -> {
                dataModelObserver(null)
            }
            else -> {}
        }
    }

    protected open fun unsubscribeCalls() {
     //this will be empty
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        subscribeCalls()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        unsubscribeCalls()
    }
}
