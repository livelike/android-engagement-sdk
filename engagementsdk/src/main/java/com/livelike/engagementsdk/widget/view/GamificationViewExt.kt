@file:Suppress("UNNECESSARY_SAFE_CALL")

package com.livelike.engagementsdk.widget.view


import com.livelike.engagementsdk.core.data.models.RewardsType
import com.livelike.engagementsdk.widget.SpecifiedWidgetView
import com.livelike.engagementsdk.widget.data.models.Badge
import com.livelike.engagementsdk.widget.data.models.ProgramGamificationProfile
import com.livelike.engagementsdk.widget.view.components.ProgressionMeterView
import com.livelike.utils.logDebug

/**
 * extensions related to gamification views like progression meter
 */


internal fun SpecifiedWidgetView.wouldShowProgressionMeter(
    rewardsType: RewardsType?,
    latest: ProgramGamificationProfile?,
    progressionMeterView: ProgressionMeterView
) {
    latest?.let {
        logDebug { "showing Gamification Meter ,Reward Type:$rewardsType newBadge:${it.newBadges} currentBadge:${it.currentBadge} points:${it.points} newPoints:${it.newPoints}" }
        if (rewardsType!! == RewardsType.BADGES) {
            val nextBadgeToDisplay: Badge? = if (it.newBadges.isNullOrEmpty()) {
                it.nextBadge
            } else {
                it.newBadges?.maxOrNull()!!
            }
            nextBadgeToDisplay?.let { nextBadge ->
                progressionMeterView.animatePointsBadgeProgression(
                    it.points - it.newPoints,
                    it.newPoints,
                    nextBadge.points,
                    nextBadge.imageFile
                )
            }
        }
    }
}
