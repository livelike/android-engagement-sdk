package com.livelike.engagementsdk.widget

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.livelike.common.DataStoreDelegate
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.FontFamilyProvider
import com.livelike.engagementsdk.LiveLikeEngagementTheme
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.R
import com.livelike.engagementsdk.ViewAnimationEvents
import com.livelike.engagementsdk.WidgetInfos
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.core.services.messaging.proxies.LiveLikeWidgetEntity
import com.livelike.engagementsdk.core.services.messaging.proxies.WidgetLifeCycleEventsListener
import com.livelike.engagementsdk.core.utils.AndroidResource
import com.livelike.engagementsdk.widget.WidgetType.ALERT
import com.livelike.engagementsdk.widget.WidgetType.CHEER_METER
import com.livelike.engagementsdk.widget.WidgetType.COLLECT_BADGE
import com.livelike.engagementsdk.widget.WidgetType.IMAGE_NUMBER_PREDICTION
import com.livelike.engagementsdk.widget.WidgetType.IMAGE_NUMBER_PREDICTION_FOLLOW_UP
import com.livelike.engagementsdk.widget.WidgetType.IMAGE_POLL
import com.livelike.engagementsdk.widget.WidgetType.IMAGE_PREDICTION
import com.livelike.engagementsdk.widget.WidgetType.IMAGE_PREDICTION_FOLLOW_UP
import com.livelike.engagementsdk.widget.WidgetType.IMAGE_QUIZ
import com.livelike.engagementsdk.widget.WidgetType.IMAGE_SLIDER
import com.livelike.engagementsdk.widget.WidgetType.POINTS_TUTORIAL
import com.livelike.engagementsdk.widget.WidgetType.SOCIAL_EMBED
import com.livelike.engagementsdk.widget.WidgetType.TEXT_ASK
import com.livelike.engagementsdk.widget.WidgetType.TEXT_NUMBER_PREDICTION
import com.livelike.engagementsdk.widget.WidgetType.TEXT_NUMBER_PREDICTION_FOLLOW_UP
import com.livelike.engagementsdk.widget.WidgetType.TEXT_POLL
import com.livelike.engagementsdk.widget.WidgetType.TEXT_PREDICTION
import com.livelike.engagementsdk.widget.WidgetType.TEXT_PREDICTION_FOLLOW_UP
import com.livelike.engagementsdk.widget.WidgetType.TEXT_QUIZ
import com.livelike.engagementsdk.widget.WidgetType.VIDEO_ALERT
import com.livelike.engagementsdk.widget.data.respository.WidgetInteractionRepository
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.repository.ProgramRepository
import com.livelike.engagementsdk.widget.view.AlertWidgetView
import com.livelike.engagementsdk.widget.view.CheerMeterView
import com.livelike.engagementsdk.widget.view.CollectBadgeWidgetView
import com.livelike.engagementsdk.widget.view.EmojiSliderWidgetView
import com.livelike.engagementsdk.widget.view.NumberPredictionView
import com.livelike.engagementsdk.widget.view.PollView
import com.livelike.engagementsdk.widget.view.PredictionView
import com.livelike.engagementsdk.widget.view.QuizView
import com.livelike.engagementsdk.widget.view.SocialEmbedWidgetView
import com.livelike.engagementsdk.widget.view.TextAskView
import com.livelike.engagementsdk.widget.view.components.EggTimerCloseButtonView
import com.livelike.engagementsdk.widget.view.components.PointsTutorialView
import com.livelike.engagementsdk.widget.view.components.TagView
import com.livelike.engagementsdk.widget.view.components.TitleView
import com.livelike.engagementsdk.widget.view.components.VideoAlertWidgetView
import com.livelike.engagementsdk.widget.viewModel.BaseViewModel
import com.livelike.engagementsdk.widget.viewModel.WidgetStates
import com.livelike.network.NetworkApiClient
import com.livelike.realtime.RealTimeClientMessage
import com.livelike.serialization.gson
import com.livelike.utils.Once
import com.livelike.utils.logDebug
import com.livelike.widget.parseDuration
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import java.util.Calendar
import kotlin.math.min

internal class WidgetProvider : WidgetProviderCore() {
    fun get(
        widgetMessagingClient: ((RealTimeClientMessage) -> Unit)? = null,
        widgetInfos: WidgetInfos,
        context: Context,
        analyticsService: AnalyticsService,
        configurationOnce: Once<SdkConfiguration>,
        onDismiss: (() -> Unit)?,
        currentProfileOnce: Once<LiveLikeProfile>,
        programRepository: ProgramRepository? = null,
        animationEventsFlow: MutableStateFlow<ViewAnimationEvents?>,
        widgetThemeAttributes: WidgetViewThemeAttributes,
        liveLikeEngagementTheme: LiveLikeEngagementTheme?,
        widgetInteractionRepository: WidgetInteractionRepository? = null,
        networkApiClient: NetworkApiClient,
        rewardItemMapCache: Map<String, RewardItem>,
        userProfileRewardDelagate: UserProfileDelegate?,
        dataStoreDelegate: DataStoreDelegate,
        viewModelDispatcher: CoroutineDispatcher = Dispatchers.Default,
        uiDispatcher: CoroutineDispatcher = Dispatchers.Main,
        predictionLottieAnimationPath: ((String) -> String?)?,
    ): SpecifiedWidgetView? {
        val widgetModel = getWidgetModel(
            widgetMessagingClient,
            widgetInfos,
            analyticsService,
            configurationOnce,
            currentProfileOnce,
            onDismiss,
            programRepository,
            animationEventsFlow,
            widgetInteractionRepository,
            networkApiClient,
            rewardItemMapCache,
            userProfileRewardDelagate,
            dataStoreDelegate,
            predictionLottieAnimationPath,
            viewModelDispatcher,
            uiDispatcher
        )
        logDebug { "WidgetProvider: WidgetModel : $widgetModel for ${widgetInfos.widgetId},${widgetInfos.type}" }
        val specifiedWidgetView = when (WidgetType.fromString(widgetInfos.type)) {
            ALERT -> AlertWidgetView(context).apply {
                this.widgetsTheme = liveLikeEngagementTheme?.widgets
                this.fontFamilyProvider = liveLikeEngagementTheme?.fontFamilyProvider
                widgetViewModel = widgetModel
            }

            VIDEO_ALERT -> VideoAlertWidgetView(context).apply {
                this.widgetsTheme = liveLikeEngagementTheme?.widgets
                this.fontFamilyProvider = liveLikeEngagementTheme?.fontFamilyProvider
                widgetViewModel = widgetModel
            }

            TEXT_QUIZ, IMAGE_QUIZ -> QuizView(context).apply {
                widgetViewThemeAttributes = widgetThemeAttributes
                this.widgetsTheme = liveLikeEngagementTheme?.widgets
                this.fontFamilyProvider = liveLikeEngagementTheme?.fontFamilyProvider
                widgetViewModel = widgetModel
            }

            IMAGE_PREDICTION, IMAGE_PREDICTION_FOLLOW_UP, TEXT_PREDICTION, TEXT_PREDICTION_FOLLOW_UP -> PredictionView(
                context
            ).apply {
                widgetViewThemeAttributes = widgetThemeAttributes
                this.widgetsTheme = liveLikeEngagementTheme?.widgets
                this.fontFamilyProvider = liveLikeEngagementTheme?.fontFamilyProvider
                widgetViewModel = widgetModel
            }

            TEXT_POLL, IMAGE_POLL -> PollView(context).apply {
                widgetViewThemeAttributes = widgetThemeAttributes
                this.widgetsTheme = liveLikeEngagementTheme?.widgets
                this.fontFamilyProvider = liveLikeEngagementTheme?.fontFamilyProvider
                widgetViewModel = widgetModel
            }

            POINTS_TUTORIAL -> PointsTutorialView(context).apply {
                this.widgetsTheme = liveLikeEngagementTheme?.widgets
                this.fontFamilyProvider = liveLikeEngagementTheme?.fontFamilyProvider
                widgetViewModel = widgetModel
            }

            COLLECT_BADGE -> CollectBadgeWidgetView(context).apply {
                this.widgetsTheme = liveLikeEngagementTheme?.widgets
                this.fontFamilyProvider = liveLikeEngagementTheme?.fontFamilyProvider
                widgetViewModel = widgetModel
            }

            CHEER_METER -> CheerMeterView(context).apply {
                this.widgetsTheme = liveLikeEngagementTheme?.widgets
                this.fontFamilyProvider = liveLikeEngagementTheme?.fontFamilyProvider
                widgetViewThemeAttributes = widgetThemeAttributes
                widgetViewModel = widgetModel
            }

            IMAGE_SLIDER -> EmojiSliderWidgetView(context).apply {
                this.widgetsTheme = liveLikeEngagementTheme?.widgets
                this.fontFamilyProvider = liveLikeEngagementTheme?.fontFamilyProvider
                widgetViewThemeAttributes = widgetThemeAttributes
                widgetViewModel = widgetModel
            }

            SOCIAL_EMBED -> SocialEmbedWidgetView(context).apply {
                this.widgetsTheme = liveLikeEngagementTheme?.widgets
                this.fontFamilyProvider = liveLikeEngagementTheme?.fontFamilyProvider
                widgetViewModel = widgetModel
            }

            TEXT_ASK -> TextAskView(context).apply {
                widgetViewThemeAttributes = widgetThemeAttributes
                this.widgetsTheme = liveLikeEngagementTheme?.widgets
                this.fontFamilyProvider = liveLikeEngagementTheme?.fontFamilyProvider
                widgetViewModel = widgetModel
            }

            TEXT_NUMBER_PREDICTION, TEXT_NUMBER_PREDICTION_FOLLOW_UP, IMAGE_NUMBER_PREDICTION, IMAGE_NUMBER_PREDICTION_FOLLOW_UP -> NumberPredictionView(
                context
            ).apply {
                widgetViewThemeAttributes = widgetThemeAttributes
                this.widgetsTheme = liveLikeEngagementTheme?.widgets
                this.fontFamilyProvider = liveLikeEngagementTheme?.fontFamilyProvider
                widgetViewModel = widgetModel
            }

            else -> null
        }
        logDebug {
            "WidgetProvider: Widget created from provider, type: ${
                WidgetType.fromString(
                    widgetInfos.type
                )
            }"
        }
        logDebug { "WidgetProvider: WidgetView Created: $specifiedWidgetView" }
        specifiedWidgetView?.widgetId = widgetInfos.widgetId
        specifiedWidgetView?.widgetInfos = widgetInfos
        return specifiedWidgetView
    }
}

abstract class SpecifiedWidgetView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    internal var fontFamilyProvider: FontFamilyProvider? = null

    // initially it will be false, when widget moves to interaction state it will be turned on to show it to user in result state
    protected var showResultAnimation: Boolean = false

    var widgetId: String = ""
    lateinit var widgetInfos: WidgetInfos
    open var widgetViewModel: BaseViewModel? = null
    open var dismissFunc: ((action: DismissAction) -> Unit)? = null
    open var widgetViewThemeAttributes: WidgetViewThemeAttributes = WidgetViewThemeAttributes()
    open var widgetsTheme: WidgetsTheme? = null

    var widgetLifeCycleEventsListener: WidgetLifeCycleEventsListener? = null

    lateinit var widgetData: LiveLikeWidgetEntity
    protected val uiScope = MainScope()

    init {
        layoutParams = LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
        )
        orientation = VERTICAL
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        widgetData = gson.fromJson(widgetInfos.payload.toString(), LiveLikeWidgetEntity::class.java)
        postDelayed(
            {
                widgetData.height = height
                widgetLifeCycleEventsListener?.onWidgetPresented(widgetData)
            }, 500
        )
        subscribeWidgetStateAndPublishToLifecycleListener()
    }

    /**
     * would inflate and add sponsor ui as a widget view footer if sponsor exists
     */
    protected fun wouldInflateSponsorUi() {
        widgetData.sponsors?.let {
            if (it.isNotEmpty()) {
                val sponsor = it[0]
                val sponsorView = inflate(context, R.layout.default_sponsor_ui, null).apply {
                    tag = "sponsorView"
                }
                addView(sponsorView)
                val sponsorImageView = sponsorView.findViewById<ImageView>(R.id.sponsor_iv)
                Glide.with(context).load(sponsor.logoUrl).into(sponsorImageView)
            }
        }
    }

  /**
    this checks if the sponsor view is inflated, if yes the bottom rounded corner of the widget body  is changed to square
  */
    protected fun checkIfSponsorViewIsInflated(bodyView:View,squareDrawable:Drawable, roundDrawable:Drawable){
        val sponsorView = findViewWithTag<View>("sponsorView")
        if (sponsorView != null) {
           bodyView.background = squareDrawable
        } else {
            bodyView.background = roundDrawable
        }
    }

    private fun subscribeWidgetStateAndPublishToLifecycleListener() {
        uiScope.launch {
            widgetViewModel?.widgetStateFlow?.collect {
                it?.let {
                    widgetLifeCycleEventsListener?.onWidgetStateChange(it, widgetData)
                }
            }
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        uiScope.cancel()
        widgetLifeCycleEventsListener?.onWidgetDismissed(widgetData)
    }

    fun onWidgetInteractionCompleted() {
        widgetLifeCycleEventsListener?.onWidgetInteractionCompleted(widgetData)
    }

    internal fun showTimer(
        time: String,
        v: EggTimerCloseButtonView?,
        onUpdate: (Float) -> Unit,
        dismissAction: (action: DismissAction) -> Unit
    ) {
        if (widgetViewModel?.showTimer == false) {
            v?.visibility = View.GONE
            return
        }

        val animationLength = parseDuration(time).toFloat()
        var remainingAnimationLength = animationLength
        if (widgetViewModel?.timerStartTime != null) {
            remainingAnimationLength =
                animationLength - (Calendar.getInstance().timeInMillis - (widgetViewModel?.timerStartTime
                    ?: 0)).toFloat()
        } else {
            widgetViewModel?.timerStartTime = Calendar.getInstance().timeInMillis
        }
        val animationEggTimerProgress =
            (animationLength - remainingAnimationLength) / animationLength

        if (animationEggTimerProgress < 1f) {
            v?.startAnimationFrom(
                animationEggTimerProgress,
                remainingAnimationLength,
                onUpdate,
                dismissAction,
                widgetViewModel?.showDismissButton ?: true
            )
        }
    }

    protected fun applyThemeOnTitleView(it: WidgetBaseThemeComponent) {
        findViewById<TitleView>(R.id.titleView)?.componentTheme = it.title
        AndroidResource.updateThemeForView(
            findViewById<TitleView>(R.id.titleView)?.titleViewBinding?.titleTextView,
            it.title,
            fontFamilyProvider
        )
        if (it.header?.background != null) {
            findViewById<View>(R.id.txtTitleBackground)?.background =
                AndroidResource.createDrawable(it.header)
        }
        AndroidResource.setPaddingForView(findViewById(R.id.txtTitleBackground), it.header?.padding)
    }

    /**
     * this method in used to apply theme on tag view
     **/
    protected fun applyThemeOnTagView(it: WidgetBaseThemeComponent) {
        findViewById<TagView>(R.id.tagView)?.componentTheme = it.tag
        AndroidResource.updateThemeForView(
            findViewById(R.id.tagTextView), it.tag, fontFamilyProvider
        )
    }


    /**
     * this method in used to set tag view with style changes (default appearance)
     **/
    protected fun setTagViewWithStyleChanges(tag: String) {
        val tagView = findViewById<TagView>(R.id.tagView)
        if (tag.isNotEmpty()) {
            tagView.tag = tag
            tagView.visibility = View.VISIBLE
            AndroidResource.updateDefaultThemeForTagView(
                findViewById<TitleView>(R.id.titleView)?.titleViewBinding?.titleTextView,
                findViewById(R.id.titleView)
            )
        } else {
            tagView.visibility = View.GONE
            findViewById<TitleView>(R.id.titleView)?.titleViewBinding?.titleTextView?.isAllCaps =
                context.resources.getBoolean(R.bool.livelike_widget_title_text_all_caps)
        }
    }

    /**
     * override this method in respective widgets to respect runtime unified json theme updation
     **/
    open fun applyTheme(theme: WidgetsTheme) {
        widgetsTheme = theme
    }

    fun applyTheme(theme: LiveLikeEngagementTheme) {
        fontFamilyProvider = theme.fontFamilyProvider
        applyTheme(theme.widgets)
    }

    open fun getCurrentState(): WidgetStates? {
        return widgetViewModel?.widgetStateFlow?.value
    }

    open fun setState(widgetStates: WidgetStates) {
        val nextStateOrdinal = widgetStates.ordinal
        widgetViewModel?.widgetStateFlow?.value =
            WidgetStates.values()[min(nextStateOrdinal, WidgetStates.FINISHED.ordinal)]
    }

    open fun moveToNextState() {
        val nextStateOrdinal = (widgetViewModel?.widgetStateFlow?.value?.ordinal ?: 0) + 1
        widgetViewModel?.widgetStateFlow?.value =
            WidgetStates.values()[min(nextStateOrdinal, WidgetStates.FINISHED.ordinal)]
    }
}
