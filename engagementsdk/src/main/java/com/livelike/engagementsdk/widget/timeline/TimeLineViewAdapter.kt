package com.livelike.engagementsdk.widget.timeline

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import com.livelike.common.user
import com.livelike.engagementsdk.ContentSession
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.LiveLikeEngagementTheme
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.WidgetInfos
import com.livelike.engagementsdk.core.utils.AndroidResource
import com.livelike.engagementsdk.databinding.LivelikeProgressItemBinding
import com.livelike.engagementsdk.databinding.LivelikeTimelineItemBinding
import com.livelike.engagementsdk.widget.LiveLikeWidgetViewFactory
import com.livelike.engagementsdk.widget.WidgetProvider
import com.livelike.engagementsdk.widget.viewModel.WidgetStates
import kotlinx.coroutines.flow.MutableStateFlow
import java.lang.ref.WeakReference

internal class TimeLineViewAdapter(
    private val sdk: EngagementSDK,
    private val timeLineViewModel: WidgetTimeLineViewModel
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    init {
        setHasStableIds(true)
    }

    var widgetViewFactory: LiveLikeWidgetViewFactory? = null
    val list: ArrayList<TimelineWidgetResource> = arrayListOf()
    var isLoadingInProgress = false
    var isEndReached = false
    var liveLikeEngagementTheme: LiveLikeEngagementTheme? = null
    var widgetTimerController: WidgetTimerController? = null

    fun addAll(list: List<TimelineWidgetResource>) {
        this.list.addAll(list.filter { item -> this.list.any { it.liveLikeWidget.id == item.liveLikeWidget.id }.not() })
        notifyItemRangeInserted(itemCount - list.size, itemCount)
    }

    fun addAll(index: Int, list: List<TimelineWidgetResource>) {
        this.list.addAll(index, list.filter { item -> this.list.any { it.liveLikeWidget.id == item.liveLikeWidget.id }.not() })
        notifyItemInserted(index)
    }

    override fun onCreateViewHolder(p0: ViewGroup, viewtype: Int): RecyclerView.ViewHolder {
        return when (viewtype) {

            VIEW_TYPE_DATA -> {
                TimeLineItemViewHolder(
                    LivelikeTimelineItemBinding.inflate(LayoutInflater.from(p0.context), p0, false)
                )
            }

            VIEW_TYPE_PROGRESS -> {
                ProgressViewHolder(
                    LivelikeProgressItemBinding.inflate(LayoutInflater.from(p0.context), p0, false)
                )
            }

            else -> throw IllegalArgumentException("Different View type")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == list.size - 1 && isLoadingInProgress && !isEndReached) VIEW_TYPE_PROGRESS else VIEW_TYPE_DATA
    }

    override fun onBindViewHolder(itemViewHolder: RecyclerView.ViewHolder, p1: Int) {
        if (itemViewHolder is TimeLineItemViewHolder) {
            val timelineWidgetResource = list[p1]
            // val liveLikeWidget = timelineWidgetResource.liveLikeWidget
            liveLikeEngagementTheme?.let {
                itemViewHolder.binding.widgetView.applyTheme(it)
            }
            itemViewHolder.binding.widgetView.apply {
                enableDefaultWidgetTransition = false
                showTimer = widgetTimerController != null
                showDismissButton = false
                allowWidgetSwipeToDismiss = false
                this.widgetViewFactory = this@TimeLineViewAdapter.widgetViewFactory
                displayWidget(itemViewHolder, timelineWidgetResource)
                setState(
                    maxOf(
                        timelineWidgetResource.widgetState,
                        getCurrentState() ?: WidgetStates.READY
                    )
                )
            }
        }
    }

    private fun displayWidget(
        itemViewHolder: TimeLineItemViewHolder,
        timelineWidgetResource: TimelineWidgetResource
    ) {
        val liveLikeWidget = timelineWidgetResource.liveLikeWidget
        val widgetResourceJson =
            JsonParser.parseString(GsonBuilder().create().toJson(liveLikeWidget)).asJsonObject
        var widgetType = widgetResourceJson.get("kind").asString
        widgetType = if (widgetType.contains("follow-up")) {
            "$widgetType-updated"
        } else {
            "$widgetType-created"
        }
        val widgetId = widgetResourceJson["id"].asString
        val contextRef = WeakReference(itemViewHolder.binding.widgetView.context)
        itemViewHolder.binding.widgetView.run {
            // TODO segregate widget view and viewmodel creation
            val widgetView = WidgetProvider()
                .get(
                    null,
                    WidgetInfos(widgetType, widgetResourceJson, widgetId),
                    context,
                    sdk.analyticService,
                    sdk.sdkConfigurationOnce,
                    {
                        //widgetContainerViewModel.currentWidgetViewFlow?.value = null
                    },
                    sdk.user().currentProfileOnce,
                    null,
                    MutableStateFlow(null),
                    widgetViewThemeAttributes,
                    engagementSDKTheme,
                    (timeLineViewModel.contentSession as ContentSession).widgetInteractionRepository,
                    sdk.networkClient,
                    timeLineViewModel.contentSession.rewardItemMapCache,
                    sdk.userProfileDelegate,
                    sdk.dataStoreDelegate
                ) {
                    if(contextRef.get() != null){
                        AndroidResource.selectRandomLottieAnimation(it, contextRef)
                    } else {
                        ""
                    }
                }
            timeLineViewModel.widgetViewModelCache[widgetId]?.let {
                widgetView?.widgetViewModel = it
            }
            timeLineViewModel.widgetViewModelCache[widgetId] = widgetView?.widgetViewModel
            widgetView?.widgetViewModel?.showDismissButton = false
            widgetView?.let { view ->
//                timeLineViewModel.uiScope.launch {
                this@run.displayWidget(widgetType, view)
//                }
            }
        }
    }

    override fun getItemCount(): Int = list.size

    override fun getItemId(position: Int): Long {
        return list[position].liveLikeWidget.id.hashCode().toLong()
    }

    companion object {
        private const val VIEW_TYPE_DATA = 0
        private const val VIEW_TYPE_PROGRESS = 1 // for load more // progress view type
    }
}

class TimeLineItemViewHolder(val binding: LivelikeTimelineItemBinding) :
    RecyclerView.ViewHolder(binding.root)

class ProgressViewHolder(val binding: LivelikeProgressItemBinding) :
    RecyclerView.ViewHolder(binding.root)

data class TimelineWidgetResource(
    var widgetState: WidgetStates,
    val liveLikeWidget: LiveLikeWidget,
    var apiSource: WidgetApiSource // this has been added to show/hide animation . if real time widget animation will be shown else not
)
