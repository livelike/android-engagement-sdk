package com.livelike.engagementsdk.widget.util

import android.graphics.Typeface
import android.widget.Button
import android.widget.TextView

fun setCustomFontWithButtonStyle(
     button: Button,
     fontPath: String?

) {
    if (fontPath != null) {
        try {
            val typeface = Typeface.createFromAsset(
                button.context.assets,
                fontPath
            )
            button.typeface = typeface

        } catch (e: Exception) {
            e.printStackTrace()
            button.typeface = null
        }
    } else {
        button.typeface = null
    }
}

 fun setCustomFontWithTextStyle(
    textView: TextView,
    fontPath: String?,
    textStyle: Int
) {
    if (fontPath != null) {
        try {
            val typeFace =
                Typeface.createFromAsset(
                    textView.context.assets,
                    fontPath
                )
            textView.setTypeface(typeFace, textStyle)
        } catch (e: Exception) {
            e.printStackTrace()
            textView.typeface = null
        }
    } else {
        textView.typeface = null
    }
}