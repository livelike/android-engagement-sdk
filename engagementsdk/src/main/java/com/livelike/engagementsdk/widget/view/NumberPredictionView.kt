package com.livelike.engagementsdk.widget.view


import android.content.Context
import android.graphics.drawable.StateListDrawable
import android.text.InputType
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.R
import com.livelike.engagementsdk.core.utils.AndroidResource
import com.livelike.engagementsdk.databinding.WidgetNumberPredictionBinding
import com.livelike.engagementsdk.widget.NumberPredictionOptionsTheme
import com.livelike.engagementsdk.widget.SpecifiedWidgetView
import com.livelike.engagementsdk.widget.WidgetsTheme
import com.livelike.engagementsdk.widget.adapters.NumberPredictionOptionAdapter
import com.livelike.engagementsdk.widget.viewModel.BaseViewModel
import com.livelike.engagementsdk.widget.viewModel.NumberPredictionViewModel
import com.livelike.engagementsdk.widget.viewModel.NumberPredictionWidget
import com.livelike.engagementsdk.widget.viewModel.WidgetStates
import com.livelike.utils.parseISODateTime
import com.livelike.widget.parseDuration
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class NumberPredictionView(context: Context, attr: AttributeSet? = null) :
    SpecifiedWidgetView(context, attr), NumberPredictionOptionAdapter.EnableSubmitListener {

    private var viewModel: NumberPredictionViewModel? = null

    private var inflated = false

    private var isFirstInteraction = false

    private var isFollowUp = false

    private var binding: WidgetNumberPredictionBinding? = null

    private var optionAdapter: NumberPredictionOptionAdapter? = null

    override var dismissFunc: ((action: DismissAction) -> Unit)? = { viewModel?.dismissWidget(it) }

    override var widgetViewModel: BaseViewModel? = null
        get() = viewModel
        set(value) {
            field = value
            viewModel = value as NumberPredictionViewModel
        }

    init {
        isFirstInteraction = viewModel?.getUserInteraction() != null
    }


    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        widgetObserver(viewModel?.dataFlow?.value)
        viewModel?.widgetStateFlow?.value?.let { widgetStateObserver(it) }
        uiScope.launch {
            viewModel?.widgetStateFlow?.collect { widgetStateObserver(it) }
        }
    }

    private fun widgetStateObserver(widgetStates: WidgetStates?) {
        when (widgetStates) {
            WidgetStates.READY -> {
                lockInteraction()
            }

            WidgetStates.INTERACTING -> {
                unLockInteraction()

                // show timer while widget interaction mode
                viewModel?.dataFlow?.value?.resource?.timeout?.let { timeout ->
                    showTimer(
                        timeout, binding?.textEggTimer,
                        {
                            viewModel?.animationEggTimerProgress = it
                        },
                        {
                            viewModel?.dismissWidget(it)
                        }
                    )
                }
            }

            WidgetStates.RESULTS, WidgetStates.FINISHED -> {
                lockInteraction()
                disableLockButton()
                binding?.labelLock?.visibility = View.VISIBLE
                onWidgetInteractionCompleted()

                if (optionAdapter?.selectedUserVotes != null && optionAdapter?.selectedUserVotes!!.isNotEmpty() &&
                    optionAdapter?.selectedUserVotes!!.size == viewModel?.dataFlow?.value?.resource?.options?.size && viewModel?.numberPredictionFollowUp == false
                ) {
                    binding?.labelLock?.visibility = View.VISIBLE
                } else {
                    binding?.labelLock?.visibility = View.GONE
                }
            }

            else -> {}
        }
        if (viewModel?.enableDefaultWidgetTransition == true) {
            defaultStateTransitionManager(widgetStates)
        }
    }


    private fun widgetObserver(widget: NumberPredictionWidget?) {
        widget?.apply {
            val optionList = resource.getMergedOptions() ?: return
            if (!inflated) {
                inflated = true
                binding = WidgetNumberPredictionBinding.inflate(
                    LayoutInflater.from(context),
                    this@NumberPredictionView,
                    true
                )
                wouldInflateSponsorUi()
            }
            isFollowUp = resource.kind?.contains("follow-up") ?: false
            binding?.apply {
                titleView.title = resource.question ?: ""
                txtTitleBackground.setBackgroundResource(R.drawable.header_rounded_corner_prediciton)
                layTextRecyclerView.setBackgroundResource(R.drawable.body_rounded_corner_prediction)
                titleView.titleViewBinding.titleTextView.gravity = Gravity.START
            }
            setBodyBackgroundForSponsor()

            // added tag for identification of widget (by default will be empty)
            if (isFollowUp) {
                setTagViewWithStyleChanges(context.resources.getString(R.string.livelike_number_prediction_follow_up_tag))
                hideLockButton()
            } else {
                setTagViewWithStyleChanges(context.resources.getString(R.string.livelike_number_prediction_tag))
                showLockButton()
            }

            optionAdapter = NumberPredictionOptionAdapter(optionList, type)
            optionAdapter?.interactiveUntil = resource.interactiveUntil
            optionAdapter?.apply {
                this.submitListener = this@NumberPredictionView
            }

            /*viewModel?.adapter =
                viewModel?.adapter ?: NumberPredictionOptionAdapter(optionList, type)
            viewModel?.adapter?.interactiveUntil = resource.interactiveUntil
            viewModel?.adapter?.apply {
                this.submitListener = this@NumberPredictionView
            }*/

            disableLockButton()

            resource.interactiveUntil?.parseISODateTime()?.let {
                val epochTimeMs = it.toInstant().toEpochMilli()
                viewModel?.startInteractiveUntilTimeout(epochTimeMs)
            }
            uiScope.launch {
                viewModel?.disableInteractionFlow?.collect {
                    if (it == true) {
                        lockInteraction()
                        disableLockButton()
                    }
                }
            }

            binding?.textRecyclerView?.apply {
                /*  viewModel?.adapter?.restoreSelectedVotes(viewModel?.getUserInteraction()?.votes)
                  this.adapter = viewModel?.adapter*/
                optionAdapter?.restoreSelectedVotes(viewModel?.getUserInteraction()?.votes)
                this.adapter = optionAdapter
                setHasFixedSize(true)
            }

            binding?.predictBtn?.setOnClickListener {
                /* if (viewModel?.adapter?.selectedUserVotes!!.isEmpty() ||
                     viewModel?.adapter?.selectedUserVotes!!.size != viewModel?.adapter?.myDataset?.size
                 ) return@setOnClickListener*/

                if (optionAdapter?.selectedUserVotes!!.isEmpty() ||
                    optionAdapter?.selectedUserVotes!!.size != optionAdapter?.myDataset?.size
                ) return@setOnClickListener

                lockVote()
            }

            if (viewModel?.getUserInteraction() != null) {
                disableLockButton()
                binding?.labelLock?.visibility = View.VISIBLE

            } else if (optionAdapter?.selectedPosition != RecyclerView.NO_POSITION) {
                enableLockButton()
                binding?.labelLock?.visibility = View.GONE
            }

            widgetsTheme?.let {
                applyTheme(it)
            }

            if (isFollowUp) {
                val selectedPredictionVoteList =
                    viewModel?.getWidgetNumberPredictionVotedAnswerList(resource.imageNumberPredictionId)

                optionAdapter?.selectionLocked = true
                optionAdapter?.restoreSelectedVotes(selectedPredictionVoteList) // this sets the user selection

                /* val isUserCorrect =
                     viewModel?.isUserCorrect(selectedPredictionVoteList, viewModel?.data?.currentData?.resource?.options)*/
                val isUserCorrect =
                    viewModel?.isUserCorrect(
                        selectedPredictionVoteList,
                        viewModel?.dataFlow?.value?.resource?.options
                    )
                if (isUserCorrect != null) {
                    optionAdapter?.isCorrect = isUserCorrect
                }

                viewModel?.followupState(selectedPredictionVoteList)
            }

            setImeOptionDoneInKeyboard()

            if (widgetViewModel?.widgetStateFlow?.value == null || widgetViewModel?.widgetStateFlow?.value == WidgetStates.READY)
                widgetViewModel?.widgetStateFlow?.value = WidgetStates.READY
        }

        if (widget == null) {
            inflated = false
            removeAllViews()
            parent?.let { (it as ViewGroup).removeAllViews() }
        }
    }


    private fun defaultStateTransitionManager(widgetStates: WidgetStates?) {
        when (widgetStates) {
            WidgetStates.READY -> {
                moveToNextState()
            }

            WidgetStates.INTERACTING -> {
                viewModel?.dataFlow?.value?.let {
                    viewModel?.startDismissTimeout(
                        it.resource.timeout,
                        isFollowUp,
                        optionAdapter?.selectedUserVotes
                    )
                }
            }

            WidgetStates.RESULTS -> {
                optionAdapter?.selectionLocked = true
                viewModel?.uiScope?.launch {
                    if (isFollowUp) {
                        viewModel?.dataFlow?.value?.let {
                            delay(parseDuration(it.resource.timeout))
                            viewModel?.dismissWidget(DismissAction.TIMEOUT)
                        }
                    }
                }
            }

            WidgetStates.FINISHED -> {
                widgetObserver(null)
            }

            else -> {}
        }
    }

    private fun setBodyBackgroundForSponsor(){
        val bodyView = binding?.layTextRecyclerView ?: return
        val squareDrawable = ContextCompat.getDrawable(context, R.drawable.body_square_corner_quiz)
        val roundDrawable = ContextCompat.getDrawable(context, R.drawable.body_rounded_corner_quiz)

        if (squareDrawable != null && roundDrawable != null) {
            checkIfSponsorViewIsInflated(bodyView, squareDrawable, roundDrawable)
        }
    }

    private fun disableLockButton() {
        if (binding?.predictBtn != null) {
            binding?.predictBtn?.isEnabled = false
        }
    }


    private fun enableLockButton() {
        if (binding?.predictBtn != null) {
            binding?.predictBtn?.isEnabled = true
            binding?.labelLock?.visibility = GONE
        }
    }


    private fun hideLockButton() {
        binding?.layLock?.visibility = GONE
    }

    private fun showLockButton() {
        binding?.layLock?.visibility = VISIBLE
    }


    private fun lockInteraction() {
        //viewModel?.adapter?.selectionLocked = true
        optionAdapter?.selectionLocked = true
        viewModel?.dataFlow?.value?.let {
            if (isFollowUp && viewModel?.showTimer == true) {
                binding?.textEggTimer?.showCloseButton { viewModel?.dismissWidget(DismissAction.TIMEOUT) }
            }
        }
    }

    private fun unLockInteraction() {
        viewModel?.dataFlow?.value?.let {
            if (!isFollowUp) {
                optionAdapter?.selectionLocked = false
                //viewModel?.adapter?.selectionLocked = false
                // marked widget as interactive
                viewModel?.markAsInteractive()
            }
        }
    }

    /** changes the return key as done in keyboard */
    private fun setImeOptionDoneInKeyboard() {
        if (optionAdapter?.binding?.incrementDecrementLayout?.userInput != null) {
            optionAdapter?.binding?.incrementDecrementLayout?.userInput?.imeOptions =
                EditorInfo.IME_ACTION_DONE
            optionAdapter?.binding?.incrementDecrementLayout?.userInput?.setRawInputType(
                InputType.TYPE_CLASS_NUMBER
            )
        }
    }

    override fun onSubmitEnabled(isSubmitBtnEnabled: Boolean) {
        if (isSubmitBtnEnabled &&
            viewModel?.isInteractivityExpired(optionAdapter?.interactiveUntil) == true
        ) {
            enableLockButton()
        } else {
            disableLockButton()
        }
    }

    /** submits user's vote */
    private fun lockVote() {
        isFirstInteraction = true
        optionAdapter?.selectionLocked = true
        viewModel?.run {
            uiScope.launch {
                lockInteractionAndSubmitVote(optionAdapter?.selectedUserVotes)
            }
        }
        //viewModel?.adapter?.selectionLocked = true
        optionAdapter?.notifyDataSetChanged()

        disableLockButton()
        binding?.labelLock?.visibility = View.VISIBLE
    }


    override fun applyTheme(theme: WidgetsTheme) {
        super.applyTheme(theme)
        viewModel?.dataFlow?.value?.let { widget ->
            theme.getThemeLayoutComponent(widget.type)?.let { themeComponent ->
                if (themeComponent is NumberPredictionOptionsTheme) {
                    applyThemeOnTitleView(themeComponent)
                    applyThemeOnTagView(themeComponent)
                    AndroidResource.createDrawable(themeComponent.body)?.let {
                        binding?.layTextRecyclerView?.background = it
                    }
                    /* viewModel?.adapter?.component = themeComponent
                     viewModel?.adapter?.notifyDataSetChanged()*/

                    optionAdapter?.component = themeComponent
                    optionAdapter?.notifyDataSetChanged()

                    // submit button drawables with state
                    val submitButtonEnabledDrawable = AndroidResource.createDrawable(
                        themeComponent.submitButtonEnabled
                    )
                    val submitButtonDisabledDrawable = AndroidResource.createDrawable(
                        themeComponent.submitButtonDisabled
                    )
                    val state = StateListDrawable()
                    state.addState(
                        intArrayOf(android.R.attr.state_enabled),
                        submitButtonEnabledDrawable
                    )
                    state.addState(intArrayOf(), submitButtonDisabledDrawable)
                    binding?.predictBtn?.background = state

                    //confirmation label theme
                    AndroidResource.updateThemeForView(
                        binding?.labelLock,
                        themeComponent.confirmation,
                        fontFamilyProvider
                    )
                    if (themeComponent.confirmation?.background != null) {
                        binding?.labelLock?.background =
                            AndroidResource.createDrawable(themeComponent.confirmation)
                    }
                    themeComponent.confirmation?.padding?.let {
                        AndroidResource.setPaddingForView(
                            binding?.labelLock,
                            themeComponent.confirmation.padding
                        )
                    }
                }
            }
        }
    }

}