package com.livelike.engagementsdk.widget.timeline

import com.livelike.engagementsdk.LiveLikeContentSession
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.widget.model.GetPublishedWidgetsRequestOptions
import com.livelike.engagementsdk.widget.viewModel.BaseViewModel
import com.livelike.engagementsdk.widget.viewModel.ViewModel
import com.livelike.engagementsdk.widget.viewModel.WidgetStates
import com.livelike.utils.logDebug
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

/**
 * @contentSession: object of LiveLikeContentSession
 * predicate for filtering the widgets to only specific kind of widgets
 */
open class WidgetTimeLineViewModel(
    internal val contentSession: LiveLikeContentSession,
    viewModelDispatcher: CoroutineDispatcher = Dispatchers.Default,
    uiDispatcher: CoroutineDispatcher = Dispatchers.Main,
    private val predicate: (LiveLikeWidget) -> Boolean = { _ -> true }
) : ViewModel(viewModelDispatcher, uiDispatcher) {
    /**
     * it contains all the widgets that has been loaded onto this timeline
     **/
    val timeLineWidgets = mutableListOf<TimelineWidgetResource>()
    val timeLineWidgetsFlow =
        MutableStateFlow<Pair<WidgetApiSource, List<TimelineWidgetResource>>?>(null)

    var decideWidgetInteractivity: DecideWidgetInteractivity? = null
    internal val widgetEventFlow = MutableStateFlow<String?>(null)

    var widgetViewModelCache = mutableMapOf<String, BaseViewModel?>()

    init {
        loadPastPublishedWidgets(LiveLikePagination.FIRST)
        observeForLiveWidgets()
    }

    /**
     * load history widgets (published)
     **/
    private fun loadPastPublishedWidgets(page: LiveLikePagination) {
        widgetEventFlow.value = WIDGET_LOADING_STARTED
        contentSession.getPublishedWidgets(GetPublishedWidgetsRequestOptions(liveLikePagination = page)){result, error->
            val filteredWidgets = arrayListOf<TimelineWidgetResource>()
            result?.let { list ->
                logDebug { "Get Past Published Widgets" }
                if (list.isEmpty()) {
                    widgetEventFlow.value = WIDGET_LOADING_COMPLETE
                    return@let
                }
                val widgets = list.map {
                    TimelineWidgetResource(
                        decideWidgetInteraction(it, WidgetApiSource.HISTORY_API),
                        it, WidgetApiSource.HISTORY_API
                    )
                }
                filteredWidgets.addAll(widgets.filter { predicate(it.liveLikeWidget) })
                timeLineWidgets.addAll(filteredWidgets)
                logDebug { "timeline widget total:${timeLineWidgets.size}, filtered widget:${filteredWidgets.size}" }
                filteredWidgets.onEach {
                    logDebug { "${it.liveLikeWidget.kind}>>>${it.widgetState}" }
                }
                uiScope.launch {
                    timeLineWidgetsFlow.value =
                        Pair(WidgetApiSource.HISTORY_API, filteredWidgets)
                }
            }
            widgetEventFlow.value = WIDGET_LOADING_COMPLETE
            // this means that published result is finished, there are no more to display
            if ((result == null && error == null)) {
                logDebug { "timeline list finished" }
                widgetEventFlow.value = WIDGET_TIMELINE_END
            } else if (filteredWidgets.isEmpty()) {
                // to load more widget if the filtered widget is empty, a use case if user wants to show only a specific widget and it is not available in first page
                // so it will until it reaches end or that page that contain that specific widget
                loadPastPublishedWidgets(LiveLikePagination.NEXT)
            }
        }
        /*contentSession.getPublishedWidgets(
            GetPublishedWidgetsRequestOptions(liveLikePagination = page),
            object : LiveLikeCallback<List<LiveLikeWidget>>() {
                override fun onResponse(result: List<LiveLikeWidget>?, error: String?) {
                    val filteredWidgets = arrayListOf<TimelineWidgetResource>()
                    result?.let { list ->
                        logDebug { "Get Past Published Widgets" }
                        if (list.isEmpty()) {
                            widgetEventFlow.value = WIDGET_LOADING_COMPLETE
                            return@let
                        }
                        val widgets = list.map {
                            TimelineWidgetResource(
                                decideWidgetInteraction(it, WidgetApiSource.HISTORY_API),
                                it, WidgetApiSource.HISTORY_API
                            )
                        }
                        filteredWidgets.addAll(widgets.filter { predicate(it.liveLikeWidget) })
                        timeLineWidgets.addAll(filteredWidgets)
                        logDebug { "timeline widget total:${timeLineWidgets.size}, filtered widget:${filteredWidgets.size}" }
                        filteredWidgets.onEach {
                            logDebug { "${it.liveLikeWidget.kind}>>>${it.widgetState}" }
                        }
                        uiScope.launch {
                            timeLineWidgetsFlow.value =
                                Pair(WidgetApiSource.HISTORY_API, filteredWidgets)
                        }
                    }
                    widgetEventFlow.value = WIDGET_LOADING_COMPLETE
                    // this means that published result is finished, there are no more to display
                    if ((result == null && error == null)) {
                        logDebug { "timeline list finished" }
                        widgetEventFlow.value = WIDGET_TIMELINE_END
                    } else if (filteredWidgets.isEmpty()) {
                        // to load more widget if the filtered widget is empty, a use case if user wants to show only a specific widget and it is not available in first page
                        // so it will until it reaches end or that page that contain that specific widget
                        loadPastPublishedWidgets(LiveLikePagination.NEXT)
                    }
                }
            }
        )*/
    }

    /**
     * this call load the next available page of past published widgets on this program.
     **/
    fun loadMore() {
        if (widgetEventFlow.value?.equals(WIDGET_TIMELINE_END) == true) {
            return
        }
        if (widgetEventFlow.value?.equals(WIDGET_LOADING_COMPLETE) == true) {
            loadPastPublishedWidgets(LiveLikePagination.NEXT)
        }
    }

    /**
     * observe the live (real time) widgets
     **/
    private fun observeForLiveWidgets() {
        uiScope.launch {
            contentSession.widgetFlow.collect {
                onNewWidgetInternal(it)
            }
        }
    }

    fun addNewWidgetFromPolling(newWidget: LiveLikeWidget) {
        onNewWidgetInternal(newWidget)
    }
    private fun onNewWidgetInternal(newWidget: LiveLikeWidget) {
        val widget = TimelineWidgetResource(
            decideWidgetInteraction(newWidget, WidgetApiSource.REALTIME_API),
            newWidget,
            WidgetApiSource.REALTIME_API
        )
        if (predicate(widget.liveLikeWidget)) {
            timeLineWidgets.add(0, widget)
            uiScope.launch {
                timeLineWidgetsFlow.value =
                    Pair(WidgetApiSource.REALTIME_API, listOf(widget))
            }
        }
    }

    fun wouldAllowWidgetInteraction(liveLikeWidget: LiveLikeWidget): Boolean {
        return timeLineWidgets.find { it.liveLikeWidget.id == liveLikeWidget.id }?.widgetState == WidgetStates.INTERACTING
    }

    open fun decideWidgetInteraction(
        liveLikeWidget: LiveLikeWidget,
        timeLineWidgetApiSource: WidgetApiSource
    ): WidgetStates {
        val isInteractive = if (decideWidgetInteractivity != null) {
            decideWidgetInteractivity?.wouldAllowWidgetInteraction(liveLikeWidget) ?: false
        } else {
            timeLineWidgetApiSource == WidgetApiSource.REALTIME_API
        }
        logDebug { "Allow Interaction:$isInteractive, Kind:${liveLikeWidget.kind}, apiSource:${timeLineWidgetApiSource}, interactivity:$decideWidgetInteractivity" }
        return if (isInteractive) WidgetStates.INTERACTING else WidgetStates.RESULTS
    }

    /**
     * Call this method to close down all connections and scopes, it should be called from onClear() method
     * of android viewmodel. In case landscape not supported then onDestroy.
     */
    fun clear() {
        uiScope.cancel()
        for (entry in widgetViewModelCache) {
            entry.value?.onClear()
        }
        widgetViewModelCache.clear()
    }

    /**
     * used for timeline widget loading starting / completed
     **/
    companion object {
        const val WIDGET_LOADING_COMPLETE = "loading-complete"
        const val WIDGET_LOADING_STARTED = "loading-started"
        const val WIDGET_TIMELINE_END = "timeline-reached"
    }
}

// Timeline view will have default implementation
interface DecideWidgetInteractivity {
    //    TODO discuss with team if there is a requirement to add TimeLineWidgetApiSource as param in this function
    fun wouldAllowWidgetInteraction(widget: LiveLikeWidget): Boolean
}

enum class WidgetApiSource {
    REALTIME_API,
    HISTORY_API
}
