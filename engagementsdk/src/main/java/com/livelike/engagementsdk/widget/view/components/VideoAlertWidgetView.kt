package com.livelike.engagementsdk.widget.view.components

import android.content.Context
import android.content.Intent
import android.graphics.Outline
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.R
import com.livelike.engagementsdk.core.utils.AndroidResource
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.livelike.engagementsdk.databinding.VideoWidgetBinding
import com.livelike.engagementsdk.widget.SpecifiedWidgetView
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.WidgetsTheme
import com.livelike.engagementsdk.widget.model.Alert
import com.livelike.engagementsdk.widget.viewModel.BaseViewModel
import com.livelike.engagementsdk.widget.viewModel.VideoWidgetViewModel
import com.livelike.engagementsdk.widget.viewModel.WidgetStates
import kotlinx.coroutines.launch


internal class VideoAlertWidgetView : SpecifiedWidgetView {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    private var inflated = false
    private var binding: VideoWidgetBinding? = null
    var viewModel: VideoWidgetViewModel? = null
    private var mediaPlayer: MediaPlayer? = null
    private var isMuted: Boolean = false
    private var playedAtLeastOnce: Boolean = false
    private var stopPosition: Int = 0

    override var dismissFunc: ((action: DismissAction) -> Unit)? =
        {
            viewModel?.dismissWidget(it)
            removeAllViews()
        }

    override var widgetViewModel: BaseViewModel? = null
        set(value) {
            field = value
            viewModel = value as VideoWidgetViewModel
        }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        uiScope.launch {
            launch {
                viewModel?.dataFlow?.collect {
                    logDebug { "showing the Video WidgetView" }
                    it?.let { inflate(context, it) }
                }
            }
            launch {
                viewModel?.widgetStateFlow?.collect { widgetStates ->
                    logDebug { "Current State: $widgetStates" }
                    widgetStates?.let {
                        if (widgetStates == WidgetStates.INTERACTING) {
                            // will only be fired if link is available in alert widget
                            viewModel?.markAsInteractive()
                        }
                        if (viewModel?.enableDefaultWidgetTransition == true) {
                            defaultStateTransitionManager(widgetStates)
                        }
                    }
                }
            }
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        release()
    }

    override fun moveToNextState() {
        super.moveToNextState()
        if (widgetViewModel?.widgetStateFlow?.value == WidgetStates.INTERACTING) {
            widgetViewModel?.widgetStateFlow?.value = WidgetStates.FINISHED
        } else {
            super.moveToNextState()
        }
    }

    private fun inflate(context: Context, resourceAlert: Alert) {
        if (!inflated) {
            inflated = true
            binding = VideoWidgetBinding.inflate(
                LayoutInflater.from(context),
                this@VideoAlertWidgetView,
                true
            )
        }
        binding?.apply {
            bodyText.text = resourceAlert.text
            labelText.text = resourceAlert.title
            linkText.text = resourceAlert.link_label
            soundView.visibility = View.GONE
            playbackErrorView.visibility = View.GONE
        }

        if (!resourceAlert.videoUrl.isNullOrEmpty()) {
            setFrameThumbnail(resourceAlert.videoUrl)
        }

        if (!resourceAlert.link_url.isNullOrEmpty()) {
            binding?.linkBackground?.setOnClickListener {
                openBrowser(context, resourceAlert.link_url)
            }
        } else {
            binding?.apply {
                linkArrow.visibility = View.GONE
                linkBackground.visibility = View.GONE
                linkText.visibility = View.GONE
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                setPlayerViewCornersRound(isOnlyBottomCornersToBeRounded = true)
            }
        }

        if (resourceAlert.title.isNullOrEmpty()) {
            binding?.labelText?.visibility = GONE
            binding?.widgetContainer?.setBackgroundResource(R.drawable.video_alert_all_rounded_corner)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                setPlayerViewCornersRound(isOnlyBottomCornersToBeRounded = false)
            }
            val params = binding?.widgetContainer?.layoutParams as ConstraintLayout.LayoutParams
            params.topMargin = AndroidResource.dpToPx(0)
            binding?.widgetContainer?.requestLayout()
        } else {
            binding?.widgetContainer?.setBackgroundResource(R.drawable.video_alert_rounded_corner_black_background)
            val params = binding?.widgetContainer?.layoutParams as ConstraintLayout.LayoutParams
            binding?.widgetContainer?.apply {
                layoutParams = params
                requestLayout()
            }
        }

        if (!resourceAlert.text.isNullOrEmpty()) {
            binding?.bodyText?.apply {
                visibility = View.VISIBLE
                text = resourceAlert.text
            }
        } else {
            binding?.bodyText?.visibility = View.GONE
        }

        setOnClickListeners()

        widgetsTheme?.let {
            applyTheme(it)
        }
    }

    override fun applyTheme(theme: WidgetsTheme) {
        super.applyTheme(theme)
        viewModel?.dataFlow?.value?.let { _ ->
            theme.getThemeLayoutComponent(WidgetType.VIDEO_ALERT)?.let { themeComponent ->
                AndroidResource.updateThemeForView(
                    binding?.labelText,
                    themeComponent.title,
                    fontFamilyProvider
                )
                if (themeComponent.header?.background != null) {
                    binding?.labelText?.background =
                        AndroidResource.createDrawable(themeComponent.header)
                }
                themeComponent.header?.padding?.let {
                    AndroidResource.setPaddingForView(
                        binding?.labelText,
                        themeComponent.header.padding
                    )
                }

                binding?.widgetContainer?.background =
                    AndroidResource.createDrawable(themeComponent.body)

                AndroidResource.updateThemeForView(
                    binding?.bodyText,
                    themeComponent.body,
                    fontFamilyProvider
                )

                if (themeComponent.footer?.background != null) {
                    binding?.linkBackground?.background =
                        AndroidResource.createDrawable(themeComponent.footer)
                }

                AndroidResource.updateThemeForView(
                    binding?.linkText,
                    themeComponent.footer,
                    fontFamilyProvider
                )
            }
        }
    }

    /** sets the listeners */
    private fun setOnClickListeners() {
        binding?.soundView?.setOnClickListener {
            if (isMuted) {
                unMute()
            } else {
                mute()
            }
        }

        binding?.widgetContainer?.setOnClickListener {
            if (binding?.playerView!!.isPlaying) {
                pause()
            } else {
                if (stopPosition > 0) { // already running
                    resume()
                } else {
                    play()
                }
            }
        }
    }

    /** sets the video view */
    private fun initializePlayer(videoUrl: String) {
        try {
            val uri = Uri.parse(videoUrl)
            binding?.apply {
                playerView.setVideoURI(uri)
                //playerView.seekTo(stopPosition)
                playerView.requestFocus()
                playerView.start()
            }
            unMute()

            // perform set on prepared listener event on video view
            try {
                binding?.playerView?.setOnPreparedListener { mp ->
                    // do something when video is ready to play
                    this.mediaPlayer = mp
                    playedAtLeastOnce = true
                    binding?.apply {
                        progressBar.visibility = View.GONE
                        playbackErrorView.visibility = View.GONE
                        soundView.visibility = VISIBLE
                        icSound.visibility = VISIBLE
                    }
                }

                binding?.playerView?.setOnCompletionListener {
                    binding?.playerView?.stopPlayback()
                    binding?.soundView?.visibility = GONE
                    setFrameThumbnail(videoUrl)
                }

                binding?.playerView?.setOnErrorListener { _, _, _ ->
                    logError { "Error on playback" }
                    binding?.apply {
                        progressBar.visibility = GONE
                        icPlay.visibility = GONE
                        playerView.visibility = INVISIBLE
                        playbackErrorView.visibility = VISIBLE
                        soundView.visibility = GONE
                    }
                    true
                }
            } catch (e: Exception) {
                binding?.progressBar?.visibility = GONE
                binding?.playbackErrorView?.visibility = VISIBLE
                e.printStackTrace()
            }
        } catch (e: Exception) {
            binding?.progressBar?.visibility = GONE
            binding?.playbackErrorView?.visibility = VISIBLE
            e.printStackTrace()
        }
    }

    /** responsible for playing the video */
    private fun play() {
        binding?.apply {
            progressBar.visibility = View.VISIBLE
            icPlay.visibility = View.GONE
            playbackErrorView.visibility = View.GONE
            thumbnailView.visibility = View.GONE
            playerView.visibility = View.VISIBLE
        }
        viewModel?.registerPlayStarted()
        viewModel?.dataFlow?.value?.videoUrl?.let { initializePlayer(it) }
    }

    /** responsible for resuming the video from where it was stopped */
    private fun resume() {
        binding?.apply {
            soundView.visibility = VISIBLE
            playbackErrorView.visibility = GONE
            progressBar.visibility = GONE
            icPlay.visibility = GONE
            playerView.seekTo(stopPosition)
        }
        if (binding?.playerView?.currentPosition == 0) {
            play()
        } else {
            binding?.playerView?.start()
        }
    }

    /** responsible for stopping the video */
    private fun pause() {
        binding?.apply {
            stopPosition = playerView.currentPosition
            playerView.pause()
            soundView.visibility = GONE
            icPlay.visibility = View.VISIBLE
            playbackErrorView.visibility = View.GONE
            icPlay.setImageResource(R.drawable.ic_play_button)
        }
    }

    /** responsible for stopping the player and releasing it */
    private fun release() {
        try {
            playedAtLeastOnce = false
            if (binding?.playerView != null && binding?.playerView!!.isPlaying) {
                binding?.playerView?.stopPlayback()
                binding?.playerView?.seekTo(0)
                stopPosition = 0
                mediaPlayer?.stop()
                mediaPlayer?.release()
                mediaPlayer = null
            }
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }
    }

    /** checks if the player is paused */
    fun isPaused(): Boolean {
        return !mediaPlayer!!.isPlaying && playedAtLeastOnce
    }

    /** mutes the video */
    private fun mute() {
        try {
            isMuted = true
            mediaPlayer?.setVolume(0f, 0f)
            binding?.icSound?.setImageResource(R.drawable.ic_volume_on)
            binding?.muteTv?.text = context.resources.getString(R.string.livelike_unmute_label)
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }
    }

    /** unmute the video */
    private fun unMute() {
        try {
            isMuted = false
            mediaPlayer?.setVolume(1f, 1f)
            binding?.icSound?.setImageResource(R.drawable.ic_volume_off)
            binding?.muteTv?.text = context.resources.getString(R.string.livelike_mute_label)
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }
    }

    /** extract thumbnail from the video url */
    private fun setFrameThumbnail(videoUrl: String) {
        binding?.apply {
            thumbnailView.visibility = VISIBLE
            icPlay.visibility = VISIBLE
            progressBar.visibility = GONE
            playbackErrorView.visibility = GONE
            icPlay.setImageResource(R.drawable.ic_play_button)
            playerView.visibility = INVISIBLE
        }
        var requestOptions = RequestOptions()

        if (videoUrl.isNotEmpty()) {
            requestOptions = if (viewModel?.dataFlow?.value?.title.isNullOrEmpty()) {
                requestOptions.transform(CenterCrop(), GranularRoundedCorners(16f, 16f, 16f, 16f))
            } else {
                requestOptions.transform(CenterCrop(), GranularRoundedCorners(0f, 0f, 16f, 16f))
            }
            Glide.with(context.applicationContext)
                .asBitmap()
                .load(videoUrl)
                .apply(requestOptions)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .thumbnail(0.1f)
                .into(binding?.thumbnailView!!)
        }
    }

    private fun defaultStateTransitionManager(widgetStates: WidgetStates?) {
        when (widgetStates) {
            WidgetStates.READY -> {
                viewModel?.widgetStateFlow?.value = WidgetStates.INTERACTING
            }
            WidgetStates.INTERACTING -> {
                viewModel?.dataFlow?.value?.let {
                    viewModel?.startDismissTimeout(it.timeout) {
                        viewModel?.widgetStateFlow?.value = WidgetStates.FINISHED
                    }
                }
            }
            WidgetStates.FINISHED -> {
                removeAllViews()
                parent?.let { (it as ViewGroup).removeAllViews() }
            }
            WidgetStates.RESULTS -> {
                // not required
            }
            null -> {
                //not required
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setPlayerViewCornersRound(isOnlyBottomCornersToBeRounded: Boolean) {
        binding?.playerView?.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                val corner = 20f
                if (isOnlyBottomCornersToBeRounded) {
                    outline.setRoundRect(0, -corner.toInt(), view.width, view.height, corner)
                } else {
                    outline.setRoundRect(
                        0,
                        0,
                        view.width,
                        view.height,
                        corner
                    ) // for making all corners rounded
                }
            }
        }

        binding?.playerView?.clipToOutline = true
    }

    private fun openBrowser(context: Context, linkUrl: String) {
        viewModel?.onVideoAlertClickLink(linkUrl)
        val universalLinkIntent =
            Intent(Intent.ACTION_VIEW, Uri.parse(linkUrl)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        if (universalLinkIntent.resolveActivity(context.packageManager) != null) {
            ContextCompat.startActivity(context, universalLinkIntent, Bundle.EMPTY)
        }
    }
}
