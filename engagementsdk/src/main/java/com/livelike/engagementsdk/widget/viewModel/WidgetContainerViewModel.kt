package com.livelike.engagementsdk.widget.viewModel

import android.animation.LayoutTransition
import android.annotation.SuppressLint
import android.os.Build
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.gson.JsonPrimitive
import com.livelike.common.DataStoreDelegate
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.*
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.core.services.messaging.proxies.WidgetLifeCycleEventsListener
import com.livelike.engagementsdk.core.utils.AndroidResource
import com.livelike.engagementsdk.widget.*
import com.livelike.engagementsdk.widget.data.respository.WidgetInteractionRepository
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.repository.ProgramRepository
import com.livelike.engagementsdk.widget.util.SwipeDismissTouchListener
import com.livelike.engagementsdk.widget.utils.toAnalyticsString
import com.livelike.engagementsdk.widget.widgetModel.*
import com.livelike.network.NetworkApiClient
import com.livelike.utils.Once
import com.livelike.utils.logDebug
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import java.lang.ref.WeakReference

// TODO remove view references from this view model, also clean content session for same.

internal class WidgetContainerViewModel(var currentWidgetViewFlow: MutableStateFlow<Pair<String, WidgetInfos>?>?) {

    internal var networkApiClient: NetworkApiClient? = null
    private lateinit var currentWidgetId: String
    private lateinit var currentWidgetType: String
    var enableDefaultWidgetTransition: Boolean = true
    var allowWidgetSwipeToDismiss: Boolean = true
        set(value) {
            field = value
            if (value) {
                widgetContainerFrameLayout?.setOnTouchListener(swipeDismissTouchListener)
            } else {
                widgetContainerFrameLayout?.setOnTouchListener(null)
            }
        }

    var showTimer: Boolean = true
    internal var showDismissButton: Boolean = true
    internal var configurationOnce: Once<SdkConfiguration>? = null
    var session: LiveLikeContentSession? = null
        set(value) {
            field = value
//            if (value != null && session is ContentSession) {
            analyticsService = (value as? ContentSession)?.analyticService
            configurationOnce = (value as? ContentSession)?.configurationOnce
            currentWidgetViewFlow = (value as? ContentSession)?.currentWidgetViewFlow
            isLayoutTransitionEnabled = (value as? ContentSession)?.isLayoutTransitionEnabled
            sessionScope = (value as? ContentSession)?.sessionScope
            rewardItemMapCache = (value as? ContentSession)?.rewardItemMapCache ?: mutableMapOf()
            animationEventsFlow = (value as? ContentSession)?.animationEventsFlow
            dataStoreDelegate = (value as? ContentSession)?.dataStoreDelegate
            programRepository = (value as? ContentSession)?.programRepository
            currentProfileOnce = (value as? ContentSession)?.currentProfileOnce
            userProfileRewardDelegate = (value as? ContentSession)?.userProfileRewardDelegate
            widgetInteractionRepository = (value as? ContentSession)?.widgetInteractionRepository
            networkApiClient = (value as? ContentSession)?.networkApiClient
//            }
        }
    var widgetLifeCycleEventsListener: WidgetLifeCycleEventsListener? = null
    internal var widgetViewThemeAttributes: WidgetViewThemeAttributes = WidgetViewThemeAttributes()
    private var dismissWidget: ((action: DismissAction) -> Unit)? = null
    private var widgetContainerFrameLayout: FrameLayout? = null
    var analyticsService: AnalyticsService? = null
    private lateinit var programId: String

    // Swipe to dismiss
    private var swipeDismissTouchListener: View.OnTouchListener? = null
    private val uiScope = MainScope()
    var widgetViewViewFactory: LiveLikeWidgetViewFactory? = null
    var isLayoutTransitionEnabled: Boolean? = null
    internal var currentProfileOnce: Once<LiveLikeProfile>? = null
    var programRepository: ProgramRepository? = null
    var animationEventsFlow: MutableStateFlow<ViewAnimationEvents?>? = MutableStateFlow(null)

    // var widgetThemeAttributes: WidgetViewThemeAttributes? = null
    var liveLikeThemeFlow = MutableStateFlow<LiveLikeEngagementTheme?>(null)
    var widgetInteractionRepository: WidgetInteractionRepository? = null
    var sessionScope: CoroutineScope? = null
    var rewardItemMapCache: MutableMap<String, RewardItem> = mutableMapOf()
    var userProfileRewardDelegate: UserProfileDelegate? = null
    var dataStoreDelegate: DataStoreDelegate? = null
    var currentWidgetViewFlowJob: Job? = null

    @SuppressLint("ClickableViewAccessibility")
    fun setWidgetContainer(
        widgetContainer: FrameLayout, widgetViewThemeAttributes: WidgetViewThemeAttributes
    ) {
        this.widgetContainerFrameLayout = widgetContainer
        this.widgetViewThemeAttributes = widgetViewThemeAttributes
        swipeDismissTouchListener = SwipeDismissTouchListener(widgetContainer,
            null,
            object : SwipeDismissTouchListener.DismissCallbacks {
                override fun canDismiss(token: Any?): Boolean {
                    return true
                }

                override fun onDismiss(view: View?, token: Any?) {
                    logDebug { "onDismiss With Swipe Called" }
                    if (currentWidgetViewFlow?.value != null) {
                        currentWidgetViewFlow!!.value = null
                    }
                    if (dismissWidget == null && this@WidgetContainerViewModel::currentWidgetType.isInitialized) {
                        analyticsService?.trackWidgetDismiss(
                            currentWidgetType,
                            currentWidgetId,
                            programId,
                            null,
                            null,
                            DismissAction.SWIPE
                        )
                    } else {
                        dismissWidget?.invoke(DismissAction.SWIPE)
                    }

                    dismissWidget = null
                    removeViews()
                }
            })
        if (allowWidgetSwipeToDismiss) {
            widgetContainer.setOnTouchListener(swipeDismissTouchListener)
        }
        currentWidgetViewFlowJob?.cancel()
        logDebug { "setWidget Container: $currentWidgetViewFlow" }
        if (currentWidgetViewFlow != null) {
            currentWidgetViewFlowJob = uiScope.launch {
                currentWidgetViewFlow!!.collect { pair ->
                    logDebug { "Received to show Widget: $currentWidgetViewFlowJob" }
                    if (pair != null) {
                        logDebug { "Received to show Widget: ${pair.first}, ${pair.second.payload}" }
                        val specifiedWidgetView = WidgetProvider().get(
                            null,
                            pair.second,
                            widgetContainer.context,
                            analyticsService!!,
                            configurationOnce!!,
                            {
                                pair.second.onDismiss?.invoke()
                            },
                            currentProfileOnce!!,
                            programRepository,
                            animationEventsFlow!!,
                            widgetViewThemeAttributes, //?: WidgetViewThemeAttributes(),
                            liveLikeThemeFlow.value,
                            widgetInteractionRepository = widgetInteractionRepository,
                            networkApiClient = networkApiClient!!,
                            rewardItemMapCache,
                            userProfileRewardDelegate,
                            dataStoreDelegate!!
                        ) {
                            val weakContext = WeakReference(widgetContainer.context)
                            AndroidResource.selectRandomLottieAnimation(
                                it, weakContext
                            )
                        }
                        widgetObserver(specifiedWidgetView, pair.first)
                    } else {
                        logDebug { "Widget Pair is NULL" }
                        removeViews()
                    }
                }
            }
        }

        uiScope.launch {
            currentWidgetViewFlow?.collect {
                logDebug { "Recieved widget in widget Container ViewModel" }
            }
        }
        // Show / Hide animation
        // changes because of ES-1572
        if (isLayoutTransitionEnabled!!) {
            widgetContainer.layoutTransition = LayoutTransition()
        }
    }

    internal fun widgetObserver(widgetView: SpecifiedWidgetView?, widgetType: String?) {
        logDebug { "widget Observer first removing old views before adding the new view" }
        removeViews()
        var customView: View? = null

        val fromString = WidgetType.fromString(widgetType!!) // [CAF] Evaluate use of !! here
        if (fromString == WidgetType.TEXT_PREDICTION_FOLLOW_UP || fromString == WidgetType.IMAGE_PREDICTION_FOLLOW_UP) {
            customView = widgetViewViewFactory?.createPredictionFollowupWidgetView(
                widgetView?.widgetViewModel as FollowUpWidgetViewModel,
                fromString == WidgetType.IMAGE_PREDICTION_FOLLOW_UP
            )
        } else if (fromString == WidgetType.TEXT_NUMBER_PREDICTION_FOLLOW_UP || fromString == WidgetType.IMAGE_NUMBER_PREDICTION_FOLLOW_UP) {
            customView = widgetViewViewFactory?.createNumberPredictionFollowupWidgetView(
                widgetView?.widgetViewModel as NumberPredictionFollowUpWidgetModel,
                fromString == WidgetType.IMAGE_NUMBER_PREDICTION_FOLLOW_UP
            )

        }

        if (customView == null) {
            when (widgetView?.widgetViewModel) {
                is CheerMeterWidgetmodel -> {
                    customView =
                        widgetViewViewFactory?.createCheerMeterView(widgetView.widgetViewModel as CheerMeterWidgetmodel)
                }

                is AlertWidgetModel -> {
                    customView =
                        widgetViewViewFactory?.createAlertWidgetView(widgetView.widgetViewModel as AlertWidgetModel)
                }

                is QuizWidgetModel -> {
                    customView = widgetViewViewFactory?.createQuizWidgetView(
                        widgetView.widgetViewModel as QuizWidgetModel,
                        fromString == WidgetType.IMAGE_QUIZ
                    )
                }

                is PredictionWidgetViewModel -> {
                    customView = widgetViewViewFactory?.createPredictionWidgetView(
                        widgetView.widgetViewModel as PredictionWidgetViewModel,
                        fromString == WidgetType.IMAGE_PREDICTION
                    )
                }

                is PollWidgetModel -> {
                    customView = widgetViewViewFactory?.createPollWidgetView(
                        widgetView.widgetViewModel as PollWidgetModel,
                        fromString == WidgetType.IMAGE_POLL
                    )
                }

                is ImageSliderWidgetModel -> {
                    customView = widgetViewViewFactory?.createImageSliderWidgetView(
                        widgetView.widgetViewModel as ImageSliderWidgetModel
                    )
                }

                is VideoAlertWidgetModel -> {
                    customView = widgetViewViewFactory?.createVideoAlertWidgetView(
                        widgetView.widgetViewModel as VideoAlertWidgetModel
                    )
                }

                is TextAskWidgetModel -> {
                    customView = widgetViewViewFactory?.createTextAskWidgetView(
                        widgetView.widgetViewModel as TextAskWidgetModel
                    )
                }

                is NumberPredictionWidgetModel -> {
                    customView = widgetViewViewFactory?.createNumberPredictionWidgetView(
                        widgetView.widgetViewModel as NumberPredictionWidgetModel,
                        fromString == WidgetType.IMAGE_NUMBER_PREDICTION
                    )
                }

                is SocialEmbedWidgetModel -> {
                    customView = widgetViewViewFactory?.createSocialEmbedWidgetView(
                        widgetView.widgetViewModel as SocialEmbedWidgetModel
                    )
                }
            }
        }
        if (customView != null) {
            displayWidget(customView)
        } else if (widgetView != null) {
            widgetView.widgetViewModel?.enableDefaultWidgetTransition =
                enableDefaultWidgetTransition
            widgetView.widgetViewModel?.showTimer = showTimer
            widgetView.widgetViewModel?.showDismissButton = showDismissButton
            displayWidget(widgetView)
        }
        if (widgetContainerFrameLayout != null) {
            widgetView?.widgetId?.let { widgetId ->
                var linkUrl: String? = null

                if (widgetView.widgetInfos.payload.get("link_url") is JsonPrimitive) {
                    linkUrl = widgetView.widgetInfos.payload.get("link_url")?.asString
                }

                if (widgetView.widgetInfos.payload.get("program_id") is JsonPrimitive) {
                    programId = widgetView.widgetInfos.payload.get("program_id").asString
                }

                currentWidgetType = WidgetType.fromString(
                    widgetType
                )?.toAnalyticsString() ?: ""
                currentWidgetId = widgetId
                analyticsService?.trackWidgetDisplayed(
                    currentWidgetType, widgetId, programId, linkUrl
                )
            }
        }
    }

    private fun displayWidget(view: View) {
        if (view is SpecifiedWidgetView) {
            dismissWidget = view.dismissFunc
            view.widgetViewThemeAttributes.apply {
                widgetWinAnimation = widgetViewThemeAttributes.widgetWinAnimation
                widgetLoseAnimation = widgetViewThemeAttributes.widgetLoseAnimation
                widgetDrawAnimation = widgetViewThemeAttributes.widgetDrawAnimation
            }
            view.widgetLifeCycleEventsListener = widgetLifeCycleEventsListener
            logDebug { "NOW - Show WidgetInfos" }
        }

        (view.parent as ViewGroup?)?.removeAllViews() // Clean the view parent in case of reuse
        widgetContainerFrameLayout?.addView(view)
    }

    internal fun removeViews() {
        logDebug { "NOW - Dismiss WidgetInfos" }
        widgetContainerFrameLayout?.removeAllViews()
        widgetContainerFrameLayout?.apply {
            if (if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    !isInLayout
                } else {
                    true
                }
            ) requestLayout()
        }
    }
}
