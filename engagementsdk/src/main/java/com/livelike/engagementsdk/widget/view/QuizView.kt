package com.livelike.engagementsdk.widget.view

import android.animation.Animator
import android.content.Context
import android.graphics.drawable.Drawable
import android.graphics.drawable.StateListDrawable
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.R
import com.livelike.engagementsdk.core.utils.AndroidResource
import com.livelike.engagementsdk.databinding.WidgetTextOptionSelectionBinding
import com.livelike.engagementsdk.widget.OptionsWidgetThemeComponent
import com.livelike.engagementsdk.widget.SpecifiedWidgetView
import com.livelike.engagementsdk.widget.WidgetsTheme
import com.livelike.engagementsdk.widget.adapters.WidgetOptionsViewAdapter
import com.livelike.engagementsdk.widget.model.Resource
import com.livelike.engagementsdk.widget.util.setCustomFontWithButtonStyle
import com.livelike.engagementsdk.widget.util.setCustomFontWithTextStyle
import com.livelike.engagementsdk.widget.viewModel.BaseViewModel
import com.livelike.engagementsdk.widget.viewModel.QuizViewModel
import com.livelike.engagementsdk.widget.viewModel.QuizWidget
import com.livelike.engagementsdk.widget.viewModel.WidgetStates
import com.livelike.utils.logDebug
import com.livelike.utils.parseISODateTime
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.ref.WeakReference


class QuizView(context: Context, attr: AttributeSet? = null) : SpecifiedWidgetView(context, attr) {

    private var viewModel: QuizViewModel? = null

    override var dismissFunc: ((action: DismissAction) -> Unit)? = { viewModel?.dismissWidget(it) }

    override var widgetViewModel: BaseViewModel? = null
        set(value) {
            field = value
            viewModel = value as QuizViewModel
        }

    private var isFirstInteraction = false
    private var binding: WidgetTextOptionSelectionBinding? = null
    private var adapter: WidgetOptionsViewAdapter? = null

    init {
        isFirstInteraction = viewModel?.getUserInteraction() != null
    }

    // Refresh the view when re-attached to the activity
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        resourceObserver(viewModel?.dataFlow?.value)
        viewModel?.widgetStateFlow?.value?.let { stateWidgetObserver(it) }
        uiScope.launch {
            /*launch { viewModel?.dataFlow?.collect { resourceObserver(it) } }*/
            launch { viewModel?.widgetStateFlow?.collect { stateWidgetObserver(it) } }
            launch { viewModel?.currentVoteIdFlow?.collect { onClickObserver() } }
        }
        // viewModel?.results?.subscribe(this.hashCode()) { resultsObserver(it) }
    }

    private fun stateWidgetObserver(widgetStates: WidgetStates?) {
        when (widgetStates) {
            WidgetStates.READY -> {
                lockInteraction()
            }
            WidgetStates.INTERACTING -> {
                unLockInteraction()
                //showResultAnimation = true
                showResultAnimation = context.resources.getBoolean(R.bool.livelike_widget_show_animation)
                // show timer while widget interaction mode
                viewModel?.dataFlow?.value?.resource?.timeout?.let { timeout ->
                    showTimer(
                        timeout, binding?.textEggTimer,
                        {
                            viewModel?.animationEggTimerProgress = it
                        },
                        {
                            viewModel?.dismissWidget(it)
                        }
                    )
                }
                binding?.layLock?.layLock?.visibility = View.VISIBLE
            }
            WidgetStates.RESULTS, WidgetStates.FINISHED -> {
                lockInteraction()
                onWidgetInteractionCompleted()
                disableLockButton()
                   val labelVisible =
                    context.resources.getBoolean(R.bool.livelike_widget_show_label_lock)
                if(labelVisible){
                    binding?.layLock?.labelLock?.visibility = View.VISIBLE
                }else{
                    binding?.layLock?.labelLock?.visibility = View.GONE
                }
                uiScope.launch {
                    viewModel?.resultsFlow?.collect {
                        if (isFirstInteraction) {
                            resultsObserver(viewModel?.resultsFlow?.value)
                        }
                    }
                }

                if (isFirstInteraction) {
                    viewModel?.apply {
                        val isUserCorrect =
                            adapter?.selectedPositionFlow?.value?.let {
                                if (it > -1) {
                                    return@let adapter?.myDataset?.get(it)?.isCorrect
                                }
                                return@let false
                            }
                                ?: false
                        val rootPath =
                            if (isUserCorrect) widgetViewThemeAttributes.widgetWinAnimation else widgetViewThemeAttributes.widgetLoseAnimation

                        val weakContext = WeakReference(context)
                        animationPath =
                            AndroidResource.selectRandomLottieAnimation(rootPath, weakContext) ?: ""
                    }

//                    viewModel?.adapter?.correctOptionId = viewModel?.adapter?.myDataset?.find { it.is_correct }?.id ?: ""
                    adapter?.correctOptionId =
                        adapter?.myDataset?.find { it.isCorrect ?: false }?.id ?: ""
//                    viewModel?.adapter?.userSelectedOptionId = viewModel?.adapter?.selectedPosition?.let { it1 ->
                    adapter?.userSelectedOptionIdFlow?.value =
                        adapter?.selectedPositionFlow?.value?.let { it1 ->
                            if (it1 > -1)
//                                return@let viewModel?.adapter?.myDataset?.get(it1)?.id
                                return@let adapter?.myDataset?.get(it1)?.id
                            return@let null
                        } ?: ""

//                    binding?.textRecyclerView?.swapAdapter(viewModel?.adapter, false)
                    binding?.textRecyclerView?.swapAdapter(adapter, false)
                    binding?.textRecyclerView?.adapter?.notifyItemChanged(0)
                }

                binding?.followupAnimation?.apply {
                    if (isFirstInteraction) {
                        setAnimation(viewModel?.animationPath)
                        progress = viewModel?.animationProgress ?: 0f
                        addAnimatorUpdateListener { valueAnimator ->
                            viewModel?.animationProgress = valueAnimator.animatedFraction
                        }
                        if (progress != 1f) {
                            resumeAnimation()
                        }
                        visibility = if (showResultAnimation) {
                            View.VISIBLE
                        } else {
                            View.GONE
                        }
                    } else {
                        visibility = View.GONE
                    }
                }

                viewModel?.points?.let {
                    if (viewModel?.dataStoreDelegate?.shouldShowPointTutorial() != true && it > 0) {
                        binding?.pointView?.startAnimation(it, true)
                        wouldShowProgressionMeter(
                            viewModel?.rewardsType,
                            viewModel?.gamificationProfile?.value,
                            binding?.progressionMeterView!!
                        )
                    }
                }
            }
            else -> {}
        }
        if (viewModel?.enableDefaultWidgetTransition == true) {
            defaultStateTransitionManager(widgetStates)
        }
    }

    private var inflated = false

    private fun onClickObserver() {
        viewModel?.onOptionClicked()
    }

    override fun applyTheme(theme: WidgetsTheme) {
        super.applyTheme(theme)
        viewModel?.dataFlow?.value?.let { widget ->
            theme.getThemeLayoutComponent(widget.type)?.let { themeComponent ->
                if (themeComponent is OptionsWidgetThemeComponent) {
                    applyThemeOnTitleView(themeComponent)
                    applyThemeOnTagView(themeComponent)
                    adapter?.component = themeComponent
                    adapter?.notifyDataSetChanged()
                    AndroidResource.createDrawable(themeComponent.body)?.let {
                        binding?.layTextRecyclerView?.background = it
                    }

                    // submit button drawables theme
                    val submitButtonEnabledDrawable = AndroidResource.createDrawable(
                        themeComponent.submitButtonEnabled
                    )
                    val submitButtonDisabledDrawable = AndroidResource.createDrawable(
                        themeComponent.submitButtonDisabled
                    )
                    val state = StateListDrawable()
                    state.addState(
                        intArrayOf(android.R.attr.state_enabled),
                        submitButtonEnabledDrawable
                    )
                    state.addState(intArrayOf(), submitButtonDisabledDrawable)
                    binding?.layLock?.btnLock?.background = state

                    //confirmation label theme
                    AndroidResource.updateThemeForView(
                        binding?.layLock?.labelLock,
                        themeComponent.submitButtonDisabled,
                        fontFamilyProvider
                    )

                    //submit button font
                    AndroidResource.updateThemeForView(
                        binding?.layLock?.btnLock,
                        themeComponent.submitButtonDisabled,
                        fontFamilyProvider
                    )
                    AndroidResource.updateThemeForView(
                        binding?.layLock?.btnLock,
                        themeComponent.submitButtonEnabled,
                        fontFamilyProvider
                    )

                    if (themeComponent.confirmation?.background != null) {
                        binding?.layLock?.labelLock?.background =
                            AndroidResource.createDrawable(themeComponent.confirmation)
                    }
                    themeComponent.confirmation?.padding?.let {
                        AndroidResource.setPaddingForView(
                            binding?.layLock?.labelLock,
                            themeComponent.confirmation.padding
                        )
                    }
                }
            }
        }
    }

    private fun resourceObserver(widget: QuizWidget?) {
        widget?.apply {
            val optionList = resource.getMergedOptions() ?: return
            if (!inflated) {
                inflated = true
                binding = WidgetTextOptionSelectionBinding.inflate(
                    LayoutInflater.from(context),
                    this@QuizView,
                    true
                )
                wouldInflateSponsorUi()
            }

            // added tag for identification of widget (by default will be empty)
            setTagViewWithStyleChanges(context.resources.getString(R.string.livelike_quiz_tag))
            binding?.apply {
                logDebug { "Question:${resource.question}" }
                titleView.title = resource.question ?: ""

                txtTitleBackground.background = ContextCompat.getDrawable(context, R.drawable.header_rounded_corner_quiz)
                layTextRecyclerView.background = ContextCompat.getDrawable(context, R.drawable.body_rounded_corner_quiz)
                setBodyBackgroundForSponsor()

                val horizontalDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
                val divider: Drawable? = ContextCompat.getDrawable(context, R.drawable.quiz_list_item_separator)
                divider?.let { horizontalDecoration.setDrawable(it) }
                textRecyclerView.addItemDecoration(horizontalDecoration)

                titleView.titleViewBinding.titleTextView.gravity = Gravity.START
                layLock.btnLock.text = context.resources.getString(R.string.livelike_answer_label)
                layLock.labelLock.text =
                    context.resources.getString(R.string.livelike_answered_label)

                setCustomFontWithButtonStyle(layLock.btnLock,widgetViewThemeAttributes.footerBtnFontPath)
                setCustomFontWithTextStyle(layLock.labelLock,widgetViewThemeAttributes.footerBtnFontPath,0)

            }

            adapter = adapter ?: WidgetOptionsViewAdapter(
                optionList, type,
                selectionLockedFlow = viewModel!!.selectionLockedFlow,
                selectedPositionFlow = viewModel!!.selectedPositionFlow,
                userSelectedOptionIdFlow = viewModel!!.userSelectedOptionIdFlow
            )

            adapter?.interacativeUntil = resource.interactiveUntil

            resource.interactiveUntil?.parseISODateTime()?.let {
                val epochTimeMs = it.toInstant().toEpochMilli()
                viewModel?.startInteractiveUntilTimeout(epochTimeMs)
            }
            uiScope.launch {
                viewModel?.disableInteractionFlow?.collect {
                    if (it == true) {
                        lockInteraction()
                        binding?.layLock?.btnLock?.apply {
                            isEnabled = false
                            alpha = 0.5f
                        }
                    }
                }
            }

            // set on click
            adapter?.onClick = {
                adapter?.apply {
                    val currentSelectionId = myDataset[selectedPositionFlow.value]
                    viewModel?.currentVoteIdFlow?.value = currentSelectionId.id
                    widgetLifeCycleEventsListener?.onUserInteract(widgetData)
                    isFirstInteraction = true
                }
                enableLockButton()
            }

            widgetsTheme?.let {
                applyTheme(it)
            }
            disableLockButton()
            binding?.textRecyclerView?.apply {
                isFirstInteraction = viewModel?.getUserInteraction() != null
                this.adapter = this@QuizView.adapter
                this@QuizView.adapter?.restoreSelectedPosition(viewModel?.getUserInteraction()?.choiceId)
                setHasFixedSize(true)
            }
            binding?.layLock?.btnLock?.setOnClickListener {
                if (adapter?.selectedPositionFlow?.value != RecyclerView.NO_POSITION) {
                    lockVote()
                    binding?.textEggTimer?.visibility = GONE
                }
            }
            if (viewModel?.getUserInteraction() != null) {
                if(context.resources.getBoolean(R.bool.livelike_widget_show_label_lock)){
                    binding?.layLock?.labelLock?.visibility = VISIBLE
                }else{
                    binding?.layLock?.labelLock?.visibility = View.GONE
                }
            } else if (adapter?.selectedPositionFlow?.value != RecyclerView.NO_POSITION) {
                enableLockButton()
            }

            if (widgetViewModel?.widgetStateFlow?.value == null || widgetViewModel?.widgetStateFlow?.value == WidgetStates.READY)
                widgetViewModel?.widgetStateFlow?.value = WidgetStates.READY
        }
        if (widget == null) {
            inflated = false
            removeAllViews()
            parent?.let { (it as ViewGroup).removeAllViews() }
        }
    }

    private fun setBodyBackgroundForSponsor(){
        val bodyView = binding?.layTextRecyclerView ?: return
        val squareDrawable = ContextCompat.getDrawable(context, R.drawable.body_square_corner_quiz)
        val roundDrawable = ContextCompat.getDrawable(context, R.drawable.body_rounded_corner_quiz)

        if (squareDrawable != null && roundDrawable != null) {
            checkIfSponsorViewIsInflated(bodyView, squareDrawable, roundDrawable)
        }
    }


    private fun lockVote() {
        disableLockButton()
        viewModel?.currentVoteIdFlow?.value?.let { id ->
            adapter?.myDataset?.find { it.id == id }?.let { option ->
                viewModel?.saveInteraction(option)
            }
        }
        val labelVisible =
            context.resources.getBoolean(R.bool.livelike_widget_show_label_lock)
        if(labelVisible) {
            binding?.layLock?.labelLock?.visibility = View.VISIBLE
        }else{
            binding?.layLock?.labelLock?.visibility = View.GONE
        }
        viewModel?.run {
            timeOutJob?.cancel()
            uiScope.launch {
                lockInteractionAndSubmitVote()
            }
        }
    }

    private fun enableLockButton() {
        binding?.layLock?.btnLock?.apply {
            isEnabled = true
            alpha = 1f
        }
    }

    private fun disableLockButton() {
        binding?.layLock?.layLock?.visibility = VISIBLE
        binding?.layLock?.btnLock?.apply {
            isEnabled = false
            alpha = 0.5f
        }
    }

    private fun lockInteraction() {
        viewModel?.selectionLockedFlow?.value = true
    }

    private fun unLockInteraction() {
        viewModel?.selectionLockedFlow?.value = false
        viewModel?.markAsInteractive()
    }

    private fun defaultStateTransitionManager(widgetStates: WidgetStates?) {
        when (widgetStates) {
            WidgetStates.READY -> {
                moveToNextState()
            }
            WidgetStates.INTERACTING -> {
                viewModel?.dataFlow?.value?.let {
                    viewModel?.startDismissTimout(
                        it.resource.timeout
                    )
                }
            }
            WidgetStates.RESULTS -> {
                if (!isFirstInteraction) {
                    viewModel?.dismissWidget(DismissAction.TIMEOUT)
                }
                binding?.followupAnimation?.apply {
                    addAnimatorListener(object : Animator.AnimatorListener {
                        override fun onAnimationRepeat(animation: Animator) {
                            // nothing needed here
                        }

                        override fun onAnimationEnd(animation: Animator) {
                            viewModel?.uiScope?.launch {
                                delay(11000)
                                viewModel?.dismissWidget(DismissAction.TIMEOUT)
                            }
                        }

                        override fun onAnimationCancel(animation: Animator) {
                            // nothing needed here
                        }

                        override fun onAnimationStart(animation: Animator) {
                            // nothing needed here
                        }
                    })
                }
            }
            WidgetStates.FINISHED -> {
                resourceObserver(null)
            }
            else -> {}
        }
    }

    private fun showPercentage(mergedTotal: Int) {
        if (mergedTotal > 0) {
            adapter?.showPercentage = true
        }
    }

    private fun resultsObserver(resource: Resource?) {
        (resource ?: viewModel?.dataFlow?.value?.resource)?.apply {
            val optionResults = this.getMergedOptions() ?: return
            val totalVotes = optionResults.sumOf { it.getMergedVoteCount().toInt() }
            val options = viewModel?.dataFlow?.value?.resource?.getMergedOptions() ?: return
            for (opt in options) {
                optionResults.find {
                    it.id == opt.id
                }?.apply {
                    opt.updateCount(this)
                    opt.percentage = opt.getPercent(totalVotes.toFloat())
                }
            }
            adapter?.myDataset = options
            showPercentage(this.getMergedTotal())
            binding?.textRecyclerView?.swapAdapter(adapter, false)
        }
    }
}
