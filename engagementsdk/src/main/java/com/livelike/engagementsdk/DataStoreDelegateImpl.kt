package com.livelike.engagementsdk

import android.content.Context
import com.livelike.common.DataStoreDelegate
import com.livelike.common.NumberPredictionSavedWidgetVote
import com.livelike.common.SavedWidgetVote
import com.livelike.serialization.gson
import com.livelike.engagementsdk.core.data.models.NumberPredictionVotes
import com.livelike.engagementsdk.core.utils.liveLikeSharedPrefs.getSharedPreferences


internal class DataStoreDelegateImpl(val context: Context) : DataStoreDelegate {

    private var sdkSharedPreference =
        context.getSharedPreferences("$${context.packageName}-livelike-sdk", Context.MODE_PRIVATE)

    private val PREF_CHAT_ROOM_MSG_RECEIVED = "pubnub message received"
    private val PREFERENCE_KEY_WIDGET_CLAIM_TOKEN = "claim_token"
    private val PREFERENCE_KEY_WIDGETS_PREDICTIONS_VOTED = "predictions-voted"
    private val PREFERENCE_KEY_WIDGETS_NUMBER_PREDICTIONS_VOTED = "number-predictions-voted"
    private val PREFERENCE_KEY_POINTS_TOTAL = "PointsTotal"
    private val PREFERENCE_KEY_POINTS_TUTORIAL = "PointsTutorial"


    override fun getAccessToken(): String? {
        return sdkSharedPreference.getString("AccessToken", null)
    }

    override fun setAccessToken(accessToken: String) {
        sdkSharedPreference.edit().putString("AccessToken", accessToken).apply()
    }

    override fun getChatRoomMessageReceived(): String? {
        return sdkSharedPreference.getString(PREF_CHAT_ROOM_MSG_RECEIVED, null)
    }

    override fun setChatRoomMessageReceived(data: String) {
        sdkSharedPreference.edit().putString(PREF_CHAT_ROOM_MSG_RECEIVED, data).apply()
    }

    override fun getWidgetClaimToken(): String? {
        return sdkSharedPreference.getString(PREFERENCE_KEY_WIDGET_CLAIM_TOKEN, null)
    }

    override fun setWidgetClaimToken(token: String) {
        sdkSharedPreference.edit().putString(PREFERENCE_KEY_WIDGET_CLAIM_TOKEN, token).apply()
    }

    override fun addPublishedMessage(channel: String, messageId: String) {
        val msgList = getPublishedMessages(channel)
        msgList.add(messageId)
        val editor = sdkSharedPreference.edit()
        editor.putStringSet("$channel-published", msgList).apply()
    }

    override fun flushPublishedMessage(vararg channels: String) {
        val editor = sdkSharedPreference.edit()
        for (channel in channels) {
            editor.remove("$channel-published")
        }
        editor.apply()
    }

    override fun getPublishedMessages(channel: String): MutableSet<String> {
        return sdkSharedPreference.getStringSet("$channel-published", mutableSetOf())
            ?: mutableSetOf()
    }

    override fun addWidgetPredictionVoted(id: String, optionId: String) {
        val editor = sdkSharedPreference.edit()
        val idsList = getWidgetPredictionVoted().toMutableSet()
        idsList.remove(idsList.find { savedWidgetVote -> savedWidgetVote.id == id })
        idsList.add(SavedWidgetVote(id, optionId))
        editor.putString(
            PREFERENCE_KEY_WIDGETS_PREDICTIONS_VOTED,
            gson.toJson(idsList.toTypedArray())
        )
            .apply()
    }

    override fun getWidgetPredictionVoted(): Array<SavedWidgetVote> {
        val predictionVotedJson =
            sdkSharedPreference.getString(PREFERENCE_KEY_WIDGETS_PREDICTIONS_VOTED, "") ?: ""
        return gson.fromJson(predictionVotedJson, Array<SavedWidgetVote>::class.java)
            ?: emptyArray()
    }

    override fun getWidgetPredictionVotedAnswerIdOrEmpty(id: String?): String {
        return getWidgetPredictionVoted().find { it.id == id }?.optionId ?: ""
    }

    override fun addPoints(points: Int) {
        sdkSharedPreference.edit().putInt(PREFERENCE_KEY_POINTS_TOTAL, points + getTotalPoints())
            .apply()
    }

    override fun getTotalPoints(): Int {
        return sdkSharedPreference.getInt(PREFERENCE_KEY_POINTS_TOTAL, 0)
    }

    override fun shouldShowPointTutorial(): Boolean {
        return getSharedPreferences()
            .getBoolean(PREFERENCE_KEY_POINTS_TUTORIAL, true)
    }

    override fun pointTutorialSeen() {
        if (shouldShowPointTutorial()) {
            sdkSharedPreference.edit().putBoolean(PREFERENCE_KEY_POINTS_TUTORIAL, false).apply()
        }
    }

    override fun addWidgetNumberPredictionVoted(
        id: String,
        widgetVoteList: List<NumberPredictionVotes>
    ) {
        val editor = sdkSharedPreference.edit()
        val idsList = getWidgetNumberPredictionVoted().toMutableSet()
        for (item in widgetVoteList) {
            idsList.add(NumberPredictionSavedWidgetVote(id, item.optionId ?: "", item.number))
        }
        editor.putString(
            PREFERENCE_KEY_WIDGETS_NUMBER_PREDICTIONS_VOTED, gson.toJson(idsList.toTypedArray())
        )
            .apply()
    }

    override fun getWidgetNumberPredictionVoted(): Array<NumberPredictionSavedWidgetVote> {
        val predictionVotedJson = sdkSharedPreference.getString(
            PREFERENCE_KEY_WIDGETS_NUMBER_PREDICTIONS_VOTED, ""
        ) ?: ""
        return gson.fromJson(
            predictionVotedJson,
            Array<NumberPredictionSavedWidgetVote>::class.java
        )
            ?: emptyArray()
    }

}