package com.livelike.engagementsdk

import java.text.SimpleDateFormat
import java.util.*

internal const val BUGSNAG_ENGAGEMENT_SDK_KEY = "abb12b7b7d7868c07733e3e3808656c8"

internal const val CHAT_PROVIDER = "pubnub"

// Date time formatters

internal val DEFAULT_CHAT_MESSAGE_DATE_TIME_FORMATTER = SimpleDateFormat(
    "MMM d, h:mm a",
    Locale.getDefault()
)

internal const val INTEGRATOR_USED_CHATROOM_DELEGATE_KEY = "IntegratorUsedChatRoomDelegate"

internal const val SPONSOR_URL_NOT_FOUND_ERROR = "Sponsor Url not found"