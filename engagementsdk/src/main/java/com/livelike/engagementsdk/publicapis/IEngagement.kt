package com.livelike.engagementsdk.publicapis

import com.livelike.engagementsdk.widget.domain.UserProfileDelegate

interface IEngagement {

    /* Set user profile delegate to intercept any user related updates like rewards */
    var userProfileDelegate: UserProfileDelegate?


}
