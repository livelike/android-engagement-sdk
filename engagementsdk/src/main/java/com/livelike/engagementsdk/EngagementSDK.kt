package com.livelike.engagementsdk


import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.net.Uri
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.livelike.common.DataStoreDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.common.TimeCodeGetter
import com.livelike.engagementsdk.chat.ImageSize
import com.livelike.engagementsdk.chat.LiveLikeChatSession
import com.livelike.engagementsdk.core.AccessTokenDelegate
import com.livelike.engagementsdk.core.utils.*
import com.livelike.engagementsdk.core.utils.liveLikeSharedPrefs.initLiveLikeSharedPrefs
import com.livelike.engagementsdk.publicapis.*
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.utils.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import java.lang.ref.WeakReference
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

/**
 * This class to initialize the EngagementSDK. This is the entry point for SDK usage.
 * This creates an instance of EngagementSDK.
 *
 * @param clientId Client's id
 * @param applicationContext The application context
 */
class EngagementSDK(
    private val clientId: String,
    val applicationContext: Context,
    errorDelegate: ErrorDelegate? = null,
    originURL: String = "https://cf-blast.livelikecdn.com",
    accessTokenDelegate: AccessTokenDelegate,
    dataStoreDelegate: DataStoreDelegate = DataStoreDelegateImpl(applicationContext)
) : LiveLikeKotlin(
    clientId, errorDelegate, originURL, dataStoreDelegate, accessTokenDelegate, userAgent
), IEngagement {

    override var userProfileDelegate: UserProfileDelegate? = null
        set(value) {
            field = value
            for (session in baseSessionList) {
                if (session is ContentSession) {
                    session.userProfileRewardDelegate = value
                }
            }
            // userRepository.userProfileDelegate = value
        }


    /**
     * SDK Initialization logic.
     */
    init {
        initLiveLikeSharedPrefs(applicationContext)
    }


    /**
     *  Creates a content session without sync.
     *  @param programId Backend generated unique identifier for current program
     *  @param connectToDefaultChatRoom allow chatSession to connect to default chatRoom
     */
    fun createContentSession(
        programId: String,
        errorDelegate: ErrorDelegate? = null,
        connectToDefaultChatRoom: Boolean = true,
        includeFilteredChatMessages: Boolean = false,
        chatEnforceUniqueMessageCallback: Boolean = false
    ): LiveLikeContentSession {
        return createContentSession(
            programId,
            { EpochTime(0) },
            errorDelegate,
            connectToDefaultChatRoom,
            includeFilteredChatMessages,
            chatEnforceUniqueMessageCallback = chatEnforceUniqueMessageCallback

        )
    }


    private suspend fun preLoadImage(url: String) = suspendCoroutine { cont ->
        Glide.with(applicationContext).load(url).addListener(object : RequestListener<Drawable> {

            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>,
                isFirstResource: Boolean
            ): Boolean {
                cont.resume(false)
                return false
            }

            override fun onResourceReady(
                resource: Drawable,
                model: Any,
                target: Target<Drawable>?,
                dataSource: DataSource,
                isFirstResource: Boolean
            ): Boolean {
                cont.resume(true)
                return false
            }
        }).apply(
            RequestOptions().override(
                AndroidResource.dpToPx(74), AndroidResource.dpToPx(74)
            ).transform(
                MultiTransformation(FitCenter(), RoundedCorners(12))
            )
        ).preload()
    }

    @Deprecated(
        "Use TimeCodeGetter",
        ReplaceWith("com.livelike.common.TimeCodeGetter")
    )
    /**
     * Use to retrieve the current timecode from the videoplayer to enable Spoiler-Free Sync.
     *
     */
    interface TimecodeGetter : TimecodeGetterCore

    /**
     *  Creates a content session with sync.
     *  @param programId Backend generated identifier for current program
     *  @param timecodeGetter returns the video timecode
     *  @param connectToDefaultChatRoom allow chatSession to connect to default chatRoom
     */
    fun createContentSession(
        programId: String,
        timecodeGetter: TimeCodeGetter,
        errorDelegate: ErrorDelegate? = null,
        connectToDefaultChatRoom: Boolean = true,
        includeFilteredChatMessages: Boolean = false,
        sessionDispatcher: CoroutineDispatcher = Dispatchers.Default,
        uiDispatcher: CoroutineDispatcher = Dispatchers.Main,
        chatEnforceUniqueMessageCallback: Boolean = false
    ): LiveLikeContentSession {
        return createContentSession(
            programId,
            timecodeGetter,
            errorDelegate,
            connectToDefaultChatRoom,
            createChatSession(
                timecodeGetter,
                errorDelegate,
                includeFilteredChatMessages,
                chatEnforceUniqueMessageCallback = chatEnforceUniqueMessageCallback
            ),
            { preLoadImage(it) },
            applicationContext.resources.getBoolean(R.bool.livelike_widget_component_layout_transition_enabled),
            {
                val weakContext = WeakReference(applicationContext)
                AndroidResource.selectRandomLottieAnimation(it, weakContext)
            },
            sessionDispatcher, uiDispatcher
        )
    }


    /**
     *  Creates a chat session.
     *  @param programId Backend generated identifier for current program
     *  @param timeCodeGetter returns the video timecode
     */
    fun createChatSession(
        timeCodeGetter: TimeCodeGetter,
        errorDelegate: ErrorDelegate? = null,
        includeFilteredChatMessages: Boolean = false,
        sessionDispatcher: CoroutineDispatcher = Dispatchers.Default,
        uiDispatcher: CoroutineDispatcher = Dispatchers.Main,
        chatEnforceUniqueMessageCallback: Boolean = false
    ): LiveLikeChatSession {
        return createChatSession(
            timeCodeGetter = timeCodeGetter,
            errorDelegate = errorDelegate,
            includeFilteredChatMessages = includeFilteredChatMessages,
            quoteChatMessageDeletedMessage = applicationContext.getString(R.string.livelike_quote_chat_message_deleted_message),
            chatMessageDeletedMessage = applicationContext.getString(R.string.livelike_chat_message_deleted_message),
            isPlatformLocalContentImageUrl = {
                val uri = Uri.parse(it)
                uri.scheme != null && uri.scheme.equals("content")
            },
            getInputStreamForImageUrl = {
                val uri = Uri.parse(it)
                applicationContext.contentResolver.openInputStream(uri)
            },
            getSizeOfImage = {
                val bitmap = BitmapFactory.decodeByteArray(it, 0, it.size)
                ImageSize(bitmap.height, bitmap.width).also {
                    bitmap.recycle()
                }
            },
            preLoadImage = { preLoadImage(it) },
            sessionDispatcher = sessionDispatcher,
            mainDispatcher = uiDispatcher,
            chatEnforceUniqueMessageCallback = chatEnforceUniqueMessageCallback
        )
    }

    companion object {
        @JvmStatic
        var enableDebug: Boolean = false
    }
}
