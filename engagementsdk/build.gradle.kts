@Suppress("DSL_SCOPE_VIOLATION")

plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
//    alias(libs.plugins.dokka)
    alias(libs.plugins.kotlin.kapt)
    alias(libs.plugins.detekt)
    alias(libs.plugins.kotlin.parcelize)
    `maven-publish`
}

group = "com.livelike"
val sdkVersion = "2.98.8"
//To notify if allowing sending notification to release channel or test channel
val sendToReleaseChannel = true


android {
    namespace = "com.livelike.engagementsdk"
    compileSdk = libs.versions.compile.sdk.version.get().toInt()

    defaultConfig {
        // required for customizing vector icon colors
        vectorDrawables.useSupportLibrary = true
        multiDexEnabled = true
        minSdk = libs.versions.min.sdk.version.get().toInt()
        targetSdk = libs.versions.target.sdk.version.get().toInt()

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("proguard-rules.pro")

        buildConfigField("String", "SDK_VERSION", "\"$sdkVersion\"")
    }

    buildFeatures {
        viewBinding = true
    }

    buildTypes {
        getByName("debug") {}
        getByName("release") {
            isMinifyEnabled = false
//            isShrinkResources = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    kotlinOptions {
        jvmTarget = "17"
    }

    /*tasks.dokkaHtml.configure {
        //outputDirectory.set(file("../documentation/html"))
        dokkaSourceSets {
            named("main") {
                skipDeprecated.set(true)
                includeNonPublic.set(false)
                skipEmptyPackages.set(true)
                reportUndocumented.set(true)
            }
        }
    }*/

//    configurations {
//        debug
//        release
//    }

    testOptions {
        unitTests.isIncludeAndroidResources = true
    }

}

/*tasks.register("buildAndSendNotification") {
    doLast {
        val changeLogVersion = sdkVersion.replace(".", "")
        val text =
            "*Android SDK v${sdkVersion} Released*" + "\nChangelog Details: https://docs.livelike.com/changelog/android-sdk-${changeLogVersion}."
        val webHook =
            if (sendToReleaseChannel) "https://hooks.slack.com/services/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"
            else "https://hooks.slack.com/services/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"
        val channel = "#release-notifications"
        val payload =
            "{\"channel\": \"${channel}\", \"username\": \"Bot\", \"text\": \"${text}\", \"icon_emoji\": \":ghost:\"}"
        val command = listOf(
            "curl",
            "-X",
            "POST",
            webHook,
            "-H",
            "Content-Type: application/json",
            "-d",
            payload
        )
        exec {
            commandLine = command
        }
    }
}

tasks.assemble {
    finalizedBy(tasks.getByName("buildAndSendNotification"))
}*/

publishing {
    publications {
        // creating a release publication
        create<MavenPublication>("maven") {
            groupId = "com.livelike.android-engagement-sdk"
            artifactId = "engagementsdk"
            version = sdkVersion

            afterEvaluate {
                from(components["release"])
            }
        }
    }
}

dependencies {
    implementation(libs.appcompat)
    implementation(libs.constraintlayout)
    implementation(libs.material)
    implementation(libs.bundles.coroutines)
    implementation(libs.multidex)

    // Pubnub SDK for real-time communication
    implementation(libs.lottie)
    implementation(libs.swiperefreshlayout)

    // Glide
    //noinspection GradleDependency
    implementation(libs.glide)
    implementation(libs.bundles.android.x)
    kapt(libs.glide.compiler)
    implementation(libs.gif.drawable)

    //core modules
    api(project(":livelike-core:network"))
    api(project(":livelike-core:utils"))
    api(project(":livelike-core:serialization"))

    //livelike-kotlin modules
    api(project(":livelike-kotlin:widget"))
    api(project(":livelike-kotlin:chat"))
    api(project(":livelike-kotlin:comment"))
    api(project(":livelike-kotlin:video"))
    api(project(":livelike-kotlin:common"))
    api(project(":livelike-kotlin:reaction"))
    api(project(":livelike-kotlin:gamification"))
    api(project(":livelike-kotlin:social"))
    api(project(":livelike-kotlin:presence"))
    api(project(":livelike-kotlin:real-time"))
    api(project(":livelike-kotlin:rbac"))

    // unit tests components
    testImplementation(libs.mockk)
    testImplementation(libs.coroutines.test)
    testImplementation(libs.junit)
    testImplementation(libs.gson)
    //to test flow
    testImplementation(libs.turbine)
}