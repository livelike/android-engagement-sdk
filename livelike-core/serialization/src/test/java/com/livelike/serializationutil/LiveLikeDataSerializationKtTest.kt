import com.livelike.serialization.toJsonString
import org.junit.Test

class LiveLikeDataSerializationKtTest {
    @Test
    fun testDataClassNonConstructorVariableToJson() {
        val sample = SampleTest("vickey")
        val sampleJsonString = sample.toJsonString()
        println(sampleJsonString)
        assert(sampleJsonString.contains("hello"))
    }
}

data class SampleTest(
    var a1: String,

    ) {
    var a2: String = "hello"
}