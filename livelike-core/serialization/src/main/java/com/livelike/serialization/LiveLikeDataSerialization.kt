package com.livelike.serialization

import com.google.gson.JsonParseException
import com.google.gson.reflect.TypeToken
import com.livelike.network.NetworkResult
import com.livelike.utils.LiveLikeException
import com.livelike.utils.UNKNOWN_ERROR
import com.livelike.utils.UNKNOWN_SERVER_ERROR
import com.livelike.utils.logDebug
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


@Throws(LiveLikeException::class, NullPointerException::class)
suspend inline fun <reified R : Any, reified E : Any> processResultWitCustomException(
    networkResult: NetworkResult
): R = withContext(Dispatchers.Default) {
    logDebug { "Type: ${R::class} , network Result: $networkResult " }
    if (networkResult is NetworkResult.Success) {
        val data = networkResult.data.let { it.ifEmpty { "{}" } }
        if (R::class == String::class) return@withContext data as R
        return@withContext gson.fromJson<R>(data, object : TypeToken<R>() {}.type)
    } else if (networkResult is NetworkResult.Error) {
        val exception = when (networkResult) {
            is NetworkResult.Error.HttpError -> {
                if (E::class == String::class) {
                    LiveLikeException(networkResult.errorBody ?: UNKNOWN_SERVER_ERROR)
                } else {
                    LiveLikeException(
                        gson.fromJson(
                            networkResult.errorBody, E::class.java
                        )
                    )
                }
            }

            is NetworkResult.Error.NetworkError -> {
                networkResult.exception
            }

            is NetworkResult.Error.UnknownError -> {
                networkResult.exception
            }
        }
        throw exception
    }
    throw LiveLikeException(UNKNOWN_ERROR)
}

@Throws(LiveLikeException::class, NullPointerException::class)
suspend inline fun <reified R : Any> NetworkResult.processResult(): R {
    logDebug { "Process Result: $this" }
    return processResultWitCustomException<R, String>(this)
}


@Throws(LiveLikeException::class, NullPointerException::class)
suspend inline fun <reified R : Any, reified E : Any> NetworkResult.processResultWithError(): R {
    return processResultWitCustomException<R, E>(this)
}

fun Any.toJsonString(supportNull: Boolean = false): String {
    try {
        logDebug { "toJson String :$this" }
        if (supportNull) return gsonWithNullSupport.toJson(this)
        return gson.toJson(this)
    } catch (e: NullPointerException) {
        throw LiveLikeException(e)
    } catch (e: JsonParseException) {
        throw LiveLikeException(e)
    } catch (e: java.lang.Exception) {
        throw LiveLikeException(e)
    }
}

