package com.livelike.serialization

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.livelike.utils.parseISODateTime
import org.threeten.bp.ZonedDateTime
import java.lang.reflect.Type

internal class DateDeserializer : JsonDeserializer<ZonedDateTime> {

    override fun deserialize(
        element: JsonElement,
        arg1: Type,
        arg2: JsonDeserializationContext
    ): ZonedDateTime? {
        val date = element.asString
        return date.parseISODateTime()
    }
}