package com.livelike.serialization

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import org.threeten.bp.ZonedDateTime
import java.lang.reflect.Type

internal class DateSerializer : JsonSerializer<ZonedDateTime> {
    override fun serialize(
        src: ZonedDateTime?,
        typeOfSrc: Type?,
        context: JsonSerializationContext?
    ): JsonElement {
        val obj = JsonObject()
        obj.addProperty("program_date_time", isoUTCDateTimeFormatter.format(src).toString())
        return obj.get("program_date_time")
    }
}