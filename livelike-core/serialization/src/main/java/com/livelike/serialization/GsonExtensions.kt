package com.livelike.serialization

import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter

fun JsonObject.extractStringOrEmpty(propertyName: String): String {
    return if (this.has(propertyName) && !this[propertyName].isJsonNull) this[propertyName].asString else ""
}

val gson = GsonBuilder()
    .registerTypeAdapter(
        ZonedDateTime::class.java,
        DateDeserializer()
    )
    .registerTypeAdapter(
        ZonedDateTime::class.java,
        DateSerializer()
    )
    .create()!!


val gsonWithNullSupport = GsonBuilder()
    .registerTypeAdapter(
        ZonedDateTime::class.java,
        DateDeserializer()
    )
    .registerTypeAdapter(
        ZonedDateTime::class.java,
        DateSerializer()
    )
    .serializeNulls()
    .create()!!

val isoUTCDateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("UTC"))
