@Suppress("DSL_SCOPE_VIOLATION")

plugins {
    `java-library`
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.detekt)
    `maven-publish`
//    alias(libs.plugins.dokka)
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}


publishing {
    publications {
        // creating a release publication
        create<MavenPublication>("maven") {
            groupId = "com.livelike.android-engagement-sdk"
            artifactId = "livelike-core"
            version = "0.0.1"

            from(components["java"])
        }
    }
}

tasks.jar {
    from(subprojects.map { it.sourceSets.main.get().output })
}

dependencies {
    api(project(":livelike-core:utils"))
    api(project(":livelike-core:serialization"))
    api(project(":livelike-core:network"))
}