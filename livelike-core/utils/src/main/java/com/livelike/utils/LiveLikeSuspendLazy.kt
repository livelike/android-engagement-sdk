package com.livelike.utils

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.sync.Mutex

class Once<out T : Any>(private val block: suspend () -> T) {
    private val mutex = Mutex()
    private lateinit var value: T
    private val mutableStateFlow = MutableStateFlow<T?>(null)
    suspend operator fun invoke(refresh: Boolean = false): T {
        mutex.lock()
        try {
            if (!this::value.isInitialized || refresh) {
                value = block()
                mutableStateFlow.tryEmit(value)
            }
        } finally {
            mutex.unlock()
        }
        return value
    }

    fun flow(): StateFlow<T?> = mutableStateFlow
}

fun <T : Any> suspendLazy(initializer: suspend () -> T): Once<T> = Once(initializer)