package com.livelike.utils

/** The different verbosity types */
enum class LogLevel(
    val code: Int,
) {
    /** Highly detailed level of logging, best used when trying to understand the working of a specific section/feature of the Engagement SDK. */
    Verbose(2),

    /** Information that is diagnostically helpful to integrators and Engagement SDK developers. */
    Debug(3),

    /** Information that is always useful to have, but not vital. */
    Info(4),

    /** Information related to events that could potentially cause oddities, but the Engagement SDK will continue working as expected. */
    Warn(5),

    /** An error occurred that is fatal to a specific operation/component, but not the overall Engagement SDK. */
    Error(6),

    /** An error occurred that is fatal to a specific operation/component, but not the overall Engagement SDK. */
    Assert(7),

    /** No logging enabled. */
    None(8)
}


/** The lowest (most granular) log level to log */
var minimumLogLevel: LogLevel =
    LogLevel.Warn

inline fun <reified T> T.logVerbose(noinline message: () -> Any?) = log(T::class.java, LogLevel.Verbose, message)
inline fun <reified T> T.logDebug(noinline message: () -> Any?) = log(T::class.java, LogLevel.Debug, message)
inline fun <reified T> T.logInfo(noinline message: () -> Any?) = log(T::class.java, LogLevel.Info, message)
inline fun <reified T> T.logWarn(noinline message: () -> Any?) = log(T::class.java, LogLevel.Warn, message)
inline fun <reified T> T.logError(noinline message: () -> Any?) = log(T::class.java, LogLevel.Error, message)
inline fun <reified T> T.logAssert(noinline message: () -> Any?) = log(T::class.java, LogLevel.Assert, message)

private var handler: ((String) -> Unit)? = null

/**
 * Add a log handler to intercept the logs.
 * This can be helpful for debugging.
 *
 * @param logHandler Method processing the log string received.
 */
fun registerLogsHandler(logHandler: (String) -> Unit) {
    handler = logHandler
}

/**
 * call it to tear down the registered logs handler
 */

fun unregisterLogsHandler() {
    handler = null
}

/**
 *
 */
class SDKLoggerBridge (
    /**
     * called when sdk wishes to log a message with a throwable object
     * on the underlying logger
     *
     * @param level verbosity level
     * @param tag the category of log message
     * @param message the message given to log
     * @param throwable the associated throwable object
     */
    val exceptionLogger: (level: LogLevel, tag: String, message: String, throwable: Throwable) -> Unit = { level, tag, message, _ ->
        println("${level.name} ${tag}: $message")
    },

    /**
     * called when sdk wishes to log a message
     * on the underlying logger
     *
     * @param level verbosity level
     * @param tag the category of log message
     * @param message the message given to log
     */
    val logger: (level: LogLevel, tag: String, message: String) -> Unit = { level, tag, message ->
        println("${level.name} ${tag}: $message")
    }
)

private var loggerBridge = SDKLoggerBridge()

/**
 * apply a bridge interface such that a client can attach
 * a logger of there choice
 *
 * @param bridge definition class describing attachment to another logger
 */
fun registerLoggerBridge( bridge: SDKLoggerBridge){
    loggerBridge = bridge
}

/**
 * remove active logger bridge
 */
fun unregisterLoggerBridge( ){
    loggerBridge = SDKLoggerBridge()
}


fun <T> log(klass: Class<out T>, level: LogLevel, message: () -> Any?) {
    if (level >= minimumLogLevel) {
        message().let {
            val tag = klass.canonicalName ?: "com.livelike"
            when (it) {
                is Throwable -> loggerBridge.exceptionLogger.invoke(level,  tag, it.message ?: "", it)
                is Unit -> Unit
                null -> Unit
                else -> loggerBridge.logger.invoke(level, tag, it.toString())
            }
        }
        message().let {
            handler?.invoke(it.toString())
        }
    }
}
