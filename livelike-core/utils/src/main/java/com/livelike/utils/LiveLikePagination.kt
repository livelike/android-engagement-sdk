package com.livelike.utils

import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination

data class PaginationResponse<T>(
    val next: String? = null,
    val previous: String? = null,
    val count: Int,
    val results: List<T>
)

fun getUrlForPaginationWithRequestParam(
    liveLikePagination: LiveLikePagination,
    defaultUrl: String,
    requestKey: String,
    paginationMap: Map<String, PaginationResponse<*>>
): String? {
    return if (paginationMap[requestKey] == null || liveLikePagination == LiveLikePagination.FIRST) {
        defaultUrl
    } else if (liveLikePagination == LiveLikePagination.PREVIOUS) {
        paginationMap[requestKey]?.previous
    } else {
        paginationMap[requestKey]?.next
    }
}