package com.livelike.utils

const val UNKNOWN_ERROR = "Unknown Error has occurred"
const val UNKNOWN_SERVER_ERROR = "Some Error has Occurred"
const val NO_MORE_DATA = "No More data to load"
const val UNABLE_TO_LOAD_DATA = "Unable to load Data"
const val UNABLE_TO_CREATE_DATA = "Unable to create Data"
const val UNABLE_TO_DELETE_DATA = "Unable to delete Data"
const val UNABLE_TO_UPDATE_DATA = "Unable to update Data"
const val COMMENT_BOARD_TEMPLATE_URL_IS_NULL = "Comment Board Template Url is Null"
const val COMMENT_BOARD_ID_NOT_FOUND= "Comment Board id not found"
