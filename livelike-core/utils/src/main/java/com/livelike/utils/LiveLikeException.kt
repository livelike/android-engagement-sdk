package com.livelike.utils


class LiveLikeException : Exception {

    var error: Any? = null

    constructor(error: Any?) : super(error.toString())
    constructor(message: String?) : super(message)
    constructor(exception: Throwable) : super(exception)
}