@Suppress("DSL_SCOPE_VIOLATION")

plugins {
    `java-library`
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.detekt)
    `maven-publish`
//    alias(libs.plugins.dokka)
}

group = "com.livelike"

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

publishing {
    publications {
        // creating a release publication
        create<MavenPublication>("maven") {
            groupId = "com.livelike.android-engagement-sdk"
            artifactId = "livelike-core-utils"
            version = "0.0.1"

            from(components["java"])
        }
    }
}

dependencies {
    //coroutines
    api(libs.coroutines.core)
    //date time lib
    api(libs.three.ten)
}