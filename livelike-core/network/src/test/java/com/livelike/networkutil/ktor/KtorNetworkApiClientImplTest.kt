package com.livelike.networkutil.ktor

import com.livelike.network.NetworkClientConfig
import com.livelike.network.NetworkResult
import com.livelike.network.ktor.KtorNetworkApiClientImpl
import io.ktor.client.engine.mock.*
import io.ktor.http.*
import io.ktor.utils.io.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class KtorNetworkApiClientImplTest {
    private val testDispatcher = StandardTestDispatcher()

    private val successResponse = """{"ip":"127.0.0.1","id":123}"""
    private val errorResponse = "{\"error\":\"Some Error Occurred\"}"

    private val mockEngine = MockEngine { request ->
        when (request.url.encodedPath) {
            "/success_url" -> respond(
                content = ByteReadChannel(successResponse),
                status = HttpStatusCode.OK,
                headers = headersOf(HttpHeaders.ContentType, "application/json"),
            )
            "/error_url" -> respondError(
                content = errorResponse,
                status = HttpStatusCode.NotFound,
                headers = headersOf(HttpHeaders.ContentType, "application/json"),
            )
            else -> respond(
                content = ByteReadChannel(successResponse),
                status = HttpStatusCode.OK,
                headers = headersOf(HttpHeaders.ContentType, "application/json"),
            )
        }
    }
    private val apiClient =
        KtorNetworkApiClientImpl(NetworkClientConfig(), mockEngine, testDispatcher)

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
    }

    @Test
    fun testSuccessAPI() = runTest {
        val result = apiClient.get("/success_url")
        assert(result is NetworkResult.Success)
    }

    @Test
    fun testSuccessPostAPI() = runTest {
        val result = apiClient.post("/success_url")
        assert(result is NetworkResult.Success)
    }

    @Test
    fun testErrorAPI() = runTest {
        val result = apiClient.get("error_url")
        assert(result is NetworkResult.Error)
    }

    @Test
    fun testParsingSuccessDataClassFromJsonResponseAPI() = runTest {
        val result = apiClient.get("success_url")
        if (result is NetworkResult.Success) {
            assert(result.data == successResponse)
        }
    }

    @Test
    fun testParsingErrorDataClassFromJsonResponseAPI() = runTest {
        val result = apiClient.get("error_url")
        if (result is NetworkResult.Error.HttpError) {
            assert(result.errorBody == errorResponse)
        }
    }
}