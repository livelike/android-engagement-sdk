package com.livelike.network

import com.livelike.network.ktor.KtorNetworkApiClientImpl
import kotlinx.coroutines.CoroutineDispatcher
import java.io.InputStream

interface NetworkApiClient {
    suspend fun post(
        url: String,
        body: String? = null,
        accessToken: String? = null,
        headers: List<Pair<String, String>> = emptyList()
    ): NetworkResult

    suspend fun multipartFormData(
        url: String,
        fileType: String,
        fileName: String,
        byteArray: ByteArray,
        contentType: String,
        accessToken: String?,
        formData: Map<String, String>,
        headers: List<Pair<String, String>> = emptyList()
    ): NetworkResult

    suspend fun get(
        url: String,
        accessToken: String? = null,
        queryParameters: List<Pair<String, Any?>> = emptyList(),
        headers: List<Pair<String, String>> = emptyList()
    ): NetworkResult

    suspend fun put(
        url: String,
        body: String? = null,
        accessToken: String? = null,
        headers: List<Pair<String, String>> = emptyList()
    ): NetworkResult

    suspend fun patch(
        url: String,
        body: String? = null,
        accessToken: String? = null,
        headers: List<Pair<String, String>> = emptyList()
    ): NetworkResult

    suspend fun delete(
        url: String,
        accessToken: String? = null,
        headers: List<Pair<String, String>> = emptyList()
    ): NetworkResult

    suspend fun getInputStreamForDownloadFileFromUrl(url: String): InputStream

    companion object {
        const val TIME_OUT = 60L
        fun getInstance(
            config: NetworkClientConfig,
            dispatcher: CoroutineDispatcher
        ): NetworkApiClient = KtorNetworkApiClientImpl(config, dispatcher = dispatcher)
    }
}

