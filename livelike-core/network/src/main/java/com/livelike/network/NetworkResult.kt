package com.livelike.network

/**
 * A generic class that holds a value with its status.
 * @param <T>
 */
sealed class NetworkResult {

    data class Success(val data: String) : NetworkResult()
    sealed class Error : NetworkResult() {
        /**
         * Represents server (50x) and client (40x) errors.
         */
        data class HttpError(val code: Int, val errorBody: String?) : Error()

        /**
         * Represent IOExceptions and connectivity issues.
         */
        class NetworkError(val exception: Exception) : Error()

        class UnknownError(val exception: Exception) : Error() {
            constructor(error: String) : this(Exception(error))
        }
    }

    override fun toString(): String {
        return when (this) {
            is Success -> "Success[data=$data]"
            is Error.HttpError -> "HttpError[code=$code,body=$errorBody]"
            is Error.NetworkError -> "NetworkError[exception=${exception.message}]"
            is Error.UnknownError -> "UnknownError[exception=${exception.message}]"
        }
    }
}
