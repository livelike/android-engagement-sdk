package com.livelike.network

data class NetworkClientConfig(
    val userAgent: String? = null
)