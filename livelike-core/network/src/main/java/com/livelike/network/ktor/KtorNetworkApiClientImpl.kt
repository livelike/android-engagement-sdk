package com.livelike.network.ktor


import com.livelike.network.NetworkApiClient
import com.livelike.network.NetworkClientConfig
import com.livelike.network.NetworkResult
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import io.ktor.client.HttpClient
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.plugins.ClientRequestException
import io.ktor.client.plugins.DefaultRequest
import io.ktor.client.plugins.ServerResponseException
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.plugins.onUpload
import io.ktor.client.request.delete
import io.ktor.client.request.forms.MultiPartFormDataContent
import io.ktor.client.request.forms.formData
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.request.headers
import io.ktor.client.request.parameter
import io.ktor.client.request.patch
import io.ktor.client.request.post
import io.ktor.client.request.put
import io.ktor.client.request.setBody
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.bodyAsText
import io.ktor.http.ContentType
import io.ktor.http.Headers
import io.ktor.http.HttpHeaders
import io.ktor.http.contentType
import io.ktor.util.InternalAPI
import io.ktor.utils.io.errors.IOException
import io.ktor.utils.io.jvm.javaio.toInputStream
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import java.io.InputStream
import java.util.concurrent.TimeUnit

internal class KtorNetworkApiClientImpl(
    private val config: NetworkClientConfig,
    engine: HttpClientEngine = OkHttp.create {
        config {
            connectTimeout(NetworkApiClient.TIME_OUT, TimeUnit.SECONDS)
            writeTimeout(NetworkApiClient.TIME_OUT, TimeUnit.SECONDS)
            readTimeout(NetworkApiClient.TIME_OUT, TimeUnit.SECONDS)
        }
    },
    private val dispatcher: CoroutineDispatcher,
) : NetworkApiClient {

    private val client: HttpClient = HttpClient(engine) {
        install(Logging) {
            logger = object : Logger {
                override fun log(message: String) {
                    // if (BuildConfig.DEBUG)
                    logDebug { message }
                }
            }
            level = LogLevel.ALL
        }
        install(DefaultRequest) {
            config.userAgent?.let {
                header(
                    "User-Agent", it
                )
            }
        }
    }

    private suspend fun safeApiCall(call: suspend () -> HttpResponse): NetworkResult =
        withContext(dispatcher) {
            try {
                logDebug { "safeAPICall" }
                val response = call()
                logDebug { "response header ${response.headers["X-Cache"]}" }
                logDebug { "response status :${response.status}" }
                if (response.status.value in 200..299)
                    NetworkResult.Success(response.bodyAsText())
                else
                    NetworkResult.Error.HttpError(response.status.value, response.bodyAsText())
            } catch (e: ClientRequestException) {
                NetworkResult.Error.HttpError(
                    e.response.status.value, e.response.bodyAsText()
                )
            } catch (e: ServerResponseException) {
                NetworkResult.Error.HttpError(
                    e.response.status.value, e.response.bodyAsText()
                )
            } catch (e: IOException) {
                NetworkResult.Error.NetworkError(e)
            } catch (e: Exception) {
                NetworkResult.Error.UnknownError(e)
            }
        }

    override suspend fun post(
        url: String,
        body: String?,
        accessToken: String?,
        headers: List<Pair<String, String>>
    ): NetworkResult = try {
        safeApiCall {
            logDebug { "post body:$body , url:$url , accessToken: $accessToken" }
            client.post(url) {
                contentType(ContentType.Application.Json)
                body?.let {
                    setBody<String>(body)
                }
                headers {
                    accessToken?.let {
                        append("Authorization", "Bearer $accessToken")
                    }
                    for (header in headers)
                        append(header.first, header.second)
                }
            }
        }
    } catch (e: Exception) {
        logError { "Error during POST request: ${e.message}"}
        NetworkResult.Error.UnknownError(e)
    }

    override suspend fun multipartFormData(
        url: String,
        fileType: String,
        fileName: String,
        byteArray: ByteArray,
        contentType: String,
        accessToken: String?,
        formData: Map<String, String>,
        headers: List<Pair<String, String>>
    ): NetworkResult = safeApiCall {
        client.post(url) {
            setBody(MultiPartFormDataContent(formData {
                append(fileType, byteArray, Headers.build {
                    append(HttpHeaders.ContentType, contentType)
                    append(HttpHeaders.ContentDisposition, "filename=\"$fileName\"")
                })
            }))
            onUpload { bytesSentTotal, contentLength ->
                logDebug { "bytes: $bytesSentTotal, total:$contentLength" }
            }
//            contentType(ContentType.Application.Json)
            headers {
                accessToken?.let {
                    append("Authorization", "Bearer $accessToken")
                }
                for (header in headers)
                    append(header.first, header.second)
            }
        }
    }

    override suspend fun get(
        url: String,
        accessToken: String?,
        queryParameters: List<Pair<String, Any?>>,
        headers: List<Pair<String, String>>
    ): NetworkResult = safeApiCall {
        client.get(url) {
            headers {
                accessToken?.let {
                    append("Authorization", "Bearer $accessToken")
                }
                for (header in headers)
                    append(header.first, header.second)
            }
            contentType(ContentType.Application.Json)
            for (it in queryParameters) {
                parameter(it.first, it.second)
            }
        }
    }

    override suspend fun put(
        url: String,
        body: String?,
        accessToken: String?,
        headers: List<Pair<String, String>>
    ): NetworkResult = safeApiCall {
        client.put(url) {
            contentType(ContentType.Application.Json)
            body?.let {
                setBody<String>(body)
            }
            headers {
                accessToken?.let {
                    append("Authorization", "Bearer $accessToken")
                }
                for (header in headers)
                    append(header.first, header.second)
            }
        }
    }

    override suspend fun patch(
        url: String,
        body: String?,
        accessToken: String?,
        headers: List<Pair<String, String>>
    ): NetworkResult = safeApiCall {
        client.patch(url) {
            contentType(ContentType.Application.Json)
            body?.let {
                setBody<String>(body)
            }
            headers {
                accessToken?.let {
                    append("Authorization", "Bearer $accessToken")
                }
                for (header in headers)
                    append(header.first, header.second)
            }
        }
    }

    override suspend fun delete(
        url: String,
        accessToken: String?,
        headers: List<Pair<String, String>>
    ): NetworkResult = safeApiCall {
        client.delete(url) {
            headers {
                accessToken?.let {
                    append("Authorization", "Bearer $accessToken")
                }
                for (header in headers)
                    append(header.first, header.second)
            }
            contentType(ContentType.Application.Json)
        }
    }

    @OptIn(InternalAPI::class)
    override suspend fun getInputStreamForDownloadFileFromUrl(url: String): InputStream {
        val response = client.get(url)
        return response.content.toInputStream()
    }
}