[versions]
androidDocumentationPlugin = "1.9.0"
leakcanaryAndroid = "2.14"
media3Exoplayer = "1.1.1"

target-sdk-version = "34"
min-sdk-version = "21"
compile-sdk-version = "34"

coroutines = "1.7.2"
google-gson = "2.9.1"
ktor = "2.3.4"
threeTen = "1.3.8"
pubnub = "7.7.1"
ktx = "1.12.0"
appcompat = "1.6.1"
material = "1.10.0"
exoplayer = "2.19.1"
constraintlayout = "2.1.4"
multidex = "2.0.1"
lottie = "6.0.0"
swiperefreshlayout = "1.1.0"
glide = "4.16.0"
gif-drawable = "1.2.29"
activity-x = "1.8.0"
fragment-x = "1.6.1"
mockk = "1.13.7"
junit = "4.13.2"
androidx-junit = "1.1.5"
espresso = "3.5.1"
gson = "2.9.1"
turbine = "0.9.0"
flexlayout = "3.0.0"
databinding = "8.0.2"

androidGradlePlugin = "8.5.2"
kotlin-android = "1.8.10"
kotlin-jvm = "1.8.10"
detekt = "1.23.1"
dependencycheck = "8.4.0"
lifecycle-runtime-ktx = "2.6.2"
activity-compose = "1.8.0"
compose-bom = "2023.03.00"

[libraries]
coroutines-core = { group = "org.jetbrains.kotlinx", name = "kotlinx-coroutines-core", version.ref = "coroutines" }
coroutines-android = { group = "org.jetbrains.kotlinx", name = "kotlinx-coroutines-android", version.ref = "coroutines" }
gson = { module = "com.google.code.gson:gson", version.ref = "google-gson" }
ktor-okhttp = { group = "io.ktor", name = "ktor-client-okhttp", version.ref = "ktor" }
ktor-logging = { group = "io.ktor", name = "ktor-client-logging", version.ref = "ktor" }
leakcanary-android = { module = "com.squareup.leakcanary:leakcanary-android", version.ref = "leakcanaryAndroid" }
media3-exoplayer = { module = "androidx.media3:media3-exoplayer", version.ref = "media3Exoplayer" }
media3-exoplayer-dash = { module = "androidx.media3:media3-exoplayer-dash", version.ref = "media3Exoplayer" }
media3-exoplayer-hls = { module = "androidx.media3:media3-exoplayer-hls", version.ref = "media3Exoplayer" }
media3-ui = { module = "androidx.media3:media3-ui", version.ref = "media3Exoplayer" }
three-ten = { group = "org.threeten", name = "threetenbp", version.ref = "threeTen" }
pubnub = { group = "com.pubnub", name = "pubnub-kotlin", version.ref = "pubnub" }
android-ktx = { group = "androidx.core", name = "core-ktx", version.ref = "ktx" }
appcompat = { group = "androidx.appcompat", name = "appcompat", version.ref = "appcompat" }
material = { group = "com.google.android.material", name = "material", version.ref = "material" }
exoplayer = { group = "com.google.android.exoplayer", name = "exoplayer", version.ref = "exoplayer" }
constraintlayout = { group = "androidx.constraintlayout", name = "constraintlayout", version.ref = "constraintlayout" }
multidex = { group = "androidx.multidex", name = "multidex", version.ref = "multidex" }
lottie = { group = "com.airbnb.android", name = "lottie", version.ref = "lottie" }
swiperefreshlayout = { group = "androidx.swiperefreshlayout", name = "swiperefreshlayout", version.ref = "swiperefreshlayout" }
glide = { group = "com.github.bumptech.glide", name = "glide", version.ref = "glide" }
glide-compiler = { group = "com.github.bumptech.glide", name = "compiler", version.ref = "glide" }
gif-drawable = { group = "pl.droidsonroids.gif", name = "android-gif-drawable", version.ref = "gif-drawable" }
activity-x = { group = "androidx.activity", name = "activity-ktx", version.ref = "activity-x" }
fragment-x = { group = "androidx.fragment", name = "fragment-ktx", version.ref = "fragment-x" }
flex-layout = { group = "com.google.android.flexbox", name = "flexbox", version.ref = "flexlayout" }
data-binding = { group = "androidx.databinding", name = "databinding-runtime", version.ref = "databinding" }

coroutines-test = { group = "org.jetbrains.kotlinx", name = "kotlinx-coroutines-test", version.ref = "coroutines" }
mockk = { group = "io.mockk", name = "mockk", version.ref = "mockk" }
junit = { group = "junit", name = "junit", version.ref = "junit" }
androidXJunit = { group = "androidx.test.ext", name = "junit", version.ref = "androidx-junit" }
espresso = { group = "androidx.test.espresso", name = "espresso-core", version.ref = "espresso" }
ktor-test = { group = "io.ktor", name = "ktor-client-mock", version.ref = "ktor" }
turbine = { group = "app.cash.turbine", name = "turbine", version.ref = "turbine" }
lifecycle-runtime-ktx = { group = "androidx.lifecycle", name = "lifecycle-runtime-ktx", version.ref = "lifecycle-runtime-ktx" }
activity-compose = { group = "androidx.activity", name = "activity-compose", version.ref = "activity-compose" }
compose-bom = { group = "androidx.compose", name = "compose-bom", version.ref = "compose-bom" }
ui = { group = "androidx.compose.ui", name = "ui" }
ui-graphics = { group = "androidx.compose.ui", name = "ui-graphics" }
ui-tooling = { group = "androidx.compose.ui", name = "ui-tooling" }
ui-tooling-preview = { group = "androidx.compose.ui", name = "ui-tooling-preview" }
ui-test-manifest = { group = "androidx.compose.ui", name = "ui-test-manifest" }
ui-test-junit4 = { group = "androidx.compose.ui", name = "ui-test-junit4" }
material3 = { group = "androidx.compose.material3", name = "material3" }

[bundles]
ktor = ["ktor-logging", "ktor-okhttp"]
coroutines = ["coroutines-core", "coroutines-android"]
android-x = ["activity-x", "fragment-x"]
test = ["mockk", "coroutines-test", "junit", "ktor-test", "gson", "turbine"]

[plugins]
android-application = { id = "com.android.application", version.ref = "androidGradlePlugin" }
kotlin-android = { id = "org.jetbrains.kotlin.android", version.ref = "kotlin-android" }
android-library = { id = "com.android.library", version.ref = "androidGradlePlugin" }
kotlin-jvm = { id = "org.jetbrains.kotlin.jvm", version.ref = "kotlin-jvm" }
detekt = { id = "io.gitlab.arturbosch.detekt", version.ref = "detekt" }
dependencycheck = { id = "org.owasp.dependencycheck", version.ref = "dependencycheck" }
kotlin-kapt = { id = "org.jetbrains.kotlin.kapt", version.ref = "kotlin-android" }
kotlin-parcelize = { id = "org.jetbrains.kotlin.plugin.parcelize", version.ref = "kotlin-android" }