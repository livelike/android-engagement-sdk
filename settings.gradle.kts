pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}

dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        maven { url = uri("https://jitpack.io") }
        maven { url = uri("https://repo.gradle.org/gradle/libs-releases") }
        maven { url = uri("https://kotlin.bintray.com/kotlinx/") }
        maven { url = uri("https://maven.pkg.jetbrains.space/public/p/kotlinx-html/maven") }
    }
}
rootProject.name = "android-engagement-sdk"

include(
    "livelikedemo",
    "engagementsdk",
    "pluginexoplayer",
    "livelike-sceenic-plugin",
    "sceenic-video-sdk",
    "livelike-core",
    "livelike-core:network",
    "livelike-core:serialization",
    "livelike-core:utils",
    "livelike-kotlin",
    "livelike-kotlin:chat",
    "livelike-kotlin:widget",
    "livelike-kotlin:reaction",
    "livelike-kotlin:gamification",
    "livelike-kotlin:common",
    "livelike-kotlin:video",
    "livelike-kotlin:comment",
    "livelike-kotlin:social",
    "livelike-kotlin:presence",
    "livelike-kotlin:real-time",
    "livelike-kotlin:rbac",
    "livelike-ui:comments",
    "livelike-ui:reactions"
)

startParameter.setExcludedTaskNames(
    listOf(
        "livelikedemo:detekt",
        "livelikedemo:lintRelease",
        "livelikedemo:lintDebug"
    )
)