import org.owasp.dependencycheck.gradle.extension.NodeAuditExtension

// Top-level build file where you can add configuration options common to all sub-projects/modules.
@Suppress("DSL_SCOPE_VIOLATION")

plugins {
    alias(libs.plugins.android.application) apply false
    alias(libs.plugins.kotlin.android) apply false
    alias(libs.plugins.kotlin.jvm) apply false
    alias(libs.plugins.android.library) apply false
    alias(libs.plugins.detekt) apply false
    alias(libs.plugins.kotlin.kapt) apply false
    alias(libs.plugins.dependencycheck) apply false
    alias(libs.plugins.kotlin.parcelize) apply false
}

tasks.register<io.gitlab.arturbosch.detekt.Detekt>("detektAll") {
    description = "Runs Detekt on the whole project at once."
    parallel = true
    setSource(projectDir)
    include("**/*.kt", "**/*.kts")
    exclude("**/resources/**", "**/build/**", "**/livelikedemo/**")
    config.setFrom("$rootDir/config/detekt/detekt.yml")
    buildUponDefaultConfig = true
}

allprojects {
    apply(plugin = "org.owasp.dependencycheck")
    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            jvmTarget = "17"
        }
    }
}

configure<org.owasp.dependencycheck.gradle.extension.DependencyCheckExtension> {
    format = org.owasp.dependencycheck.reporting.ReportGenerator.Format.HTML.toString()
    skipProjects = listOf(":livelikedemo")
    analyzers(closureOf<org.owasp.dependencycheck.gradle.extension.AnalyzerExtension> {
        experimentalEnabled = true
        assemblyEnabled = false
        bundleAuditEnabled = false

        isExperimentalEnabled = true
        isBundleAuditEnabled = false
        isAssemblyEnabled = false

        retirejs(closureOf<org.owasp.dependencycheck.gradle.extension.RetireJSExtension> {
            isEnabled = true
            enabled = true
        })

        nodeAudit(closureOf<NodeAuditExtension> {
            isEnabled = true
            skipDevDependencies = true
        })
    })
}

