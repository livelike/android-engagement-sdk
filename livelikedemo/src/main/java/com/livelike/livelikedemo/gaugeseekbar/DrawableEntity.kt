package com.livelike.livelikedemo.gaugeseekbar

import android.graphics.PointF
import android.graphics.drawable.Drawable

abstract class DrawableEntity(protected var centerPosition: PointF) : Drawable()
