package com.livelike.livelikedemo

import android.content.Context
import android.os.Bundle
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.LiveLikeContentSession
import com.livelike.engagementsdk.core.AccessTokenDelegate
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.livelikedemo.LiveLikeApplication.Companion.PREFERENCES_APP_ID
import com.livelike.livelikedemo.channel.ChannelManager
import com.livelike.livelikedemo.databinding.ActivitySampleAppBinding

class SampleAppActivity : AppCompatActivity() {

    private var session: LiveLikeContentSession? = null
    private lateinit var channelManager: ChannelManager
    private var engagementSDK: EngagementSDK? = null
    private lateinit var binding: ActivitySampleAppBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Default)
        binding = ActivitySampleAppBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initSDK()
    }

    private fun initSDK() {
        channelManager = (application as LiveLikeApplication).channelManager
        LiveLikeApplication.selectedEnvironment?.let {
            engagementSDK = EngagementSDK(
                it.clientId, applicationContext,
                object : ErrorDelegate() {
                    override fun onError(error: String) {
                        android.os.Handler(Looper.getMainLooper()).postDelayed(
                            {
                                initSDK()
                            },
                            1000
                        )
                    }
                },
                accessTokenDelegate = object : AccessTokenDelegate {
                    override fun getAccessToken(): String? {
                        return getSharedPreferences(
                            PREFERENCES_APP_ID,
                            Context.MODE_PRIVATE
                        ).getString(
                            PREF_USER_ACCESS_TOKEN_1,
                            null
                        ).apply {
                            println("Token:$this")
                        }
                    }

                    override fun storeAccessToken(accessToken: String?) {
                        getSharedPreferences(PREFERENCES_APP_ID, Context.MODE_PRIVATE).edit()
                            .putString(
                                PREF_USER_ACCESS_TOKEN_1, accessToken
                            ).apply()
                    }
                },
            )
        }
        channelManager.selectedChannel.let { channel ->
            if (channel != ChannelManager.NONE_CHANNEL) {
                session =
                    engagementSDK?.createContentSession(
                        channel.llProgram!!,
                        errorDelegate = object :
                            ErrorDelegate() {
                            override fun onError(error: String) {
                                println("Error:$error")
                            }
                        }
                    )
                binding.chatView.setSession(session!!.chatSession)
                binding.widgetView.setSession(session!!)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        session!!.resume()
    }

    override fun onPause() {
        super.onPause()
        session!!.pause()
    }

    override fun onDestroy() {
        super.onDestroy()
        session!!.close()
        engagementSDK?.close()
        engagementSDK = null
    }
}
