package com.livelike.livelikedemo.customchat

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.JsonParser
import com.livelike.engagementsdk.ChatRoomListener
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.MessageListener
import com.livelike.engagementsdk.chat
import com.livelike.engagementsdk.chat.ChatRoomInfo
import com.livelike.engagementsdk.chat.MessageSwipeController
import com.livelike.engagementsdk.chat.SwipeControllerActions
import com.livelike.engagementsdk.chat.data.remote.LiveLikeOrdering
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.chat.data.remote.PinMessageInfo
import com.livelike.engagementsdk.chat.stickerKeyboard.findImages
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage
import com.livelike.livelikedemo.CustomChatActivity
import com.livelike.livelikedemo.LiveLikeApplication
import com.livelike.livelikedemo.LiveLikeApplication.Companion.PREFERENCES_APP_ID
import com.livelike.livelikedemo.databinding.CustomChatItemBinding
import com.livelike.livelikedemo.databinding.FragmentChatBinding
import com.livelike.utils.logDebug
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale


class ChatFragment : Fragment() {

    private lateinit var binding: FragmentChatBinding
    private var currentQuoteMessage: LiveLikeChatMessage? = null
        set(value) {
            field = value
            if (value != null) {
                binding.layQuote.visibility = View.VISIBLE
                binding.txtQuoteMsg.text = when (value.imageUrl != null) {
                    true -> "Image"
                    else -> value.message
                }
            } else {
                binding.layQuote.visibility = View.INVISIBLE
                binding.txtQuoteMsg.text = ""
            }
        }
    private val images = arrayListOf(
        "https://i.picsum.photos/id/574/200/300.jpg?hmac=8A2sOGZU1xgRXI46snJ80xNY3Yx-KcLVsBG-wRchwFg",
        "https://i.picsum.photos/id/958/200/300.jpg?hmac=oCwv3AFzS5VqZv3nvDJ3H5RzcDH2OiL2g-GGwWL5fsI",
        "https://i.picsum.photos/id/658/200/300.jpg?hmac=K1TI0jSVU6uQZCZkkCMzBiau45UABMHNIqoaB9icB_0",
        "https://i.picsum.photos/id/237/200/300.jpg?hmac=TmmQSbShHz9CdQm0NkEjx1Dyh_Y984R9LpNrpvH2D_U",
        "https://i.picsum.photos/id/185/200/300.jpg?hmac=77sYncM4jSlhNlIKtqotElWQuIV3br7wNsq18rlbKnA"
    )
    private val gifs = arrayListOf(
        "https://media.giphy.com/media/SggILpMXO7Xt6/giphy.gif",
        "https://media.giphy.com/media/xULW8PLGQwyZNaw68U/giphy.gif",
        "https://media.giphy.com/media/wqb5K5564JSlW/giphy.gif",
        "https://media.giphy.com/media/401riYDwVhHluIByQH/giphy.gif",
        "https://media.giphy.com/media/4rW9yu8QaZJFC/giphy.gif"
    )

    private val scope = CoroutineScope(Dispatchers.Main + SupervisorJob())

    private val adapter = CustomChatAdapter()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding = FragmentChatBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.rcylChat.adapter = adapter
        context?.let {
            val messageSwipeController =
                MessageSwipeController(it, object : SwipeControllerActions {
                    override fun showReplyUI(position: Int) {
                        currentQuoteMessage = adapter.getChatMessage(position)
                        if (currentQuoteMessage != null && currentQuoteMessage!!.id == null) {
                            currentQuoteMessage = null
                            Toast.makeText(
                                context,
                                "Reply Not Allowed!! Try Again",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            Toast.makeText(context, "Send Reply", Toast.LENGTH_SHORT).show()
                        }
                        println("ChatFragment.showReplyUI>>$currentQuoteMessage")
                    }
                })
            val itemTouchHelper = ItemTouchHelper(messageSwipeController)
            itemTouchHelper.attachToRecyclerView(binding.rcylChat)
        }

        // presently program id and chat room has been harcoded for just
        // testing purpose wheather we the data is received and rendered correctly
        // will remove this latter

        /* (activity as CustomChatActivity).selectedHomeChat?.let { homeChat ->
             var programId = homeChat.channel.llProgram

             if(programId.equals("5b4b2538-02c3-4ad2-820a-2c5e6da66314",ignoreCase = true)){
                 custom.visibility = View.VISIBLE
                 (activity as CustomChatActivity).selectedHomeChat?.session?.chatSession?.
                 connectToChatRoom("4121f7af-9f18-43e5-a658-0ac364e2f176", object : LiveLikeCallback<Unit>() {
                     override fun onResponse(result: Unit?, error: String?) {
                         println("ChatOnlyActivity.onResponse -> $result -> $error")
                     }
                 })
             }else{
                 custom.visibility = View.GONE
             }
         }*/
        adapter.sdk = (activity as CustomChatActivity).sdk

        binding.edMsg.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                val matcher = s.toString().findImages()
                if (matcher.matches()) {
                    sendMessage(null, s.toString().substring(1, s.toString().length - 1))
                    binding.edMsg.text?.clear()
                }
            }
        })

        (activity as CustomChatActivity).selectedHomeChat?.let { homeChat ->
            adapter.chatRoomId = homeChat.session.chatSession.getCurrentChatRoom()

            binding.imgQuoteRemove.setOnClickListener {
                currentQuoteMessage = null
            }

            adapter.loadPinnedMessage(context)
            adapter.chatList.clear()
            adapter.chatList.addAll(homeChat.session.chatSession.getLoadedMessages())
            homeChat.session.chatSession.setChatRoomListener(object : ChatRoomListener {
                override fun onChatRoomUpdate(chatRoom: ChatRoomInfo) {
//                        switch_quote_message.isChecked = chatRoom.enableMessageReply
                }
            })
//            switch_reply_message.setOnCheckedChangeListener { buttonView, isChecked ->
//                (activity as CustomChatActivity).sdk?.chat()
//                    ?.updateChatRoom(homeChat.session.chatSession.getCurrentChatRoom(),
//                        enableMessageReply = isChecked,
//                        liveLikeCallback = object : LiveLikeCallback<ChatRoomInfo>() {
//                            override fun onResponse(result: ChatRoomInfo?, error: String?) {
//                                switch_reply_message.isChecked = result?.enableMessageReply ?: false
//                                error?.let {
//                                    Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
//                                }
//                            }
//                        })
//            }
            (activity as CustomChatActivity).sdk?.chat()
                ?.getChatRoom(homeChat.session.chatSession.getCurrentChatRoom()) { result, error ->
                    error?.let {
                        Toast.makeText(context, "FetchError: $it", Toast.LENGTH_SHORT)
                            .show()
                    }
//                            switch_quote_message.isChecked = result?.enableMessageReply ?: false
                }

            homeChat.session.chatSession.setMessageListener(object : MessageListener {
                private val TAG = "LiveLike"
                override fun onNewMessage(message: LiveLikeChatMessage) {
                    Log.i(TAG, "onNewMessage: $message")
                    val index =
                        adapter.chatList.indexOfFirst { it.clientMessageId == message.clientMessageId }
                    if (index > -1) {
                        adapter.chatList[index] = message
                    } else {
                        adapter.chatList.add(message)
                    }

                    if (index > -1) {
                        adapter.notifyItemChanged(index)
                        if (index == adapter.itemCount - 1) {
                            binding.rcylChat.scrollToPosition(adapter.itemCount - 1)
                        }
                    } else {
                        adapter.notifyItemInserted(adapter.chatList.size - 1)
                        binding.rcylChat.scrollToPosition(adapter.itemCount - 1)
                    }
                    Log.d(TAG, "onNewMessage: ${adapter.chatList.size}")
                }

                override fun onHistoryMessage(messages: List<LiveLikeChatMessage>) {
                    Log.d(TAG, "onHistoryMessage: ${messages.size}")
                    messages.toMutableList()
                        .removeAll {
                            homeChat.session.chatSession.getDeletedMessages().contains(it.id)
                        }
                    val empty = adapter.chatList.isEmpty()
                    adapter.chatList.addAll(0, messages)
                    adapter.notifyDataSetChanged()
                    binding.laySwipe.isRefreshing = false

                    if (empty) {
                        binding.rcylChat.scrollToPosition(adapter.itemCount - 1)
                    }
                }

                override fun onDeleteMessage(messageId: String) {
                    Log.d(TAG, "onDeleteMessage: $messageId")
                    val index = adapter.chatList.indexOfFirst { it.id == messageId }
                    if (index > -1) {
                        adapter.chatList.removeAt(index)
                        adapter.notifyItemRemoved(index)
                    }
                }

                override fun onPinMessage(message: PinMessageInfo) {
                    println("ChatFragment.onPinMessage")
                    Toast.makeText(
                        context,
                        "Pinned: ${message.messageId}\n${message.pinnedById}\n${message.messagePayload?.message}",
                        Toast.LENGTH_SHORT
                    ).show()
                    adapter.pinnedList.add(message)
                    adapter.notifyDataSetChanged()
                }

                override fun onUnPinMessage(pinMessageId: String) {
                    Toast.makeText(context, "UnPinned: $pinMessageId", Toast.LENGTH_SHORT)
                        .show()
                    val index = adapter.pinnedList.indexOfFirst { it.id == pinMessageId }
                    if (index != -1) {
                        adapter.pinnedList.removeAt(index)
                        adapter.notifyDataSetChanged()
                    }
                }

                override fun onErrorMessage(error: String, clientMessageId: String?) {
                    Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
                }
            })

            binding.laySwipe.isRefreshing = true
            homeChat.session.chatSession.loadNextHistory()
            binding.laySwipe.setOnRefreshListener {
                homeChat.session.chatSession.loadNextHistory()
            }
            binding.btnSend.setOnClickListener {
                val msg = binding.edMsg.text.toString()
                if (msg.trim().isNotEmpty()) {
                    sendMessage(msg, null)
                }
            }
            binding.btnImgSend.setOnClickListener {
                sendMessage(null, images.random())
            }
            binding.btnGifSend.setOnClickListener {
                sendMessage(null, gifs.random())
            }

            // send custom message
            binding.custom.setOnClickListener {
                scope.launch(Dispatchers.IO) {
                    sendCustomMessage(
                        "{\n" +
                                "  \"check1\": \"heyaa, this is for testing\"\n" +
                                "}"
                    )
                }
            }
        }
    }

    private fun sendMessage(message: String?, imageUrl: String?) {
        (activity as CustomChatActivity).selectedHomeChat?.let { homeChat ->
            fun callback(result: LiveLikeChatMessage?, error: String?) {
                binding.edMsg.text?.clear()
                result?.let { message ->
                    val index = adapter.chatList.indexOfFirst { message.id == it.id }
                    if (index == -1) {
                        adapter.chatList.add(message)
                        adapter.notifyItemInserted(adapter.chatList.size - 1)
                        binding.rcylChat.scrollToPosition(adapter.itemCount - 1)
                    }
                    currentQuoteMessage = null
                }
            }
            if (currentQuoteMessage != null && currentQuoteMessage!!.id != null) {
                homeChat.session.chatSession.quoteMessage(
                    message,
                    imageUrl,
                    imageWidth = 150,
                    imageHeight = 150,
                    quoteMessageId = currentQuoteMessage!!.id!!,
                    quoteMessage = currentQuoteMessage!!,
                    messageMetadata = sampleMapForMessageMetadata(),
                    liveLikePreCallback = ::callback
                )
            } else {
                homeChat.session.chatSession.sendMessage(
                    message, imageUrl = imageUrl, imageWidth = 150, imageHeight = 150,
                    messageMetadata = sampleMapForMessageMetadata(),
                    liveLikePreCallback = ::callback
                )
            }
        }
    }

    private fun sampleMapForMessageMetadata(): Map<String, Any?>? {
        val emptyMutableMap = mutableMapOf<String, Any?>()
        emptyMutableMap["Apple"] = "200"
        emptyMutableMap["Name"] = "Test"
        emptyMutableMap["Price"] = 22.22
        emptyMutableMap["isTestingOk"] = true
        return emptyMutableMap
    }

    // custom message api call
    private fun sendCustomMessage(post: String? = null) {
        (activity as CustomChatActivity).selectedHomeChat?.let { homeChat ->
            post?.let {
                homeChat.session.chatSession.sendCustomChatMessage(
                    post
                ) { result, error ->
                    result?.let {
                        Log.d("responseCode", result.id!!)
                    }
                    error?.let {
                        println("ChatFragment.onResponse>> $error")
                        Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
                    }
                }
            }

            /*var chatRoomId = homeChat.session.chatSession.getCurrentChatRoom()
            Log.d("custom-message1", chatRoomId)
            val urls =
                "${com.livelike.engagementsdk.BuildConfig.CONFIG_URL}chat-rooms/$chatRoomId/custom-messages/"

            Log.d("custom-message", urls)

            val body = post?.toRequestBody()
            val request: Request = Request.Builder()
                .url(urls)
                .method("POST", body)
                .addHeader(
                    authorization,
                    accessToken
                )
                .addHeader(contentType, applicationJSON)
                .build()
            val response: Response = client.newCall(request).execute()
            val code = response.code
            Log.d("responseCode", code.toString())*/
        }
    }

    override fun onPause() {
        super.onPause()
        (activity as? CustomChatActivity)?.selectedHomeChat?.let {
            val sharedPref =
                (activity as? CustomChatActivity)?.getSharedPreferences(
                    PREFERENCES_APP_ID,
                    Context.MODE_PRIVATE
                )
            sharedPref?.edit()?.putLong(
                "msg_time_${it.channel.llProgram}",
                Calendar.getInstance().timeInMillis
            )?.apply()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = ChatFragment()
    }
}

class CustomChatAdapter : RecyclerView.Adapter<CustomChatViewHolder>() {
    var chatRoomId: String? = null
    val chatList = arrayListOf<LiveLikeChatMessage>()
    var sdk: EngagementSDK? = null
    var pinnedList = arrayListOf<PinMessageInfo>()

    fun loadPinnedMessage(context: Context?) {
        sdk?.chat()?.getPinMessageInfoList(
            chatRoomId!!,
            LiveLikeOrdering.ASC,
            LiveLikePagination.FIRST
        ) { result, error ->
            result?.let {
                logDebug { "pinned msg ->  ${result.get(0)}" }
                pinnedList.addAll(it.toSet())
                notifyDataSetChanged()
            }
            error?.let {
                Toast.makeText(
                    context,
                    it,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomChatViewHolder {
        val itemBinding =
            CustomChatItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CustomChatViewHolder(
            itemBinding
        )
    }

    override fun getItemViewType(position: Int): Int {
        val chat = chatList[position]
        if (chat.quoteMessage != null) {
            return 2
        }
        return 1
    }

    override fun onBindViewHolder(holder: CustomChatViewHolder, position: Int) {
        val chatMessage = chatList[position]
        holder.itemBinding.messageMetaData.visibility = View.VISIBLE
        if (chatMessage.quoteMessage != null) {
            holder.itemBinding.layQuoteChatMsg.visibility = View.VISIBLE
            holder.itemBinding.txtNameQuote.text = chatMessage.quoteMessage?.nickname
            holder.itemBinding.txtMessageQuote.text = chatMessage.quoteMessage?.message
            if (chatMessage.quoteMessage?.imageUrl != null) {
                holder.itemBinding.imgMessageQuote.visibility = View.VISIBLE
                Glide.with(holder.itemBinding.imgMessageQuote.context)
                    .load(chatMessage.quoteMessage!!.imageUrl!!)
                    .apply(
                        RequestOptions().override(
                            chatMessage.quoteMessage!!.imageWidth!!,
                            chatMessage.quoteMessage!!.imageHeight!!
                        )
                    )
                    .into(holder.itemBinding.imgMessageQuote)

            } else {
                holder.itemBinding.imgMessageQuote.visibility = View.GONE
            }
        } else {
            holder.itemBinding.layQuoteChatMsg.visibility = View.GONE
        }

        holder.itemBinding.normalMessage.visibility = View.VISIBLE
        holder.itemBinding.customMessages.visibility = View.GONE
        holder.itemBinding.customTv.visibility = View.GONE
        holder.itemBinding.txtName.text = chatMessage.nickname
        val dateTime = Date()
        chatMessage.timestamp?.let {
            dateTime.time = it.toLong()
        }
        holder.itemBinding.imgPin.visibility = when {
            pinnedList.any { it.messageId == chatMessage.id } -> {
                View.VISIBLE
            }

            else -> {
                View.INVISIBLE
            }
        }
        if (chatMessage.imageUrl != null && chatMessage.imageWidth != null && chatMessage.imageHeight != null
        ) {
            holder.itemBinding.imgMessage.visibility = View.VISIBLE
            chatMessage.imageUrl?.let {
                Glide.with(holder.itemBinding.imgMessage.context)
                    .load(it)
                    .apply(
                        RequestOptions().override(
                            chatMessage.imageWidth!!,
                            chatMessage.imageHeight!!
                        )
                    )
                    .into(holder.itemBinding.imgMessage)
            }
            holder.itemBinding.txtMessage.text = ""
        } else {
            holder.itemBinding.imgMessage.visibility = View.GONE
            if (chatMessage.messageMetadata != null) {
                holder.itemBinding.messageMetaData.text = chatMessage.messageMetadata.toString()
            }

            if (!chatMessage.message.isNullOrEmpty()) {
                holder.itemBinding.txtMessage.text = chatMessage.message
            } else if (!chatMessage.customData.isNullOrEmpty()) {
                var customData = chatMessage.customData

                holder.itemBinding.normalMessage.visibility = View.GONE
                holder.itemBinding.customMessages.visibility = View.VISIBLE
                try {
                    val jsonObject =
                        JsonParser.parseString(customData.toString()).asJsonObject

                    if (jsonObject.has("kind")) {
                        holder.itemBinding.widgetView.visibility = View.VISIBLE
                        holder.itemBinding.widgetView.displayWidget(
                            (holder.itemView.context.applicationContext as LiveLikeApplication).sdk,
                            jsonObject
                        )
                    } else {
                        holder.itemBinding.customTv.visibility = View.VISIBLE
                        holder.itemBinding.widgetView.visibility = View.GONE
                        holder.itemBinding.customTv.text = jsonObject.toString()
                    }
                } catch (_: Exception) {
                    holder.itemBinding.customTv.text = customData
                    holder.itemBinding.customTv.visibility = View.VISIBLE
                    holder.itemBinding.widgetView.visibility = View.GONE
                }
            }
        }

        holder.itemView.setOnClickListener {
            val index = pinnedList.indexOfFirst { it.messageId == chatMessage.id }
            if (index != -1) {
                sdk?.chat()?.unPinMessage(
                    pinnedList[index].id
                ) { result, error ->
                    error?.let {
                        Toast.makeText(
                            holder.itemView.context,
                            it,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    result?.let {
                        Toast.makeText(
                            holder.itemView.context,
                            "Message Unpinned",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            } else {
                sdk?.chat()?.pinMessage(
                    chatMessage.id!!,
                    chatRoomId!!,
                    chatMessage
                ) { result, error ->
                    error?.let {
                        Toast.makeText(holder.itemView.context, it, Toast.LENGTH_SHORT)
                            .show()
                    }
                    result?.let { messageInfo ->
                        Toast.makeText(
                            holder.itemView.context, "Pin Message ${messageInfo.id}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }

        holder.itemBinding.txtMsgTime.text = SimpleDateFormat(
            "MMM d, h:mm a",
            Locale.getDefault()
        ).format(dateTime)

    }

    override fun getItemCount(): Int = chatList.size
    fun getChatMessage(position: Int): LiveLikeChatMessage {
        val msg = chatList[position]
        return LiveLikeChatMessage(message = msg.message).apply {
            isDeleted = msg.isDeleted
//            channel = msg.channel
            customData = msg.customData
            id = msg.id
            imageUrl = msg.imageUrl
            imageHeight = msg.imageHeight
            imageWidth = msg.imageWidth
            nickname = msg.nickname
            quoteMessage = msg.quoteMessage
            senderId = msg.senderId
            timestamp = msg.timestamp
            profilePic = msg.profilePic
        }
    }

}

class CustomChatViewHolder(var itemBinding: CustomChatItemBinding) :
    RecyclerView.ViewHolder(itemBinding.root)
