package com.livelike.livelikedemo.customchat

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.livelike.engagementsdk.LiveLikeContentSession
import com.livelike.livelikedemo.CustomChatActivity
import com.livelike.livelikedemo.LiveLikeApplication
import com.livelike.livelikedemo.LiveLikeApplication.Companion.PREFERENCES_APP_ID
import com.livelike.livelikedemo.channel.Channel
import com.livelike.livelikedemo.databinding.FragmentHomeBinding
import com.livelike.livelikedemo.databinding.HomeChatItemBinding
import java.util.Calendar

class HomeFragment : Fragment() {


    private lateinit var binding: FragmentHomeBinding

    private val adapter = HomeAdapter(object : ItemClickListener {
        override fun itemClick(homeChat: HomeChat) {
            homeChat.msgCount = 0
            (activity as? CustomChatActivity)?.showChatScreen(homeChat)
        }
    })

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rcylChats.adapter = adapter
        val dividerItemDecoration = DividerItemDecoration(
            binding.rcylChats.context,
            (binding.rcylChats.layoutManager as LinearLayoutManager).orientation
        )
        binding.rcylChats.addItemDecoration(dividerItemDecoration)
        if (adapter.sessionsList.isEmpty()) {
            (activity?.application as? LiveLikeApplication)?.let { application ->
                val channelManager = application.channelManager
                val sdk = application.sdk
                val channels = channelManager.getChannels()
                val sessions =
                    ArrayList(
                        channels.map { channel ->
                            HomeChat(
                                channel,
                                sdk.createContentSession(channel.llProgram.toString(), chatEnforceUniqueMessageCallback = true)
                            )
                        }
                    )
                adapter.sessionsList.addAll(sessions)
                adapter.notifyDataSetChanged()
                binding.laySwipe.postDelayed(runnable, 8000L)
            }
        }
        // fetching old messages
        binding.laySwipe.setOnRefreshListener {
            loadUnreadCount()
        }
    }

    private val runnable = { loadUnreadCount() }

    override fun onDestroy() {
        if (this::binding.isInitialized) {
            binding.laySwipe?.removeCallbacks(runnable)
        }
        for (it in adapter.sessionsList) {
            it.session.close()
        }
        super.onDestroy()
    }

    private fun loadUnreadCount() {
        val sharedPref =
            activity?.application?.getSharedPreferences(PREFERENCES_APP_ID, Context.MODE_PRIVATE)
        sharedPref?.let {
            binding.laySwipe?.isRefreshing = true
            adapter.sessionsList.forEachIndexed { index, homeChat ->
                val time = sharedPref.getLong("msg_time_${homeChat.channel.llProgram}", 0L)
                homeChat.session.chatSession.getMessageCount(
                    when (time) {
                        0L -> Calendar.getInstance().timeInMillis
                        else -> time
                    }
                ) { result, error ->
                    homeChat.msgCount = result?.toInt() ?: 0
                    adapter.sessionsList[index] = homeChat
                    binding.laySwipe?.isRefreshing = false
                    binding.rcylChats.post(Runnable { adapter.notifyItemChanged(index) })
                    // adapter.notifyItemChanged(index)
                }
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }
}

class HomeAdapter(private val itemClickListener: ItemClickListener) :
    RecyclerView.Adapter<HomeViewHolder>() {

    val sessionsList = arrayListOf<HomeChat>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {

        val itemBinding =
            HomeChatItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HomeViewHolder(
            itemBinding
        )
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        val homeChat = sessionsList[position]
        holder.itemBinding.txtName.text = homeChat.channel.name
        holder.itemBinding.txtMsgCount.text = homeChat.msgCount.toString()
        holder.itemView.setOnClickListener {
            itemClickListener.itemClick(homeChat)
        }
    }

    override fun getItemCount(): Int = sessionsList.size
}

interface ItemClickListener {
    fun itemClick(homeChat: HomeChat)
}

class HomeViewHolder(var itemBinding: HomeChatItemBinding) :
    RecyclerView.ViewHolder(itemBinding.root)

class HomeChat(val channel: Channel, val session: LiveLikeContentSession) {
    var msgCount = 0
}
