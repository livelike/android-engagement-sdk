package com.livelike.livelikedemo

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.livelike.comment.LiveLikeCommentBoardClient
import com.livelike.comment.LiveLikeCommentClient
import com.livelike.comment.comment
import com.livelike.comment.commentBoard
import com.livelike.comment.models.AddCommentReplyRequestOptions
import com.livelike.comment.models.AddCommentRequestOptions
import com.livelike.comment.models.Comment
import com.livelike.comment.models.CommentSortingOptions
import com.livelike.comment.models.DeleteCommentRequestOptions
import com.livelike.comment.models.GetCommentRepliesRequestOptions
import com.livelike.comment.models.GetCommentsRequestOptions
import com.livelike.comment.models.UpdateCommentRequestOptions
import com.livelike.common.LiveLikeCallback
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.livelikedemo.databinding.ActivityCommentDemoBinding
import com.livelike.livelikedemo.databinding.ItemCommentBinding
import com.livelike.utils.PaginationResponse
import com.livelike.utils.logDebug

private lateinit var binding: ActivityCommentDemoBinding
var activeCommentSession: LiveLikeCommentClient? = null
var parentId: String? = null
private lateinit var boards: LiveLikeCommentBoardClient
private val adapter = CommentAdapter()
private val adapterR = ReplyAdapter()


class CommentDemo : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCommentDemoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.recyclerViewAddComment.layoutManager = LinearLayoutManager(this)
        binding.recyclerViewAddReply.layoutManager = LinearLayoutManager(this)
        boards = (application as LiveLikeApplication).sdk.commentBoard()
        binding.recyclerViewAddComment.adapter = adapter
        binding.recyclerViewAddReply.adapter = adapterR

        activeCommentSession =
            (application as LiveLikeApplication).sdk.comment(
                "d7a27bbd-edd2-4b96-909c-68313607bb03"
            )

        getCommentBoardDetails(LiveLikePagination.FIRST)

        binding.btTest.setOnClickListener {
            getCommentBoardDetails(LiveLikePagination.FIRST)
        }

        binding.btAddComment.setOnClickListener {

            activeCommentSession?.let { active ->

                fun callback(result: Comment?, error: String?) {
                    result?.let {
                        adapter.commentList.add(result)
                        adapter.notifyItemInserted(adapter.commentList.size - 1)
                        binding.recyclerViewAddComment.scrollToPosition(adapter.itemCount - 1)
                    }
                }

                active.addComment(
                    AddCommentRequestOptions(
                        binding.etCommentText.text.toString().trim(),
                    ), ::callback
                )
            }
            binding.etCommentText.text.clear()

        }

        binding.btAddReply.setOnClickListener {
            activeCommentSession?.let { active ->

                fun callback(result: Comment?, error: String?) {
                    result?.let {
                        adapterR.replyList.add(result)
                        adapterR.notifyItemInserted(adapterR.replyList.size - 1)
                        binding.recyclerViewAddReply.scrollToPosition(adapterR.itemCount - 1)
                    }
                }
                Log.d("green", "add reply parent: $parentId")
                parentId?.let {
                    active.addCommentReply(
                        AddCommentReplyRequestOptions(
                            text = binding.etReplyText.text.toString().trim(),
                            parentCommentId = it
                        ), ::callback
                    )

                }
            }
            binding.etReplyText.text.clear()

        }

        binding.btNext.setOnClickListener {
            getCommentBoardDetails(LiveLikePagination.NEXT)
        }
        binding.btPrev.setOnClickListener {
            getCommentBoardDetails(LiveLikePagination.PREVIOUS)
        }

    }

    private fun getCommentBoardDetails(pagination: LiveLikePagination) {
        activeCommentSession?.let { active ->
            // Call the getComments function

            active.getCommentsPage(
                GetCommentsRequestOptions(
                    CommentSortingOptions.OLDEST,
                    repliedSince = if (binding.sortTimeStampReply.isChecked) {
                        binding.etTimestamp.text.toString().trim()
                    } else {
                        null
                    },
                    repliedUntil = if (binding.sortTimeStampReply.isChecked) {
                        binding.etTimestamp.text.toString().trim()
                    } else {
                        null
                    }

                ), pagination
            ) { result, error ->
                result?.let {
                    adapter.commentList.clear()
                    adapter.commentList.addAll(result.results)
                    adapter.notifyDataSetChanged()
                }
            }
        }

    }
}

class CommentAdapter : RecyclerView.Adapter<CommentAdapter.ViewHolder>() {
    val commentList = arrayListOf<Comment>()


    class ViewHolder(var itemBinding: ItemCommentBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            ItemCommentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(
            itemBinding
        )
    }

    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemBinding.tvItemComment.text = commentList[position].text
        holder.itemView.setOnClickListener {
            adapterR.replyList.clear()
            adapterR.notifyDataSetChanged()
            binding.parentText.text = "Parent Message: ${commentList[position].text}"
            parentId = commentList[position].id
            Log.d("green", "parent itemview: $parentId")
            activeCommentSession?.let { active ->
                Log.d("green", "activeCommentSession itemview")
                parentId?.let { it1 ->
                    GetCommentRepliesRequestOptions(
                        it1,
                        CommentSortingOptions.NEWEST
                    )
                }?.let { it2 ->
                    active.getCommentReplies(it2, LiveLikePagination.FIRST) { result, error ->
                        result?.let {
                            Log.d("green", "item click get replies")

                            adapterR.replyList.addAll(result)
                            adapterR.notifyDataSetChanged()
                        }
                    }
                }
            }


        }

    }

    override fun getItemCount(): Int {
        return commentList.size
    }

}

class ReplyAdapter() : RecyclerView.Adapter<ReplyAdapter.ViewHolder>() {

    val replyList = arrayListOf<Comment>()

    class ViewHolder(var itemBinding: ItemCommentBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            ItemCommentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(
            itemBinding
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (replyList[position].isDeleted) {
            holder.itemBinding.tvItemComment.text = "Is Deleted : True"
        } else {
            holder.itemBinding.tvItemComment.text = replyList[position].text
        }
        val com = replyList[position]
        holder.itemBinding.ivDelete.setOnClickListener {
            activeCommentSession?.let { active ->
                active.deleteComment(DeleteCommentRequestOptions(replyList[position].id)) { result, error ->
//                    com.text = "Is Deleted : True"
                    notifyDataSetChanged()
                }
            }
        }

        holder.itemBinding.ivEdit.setOnClickListener {
            binding.etEditcom.visibility = View.VISIBLE
            binding.btedit.visibility = View.VISIBLE

        }

        binding.btedit.setOnClickListener {
            activeCommentSession?.let { active ->
                active.editComment(
                    UpdateCommentRequestOptions(
                        replyList[position].id,
                        text = binding.etEditcom.text.toString()
                    )
                ) { result, error ->
//                    com.text = result?.text.toString()
                    notifyDataSetChanged()
                }
            }
            binding.etEditcom.visibility = View.INVISIBLE
            binding.btedit.visibility = View.INVISIBLE
        }
    }

    override fun getItemCount(): Int {
        return replyList.size
    }
}
