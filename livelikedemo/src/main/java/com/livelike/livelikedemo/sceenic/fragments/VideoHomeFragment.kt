package com.livelike.livelikedemo.sceenic.fragments

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.livelike.engagementsdk.video.model.VideoRoom
import com.livelike.livelikedemo.LiveLikeApplication
import com.livelike.livelikedemo.databinding.FragmentVideoHomeBinding
import com.livelike.livelikedemo.sceenic.SceenicDemoActivity


/**
 * A simple [Fragment] subclass.
 * Use the [VideoHomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class VideoHomeFragment : Fragment() {

    private var roomId: String? = null
    private lateinit var binding: FragmentVideoHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentVideoHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnCreate.setOnClickListener {

            (activity as SceenicDemoActivity).createVideoRoom {result: VideoRoom?, error: String?->
                result?.let { videoRoom ->
                    binding.txtVideoRoomDetails.text = videoRoom.toString()
                    binding.edRoomId.setText(videoRoom.id.toString())
                    roomId = videoRoom.id
                    context?.getSharedPreferences(
                        "video_call_${LiveLikeApplication.selectEnvironmentKey}",
                        Context.MODE_PRIVATE
                    )?.edit()
                        ?.putString(
                            "video_room_id", videoRoom.id
                        )?.apply()
                }
                error?.let {
                    Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
                }
            }
        }
        context?.getSharedPreferences(
            "video_call_${LiveLikeApplication.selectEnvironmentKey}",
            Context.MODE_PRIVATE
        )?.apply {
            getString("video_room_id", null)?.let {
                binding.edRoomId.setText(it)
            }
            getString("user_name", null)?.let {
                binding.edUserName.setText(it)
            }
        }

        binding.edUserName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                (activity as SceenicDemoActivity).name = p0.toString()
                context?.getSharedPreferences(
                    "video_call_${LiveLikeApplication.selectEnvironmentKey}",
                    Context.MODE_PRIVATE
                )?.edit()?.putString("user_name", p0.toString())?.apply()
            }

        })
//        ed_room_id.setText("66d19db0-c9ef-4488-8714-97f3e696b0cf")
        binding.btnDetails.setOnClickListener {
            val id = binding.edRoomId.text.toString()
            if (id.isNotEmpty()) {
                (activity as SceenicDemoActivity).getVideoRoomDetails(id){result: VideoRoom?, error: String?->
                    result?.let {
                        binding.txtVideoRoomDetails.text = it.toString()
                        roomId = it.id
                        context?.getSharedPreferences(
                            "video_call_${LiveLikeApplication.selectEnvironmentKey}",
                            Context.MODE_PRIVATE
                        )?.edit()
                            ?.putString(
                                "video_room_id", it.id
                            )?.apply()
                    }
                    error?.let {
                        Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

        binding.btnConnectVideoRoom.setOnClickListener {
            roomId?.let {
                (activity as SceenicDemoActivity).connectToVideoRoom(it)
            }
        }
    }

}