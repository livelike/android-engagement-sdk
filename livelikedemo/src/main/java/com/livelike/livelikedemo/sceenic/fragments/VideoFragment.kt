package com.livelike.livelikedemo.sceenic.fragments

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.livelike.livelikedemo.LiveLikeApplication
import com.livelike.livelikedemo.databinding.FragmentVideoBinding
import com.livelike.livelikedemo.sceenic.SceenicDemoActivity
import com.livelike.sceenic.plugin.LiveLikeSceenicVideoSession
import com.livelike.sceenic.plugin.extensions.createVideoSession
import com.livelike.sceenic.plugin.utils.VideoConnectionState

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"

/**
 * A simple [Fragment] subclass.
 * Use the [VideoFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class VideoFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var roomId: String? = null
    private var requestPermissionLauncher: ActivityResultLauncher<Array<String>>? = null
    private var isPermissionGranted = false
    private var session: LiveLikeSceenicVideoSession? = null
    private lateinit var binding: FragmentVideoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestPermissionLauncher =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
                isPermissionGranted = permissions.values.reduce { b1, b2 -> b1 && b2 }
                if (isPermissionGranted) {
                    setSession()
                }
            }
        arguments?.let {
            roomId = it.getString(ARG_PARAM1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentVideoBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        session = (activity?.application as? LiveLikeApplication)?.sdk?.createVideoSession()
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.RECORD_AUDIO
            ) +
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissionLauncher?.launch(
                arrayOf(
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.CAMERA
                )
            )
            return
        }
        setSession()
    }

    private fun setSession() {
        session?.let { videoSession ->
            binding.videoView.setSession(videoSession)
            videoSession.videoConnectionStateEventStream.subscribe(this) {
                if (it == VideoConnectionState.DISCONNECTED) {
                    activity?.onBackPressed()
                }
            }
            roomId?.let { id ->
                videoSession.joinVideoRoom(id) { result, error ->
                    result?.let {
                        (activity as? SceenicDemoActivity)?.name?.let {
                            videoSession.setDisplayName(it)
                        }
                    }
                }

            }
            videoSession.localParticipantEventStream.subscribe(this) {
                it?.let {
                    println("VideoFragment.setSession ${it.isVideoEnabled},${it.isAudioEnabled}")
                }
            }
            videoSession.errorEventStream.subscribe(this) {
                it?.let {
                    Toast.makeText(context, it.toString(), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        session?.pause()
    }

    override fun onResume() {
        super.onResume()
        session?.resume()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        session?.leaveVideoRoom()
    }

    companion object {
        @JvmStatic
        fun newInstance(roomId: String) =
            VideoFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, roomId)
                }
            }
    }
}