package com.livelike.livelikedemo.sceenic

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.livelike.engagementsdk.video
import com.livelike.engagementsdk.video.CreateVideoRoomRequest
import com.livelike.engagementsdk.video.model.VideoRoom
import com.livelike.livelikedemo.LiveLikeApplication
import com.livelike.livelikedemo.R
import com.livelike.livelikedemo.sceenic.fragments.VideoFragment
import com.livelike.livelikedemo.sceenic.fragments.VideoHomeFragment
import kotlin.random.Random

class SceenicDemoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sceenic_demo)
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, VideoHomeFragment())
            .addToBackStack("videoHome")
            .commit()
    }

    var name: String? = null

    fun connectToVideoRoom(roomId: String) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, VideoFragment.newInstance(roomId))
            .addToBackStack("videoHome")
            .commit()
    }

    fun createVideoRoom(liveLikeCallback: com.livelike.common.LiveLikeCallback<VideoRoom>) {
        val i = Random.nextInt(10000)
        (application as LiveLikeApplication).sdk.video()
            .createVideoRoom(
                CreateVideoRoomRequest("$i", "${i * i}"), liveLikeCallback
            )
    }

    fun getVideoRoomDetails(id: String, liveLikeCallback: com.livelike.common.LiveLikeCallback<VideoRoom>) {
        (application as LiveLikeApplication).sdk.video().getVideoRoom(
            id,
            liveLikeCallback
        )
    }
}