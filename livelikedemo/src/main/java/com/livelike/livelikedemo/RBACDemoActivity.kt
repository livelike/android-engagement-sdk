package com.livelike.livelikedemo

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.livelikedemo.databinding.ActivityRbacBinding
import com.livelike.rbac.LiveLikeRBACClient
import com.livelike.rbac.models.AssignRoleRequestOptions
import com.livelike.rbac.models.ListAssignedRoleRequestOptions
import com.livelike.rbac.models.UnAssignRoleRequestOptions
import com.livelike.rbac.rbac

class RBACDemoActivity: AppCompatActivity() {
    private lateinit var binding: ActivityRbacBinding

    private lateinit var rbac: LiveLikeRBACClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        rbac = (application as LiveLikeApplication).sdk.rbac()

        binding = ActivityRbacBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setListeners()
    }


    private fun setListeners(){
        binding.assignedRoles.setOnClickListener {
            rbac.getAssignedRoles(
                ListAssignedRoleRequestOptions(profileId = "53452795-ec51-49c6-aa84-e750e052b7bd"),
                LiveLikePagination.FIRST
            ){ result, error ->
                error?.let {
                      Toast.makeText(applicationContext,it,Toast.LENGTH_SHORT).show()
                }
                result?.let {assignedRoles ->
                    runOnUiThread {
                        val roleNames = assignedRoles.results.map { it.rbacRole.name }.toTypedArray()

                        AlertDialog.Builder(this)
                            .setTitle("Assigned Roles")
                            .setItems(roleNames) { dialog, which ->

                                val selectedRole = roleNames[which]
                                Toast.makeText(applicationContext, "Selected role: $selectedRole", Toast.LENGTH_SHORT).show()
                            }
                            .create()
                            .show()
                    }
                }
            }
        }

        binding.createRole.setOnClickListener {
            rbac.createRoleAssignment(
                AssignRoleRequestOptions(profileId = binding.profileId.text.toString(), roleKey = binding.roleKey.text.toString(),
                    resourceKind = binding.resourceKind.text.toString(),
                    resourceKey = binding.resourceKey.text.toString())){ result, error ->
                error?.let {
                    Toast.makeText(applicationContext,it,Toast.LENGTH_SHORT).show()
                }
                result?.let {assignedRoles ->
                    runOnUiThread {
                        Toast.makeText(applicationContext, "Role assigned successfully ${assignedRoles}", Toast.LENGTH_SHORT).show()
                    }
                }
            }

        }


        binding.deleteRoles.setOnClickListener {
            rbac.deleteRoleAssignment(UnAssignRoleRequestOptions(
                assignedRoleId = binding.assignedRoleId.text.toString()
            )){ result, error ->
                error?.let {
                    Toast.makeText(applicationContext,"could not delete role --${it}",Toast.LENGTH_SHORT).show()
                }

                result?.let {
                    runOnUiThread {
                        Toast.makeText(applicationContext, "succesfully deleted role", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }


    }
}