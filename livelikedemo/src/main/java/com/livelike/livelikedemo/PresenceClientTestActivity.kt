package com.livelike.livelikedemo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.livelike.common.user
import com.livelike.livelikedemo.databinding.ActivityPresenceClientTestBinding
import com.livelike.mobile.presence.LiveLikePresenceClient
import com.livelike.mobile.presence.models.PresenceEvent
import com.livelike.mobile.presence.presenceClient
import com.livelike.utils.logDebug
import kotlinx.coroutines.launch

class PresenceClientTestActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPresenceClientTestBinding

    private lateinit var presenceClient: LiveLikePresenceClient

    private lateinit var adapter: PresenceAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPresenceClientTestBinding.inflate(layoutInflater)
        setContentView(binding.root)

        presenceClient = (application as LiveLikeApplication).sdk.presenceClient {
                //Ready to use
        }

        binding.watchChannel.setOnClickListener {
            presenceClient.subscribeForPresence(
                setOf(binding.channelName.text.toString().trim()),
                callback = ::onUpdate
            )
        }

        binding.joinChannel.setOnClickListener {
            presenceClient.joinChannels(
                setOf(binding.channelName.text.toString().trim()),
            )
        }

        binding.leaveChannel.setOnClickListener {
            presenceClient.leaveChannels(setOf(binding.channelName.text.toString().trim()))
        }

        binding.setAttributes.setOnClickListener {
            presenceClient.setAttributes(
                setOf(binding.channelName.text.toString().trim()),
                buildAttributes()
            )
        }

        binding.whereNow.setOnClickListener {
            val sdk = (application as LiveLikeApplication).sdk
            sdk.sdkScope.launch {
                sdk.user().currentProfileOnce.invoke().let { pair ->
                    pair.id.let { userId ->
                        presenceClient.whereNow(userId) { result, error ->
                            error?.let(::showToast)
                            result?.joinToString()?.let {
                                showToast("channels: $it")
                            }
                        }
                    }
                }
            }
        }

        binding.getAttributes.setOnClickListener {
            val sdk = (application as LiveLikeApplication).sdk
            sdk.sdkScope.launch {
                sdk.user().currentProfileOnce.invoke().let { pair ->
                    pair.id.let { userId ->
                        presenceClient.getAttributes(
                            userId,
                            setOf(binding.channelName.text.toString().trim())
                        ) { result, error ->
                            error?.let(::showToast)
                            showToast("attributes by channel: $result")
                        }
                    }
                }
            }
        }

        binding.hereNow.setOnClickListener {
            presenceClient.hereNow(
                setOf(binding.channelName.text.toString().trim())
            ) { result, error ->
                error?.let(::showToast)
                showToast("here now: $result")
            }
        }
        adapter = PresenceAdapter()
        binding.logView.adapter = adapter
    }

    private fun buildAttributes() = binding.attributes.text.toString()
        .split(",")
        .map { it.trim() }
        .zipWithNext()
        .filterIndexed { index, _ ->
            index % 2 == 0
        }
        .toMap()

    private fun onUpdate(presenceEvent: PresenceEvent) {
        logDebug { presenceEvent.toOutputString() }
        runOnUiThread {
            adapter.addEvent(presenceEvent)
            binding.logView.smoothScrollToPosition(0)
        }
    }

    override fun onDestroy() {
        presenceClient.destroy()
        super.onDestroy()
    }

    private fun showToast(message: String?) {
        message?.let {
            runOnUiThread {
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            }
        }
    }

}

class PresenceAdapter : RecyclerView.Adapter<PresenceAdapter.ViewHolder>() {

    private var log = mutableListOf<PresenceEvent>()

    class ViewHolder(val textView: TextView) : RecyclerView.ViewHolder(textView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.activity_presence_client_test_item, parent, false) as TextView
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        log[position].let { presenceEvent ->
            holder.textView.text = presenceEvent.toOutputString()
        }
    }

    override fun getItemCount(): Int {
        return log.count()
    }

    fun addEvent(event: PresenceEvent) {
        log.add(0, event)
        notifyItemInserted(0)
    }

}

private fun PresenceEvent.toOutputString() = """
    type ${javaClass.simpleName}    
    userId $userId
    channel $channel
    attributes $attributes
""".trimIndent()


