package com.livelike.livelikedemo

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.livelike.comment.LiveLikeCommentClient
import com.livelike.comment.comment
import com.livelike.comment.commentBoard
import com.livelike.comment.models.AddCommentReplyRequestOptions
import com.livelike.comment.models.AddCommentRequestOptions
import com.livelike.comment.models.Comment
import com.livelike.comment.models.CommentSortingOptions
import com.livelike.comment.models.CreateCommentBanRequestOptions
import com.livelike.comment.models.CreateCommentBoardRequestOptions
import com.livelike.comment.models.DeleteCommentBanRequestOption
import com.livelike.comment.models.DeleteCommentBoardRequestOptions
import com.livelike.comment.models.DeleteCommentRequestOptions
import com.livelike.comment.models.GetBanDetailsRequestOptions
import com.livelike.comment.models.GetCommentRepliesRequestOptions
import com.livelike.comment.models.GetCommentsRequestOptions
import com.livelike.comment.models.ListCommentBoardBanRequestOptions
import com.livelike.comment.models.UpdateCommentBoardRequestOptions
import com.livelike.comment.models.UpdateCommentRequestOptions
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.livelikedemo.databinding.ActivityBoardsTestBinding

private const val NO_BOARD_SET = "no active board set"
private const val NO_COMMENT_SET = "no active comment set"

class CommentBoardsTestActivity : AppCompatActivity() {

    private var activeComment: Comment? = null
    private var activeCommentBoard: com.livelike.comment.models.CommentBoard? = null
    private var activeCommentSession: LiveLikeCommentClient? = null

    private lateinit var boards: com.livelike.comment.LiveLikeCommentBoardClient
    private lateinit var binding: ActivityBoardsTestBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityBoardsTestBinding.inflate(layoutInflater)
        setContentView(binding.root)

        boards = (application as LiveLikeApplication).sdk.commentBoard()

        binding.createBoard.setOnClickListener { _ ->
            boards.createCommentBoard(
                CreateCommentBoardRequestOptions(
                    binding.customId.text.toString().trim(),
                    binding.customTitle.text.toString().trim(),
                    binding.allowComments.isChecked,
                    try {
                        binding.replyDepth.text.toString().toInt()
                    } catch (_: Exception) {
                        0
                    }
                )
            ) { result, error ->
                showToast(error, "create")
                showToast(result?.toString(), "create")
                result?.let {
                    activeCommentBoard = result
                    activeCommentSession = it.id.let { it1 ->
                        (application as LiveLikeApplication).sdk.comment(
                            it1
                        )
                    }
                }
            }
        }

        binding.getBoard.setOnClickListener { _ ->
            boards.getCommentBoards(LiveLikePagination.FIRST) { result, error ->
                showToast(error, "get board")
                result?.let {
                    showSelection(it, "get board") { choice ->
                        activeCommentBoard = choice
                        activeCommentSession = choice.id.let { it1 ->
                            (application as LiveLikeApplication).sdk.comment(
                                it1,
                            )
                        }
                    }
                }
            }
        }

        binding.updateBoard.setOnClickListener {
            activeCommentBoard?.let { active ->
                boards.updateCommentBoard(
                    UpdateCommentBoardRequestOptions(
                        active.id,
                        binding.updateCustomId.text.toString().trim().let(::nullOverEmpty),
                        binding.updateTitle.text.toString().trim().let(::nullOverEmpty),
                        binding.updateAllowComments.isChecked,
                        try {
                            binding.updateReplyDepth.text.toString().toInt()
                        } catch (_: Exception) {
                            0
                        }
                    )
                ) { result, error ->
                    showToast(error, "update")
                    showToast(result?.toString(), "update")
                }
            } ?: showToast(NO_BOARD_SET, "error")
        }

        binding.deleteBoard.setOnClickListener {
            activeCommentBoard?.let { active ->
                boards.deleteCommentBoards(DeleteCommentBoardRequestOptions(active.id)) { result, error ->
                    showToast(error, "delete")
                    result?.let {
                        showToast("deleted", "delete")
                    }
                }
            } ?: showToast(NO_BOARD_SET, "error")
        }

        binding.createCommentButton.setOnClickListener {
            activeCommentSession?.let { active ->
                fun callback(result: Comment?, error: String?) {
                    showToast(
                        error, "create comment ${
                            when (binding.parentCommentId.text.toString().trim()
                                .let(::nullOverEmpty)) {
                                null -> ""
                                else -> "reply"
                            }
                        }"
                    )
                    result?.let {
                        showSelection(
                            listOf(result), "create comment ${
                                when (binding.parentCommentId.text.toString().trim()
                                    .let(::nullOverEmpty)) {
                                    null -> ""
                                    else -> "reply"
                                }
                            }"
                        ) {
                            activeComment = it
                            binding.parentCommentId.setText(it.id)
                        }
                    }
                }
                binding.parentCommentId.text.toString().trim().let(::nullOverEmpty)?.let {
                    active.addCommentReply(
                        AddCommentReplyRequestOptions(
                            text = binding.newCommentText.text.toString().trim(),
                            parentCommentId = it
                        ), ::callback
                    )
                } ?: active.addComment(
                    AddCommentRequestOptions(
                        binding.newCommentText.text.toString().trim(),
                    ), ::callback
                )
            } ?: showToast(NO_BOARD_SET, "error")
        }

        binding.getCommentsButton.setOnClickListener {
            activeCommentSession?.let { active ->
                active.getCommentsPage(
                    GetCommentsRequestOptions(
                        getSortOrder()
                    ), LiveLikePagination.FIRST
                ) { result, error ->
                    showToast(error, "get comments")
                    result?.results?.let {
                        showSelection(it, "get comments") { result ->
                            activeComment = result
                            binding.parentCommentId.setText(result.id)
                        }
                    }
                }
            } ?: showToast(NO_BOARD_SET, "error")
        }

        binding.getCommentsReplyButton.setOnClickListener {
            activeCommentSession?.let { active ->
                activeComment?.id?.let { it ->
                    active.getCommentReplies(
                        GetCommentRepliesRequestOptions(
                            it,
                            getSortOrder()
                        ),
                        LiveLikePagination.FIRST
                    ) { result, error ->
                        showToast(error, "get comment replies")
                        result?.let {
                            showSelection(it, "get comments replies") { result ->
                                activeComment = result
                                binding.parentCommentId.setText(result.id)
                            }
                        }
                    }
                } ?: showToast(NO_COMMENT_SET, "error")
            } ?: showToast(NO_BOARD_SET, "error")
        }

        binding.editCommentButton.setOnClickListener {
            activeCommentSession?.let { active ->
                activeComment?.id?.let {
                    active.editComment(
                        UpdateCommentRequestOptions(
                            commentId = it, binding.editCommentText.text.toString().trim()
                        )
                    ) { result, error ->
                        showToast(error, "edit comment")
                        showToast(result?.toString(), "edit comment")
                    }
                } ?: showToast(NO_COMMENT_SET, "error")
            } ?: showToast(NO_BOARD_SET, "error")
        }


        binding.deleteActiveCommentButton.setOnClickListener {
            activeCommentSession?.let { active ->
                activeComment?.id?.let {
                    active.deleteComment(
                        DeleteCommentRequestOptions(
                            commentId = it
                        )
                    ) { result, error ->
                        error?.let { showToast(error, "Delete Active Comment") }
                        result?.let {
                            showToast(activeComment?.text, "Delete Active Comment")
                            activeComment = null
                        }
                    }
                } ?: showToast(NO_COMMENT_SET, "error")
            } ?: showToast(NO_BOARD_SET, "error")
        }


        binding.createBanButton.setOnClickListener { _ ->
            boards.createCommentBoardBan(
                CreateCommentBanRequestOptions(
                    binding.profileId.text.toString().trim(),
                    binding.commentBoardId.text.toString().trim().let(::nullOverEmpty),
                    binding.description.text.toString().trim().let(::nullOverEmpty)
                )
            ) { result, error ->
                showToast(error, "comment board ban")
                showToast(result?.toString(), "comment board ban")
            }
        }

        binding.listBanButton.setOnClickListener {
            boards.getCommentBoardBans(
                ListCommentBoardBanRequestOptions(
                    binding.banProfileId.text.toString().trim().let(::nullOverEmpty),
                    binding.banCommentBoardId.text.toString().trim().let(::nullOverEmpty)
                ), LiveLikePagination.FIRST
            ) { result, error ->
                showToast(error, "comment board ban")
                showToast(result?.toString(), "comment board ban list")
            }
        }

        binding.deleteBanButton.setOnClickListener {
            boards.deleteCommentBoardBan(
                DeleteCommentBanRequestOption(binding.commentBoardBanId.text.toString().trim())
            ) { result, error ->
                error?.let { showToast(error, "delete comment board ban") }
                result?.let {
                    showToast("Deleted:", "comment board ban deleted")
                }
            }
        }

        binding.getDetailBanButton.setOnClickListener {
            boards.getCommentBoardBan(
                GetBanDetailsRequestOptions(
                    binding.commentBoardBanId.text.toString().trim()
                )
            ) { result, error ->
                showToast(error, "ban detail not found!")
                showToast(result?.toString(), "comment board ban detail")
            }
        }
    }


    private fun getSortOrder(): CommentSortingOptions {
        return if (binding.sortOrder.isChecked) {
            CommentSortingOptions.NEWEST
        } else {
            CommentSortingOptions.OLDEST
        }
    }

    private fun <T> showSelection(data: List<T>, title: String, onSelect: (T) -> Unit) {
        runOnUiThread {
            AlertDialog.Builder(this)
                .setTitle(title)
                .setItems(data.map { it.toString() }.toTypedArray()) { _, pos ->
                    onSelect(data[pos])
                }
                .create()
                .show()
        }
    }

    private fun showToast(message: String?, title: String) {
        message?.let {
            runOnUiThread {
                AlertDialog.Builder(this)
                    .setTitle(title)
                    .setMessage(it)
                    .create()
                    .show()
            }
        }
    }

}

fun nullOverEmpty(s: String): String? {
    return if (s != "") {
        s
    } else null
}
