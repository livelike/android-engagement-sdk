package com.livelike.livelikedemo.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.fetchWidgetDetails
import com.livelike.engagementsdk.widget.data.models.PredictionWidgetUserInteraction
import com.livelike.engagementsdk.widget.view.WidgetView
import com.livelike.livelikedemo.databinding.ItemLayoutBinding

class UnclaimedInteractionAdapter(
    private val context: Context,
    private val engagementSDK: EngagementSDK?,
    private val interactionList: List<PredictionWidgetUserInteraction>
) : RecyclerView.Adapter<UnclaimedInteractionAdapter.InteractionViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): UnclaimedInteractionAdapter.InteractionViewHolder {

        val itemBinding =
            ItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return InteractionViewHolder(
            itemBinding
        )

    }

    override fun onBindViewHolder(
        holder: UnclaimedInteractionAdapter.InteractionViewHolder, position: Int
    ) {
        val interaction = interactionList[position]
        engagementSDK?.fetchWidgetDetails(interaction.widgetId,
            interaction.widgetKind
        ) { result, error ->
            if (result != null) {
                Log.d("unclaimed", "widget loaded")
                holder.widgetKindTv.text = "Interacted Widget id:${result.id}"
                holder.widgetIdTv.text = "Question: ${result.question}"

                val selectedOptionId = interaction.optionId
                val options = result.options ?: emptyList()
                for (option in options) {
                    if (option?.id == selectedOptionId) {
                        val optionName = option.description
                        holder.widgetAnswerTv.text = "Your answer was : $optionName"
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return interactionList.size
    }

    inner class InteractionViewHolder(itemBinding: ItemLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        val widgetKindTv: TextView = itemBinding.widgetKindTv
        val widgetIdTv: TextView = itemBinding.widgetIdTv
        val widgetAnswerTv: TextView = itemBinding.widgetAnswerTv
        val widgetView: WidgetView = itemBinding.widgetView
    }
}
