package com.livelike.livelikedemo.channel

import android.content.Context
import android.os.Build
import android.util.Log
import androidx.annotation.NonNull
import com.livelike.engagementsdk.BuildConfig
import com.livelike.livelikedemo.LiveLikeApplication.Companion.PREFERENCES_APP_ID
import com.livelike.network.NetworkApiClient
import com.livelike.network.NetworkClientConfig
import kotlinx.coroutines.*
import org.json.JSONException
import org.json.JSONObject
import java.net.URL
import com.livelike.network.NetworkResult

class ChannelManager(private val channelConfigUrl: String, private val appContext: Context) {

    private var job: Job? = null
    var nextUrl: String? = null
    var previousUrl: String? = null
    private val channelSelectListeners = mutableListOf<(Channel) -> Unit>()
    private val channelList: MutableList<Channel> = mutableListOf()

    @NonNull
    var selectedChannel: Channel = NONE_CHANNEL
        set(channel) {
            field = channel
            persistChannel(channel.name)
            for (listener in channelSelectListeners)
                listener.invoke(channel)
        }

    private val uiScope = CoroutineScope(Dispatchers.Main + SupervisorJob())

    init {
        loadClientConfig()
    }

    fun loadClientConfig(url: String? = null) {
        val networkApiClient = NetworkApiClient.getInstance(
            NetworkClientConfig("Android/${BuildConfig.SDK_VERSION} ${Build.MODEL}/${Build.VERSION.SDK_INT}"),
            Dispatchers.IO
        )
        job = uiScope.launch {
            val result = networkApiClient.get(url ?: channelConfigUrl)
            if (result is NetworkResult.Success) {
                val savedChannel =
                    appContext.getSharedPreferences(PREFERENCES_APP_ID, Context.MODE_PRIVATE)
                        .getString(PREFERENCE_CHANNEL_ID, "")
                try {
                    val json = JSONObject(result.data)
                    val results = json.getJSONArray("results")
                    val nextUrl = json.getString("next")
                    if (nextUrl != "null")
                        this@ChannelManager.nextUrl = nextUrl
                    else
                        this@ChannelManager.nextUrl = null
                    val previousUrl = json.getString("previous")
                    if (previousUrl != "null")
                        this@ChannelManager.previousUrl = previousUrl
                    else
                        this@ChannelManager.previousUrl = null

                    if (results.length() > 0) {
                        channelList.clear()
                    }
                    for (i in 0 until results.length()) {
                        val channel = getChannelFor(results.getJSONObject(i))
                        channel.let {
                            channelList.add(channel)
                            if (savedChannel == channel.name) {
                                selectedChannel = channel
                            }
                        }
                    }
                } catch (e: JSONException) {
                    e.message?.let { Log.e("ChannelMger", it) }
                }
            } else if (result is NetworkResult.Error) {
                when (result) {
                    is NetworkResult.Error.HttpError -> {
                        result.errorBody?.let {
                            Log.e("ChannelMgr", it)
                        }
                    }
                    is NetworkResult.Error.NetworkError -> {
                        result.exception.message?.let {
                            Log.e("ChannelMgr", it)
                        }
                    }
                    is NetworkResult.Error.UnknownError -> {
                        result.exception.message?.let {
                            Log.e("ChannelMgr", it)
                        }
                    }
                }
            }
        }
    }

    private fun getChannelFor(channelData: JSONObject): Channel {
        val streamUrl = channelData.getString("stream_url")
        val url: URL? = when {
            streamUrl.isNullOrEmpty() || streamUrl.equals("null") -> null
            else -> URL(streamUrl)
        }
        return Channel(
            channelData.getString("title"),
            url,
            null,
            channelData.getString("id")
        )
    }

    private fun persistChannel(channelName: String) {
        appContext.getSharedPreferences(PREFERENCES_APP_ID, Context.MODE_PRIVATE)
            .edit()
            .putString(PREFERENCE_CHANNEL_ID, channelName)
            .apply()
    }

    fun getChannels(): ArrayList<Channel> {
        return ArrayList(channelList)
    }

    fun close() {
        job?.cancel()
        uiScope.cancel()
    }

    companion object {
        private const val PREFERENCE_CHANNEL_ID = "ChannelId"
        val NONE_CHANNEL = Channel("None - Clear Session")
    }
}


data class Channel(
    val name: String,
    val video: URL? = null,
    val thumbnail: URL? = null,
    val llProgram: String? = null
)
