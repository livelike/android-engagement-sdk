package com.livelike.livelikedemo

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.livelike.engagementsdk.BuildConfig
import com.livelike.engagementsdk.LiveLikeContentSession
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.core.services.messaging.proxies.LiveLikeWidgetEntity
import com.livelike.engagementsdk.core.services.messaging.proxies.WidgetInterceptor
import com.livelike.engagementsdk.widget
import com.livelike.engagementsdk.widget.LiveLikeWidgetViewFactory
import com.livelike.engagementsdk.widget.domain.Reward
import com.livelike.engagementsdk.widget.domain.RewardSource
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.widgetModel.AlertWidgetModel
import com.livelike.engagementsdk.widget.widgetModel.CheerMeterWidgetmodel
import com.livelike.engagementsdk.widget.widgetModel.FollowUpWidgetViewModel
import com.livelike.engagementsdk.widget.widgetModel.ImageSliderWidgetModel
import com.livelike.engagementsdk.widget.widgetModel.NumberPredictionFollowUpWidgetModel
import com.livelike.engagementsdk.widget.widgetModel.NumberPredictionWidgetModel
import com.livelike.engagementsdk.widget.widgetModel.PollWidgetModel
import com.livelike.engagementsdk.widget.widgetModel.PredictionWidgetViewModel
import com.livelike.engagementsdk.widget.widgetModel.QuizWidgetModel
import com.livelike.engagementsdk.widget.widgetModel.SocialEmbedWidgetModel
import com.livelike.engagementsdk.widget.widgetModel.TextAskWidgetModel
import com.livelike.engagementsdk.widget.widgetModel.VideoAlertWidgetModel
import com.livelike.livelikedemo.channel.ChannelManager
import com.livelike.livelikedemo.customwidgets.CustomAlertWidget
import com.livelike.livelikedemo.customwidgets.CustomCheerMeter
import com.livelike.livelikedemo.customwidgets.CustomImageSlider
import com.livelike.livelikedemo.customwidgets.CustomNumberPredictionWidget
import com.livelike.livelikedemo.customwidgets.CustomPollWidget
import com.livelike.livelikedemo.customwidgets.CustomPredictionWidget
import com.livelike.livelikedemo.customwidgets.CustomQuizWidget
import com.livelike.livelikedemo.customwidgets.CustomSocialEmbed
import com.livelike.livelikedemo.customwidgets.CustomTextAskWidget
import com.livelike.livelikedemo.customwidgets.SponsoredWidgetView
import com.livelike.livelikedemo.databinding.ActivityEachWidgetTypeWithVarianceBinding
import com.livelike.livelikedemo.databinding.RcylItemHeaderBinding
import com.livelike.livelikedemo.databinding.RcylListItemBinding
import com.livelike.livelikedemo.models.AlertRequest
import com.livelike.livelikedemo.models.CheerMeterRequestResponse
import com.livelike.livelikedemo.models.EmojiSliderRequest
import com.livelike.livelikedemo.models.NumberPredictionRequest
import com.livelike.livelikedemo.models.PollRequestResponse
import com.livelike.livelikedemo.models.PredictionRequest
import com.livelike.livelikedemo.models.QuizRequest
import com.livelike.livelikedemo.models.TextAskRequest
import com.livelike.livelikedemo.utils.DialogUtils.showDialog
import com.livelike.livelikedemo.utils.ThemeRandomizer
import com.livelike.network.NetworkApiClient
import com.livelike.network.NetworkClientConfig
import com.livelike.serialization.toJsonString
import com.livelike.utils.logDebug
import com.livelike.widget.CreateAlertRequest
import com.livelike.widget.CreateImageNumberPredictionFollowUpRequest
import com.livelike.widget.CreateImageNumberPredictionRequest
import com.livelike.widget.CreateImagePollRequest
import com.livelike.widget.CreateImagePredictionRequest
import com.livelike.widget.CreateImageQuizRequest
import com.livelike.widget.CreateRichPostRequest
import com.livelike.widget.CreateTextAskRequest
import com.livelike.widget.CreateTextPollRequest
import com.livelike.widget.CreateTextPredictionRequest
import com.livelike.widget.CreateTextQuizRequest
import com.livelike.widget.LiveLikeWidgetClient
import com.livelike.widget.PublishWidgetRequest
import com.livelike.widget.UpdateImageNumberPredictionOptionRequest
import com.livelike.widget.UpdateImagePredictionOptions
import com.livelike.widget.UpdateTextPredictionOptions
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Calendar
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine
import kotlin.random.Random


class WidgetOnlyActivity : AppCompatActivity() {
    private lateinit var session: LiveLikeContentSession
    private lateinit var channelManager: ChannelManager
    private lateinit var liveLikeWidgetClient: LiveLikeWidgetClient

    private val twoOptions = "2 Options"
    private val fourOptions = "4 Options"
    private val textQuiz = "text-quizzes"
    private val imgQuiz = "image-quizzes"
    private val txtPoll = "text-polls"
    private val imgPoll = "image-polls"
    private val txtPrediction = "text-predictions"
    private val imgPrediction = "image-predictions"
    private val emojiSlider = "emoji-sliders"
    private val alerts = "alerts"
    private val cheerMeter = "cheer-meters"
    private val textAsk = "text-asks"
    private val imgNumberPrediction = "image-number-predictions"
    private val richPost = "rich-posts"
    private lateinit var binding: ActivityEachWidgetTypeWithVarianceBinding
    private var showNotification: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEachWidgetTypeWithVarianceBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        showNotification = intent.getBooleanExtra("showNotification", true)

        //setContentView(R.layout.activity_each_widget_type_with_variance)
        channelManager = (application as LiveLikeApplication).channelManager
        session = (application as LiveLikeApplication).createPublicSession(
            channelManager.selectedChannel.llProgram.toString(), allowTimeCodeGetter = false,
            widgetInterceptor = when (showNotification) {
                true -> dialog
                else -> null
            },
        )
        liveLikeWidgetClient = (application as LiveLikeApplication).sdk.widget()
        val adapter = HeaderAdapter(
            binding.progressView, channelManager.selectedChannel.llProgram.toString(), listOf(
                PostType("Text Quiz", true),
                PostType(twoOptions, false, textQuiz, 2),
                PostType(fourOptions, false, textQuiz, 4),

                PostType("Image Quiz", true),
                PostType(twoOptions, false, imgQuiz, 2),
                PostType(fourOptions, false, imgQuiz, 4),

                PostType("Text Poll", true),
                PostType(twoOptions, false, txtPoll, 2),
                PostType(fourOptions, false, txtPoll, 4),

                PostType("Image Poll", true),
                PostType(twoOptions, false, imgPoll, 2),
                PostType(fourOptions, false, imgPoll, 4),

                PostType("Text Prediction", true),
                PostType(twoOptions, false, txtPrediction, 2),
                PostType(fourOptions, false, txtPrediction, 4),

                PostType("Image Prediction", true),
                PostType(twoOptions, false, imgPrediction, 2),
                PostType(fourOptions, false, imgPrediction, 4),

                PostType("Image Slider", true),
                PostType("1 Options", false, emojiSlider, 1),
                PostType(twoOptions, false, emojiSlider, 2),
                PostType("3 Options", false, emojiSlider, 3),
                PostType(fourOptions, false, emojiSlider, 4),
                PostType("5 Options", false, emojiSlider, 5),

                PostType("Alert", true),
                PostType("Text Only", false, alerts, 1),
                PostType("Image Only", false, alerts, 2),
                PostType("Text and Image", false, alerts, 3),
                PostType("Text,Image and Universal URL(random)", false, alerts, 4),

                PostType("Cheer Meter", true),
                PostType(twoOptions, false, cheerMeter, 2),

                PostType("Text Ask", true),
                PostType(twoOptions, false, textAsk, 2),

                PostType("Image Prediction", true),
                PostType("Image Number Prediction", false, imgNumberPrediction, 2),

                PostType("Rich Post", true),
                PostType("Rich Post", false, richPost, 2)

            )
        )
        binding.rcylView.adapter = adapter
        val jsonTheme = intent.getStringExtra("jsonTheme")
        if (jsonTheme != null) {
            Toast.makeText(
                applicationContext, "JSON Theme Customization is hold for now", Toast.LENGTH_LONG
            ).show()
            try {
                binding.widgetView.applyTheme(ThemeRandomizer.themesList.last())
            } catch (e: Exception) {
                Toast.makeText(applicationContext, "${e.message}", Toast.LENGTH_LONG).show()
            }
        }

        val rnd = java.util.Random()
        binding.btnChangeBackground.setOnClickListener {
            if (rnd.nextBoolean()) {
                val color: Int =
                    Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
                binding.fmLay.setBackgroundColor(color)
            } else {
                val list = arrayListOf(
                    R.drawable.background1, R.drawable.background2, R.drawable.background3
                )
                binding.fmLay.setBackgroundResource(list.random())
            }
        }
        lifecycleScope.launch {
            session.widgetFlow.collect { liveLikeWidget ->
                println("Widget:${liveLikeWidget.id},${liveLikeWidget.programId},${liveLikeWidget.options?.size}")
            }
        }

        binding.widgetView.setSession(session)
        if (intent.getBooleanExtra(
                "customCheerMeter", false
            )
        ) binding.widgetView.widgetViewFactory = object : LiveLikeWidgetViewFactory {
            override fun createCheerMeterView(cheerMeterWidgetModel: CheerMeterWidgetmodel): View? {
                return CustomCheerMeter(this@WidgetOnlyActivity).apply {
                    this.cheerMeterWidgetModel = cheerMeterWidgetModel
                }
            }

            override fun createAlertWidgetView(alertWidgetModel: AlertWidgetModel): View? {
                return SponsoredWidgetView(
                    this@WidgetOnlyActivity, CustomAlertWidget(this@WidgetOnlyActivity).apply {
                        alertModel = alertWidgetModel
                    }, alertWidgetModel.widgetData
                )
            }

            override fun createQuizWidgetView(
                quizWidgetModel: QuizWidgetModel, isImage: Boolean
            ): View? {
                return CustomQuizWidget(this@WidgetOnlyActivity).apply {
                    this.quizWidgetModel = quizWidgetModel
                    this.isImage = isImage
                }
            }

            override fun createPredictionWidgetView(
                predictionViewModel: PredictionWidgetViewModel, isImage: Boolean
            ): View? {
                return CustomPredictionWidget(this@WidgetOnlyActivity).apply {
                    this.predictionWidgetViewModel = predictionViewModel
                    this.isImage = isImage
                }
            }

            override fun createPredictionFollowupWidgetView(
                followUpWidgetViewModel: FollowUpWidgetViewModel, isImage: Boolean
            ): View? {
                return CustomPredictionWidget(this@WidgetOnlyActivity).apply {
                    this.followUpWidgetViewModel = followUpWidgetViewModel
                    this.isImage = isImage
                    this.isFollowUp = true
                }
            }

            override fun createPollWidgetView(
                pollWidgetModel: PollWidgetModel, isImage: Boolean
            ): View? {
                return CustomPollWidget(this@WidgetOnlyActivity).apply {
                    this.pollWidgetModel = pollWidgetModel
                    this.isImage = isImage
                }
            }

            override fun createImageSliderWidgetView(imageSliderWidgetModel: ImageSliderWidgetModel): View? {
                return CustomImageSlider(this@WidgetOnlyActivity).apply {
                    this.imageSliderWidgetModel = imageSliderWidgetModel
                }
            }

            override fun createVideoAlertWidgetView(videoAlertWidgetModel: VideoAlertWidgetModel): View? {
                return null
            }

            override fun createTextAskWidgetView(imageSliderWidgetModel: TextAskWidgetModel): View? {
                return CustomTextAskWidget(this@WidgetOnlyActivity).apply {
                    this.askWidgetModel = imageSliderWidgetModel
                }
            }

            override fun createNumberPredictionWidgetView(
                numberPredictionWidgetModel: NumberPredictionWidgetModel, isImage: Boolean
            ): View? {
                return CustomNumberPredictionWidget(this@WidgetOnlyActivity).apply {
                    this.numberPredictionWidgetViewModel = numberPredictionWidgetModel
                    this.isImage = isImage
                }
            }

            override fun createNumberPredictionFollowupWidgetView(
                followUpWidgetViewModel: NumberPredictionFollowUpWidgetModel, isImage: Boolean
            ): View? {
                return CustomNumberPredictionWidget(this@WidgetOnlyActivity).apply {
                    this.followUpWidgetViewModel = followUpWidgetViewModel
                    this.isImage = isImage
                    this.isFollowUp = true
                }
            }

            override fun createSocialEmbedWidgetView(socialEmbedWidgetModel: SocialEmbedWidgetModel): View? {
                return CustomSocialEmbed(this@WidgetOnlyActivity).apply {
                    this.socialEmbedWidgetModel = socialEmbedWidgetModel
                }
            }
        }

        (applicationContext as LiveLikeApplication).sdk.userProfileDelegate =
            object : UserProfileDelegate {
                override fun userProfile(
                    userProfile: LiveLikeProfile, reward: Reward, rewardSource: RewardSource
                ) {
                    val text =
                        "rewards recieved from ${rewardSource.name} : id is ${reward.rewardItem}, amount is ${reward.amount}"
                    binding.rewardsTv.text =
                        "At time ${SimpleDateFormat("hh:mm:ss").format(Calendar.getInstance().time)} : $text"
                    println(text)
                }
            }

        binding.widgetView.postDelayed(
            {
                val availableRewards = session.getRewardItems().joinToString { rewardItem ->
                    rewardItem.name
                }
                // check added to prevent crash - ES - 1466
                if (!(isFinishing || isDestroyed)) {
                    AlertDialog.Builder(this).apply {
                        setTitle("Welcome! You have chance to win rewards!").setMessage(
                            availableRewards
                        ).create()
                    }.show()
                }
            }, 2000
        )

        //TODO: Comment for now ,check for later if required or not
        /*EngagementSDK.predictionWidgetVoteRepository = object : PredictionWidgetVoteRepository {
            val predictionWidgetVoteRepository = LocalPredictionWidgetVoteRepository()

            override fun add(vote: PredictionWidgetVote, completion: () -> Unit) {
                predictionWidgetVoteRepository.add(vote, completion)
                AlertDialog.Builder(this@WidgetOnlyActivity).apply {
                    setTitle("PredictionVoteRepo.add called")
                        .setMessage("Sdk wants to store claim token : ${vote.claimToken}")
                        .create()
                }.show()
            }

            override fun get(predictionWidgetID: String): String? {
                return predictionWidgetVoteRepository.get(predictionWidgetID)
            }
        }*/
    }

    private val dialog = object : WidgetInterceptor() {
        override fun widgetWantsToShow(widgetData: LiveLikeWidgetEntity) {
            showDialog(this@WidgetOnlyActivity) {}
        }
    }


    override fun onResume() {
        super.onResume()
        session.resume()
    }

    override fun onPause() {
        super.onPause()
        session.pause()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        (application as LiveLikeApplication).removePublicSession()
    }

    inner class HeaderAdapter(
        val progressBar: ProgressBar, val programId: String, private val data: List<PostType>
    ) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        val question = "Who will win?"

        override fun onCreateViewHolder(
            parent: ViewGroup, viewType: Int
        ): RecyclerView.ViewHolder {
            if (viewType == TYPE_ITEM) {
                // inflate your layout and pass it to view holder
                val itemBinding =
                    RcylListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)

                return VHItem(
                    itemBinding
                )
            } else if (viewType == TYPE_HEADER) {
                // inflate your layout and pass it to view holder
                val itemBinding = RcylItemHeaderBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )

                return VHHeader(
                    itemBinding
                )
            }
            throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
        }

        override fun onBindViewHolder(
            holder: RecyclerView.ViewHolder, position: Int
        ) {
            val dataItem = getItem(position)
            if (holder is VHItem) {
                holder.button.text = dataItem.title
            } else if (holder is VHHeader) {
                holder.title.text = dataItem.title
            }
        }

        override fun getItemCount(): Int {
            return data.size
        }

        override fun getItemViewType(position: Int): Int {
            return if (isPositionHeader(position)) TYPE_HEADER else TYPE_ITEM
        }

        private fun isPositionHeader(position: Int): Boolean {
            return data[position].isHeader
        }

        private fun getItem(position: Int): PostType {
            return data[position]
        }

        internal inner class VHItem(var itemBinding: RcylListItemBinding) :
            RecyclerView.ViewHolder(itemBinding.root) {
            var button: Button = itemBinding.button
            private val networkApiClient = NetworkApiClient.getInstance(
                NetworkClientConfig("Android/${BuildConfig.SDK_VERSION} ${Build.MODEL}/${Build.VERSION.SDK_INT}"),
                Dispatchers.IO
            )
            private val images = arrayListOf<String>(
                "https://cf-blast-storage-staging.livelikecdn.com/assets/e38f0089-00be-4236-830a-6989e8298b50.jpg",
                "https://cf-blast-storage-staging.livelikecdn.com/assets/3c4db954-216b-40a8-ac34-9d43036b009d.jpg",
                "https://cf-blast-storage-staging.livelikecdn.com/assets/d223c74b-c29c-44a0-a3ce-1b8301971cf3.jpg",
                "https://cf-blast-storage-staging.livelikecdn.com/assets/84af0879-cfe9-4e37-8f38-adfe0a51ee57.jpg",
                "https://cf-blast-storage-staging.livelikecdn.com/assets/0cd1f9ad-c92c-4f6d-ada5-bab926df175f.jpg"
            )

            private val alertWidgetUrls = arrayListOf(
                "tel:8372873843432",
                "https://www.google.co.in",
                "https://www.youtube.com/watch?v=m5C6gKviWIQ",
                "nothing://nothing"
            )

            init {
                button.setOnClickListener {
                    val type = getItem(adapterPosition)
                    val options: ArrayList<Option> = ArrayList()
                    val choices: ArrayList<Choice> = ArrayList()
                    val scope = CoroutineScope(Dispatchers.Main + SupervisorJob())
                    accessToken = when (LiveLikeApplication.selectEnvironmentKey) {
                        "Production" -> "zslM9_lbiy3SWkMbKsoGfAkK2Kg46dfVkN1Zsdt8K_P3BJU-AJOeSQ"
                        "Staging" -> "db1GX0KrnGWwSOplsMTLJpFBbLds15TbULIxr6J189sabhDdbsrKoA"
                        "QA" -> "95BmUG5FWgtikjrj-hqlYblfdF4X5nldidDepAmhFefBUy2lRPXEEw"
                        "QA Iconic" -> "NmQ6vAUxPtB2yBl2uLcebbJKQa3DWH_RnxcZpjVnBspVPZH_KB1BtQ"
                        "QA DIG" -> "fP_GMdNpPY5JlkwX6_cgLrzdwA5L_99LmyvRgMRVUzY5oYM-_1JuvA"
                        else -> "db1GX0KrnGWwSOplsMTLJpFBbLds15TbULIxr6J189sabhDdbsrKoA"
                    }
                    scope.launch(Dispatchers.IO) {
                        when (type.url) {
                            textQuiz -> {
                                for (i in 0 until type.count) {
                                    choices.add(
                                        Choice(
                                            description = "Choice ${i + 1}", is_correct = i == 0
                                        )
                                    )
                                }
                            }

                            imgQuiz -> {
                                for (i in 0 until type.count) {
                                    choices.add(
                                        Choice(
                                            description = "Choice ${i + 1}",
                                            image_url = images[i],
                                            is_correct = i == 0
                                        )
                                    )
                                }
                            }

                            txtPoll, txtPrediction -> {
                                for (i in 0 until type.count) {
                                    options.add(
                                        Option(
                                            description = "Option $i",
                                            is_correct = i == 0
                                        )
                                    )
                                }
                            }

                            imgPoll, imgPrediction, cheerMeter, emojiSlider, imgNumberPrediction -> {
                                for (i in 0 until type.count) {
                                    options.add(
                                        Option(
                                            description = "Option $i", image_url = images[i],
                                            is_correct = i == 0
                                        )
                                    )
                                }
                            }
                        }
                        val timeout = "P0DT00H00M20S"
                        val request: Any? = when (type.url) {
                            alerts -> when (type.count) {
                                1 -> AlertRequest(
                                    title = "Alert",
                                    text = type.title,
                                    program_id = programId,
                                    timeout = timeout
                                )

                                2 -> AlertRequest(
                                    title = "Alert",
                                    image_url = images[0],
                                    program_id = programId,
                                    timeout = timeout
                                )

                                3 -> AlertRequest(
                                    title = "Alert",
                                    text = type.title,
                                    image_url = images[0],
                                    program_id = programId,
                                    timeout = timeout
                                )

                                4 -> AlertRequest(
                                    title = "Alert",
                                    text = type.title,
                                    image_url = images[0],
                                    link_url = alertWidgetUrls[Random.nextInt(0, 4)],
                                    link_label = "Google",
                                    program_id = programId,
                                    timeout = timeout
                                )

                                else -> null
                            }

                            textQuiz, imgQuiz -> QuizRequest(
                                choices, null, programId, question, timeout
                            )

                            txtPoll, imgPoll -> PollRequestResponse(
                                options, null, programId, question, timeout
                            )

                            txtPrediction, imgPrediction -> PredictionRequest(
                                "The confirmation Message",
                                options,
                                null,
                                programId,
                                question,
                                timeout
                            )

                            emojiSlider -> EmojiSliderRequest(
                                0.0, options, null, programId, question, timeout
                            )

                            cheerMeter -> CheerMeterRequestResponse(
                                "tap", options, null, programId, question, timeout
                            )

                            textAsk -> TextAskRequest(
                                "This is a prompt",
                                null,
                                programId,
                                timeout,
                                "this is the title",
                            )

                            imgNumberPrediction -> NumberPredictionRequest(
                                "The confirmation Message",
                                options,
                                null,
                                programId,
                                question,
                                timeout
                            )

                            richPost -> programId to "<p>Hii,<br><br><u>Testing</u> the rich post from <b>android<br></b></p><p><b><br></b></p><ol><li><b>Testing</b></li><li><b><br></b></li></ol><p><span style=\\\"font-family: Impact;\\\">\uFEFFTesting</span></p><p><span style=\\\"font-family: Impact;\\\"><br></span></p><p><span style=\\\"font-family: &quot;Times New Roman&quot;;\\\">\uFEFF</span><span style=\\\"font-family: Impact;\\\"><br></span></p><ul><li><span style=\\\"font-family: Impact;\\\"><span style=\\\"font-family: &quot;Comic Sans MS&quot;;\\\">Testing</span><br></span><b><br></b></li></ul>"
                            else -> null
                        }
                        scope.launch {
                            progressBar.visibility = View.VISIBLE
                        }
                        val responseString = try {
                            postAPI(type.url!!, request)
                        } catch (e: Exception) {
                            hideProgress(scope,e.message.toString())
                            e.printStackTrace()
                            null
                        }
                        val gson = Gson()
                        responseString?.let {
                            scope.launch {
                                Toast.makeText(
                                    applicationContext, "Widget Created", Toast.LENGTH_SHORT
                                ).show()
                            }
                            val liveLikeWidget = gson.fromJson(
                                responseString, LiveLikeWidget::class.java
                            )
                            liveLikeWidgetClient.publishWidget(
                                request = PublishWidgetRequest(
                                    type = liveLikeWidget.getWidgetType()!!,
                                    id = liveLikeWidget.id,
                                    publishDelay = "P0DT00H00M00S",
                                    programDateTime = null
                                )
                            ) { result, error ->
                                result?.let {
                                    fun callback(
                                        result: com.livelike.engagementsdk.widget.model.Option?,
                                        error: String?
                                    ) {
                                        result?.let {
                                            logDebug { "result: $result" }
                                            val followUp = liveLikeWidget.followUps!![0]
                                            when (type.url) {
                                                imgNumberPrediction -> {
                                                    liveLikeWidgetClient.createImageNumberPredictionFollowUp(
                                                        CreateImageNumberPredictionFollowUpRequest(
                                                            liveLikeWidget.id,
                                                            null
                                                        )
                                                    ) { result, error ->
                                                        result?.let {
                                                            liveLikeWidgetClient.publishWidget(
                                                                PublishWidgetRequest(
                                                                    type = followUp.getWidgetType()!!,
                                                                    id = followUp.id,
                                                                    publishDelay = "P0DT00H00M00S",
                                                                    programDateTime = null
                                                                )
                                                            ) { result, error ->
                                                                result?.let {
                                                                    hideProgress(
                                                                        scope,
                                                                        it.toString()
                                                                    )
                                                                }
                                                                error?.let {
                                                                    hideProgress(scope, it)
                                                                }
                                                            }
                                                        }
                                                        error?.let {
                                                            hideProgress(scope, it)
                                                        }
                                                    }
                                                }

                                                else -> {
                                                    liveLikeWidgetClient.publishWidget(
                                                        PublishWidgetRequest(
                                                            type = followUp.getWidgetType()!!,
                                                            id = followUp.id,
                                                            publishDelay = "P0DT00H00M00S",
                                                            programDateTime = null
                                                        )
                                                    ) { result, error ->
                                                        result?.let {
                                                            hideProgress(scope,it.toString())
                                                        }
                                                        error?.let {
                                                            hideProgress(scope,it.toString())
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                        error?.let {
                                            scope.launch {
                                                progressBar.visibility = View.GONE
                                                Toast.makeText(
                                                    applicationContext,
                                                    "$it",
                                                    Toast.LENGTH_LONG
                                                ).show()
                                            }
                                        }
                                    }
                                    scope.launch {
                                        when (type.url) {
                                            txtPrediction, imgPrediction -> {
                                                delay(5000L)
                                                if (liveLikeWidget.kind.contains("prediction")) {

                                                    if (liveLikeWidget.kind.contains("text"))
                                                        liveLikeWidgetClient.updateTextPredictionOption(
                                                            UpdateTextPredictionOptions(
                                                                liveLikeWidget.id,
                                                                liveLikeWidget.options!![0].id,
                                                                null,
                                                                true,
                                                                null
                                                            ), ::callback
                                                        )
                                                    else if (liveLikeWidget.kind.contains("image"))
                                                        liveLikeWidgetClient.updateImagePredictionOption(
                                                            UpdateImagePredictionOptions(
                                                                liveLikeWidget.id,
                                                                liveLikeWidget.options!![0].id,
                                                                null,
                                                                true,
                                                                null
                                                            ), ::callback
                                                        )
                                                }
                                            }

                                            richPost -> {
                                                Toast.makeText(
                                                    applicationContext,
                                                    "$it\n$liveLikeWidget",
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            }

                                            imgNumberPrediction -> {
                                                delay(15000L)
                                                liveLikeWidgetClient.updateImageNumberPredictionOption(
                                                    UpdateImageNumberPredictionOptionRequest(
                                                        liveLikeWidget.id,
                                                        liveLikeWidget.options!![0].id,
                                                        10,
                                                        null,
                                                        null
                                                    ), ::callback
                                                )
                                            }

                                            else -> {
                                                progressBar.visibility = View.GONE
                                            }
                                        }

                                        Toast.makeText(
                                            applicationContext, "$it", Toast.LENGTH_LONG
                                        ).show()
                                    }
                                }
                                error?.let {
                                    hideProgress(scope, it)
                                }
                            }
                        }


                        /*var response: Any? = null
                            try {

                                response = if (responseString == null) null else when (type.url) {
                                    alerts -> gson.fromJson(responseString, AlertResponse::class.java)
                                    textQuiz, imgQuiz -> gson.fromJson(
                                        responseString, QuizResponse::class.java
                                    )

                                    txtPoll, imgPoll -> {
                                        gson.fromJson(
                                            responseString, PollRequestResponse::class.java
                                        ).also {
                                            if (type.url == txtPoll) {

                                            } else {
                                            }
                                        }

                                    }

                                    txtPrediction, imgPrediction -> gson.fromJson(
                                        responseString, PredictionResponse::class.java
                                    )

                                    emojiSlider -> gson.fromJson(
                                        responseString, PredictionResponse::class.java
                                    )

                                    cheerMeter -> gson.fromJson(
                                        responseString, CheerMeterRequestResponse::class.java
                                    )

                                    textAsk -> gson.fromJson(
                                        responseString, TextAskResponse::class.java
                                    )

                                    imgNumberPrediction -> gson.fromJson(
                                        responseString, NumberPredictionResponse::class.java
                                    )

                                    else -> null
                                }
                            } catch (e: java.lang.Exception) {
                                e.printStackTrace()
                                scope.launch {
                                    progressBar.visibility = View.GONE
                                    Toast.makeText(
                                        applicationContext, "${e.message}", Toast.LENGTH_SHORT
                                    ).show()
                                }
                                response = null
                            }
                            response?.let {
                                when (it) {
                                    is AlertResponse -> {
                                        putAPI(it.schedule_url)
                                    }

                                    is PollRequestResponse -> {
    //                                    it.schedule_url?.let { it1 ->
    //                                        putAPI(it1)
    //                                    }
                                    }

                                    is QuizResponse -> {
                                        putAPI(it.schedule_url)
                                    }

                                    is TextAskResponse -> {
                                        putAPI(it.schedule_url)
                                    }

                                    is PredictionResponse -> {
                                        it.schedule_url?.let { it1 -> putAPI(it1) }
                                        scope.launch {
                                            progressBar.visibility = View.GONE
                                        }
                                        if (it.kind?.contains("prediction") == true) {
                                            delay(10000)
                                            it.options?.let { list ->
                                                for (i in list.indices) {
                                                    val option = list[i]
                                                    if (i == 0) {
                                                        option.is_correct = true
                                                        option.vote_count = 0
                                                        patchAPI(option.url, option)
                                                    } else if (i == 1) {
                                                        option.is_correct = false
                                                        option.vote_count = 1
                                                    }
                                                    list[i] = option
                                                }
                                            }

                                            delay(5000)
                                            val followRequest = FollowUpRequest(
                                                it.options,
                                                it.program_date_time,
                                                it.question,
                                                it.scheduled_at,
                                                "P0DT00H00M07S"
                                            )
                                            it.follow_ups?.let { followups ->
                                                val res = patchAPI(
                                                    followups[0].url, followRequest
                                                )
                                                val resp =
                                                    gson.fromJson(res, FollowUpResponse::class.java)
                                                resp?.let { r ->
                                                    r.schedule_url.let { it1 -> putAPI(it1) }
                                                }
                                            }
                                        }
                                    }

                                    is CheerMeterRequestResponse -> {
                                        it.schedule_url?.let { it1 -> putAPI(it1) }
                                    }

                                    is NumberPredictionResponse -> {
                                        it.scheduleUrl?.let { it1 -> putAPI(it1) }
                                    }
                                }
                                scope.launch {
                                    progressBar.visibility = View.GONE
                                }
                            }*/
                    }
                }
            }

            private fun hideProgress(scope: CoroutineScope, message: String) {
                scope.launch {
                    progressBar.visibility =
                        View.GONE
                    Toast.makeText(
                        applicationContext,
                        "$message",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            private var accessToken: String =
                "NmQ6vAUxPtB2yBl2uLcebbJKQa3DWH_RnxcZpjVnBspVPZH_KB1BtQ"

            private suspend fun postAPI(
                url: String, post: Any? = null
            ): String? {
                val widget = suspendCoroutine { cont ->
                    fun callback(result: LiveLikeWidget?, error: String?) {
                        result?.let {
                            cont.resume(it)
                        }
                        error?.let {
                            cont.resumeWithException(Exception(it))
                        }
                    }
                    when (url) {
                        txtPoll -> {
                            val txtPoll = post as PollRequestResponse
                            liveLikeWidgetClient.createTextPoll(
                                request = CreateTextPollRequest(
                                    options = emptyList(),
                                    programId = txtPoll.program_id,
                                    question = txtPoll.question,
                                    timeout = txtPoll.timeout,
                                    sponsorIds = listOf("bf61bc65-088f-4fee-953e-bd05aa27f8d5")
                                ).run {
                                    copy(options = txtPoll.options.map { CreateTextPollRequest.Option(it.description) })
                                }, ::callback
                            )
                        }

                        imgPoll -> {
                            val imgPoll = post as PollRequestResponse
                            liveLikeWidgetClient.createImagePoll(
                                request = CreateImagePollRequest(
                                    options = emptyList(),
                                    programId = imgPoll.program_id,
                                    question = imgPoll.question,
                                    timeout = imgPoll.timeout,
                                    sponsorIds = listOf("bf61bc65-088f-4fee-953e-bd05aa27f8d5")
                                ).run {
                                    copy(options = imgPoll.options.map {
                                        CreateImagePollRequest.Option(
                                            it.description, it.image_url!!
                                        )
                                    })
                                }, ::callback
                            )
                        }

                        textQuiz -> {
                            val txtQuiz = post as QuizRequest
                            liveLikeWidgetClient.createTextQuiz(
                                CreateTextQuizRequest(
                                    choices = emptyList(),
                                    programId = txtQuiz.program_id!!,
                                    question = txtQuiz.question!!,
                                    timeout = txtQuiz.timeout!!,
                                    sponsorIds = listOf("bf61bc65-088f-4fee-953e-bd05aa27f8d5")
                                ).run {
                                    copy(choices = txtQuiz.choices!!.map {
                                        CreateTextQuizRequest.Choice(
                                            it.description,
                                            it.is_correct!!
                                        )
                                    })
                                }, ::callback
                            )
                        }

                        imgQuiz -> {
                            val txtQuiz = post as QuizRequest
                            liveLikeWidgetClient.createImageQuiz(
                                CreateImageQuizRequest(
                                    choices = emptyList(),
                                    programId = txtQuiz.program_id!!,
                                    question = txtQuiz.question!!,
                                    timeout = txtQuiz.timeout!!,
                                    sponsorIds = listOf("bf61bc65-088f-4fee-953e-bd05aa27f8d5")
                                ).run {
                                    copy(choices = txtQuiz.choices!!.map {
                                        CreateImageQuizRequest.Choice(
                                            it.description,
                                            it.is_correct!!,
                                            it.image_url!!
                                        )
                                    })
                                }, ::callback
                            )
                        }

                        txtPrediction -> {
                            val request = post as PredictionRequest
                            liveLikeWidgetClient.createTextPrediction(
                                CreateTextPredictionRequest(
                                    options = emptyList(),
                                    programId = request.program_id!!,
                                    question = request.question!!,
                                    timeout = request.timeout!!,
                                    confirmationMessage = "Thanks",
                                    sponsorIds = listOf("bf61bc65-088f-4fee-953e-bd05aa27f8d5")
                                ).run {
                                    copy(options = request.options!!.map {
                                        CreateTextPredictionRequest.Option(it.description)
                                    })
                                }, ::callback
                            )
                        }

                        imgPrediction -> {
                            val request = post as PredictionRequest
                            liveLikeWidgetClient.createImagePrediction(
                                CreateImagePredictionRequest(
                                    options = emptyList(),
                                    programId = request.program_id!!,
                                    question = request.question!!,
                                    timeout = request.timeout!!,
                                    confirmationMessage = "Thanks"
                                ).run {
                                    copy(options = request.options!!.map {
                                        CreateImagePredictionRequest.Option(it.description, it.image_url!!)
                                    })
                                }, ::callback
                            )
                        }

                        emojiSlider -> {
                            val request = post as EmojiSliderRequest
                            /*liveLikeWidgetClient.createImageSlider(
                                    CreateImageSliderRequest(
                                        options = emptyList(),
                                        programId = request.program_id!!,
                                        question = request.question!!,
                                        timeout = request.timeout!!,
                                        initialMagnitude = "0.1",
                                        sponsorIds = listOf("bf61bc65-088f-4fee-953e-bd05aa27f8d5")
                                    ).run {
                                        copy(options = request.options!!.map {
                                            this.Option(it.description, it.image_url!!)
                                        })
                                    }, ::callback
                                )*/
                        }

                        cheerMeter -> {
                            val request = post as CheerMeterRequestResponse
                            /*liveLikeWidgetClient.createCheerMeter(
                                    CreateCheerMeterRequest(
                                        options = emptyList(),
                                        programId = request.program_id!!,
                                        question = request.question!!,
                                        timeout = request.timeout!!,
                                        cheerType = "tap",
                                        sponsorIds = listOf("bf61bc65-088f-4fee-953e-bd05aa27f8d5")
                                    ).run {
                                        copy(options = request.options!!.map {
                                            this.Option(it.description, it.image_url!!)
                                        })
                                    }, ::callback
                                )*/
                        }

                        alerts -> {
                            val request = post as AlertRequest
                            liveLikeWidgetClient.createAlert(
                                request = CreateAlertRequest(
                                    programId = request.program_id!!,
                                    timeout = request.timeout!!,
                                    title = request.title,
                                    text = request.text,
                                    imageURL = request.image_url,
                                    linkLabel = request.link_label,
                                    linkURL = request.link_url,
                                    sponsorIds = listOf("bf61bc65-088f-4fee-953e-bd05aa27f8d5")
                                ), ::callback
                            )
                        }

                        textAsk -> {
                            val request = post as TextAskRequest
                            liveLikeWidgetClient.createTextAsk(
                                CreateTextAskRequest(
                                    programId = request.program_id!!,
                                    timeout = request.timeout!!,
                                    title = request.title!!,
                                    prompt = request.prompt,
                                    confirmationMessage = "Thanks for submission",
                                    sponsorIds = listOf("bf61bc65-088f-4fee-953e-bd05aa27f8d5")
                                ), ::callback
                            )
                        }

                        imgNumberPrediction -> {
                            val request = post as NumberPredictionRequest
                            liveLikeWidgetClient.createImageNumberPrediction(
                                CreateImageNumberPredictionRequest(
                                    options = emptyList(),
                                    programId = request.programId!!,
                                    question = request.question!!,
                                    timeout = request.timeout!!,
                                    programDateTime = request.programDateTime,
                                    confirmationMessage = request.confirmationMessage,
                                    sponsorIds = listOf("bf61bc65-088f-4fee-953e-bd05aa27f8d5")
                                ).run {
                                    copy(options = request.options!!.map {
                                        CreateImageNumberPredictionRequest.Option(
                                            5,
                                            it.image_url!!,
                                            it.description
                                        )
                                    })
                                }, ::callback
                            )
                        }

                        richPost -> {
                            val pair = post as Pair<String, String>
                            liveLikeWidgetClient.createRichPost(
                                CreateRichPostRequest(
                                    pair.second,
                                    pair.first
                                ), ::callback
                            )
                        }
                    }
                }
                return widget.toJsonString()
//                } else if (url == imgPoll) {
//                }
//                val result = networkApiClient.post(
//                    "${LiveLikeApplication.selectedEnvironment?.configUrl}/api/v1/$url/",
//                    body = post?.toJsonString(),
//                    accessToken = accessToken
//                )
//                if (result is NetworkResult.Success) {
//                    return result.data
//                }
//                return null
            }
        }

        internal inner class VHHeader(var itemBinding: RcylItemHeaderBinding) :
            RecyclerView.ViewHolder(itemBinding.root) {
            var title: TextView = itemBinding.textView
        }

        private val TYPE_HEADER = 0
        private val TYPE_ITEM = 1
    }

    override fun onDestroy() {
        //TODO:
//        EngagementSDK.predictionWidgetVoteRepository = LocalPredictionWidgetVoteRepository()
        super.onDestroy()
    }
}

data class PostType(
    val title: String, val isHeader: Boolean = false, val url: String? = null, val count: Int = 0
)

data class Choice(
    val answer_count: Int? = null,
    val answer_url: String? = null,
    val description: String,
    val id: String? = null,
    val is_correct: Boolean? = null,
    val translatable_fields: List<String>? = null,
    val image_url: String? = null
)

data class CreatedBy(
    val id: String, val image_url: String, val name: String
)

data class Option(
    val description: String,
    val id: String? = null,
    val translatable_fields: List<String>? = null,
    val vote_count: Int? = null,
    val vote_url: String? = null,
    val image_url: String? = null,
    val is_correct: Boolean? = null,
    val url: String? = null
)
