package com.livelike.livelikedemo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.livelike.livelikedemo.channel.ChannelManager
import com.livelike.livelikedemo.databinding.ActivityMain2Binding
import com.livelike.livelikedemo.ui.main.MMLChatView
import com.livelike.livelikedemo.ui.main.SectionsPagerAdapter

class MainActivity2 : AppCompatActivity() {

    private lateinit var channelManager: ChannelManager
    private lateinit var binding: ActivityMain2Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding=ActivityMain2Binding.inflate(layoutInflater)
        setContentView(binding.root)

        channelManager = (application as LiveLikeApplication).channelManager
        val channel = channelManager.selectedChannel
        if (channel != ChannelManager.NONE_CHANNEL) {
            val session =
                (application as LiveLikeApplication).createPublicSession(channel.llProgram.toString(),)
            val sectionsPagerAdapter = SectionsPagerAdapter(this, session)
            val viewPager: ViewPager = binding.viewPager
            viewPager.offscreenPageLimit = 1
            viewPager.adapter = sectionsPagerAdapter
            viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {

                }

                override fun onPageSelected(position: Int) {
                    (sectionsPagerAdapter.views[2] as? MMLChatView)?.dismissReactionPanel()
                }

                override fun onPageScrollStateChanged(state: Int) {

                }

            })
            val tabs: TabLayout = binding.tabs
            tabs.setupWithViewPager(viewPager)
        }
    }
}
