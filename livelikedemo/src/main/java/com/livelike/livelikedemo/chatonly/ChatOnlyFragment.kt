package com.livelike.livelikedemo.chatonly

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.livelike.engagementsdk.ChatRoomListener
import com.livelike.engagementsdk.chat
import com.livelike.engagementsdk.chat.ChatRoomInfo
import com.livelike.engagementsdk.chat.Visibility
import com.livelike.engagementsdk.chat.data.remote.ContentFilter
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.livelikedemo.ChatOnlyActivity
import com.livelike.livelikedemo.LiveLikeApplication
import com.livelike.livelikedemo.databinding.FragmentChatOnlyBinding


class ChatOnlyFragment(private val chatRoomId: String) : Fragment() {

    private lateinit var binding: FragmentChatOnlyBinding
    private var title: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding = FragmentChatOnlyBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.btnChatRoomMembers.setOnClickListener {
            val id = binding.txtChatRoomId.text.toString()
            if (id.isNotEmpty()) {
                binding.prgMembers.visibility = View.VISIBLE
                (activity?.application as? LiveLikeApplication)?.sdk?.chat()?.getMembersOfChatRoom(
                    id,
                    LiveLikePagination.FIRST
                ) { result, error ->
                    if (result?.isNotEmpty() == true)
                        binding.txtChatRoomMembersCount.text = "Members: ${result.size}"
                    else
                        binding.txtChatRoomMembersCount.text = ""
                    binding.prgMembers.visibility = View.INVISIBLE
                    result?.let { list ->
                        if (list.isNotEmpty()) {
                            AlertDialog.Builder(context).apply {
                                setTitle("Room Members")
                                setItems(
                                    list.map { "${it.nickname} (${it.id})" }
                                        .toTypedArray()
                                ) { _, which ->
                                    // On change of theme we need to create the session in order to pass new attribute of theme to widgets and chat
                                    val item = list[which]
                                    item.let {
                                        val clipboard =
                                            context?.getSystemService(Context.CLIPBOARD_SERVICE) as? ClipboardManager
                                        val clip =
                                            ClipData.newPlainText("copied User Id", item.id)
                                        clipboard?.setPrimaryClip(clip)
                                    }
                                }
                                create()
                            }.show()
                        }
                    }
                }
            } else {
                showToast("Select Room")
            }
        }

        binding.txtChatRoomVisibility.setOnClickListener {
            if (binding.txtChatRoomId.text.isNotEmpty()) {
                (activity as? ChatOnlyActivity)?.selectVisibility(object : VisibilityInterface {
                    override fun onSelectItem(visibility: Visibility) {
                        binding.prgVisibility.visibility = View.VISIBLE
                        (activity?.application as? LiveLikeApplication)?.sdk?.chat()
                            ?.updateChatRoom(
                                binding.txtChatRoomId.text.toString(),
                                title,
                                visibility
                            ) { result, error ->
                                binding.prgVisibility.visibility = View.INVISIBLE
                                updateData(result)
                                error?.let {
                                    showToast(it)
                                }
                            }
                    }
                })
            }
        }

        (activity as? ChatOnlyActivity)?.privateGroupChatsession?.let {
            binding.chatView.enableQuoteMessage = true
            binding.chatView.enableChatMessageURLs = true
            binding.chatView.setSession(it)
            it.setChatRoomListener(object : ChatRoomListener {
                override fun onChatRoomUpdate(chatRoom: ChatRoomInfo) {
                    (activity as? ChatOnlyActivity)?.chatRoomInfo = chatRoom
                    binding.txtChatRoomId.post {
                        updateData(chatRoom)
                    }
                }
            })
            it.connectToChatRoom(
                chatRoomId
            ) { result, error -> println("ChatOnlyActivity.onResponse -> $result -> $error") }
        }
        (activity as? ChatOnlyActivity)?.chatRoomInfo?.let {
            binding.txtChatRoomId.post {
                updateData(it)
            }
        }
        binding.txtChatRoomId.setOnClickListener {
            val clipboard =
                context?.getSystemService(Context.CLIPBOARD_SERVICE) as? ClipboardManager
            val clip =
                ClipData.newPlainText("copied ChatRoom Id", binding.txtChatRoomId.text.toString())
            clipboard?.setPrimaryClip(clip)
        }

        binding.btnChatRoom1.setOnClickListener {
            (activity as? ChatOnlyActivity)?.privateGroupChatsession?.connectToChatRoom("564a120f-b3d9-41a0-8f21-8827dcb79866")
        }
        binding.btnChatRoom2.setOnClickListener {
            (activity as? ChatOnlyActivity)?.privateGroupChatsession?.connectToChatRoom("af5e6e1e-ea65-4195-9f15-ccbaa8a1ffbe")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        (activity as? ChatOnlyActivity)?.privateGroupChatsession?.setChatRoomListener(null)
        binding.chatView.clearSession()
    }

    @SuppressLint("SetTextI18n")
    private fun updateData(result: ChatRoomInfo?) {
        result?.let {
            title = it.title
            binding.txtChatRoomTitle.text =
                """
                    Title: ${it.title}
                    Visibility: ${it.visibility?.name}
                    Content Filter: ${it.contentFilter}
                    Custom Data: ${it.customData}
                    Token gate: ${
                    it.tokenGates?.map { gate -> gate.contractAddress }?.joinToString() ?: "None"
                }
                """.trimIndent()
            binding.txtChatRoomId.text = it.id
            binding.txtChatRoomMembersCount.text = ""
            binding.txtChatRoomVisibility.text = it.visibility?.name
            binding.chatView.isChatInputVisible = (it.contentFilter == ContentFilter.producer).not()
        }
    }

    fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    interface VisibilityInterface {
        fun onSelectItem(visibility: Visibility)
    }

    companion object {
        @JvmStatic
        fun newInstance(chatRoomId: String) = ChatOnlyFragment(chatRoomId)
    }
}
