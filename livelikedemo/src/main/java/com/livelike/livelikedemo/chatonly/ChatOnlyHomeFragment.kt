package com.livelike.livelikedemo.chatonly

import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.livelike.common.profile
import com.livelike.engagementsdk.chat
import com.livelike.engagementsdk.chat.ChatRoomInfo
import com.livelike.engagementsdk.chat.Visibility
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.BlockedInfo
import com.livelike.engagementsdk.publicapis.ChatRoomAdd
import com.livelike.engagementsdk.publicapis.ChatRoomDelegate
import com.livelike.engagementsdk.publicapis.ChatRoomInvitation
import com.livelike.engagementsdk.publicapis.ChatRoomInvitationStatus
import com.livelike.engagementsdk.publicapis.LiveLikeUserApi
import com.livelike.livelikedemo.ChatOnlyActivity
import com.livelike.livelikedemo.LiveLikeApplication
import com.livelike.livelikedemo.databinding.BlockListItemBinding
import com.livelike.livelikedemo.databinding.ChatOnlyCheckBoxBinding
import com.livelike.livelikedemo.databinding.FragmentChatOnlyHomeBinding
import com.livelike.livelikedemo.databinding.InviteListItemBinding
import com.livelike.livelikedemo.databinding.UserListItemBinding
import java.util.Locale

class ChatOnlyHomeFragment : Fragment() {


    private lateinit var binding: FragmentChatOnlyHomeBinding
    private lateinit var checkBoxBinding: ChatOnlyCheckBoxBinding
    private var chatRoomList: ArrayList<ChatRoomInfo> = arrayListOf()
    private val inviteAdapter =
        InviteListAdapter(
            ::updateInviteStatus
        )

    //    private val adapter = UserAdapter()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding = FragmentChatOnlyHomeBinding.inflate(inflater, container, false)
        checkBoxBinding = ChatOnlyCheckBoxBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val adapter = BlockedListAdapter(::unblock)
        binding.rcylBlock.adapter = adapter
        binding.btnBlockList.setOnClickListener {
            binding.prgBlock.visibility = View.VISIBLE
            (activity?.application as? LiveLikeApplication)?.sdk?.profile()
                ?.getBlockedProfileList(LiveLikePagination.FIRST) { result, error ->
                    binding.prgBlock.visibility = View.INVISIBLE
                    error?.let { it1 -> showToast(it1) }
                    result?.let {
                        adapter.blockedList.clear()
                        adapter.blockedList.addAll(it)
                        adapter.notifyDataSetChanged()
                    }
                }
        }
        binding.btnBlockCheck.setOnClickListener {
            val profileId = binding.edBlockProfileId.text.toString()
            binding.prgBlock.visibility = View.VISIBLE
            (activity?.application as? LiveLikeApplication)?.sdk?.profile()
                ?.getProfileBlockInfo(profileId) { result, error ->
                    binding.prgBlock.visibility = View.INVISIBLE
                    error?.let { it1 -> showToast(it1) }
                    showToast(
                        when (result != null) {
                            true -> "Blocked"
                            else -> "Not Blocked"
                        }
                    )
                }
        }
        binding.btnBlock.setOnClickListener {
            val profileId = binding.edBlockProfileId.text.toString()
            if (profileId.isNotEmpty()) {
                binding.prgBlock.visibility = View.VISIBLE
                (activity?.application as? LiveLikeApplication)?.sdk?.profile()?.blockProfile(
                    profileId
                ) { result, error ->
                    binding.prgBlock.visibility = View.INVISIBLE
                    result?.let {
                        activity?.runOnUiThread {
                            showToast("BLocked User: ${it.blockedProfileID}")
                        }
                        // logDebug {"BLocked User: ${it.blockedProfileID}"}
                    }
                    error?.let { it1 ->
                        activity?.runOnUiThread {
                            showToast("BLocked User: ${it1}")
                        }
                        //logDebug {"BLocked User: ${it1}"}
                    }
                }
            } else {
                showToast("enter profile id")
            }
        }
        binding.btnCreate.setOnClickListener {
            val title = binding.edChatRoomTitle.text.toString()
            val visibility =
                if (binding.btnVisibility.text.toString().lowercase(Locale.getDefault())
                        .contains("visibility").not()
                )
                    Visibility.valueOf(binding.btnVisibility.text.toString())
                else
                    Visibility.everyone
            binding.prgCreate.visibility = View.VISIBLE
            (activity?.application as? LiveLikeApplication)?.sdk?.chat()?.createChatRoom(
                when (title.trim().isEmpty()) {
                    true -> null
                    else -> title
                },
                visibility,
                null
            ) { result, error ->
                val response = when {
                    result != null ->
                        "${
                            result.title
                                ?: "No Title"
                        }(${result.id}),  Room Id copy to clipboard"

                    else -> error
                }
                var clipboard =
                    context?.getSystemService(Context.CLIPBOARD_SERVICE) as? ClipboardManager
                var clip = ClipData.newPlainText("copied ChatRoomId", result?.id)
                clipboard?.setPrimaryClip(clip)
                response?.let { it1 -> showToast(it1) }

                binding.edChatRoomTitle.setText("")
                binding.prgCreate.visibility = View.INVISIBLE
            }
        }

        binding.btnJoin.setOnClickListener {
            val id = binding.edChatRoomId.text.toString()
            if (id.isEmpty()) {
                showToast("Enter Room Id First")
                return@setOnClickListener
            }
            binding.prgJoin.visibility = View.VISIBLE
            (activity?.application as? LiveLikeApplication)?.sdk?.chat()
                ?.addCurrentProfileToChatRoom(id) { result, error ->
                    result?.let {
                        showToast("User Added Successfully")
                    }
                    binding.edChatRoomId.setText("")
                    error?.let {
                        showToast(it)
                    }
                    binding.prgJoin.visibility = View.INVISIBLE
                    binding.btnRefresh.callOnClick()
                }
        }

        binding.btnAdd.setOnClickListener {
            val chatRoomId = binding.edChatRoomId1.text.toString()
            val userId = binding.edUserId.text.toString()
            if (chatRoomId.isEmpty() || userId.isEmpty()) {
                showToast("Enter Room Id,User Id First")
                return@setOnClickListener
            }
            binding.prgAdd.visibility = View.VISIBLE
            (activity?.application as? LiveLikeApplication)?.sdk?.chat()
                ?.addProfileToChatRoom(
                    chatRoomId,
                    userId
                ) { result, error ->
                    result?.let {
                        showToast("User Added Successfully")
                    }
                    binding.edChatRoomId1.setText("")
                    binding.edUserId.setText("")
                    error?.let {
                        showToast(it)
                    }
                    binding.prgAdd.visibility = View.INVISIBLE
                    binding.btnRefresh.callOnClick()
                }
        }

        binding.btnInvite.setOnClickListener {
            val chatRoomId = binding.edChatRoomIdInvite1.text.toString()
            val userId = binding.edUserInviteId.text.toString()
            if (chatRoomId.isEmpty() || userId.isEmpty()) {
                showToast("Enter Room Id,User Id First")
                return@setOnClickListener
            }
            binding.prgAddInvite.visibility = View.VISIBLE
            (activity?.application as? LiveLikeApplication)?.sdk?.chat()
                ?.sendChatRoomInviteToProfile(
                    chatRoomId,
                    userId
                ) { result, error ->
                    result?.let {
                        showToast("User Invited Successfully")
                    }
                    binding.edChatRoomIdInvite1.setText("")
                    binding.edUserInviteId.setText("")
                    error?.let {
                        showToast(it)
                    }
                    binding.prgAddInvite.visibility = View.INVISIBLE
                    binding.btnRefresh.callOnClick()
                }
        }

        binding.btnMuteStatus.setOnClickListener {
            val id = binding.edChatRoomId.text.toString()
            if (id.isEmpty()) {
                showToast("Enter Room Id First")
                return@setOnClickListener
            }
            binding.prgJoin.visibility = View.VISIBLE
            (activity?.application as? LiveLikeApplication)?.sdk?.chat()?.getProfileMutedStatus(
                id
            ) { result, error ->
                result?.let {
                    binding.btnMuteStatus.post {
                        showToast("User is ${if (result.isMuted) " " else "not "}muted ")
                    }
                }
                error?.let {
                    binding.btnMuteStatus.post {
                        showToast(it)
                    }
                }
                binding.prgMute.visibility = View.INVISIBLE
            }
        }

        binding.btnRefresh.setOnClickListener {
            binding.prgRefresh.visibility = View.VISIBLE
            (activity?.application as? LiveLikeApplication)?.sdk?.chat()
                ?.getCurrentProfileChatRoomList(LiveLikePagination.FIRST) { result, error ->
                    binding.prgRefresh.visibility = View.INVISIBLE
                    chatRoomList.clear()
                    result?.let { it1 ->
                        chatRoomList.addAll(it1)
                    }
                    error?.let {
                        showToast(it)
                    }
                }
        }

        binding.btnChange.setOnClickListener {
            AlertDialog.Builder(context).apply {
                setTitle("Select a private group")
                setItems(chatRoomList.map { "${it.id}(${it.title})" }.toTypedArray()) { _, which ->
                    // On change of theme we need to create the session in order to pass new attribute of theme to widgets and chat
//                    (application as LiveLikeApplication).removePrivateSession()
                    val session = (activity as? ChatOnlyActivity)?.sessionMap?.get(
                        chatRoomList.elementAt(which).id
                    )
                    if (session == null) {

//                        val checkBoxView =
//                            View.inflate(context, R.layout.chat_only_check_box, null)
                        val builder = AlertDialog.Builder(context)
                        builder.setTitle("Avatar")
                            .setView(checkBoxBinding.root)
                            .setCancelable(false)
                            .setPositiveButton("Done") { _, _ ->
                                val url = checkBoxBinding.edAvatar.text.toString()
                                (activity as? ChatOnlyActivity)?.changeChatRoom(
                                    chatRoomList.elementAt(which).id,
                                    url,
                                    binding.showFiltered.isChecked
                                )
                            }
                            .show()
                    } else {
                        (activity as? ChatOnlyActivity)?.changeChatRoom(
                            chatRoomList.elementAt(which).id,
                            showFiltered = binding.showFiltered.isChecked
                        )
                    }
                }
                create()
            }.show()
        }

        binding.btnDelete.setOnClickListener {
            val id = binding.edChatRoomId.text.toString()
            if (id.isNotEmpty()) {
                binding.prgDelete.visibility = View.VISIBLE
                (activity?.application as? LiveLikeApplication)?.sdk?.chat()
                    ?.deleteCurrentProfileFromChatRoom(
                        id
                    ) { result, error ->
                        result?.let {
                            showToast("Deleted ChatRoom")
                            (activity as? ChatOnlyActivity)?.privateGroupChatsession?.close()
                            (activity?.application as? LiveLikeApplication)?.removePrivateSession()
                        }
                        binding.prgDelete.visibility = View.INVISIBLE
                        binding.btnRefresh.callOnClick()
                    }
            } else {
                showToast("Enter Room ID")
            }
        }

        binding.btnVisibility.setOnClickListener {
            (activity as? ChatOnlyActivity)?.selectVisibility(object :
                ChatOnlyFragment.VisibilityInterface {
                override fun onSelectItem(visibility: Visibility) {
                    binding.btnVisibility.text = visibility.name
                }
            })
        }
        binding.btnRefresh.callOnClick()



        binding.rcylInvite.adapter = inviteAdapter
        binding.btnInviteListFirst.setOnClickListener {
            inviteList(LiveLikePagination.FIRST)
        }
        binding.btnInviteListPrevious.setOnClickListener {
            inviteList(LiveLikePagination.PREVIOUS)
        }
        binding.btnInviteListNext.setOnClickListener {
            inviteList(LiveLikePagination.NEXT)
        }
        binding.btnInviteList.setOnClickListener {
            inviteList(LiveLikePagination.FIRST)
        }

        binding.btnInviteByList.setOnClickListener {
            binding.prgInviteByList.visibility = View.VISIBLE
            (activity?.application as? LiveLikeApplication)?.sdk?.chat()
                ?.getInvitationsSentByCurrentProfileWithInvitationStatus(
                    LiveLikePagination.FIRST,
                    ChatRoomInvitationStatus.PENDING
                ) { result, error ->
                    result?.let {
                        AlertDialog.Builder(context).apply {
                            setTitle("List")
                            setItems(it.map { "${it.invitedProfile.nickname} => (${it.chatRoom.title},${it.chatRoomId})" }
                                .toTypedArray()) { _, _ ->
                            }
                            create()
                        }.show()
                    }
                    error?.let {
                        showToast(it)
                    }
                    binding.prgInviteByList.visibility = View.INVISIBLE
                }
        }

        (activity?.application as? LiveLikeApplication)?.sdk?.chat()?.chatRoomDelegate =
            object : ChatRoomDelegate() {
                override fun onNewChatRoomAdded(chatRoomAdd: ChatRoomAdd) {
                    showMessageDialog("Title: ${chatRoomAdd.chatRoomTitle}\nId: ${chatRoomAdd.chatRoomID}\nBy User: ${chatRoomAdd.senderNickname}")
                }

                override fun onReceiveInvitation(invitation: ChatRoomInvitation) {
                    showMessageDialog("Receive invitation from ${invitation.invitedBy.nickname} => ${invitation.invitedBy.userId}")
                }

                override fun onBlockProfile(blockedInfo: BlockedInfo) {
                    showMessageDialog("on Block Profile:$blockedInfo")
                }

                override fun onUnBlockProfile(blockInfoId: String, blockProfileId: String) {
                    showMessageDialog("On Unblock $blockInfoId,\n $blockProfileId")
                }
            }

//        btn_search.setOnClickListener {
//            search(LiveLikePagination.FIRST)
//        }
//
//        rcyl_members.adapter = adapter
//        btn_first.setOnClickListener {
//            search(LiveLikePagination.FIRST)
//        }
//        btn_next.setOnClickListener {
//            search(LiveLikePagination.NEXT)
//        }
//        btn_previous.setOnClickListener {
//            search(LiveLikePagination.PREVIOUS)
//        }
    }

    private fun showMessageDialog(text: String) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(text)
            .setCancelable(true)
            .show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        (activity?.application as? LiveLikeApplication)?.sdk?.chat()?.chatRoomDelegate = null
    }

    private fun inviteList(pagination: LiveLikePagination) {
        binding.prgInviteList.visibility = View.VISIBLE
        (activity?.application as? LiveLikeApplication)?.sdk?.chat()
            ?.getInvitationsReceivedByCurrentProfileWithInvitationStatus(
                pagination,
                ChatRoomInvitationStatus.PENDING
            ) { result, error ->
                result?.let {
                    inviteAdapter.inviteList.clear()
                    inviteAdapter.inviteList.addAll(it)
                    inviteAdapter.notifyDataSetChanged()
                    if (it.isEmpty()) {
                        showToast("List Empty")
                    }
                }
                error?.let {
                    showToast(it)
                }
                binding.prgInviteList.visibility = View.GONE
            }
    }

    private fun unblock(blockID: String) {
        (activity?.application as? LiveLikeApplication)?.sdk?.profile()
            ?.unBlockProfile(blockID) { result, error ->
                error?.let {
                    showToast(it)
                }
                result?.let {
                    showToast("Success Unblock")
                    binding.btnBlockList.callOnClick()
                }
            }
    }

    private fun updateInviteStatus(
        chatRoomInvitation: ChatRoomInvitation,
        chatRoomInvitationStatus: ChatRoomInvitationStatus
    ) {
        (activity?.application as? LiveLikeApplication)?.sdk?.chat()?.updateChatRoomInviteStatus(
            chatRoomInvitation,
            chatRoomInvitationStatus
        ) { result, error ->
            result?.let {
                showToast("Status: ${it.status}")
            }
            error?.let {
                showToast(it)
            }
            binding.btnInviteListFirst.callOnClick()
        }
    }


    fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    companion object {
        @JvmStatic
        fun newInstance() = ChatOnlyHomeFragment()
    }

//    private fun search(pagination: LiveLikePagination) {
//        val search = ed_search.text.toString()
//        (activity?.application as? LiveLikeApplication)?.sdk?.searchUser(search,
//            pagination,
//            object : LiveLikeCallback<List<LiveLikeUserApi>>() {
//                override fun onResponse(result: List<LiveLikeUserApi>?, error: String?) {
//                    error?.let {
//                        showToast(it)
//                    }
//                    result?.let {
//                        adapter.userList.clear()
//                        adapter.userList.addAll(it)
//                        adapter.notifyDataSetChanged()
//                    }
//                }
//
//            })
//    }
}

class InviteListAdapter(
    private val updateStatus: (invitation: ChatRoomInvitation, status: ChatRoomInvitationStatus) -> Unit
) :
    RecyclerView.Adapter<InviteListAdapter.InviteListViewHolder>() {
    inner class InviteListViewHolder(var itemBinding: InviteListItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root)

    val inviteList = arrayListOf<ChatRoomInvitation>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InviteListViewHolder {
        val itemBinding =
            InviteListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return InviteListViewHolder(
            itemBinding
        )
    }

    override fun onBindViewHolder(holder: InviteListViewHolder, position: Int) {
        val item = inviteList[position]
        holder.itemBinding.txtInvitation.text =
            "ChatRoom: ${item.chatRoom.title},${item.chatRoomId} ,By User: ${item.invitedBy.nickname}"
        holder.itemBinding.btnAccept.setOnClickListener {
            updateStatus.invoke(item, ChatRoomInvitationStatus.ACCEPTED)
        }
        holder.itemBinding.btnReject.setOnClickListener {
            updateStatus.invoke(item, ChatRoomInvitationStatus.REJECTED)
        }
    }

    override fun getItemCount(): Int = inviteList.size
}

class BlockedListAdapter(
    private val unBlockProfile: (blockId: String) -> Unit
) :
    RecyclerView.Adapter<BlockedListAdapter.BlockListViewHolder>() {
    inner class BlockListViewHolder(var blockBinding: BlockListItemBinding) :
        RecyclerView.ViewHolder(blockBinding.root)

    val blockedList = arrayListOf<BlockedInfo>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BlockListViewHolder {

        val blockBinding =
            BlockListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BlockListViewHolder(
            blockBinding
        )
    }

    override fun onBindViewHolder(holder: BlockListViewHolder, position: Int) {
        val item = blockedList[position]
        holder.blockBinding.txtTitle.text =
            "${item.blockedProfile.userId}(${item.blockedProfile.nickname})"

        holder.blockBinding.btnUnblock.setOnClickListener {
            unBlockProfile.invoke(item.id)
        }
    }

    override fun getItemCount(): Int = blockedList.size
}

