package com.livelike.livelikedemo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.livelike.engagementsdk.ContentSession
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.LiveLikeContentSession
import com.livelike.engagementsdk.core.services.messaging.proxies.LiveLikeWidgetEntity
import com.livelike.engagementsdk.core.services.messaging.proxies.WidgetLifeCycleEventsListener
import com.livelike.engagementsdk.fetchWidgetDetails
import com.livelike.engagementsdk.widget.viewModel.PollViewModel
import com.livelike.engagementsdk.widget.viewModel.WidgetStates
import com.livelike.engagementsdk.widget.widgetModel.PollWidgetModel
import com.livelike.livelikedemo.databinding.ActivityEmbedWidgetBinding
import com.livelike.livelikedemo.databinding.ItemWidgetBinding

import com.livelike.livelikedemo.databinding.TestWidgetBinding
import com.livelike.utils.logDebug
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


// Data class to hold widget information
data class WidgetInfo(
    val widgetId: String,
    val kind: String
)


class EmbedWidgetActivity : AppCompatActivity() {
    private lateinit var binding: ActivityEmbedWidgetBinding
    var session: LiveLikeContentSession? = null
    private lateinit var widgetAdapter: WidgetAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEmbedWidgetBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val channelManager = (application as LiveLikeApplication).channelManager
        val channel = channelManager.selectedChannel
        session = (application as LiveLikeApplication).createPublicSession(
            channel.llProgram.toString(),
            null,
        )

        setupRecyclerView()
        loadWidgets()
    }

    private fun setupRecyclerView() {
        widgetAdapter = WidgetAdapter((application as LiveLikeApplication).sdk)
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(this@EmbedWidgetActivity)
            adapter = widgetAdapter
        }
    }

    private fun loadWidgets() {
        // Sample widget list - replace with your actual widget data
        val widgets = listOf(
            WidgetInfo("1be12197-7263-4bd4-856a-ecc3ef18c563", "text-poll"),
            WidgetInfo("7ad9a986-738c-4711-9bdf-b71d8bd7dbe6", "text-quiz"),
            WidgetInfo("78213c48-dd85-4e76-8d14-badeb85b0f12", "image-quiz"),
            WidgetInfo("8d5abdc8-05b6-4908-a501-309233cde39a", "text-poll"),
            WidgetInfo("a0aa84e2-c424-40bf-bcee-76a9a752fc6f", "image-poll"),
            WidgetInfo("684b13f4-0039-461a-be73-694c6e8a1c97", "cheer-meter"),
        )
        widgetAdapter.submitList(widgets)
    }
}

class WidgetAdapter(
    private val sdk: EngagementSDK
) : RecyclerView.Adapter<WidgetAdapter.WidgetViewHolder>() {

    private var widgets = listOf<WidgetInfo>()

    fun submitList(newWidgets: List<WidgetInfo>) {
        widgets = newWidgets
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WidgetViewHolder {
        val binding = ItemWidgetBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return WidgetViewHolder(binding)
    }

    override fun onBindViewHolder(holder: WidgetViewHolder, position: Int) {
        holder.bind(widgets[position])
    }

    override fun getItemCount(): Int = widgets.size

    inner class WidgetViewHolder(
        private val binding: ItemWidgetBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            setupWidgetView()
        }

        private fun setupWidgetView() {
            binding.widgetView.apply {
                showTimer = false
                //enableDefaultWidgetTransition = false
                allowWidgetSwipeToDismiss = false
                showDismissButton = false

               /* widgetLifeCycleEventsListener = object : WidgetLifeCycleEventsListener() {
                    override fun onWidgetPresented(widgetData: LiveLikeWidgetEntity) {}

                    override fun onWidgetInteractionCompleted(widgetData: LiveLikeWidgetEntity) {}

                    override fun onWidgetDismissed(widgetData: LiveLikeWidgetEntity) {}

                    override fun onUserInteract(widgetData: LiveLikeWidgetEntity) {}

                    override fun onWidgetStateChange(
                        state: WidgetStates,
                        widgetData: LiveLikeWidgetEntity
                    ) {
                        when (state) {
                            WidgetStates.READY -> {
                                moveToNextState()
                            }
                            WidgetStates.INTERACTING -> {
                                println("Widget ${widgetData.id} in Interacting State")
                            }
                            WidgetStates.RESULTS -> {
                                println("Widget ${widgetData.id} in Results State")
                            }
                            WidgetStates.FINISHED -> {
                                println("Widget ${widgetData.id} in Finished State")
                            }
                        }
                    }
                }*/
            }
        }

        fun bind(widgetInfo: WidgetInfo) {
            sdk.fetchWidgetDetails(
                widgetInfo.widgetId,
                widgetInfo.kind
            ) { result, error ->
                result?.let {
                    binding.widgetView.displayWidget(
                        sdk,
                        result,
                        showWithInteractionData = true
                    )
                   // binding.widgetView.moveToNextState()
                }
                error?.let {
                    Toast.makeText(
                        binding.root.context,
                        it,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }
}