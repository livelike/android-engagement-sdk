package com.livelike.livelikedemo

// Preference keys
const val PREF_USER_ACCESS_TOKEN = "user_access_token"
const val PREF_USER_ACCESS_TOKEN_1 = "user_access_token_1"

const val PREF_CHAT_ROOM_LAST_TIME = "chat_room_last_time_map"
const val PREF_MY_WIDGETS = "my_widgets"
