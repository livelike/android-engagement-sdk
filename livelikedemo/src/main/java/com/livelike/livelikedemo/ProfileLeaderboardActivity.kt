package com.livelike.livelikedemo

import android.app.AlertDialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.core.data.models.LeaderBoardEntry
import com.livelike.engagementsdk.leaderboard
import com.livelike.livelikedemo.databinding.ProfileLeaderboardBinding

class ProfileLeaderboardActivity : AppCompatActivity() {
    private lateinit var binding: ProfileLeaderboardBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ProfileLeaderboardBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val leaderboardClient = (applicationContext as LiveLikeApplication).sdk.leaderboard()
        binding.leaderboards.setOnClickListener {
            val profileIds = binding.profileId.text.toString()
            leaderboardClient.getProfileLeaderboards(
                profileIds.trim(),
                LiveLikePagination.FIRST
            ) { result, error ->
                buildResultDialog(result, error)
            }
        }

        binding.leaderboardViews.setOnClickListener {
            val profileIds = binding.profileId.text.toString()
            leaderboardClient.getProfileLeaderboardViews(
                profileIds.trim(), LiveLikePagination.FIRST
            ) { result, error ->
                buildResultDialog(result, error)
            }
        }
    }


    private fun buildResultDialog(result: List<LeaderBoardEntry>?, error: String?) {
        error?.let {
            AlertDialog.Builder(this)
                .setMessage(it)
                .create()
                .show()
        }

        result?.let {
            AlertDialog.Builder(this)
                .setTitle("Total count: " + result.size)
                .setItems(it.map { leaderboard ->
                    "\nprofile Id: ${leaderboard.profileId}, profile nickname : ${leaderboard.profileNickname}, " +
                            "percentile rank: ${leaderboard.entry?.percentileRank}, " +
                            "rank: ${leaderboard.rank}, score:${leaderboard.entry?.score} ," +
                            "leaderboard reward item :- ${leaderboard.leaderboard?.rewardItem}"
                }.toTypedArray()) { _, _ -> }
                .create()
                .show()
        }
    }
}