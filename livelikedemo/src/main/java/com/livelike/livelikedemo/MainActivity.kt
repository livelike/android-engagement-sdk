package com.livelike.livelikedemo

import android.app.Activity
import android.app.AlertDialog
import android.content.*
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonParser
import com.livelike.common.profile
import com.livelike.common.user
import com.livelike.engagementsdk.*
import com.livelike.engagementsdk.chat.data.remote.*
import com.livelike.engagementsdk.core.utils.isNetworkConnected
import com.livelike.engagementsdk.publicapis.Access
import com.livelike.engagementsdk.publicapis.LiveLikeUserApi
import com.livelike.engagementsdk.publicapis.SmartContractDetails
import com.livelike.livelikedemo.LiveLikeApplication.Companion.CHAT_ROOM_LIST
import com.livelike.livelikedemo.LiveLikeApplication.Companion.PREFERENCES_APP_ID
import com.livelike.livelikedemo.databinding.ActivityMainBinding
import com.livelike.livelikedemo.databinding.ActvityTestPubnubSubscribeBinding
import com.livelike.livelikedemo.pubnub.PubNubTestActivity
import com.livelike.livelikedemo.sceenic.SceenicDemoActivity
import com.livelike.livelikedemo.ui.main.SmartContractsAdapter
import com.livelike.livelikedemo.utils.ThemeRandomizer
import com.livelike.utils.Result
import com.livelike.utils.logDebug
import kotlinx.coroutines.launch
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.lang.ref.WeakReference
import java.nio.charset.StandardCharsets
import kotlin.reflect.KClass

class MainActivity : AppCompatActivity() {

    data class PlayerInfo(
        val playerName: String,
        val cls: KClass<out Activity>,
        var theme: Int,
        var keyboardClose: Boolean = true,
        var showNotification: Boolean = false,
        var jsonTheme: String? = null,
        var avatarUrl: String? = null,
        var showAvatar: Boolean = true,
        var customCheerMeter: Boolean = false,
        var showLink: Boolean = false,
        var showImagePicker: Boolean = false,
        var customLink: String? = null,
        var allowDiscard: Boolean = true,
        var allowDefaultChatRoom: Boolean = true,
        var quoteMsg: Boolean = false,
        var hideDeletedMessage:Boolean = false,
        var includeFilteredChatMessages: Boolean = false,
        var allowDelete: Boolean = true,
        var showPicker: Boolean = true
    )

    private lateinit var userStream: Stream<LiveLikeUserApi>
    private var networkCallback: ConnectivityManager.NetworkCallback? = null
    private val selectedEnvironment: String = "Selected App Environment"
    private val mConnReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (context.isNetworkConnected()) {
                (application as LiveLikeApplication).channelManager.loadClientConfig()
            }
        }
    }
    private var chatRoomIds: MutableSet<String> = mutableSetOf()
    internal lateinit var binding: ActivityMainBinding

    override fun onDestroy() {
        ExoPlayerActivity.privateGroupRoomId = null

        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            cm.unregisterNetworkCallback(networkCallback!!)
        } else {
            unregisterReceiver(mConnReceiver)
        }
        userStream.unsubscribe(this)
        (application as LiveLikeApplication).removePublicSession()
        (application as LiveLikeApplication).removePrivateSession()
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (this.isTaskRoot) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                this.finishAfterTransition()
            }
        } else {
            super.onBackPressed()
        }
    }

    private fun registerNetWorkCallback() {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            networkCallback = object : ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
//                    (application as LiveLikeApplication).initSDK()
                    (application as LiveLikeApplication).channelManager.loadClientConfig()
                }

                override fun onLost(network: Network) {
                    super.onLost(network)
                }

                override fun onUnavailable() {
                    super.onUnavailable()
                }
            }
            cm.registerDefaultNetworkCallback(networkCallback!!)
        } else {
            @Suppress("DEPRECATION") //kept due to pre M support
            registerReceiver(mConnReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        }
    }

    val player = PlayerInfo(
        "Exo Player",
        ExoPlayerActivity::class,
        R.style.Default,
        true,
    )

    val onlyWidget = PlayerInfo(
        "Widget Only",
        WidgetOnlyActivity::class,
        R.style.Default,
        true,
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        (application as LiveLikeApplication).selectEnvironment("QA")
        binding.envLabel.text = "QA"
        binding.eventsLabel.text = "Select Channel"
        registerNetWorkCallback()
        binding.sdkVersion.text =
            "SDK Version : ${com.livelike.engagementsdk.BuildConfig.SDK_VERSION}"
        if (BuildConfig.VERSION_CODE > 1) {
            binding.buildNo.text = "Bitrise build : ${BuildConfig.VERSION_CODE}"
        }
        binding.edCustomData.setText("{\"age\":35,\"lang\":\"en\"}")
        val drawerDemoActivity =
            PlayerInfo("Exo Player", TwoSessionActivity::class, R.style.Default, false)

        binding.layoutSidePanel.setOnClickListener {
            player.customLink = binding.edLinkCustom.text.toString()
            player.quoteMsg = binding.chkEnableQuoteMsg.isChecked
            player.hideDeletedMessage = binding.chkEnableHideDeletedMessage.isChecked
            startActivity(playerDetailIntent(player))
        }

        binding.envButton.setOnClickListener {
            AlertDialog.Builder(this).apply {
                setTitle("Select Environment")
                setItems(LiveLikeApplication.environmentMap.map { it.key }
                    .toTypedArray()) { _, which ->
                    (application as LiveLikeApplication).selectEnvironment(LiveLikeApplication.environmentMap.keys.toList()[which])
                    binding.eventsLabel.text = "Select Channel"
                    LiveLikeApplication.environmentMap.keys.toList()[which].let {
                        binding.envLabel.text = it
                        getSharedPreferences(PREFERENCES_APP_ID, Context.MODE_PRIVATE).apply {
                            edit().putString(selectedEnvironment, it).apply()
                        }
                    }
                }
                create()
            }.show()
        }

        binding.layoutOverlay.setOnClickListener {
            startActivity(playerDetailIntent(drawerDemoActivity))
        }

        // chk_show_dismiss.isChecked = player.showNotification
        binding.chkShowDismiss.isChecked = false
        binding.chkShowDismiss.setOnCheckedChangeListener { _, isChecked ->
            player.showNotification = isChecked
            onlyWidget.showNotification = isChecked
        }
        binding.chkShowAvatar.isChecked = player.showAvatar
        binding.chkShowAvatar.setOnCheckedChangeListener { _, isChecked ->
            player.showAvatar = isChecked
        }
        binding.chkAllowDelete.isChecked = player.allowDelete
        binding.chkAllowDelete.setOnCheckedChangeListener { _, isChecked ->
            player.allowDelete = isChecked
        }

        binding.chkCustomWidgetsUi.setOnCheckedChangeListener { _, isChecked ->
            player.customCheerMeter = isChecked
            onlyWidget.customCheerMeter = isChecked
            LiveLikeApplication.showCustomWidgetsUI = isChecked
        }
        binding.chkAllowDiscardOwnMsg.isChecked = true
        binding.chkAllowDiscardOwnMsg.setOnCheckedChangeListener { btn, isChecked ->
            player.allowDiscard = isChecked
        }

        binding.chkEnableDebug.isChecked = EngagementSDK.enableDebug
        binding.chkEnableDebug.setOnCheckedChangeListener { _, isChecked ->
            EngagementSDK.enableDebug = isChecked
        }
        binding.chkShowLinks.setOnCheckedChangeListener { _, isChecked ->
            player.showLink = isChecked
        }
        binding.chkShowCustomImagePiker.setOnCheckedChangeListener { _, isChecked ->
            player.showImagePicker = isChecked
        }
        binding.chkEnableQuoteMsg.setOnCheckedChangeListener { _, isChecked ->
            player.quoteMsg = isChecked
        }

        binding.chkEnableIncludeFilter.setOnCheckedChangeListener { _, isChecked ->
            player.includeFilteredChatMessages = isChecked
        }

        binding.chkShowPicker.setOnCheckedChangeListener { _, isChecked ->
            player.showPicker = isChecked
        }
        binding.chkShowPicker.isChecked = player.showPicker

        binding.chkAllowDefaultLoadChatRoom.isChecked = true
        binding.chkAllowDefaultLoadChatRoom.setOnCheckedChangeListener { _, isChecked ->
            player.allowDefaultChatRoom = isChecked
        }


        binding.sampleApp.setOnClickListener {
            startActivity(Intent(this, SampleAppActivity::class.java))
        }

        binding.eventsButton.setOnClickListener {
            (application as LiveLikeApplication).channelManager.let { channelManager ->
                val channels = channelManager.getChannels()
                AlertDialog.Builder(this).apply {
                    setTitle("Choose a channel to watch!")
                    setItems(channels.map { it.name }.toTypedArray()) { _, which ->
                        channelManager.selectedChannel = channels[which]
                        binding.eventsLabel.text = channelManager.selectedChannel.name
                    }
                    if (channelManager.nextUrl?.isNotEmpty() == true)
                        setPositiveButton(
                            "Load Next"
                        ) { dialog, _ ->
                            channelManager.loadClientConfig(channelManager.nextUrl)
                            dialog.dismiss()
                        }
                    if (channelManager.previousUrl?.isNotEmpty() == true)
                        setNeutralButton(
                            "Load Previous"
                        ) { dialog, _ ->
                            channelManager.loadClientConfig(channelManager.previousUrl)
                            dialog.dismiss()
                        }
                    create()
                }.show()
            }
        }

        binding.liveBlog.setOnClickListener {
            startActivity(Intent(this, LiveBlogActivity::class.java))
        }

        binding.timelineTwo.setOnClickListener {
            startActivity(Intent(this, IntractableTimelineActivity::class.java))
        }

        binding.leaderboardButton.setOnClickListener {
            startActivity(Intent(this, LeaderBoardActivity::class.java))
        }

        binding.unclaimedInteraction.setOnClickListener {
            startActivity(Intent(this, UnclaimedInteractionActivity::class.java))
        }

        binding.widgetsJsonButton.setOnClickListener {
            startActivity(Intent(this, WidgetJsonActivity::class.java))
        }

        binding.sceenicDemo.setOnClickListener {
            startActivity(Intent(this, SceenicDemoActivity::class.java))
        }

        binding.privateGroupButton.setOnClickListener {
            AlertDialog.Builder(this).apply {
                setTitle("Select a private group")
                setItems(chatRoomIds.toTypedArray()) { _, which ->
                    // On change of theme we need to create the session in order to pass new attribute of theme to widgets and chat
                    (application as LiveLikeApplication).removePrivateSession()
                    binding.privateGroupLabel.text = chatRoomIds.elementAt(which)
                    ExoPlayerActivity.privateGroupRoomId = chatRoomIds.elementAt(which)

                    // Copy to clipboard
                    var clipboard: ClipboardManager =
                        getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                    var clip = ClipData.newPlainText("label", chatRoomIds.elementAt(which))
                    clipboard.setPrimaryClip(clip)
                    Toast.makeText(
                        applicationContext,
                        "Room Id Copy To Clipboard",
                        Toast.LENGTH_LONG
                    )
                        .show()
                }
                create()
            }.show()
        }

        binding.themesButton.setOnClickListener {
            val channels = arrayListOf("Default", "Turner", "Custom Chat Reaction", "None")
            AlertDialog.Builder(this).apply {
                setTitle("Choose a theme!")
                setItems(channels.toTypedArray()) { _, which ->
                    // On change of theme we need to create the session in order to pass new attribute of theme to widgets and chat
                    (application as LiveLikeApplication).removePublicSession()
                    binding.themesLabel.text = channels[which]
                    EngagementSDK.enableDebug = false
                    player.theme = when (which) {
                        0 -> R.style.Default
                        1 -> {
                            EngagementSDK.enableDebug = false
                            R.style.MMLChatTheme
                        }

                        2 -> {
                            EngagementSDK.enableDebug = false
                            R.style.CustomChatReactionTheme
                        }

                        else -> R.style.AppTheme
                    }
                    onlyWidget.theme = when (which) {
                        0 -> R.style.Default
                        1 -> {
                            EngagementSDK.enableDebug = false
                            R.style.MMLChatTheme
                        }

                        2 -> {
                            EngagementSDK.enableDebug = false
                            R.style.CustomChatReactionTheme
                        }

                        else -> R.style.AppTheme
                    }
                }
                create()
            }.show()
        }

        binding.themesJsonButton.setOnClickListener {
            setupJsonThemesFilePath()
//            DialogUtils.showFilePicker(
//                this,
//                DialogSelectionListener { files ->
//
//                }
//            )
        }

        binding.testCompose.setOnClickListener {
            startActivity(Intent(this, TestWithComposeActivity::class.java))
        }

        binding.testPubNub.setOnClickListener {
            startActivity(Intent(this, PubNubTestActivity::class.java))
        }

        //startActivity(Intent(this, MainActivity3::class.java))

        binding.eventsLabel.text =
            (application as LiveLikeApplication).channelManager.selectedChannel.name

        getSharedPreferences(PREFERENCES_APP_ID, Context.MODE_PRIVATE).apply {
//            getString("UserNickname", "")
//                .let {
//                    binding.nicknameText.setText(it)
////                edit().putString("userPic","http://lorempixel.com/200/200/?$it").commit()
//                }

//            getString("userPic", "").let {
//                if (it.isNullOrEmpty()) {
//                    edit().putString(
//                        "userPic",
//                        "https://loremflickr.com/200/200?lock=${java.util.UUID.randomUUID()}"
//                    ).apply()
//                } else {
//                    edit().putString("userPic", it).apply()
//                }
//            }
            getString(selectedEnvironment, null)?.let {
                (application as LiveLikeApplication).selectEnvironment(it)
                binding.eventsLabel.text = "Select Channel"
                binding.envLabel.text = it
            }
            chatRoomIds = getStringSet(CHAT_ROOM_LIST, mutableSetOf()) ?: mutableSetOf()
        }

        (application as LiveLikeApplication).sdk.user().getCurrentUserDetails { result, error ->
            result?.let {
                binding.txtNicknameServer.text = it.nickname + " " + it.profilePic
                binding.nicknameText.setText(it.nickname)
                binding.edAvatar.setText(it.profilePic)
            }
            error?.let {
                Toast.makeText(this@MainActivity, it, Toast.LENGTH_SHORT).show()
            }
        }

        binding.btnCreate.setOnClickListener {
            val title = binding.chatroomText.text.toString()
            binding.progressBar.visibility = View.VISIBLE
            fun liveLikeCallback(result: ChatRoom?, error: String?) {
                binding.textView2.text = when {
                    result != null -> "${result.title ?: "No Title"}(${result.id})"
                    else -> error
                }
                result?.let {
                    chatRoomIds.add(it.id)
                    getSharedPreferences(PREFERENCES_APP_ID, Context.MODE_PRIVATE)
                        .edit().apply {
                            putStringSet(CHAT_ROOM_LIST, chatRoomIds).apply()
                        }
                }
                binding.progressBar.visibility = View.GONE
            }

            val chat = (application as LiveLikeApplication).sdk.chat()

            if (binding.toggleCreatechat.isChecked)// Token Gating On
                chat.createChatRoom(title, null, getTokenGatesList(), ::liveLikeCallback)
            else
                chat.createChatRoom(title, null, liveLikeCallback = ::liveLikeCallback)
        }

        binding.btnJoin.setOnClickListener {
            val chatRoomId = binding.chatroomText1.text.toString()
            if (chatRoomId.isEmpty().not()) {
                chatRoomIds.add(chatRoomId)
                getSharedPreferences(PREFERENCES_APP_ID, Context.MODE_PRIVATE)
                    .edit().putStringSet(CHAT_ROOM_LIST, chatRoomIds).apply()
                binding.chatroomText1.setText("")
            }
        }

        binding.toggleAutoKeyboardHide.setOnCheckedChangeListener { _, isChecked ->
            player.keyboardClose = isChecked
        }
        binding.toggleAutoKeyboardHide.isChecked = player.keyboardClose

        binding.chatInputVisibilitySwitch.setOnCheckedChangeListener { _, isChecked ->
            ExoPlayerActivity.isHideChatInput = isChecked
        }
        binding.toggleCreatechat.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                addTokenGateView()
            } else {
                binding.tvAdd.visibility = View.GONE
                binding.layout.removeAllViews()
            }
        }

        binding.requestCall.setOnClickListener {
            startActivity(Intent(this, NetworkRequestActivity::class.java))
        }

        binding.tvAdd.setOnClickListener {
            val editText =
                binding.layout.getChildAt(0).findViewById<EditText>(R.id.ed_contract_address)
            if (TextUtils.isEmpty(editText.text.toString())) {
                Toast.makeText(this, "Contract Address Can't be Empty", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            addTokenGateView()
        }
        /*binding.nicknameText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                getSharedPreferences(PREFERENCES_APP_ID, Context.MODE_PRIVATE).edit().apply {
                    putString("UserNickname", p0?.trim().toString())
                }.apply()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })*/
        /*binding.nicknameText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                getSharedPreferences(PREFERENCES_APP_ID, Context.MODE_PRIVATE).edit().apply {
                    putString("UserNickname", p0?.trim().toString())
                }.apply()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })*/

        userStream = (application as LiveLikeApplication).sdk.profile().profileStream
        logDebug { "ToStream9:$userStream" }
        UserStreamCallbackWrapper(this, userStream)

        lifecycleScope.launch {
            (application as LiveLikeApplication).sdk.profile().currentProfileFlow.collect {
                println("Current User: $it")
            }
        }

        binding.btnUserDetails.setOnClickListener {
            (application as LiveLikeApplication).sdk.user().getCurrentUserDetails { result, error ->
                result?.let {
                    Toast.makeText(
                        applicationContext,
                        "User:$it",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                error?.let {
                    Toast.makeText(
                        applicationContext,
                        it,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

        binding.btnProgramFromCustomId.setOnClickListener {
            (application as LiveLikeApplication).sdk.getProgramByCustomID("android_custom_id") { result, error ->
                result?.let { println("program:$it") }
                error?.let { println("error:$it") }
            }
        }

        binding.btnNickName.setOnClickListener {
            if (binding.nicknameText.text.toString().isEmpty().not())
                (application as LiveLikeApplication).sdk.user()
                    .updateChatNickname(binding.nicknameText.text.toString()) { result, error ->
                        result?.let {
                            Toast.makeText(this@MainActivity, "$it", Toast.LENGTH_SHORT)
                                .show()
                        }
                        error?.let {
                            Toast.makeText(
                                this@MainActivity,
                                "$it",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
        }
        binding.btnCustomData.setOnClickListener {
            (application as LiveLikeApplication).sdk.user()
                .updateUserCustomData(binding.edCustomData.text.toString()) { result, error ->
                    println("Completed update custom data")
                }
        }

        binding.widgetsFrameworkButton.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    WidgetFrameworkTestActivity::class.java
                )
            )
        }

        binding.widgetViewModel.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    WidgetActivity::class.java
                )
            )
        }

        binding.leaderboardRank.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    LeaderBoardPositionActitivty::class.java
                )
            )
        }

        binding.edAvatar.setText("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSmyhNs7yHwEYgBHE0MNK3H5YeDbCcf3BDF9A&usqp=CAU")

        binding.btnAvatar.setOnClickListener {
            val url = binding.edAvatar.text.toString()
            player.avatarUrl = url
            (application as LiveLikeApplication).sdk.profile()
                .updateChatUserPic(url) { result, error ->
                    result?.let {
                        println("$it")
                    }
                    error?.let {
                        println("$it")
                    }
                }
        }

        binding.btnAvatarRemove.setOnClickListener {
            player.avatarUrl = null
        }

        binding.widgetsOnlyButton.setOnClickListener {
            startActivity(playerDetailIntent(onlyWidget))
        }
        binding.chatOnlyButton.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    ChatOnlyActivity::class.java
                )
            )
        }
        binding.customChat.setOnClickListener {
            if ((application as LiveLikeApplication).channelManager.getChannels().isNotEmpty())
                startActivity(Intent(this, CustomChatActivity::class.java))
            else
                Toast.makeText(this, "Please wait for events loading", Toast.LENGTH_SHORT).show()
        }
        binding.sponsorTest.setOnClickListener {
            startActivity(Intent(this, SponsorTestActivity::class.java))
        }
        binding.badgesCollection.setOnClickListener {
            startActivity(Intent(this, BadgesCollectionActivity::class.java))
        }

        binding.rewardsClientTest.setOnClickListener {
            startActivity(Intent(this, RewardsClientTestActivity::class.java))
        }

        binding.rewardsClientTest.setOnClickListener {
            startActivity(Intent(this, RewardsClientTestActivity::class.java))
        }

        binding.questClientTest.setOnClickListener {
            startActivity(Intent(this, QuestTestActivity::class.java))
        }

        binding.getWidgetFilter.setOnClickListener {
            startActivity(Intent(this, GetWidgetTestActivity::class.java))
        }

        binding.reactionDemo.setOnClickListener {
            startActivity(Intent(this, ReactionActivity::class.java))
        }

        binding.invokedAction.setOnClickListener {
            startActivity(Intent(this, InvokedActionsHistoryActivity::class.java))
        }

        binding.leaderboardAction.setOnClickListener {
            startActivity(Intent(this, ProfileLeaderboardActivity::class.java))
        }

        binding.commentBoardAction.setOnClickListener {
            startActivity(Intent(this, CommentBoardsTestActivity::class.java))
        }

        binding.commentDemo.setOnClickListener {
            startActivity((Intent(this, CommentDemo::class.java)))
        }

        binding.commentReportDemo.setOnClickListener {
            startActivity((Intent(this, CommentReportActivity::class.java)))
        }

        binding.commentStock.setOnClickListener {
            startActivity((Intent(this, CommentStockUi::class.java)))
        }

        binding.socialDemo.setOnClickListener {
            startActivity((Intent(this, SocialClientTestActivity::class.java)))
        }

        binding.presenceDemo.setOnClickListener {
            startActivity((Intent(this, PresenceClientTestActivity::class.java)))
        }

        binding.testrbac.setOnClickListener {
            startActivity((Intent(this, RBACDemoActivity::class.java)))
        }

        binding.tgBtnJoin.setOnClickListener {
            binding.tgProgressBar1.visibility = View.VISIBLE
            (application as LiveLikeApplication).sdk.chat()
                .getTokenGatedChatRoomAccessDetails(
                    binding.tgChatroomText2.text.toString(),
                    binding.tgChatroomText1.text.toString()
                ) { result, error ->
                    binding.tgProgressBar1.visibility = View.INVISIBLE
                    result?.let {
                        if (result.access.name.equals(Access.allowed.name))
                            println { "You are allowed to enter in to the Chat Room" }
                        else
                            println { "You are not allowed to enter in to the Chat Room" }

                        showMessage(result.details)
                    }
                    error?.let {
                        showMessage(error)
                    }
                }
        }

        (application as LiveLikeApplication).removePublicSession()
        (application as LiveLikeApplication).removePrivateSession()

    }

    private fun getTokenGatesList(): List<ChatRoomTokenGate> {
        val list: MutableList<ChatRoomTokenGate> = mutableListOf()
        for (i in 0 until binding.layout.childCount) {
            val layoutTokenGate = binding.layout.getChildAt(i)
            val ed_contractAddress =
                layoutTokenGate.findViewById<EditText>(R.id.ed_contract_address)
            val spin_network = layoutTokenGate.findViewById<Spinner>(R.id.spinner_network)
            val spinTokenType = layoutTokenGate.findViewById<Spinner>(R.id.spinner_tokentype)
            val layoutTraits = layoutTokenGate.findViewById<LinearLayout>(R.id.layout_traits)
            var chatRoomTokenGate: ChatRoomTokenGate? = null
            if (layoutTraits.childCount == 0) {
                chatRoomTokenGate = ChatRoomTokenGate(
                    ed_contractAddress.text.toString(),
                    spin_network.selectedItem as Networks
                )
            } else {
                val attributes: ArrayList<Attributes> = ArrayList()
                for (i in 0 until layoutTraits.childCount) {
                    val traitChild = layoutTraits.getChildAt(i)
                    val edTraitName = traitChild.findViewById<EditText>(R.id.ed_traitname)
                    val edTraitValue = traitChild.findViewById<EditText>(R.id.ed_traitvalue)
                    attributes.add(
                        Attributes(
                            edTraitName.text.toString(),
                            edTraitValue.text.toString()
                        )
                    )
                }
                chatRoomTokenGate = ChatRoomTokenGate(
                    ed_contractAddress.text.toString(),
                    spin_network.selectedItem as Networks,
                    spinTokenType.selectedItem as TokenTypes,
                    attributes
                )
            }
            list.add(chatRoomTokenGate)
        }
        return list
    }


    private fun addTokenGateView() {
        binding.tvAdd.visibility = View.VISIBLE
        val vi =
            applicationContext.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val v: View = vi.inflate(R.layout.token_gate_view, null)

        val btnRemove = v.findViewById<View>(R.id.remove_btn) as ImageButton
        val spin_network = v.findViewById<Spinner>(R.id.spinner_network)
        val spin_token = v.findViewById<Spinner>(R.id.spinner_tokentype)
        val btnAddTrait = v.findViewById<Button>(R.id.btn_add_trait)
        val layoutTraitView = v.findViewById<LinearLayout>(R.id.layout_traits)
        val edContract = v.findViewById<EditText>(R.id.ed_contract_address)
        fun EditText.onRightDrawableClicked(onClicked: (view: EditText) -> Unit) {
            this.setOnTouchListener { v, event ->
                var hasConsumed = false
                if (v is EditText) {
                    if (event.x >= v.width - v.totalPaddingRight) {
                        if (event.action == MotionEvent.ACTION_UP) {
                            onClicked(this)
                        }
                        hasConsumed = true
                    }
                }
                hasConsumed
            }
        }

        edContract.onRightDrawableClicked {
            it.clearFocus()
            val popupWindow = PopupWindow(this)

            val listViewNames = RecyclerView(this)
            listViewNames.layoutManager = LinearLayoutManager(this)

            val contractNamesList = ArrayList<SmartContractDetails?>()
            val adapter = SmartContractsAdapter(contractNamesList)
            listViewNames.adapter = adapter

            loadNames(LiveLikePagination.FIRST, adapter, contractNamesList)

            val lm = listViewNames.layoutManager as LinearLayoutManager
            listViewNames.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(
                    rv: RecyclerView,
                    dx: Int,
                    dy: Int
                ) {
                    /**
                     * load on scroll
                     **/
                    val totalItemCount = lm.itemCount
                    val lastVisibleItem = lm.findLastVisibleItemPosition()
                    if (totalItemCount <= (lastVisibleItem + 1)) {
                        loadNames(LiveLikePagination.NEXT, adapter, contractNamesList)
                    }
                }
            })

            adapter.onItemClick = {
                popupWindow.dismiss()
                edContract.setText(it?.contractAddress)
                it?.networkType?.let { network ->
                    spin_network.setSelection(
                        Networks.values()
                            .indexOf(Networks.valueOf(network))
                    )
                }
            }

            val display = DisplayMetrics()
            getWindowManager().getDefaultDisplay().getMetrics(display)
            popupWindow.isFocusable = true
            popupWindow.width = it.width
            popupWindow.height = display.heightPixels * 20 / 100
            popupWindow.contentView = listViewNames
            popupWindow.showAsDropDown(it, -5, 0)
        }

        val values = Networks.values()
        spin_network.adapter = ArrayAdapter(
            this,
            android.R.layout.simple_dropdown_item_1line,
            values
        )
        spin_token.adapter = ArrayAdapter(
            this,
            android.R.layout.simple_dropdown_item_1line,
            TokenTypes.values()
        )

        btnAddTrait.setOnClickListener {
            addTraitView(layoutTraitView)
        }

        spin_token.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (TokenTypes.values().get(position) == TokenTypes.nonfungible) {
                    btnAddTrait.visibility = View.VISIBLE
                } else {
                    btnAddTrait.visibility = View.GONE
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        btnRemove.setOnClickListener {
            binding.layout.removeView(v)
            if (binding.layout.childCount == 0) binding.toggleCreatechat.isChecked = false
        }

        binding.layout.addView(
            v, 0, ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )
    }

    private fun loadNames(
        pagination: LiveLikePagination, adapter: SmartContractsAdapter,
        list: ArrayList<SmartContractDetails?>
    ) {
        (application as LiveLikeApplication).sdk.chat()
            .getSmartContracts(pagination) { result, error ->
                result?.let {
                    list.addAll(it)
                    adapter.notifyDataSetChanged()
                }
                error?.let {

                }
            }
    }

    private fun addTraitView(layoutTraitView: LinearLayout?) {
        val vi =
            applicationContext.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val v: View = vi.inflate(R.layout.layout_add_trait, null)
        val btnRemove = v.findViewById<View>(R.id.btn_remove_trait)

        btnRemove.setOnClickListener {
            layoutTraitView?.removeView(v)
        }
        layoutTraitView?.addView(v)
    }

    private fun showMessage(message: String?) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(message)
        builder.setPositiveButton(android.R.string.ok) { dialog, which ->
            dialog.cancel()
        }
        builder.show()
    }

    private fun setupJsonThemesFilePath() {
//        if (files?.isNotEmpty() == true) {
//            ThemeRandomizer.themesList.clear()
//            binding.themesJsonLabel.post {
//                binding.themesJsonLabel.text = "${files.size} selected"
//            }
//        } else {
//            binding.themesJsonLabel.post {
//                binding.themesJsonLabel.text = "None"
//            }
//        }
//        for (file in files ?: emptyArray()) {
//            val fin = FileInputStream(file)
//            val theme: String? = convertStreamToString(fin)
        // Make sure you close all streams.
//        fin.close()
        val theme: String? =
            assets.open("themes/livelike-theme-v2.json").bufferedReader().use { it.readText() }
        val element =
            LiveLikeEngagementTheme.instanceFrom(JsonParser.parseString(theme).asJsonObject)
        if (element is Result.Success) {
            element.data.fontFamilyProvider = object : FontFamilyProvider {
                override fun getTypeFace(fontFamilyName: String): Typeface? {
                    if (fontFamilyName.contains("Pangolin"))
                        return Typeface.createFromAsset(
                            resources.assets,
                            "fonts/Pangolin-Regular.ttf"
                        )
                    else if (fontFamilyName.contains("Raleway")) {
                        return Typeface.createFromAsset(
                            resources.assets,
                            "fonts/Raleway-Regular.ttf"
                        )
                    }
                    return null
                }
            }
            ThemeRandomizer.themesList.add(element.data)
        }
        if (theme != null) {
            player.jsonTheme = theme
            onlyWidget.jsonTheme = theme
        } else
            binding.themesJsonLabel.post {
                Toast.makeText(
                    applicationContext,
                    "Unable to get the theme json",
                    Toast.LENGTH_LONG
                ).show()
            }
//        }
    }
}

@Throws(java.lang.Exception::class)
fun convertStreamToString(inputStream: InputStream?): String? {
    val reader = BufferedReader(InputStreamReader(inputStream))
    val sb = java.lang.StringBuilder()
    var line: String?
    while (reader.readLine().also { line = it } != null) {
        sb.append(line).append("\n")
    }
    reader.close()
    return sb.toString()
}

fun Context.playerDetailIntent(player: MainActivity.PlayerInfo): Intent {
    val intent = Intent(this, player.cls.java)
    intent.putExtra("theme", player.theme)
    intent.putExtra("jsonTheme", player.jsonTheme)
    intent.putExtra("showNotification", player.showNotification)
    intent.putExtra("avatarUrl", player.avatarUrl)
    intent.putExtra("showAvatar", player.showAvatar)
    intent.putExtra("customCheerMeter", player.customCheerMeter)
    intent.putExtra("showLink", player.showLink)
    intent.putExtra("showImagePicker", player.showImagePicker)
    intent.putExtra("customLink", player.customLink)
    intent.putExtra("enableReplies", player.quoteMsg)
    intent.putExtra("hideDeletedMessage", player.hideDeletedMessage)
    intent.putExtra("includeFilteredChatMessages", player.includeFilteredChatMessages)
    intent.putExtra("allowDiscard", player.allowDiscard)
    intent.putExtra("allowDefaultLoadChatRoom", player.allowDefaultChatRoom)
    intent.putExtra("allowDelete", player.allowDelete)
    intent.putExtra("showPicker", player.showPicker)
    intent.putExtra(
        "keyboardClose",
        when (player.theme) {
            R.style.MMLChatTheme -> player.keyboardClose
            else -> true
        }
    )
    return intent
}

fun getFileFromAsset(context: Context, path: String): String? {
    try {
        val asset = context.assets
        val br = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            BufferedReader(InputStreamReader(asset.open(path), StandardCharsets.UTF_8))
        } else {
            BufferedReader(InputStreamReader(asset.open(path)))
        }
        val sb = StringBuilder()
        var str: String?
        while (br.readLine().also { str = it } != null) {
            sb.append(str)
        }
        br.close()
        return sb.toString()
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return null
}

/*
can't seem to rely on the onDestroy callback happening on app shutdown it seems
unable to disconnect from main activity. If this is truly just a race between
leak canary and onDestroy this will be no worse then the leak itself or better.
 */
private class UserStreamCallbackWrapper(
    activity: MainActivity,
    userStream: Stream<LiveLikeUserApi>
) {

    init {
        val mainActivity = WeakReference(activity)
        userStream.subscribe(activity.hashCode()) {
            mainActivity.get()?.apply {
                runOnUiThread {
                    activity.binding.txtNicknameServer.text = it?.nickname
                    it?.let {
                        Toast.makeText(
                            applicationContext,
                            "CustomData: ${it.customData}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    activity.binding.customDataValue.text = it?.customData
                }
            }
        }
    }

}