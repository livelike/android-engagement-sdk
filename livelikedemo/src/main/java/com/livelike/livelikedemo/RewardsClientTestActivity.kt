package com.livelike.livelikedemo

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.livelike.common.profile
import com.livelike.common.user
import com.livelike.common.utils.validateUuid
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.core.data.models.RewardAttribute
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.digitalGoods
import com.livelike.engagementsdk.gamification.ApplicationRewardItemsRequestParams
import com.livelike.engagementsdk.gamification.GetRedemptionKeyRequestOptions
import com.livelike.engagementsdk.gamification.IRewardsClient
import com.livelike.engagementsdk.gamification.LiveLikeDigitalGoodsClient
import com.livelike.engagementsdk.gamification.RedemptionKey
import com.livelike.engagementsdk.gamification.RedemptionKeyStatus
import com.livelike.engagementsdk.gamification.RewardEventsListener
import com.livelike.engagementsdk.gamification.RewardItemTransferRequestParams
import com.livelike.engagementsdk.gamification.RewardItemTransferType
import com.livelike.engagementsdk.gamification.RewardTransaction
import com.livelike.engagementsdk.gamification.RewardTransactionsRequestParameters
import com.livelike.engagementsdk.gamification.TransferRewardItem
import com.livelike.engagementsdk.rewards
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.livelikedemo.databinding.RewardIsClientTestActivityBinding
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId

class RewardsClientTestActivity : AppCompatActivity() {

    private lateinit var rewardsClient: IRewardsClient
    private lateinit var reedemptionClient: LiveLikeDigitalGoodsClient
    private lateinit var binding: RewardIsClientTestActivityBinding
    var rewardIems: List<RewardItem>? = null
    var selectedrewardItem: RewardItem? = null

    val rewardItemBalanceMap: MutableMap<String, Int> = mutableMapOf()

    var redemptionOptions: GetRedemptionKeyRequestOptions? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = RewardIsClientTestActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        rewardsClient = (applicationContext as LiveLikeApplication).sdk.rewards()
        reedemptionClient = (applicationContext as LiveLikeApplication).sdk.digitalGoods()


        (applicationContext as LiveLikeApplication).sdk.profile().profileStream.latest()?.let {
            binding.profileIdA.setText(it.userId)
            binding.accessToken.setText(it.accessToken)
        }

        rewardsClient.getApplicationRewardItems(LiveLikePagination.FIRST) { result, error ->
            result?.let { rewardItems ->
                fetchRewardItemBalances(rewardItems.map { it.id })
                rewardIems = rewardItems
            }
            showToast(error)
        }

        rewardsClient.rewardEventsListener = object : RewardEventsListener() {
            override fun onReceiveNewRewardItemTransfer(rewardItemTransfer: TransferRewardItem) {
                runOnUiThread { showAllRewardsTransfers() }
            }
        }

        binding.showReward.setOnClickListener {
            selectedrewardItem?.let {
                AlertDialog.Builder(this).setTitle("${it.name}: attributes")
                    .setItems(it.attributes?.map { entry ->
                        "key: ${entry.key}, value : ${entry.value}"
                    }?.toTypedArray()) { _, _ -> }.create().show()
            }
        }

        binding.imagesButton.setOnClickListener {
            selectedrewardItem?.let {
                AlertDialog.Builder(this).setTitle("${it.name}: images")
                    .setItems(it.images?.map { imageData ->
                        "name: ${imageData.name}\nurl: ${imageData.imageUrl}\nmime Type: ${imageData.mimetype}"
                    }?.toTypedArray()) { _, _ -> }.create().show()
            }
        }

        binding.filterSpinner.adapter = ArrayAdapter(this,
            android.R.layout.simple_spinner_item,
            RedemptionKeyStatus.values().map { it.name }.toMutableList().apply { add("None") })
        binding.filterSpinner.setSelection(RedemptionKeyStatus.values().size)
        binding.filterSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?, view: View?, position: Int, id: Long
            ) {
                redemptionOptions = if (position >= RedemptionKeyStatus.values().size) {
                    null
                } else {
                    GetRedemptionKeyRequestOptions(RedemptionKeyStatus.values()[position],isAssigned = true)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }
    }

    private fun fetchRewardItemBalances(ids: List<String>) {
        rewardsClient.getRewardItemBalances(LiveLikePagination.FIRST, ids) { result, error ->
            result?.let { result ->
                for (it in result) {
                    rewardItemBalanceMap[it.rewardItemId] = it.rewardItemBalance
                }
                runOnUiThread {
                    initUI()
                }
            }
            showToast(error)
        }
    }

    override fun onDestroy() {
        rewardsClient.rewardEventsListener = null
        super.onDestroy()
    }

    private fun initUI() {
        binding.progressBar.visibility = View.GONE
        binding.receipentProfileId.setText("26722d0d-c6db-417f-8395-eacb1afb019f")

        rewardIems?.let { rewardIems ->
            val adapter =
                ArrayAdapter(this, android.R.layout.simple_spinner_item, rewardIems.map { it.name })
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.rewardItemSpinnner.adapter = adapter

            binding.rewardItemSpinnner.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?, view: View?, position: Int, id: Long
                    ) {
                        selectedrewardItem = rewardIems[position]
                        binding.rewardItemBalance.text =
                            "Balance : ${(rewardItemBalanceMap.get(selectedrewardItem?.id ?: "") ?: "0")}"

                        selectedrewardItem?.let {
                            binding.rewardItemIdA.setText(it.id)
                        }
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {
                    }

                }

            binding.sendBtn.setOnClickListener {
                if (selectedrewardItem == null) {
                    showToast("Please select reward item")
                } else {
                    if (!validateUuid(binding.receipentProfileId.text.toString())) {
                        showToast("Please Enter valid recipeint id")
                        return@setOnClickListener
                    }
                    if (binding.enterAmountEt.text.isEmpty()) {
                        showToast("Please Enter amount")
                        return@setOnClickListener
                    }
                    if (binding.enterAmountEt.text.isBlank() || binding.enterAmountEt.text.toString()
                            .toInt() > (rewardItemBalanceMap.get(
                            selectedrewardItem?.id ?: ""
                        ) ?: 0)
                    ) {
                        showToast("Please Enter amount less than or equal to available balance")
                        return@setOnClickListener
                    }

                    binding.progressBar.visibility = View.VISIBLE

                    rewardsClient.transferAmountToProfileId(
                        selectedrewardItem!!.id,
                        binding.enterAmountEt.text.toString().toInt(),
                        binding.receipentProfileId.text.toString()
                    ) { result, error ->
                        runOnUiThread {
                            binding.enterAmountEt.setText("")
                            binding.rewardItemBalance.text =
                                "Balance : ${(rewardItemBalanceMap.get(selectedrewardItem?.id ?: "") ?: "0")}"
                            binding.progressBar.visibility = View.GONE
                        }
                        if (error == null) {
                            showToast("amount sent successfully")
                        } else {
                            showToast(error)
                        }
                    }

                }
            }
        }

        binding.showAllRewardTransfers.setOnClickListener {
            showAllRewardsTransfers()
        }

        binding.filterByWidgetKind.setOnClickListener {

            val checked = mutableSetOf<String>()

            AlertDialog.Builder(this).setTitle("choose kinds").setMultiChoiceItems(
                WidgetType.values().map {
                    it.event.replace("-created", "").replace("-updated", "")
                }.toTypedArray(), BooleanArray(WidgetType.values().size)
            ) { _, which, isChecked ->
                if (isChecked) {
                    checked.add(WidgetType.values()[which].toString())
                } else {
                    checked.remove(WidgetType.values()[which].toString())
                }
            }.setPositiveButton("ok") { _, _ ->
                rewardsClient.getRewardTransactions(
                    LiveLikePagination.FIRST,
                    RewardTransactionsRequestParameters(widgetKindFilter = checked)
                ) { result, error ->
                    buildResultDialog(result, error)
                }
            }.create().show()
        }

        binding.buttonGetReward.setOnClickListener {

            if (!binding.rewardUuidText.text.isNullOrBlank() && !validateUuid(binding.rewardUuidText.text.toString())) {
                buildResultDialog(null, "enter a valid uuid")
                return@setOnClickListener
            }

            rewardsClient.getRewardTransactions(
                LiveLikePagination.FIRST,
                RewardTransactionsRequestParameters(widgetIds = setOf(binding.rewardUuidText.text.toString()))
            ) { result, error ->
                buildResultDialog(result, error)
            }
        }

        binding.buttonAttributeSearch.setOnClickListener {
            val key = binding.editTextAttributeKey.text.toString()
            val value = binding.editTextAttributeValue.text.toString()

            when {
                key.isBlank() -> AlertDialog.Builder(this).setTitle("error")
                    .setMessage("Key is null").create().show()

                value.isBlank() -> AlertDialog.Builder(this).setTitle("error")
                    .setMessage("Value is null").create().show()

                else -> rewardsClient.getApplicationRewardItems(
                    LiveLikePagination.FIRST,
                    ApplicationRewardItemsRequestParams(listOf(RewardAttribute(key, value)))
                ) { result, error ->
                    runOnUiThread {
                        AlertDialog.Builder(this@RewardsClientTestActivity)
                            .setTitle("matching reward items").setItems(
                                result?.map(RewardItem::toString)?.toTypedArray()
                            ) { _, _ -> }.create().show()
                    }
                }
            }
        }

        binding.showRedemption.setOnClickListener {
            reedemptionClient.getRedemptionKeys(
                LiveLikePagination.FIRST, redemptionOptions
            ) { result, error ->
                result?.let {
                    runOnUiThread {
                        AlertDialog.Builder(this@RewardsClientTestActivity)
                            .setTitle("codes assigned to user").setItems(
                                result.map(RedemptionKey::toString).toTypedArray()
                            ) { _, _ -> }.create().show()
                    }

                }
            }
        }

        fun callback(result: RedemptionKey?, error: String?) {
            result?.let {
                runOnUiThread {
                    AlertDialog.Builder(this@RewardsClientTestActivity)
                        .setTitle("redeem code result")
                        .setItems(arrayOf(result.toString())) { _, _ -> }.create().show()
                }
            }
            error?.let {
                runOnUiThread {
                    AlertDialog.Builder(this@RewardsClientTestActivity).setMessage(it).create()
                        .show()
                }
            }
        }

        binding.redeemCode.setOnClickListener {
            reedemptionClient.redeemKeyWithId(binding.redemptionCode.text.toString(), ::callback)
        }

        binding.redeemCode2.setOnClickListener {
            reedemptionClient.redeemKeyWithCode(binding.redemptionCode.text.toString(), ::callback)
        }

        binding.filterRewards.setOnClickListener {
            if (binding.profileIdB.text.toString() != "" && binding.profileIdA.text.toString() == "") {
                showToast("you cant set profile b without a profile a")
                return@setOnClickListener
            }

            if (binding.rewardItemIdB.text.toString() != "" && binding.rewardItemIdA.text.toString() == "") {
                showToast("you cant set reward item b without a reward item a")
                return@setOnClickListener
            }

            if (binding.rewardActionKeyB.text.toString() != "" && binding.rewardActionKeyA.text.toString() == "") {
                showToast("you cant set reward item b without a reward item a")
                return@setOnClickListener
            }

            if (binding.widgetIdB.text.toString() != "" && binding.widgetIdA.text.toString() == "") {
                showToast("you cant set widget id b without a widget id a")
                return@setOnClickListener
            }
            if (binding.widgetKindB.text.toString() != "" && binding.widgetIdA.text.toString() == "") {
                showToast("you cant set widget kind b without a widget kind a")
                return@setOnClickListener
            }

            if (binding.startDate.text.toString() != "") {
                try {
                    LocalDate.parse(binding.startDate.text.toString())
                        .atStartOfDay(ZoneId.systemDefault())
                } catch (e: Exception) {
                    showToast(e.message)
                    return@setOnClickListener
                }
            }

            if (binding.endDate.text.toString() != "") {
                try {
                    LocalDate.parse(binding.endDate.text.toString())
                        .atStartOfDay(ZoneId.systemDefault())
                } catch (e: Exception) {
                    showToast(e.message)
                    return@setOnClickListener
                }
            }

            rewardsClient.getRewardTransactions(
                LiveLikePagination.FIRST,
                RewardTransactionsRequestParameters(
                    profileIds = if (binding.profileIdB.text.toString() == "") {
                        if (binding.profileIdA.text.toString() != "") {
                            listOf(binding.profileIdA.text.toString())
                        } else {
                            emptyList()
                        }
                    } else {
                        listOf(
                            binding.profileIdA.text.toString(), binding.profileIdB.text.toString()
                        )
                    }, rewardItemIds = if (binding.rewardItemIdA.text.toString() == "") {
                        if (binding.rewardItemIdA.text.toString() != "") {
                            listOf(binding.rewardItemIdA.text.toString())
                        } else {
                            emptyList()
                        }
                    } else {
                        listOf(
                            binding.rewardItemIdA.text.toString(),
                            binding.rewardItemIdB.text.toString()
                        )
                    }, rewardActionKeys = if (binding.rewardActionKeyB.text.toString() == "") {
                        if (binding.rewardActionKeyA.text.toString() != "") {
                            listOf(binding.rewardActionKeyA.text.toString())
                        } else {
                            emptyList()
                        }
                    } else {
                        listOf(
                            binding.rewardActionKeyA.text.toString(),
                            binding.rewardActionKeyB.text.toString()
                        )
                    },

                    widgetIds = if (binding.widgetIdB.text.toString() == "") {
                        if (binding.widgetIdA.text.toString() != "") {
                            setOf(binding.widgetIdA.text.toString())
                        } else {
                            setOf()
                        }
                    } else {
                        setOf(binding.widgetIdA.text.toString(), binding.widgetIdB.text.toString())
                    }, widgetKindFilter = if (binding.widgetKindB.text.toString() == "") {
                        if (binding.widgetKindA.text.toString() != "") {
                            listOf(binding.widgetKindA.text.toString())
                        } else {
                            listOf()
                        }
                    } else {
                        listOf(
                            binding.widgetKindA.text.toString(), binding.widgetKindB.text.toString()
                        )
                    }.map { name ->
//                        WidgetType.fromString(it)
                        WidgetType.values().first { it.getType() == name }.getType()
                    }.toSet(),

                    createdSince = try {
                        LocalDate.parse(binding.startDate.text.toString())
                            .atStartOfDay(ZoneId.systemDefault())
                    } catch (_: Exception) {
                        null
                    }, createdUntil = try {
                        LocalDate.parse(binding.endDate.text.toString())
                            .atStartOfDay(ZoneId.systemDefault())
                    } catch (_: Exception) {
                        null
                    }
                )
            ) { result, error ->
                buildResultDialog(result, error)
            }
        }

    }

    private fun buildResultDialog(result: List<RewardTransaction>?, error: String?) {
        error?.let {
            AlertDialog.Builder(this).setMessage(it).create().show()
        }

        result?.let {
            AlertDialog.Builder(this).setItems(it.map { rewardTransaction ->
                "reward name: ${rewardTransaction.rewardItemName}, amount : ${rewardTransaction.rewardItemAmount}widget Kind: ${rewardTransaction.widgetKind}"
            }?.toTypedArray()) { _, _ -> }.create().show()
        }
    }

    private fun showAllRewardsTransfers() {
        binding.progressBar.visibility = View.VISIBLE
        if (binding.transferTypeSelection.checkedRadioButtonId == -1) {
            rewardsClient.getRewardItemTransfers(LiveLikePagination.FIRST) { result, error ->
                onRewardTransferListResponse(result, error)
            }
        } else {
            val requestParams =
                if (binding.transferTypeSelection.checkedRadioButtonId == binding.sent.id) {
                    RewardItemTransferRequestParams(RewardItemTransferType.SENT)
                } else {
                    RewardItemTransferRequestParams(RewardItemTransferType.RECEIVED)
                }
            rewardsClient.getRewardItemTransfers(LiveLikePagination.FIRST, requestParams) { result, error ->
                onRewardTransferListResponse(result, error)
            }
        }
    }

    private fun onRewardTransferListResponse(
        result: List<TransferRewardItem>?, error: String?
    ) {
        runOnUiThread {
            binding.progressBar.visibility = View.GONE
            result?.let {
                val list = it.map { getRewardTransferString(it) }
                AlertDialog.Builder(this@RewardsClientTestActivity).apply {
                    setTitle("Rewards transfer list")
                    setItems(list.toTypedArray()) { _, _ ->
                    }
                    create()
                }.show()

            }
        }
        showToast(error)
    }

    private fun getRewardTransferString(transferRewardItem: TransferRewardItem): String {
        val transferRewardItemRow = StringBuilder()
        val rewardItem = rewardIems?.find { transferRewardItem.rewardItemId == it.id }?.name ?: "NA"
        if (transferRewardItem.senderProfileId == ((applicationContext as LiveLikeApplication).sdk.profile().profileStream.latest()?.userId
                ?: "")
        ) {
            transferRewardItemRow.append("Sent ${transferRewardItem.rewardItemAmount} $rewardItem to ${transferRewardItem.recipientProfileId}")
        } else {
            transferRewardItemRow.append("Received ${transferRewardItem.rewardItemAmount} $rewardItem from ${transferRewardItem.senderProfileId}")
        }
        return transferRewardItemRow.toString()
    }

    private fun showToast(message: String?) {
        message?.let {
            runOnUiThread {
                AlertDialog.Builder(this).setMessage(it).create().show()
            }
        }
    }


}