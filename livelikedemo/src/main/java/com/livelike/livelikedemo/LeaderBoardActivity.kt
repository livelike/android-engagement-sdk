package com.livelike.livelikedemo

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.core.data.models.LeaderBoard
import com.livelike.engagementsdk.core.data.models.LeaderBoardEntry
import com.livelike.engagementsdk.leaderboard
import com.livelike.livelikedemo.databinding.ActivityLeaderBoardBinding
import com.livelike.livelikedemo.databinding.LayLeaderBoardListItemBinding
import com.livelike.livelikedemo.utils.DialogUtils

class LeaderBoardActivity : AppCompatActivity() {
    val adapter =
        LeaderBoardEntriesAdapter(object : RecyclerViewItemClickListener<LeaderBoardEntry> {
            override fun itemClick(item: LeaderBoardEntry) {
                leaderBoardId?.let {
                    dialog?.show()
                    (application as LiveLikeApplication).sdk.leaderboard()
                        .getLeaderBoardEntryForProfile(
                            leaderBoardId!!,
                            item.profileId
                        ) { result, error ->
                            dialog?.dismiss()
                            result?.let {
                                showData(it)
                            }
                            error?.let {
                                showToast(error)
                            }
                        }
                }
            }
        })
    var dialog: Dialog? = null

    private fun showData(result: LeaderBoardEntry) {
        if (dialog?.isShowing == true) {
            dialog?.dismiss()
        }
        AlertDialog.Builder(this@LeaderBoardActivity).apply {
            setTitle("Profile")
            setItems(
                arrayOf(
                    "NickName: ${result.profile.nickname}",
                    "Id: ${result.profile.id}",
                    "Percentile Rank: ${result.percentileRank}",
                    "Rank: ${result.rank}",
                    "Score: ${result.score}",
                    "Custom Data: ${result.profile.customData}"
                )
            ) { _, _ ->
            }
            create()
        }.show()
    }

    private lateinit var binding: ActivityLeaderBoardBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLeaderBoardBinding.inflate(layoutInflater)
        setContentView(binding.root)
        dialog = DialogUtils.createProgressDialog(
            this,
            "",
            "Loading. Please wait...",
            true
        )
        dialog?.show()
        binding.rcylLeaderBoard.layoutManager =
            LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false
            )
        binding.rcylLeaderBoardEntries.layoutManager =
            LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false
            )
//        ed_txt_program_id.setText("47c14e1d-5786-401e-a850-22c5a91a5399") //QA
        binding.edTxtProgramId.setText("6834f1fd-f24d-4538-ba51-63544f9d78eb") // Prod

        binding.rcylLeaderBoardEntries.adapter = adapter
        binding.btnFetch.setOnClickListener {
            val programId = binding.edTxtProgramId.text.toString()
            if (programId.isNotEmpty()) {
                binding.prgFetchLeaderBoards.visibility = View.VISIBLE
                (application as LiveLikeApplication).sdk.leaderboard().getLeaderBoardsForProgram(
                    programId
                ) { result, error ->
                    binding.prgFetchLeaderBoards.visibility = View.INVISIBLE
                    result?.let {
                        binding.rcylLeaderBoard.adapter = LeaderBoardAdapter(
                            result,
                            object : RecyclerViewItemClickListener<LeaderBoard> {
                                override fun itemClick(item: LeaderBoard) {
                                    leaderBoardId = item.id
                                    loadEntries(LiveLikePagination.FIRST)
                                }
                            }
                        )
                    }
                    error?.let {
                        showToast(error)
                    }
                }
            }
        }
        binding.btnPrevious.setOnClickListener {
            loadEntries(LiveLikePagination.PREVIOUS)
        }
        binding.btnNext.setOnClickListener {
            loadEntries(LiveLikePagination.NEXT)
        }
        binding.btnFirst.setOnClickListener {
            loadEntries(LiveLikePagination.FIRST)
        }
        binding.btnCurrentUser.setOnClickListener {
            leaderBoardId?.let { id ->
                dialog?.show()
                (application as LiveLikeApplication).sdk.leaderboard()
                    .getLeaderBoardEntryForCurrentUserProfile(
                        id
                    ) { result, error ->
                        dialog?.dismiss()
                        result?.let {
                            showData(it)
                        }
                        error?.let {
                            showToast(it)
                        }
                    }
            }
        }
        binding.btnSortDown.setOnClickListener {
            adapter.sortList(false)
        }
        binding.btnSortUp.setOnClickListener {
            adapter.sortList(true)
        }
        binding.prgLeaderboardEntries.visibility = View.INVISIBLE
        dialog?.dismiss()
    }

    private var leaderBoardId: String? = null

    private fun loadEntries(pagination: LiveLikePagination) {
        leaderBoardId?.let {
            binding.prgLeaderboardEntries.visibility = View.VISIBLE
            (application as LiveLikeApplication).sdk.leaderboard().getEntriesForLeaderBoard(
                leaderBoardId!!, pagination
            ) { result, error ->
                binding.prgLeaderboardEntries.visibility = View.INVISIBLE
                result?.let {
                    result.let {
                        if (it.isNotEmpty())
                            adapter.list.clear()
                        adapter.list.addAll(it)
                    }
                    binding.txtCount.text = "Count: ${result.size}"
                }
                adapter.notifyDataSetChanged()
                error?.let {
                    showToast(error)
                }
            }
        }
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}

class LeaderBoardAdapter(
    private val list: List<LeaderBoard>,
    private val clickListener: RecyclerViewItemClickListener<LeaderBoard>
) :
    RecyclerView.Adapter<LeaderBoardViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): LeaderBoardViewHolder {


        val itemBinding = LayLeaderBoardListItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return LeaderBoardViewHolder(itemBinding)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(p0: LeaderBoardViewHolder, p1: Int) {
        val leaderBoard = list[p1]
        p0.itemBinding.txtLeaderboardName.text = leaderBoard.name
        p0.itemBinding.textView5.text = "Reward"
        p0.itemBinding.txtRewardItemName.text = leaderBoard.rewardItem.name
        p0.itemBinding.layLeaderBoardItem.setOnClickListener {
            clickListener.itemClick(leaderBoard)
        }
    }
}

interface RecyclerViewItemClickListener<T> {
    fun itemClick(item: T)
}

class LeaderBoardViewHolder(var itemBinding: LayLeaderBoardListItemBinding) :
    RecyclerView.ViewHolder(itemBinding.root)

class LeaderBoardEntriesAdapter(private val recyclerViewItemClickListener: RecyclerViewItemClickListener<LeaderBoardEntry>) :
    RecyclerView.Adapter<LeaderBoardViewHolder>() {
    val list: ArrayList<LeaderBoardEntry> = arrayListOf()
    private var isAscending = true
    fun sortList(isAsc: Boolean) {
        if (isAsc != isAscending) {
            isAscending = !isAscending
            list.reverse()
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): LeaderBoardViewHolder {

        val itemBinding = LayLeaderBoardListItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return LeaderBoardViewHolder(itemBinding)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(p0: LeaderBoardViewHolder, p1: Int) {
        val leaderBoard = list[p1]
        p0.itemBinding.txtLeaderboardName.text = leaderBoard.profileNickname
        p0.itemBinding.textView5.text = "Rank"
        p0.itemBinding.txtRewardItemName.text = leaderBoard.rank.toString()
        p0.itemBinding.layLeaderBoardItem.setOnClickListener {
            recyclerViewItemClickListener.itemClick(leaderBoard)
        }
    }
}
