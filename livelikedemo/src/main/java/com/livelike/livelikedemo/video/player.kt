package com.livelike.livelikedemo.video

import android.content.Context
import android.net.Uri
import android.os.Handler
import android.os.Looper
import androidx.media3.common.MediaItem
import androidx.media3.common.Player
import androidx.media3.datasource.DefaultDataSourceFactory
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.exoplayer.hls.HlsMediaSource
import androidx.media3.exoplayer.source.DefaultMediaSourceFactory
import androidx.media3.exoplayer.source.MediaSource
import androidx.media3.ui.PlayerView
import com.livelike.livelikepreintegrators.PlayerProvider
import com.livelike.livelikepreintegrators.getExoplayerPdtTime

data class PlayerState(
    var window: Int = 0,
    var position: Long = 0,
    var whenReady: Boolean = true
)

class ExoPlayerImpl(private val context: Context, private val playerView: PlayerView) :
    VideoPlayer {

    /*private var player: SimpleExoPlayer? =
        ExoPlayerFactory.newSimpleInstance(context, DefaultTrackSelector())
            .also { playerView.player = it }*/
    private var player: ExoPlayer? = ExoPlayer.Builder(context).build().also { playerView.player = it }
//    private var mediaSource: MediaSource = buildMediaSource(Uri.EMPTY)
    private var playerState = PlayerState()

    /** initialization of exoplayer with provided media source */
    private fun initializePlayer(uri: Uri, state: PlayerState, useHls: Boolean = true) {
        playerView.requestFocus()

        /** here exoplayer instance was getting created each time, so a check has been added if the instance of player
         has not been created before then only instantiation is needed else not */
        if (player == null) {
            player = ExoPlayer.Builder(context)
                .setMediaSourceFactory(DefaultMediaSourceFactory(context))
                .build().also { playerView.player = it }
        }

//        mediaSource = if (useHls) buildHLSMediaSource(uri) else buildMediaSource(uri)
        playerState = state
        val mediaItem =
            MediaItem.Builder()
                .setUri(uri)
                .setLiveConfiguration(
                    MediaItem.LiveConfiguration.Builder().setMaxPlaybackSpeed(1.02f).build()
                )
                .build()
//        player?.setMediaSource(mediaSource)
        player?.setMediaItem(mediaItem)
        player?.prepare()
        // player?.prepare(mediaSource)
        with(playerState) {
            player?.playWhenReady = whenReady
            player?.seekToDefaultPosition()
            player?.repeatMode = Player.REPEAT_MODE_ALL
        }
    }

    var pdt = 0L
    override fun getPDT(): Long {
        Handler(Looper.getMainLooper()).post {
            // things to do on the main thread
            pdt = getExoplayerPdtTime(object : PlayerProvider {
                override fun get(): ExoPlayer? {
                    return player
                }
            })
        }
        /*return getExoplayerPdtTime(object : PlayerProvider {
            override fun get(): SimpleExoPlayer? {
                return player
            }
        })*/
        return pdt
    }

    override fun playMedia(uri: Uri, startState: PlayerState) {
        initializePlayer(uri, startState)
    }

    /** responsible for starting the player, with the media source provided */
    override fun start() {
//        player?.setMediaSource(mediaSource)
        player?.prepare()
        // player?.prepare(mediaSource)
        player?.playWhenReady = true
        player?.seekToDefaultPosition()
    }

    /** responsible for stopping the player */
    override fun stop() {
        with(playerState) {
            position = player?.currentPosition ?: 0
            window = player?.currentMediaItemIndex ?: 0
            whenReady = false
        }
        player?.playWhenReady = false
        player?.stop()
    }

    /** responsible for stopping the player and releasing it */
    override fun release() {
        pdt = 0
        player?.stop()
        player?.release()
        player?.setVideoSurfaceHolder(null)
        player = null
        playerState = PlayerState()
    }

    override fun position(): Long {
        return player?.currentPosition ?: 0
    }

    override fun seekTo(position: Long) {
        player?.seekTo(position)
    }
}

interface VideoPlayer {
    fun playMedia(uri: Uri, startState: PlayerState = PlayerState())
    fun start()
    fun stop()
    fun seekTo(position: Long)
    fun release()
    fun position(): Long
    fun getPDT(): Long
}
