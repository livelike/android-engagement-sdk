package com.livelike.livelikedemo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.livelike.livelikedemo.databinding.ActivityWidgetBinding

class WidgetActivity : AppCompatActivity() {

//    private var mainViewModel: com.livelike.livelikedemo.viewmodels.WidgetViewModel? = null
    private lateinit var binding: ActivityWidgetBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = ActivityWidgetBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // to get intent received from MainActivity

        // This will create an instance of Engagement viewmodel which can be used to creating session and initialization
//        mainViewModel = ViewModelProvider(
//            this,
//            EngagementViewModelFactory(this.application)
//        )[com.livelike.livelikedemo.viewmodels.WidgetViewModel::class.java]
//        // Check whether chat or widget is selected
//         mainViewModel!!.getSession()
//            .let { binding.widgetView.setSession(it) } //TODO [CAF] consider the safety of !!
        // Example of Widget Interceptor showing a dialog
//        val interceptor = object : WidgetInterceptor() {
//            override fun widgetWantsToShow(widgetData: LiveLikeWidgetEntity) {
//                AlertDialog.Builder(this@WidgetActivity).apply {
//                    setMessage("You received a Widget, what do you want to do?")
//                    setPositiveButton("Show") { _, _ ->
//                        showWidget() // Releases the widget
//                    }
//                    setNegativeButton("Dismiss") { _, _ ->
//                        dismissWidget() // Discards the widget
//                    }
//                    create()
//                }.show()
//            }
//        }

        // You just need to add it on your session instance
        // mainViewModel?.getSession()?.widgetInterceptor = interceptor
    }

    override fun onPause() {
        super.onPause()
//        mainViewModel?.pauseSession()
    }

    override fun onResume() {
        super.onResume()
//        mainViewModel?.resumeSession()
    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//        mainViewModel?.closeSession()
//    }
}
