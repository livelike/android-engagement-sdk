package com.livelike.livelikedemo.newapp.ui.chat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.livelike.livelikedemo.R
import com.livelike.livelikedemo.channel.Channel
import com.livelike.livelikedemo.databinding.NewappFragmentChatBinding
import com.livelike.livelikedemo.newapp.ui.main.HomeViewModel

class ChatFragment : Fragment() {


    private lateinit var binding: NewappFragmentChatBinding

    private val homeViewModel: HomeViewModel by activityViewModels { HomeViewModel.factory }
    private val viewModel: ChatViewModel by activityViewModels {
        //Log.d("green", ":homeViewModel.engagementSDKFlow.value : ${homeViewModel.engagementSDKFlow.value} ")
        ChatViewModel.createFactory(
            homeViewModel.engagementSDKFlow.value!!
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = NewappFragmentChatBinding.inflate(inflater, container, false)
        return binding.also {
            it.lifecycleOwner = viewLifecycleOwner
            it.viewModel = viewModel
        }.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val navController = findNavController()

        navController.previousBackStackEntry?.savedStateHandle?.getLiveData<Channel>("channel")
            ?.observe(viewLifecycleOwner) { channel ->
                viewModel.programId.value = channel.llProgram
            }

        binding.btnProgramSelect.setOnClickListener {
            navController.navigate(R.id.action_chatFragment_to_selectDialogFragment)
        }

        binding.btEnterChat.setOnClickListener {}

        binding.btEnterCustomChat.setOnClickListener {
            navController.navigate(R.id.action_chatFragment_to_customChatFragment)
        }

    }

    override fun onResume() {
        super.onResume()
        val args = this.arguments
        val program: String = args?.get("channel").toString()
        viewModel.programId.value = program
        viewModel.isContentSession.value = true
    }


}