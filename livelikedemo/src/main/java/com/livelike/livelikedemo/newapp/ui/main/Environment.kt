package com.livelike.livelikedemo.newapp.ui.main

enum class Environment(val clientId: String, val configUrl: String, val testConfigUrl: String) {
    STAGING(
        "vLgjH7dF0uX4J4FQJK3ncMkVmsCdLWhJ0qPtsbk7",
        "https://cf-blast-staging.livelikecdn.com",
        "https://cf-blast-staging.livelikecdn.com/api/v1/programs/?client_id=vLgjH7dF0uX4J4FQJK3ncMkVmsCdLWhJ0qPtsbk7"
    ),
    PRODUCTION(
        "8PqSNDgIVHnXuJuGte1HdvOjOqhCFE1ZCR3qhqaS",
        "https://cf-blast.livelikecdn.com",
        "https://cf-blast.livelikecdn.com/api/v1/programs/?client_id=8PqSNDgIVHnXuJuGte1HdvOjOqhCFE1ZCR3qhqaS"
    ),
    QA(
        "pnODbVXg0UI80s0l2aH5Y7FOuGbftoAdSNqpdvo6",
        "https://cf-blast-qa.livelikecdn.com",
        "https://cf-blast-qa.livelikecdn.com/api/v1/programs/?client_id=pnODbVXg0UI80s0l2aH5Y7FOuGbftoAdSNqpdvo6"
    ),
    QA_ICONIC(
        "LT9lUmrzSqXvAL66rSWSGK0weclpFNbHANUTxW9O",
        "https://cf-blast-iconic.livelikecdn.com",
        "https://cf-blast-iconic.livelikecdn.com/api/v1/programs/?client_id=LT9lUmrzSqXvAL66rSWSGK0weclpFNbHANUTxW9O"
    ),
    QA_DIG(
        "lom9db0XtQUhOZQq1vz8QPfSpiyyxppiUVGMcAje",
        "https://cf-blast-dig.livelikecdn.com",
        "https://cf-blast-dig.livelikecdn.com/api/v1/programs/?client_id=lom9db0XtQUhOZQq1vz8QPfSpiyyxppiUVGMcAje"
    ),
    QA_GAME_CHANGERS(
        "GaEBcpVrCxiJOSNu4bvX6krEaguxHR9Hlp63tK6L",
        "https://cf-blast-game-changers.livelikecdn.com",
        "https://cf-blast-game-changers.livelikecdn.com/api/v1/programs/?client_id=GaEBcpVrCxiJOSNu4bvX6krEaguxHR9Hlp63tK6L"
    )
}