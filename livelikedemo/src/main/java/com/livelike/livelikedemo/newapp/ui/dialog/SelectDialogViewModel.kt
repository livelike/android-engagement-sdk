package com.livelike.livelikedemo.newapp.ui.dialog

import android.os.Build
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.livelike.engagementsdk.BuildConfig
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.livelikedemo.LiveLikeApplication
import com.livelike.livelikedemo.channel.Channel
import com.livelike.livelikedemo.newapp.ui.main.Environment
import com.livelike.network.NetworkApiClient
import com.livelike.network.NetworkClientConfig
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.net.URL

class SelectDialogViewModel(
    private val sdk: EngagementSDK,
    private val environment: Environment,
    application: LiveLikeApplication
) : AndroidViewModel(application) {

    val programListFlow = MutableStateFlow<List<Channel>>(emptyList())

    val showLoading =
        programListFlow.map { it.isEmpty() }.stateIn(viewModelScope, SharingStarted.Eagerly, false)

    private val networkApiClient = NetworkApiClient.getInstance(
        NetworkClientConfig("Android/${BuildConfig.SDK_VERSION} ${Build.MODEL}/${Build.VERSION.SDK_INT}"),
        Dispatchers.IO
    )

    init {
        loadData(null)
    }

    private fun loadData(url: String?) {
        viewModelScope.launch {
            val result = networkApiClient.get(url ?: environment.testConfigUrl)
            if (result is com.livelike.network.NetworkResult.Success) {
                val json = JSONObject(result.data)
                val results = json.getJSONArray("results")
                val nextUrl = json.getString("next")
                val channelList = arrayListOf<Channel>()
                for (i in 0 until results.length()) {
                    val channel = getChannelFor(results.getJSONObject(i))
                    channel.let {
                        channelList.add(channel)
                    }
                }
                programListFlow.value = programListFlow.value.plus(channelList)
                if (nextUrl != "null")
                    loadData(nextUrl)
            }
        }
    }

    private fun getChannelFor(channelData: JSONObject): Channel {
        val streamUrl = channelData.getString("stream_url")
        val url: URL? = when {
            streamUrl.isNullOrEmpty() || streamUrl.equals("null") -> null
            else -> URL(streamUrl)
        }
        return Channel(
            channelData.getString("title"),
            url,
            null,
            channelData.getString("id")
        )
    }

    companion object {
        fun createFactory(sdk: EngagementSDK, environment: Environment): ViewModelProvider.Factory =
            viewModelFactory {
                initializer {
                    SelectDialogViewModel(
                        sdk, environment,
                        this[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY] as LiveLikeApplication
                    )
                }
            }
    }
}

