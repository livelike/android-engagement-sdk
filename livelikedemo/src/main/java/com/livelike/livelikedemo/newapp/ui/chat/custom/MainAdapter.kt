package com.livelike.livelikedemo.newapp.ui.chat.custom

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.text.Spannable
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.JsonParser
import com.livelike.engagementsdk.chat.data.remote.PinMessageInfo
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage
import com.livelike.livelikedemo.LiveLikeApplication
import com.livelike.livelikedemo.databinding.NewappCustomChatItemBinding
import java.text.SimpleDateFormat
import java.util.*

class MainAdapter(
    private val isPin: Boolean,
    private val isLink: Boolean,
    private val isQuote: Boolean,
    private val pinMessageClick: (LiveLikeChatMessage) -> Unit,
    private val unPinMessageClick: (String) -> Unit,
) : RecyclerView.Adapter<CustomViewHolder>() {

    val chatList = arrayListOf<LiveLikeChatMessage>()
    var pinnedList = arrayListOf<PinMessageInfo>()
    private var linksRegex = Patterns.WEB_URL.toRegex()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val itemBinding =
            NewappCustomChatItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CustomViewHolder(itemBinding)
    }

    override fun getItemCount(): Int = chatList.size

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val chatMessage = chatList[position]
        holder.itemBinding.normalMessage.visibility = View.VISIBLE
        holder.itemBinding.customMessages.visibility = View.GONE
        holder.itemBinding.customTv.visibility = View.GONE

        if (isQuote) {
            if (chatMessage.quoteMessage != null) {
                holder.itemBinding.layQuoteChatMsg.visibility = View.VISIBLE
                holder.itemBinding.txtNameQuote.text = chatMessage.quoteMessage?.nickname
                holder.itemBinding.txtMessageQuote.text = chatMessage.quoteMessage?.message
                if (chatMessage.quoteMessage?.imageUrl != null) {
                    holder.itemBinding.imgMessageQuote.visibility = View.VISIBLE
                    Glide.with(holder.itemBinding.imgMessageQuote.context)
                        .load(chatMessage.quoteMessage!!.imageUrl!!)
                        .apply(
                            RequestOptions().override(
                                chatMessage.quoteMessage!!.imageWidth!!,
                                chatMessage.quoteMessage!!.imageHeight!!
                            )
                        )
                        .into(holder.itemBinding.imgMessageQuote)

                } else {
                    holder.itemBinding.imgMessageQuote.visibility = View.GONE
                }
            } else {
                holder.itemBinding.layQuoteChatMsg.visibility = View.GONE
            }
        }

        holder.itemBinding.txtName.text = chatMessage.nickname
        val dateTime = Date()
        chatMessage.timestamp?.let {
            dateTime.time = it.toLong()
        }
        holder.itemBinding.imgPin.visibility = when {
            pinnedList.any { it.messageId == chatMessage.id } -> View.VISIBLE
            else -> View.INVISIBLE
        }


        //holder.itemView.normal_message.setBackgroundColor(Color.LTGRAY)
        when {
            pinnedList.any { it.messageId == chatMessage.id } -> holder.itemBinding.normalMessage.setBackgroundColor(
                Color.LTGRAY
            )
            else -> holder.itemBinding.normalMessage.setBackgroundColor(Color.parseColor("#45AAFF"))
        }


        if (chatMessage.imageUrl != null && chatMessage.imageWidth != null && chatMessage.imageHeight != null
        ) {
            holder.itemBinding.imgMessage.visibility = View.VISIBLE
            chatMessage.imageUrl?.let {
                Glide.with(holder.itemBinding.imgMessage.context)
                    .load(it)
                    .apply(
                        RequestOptions().override(
                            chatMessage.imageWidth!!,
                            chatMessage.imageHeight!!
                        )
                    )
                    .into(holder.itemBinding.imgMessage)
            }
            holder.itemBinding.txtMessage.text = ""
        } else {
            holder.itemBinding.imgMessage.visibility = View.GONE

            if (!chatMessage.message.isNullOrEmpty()) {
                if (isLink) {
                    holder.itemBinding.txtMessage.apply {
                        linksClickable = true
                        setLinkTextColor(Color.GREEN)
                        movementMethod = LinkMovementMethod.getInstance()
                    }
                    holder.itemBinding.txtMessageQuote.apply {
                        linksClickable = true
                        setLinkTextColor(Color.GREEN)
                        movementMethod = LinkMovementMethod.getInstance()
                    }


                    val linkText = getTextWithLinks(
                        linksRegex,
                        SpannableString(chatMessage.message)
                    )

                    if (chatMessage.quoteMessage?.message != null) {

                        val linkTextQuote = getTextWithLinks(
                            linksRegex,
                            SpannableString(chatMessage.quoteMessage?.message)
                        )
                        holder.itemBinding.txtMessageQuote.text = linkTextQuote
                    }


                    holder.itemBinding.txtMessage.text = linkText

                } else {
                    holder.itemBinding.txtMessage.text = chatMessage.message
                    holder.itemBinding.txtMessageQuote.text = chatMessage.quoteMessage?.message
                }


            } else if (!chatMessage.customData.isNullOrEmpty()) {
                val customData = chatMessage.customData

                holder.itemBinding.normalMessage.visibility = View.GONE
                holder.itemBinding.customMessages.visibility = View.VISIBLE
                try {
                    val jsonObject =
                        JsonParser.parseString(customData.toString()).asJsonObject

                    if (jsonObject.has("kind")) {
                        holder.itemBinding.widgetView.visibility = View.VISIBLE
                        holder.itemBinding.widgetView.displayWidget(
                            (holder.itemView.context.applicationContext as LiveLikeApplication).sdk,
                            jsonObject
                        )
                    } else {
                        holder.itemBinding.customTv.visibility = View.VISIBLE
                        holder.itemBinding.widgetView.visibility = View.GONE
                        holder.itemBinding.customTv.text = jsonObject.toString()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    holder.itemBinding.customTv.text = customData
                    holder.itemBinding.customTv.visibility = View.VISIBLE
                    holder.itemBinding.widgetView.visibility = View.GONE
                }
            }
        }

        holder.itemView.setOnClickListener {
            if (isPin) {
                val index = pinnedList.indexOfFirst { it.messageId == chatMessage.id }
                if (index != -1) {
                    unPinMessageClick.invoke(pinnedList[index].id)
                    // holder.itemView.normal_message.setBackgroundColor(Color.parseColor("#45AAFF"))
                } else {
                    pinMessageClick.invoke(chatMessage)
                    //holder.itemView.normal_message.setBackgroundColor(Color.LTGRAY)
                }
            } else {
                return@setOnClickListener
            }
        }

        holder.itemBinding.txtMsgTime.text = SimpleDateFormat(
            "MMM d, h:mm a",
            Locale.getDefault()
        ).format(dateTime)
    }

    private fun getTextWithLinks(
        linksRegex: Regex,
        spannableString: SpannableString
    ): SpannableString {

        val result = linksRegex.toPattern().matcher(spannableString)
        while (result.find()) {
            val start = result.start()
            val end = result.end()
            spannableString.setSpan(
                InternalURLSpans(
                    spannableString.subSequence(start, end).toString()
                ),
                start,
                end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }

        return spannableString
    }

    class InternalURLSpans(private var clickedSpan: String) : ClickableSpan() {
        override fun onClick(textView: View) {
            val i = Intent(Intent.ACTION_VIEW)
            if (!clickedSpan.startsWith("http://") && !clickedSpan.startsWith("https://"))
                clickedSpan = "http://$clickedSpan"
            i.data = Uri.parse(clickedSpan)
            try {
                textView.context.startActivity(i)
            } catch (e: ActivityNotFoundException) {
                e.printStackTrace()
            }
        }

    }

    fun getChatMessage(position: Int): LiveLikeChatMessage {
        val msg = chatList[position]
        return LiveLikeChatMessage(msg.message).apply {
            isDeleted = msg.isDeleted
            customData = msg.customData
            id = msg.id
            imageUrl = msg.imageUrl
            imageHeight = msg.imageHeight
            imageWidth = msg.imageWidth
            nickname = msg.nickname
            quoteMessage = msg.quoteMessage
            senderId = msg.senderId
            timestamp = msg.timestamp
            profilePic = msg.profilePic
        }
    }

}

class CustomViewHolder(var itemBinding: NewappCustomChatItemBinding) :
    RecyclerView.ViewHolder(itemBinding.root)