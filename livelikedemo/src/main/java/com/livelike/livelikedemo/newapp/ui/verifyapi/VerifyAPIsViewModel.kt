package com.livelike.livelikedemo.newapp.ui.verifyapi

import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.livelike.comment.LiveLikeCommentClient
import com.livelike.comment.comment
import com.livelike.comment.commentBoard
import com.livelike.comment.models.AddCommentReplyRequestOptions
import com.livelike.comment.models.AddCommentRequestOptions
import com.livelike.comment.models.CommentSortingOptions
import com.livelike.comment.models.CreateCommentBoardRequestOptions
import com.livelike.comment.models.DeleteCommentBoardRequestOptions
import com.livelike.comment.models.GetCommentRepliesRequestOptions
import com.livelike.comment.models.GetCommentRequestOptions
import com.livelike.comment.models.GetCommentsRequestOptions
import com.livelike.comment.models.UpdateCommentBoardRequestOptions
import com.livelike.common.profile
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.EpochTime
import com.livelike.engagementsdk.chat
import com.livelike.engagementsdk.chat.Visibility
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.livelikedemo.LiveLikeApplication
import kotlinx.coroutines.launch
import kotlin.coroutines.resume


class VerifyAPIsViewModel(private val sdk: EngagementSDK, application: LiveLikeApplication) :
    AndroidViewModel(application) {

    val adapter = VerifyAPIListAdapter()


    fun verifyAPIs() {
        adapter.addItem(VerifyAPIItem(0, "Chat Room APIs", true))
        val chatClient = sdk.chat()
        val commentBoardClient = sdk.commentBoard()
        val userClient = sdk.profile()

        viewModelScope.launch {
            adapter.addItem(VerifyAPIItem(8, "Current User Details", false))
            val currentUser = adapter.verifyAPI(8, { cont ->
                userClient.getCurrentUserDetails { result, error ->
                    result?.let {
                        cont.resume(it)
                    }
                    error?.let {
                        cont.cancel(Throwable(it))
                    }
                }
            }, { _ -> true })

            launch {
                adapter.addItem(VerifyAPIItem(9, "Update Nick Name", false))
            }
            launch {
                adapter.addItem(VerifyAPIItem(1, "Create Chat Room", false))
                adapter.addItem(VerifyAPIItem(2, "Update Chat Room", false))
                adapter.addItem(VerifyAPIItem(3, "Add Current User To Chat Room", false))
                adapter.addItem(VerifyAPIItem(4, "Get Chat Room Details", false))
                adapter.addItem(VerifyAPIItem(6, "Get Members of ChatRoom", false))
                adapter.addItem(VerifyAPIItem(7, "Delete Current User from Chat Room", false))
                adapter.addItem(VerifyAPIItem(5, "Get Current User Chat Room List", false))
                adapter.addItem(VerifyAPIItem(20, "Chat Session", true))
                adapter.addItem(VerifyAPIItem(21, "Chat Session connected to chat room", false))
                val chatRoom = adapter.verifyAPI(1,
                    { cont ->
                        chatClient.createChatRoom(title = "Test Chat Room",
                            visibility = Visibility.members,
                            liveLikeCallback = { result, error ->
                                result?.let {
                                    cont.resume(it)
                                }
                                error?.let {
                                    cont.cancel(Throwable(it))
                                }
                            })
                    },
                    { chatRoom -> chatRoom.title == "Test Chat Room" && chatRoom.visibility == Visibility.members && chatRoom.id.isNotEmpty() })
                if (chatRoom != null) {
                    val updatedChatRoom = adapter.verifyAPI(2,
                        { cont ->
                            chatClient.updateChatRoom(
                                chatRoom.id,
                                title = "Test Chat Room New",
                                visibility = Visibility.everyone
                            ) { result, error ->
                                result?.let { cont.resume(it) }
                                error?.let { cont.cancel(Throwable(it)) }
                            }
                        },
                        { updatedChatRoom -> updatedChatRoom.title == "Test Chat Room New" && updatedChatRoom.visibility == Visibility.everyone && chatRoom.id == updatedChatRoom.id })
                    if (updatedChatRoom != null) {
                        adapter.verifyAPI(3, { cont ->
                            Log.d(" fatal 1 ", " ${updatedChatRoom.id}")
                            chatClient.addCurrentProfileToChatRoom(
                                updatedChatRoom.id
                            ) { result, error ->
                                result?.let {
                                    cont.resume(it)
                                }
                                error?.let {
                                    Log.d(" fatal ", " $error")
                                    cont.cancel(Throwable(it))
                                }
                            }
                        }, { chatRoomMembership ->
                            chatRoomMembership.id.isEmpty()
                                .not() && chatRoomMembership.profile.id == currentUser?.id
                        })

                        adapter.verifyAPI(4,
                            { cont ->
                                chatClient.getChatRoom(chatRoom.id) { result, error ->
                                    result?.let { cont.resume(it) }
                                    error?.let { cont.cancel(Throwable(it)) }
                                }
                            },
                            { room -> room.title == updatedChatRoom.title && room.id == updatedChatRoom.id && room.visibility == updatedChatRoom.visibility })
                    }
                    adapter.verifyAPI(6,
                        { cont ->
                            chatClient.getMembersOfChatRoom(
                                chatRoom.id,
                                LiveLikePagination.FIRST
                            ) { result, error ->
                                result?.let { cont.resume(it) }
                                error?.let { cont.cancel(Throwable(it)) }
                            }
                        },
                        { chatRoomMembers -> chatRoomMembers.isNotEmpty() && chatRoomMembers.any { it.id == currentUser?.id } })

                    adapter.verifyAPI(7, { cont ->
                        chatClient.deleteCurrentProfileFromChatRoom(chatRoom.id) { result, error ->
                            result?.let { cont.resume(it) }
                            error?.let { cont.cancel(Throwable(it)) }
                        }
                    }, { _ -> true })

                    adapter.verifyAPI(5, { cont ->
                        chatClient.getCurrentProfileChatRoomList(LiveLikePagination.FIRST) { result, error ->
                            result?.let { cont.resume(it) }
                            error?.let { cont.cancel(Throwable(it)) }
                        }
                    }, { chatRoomList ->
                        if (chatRoomList.isNotEmpty()) {
                            for (r in chatRoomList) {
                                return@verifyAPI r.id.isNotEmpty()
                            }
                        }
                        return@verifyAPI true
                    })
                    val chatSession = sdk.createChatSession(timeCodeGetter = { EpochTime(0L) })
                    adapter.verifyAPI(21, { cont ->
                        chatSession.connectToChatRoom(chatRoom.id) { result, error ->
                            result?.let { cont.resume(true) }
                            error?.let { cont.resume(false) }
                        }
                    }, { check ->
                        check
                    })

                }
            }

            launch {
                adapter.addItem(VerifyAPIItem(10, "Comment APIs", true))
                adapter.addItem(VerifyAPIItem(11, "Create CommentBoard", false))
                val commentBoard = adapter.verifyAPI(11,
                    { cont ->
                        commentBoardClient.createCommentBoard(
                            CreateCommentBoardRequestOptions(
                                customId = java.util.UUID.randomUUID().toString(),
                                title = "Test",
                                allowComments = true,
                                replyDepth = 1,
                                customData = "Hello",
                                description = "Hello"
                            )
                        ) { result, error ->
                            result?.let {
                                cont.resume(it)
                            }
                            error?.let { cont.cancel(Throwable(it)) }
                        }
                    },
                    { commentBoard -> commentBoard.title == "Test" && commentBoard.customData == "Hello" })

                adapter.addItem(VerifyAPIItem(12, "Update CommentBoard", false))
                adapter.verifyAPI(
                    12,
                    { cont ->
                        commentBoardClient.updateCommentBoard(
                            UpdateCommentBoardRequestOptions(
                                commentBoardId = commentBoard?.id,
                                customId = java.util.UUID.randomUUID().toString(),
                                title = "Test10",
                                allowComments = true,
                                replyDepth = 1,
                                customData = "Hello1",
                                description = "Hello1"
                            )
                        ) { result, error ->
                            result?.let { cont.resume(it) }
                            error?.let { cont.cancel(Throwable(it)) }
                        }
                    },
                    { commentBoard -> commentBoard.title == "Test10" && commentBoard.customData == "Hello1" })

                adapter.addItem(VerifyAPIItem(13, "Getting a list of Comments Boards", false))
                adapter.verifyAPI(13, { cont ->
                    commentBoardClient.getCommentBoards(
                        LiveLikePagination.FIRST
                    ) { result, error ->
                        result?.let { cont.resume(it) }
                        error?.let { cont.cancel(Throwable(it)) }
                    }
                }, { commentBoardList -> commentBoardList.isNotEmpty() })

                val activeCommentSession: LiveLikeCommentClient?
                activeCommentSession = commentBoard?.id!!.let { sdk.comment(it) }

                adapter.addItem(VerifyAPIItem(15, "Creating a Comment", false))
                val currentComment = adapter.verifyAPI(15, { cont ->
                    activeCommentSession.addComment(
                        AddCommentRequestOptions(
                            text = "TestAutoHello",
                            customData = "Peace"
                        )
                    ) { result, error ->
                        result?.let { cont.resume(it) }
                        error?.let { cont.cancel(Throwable(it)) }
                    }
                }, { comment -> comment.text == "TestAutoHello" && comment.customData == "Peace" })

                adapter.addItem(VerifyAPIItem(16, "Reply to a Comment", false))
                adapter.verifyAPI(
                    16,
                    { cont ->
                        activeCommentSession.addCommentReply(
                            AddCommentReplyRequestOptions(
                                parentCommentId = currentComment?.id!!,
                                text = "TestAutoReply",
                                customData = "TestAutoReply"
                            )
                        ) { result, error ->
                            result?.let { cont.resume(it) }
                            error?.let { cont.cancel(Throwable(it)) }
                        }
                    },
                    { comment -> comment.text == "TestAutoReply" && comment.customData == "TestAutoReply" })

                adapter.addItem(VerifyAPIItem(17, "Get Comment", false))
                adapter.verifyAPI(17, { cont ->
                    activeCommentSession.getComment(
                        GetCommentRequestOptions(currentComment?.id!!)
                    ) { result, error ->
                        result?.let { cont.resume(it) }
                        error?.let { cont.cancel(Throwable(it)) }
                    }
                }, { comment -> comment.text == "TestAutoHello" && comment.customData == "Peace" })

                adapter.addItem(VerifyAPIItem(18, "Get a list of Top Level Comments", false))
                adapter.verifyAPI(18, { cont ->
                    activeCommentSession.getCommentsPage(
                        GetCommentsRequestOptions(
                            CommentSortingOptions.NEWEST,
                            repliedSince = ""
                        ),
                        LiveLikePagination.FIRST
                    ) { result, error ->
                        result?.let { cont.resume(it) }
                        error?.let { cont.cancel(Throwable(it)) }
                    }
                }, { commentList -> commentList.results.isNotEmpty()})

                adapter.addItem(VerifyAPIItem(19, "Get a list of Replies to a Comment", false))
                adapter.verifyAPI(19, { cont ->
                    activeCommentSession.getCommentReplies(
                        GetCommentRepliesRequestOptions(
                            commentId = currentComment?.id!!,
                            CommentSortingOptions.NEWEST
                        ),
                        LiveLikePagination.FIRST
                    ) { result, error ->
                        result?.let { cont.resume(it) }
                        error?.let { cont.cancel(Throwable(it)) }
                    }

                }, { commentList -> commentList.isNotEmpty() })

                adapter.addItem(VerifyAPIItem(21, "Deleting a Comment Board", false))
                adapter.verifyAPI(21, { cont ->
                    commentBoardClient.deleteCommentBoards(
                        DeleteCommentBoardRequestOptions(commentBoardId = commentBoard.id)
                    ) { result, error ->
                        result?.let { cont.resume(it) }
                        error?.let { cont.cancel(Throwable(it)) }
                    }
                }, { _ -> true })
            }

            launch {
                adapter.addItem(VerifyAPIItem(30, "Reactions APIs", true))
                adapter.addItem(VerifyAPIItem(31, "Create Reaction Space", true))

            }

        }

    }

    companion object {
        fun createFactory(sdk: EngagementSDK): ViewModelProvider.Factory = viewModelFactory {
            initializer {
                VerifyAPIsViewModel(
                    sdk,
                    this[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY] as LiveLikeApplication
                )
            }
        }
    }
}

data class VerifyAPIItem(val id: Int, val title: String, val isHeader: Boolean) {
    var isInProgress: Boolean = false
    var isVerified: Boolean = false
    var isFailed: Boolean = false
}