package com.livelike.livelikedemo.newapp.ui.verifyapi

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.livelike.livelikedemo.databinding.VerifyApiListHeaderBinding
import com.livelike.livelikedemo.databinding.VerifyApiListItemBinding
import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.suspendCancellableCoroutine

class VerifyAPIListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val list = arrayListOf<VerifyAPIItem>()

    fun addItem(item: VerifyAPIItem) {
        list.add(item)
        notifyItemInserted(list.size - 1)
    }

    override fun getItemViewType(position: Int): Int {
        val item = list[position]
        return if (item.isHeader) 1
        else 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            1 -> VerifyAPIListHeaderViewHolder(
                VerifyApiListHeaderBinding.inflate(
                    LayoutInflater.from(
                        parent.context
                    ), parent, false
                )
            )
            else -> VerifyAPIListItemViewHolder(
                VerifyApiListItemBinding.inflate(
                    LayoutInflater.from(
                        parent.context
                    ), parent, false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = list[position]
        when (holder) {
            is VerifyAPIListHeaderViewHolder -> {
                holder.binding.title = item.title
            }
            is VerifyAPIListItemViewHolder -> {
                holder.binding.item = item
            }
        }
    }

    override fun getItemCount(): Int = list.size

    private fun updateChange(
        id: Int,
        isInProgress: Boolean = false,
        isVerified: Boolean = false,
        isFailed: Boolean = false
    ) {
        val index = list.indexOfFirst { it.id == id }
        if (index > -1) {
            val d = list[index]
            d.isVerified = isVerified
            d.isFailed = isFailed
            d.isInProgress = isInProgress
            notifyItemChanged(index)
        }
    }

    suspend fun <T : Any> verifyAPI(
        id: Int,
        block: (CancellableContinuation<T>) -> Unit,
        verify: (T) -> Boolean
    ): T? {
        val index = list.indexOfFirst { it.id == id }
        if (index > -1) {
            try {
                updateChange(id, isInProgress = true)
                notifyItemChanged(index)
                val t = suspendCancellableCoroutine(block)
                if (verify(t)) {
                    updateChange(id, isVerified = true)
                    return t
                } else {
                    updateChange(id, isFailed = true)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                updateChange(id, isFailed = true)
            }
            notifyItemChanged(index)
        }
        return null
    }
}

class VerifyAPIListItemViewHolder(val binding: VerifyApiListItemBinding) :
    RecyclerView.ViewHolder(binding.root)

class VerifyAPIListHeaderViewHolder(val binding: VerifyApiListHeaderBinding) :
    RecyclerView.ViewHolder(binding.root)
