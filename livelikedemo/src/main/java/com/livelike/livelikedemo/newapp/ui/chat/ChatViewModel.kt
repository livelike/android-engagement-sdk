package com.livelike.livelikedemo.newapp.ui.chat

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.livelikedemo.LiveLikeApplication
import kotlinx.coroutines.flow.MutableStateFlow

class ChatViewModel(private val sdk: EngagementSDK, application: LiveLikeApplication) :
    AndroidViewModel(application) {


    val isContentSession = MutableStateFlow(false)
    val chatRoomId = MutableStateFlow<String?>(null)
    val programId = MutableStateFlow<String?>(null)
    val isAvatar = MutableStateFlow(false)
    val isQuote = MutableStateFlow(false)
    val isLink = MutableStateFlow(false)
    val isFiltered = MutableStateFlow(false)
    val isDiscard = MutableStateFlow(false)
    val isPin = MutableStateFlow(false)


    companion object {
        fun createFactory(sdk: EngagementSDK): ViewModelProvider.Factory =
            viewModelFactory {
                initializer {
                    ChatViewModel(
                        sdk,
                        this[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY] as LiveLikeApplication
                    )
                }
            }
    }
}



