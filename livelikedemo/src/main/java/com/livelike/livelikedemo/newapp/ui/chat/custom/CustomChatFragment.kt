package com.livelike.livelikedemo.newapp.ui.chat.custom

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.ItemTouchHelper
import com.livelike.livelikedemo.databinding.FragmentCustomChatBinding
import com.livelike.livelikedemo.newapp.ui.chat.ChatViewModel
import com.livelike.livelikedemo.newapp.ui.main.HomeViewModel


class CustomChatFragment : Fragment() {

    private lateinit var fragmentCustomChatBinding: FragmentCustomChatBinding
    private val homeViewModel: HomeViewModel by activityViewModels { HomeViewModel.factory }
    private val chatViewModel: ChatViewModel by activityViewModels {
        ChatViewModel.createFactory(homeViewModel.engagementSDKFlow.value!!)
    }
    private val viewModel: CustomChatViewModel by viewModels {
        CustomChatViewModel.createFactory(
            homeViewModel.engagementSDKFlow.value!!,
            chatViewModel.isPin.value,
            chatViewModel.chatRoomId.value,
            chatViewModel.isLink.value,
            chatViewModel.isQuote.value
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentCustomChatBinding = FragmentCustomChatBinding.inflate(inflater, container, false)
        return fragmentCustomChatBinding.also {
            it.lifecycleOwner = viewLifecycleOwner
            it.viewModel = viewModel
        }.root


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragmentCustomChatBinding.btPin.setOnClickListener {
            showListAlertDialog()
        }

        if(chatViewModel.isQuote.value) {
            val itemTouchHelper = ItemTouchHelper(viewModel.messageSwipeController)
            itemTouchHelper.attachToRecyclerView(fragmentCustomChatBinding.rcylChat)
            fragmentCustomChatBinding.imgQuoteRemove.setOnClickListener {
                viewModel.currentQuoteMessage.value = null
            }
        }

    }

    private fun showListAlertDialog() {
        val colorList = arrayOf("", "", "", "")
        for ((t, i) in viewModel.adapter.pinnedList.withIndex()) {
            colorList[t] = i.messagePayload.message ?: ""
        }

        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Pinned Messages")
        builder.setItems(colorList) { dialog, which ->
            Toast.makeText(requireContext(), colorList[which], Toast.LENGTH_LONG).show()
        }

        val dialog = builder.create()
        dialog.show()

    }

}

