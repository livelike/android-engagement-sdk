package com.livelike.livelikedemo.newapp.utils

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.recyclerview.widget.RecyclerView
import com.livelike.livelikedemo.newapp.ui.main.Environment

/**
 * fill the Spinner with all available projects.
 * Set the Spinner selection to selectedProject.
 * If the selection changes, call the InverseBindingAdapter
 */
@BindingAdapter(
    value = ["list", "selectedItem", "selectedItemAttrChanged"],
    requireAll = false
)
fun setSpinnerAdapter(
    spinner: Spinner,
    list: Array<Environment>,
    selectedItem: Environment,
    listener: InverseBindingListener
) {
    spinner.adapter = CommonSpinnerAdapter(
        spinner.context,
        android.R.layout.simple_spinner_dropdown_item,
        list.toList()
    )
    spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) =
            listener.onChange()

        override fun onNothingSelected(adapterView: AdapterView<*>) = listener.onChange()
    }

    for (index in 0 until spinner.adapter.count) {
        val currentItem = spinner.getItemAtPosition(index) as Environment
        if (currentItem == selectedItem) {
            spinner.setSelection(index)
        }
    }

}

/**
 * get the selected projectName and use it to return a
 * Project which is then used to set appEntry.value.project
 */
@InverseBindingAdapter(attribute = "selectedItem")
fun getSelectedProject(spinner: Spinner): Environment {
    return spinner.selectedItem as Environment
}

/**
 * Adapter for displaying the name-field of an Project in a Spinner
 */
class CommonSpinnerAdapter(
    context: Context,
    textViewResourceId: Int,
    private val values: List<Environment>
) : ArrayAdapter<Environment>(context, textViewResourceId, values) {
    override fun getCount() = values.size
    override fun getItem(position: Int) = values[position]
    override fun getItemId(position: Int) = position.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val label = super.getView(position, convertView, parent) as TextView
        label.text = values[position].name
        return label
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val label = super.getDropDownView(position, convertView, parent) as TextView
        label.text = values[position].name
        return label
    }
}

@BindingAdapter("android:visibility")
fun setVisibility(view: View, value: Boolean) {
    view.visibility = if (value) View.VISIBLE else View.GONE
}

@BindingAdapter("android:scrollTo")
fun scrollTo(recyclerView: RecyclerView, scrollToPosition: Int) {
    if (scrollToPosition < (recyclerView.adapter?.itemCount ?: 0)) {
        recyclerView.scrollToPosition(scrollToPosition)
    }
}