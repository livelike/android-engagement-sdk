package com.livelike.livelikedemo.newapp.ui.verifyapi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.livelike.livelikedemo.databinding.FragmentVerifyAPIsBinding
import com.livelike.livelikedemo.newapp.ui.main.HomeViewModel
import kotlinx.coroutines.launch

class VerifyAPIsFragment : Fragment() {

    companion object {
        fun newInstance() = VerifyAPIsFragment()
    }

    private val homeViewModel: HomeViewModel by activityViewModels { HomeViewModel.factory }
    private val viewModel: VerifyAPIsViewModel by viewModels {
        VerifyAPIsViewModel.createFactory(homeViewModel.engagementSDKFlow.value!!)
    }
    private lateinit var binding: FragmentVerifyAPIsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentVerifyAPIsBinding.inflate(inflater, container, false)
        binding.also {
            it.lifecycleOwner = viewLifecycleOwner
            it.viewModel = viewModel
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launchWhenResumed {
            viewModel.verifyAPIs()
        }
    }

}