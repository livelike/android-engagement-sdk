package com.livelike.livelikedemo.newapp.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.livelike.livelikedemo.R
import com.livelike.livelikedemo.databinding.NewappFragmentHomeBinding


class HomeFragment : Fragment() {

    companion object {
        fun newInstance() = HomeFragment()
    }

    private lateinit var binding: NewappFragmentHomeBinding

    private val viewModel: HomeViewModel by activityViewModels { HomeViewModel.factory }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = NewappFragmentHomeBinding.inflate(inflater, container, false)
        return binding.also {
            binding.lifecycleOwner = viewLifecycleOwner
            binding.viewModel = viewModel
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnChat.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_chatFragment)
        }

        binding.btnVerifyApi.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_verifyFragment)
        }
    }


}