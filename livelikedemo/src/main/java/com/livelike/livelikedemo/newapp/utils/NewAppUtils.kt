package com.livelike.livelikedemo.newapp.utils

fun getClientIdPrefKey(key: String): String = "ClientId-$key"
fun getAccessTokenPrefKey(key: String): String = "AccessToken-$key"

