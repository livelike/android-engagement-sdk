package com.livelike.livelikedemo.newapp.ui.dialog

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.livelike.livelikedemo.databinding.SelectorItemBinding

class SelectListAdapter<T>(
    private val list: MutableList<T> = mutableListOf(),
    private val itemClick: (T) -> Unit
) :
    RecyclerView.Adapter<SelectListAdapter<T>.SelectListViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectListViewHolder {
        return SelectListViewHolder(
            SelectorItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SelectListViewHolder, position: Int) {
        holder.binding.title = list[position].toString()
        holder.binding.root.setOnClickListener {
            itemClick.invoke(list[position])
        }
    }

    override fun getItemCount(): Int = list.size

    fun addItems(list: List<T>, predicate: (T, T) -> Boolean) {
        val lastIndex = this.list.size
        var count = 0
        for (item in list) {
            if (this.list.find { predicate.invoke(it, item) } == null) {
                this.list.add(item)
                count++
            }
        }
        if (count > 0) notifyItemRangeInserted(lastIndex, count)
    }

    inner class SelectListViewHolder(val binding: SelectorItemBinding) :
        RecyclerView.ViewHolder(binding.root)
}