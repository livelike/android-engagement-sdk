package com.livelike.livelikedemo.newapp.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.livelike.livelikedemo.R
import com.livelike.livelikedemo.channel.Channel
import com.livelike.livelikedemo.databinding.SelectDialogFragmentBinding
import com.livelike.livelikedemo.newapp.ui.main.HomeViewModel
import kotlinx.coroutines.launch

class SelectDialogFragment : DialogFragment() {

    private val homeViewModel: HomeViewModel by activityViewModels { HomeViewModel.factory }
    private val selectDialogViewModel: SelectDialogViewModel by viewModels {
        SelectDialogViewModel.createFactory(
            homeViewModel.engagementSDKFlow.value!!,
            homeViewModel.selectedEnvironment.value
        )
    }
    private lateinit var binding: SelectDialogFragmentBinding
    val adapter = SelectListAdapter<Channel> {
//        setFragmentResult("select dialog channel", bundleOf("channel" to it))
        //val action= SelectDialogFragmentDirections.actionSelectDialogFragmentToChatFragment(it.llProgram)
        findNavController().navigate(
            R.id.action_selectDialogFragment_to_chatFragment,
            bundleOf("channel" to it.llProgram)
           //action
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SelectDialogFragmentBinding.inflate(inflater, container, false)
        return binding.also {
            it.lifecycleOwner = this
            it.viewModel = selectDialogViewModel
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rcylSelectDialog.adapter = adapter
        lifecycleScope.launchWhenResumed {
            launch {
                selectDialogViewModel.programListFlow.collect {
                    adapter.addItems(it) { item1, item2 ->
                        item1.llProgram == item2.llProgram
                    }
                }
            }
        }
    }

}