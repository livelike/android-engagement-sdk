package com.livelike.livelikedemo.newapp

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.livelike.common.AccessTokenDelegate
import com.livelike.common.TimeCodeGetter
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.EpochTime
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.livelikedemo.MainActivity
import com.livelike.livelikedemo.databinding.ActivityHome2Binding
import com.livelike.livelikedemo.newapp.ui.main.HomeViewModel
import kotlinx.coroutines.launch

class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHome2Binding
    private val homeViewModel: HomeViewModel by viewModels { HomeViewModel.factory }
    private var accessToken: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHome2Binding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.fabOldApp.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

        lifecycleScope.launchWhenResumed {
            launch {
                homeViewModel.sdkErrorMessageFlow.collect {
                    Snackbar.make(binding.root, it, Snackbar.LENGTH_SHORT).show()
                }
            }
        }

        val sdk = EngagementSDK(
            "8PqSNDgIVHnXuJuGte1HdvOjOqhCFE1ZCR3qhqaS",
            applicationContext,
            accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return accessToken
                }

                override fun storeAccessToken(accessToken: String?) {
                    this@HomeActivity.accessToken = accessToken
                }
            }, errorDelegate = object : ErrorDelegate() {
                override fun onError(error: String) {
                    println("error = [${error}]")
                }

            })
        val session = sdk.createContentSession("09d93835-ee52-4757-976c-ea09d6a5798c")
        val chatSession = sdk.createChatSession(object : TimeCodeGetter {
            override fun invoke(): EpochTime {
                return EpochTime(0L)
            }
        })
    }
}