package com.livelike.livelikedemo.newapp.ui.chat.custom

import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.livelike.common.TimeCodeGetter
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.EpochTime
import com.livelike.engagementsdk.MessageListener
import com.livelike.engagementsdk.chat
import com.livelike.engagementsdk.chat.MessageSwipeController
import com.livelike.engagementsdk.chat.SwipeControllerActions
import com.livelike.engagementsdk.chat.data.remote.LiveLikeOrdering
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.chat.data.remote.PinMessageInfo
import com.livelike.engagementsdk.chat.stickerKeyboard.findImages
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage
import com.livelike.livelikedemo.LiveLikeApplication
import kotlinx.coroutines.flow.MutableStateFlow


class CustomChatViewModel(
    private val sdk: EngagementSDK,
    val isPin: Boolean,
    private val chatRoomId: String?,
    isLink: Boolean,
    isQuote: Boolean,
    application: LiveLikeApplication
) : AndroidViewModel(application) {

    val adapter = MainAdapter(isPin, isLink, isQuote = isQuote, pinMessageClick = { chatMessage ->
        sdk.chat().pinMessage(
            chatMessage.id!!,
            chatRoomId!!,
            chatMessage
        ) { result, error ->
            error?.let {

            }
            result?.let { messageInfo ->
            }
        }
    }) {
        sdk.chat().unPinMessage(it) { result, error ->
            error?.let {}
            result?.let {}
        }
    }

    var currentQuoteMessage = MutableStateFlow<LiveLikeChatMessage?>(null)


    val messageSwipeController =
        MessageSwipeController(application.applicationContext, object : SwipeControllerActions {
            override fun showReplyUI(position: Int) {
                currentQuoteMessage.value = adapter.getChatMessage(position)
                if (currentQuoteMessage.value != null && currentQuoteMessage.value!!.id == null) {
                    currentQuoteMessage.value = null
                    Toast.makeText(
                        application.applicationContext,
                        "Reply Not Allowed!! Try Again",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(application.applicationContext, "Send Reply", Toast.LENGTH_SHORT)
                        .show()
                }
                println("ChatFragment.showReplyUI>>${currentQuoteMessage.value}")
            }
        })

    val scrollToPosition = MutableStateFlow<Int>(0)
    val isRefreshing = MutableStateFlow(false)
    val textMessage = MutableStateFlow<String>("")
    internal var linksRegex = Patterns.WEB_URL.toRegex()
    val chatSession = sdk.createChatSession(object : TimeCodeGetter {
        override fun invoke(): EpochTime {
            return EpochTime(0)
        }
    })

    init {
        chatSession.setMessageListener(object : MessageListener {
            private val TAG = "LiveLike"
            override fun onNewMessage(message: LiveLikeChatMessage) {
                Log.i(TAG, "onNewMessage: $message")

                //adapter.chatList[adapter.itemCount - 1] = message
                val index =
                    adapter.chatList.indexOfFirst { it.clientMessageId == message.clientMessageId }
                if (index > -1) {
                    adapter.chatList[index] = message
                    adapter.notifyItemChanged(index)
                } else {
                    adapter.chatList.add(message)
                    adapter.notifyItemInserted(adapter.chatList.size - 1)
                    scrollToPosition.value = adapter.itemCount - 1

                }
            }

            override fun onHistoryMessage(messages: List<LiveLikeChatMessage>) {
                Log.d(TAG, "onHistoryMessage: ${messages.size}")
                messages.toMutableList().removeAll {
                    chatSession.getDeletedMessages().contains(it.id)
                }
                val empty = adapter.chatList.isEmpty()
                adapter.chatList.addAll(0, messages)
                adapter.notifyDataSetChanged()
                isRefreshing.value = false
                if (empty) {
                    scrollToPosition.value = adapter.itemCount - 1
                }
            }

            override fun onDeleteMessage(messageId: String) {
                Log.d(TAG, "onDeleteMessage: $messageId")
                val index = adapter.chatList.indexOfFirst { it.id == messageId }
                if (index > -1) {
                    adapter.chatList.removeAt(index)
                    adapter.notifyItemRemoved(index)
                }
            }

            override fun onPinMessage(message: PinMessageInfo) {
                //"Pinned: ${message.messageId}\n${message.pinnedById}\n${message.messagePayload?.message}"
                adapter.pinnedList.add(message)
                adapter.notifyDataSetChanged()
            }

            override fun onUnPinMessage(pinMessageId: String) {
                // "UnPinned: $pinMessageId"
                val index = adapter.pinnedList.indexOfFirst { it.id == pinMessageId }
                if (index != -1) {
                    adapter.pinnedList.removeAt(index)
                    adapter.notifyDataSetChanged()
                }
            }

            override fun onErrorMessage(error: String, clientMessageId: String?) {}
        })
        loadHistory()
        sdk.chat().addCurrentProfileToChatRoom(chatRoomId!!) { result, error ->
            result?.let {}
            error?.let {}
        }
        chatSession.connectToChatRoom(chatRoomId) { result, error ->
            result?.let {}
            error?.let {}
        }
        if (isPin) {
            loadPinnedMessage()
        }

    }

    fun loadHistory() {
        isRefreshing.value = true
        chatSession.loadNextHistory()
    }

    private fun loadPinnedMessage() {
        sdk.chat().getPinMessageInfoList(
            chatRoomId!!,
            LiveLikeOrdering.ASC,
            LiveLikePagination.FIRST
        ) { result, error ->
            result?.let {
                adapter.pinnedList.addAll(it.toSet())
                adapter.notifyDataSetChanged()
            }
            error?.let {
                println("CustomChatViewModel.onResponse>>$it")
            }
        }
    }

    fun sendMessage() {
        val msg = textMessage.value
        val matcher = msg.findImages()

        fun liveLikePreCallback(result: LiveLikeChatMessage?, error: String?) {
            textMessage.value = ""
            result?.let { message ->
                val index =
                    adapter.chatList.indexOfFirst { message.clientMessageId == it.clientMessageId }
                if (index == -1) {
                    adapter.chatList.add(message)
                    adapter.notifyItemInserted(adapter.chatList.size - 1)
                    scrollToPosition.value = adapter.itemCount - 1
                }
                currentQuoteMessage.value = null
            }
        }




        if (msg.trim().isNotEmpty()) {
//            chatSession.sendMessage(
//                when (matcher.matches()) {
//                    false -> msg
//                    else -> null
//                }, imageUrl = when (matcher.matches()) {
//                    true -> msg.substring(1, msg.length - 1)
//                    else -> null
//                }, imageWidth = 150, imageHeight = 150,
//                liveLikePreCallback=liveLikePreCallback
//            )

            if (currentQuoteMessage.value != null && currentQuoteMessage.value!!.id != null) {
                chatSession.quoteMessage(
                    msg,
                    imageUrl = when (matcher.matches()) {
                        true -> msg.substring(1, msg.length - 1)
                        else -> null
                    },
                    imageWidth = 150,
                    imageHeight = 150,
                    quoteMessageId = currentQuoteMessage.value!!.id!!,
                    quoteMessage = currentQuoteMessage.value!!,
                    liveLikePreCallback = ::liveLikePreCallback
                )
            } else {
                chatSession.sendMessage(
                    msg,
                    imageUrl = when (matcher.matches()) {
                        true -> msg.substring(1, msg.length - 1)
                        else -> null
                    },
                    imageWidth = 150,
                    imageHeight = 150,
                    liveLikePreCallback = ::liveLikePreCallback
                )
            }


        }
    }


    companion object {
        fun createFactory(
            sdk: EngagementSDK,
            isPin: Boolean,
            chatRoomId: String?,
            isLink: Boolean,
            isQuote: Boolean
        ): ViewModelProvider.Factory = viewModelFactory {
            initializer {
                CustomChatViewModel(
                    sdk,
                    isPin,
                    chatRoomId,
                    isLink,
                    isQuote,
                    this[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY] as LiveLikeApplication
                )
            }
        }
    }
}