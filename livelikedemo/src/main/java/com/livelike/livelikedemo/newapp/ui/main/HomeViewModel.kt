package com.livelike.livelikedemo.newapp.ui.main

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.livelike.common.profile
import com.livelike.common.user
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.chat
import com.livelike.engagementsdk.chat.Visibility
import com.livelike.engagementsdk.core.AccessTokenDelegate
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.engagementsdk.publicapis.LiveLikeUserApi
import com.livelike.livelikedemo.LiveLikeApplication
import com.livelike.livelikedemo.newapp.utils.getAccessTokenPrefKey
import com.livelike.livelikedemo.newapp.utils.getClientIdPrefKey
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class HomeViewModel(application: LiveLikeApplication) : AndroidViewModel(application) {

    private val sharedPrefs: SharedPreferences =
        application.getSharedPreferences("NewAppSharedPrefs", Context.MODE_PRIVATE)

    val selectedEnvironment = MutableStateFlow(Environment.QA)
    val selectedClientId = MutableStateFlow(selectedEnvironment.value.clientId)
    val selectedProfileAccessToken = MutableStateFlow<String?>(null)
    private val _engagementSDKFlow = MutableStateFlow<EngagementSDK?>(null)
    val engagementSDKFlow = _engagementSDKFlow.asStateFlow()
    private val _sdkErrorMessageFlow = MutableStateFlow<String?>(null)
    val sdkErrorMessageFlow = _sdkErrorMessageFlow.filterNotNull()

    private val _currentProfile = MutableStateFlow<LiveLikeUserApi?>(null)
    val currentProfile = _currentProfile.asStateFlow()

    val currentProfileString = currentProfile.map { it.toString() }.stateIn(
        viewModelScope,
        SharingStarted.Eagerly, ""
    )

    val profilePic = MutableStateFlow<String?>(null)
    val profileName = MutableStateFlow<String?>(null)

    init {
        viewModelScope.launch {
            launch {
                selectedEnvironment.collect {
                    sharedPrefs.edit().putString("selectedEnvironment", it.name).apply()
                    selectedClientId.value =
                        sharedPrefs.getString(getClientIdPrefKey(it.name), it.clientId)!!
                    selectedProfileAccessToken.value =
                        sharedPrefs.getString(getAccessTokenPrefKey(it.name), null)
                }
            }
            launch {
                selectedProfileAccessToken.collect {
                    sharedPrefs.edit()
                        .putString(getAccessTokenPrefKey(selectedEnvironment.value.name), it)
                        .apply()
                }
            }
            launch {
                selectedClientId.collect {
                    sharedPrefs.edit()
                        .putString(getClientIdPrefKey(selectedEnvironment.value.name), it)
                        .apply()
                }
            }
            launch {
                engagementSDKFlow.collect { sdk ->
                    sdk?.profile()?.profileStream?.subscribe(this@HomeViewModel) {
                        _currentProfile.value = it
                        profileName.value = it?.nickname
                        profilePic.value = it?.profilePic
                    }
                }
            }

            sharedPrefs.getString("selectedEnvironment", Environment.QA.name)
                ?.let { selectedEnvironment.value = Environment.valueOf(it) }
        }
    }

    fun createSDKInstance() {
        val sdk = EngagementSDK(selectedClientId.value, getApplication(),
            accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return selectedProfileAccessToken.value
                }

                override fun storeAccessToken(accessToken: String?) {
                    selectedProfileAccessToken.value = accessToken
                }
            }, errorDelegate = object : ErrorDelegate() {
                override fun onError(error: String) {
                    _sdkErrorMessageFlow.value = error
                }
            }, originURL = selectedEnvironment.value.configUrl
        )
        _engagementSDKFlow.value = sdk
    }

    fun updateProfileName() {
        profileName.value?.let { it ->
            _engagementSDKFlow.value?.profile()?.updateChatNickname(it) { result, error ->
                result?.let {
                    println("$it")
                }
                error?.let {
                    println(it)
                }
            }
        }
    }

    fun updateProfilePic() {
        _engagementSDKFlow.value?.profile()?.updateChatUserPic(profilePic.value) { result, error ->
            result?.let {
                println("$it")
            }
            error?.let {
                println("$it")
            }
        }
    }

    companion object {
        val factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                HomeViewModel(this[APPLICATION_KEY] as LiveLikeApplication)
            }
        }
    }
}

