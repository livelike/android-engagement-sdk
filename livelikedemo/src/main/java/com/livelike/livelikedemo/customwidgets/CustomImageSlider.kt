package com.livelike.livelikedemo.customwidgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.livelike.engagementsdk.widget.widgetModel.ImageSliderWidgetModel
import com.livelike.livelikedemo.databinding.CustomImageSliderBinding


class CustomImageSlider : ConstraintLayout {

    lateinit var imageSliderWidgetModel: ImageSliderWidgetModel

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init()
    }

    private lateinit var binding: CustomImageSliderBinding

    private fun init() {
        binding = CustomImageSliderBinding.inflate(LayoutInflater.from(context))
        addView(binding.root)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        imageSliderWidgetModel.widgetData.let { widget ->
            widget.options?.get(0)?.imageUrl?.let {
                binding.gaugeSeekBar.setThumbImage(it)
            }
        }
        binding.gaugeSeekBar.progressChangedCallback = {
            println("CustomImageSlider.onAttachedToWindow->$it")
        }
        binding.imageButton4.setOnClickListener {
            println(
                "CustomImageSlider.onAttachedToWindow VOTED ${
                    binding.gaugeSeekBar.getProgress().toDouble()
                }"
            )
            imageSliderWidgetModel.lockInVote(binding.gaugeSeekBar.getProgress().toDouble())
            binding.gaugeSeekBar.interactive = false
        }
        imageSliderWidgetModel.voteResults.subscribe(this.hashCode()) {
            it?.let {
                binding.txtResult.text = "Result: ${it.averageMagnitude}"
            }
        }
        binding.imageButton3.setOnClickListener {
            imageSliderWidgetModel.finish()
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        imageSliderWidgetModel.voteResults.unsubscribe(this.hashCode())
    }
}
