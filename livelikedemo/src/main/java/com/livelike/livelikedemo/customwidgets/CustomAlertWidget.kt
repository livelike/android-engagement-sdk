package com.livelike.livelikedemo.customwidgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.livelike.engagementsdk.widget.widgetModel.AlertWidgetModel
import com.livelike.livelikedemo.databinding.CustomAlertWidgetBinding
import com.livelike.utils.logDebug
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.async
import kotlinx.coroutines.delay

class CustomAlertWidget : ConstraintLayout {
    private val job = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + job)
    lateinit var alertModel: AlertWidgetModel

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init()
    }

    private lateinit var binding: CustomAlertWidgetBinding
    private fun init() {
        binding = CustomAlertWidgetBinding.inflate(LayoutInflater.from(context))
        addView(binding.root)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        alertModel.widgetData.let { likeWidget ->

            binding.bodyText.text = likeWidget.title
            likeWidget.imageUrl?.let {
                binding.bodyImage.visibility = View.VISIBLE
                Glide.with(context)
                    .load(it)
                    .into(binding.bodyImage)
            }
            likeWidget.impressionUrl?.let {
                alertModel.registerImpression(it) { result, error ->
                    logDebug { "$result" }
                }
            }
            val timeMillis = likeWidget.timeout.parseDuration() ?: 5000
            uiScope.async {
                delay(timeMillis)
                alertModel.finish()
            }
        }
    }
}
