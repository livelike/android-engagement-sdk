package com.livelike.livelikedemo.customwidgets

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.livelike.engagementsdk.OptionsItem
import com.livelike.engagementsdk.widget.widgetModel.QuizWidgetModel
import com.livelike.livelikedemo.databinding.CustomQuizWidgetBinding
import com.livelike.livelikedemo.databinding.QuizImageListItemBinding
import com.livelike.livelikedemo.databinding.QuizListItemBinding

class CustomQuizWidget : ConstraintLayout {
    var quizWidgetModel: QuizWidgetModel? = null
    var isImage = false

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init()
    }

    private lateinit var binding: CustomQuizWidgetBinding
    private fun init() {
        binding = CustomQuizWidgetBinding.inflate(LayoutInflater.from(context))
        addView(binding.root)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        quizWidgetModel?.widgetData?.let { liveLikeWidget ->
            liveLikeWidget.choices?.let {
                val adapter =
                    PollListAdapter(context, isImage, ArrayList(it.map { item -> item!! }))
                binding.rcylQuizList.adapter = adapter
                binding.button2.setOnClickListener {
                    adapter.getSelectedOption()?.let { item ->
                        if (adapter.optionIdCount.isEmpty())
                            quizWidgetModel?.lockInAnswer(item.id!!)
                    }
                }
                quizWidgetModel?.voteResults?.subscribe(this.hashCode()) { result ->
                    val op =
                        result?.choices?.find { option -> option?.id == adapter.getSelectedOption()?.id }

                    op?.let { option ->
                        Toast.makeText(
                            context,
                            when (option.isCorrect) {
                                true -> "Correct"
                                else -> "Incorrect"
                            },
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    result?.choices?.let { options ->
                        for (op in options) {
                            adapter.optionIdCount[op.id ?: ""] = op.answerCount ?: 0
                        }
                        adapter.notifyDataSetChanged()
                    }
                }
            }
            binding.imageView2.setOnClickListener {
                quizWidgetModel?.finish()
            }
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        quizWidgetModel?.voteResults?.unsubscribe(this.hashCode())
    }
}

class QuizListAdapter(
    private val context: Context,
    private val isImage: Boolean,
    val list: ArrayList<OptionsItem>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var selectedIndex = -1
    val optionIdCount: HashMap<String, Int> = hashMapOf()
    fun getSelectedOption(): OptionsItem? = when (selectedIndex > -1) {
        true -> list[selectedIndex]
        else -> null
    }

    class QuizListItemViewHolder(var itemBinding: QuizImageListItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root)

    class QuizListItemTextViewHolder(var itemTextBinding: QuizListItemBinding) :
        RecyclerView.ViewHolder(itemTextBinding.root)

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {

        val itemBinding =
            QuizImageListItemBinding.inflate(LayoutInflater.from(p0.context), p0, false)
        val itemTextBinding =
            QuizListItemBinding.inflate(LayoutInflater.from(p0.context), p0, false)

        return when (isImage) {
            true -> QuizListItemViewHolder(

                itemBinding
            )


            else -> QuizListItemTextViewHolder(
                itemTextBinding
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, index: Int) {
        val item = list[index]

        when (holder) {
            is QuizListItemViewHolder -> {
                Glide.with(context)
                    .load(item.imageUrl)
                    .into(holder.itemBinding.imageButton2)
                if (selectedIndex == index) {
                    holder.itemBinding.imageButton2.setBackgroundColor(Color.BLUE)
                } else {
                    holder.itemBinding.imageButton2.setBackgroundColor(Color.GRAY)
                }
                holder.itemBinding.textView8.text = "${optionIdCount[item.id!!] ?: 0}"
                optionIdCount[item.id!!]?.let {
                    if (selectedIndex == index) {
                        if (item.isCorrect == true) {
                            holder.itemBinding.imageButton2.setBackgroundColor(Color.GREEN)
                        } else {
                            holder.itemBinding.imageButton2.setBackgroundColor(Color.RED)
                        }
                    }
                }
                holder.itemBinding.imageButton2.setOnClickListener {
                    if (optionIdCount[item.id!!] == null) {
                        selectedIndex = holder.adapterPosition
                        notifyDataSetChanged()
                    }
                }
            }
            is QuizListItemTextViewHolder -> {
                holder.itemTextBinding.textView7.text = "${optionIdCount[item.id!!] ?: 0}"
                holder.itemTextBinding.button4.text = "${item.description}"
                if (selectedIndex == index) {
                    holder.itemTextBinding.button4.setBackgroundColor(Color.BLUE)
                } else {
                    holder.itemTextBinding.button4.setBackgroundColor(Color.GRAY)
                }
                optionIdCount[item.id!!]?.let {
                    if (selectedIndex == index) {
                        if (item.isCorrect == true) {
                            holder.itemTextBinding.button4.setBackgroundColor(Color.GREEN)
                        } else {
                            holder.itemTextBinding.button4.setBackgroundColor(Color.RED)
                        }
                    }
                }
                holder.itemTextBinding.button4.setOnClickListener {
                    if (optionIdCount[item.id!!] == null) {
                        selectedIndex = holder.adapterPosition
                        notifyDataSetChanged()
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int = list.size
}
