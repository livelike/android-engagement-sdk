package com.livelike.livelikedemo.customwidgets.timeline

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.livelike.engagementsdk.widget.timeline.TimelineWidgetResource
import com.livelike.engagementsdk.widget.viewModel.WidgetStates
import com.livelike.engagementsdk.widget.widgetModel.AlertWidgetModel
import com.livelike.livelikedemo.customwidgets.parseDuration
import com.livelike.livelikedemo.databinding.MmlAlertWidgetBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.async
import kotlinx.coroutines.delay

class MMLAlertWidget(
    context: Context,
    private var alertModel: AlertWidgetModel,
    private var timelineWidgetResource: TimelineWidgetResource? = null,
    private val isActive: Boolean = false
) : ConstraintLayout(context) {

    private val job = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + job)
    private var binding: MmlAlertWidgetBinding

    init {
        binding = MmlAlertWidgetBinding.inflate(LayoutInflater.from(context))
        addView(binding.root)
        initView()
    }

    private fun initView() {
        alertModel.widgetData.let { liveLikeWidget ->
            binding.txtTitle.text = liveLikeWidget.title
            liveLikeWidget.text?.let {
                binding.txtDescription.visibility = View.VISIBLE
                binding.txtDescription.text = liveLikeWidget.text
            }
            liveLikeWidget.imageUrl?.let {
                binding.imgAlert.visibility = View.VISIBLE
                Glide.with(context)
                    .load(it)
                    .into(binding.imgAlert)
            }
            liveLikeWidget.linkLabel?.let {
                binding.btnLink.visibility = View.VISIBLE
                binding.btnLink.text = it
                liveLikeWidget.linkUrl?.let { url ->
                    binding.btnLink.setOnClickListener {
                        alertModel.alertLinkClicked(url)
                        val universalLinkIntent =
                            Intent(Intent.ACTION_VIEW, Uri.parse(liveLikeWidget.linkUrl)).setFlags(
                                Intent.FLAG_ACTIVITY_NEW_TASK
                            )
                        if (universalLinkIntent.resolveActivity(context.packageManager) != null) {
                            ContextCompat.startActivity(context, universalLinkIntent, Bundle.EMPTY)
                        }
                    }
                }
            }
            if (!isActive) {
                binding.timeBar.visibility = View.GONE
            } else {
                val timeMillis = liveLikeWidget.timeout.parseDuration()

                binding.timeBar.visibility = View.VISIBLE
                binding.timeBar.startTimer(timeMillis, timeMillis)
                uiScope.async {
                    delay(timeMillis)
                    timelineWidgetResource?.widgetState = WidgetStates.RESULTS
                    if (timelineWidgetResource == null) {
                        alertModel.finish()
                    } else {
                        binding.timeBar.visibility = View.GONE
                    }
                }
            }
        }
    }
}
