package com.livelike.livelikedemo.customwidgets

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.livelike.engagementsdk.OptionsItem
import com.livelike.engagementsdk.widget.widgetModel.PollWidgetModel
import com.livelike.livelikedemo.databinding.CustomPollWidgetBinding
import com.livelike.livelikedemo.databinding.QuizImageListItemBinding
import com.livelike.livelikedemo.databinding.QuizListItemBinding

class CustomPollWidget : ConstraintLayout {
    var pollWidgetModel: PollWidgetModel? = null
    var isImage = false

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init()
    }

    private lateinit var binding: CustomPollWidgetBinding
    private fun init() {
        binding = CustomPollWidgetBinding.inflate(LayoutInflater.from(context))
        addView(binding.root)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        // this is added just to test exposed api loadWidgetInteraction
        pollWidgetModel?.loadInteractionHistory { result, error ->
            if (result != null) {
                if (result.isNotEmpty()) {
                    for (element in result) {
                        Log.d("interaction-poll", element.optionId)
                    }
                }
            }
        }

        pollWidgetModel?.widgetData?.let { liveLikeWidget ->
            liveLikeWidget.options?.let {
                if (it.size > 2) {
                    binding.rcylPollList.layoutManager =
                        GridLayoutManager(
                            context,
                            2
                        )
                }
                val adapter =
                    PollListAdapter(context, isImage, ArrayList(it.map { item -> item!! }))
                binding.rcylPollList.adapter = adapter
                adapter.pollListener = object : PollListAdapter.PollListener {
                    override fun onSelectOption(id: String) {
                        pollWidgetModel?.submitVote(id)
                    }
                }
                binding.button2.visibility = View.GONE
                pollWidgetModel?.voteResults?.subscribe(this.hashCode()) { result ->
                    result?.choices?.let { options ->
                        for (op in options) {
                            adapter.optionIdCount[op?.id ?: ""] = op?.voteCount ?: 0
                        }
                        adapter.notifyDataSetChanged()
                    }
                }
            }
            binding.imageView2.setOnClickListener {
                pollWidgetModel?.finish()
            }
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        pollWidgetModel?.voteResults?.unsubscribe(this.hashCode())
    }
}

class PollListAdapter(
    private val context: Context,
    private val isImage: Boolean,
    val list: ArrayList<OptionsItem>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var selectedIndex = -1
    val optionIdCount: HashMap<String, Int> = hashMapOf()

    var isFollowUp = false

    fun getSelectedOption(): OptionsItem? = when (selectedIndex > -1) {
        true -> list[selectedIndex]
        else -> null
    }

    var pollListener: PollListener? = null

    interface PollListener {
        fun onSelectOption(id: String)
    }

    class PollListItemViewHolder(var itemBinding: QuizImageListItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root)

    class PollListItemTextViewHolder(var itemTextBinding: QuizListItemBinding) :
        RecyclerView.ViewHolder(itemTextBinding.root)

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {

        val itemBinding =
            QuizImageListItemBinding.inflate(LayoutInflater.from(p0.context), p0, false)
        val itemTextBinding =
            QuizListItemBinding.inflate(LayoutInflater.from(p0.context), p0, false)

        return when (isImage) {
            true -> PollListAdapter.PollListItemViewHolder(

                itemBinding
            )


            else -> PollListAdapter.PollListItemTextViewHolder(
                itemTextBinding
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, index: Int) {
        val item = list[index]

        when (holder) {
            is PollListItemViewHolder -> {
                Glide.with(context)
                    .load(item.imageUrl)
                    .into(holder.itemBinding.imageButton2)
                if (selectedIndex == index) {
                    holder.itemBinding.imageButton2.setBackgroundColor(Color.BLUE)
                } else {
                    holder.itemBinding.imageButton2.setBackgroundColor(Color.GRAY)
                }
                holder.itemBinding.textView8.text = "${optionIdCount[item.id!!] ?: 0}"

                holder.itemBinding.imageButton2.setOnClickListener {
                    selectedIndex = holder.adapterPosition
                    pollListener?.onSelectOption(item.id!!)
                    notifyDataSetChanged()
                }
            }

            is PollListItemTextViewHolder -> {
                holder.itemTextBinding.textView7.text = "${optionIdCount[item.id!!] ?: 0}"
                holder.itemTextBinding.button4.text = "${item.description}"
                if (selectedIndex == index) {
                    holder.itemTextBinding.button4.setBackgroundColor(Color.BLUE)
                } else {
                    holder.itemTextBinding.button4.setBackgroundColor(Color.GRAY)
                }

                holder.itemTextBinding.button4.setOnClickListener {
                    if (!isFollowUp) {
                        selectedIndex = holder.adapterPosition
                        pollListener?.onSelectOption(item.id!!)
                        notifyDataSetChanged()
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int = list.size
}
