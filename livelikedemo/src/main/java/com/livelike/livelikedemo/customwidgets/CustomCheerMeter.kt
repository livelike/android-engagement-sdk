package com.livelike.livelikedemo.customwidgets

import android.content.Context
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.livelike.engagementsdk.widget.widgetModel.CheerMeterWidgetmodel
import com.livelike.livelikedemo.databinding.CustomCheerMeterBinding
import org.threeten.bp.Duration
import org.threeten.bp.format.DateTimeParseException

/**
 * TODO: document your custom view class.
 */
class CustomCheerMeter : ConstraintLayout {

    private lateinit var mCountDownTimer: CountDownTimer
    var cheerMeterWidgetModel: CheerMeterWidgetmodel? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init()
    }

    private lateinit var binding: CustomCheerMeterBinding
    private fun init() {
        binding = CustomCheerMeterBinding.inflate(LayoutInflater.from(context))
        addView(binding.root)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        cheerMeterWidgetModel?.voteResults?.subscribe(this.hashCode()) {
            val op1 = it?.choices?.get(0)
            val op2 = it?.choices?.get(1)
            val vt1 = op1?.voteCount ?: 0
            val vt2 = op2?.voteCount ?: 0
            val total = vt1 + vt2
            if (total > 0) {
                val perVt1 = (vt1.toFloat() / total.toFloat()) * 100
                val perVt2 = (vt2.toFloat() / total.toFloat()) * 100
                binding.speedView1.setSpeedAt(perVt1)
                binding.speedView2.setSpeedAt(perVt2)
                binding.txtTeam1.text = "$vt1\n$perVt1"
                binding.txtTeam2.text = "$vt2\n$perVt2"
            }
        }

        cheerMeterWidgetModel?.widgetData?.let { livelikeWidget ->

            Glide.with(context)
                .load(livelikeWidget.options?.get(0)?.imageUrl)
                .into(binding.btn1)

            Glide.with(context)
                .load(livelikeWidget.options?.get(1)?.imageUrl)
                .into(binding.btn2)

            binding.btn1.setOnClickListener {
                cheerMeterWidgetModel?.submitVote(livelikeWidget.options?.get(0)?.id!!)
            }
            binding.btn2.setOnClickListener {
                cheerMeterWidgetModel?.submitVote(livelikeWidget.options?.get(1)?.id!!)
            }
            binding.imgClose.setOnClickListener {
                cheerMeterWidgetModel?.finish()
                mCountDownTimer.cancel()
            }

            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed(
                {
                    cheerMeterWidgetModel?.finish()
                    mCountDownTimer.onFinish()
                },
                livelikeWidget.timeout.parseDuration()
            )

            var i = 0
            val timer = livelikeWidget.timeout.parseDuration()
            mCountDownTimer = object : CountDownTimer(timer, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    i++
                    binding.progressBar.progress = (i * 100 / (timer / 1000)).toInt()
                }

                override fun onFinish() {
                    // Do what you want
                    i++
                    binding.progressBar.progress = 100
                }
            }
            mCountDownTimer.start()
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        cheerMeterWidgetModel?.voteResults?.unsubscribe(this.hashCode())
    }
}
fun String.parseDuration(): Long {
    var timeout = 7000L
    try {
        timeout = Duration.parse(this).toMillis()
    } catch (e: DateTimeParseException) {
        Log.e("Error", "Duration $this can't be parsed.")
    }
    return timeout
}
