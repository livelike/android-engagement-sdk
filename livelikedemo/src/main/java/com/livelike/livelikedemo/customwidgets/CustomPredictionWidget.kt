package com.livelike.livelikedemo.customwidgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import com.livelike.engagementsdk.widget.widgetModel.FollowUpWidgetViewModel
import com.livelike.engagementsdk.widget.widgetModel.PredictionWidgetViewModel
import com.livelike.livelikedemo.databinding.CustomPredictionWidgetBinding

class CustomPredictionWidget :
    ConstraintLayout {
    var predictionWidgetViewModel: PredictionWidgetViewModel? = null
    var followUpWidgetViewModel: FollowUpWidgetViewModel? = null
    var isImage = false
    var isFollowUp = false

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init()
    }

    private lateinit var binding: CustomPredictionWidgetBinding

    private fun init() {
        binding = CustomPredictionWidgetBinding.inflate(LayoutInflater.from(context))
        addView(binding.root)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        var widgetData = predictionWidgetViewModel?.widgetData
        var voteResults = predictionWidgetViewModel?.voteResults
        if (isFollowUp) {
            widgetData = followUpWidgetViewModel?.widgetData
            voteResults = followUpWidgetViewModel?.voteResults
        }
        predictionWidgetViewModel?.loadInteractionHistory { result, error ->
            for (it in result ?: emptyList()) {
                println("CustomPredictionWidget.onResponse>>${it.optionId} =>${it.isCorrect}")
            }
        }

        widgetData?.let { liveLikeWidget ->
            liveLikeWidget.options?.let {
                if (it.size > 2) {
                    binding.rcylPollList.layoutManager =
                        GridLayoutManager(
                            context,
                            2
                        )
                }
                val adapter =
                    PollListAdapter(context, isImage, ArrayList(it.map { item -> item!! }))
                binding.rcylPollList.adapter = adapter
                adapter.pollListener = object : PollListAdapter.PollListener {
                    override fun onSelectOption(id: String) {
                        predictionWidgetViewModel?.lockInVote(id)
                    }
                }
                binding.button2.visibility = View.GONE
                voteResults?.subscribe(this.hashCode()) { result ->
                    result?.choices?.let { options ->
                        for (op in options) {
                            adapter.optionIdCount[op?.id ?: ""] = op?.voteCount ?: 0
                        }
                        adapter.notifyDataSetChanged()
                    }
                }
                if (isFollowUp) {
                    for (op in it) {
                        adapter.optionIdCount[op?.id!!] = op.voteCount ?: 0
                    }
                    adapter.isFollowUp = true
                    adapter.selectedIndex =
                        it.indexOfFirst { option -> option?.id == followUpWidgetViewModel?.getPredictionVoteId() }
                    adapter.notifyDataSetChanged()
                    followUpWidgetViewModel?.claimRewards()
                }
            }
            binding.imageView2.setOnClickListener {
                finish()
            }

//            val handler = Handler()
//            handler.postDelayed(
//                {
//                    finish()
//                },
//                (liveLikeWidget.timeout ?: "").parseDuration()
//            )
        }
    }

    private fun finish() {
        predictionWidgetViewModel?.finish()
        followUpWidgetViewModel?.finish()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        predictionWidgetViewModel?.voteResults?.unsubscribe(this.hashCode())
    }
}
