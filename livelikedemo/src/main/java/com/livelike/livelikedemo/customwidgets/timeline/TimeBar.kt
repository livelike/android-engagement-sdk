package com.livelike.livelikedemo.customwidgets.timeline

import android.animation.ObjectAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import com.livelike.livelikedemo.databinding.MmlAlertWidgetBinding


class TimeBar : View {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )
    private var binding: MmlAlertWidgetBinding = MmlAlertWidgetBinding.inflate(LayoutInflater.from(context))


    fun startTimer(totalTime: Long, remainingTime: Long) {
        binding.timeBar.pivotX = 0f
        val scaleX = (totalTime - remainingTime) / totalTime.toFloat()
        ObjectAnimator.ofFloat(binding.timeBar, "scaleX", scaleX, 1f).apply {
            this.duration = remainingTime
            start()
        }
    }
}
