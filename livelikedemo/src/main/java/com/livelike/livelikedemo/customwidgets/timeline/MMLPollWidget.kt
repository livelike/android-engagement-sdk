package com.livelike.livelikedemo.customwidgets.timeline

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.livelike.engagementsdk.OptionsItem
import com.livelike.engagementsdk.widget.timeline.TimelineWidgetResource
import com.livelike.engagementsdk.widget.viewModel.WidgetStates
import com.livelike.engagementsdk.widget.widgetModel.PollWidgetModel
import com.livelike.livelikedemo.R
import com.livelike.livelikedemo.customwidgets.parseDuration
import com.livelike.livelikedemo.databinding.MmlPollImageListItemBinding
import com.livelike.livelikedemo.databinding.MmlPollTextListItemBinding
import com.livelike.livelikedemo.databinding.MmlPollWidgetBinding
import kotlinx.coroutines.*
import kotlin.collections.set

class MMLPollWidget(
    context: Context,
    private var pollWidgetModel: PollWidgetModel,
    var timelineWidgetResource: TimelineWidgetResource? = null,
    private var isImage: Boolean = false,
    private val isActive: Boolean = false
) : ConstraintLayout(context) {
    private val job = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + job)

    private var binding: MmlPollWidgetBinding

    init {
        binding = MmlPollWidgetBinding.inflate(LayoutInflater.from(context))
        addView(binding.root)
        initView()
    }

    private fun initView() {
        pollWidgetModel.widgetData.let { liveLikeWidget ->
            binding.txtTitle.text = liveLikeWidget.question
            liveLikeWidget.options?.let { list ->
                if (isImage) {
                    binding.rcylPollList.layoutManager =
                        GridLayoutManager(
                            context,
                            2
                        )
                } else {
                    binding.rcylPollList.layoutManager =
                        LinearLayoutManager(
                            context,
                            RecyclerView.VERTICAL,
                            false
                        )
                }
                val adapter =
                    PollListAdapter(
                        context,
                        isImage,
                        ArrayList(list.map { item -> item!! })
                    )

                binding.rcylPollList.adapter = adapter
                if (!isActive) {
                    adapter.isTimeLine = true
                    for (op in list) {
                        op?.let {
                            adapter.optionIdCount[op.id!!] = op.voteCount ?: 0
                        }
                    }

                    adapter.notifyDataSetChanged()
                    binding.timeBar.visibility = View.INVISIBLE
                } else {
                    adapter.pollListener = object : PollListAdapter.PollListener {
                        override fun onSelectOption(optionsItem: OptionsItem) {
                            optionsItem.id?.let {
                                pollWidgetModel.submitVote(it)
                            }
                        }
                    }
                    adapter.notifyDataSetChanged()
                    pollWidgetModel.voteResults.subscribe(this@MMLPollWidget.hashCode()) { result ->
                        result?.choices?.let { options ->
                            var change = false
                            for (op in options) {
                                if (!adapter.optionIdCount.containsKey(op.id) || adapter.optionIdCount[op.id] != op.voteCount) {
                                    change = true
                                }
                                adapter.optionIdCount[op.id ?: ""] = op.voteCount ?: 0
                            }
                            if (change)
                                adapter.notifyDataSetChanged()
                        }
                    }

                    val timeMillis = liveLikeWidget.timeout.parseDuration() ?: 5000
                    binding.timeBar.visibility = View.VISIBLE
                    binding.timeBar.startTimer(timeMillis, timeMillis)

                    uiScope.async {
                        delay(timeMillis)
                        timelineWidgetResource?.widgetState = WidgetStates.RESULTS
                        adapter.isTimeLine = true
                        adapter.notifyDataSetChanged()
                        pollWidgetModel.voteResults.unsubscribe(this@MMLPollWidget.hashCode())
                        if (timelineWidgetResource == null) {
                            pollWidgetModel.finish()
                        } else {
                            binding.timeBar.visibility = View.GONE
                        }
                    }
                }
            }
        }
    }
}

class PollListAdapter(
    private val context: Context,
    private val isImage: Boolean,
    private val list: ArrayList<OptionsItem>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var selectedIndex = -1
    val optionIdCount: HashMap<String, Int> = hashMapOf()
    var pollListener: PollListener? = null
    var isTimeLine: Boolean = false

    interface PollListener {
        fun onSelectOption(optionsItem: OptionsItem)
    }

    class PollListItemViewHolder(var itemBinding: MmlPollImageListItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root)

    class PollListItemTextViewHolder(var itemTextBinding: MmlPollTextListItemBinding) :
        RecyclerView.ViewHolder(itemTextBinding.root)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        p1: Int
    ): RecyclerView.ViewHolder {
        val itemBinding =
            MmlPollImageListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val itemTextBinding =
            MmlPollTextListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return when (isImage) {
            true -> PollListItemViewHolder(

                itemBinding
            )


            else -> PollListItemTextViewHolder(
                itemTextBinding
            )
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, index: Int) {
        val item = list[index]

        when (holder) {
            is PollListItemViewHolder -> {

                Glide.with(context)
                    .load(item.imageUrl)
                    .transform(RoundedCorners(holder.itemView.resources.getDimensionPixelSize(R.dimen.rounded_corner_box_radius)))
                    .into(holder.itemBinding.imageView)
                if (optionIdCount.containsKey(item.id)) {
                    holder.itemBinding.progressBar.visibility = View.VISIBLE
                    holder.itemBinding.textView2.visibility = View.VISIBLE
                    val total = optionIdCount.values.reduce { acc, i -> acc + i }
                    val percent = when (total > 0) {
                        true -> (optionIdCount[item.id!!]!!.toFloat() / total.toFloat()) * 100
                        else -> 0F
                    }
                    holder.itemBinding.progressBar.progress = percent.toInt()
                    holder.itemBinding.textView2.text = "${percent.toInt()} %"
                } else {
                    holder.itemBinding.progressBar.visibility = View.INVISIBLE
                    holder.itemBinding.textView2.visibility = View.GONE
                }
                holder.itemBinding.textView.text = "${item.description}"
                if (selectedIndex == index) {
                    holder.itemBinding.layPollImgOption.setBackgroundResource(R.drawable.mml_image_option_background_selected_drawable)
                    holder.itemBinding.progressBar.progressDrawable = ContextCompat.getDrawable(
                        context,
                        R.drawable.mml_custom_progress_color_options_selected
                    )
                } else {
                    holder.itemBinding.layPollImgOption.setBackgroundResource(R.drawable.mml_image_option_background_stroke_drawable)
                    holder.itemBinding.progressBar.progressDrawable = ContextCompat.getDrawable(
                        context,
                        R.drawable.mml_custom_progress_color_options
                    )
                }
                if (!isTimeLine)
                    holder.itemBinding.layPollImgOption.setOnClickListener {
                        selectedIndex = holder.adapterPosition
                        pollListener?.onSelectOption(item)
                        notifyDataSetChanged()
                    }
                else
                    holder.itemBinding.layPollImgOption.setOnClickListener(null)

            }
            is PollListItemTextViewHolder -> {

                if (optionIdCount.containsKey(item.id)) {
                    holder.itemTextBinding.txtPercent.visibility = View.VISIBLE
                    holder.itemTextBinding.progressBarText.visibility = View.VISIBLE
                    val total = optionIdCount.values.reduce { acc, i -> acc + i }
                    val percent = when (total > 0) {
                        true -> (optionIdCount[item.id!!]!!.toFloat() / total.toFloat()) * 100
                        else -> 0F
                    }
                    holder.itemTextBinding.txtPercent.text = "${percent.toInt()} %"
                    holder.itemTextBinding.progressBarText.progress = percent.toInt()
                } else {
                    holder.itemTextBinding.txtPercent.visibility = View.INVISIBLE
                    holder.itemTextBinding.progressBarText.visibility = View.INVISIBLE
                }
                holder.itemTextBinding.textPollItem.text = "${item.description}"
                if (selectedIndex == index) {
                    holder.itemTextBinding.layPollTextOption.setBackgroundResource(R.drawable.mml_image_option_background_selected_drawable)
                    holder.itemTextBinding.progressBarText.progressDrawable =
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.mml_custom_progress_color_options_selected
                        )
                } else {
                    holder.itemTextBinding.layPollTextOption.setBackgroundResource(R.drawable.mml_image_option_background_stroke_drawable)
                    holder.itemTextBinding.progressBarText.progressDrawable =
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.mml_custom_progress_color_options
                        )
                }
                if (!isTimeLine)
                    holder.itemTextBinding.layPollTextOption.setOnClickListener {
                        selectedIndex = holder.adapterPosition
                        pollListener?.onSelectOption(item)
                        notifyDataSetChanged()
                    }
                else
                    holder.itemTextBinding.layPollTextOption.setOnClickListener(null)

            }
        }
    }

    override fun getItemCount(): Int = list.size
}
