package com.livelike.livelikedemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.livelike.livelikedemo.databinding.ActivityReactionBinding
import com.livelike.livelikedemo.reaction.ReactionHomeFragment

class ReactionActivity : AppCompatActivity() {


    private lateinit var binding: ActivityReactionBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding=ActivityReactionBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportFragmentManager.beginTransaction()
            .replace(binding.container.id, ReactionHomeFragment.newInstance())
            .addToBackStack("reactionHome")
            .commit()
    }
}