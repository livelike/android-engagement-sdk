package com.livelike.livelikedemo

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.livelike.comment.LiveLikeCommentBoardClient
import com.livelike.comment.LiveLikeCommentClient
import com.livelike.comment.comment
import com.livelike.comment.commentBoard
import com.livelike.comment.models.Comment
import com.livelike.comment.models.CommentBoard
import com.livelike.comment.models.CommentReport
import com.livelike.comment.models.CreateCommentReportRequestOptions
import com.livelike.comment.models.DeleteCommentReportRequestOptions
import com.livelike.comment.models.DismissAllReportsForACommentRequestOptions
import com.livelike.comment.models.DismissCommentReportRequestOptions
import com.livelike.comment.models.GetCommentBoardDetailRequestOptions
import com.livelike.comment.models.GetCommentReportsRequestOptions
import com.livelike.comment.models.GetCommentRequestOptions
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.livelikedemo.databinding.ActivityCommentReportBinding

class CommentReportActivity : AppCompatActivity() {

    private var activeComment: Comment? = null
    private var activeCommentReport: CommentReport? = null
    private var activeCommentBoard: CommentBoard? = null
    private var activeCommentSession: LiveLikeCommentClient? = null

    private lateinit var boards: LiveLikeCommentBoardClient
    private lateinit var binding: ActivityCommentReportBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCommentReportBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        boards = (application as LiveLikeApplication).sdk.commentBoard()

        binding.getBoard.setOnClickListener { _ ->
            boards.getCommentBoardDetails(
                GetCommentBoardDetailRequestOptions(
                    binding.boardId.text.toString().trim()
                )
            ) { result, error ->
                showToast(error, "create")
                showToast(result?.toString(), "create")
                result?.let {
                    activeCommentBoard = result
                    binding.tvBoard.text = result.id
                    activeCommentSession = it.id.let { it1 ->
                        (application as LiveLikeApplication).sdk.comment(
                            it1
                        )
                    }
                }
            }
        }

        binding.btGetComment.setOnClickListener {
            activeCommentSession.let { active ->
                active?.getComment(
                    GetCommentRequestOptions(
                        binding.etCreateComment.text.toString().trim()
                    )
                ) { result, error ->
                    binding.tvComment.text = result?.text
                    activeComment = result
                }

            }
        }

        binding.createReport.setOnClickListener {
            activeCommentSession?.let { active ->
                active.createCommentReport(CreateCommentReportRequestOptions(activeComment?.id!!)) { result, error ->
                    result?.let {
                        showToast(result.toString(), result.reportStatus!!)
                        activeCommentReport = result
                    }
                    error?.let {
                        showToast(error, "Error!!")
                    }
                }
            }
        }

        binding.getReport.setOnClickListener {
            activeCommentSession?.let { active ->
                active.getCommentReports(
                    GetCommentReportsRequestOptions(
                        commentId = activeComment?.id!!
                    ),
                    LiveLikePagination.FIRST
                ) { result, error ->
                    result?.let {
                        showToast(result.toString(), result.size.toString())
                    }
                    error?.let { showToast(error, "Error!!") }
                }

            }
        }

        binding.deleteReport.setOnClickListener {
            activeCommentSession?.let { active ->
                active.deleteCommentReport(
                    DeleteCommentReportRequestOptions(
                        commentReportId = activeCommentReport?.id!!
                    )
                ) { result, error ->
                    result?.let { showToast(result.toString(), "Report Deleted") }
                    error?.let { showToast(error, "Error!!") }
                }

            }
        }


        binding.dismissReport.setOnClickListener {
            activeCommentSession?.let { active ->
                active.dismissCommentReport(
                    DismissCommentReportRequestOptions(
                        commentReportId = activeCommentReport?.id!!
                    )
                ) { result, error ->
                    result?.let { showToast(result.toString(), "Report Dismissed") }
                    error?.let { showToast(error, "Error!!") }
                }

            }
        }

        binding.dismissAllReports.setOnClickListener {
            activeCommentSession?.let { active ->
                active.dismissAllCommentReports(
                    DismissAllReportsForACommentRequestOptions(
                        commentId = activeComment?.id!!
                    )
                ) { result, error ->
                    result?.let { showToast(result.toString(), "All Report Dismissed") }
                    error?.let { showToast(error, "Error!!") }
                }

            }
        }


    }

    fun showToast(message: String?, title: String) {
        message?.let {
            runOnUiThread {
                AlertDialog.Builder(this)
                    .setTitle(title)
                    .setMessage(it)
                    .create()
                    .show()
            }
        }
    }
}