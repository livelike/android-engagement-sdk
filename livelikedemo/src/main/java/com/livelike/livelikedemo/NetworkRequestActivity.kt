package com.livelike.livelikedemo

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.livelike.common.model.RequestType
import com.livelike.common.request
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.livelikedemo.ui.theme.AndroidengagementsdkTheme

class NetworkRequestActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val sdk = (application as LiveLikeApplication).sdk

        setContent {
            AndroidengagementsdkTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    RequestView(sdk)
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RequestView(sdk: EngagementSDK) {
    val environment = LiveLikeApplication.selectedEnvironment
    var request by remember { mutableStateOf(RequestType.GET) }
    var path by remember { mutableStateOf("applications/" + environment?.clientId) }
    var url by remember { mutableStateOf("") }
    var body by remember { mutableStateOf("") }
    var accessToken by remember { mutableStateOf("") }
    var response by remember { mutableStateOf("") }
    var expanded by remember { mutableStateOf(false) }
    var queryParams by remember { mutableStateOf<List<Pair<String, String>>>(emptyList()) }
    var headers by remember { mutableStateOf<List<Pair<String, String>>>(emptyList()) }
    Scaffold { paddingValues ->
        LazyColumn(
            modifier = Modifier
                .padding(paddingValues)
                .padding(all = 10.dp)
        ) {
            item {
                Box(modifier = Modifier
                    .clickable {
                        expanded = true
                    }
                    .fillMaxWidth()) {
                    TextField(
                        value = request.name,
                        onValueChange = { },
                        enabled = false,
                        placeholder = { Text(text = "Request Type") })
                    DropdownMenu(
                        expanded = expanded,
                        onDismissRequest = { expanded = false }
                    ) {
                        for (type in RequestType.values())
                            DropdownMenuItem(
                                text = { Text(type.name) },
                                onClick = {
                                    request = type
                                    expanded = false
                                }
                            )
                    }
                }
            }
            item {
                TextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = path,
                    onValueChange = { path = it },
                    placeholder = { Text(text = "Path") })
            }
            item {
                TextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = url,
                    onValueChange = { url = it },
                    placeholder = { Text(text = "Url") })
            }
            item {
                TextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = body,
                    onValueChange = { body = it },
                    placeholder = { Text(text = "Body") })
            }
            item {
                TextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = accessToken,
                    onValueChange = { accessToken = it },
                    placeholder = { Text(text = "Access Token") })
            }
            item {
                Row(modifier = Modifier.fillMaxWidth()) {
                    Text(text = "Headers")
                    IconButton(onClick = {
                        headers = headers + Pair("", "")
                    }) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_add_white_24dp),
                            contentDescription = "Add"
                        )
                    }
                }
            }
            itemsIndexed(headers) { index, header ->
                Row(modifier = Modifier.fillMaxWidth()) {
                    TextField(value = header.first, onValueChange = {
                        headers = headers.toMutableList().apply {
                            this[index] = header.copy(first = it)
                        }
                    })
                    TextField(value = header.second, onValueChange = {
                        headers = headers.toMutableList().apply {
                            this[index] = header.copy(second = it)
                        }
                    })
                }
            }
            item {
                Row(modifier = Modifier.fillMaxWidth()) {
                    Text(text = "Query Params")
                    IconButton(onClick = {
                        queryParams = queryParams + Pair("", "")
                    }) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_add_white_24dp),
                            contentDescription = "Add"
                        )
                    }
                }
            }
            itemsIndexed(queryParams) { index, query ->
                Row(modifier = Modifier.fillMaxWidth()) {
                    TextField(value = query.first, onValueChange = {
                        queryParams = queryParams.toMutableList().apply {
                            this[index] = query.copy(first = it)
                        }
                    })
                    TextField(value = query.second, onValueChange = {
                        queryParams = queryParams.toMutableList().apply {
                            this[index] = query.copy(second = it)
                        }
                    })
                }
            }
            item {
                ElevatedButton(
                    onClick = {
                        val fUrl = url.ifEmpty {
                            null
                        }
                        sdk.request<String, String>(
                            path,
                            fUrl,
                            request,
                            queryParams,
                            body,
                            accessToken
                        ) { result, error ->
                            result?.let { response = it }
                            error?.let { response = it }
                        }
                    },
                    modifier = Modifier.fillMaxWidth(),
                ) {
                    Text(text = "Submit")
                }
            }
            item {
                Text(text = response, modifier = Modifier.fillMaxWidth())
            }
        }
    }
}