package com.livelike.livelikedemo

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.livelike.comment.CommentLocalCacheDelegate
import com.livelike.comment.LiveLikeCommentClient
import com.livelike.comment.LiveLikeCommentSession
import com.livelike.comment.ProfaneComment
import com.livelike.comment.comment
import com.livelike.comment.commentBoard
import com.livelike.comment.createCommentSession
import com.livelike.comment.models.Comment
import com.livelike.comment.models.CommentBoard
import com.livelike.comment.models.GetCommentBoardDetailRequestOptions
import com.livelike.engagementsdk.createReactionSession
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.engagementsdk.reaction
import com.livelike.engagementsdk.reaction.LiveLikeReactionSession
import com.livelike.livelikedemo.databinding.ActivityCommentStockUiBinding

class CommentStockUi : AppCompatActivity() {
    var commentClient: LiveLikeCommentClient? = null
    var activeCommentSession: LiveLikeCommentSession? = null
    private lateinit var binding: ActivityCommentStockUiBinding
    private lateinit var reactionSpaceId: String
    private lateinit var reactionSession: LiveLikeReactionSession
    private val sharedPreferences: SharedPreferences by lazy {
        getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
    }
    private val gson: Gson = GsonBuilder().serializeNulls().create()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCommentStockUiBinding.inflate(layoutInflater)
        setContentView(binding.root)

        var errorDelegate = object : ErrorDelegate() {
            override fun onError(error: String) {
                println("LiveLikeApplication.onError: $error")
            }
        }


        val boards = (application as LiveLikeApplication).sdk.commentBoard()

        (application as LiveLikeApplication).sdk.reaction().getReactionSpaceDetails(
            "5d540a98-ff46-4020-aa50-4f69e3ca1d61"
        ) { result, error ->
            result?.let {
                reactionSpaceId = result.id

                println(" green $result")
                reactionSession = (application as LiveLikeApplication).sdk.createReactionSession(
                    reactionSpaceId,
                    "25e31e53-163c-4209-8db6-4e36b3c6f1b1", errorDelegate
                )


                boards.getCommentBoardDetails(
                    GetCommentBoardDetailRequestOptions(
                        "25e31e53-163c-4209-8db6-4e36b3c6f1b1"
                    )
                ) { result: CommentBoard?, error: String? ->
                    result?.let {
                        commentClient = it.id.let { it1 ->
                            (application as LiveLikeApplication).sdk.comment(it1)
                        }
                        activeCommentSession =
                            (application as LiveLikeApplication).sdk.createCommentSession(
                                it.id,
                                filteredTextForComment = { comment ->
                                    val filteredText = comment.filteredText
                                    if (filteredText != null && filteredText.contains("***")) {
                                        "AAAAAAAAA"
                                    } else {
                                        comment.text ?: "" // Use the comment text or an empty string if it's null
                                    }
                                },
                                profaneComment = ProfaneComment.CUSTOM,
                                delegate = object : CommentLocalCacheDelegate {
                                    override fun saveHashmaps(
                                        commentReplyMap: Map<String, List<Comment>>,
                                        originalCommentReplyMap: Map<String, List<Comment>>
                                    ) {
                                        val json1 = gson.toJson(commentReplyMap)
                                        val json2 = gson.toJson(originalCommentReplyMap)

                                        sharedPreferences.edit()
                                            .putString("commentReplyMap", json1)
                                            .putString("originalCommentReplyMap", json2)
                                            .apply()
                                    }

                                    override fun getCommentReplyMap(): Map<String, List<Comment>> {
                                        val json =
                                            sharedPreferences.getString("commentReplyMap", "")
                                        return if (json!!.isNotEmpty()) {
                                            val type = object :
                                                TypeToken<Map<String, List<Comment>>>() {}.type
                                            gson.fromJson<Map<String, List<Comment>>>(json, type)
                                        } else {
                                            HashMap() // Return an empty map if no commentReplyMap is found
                                        }
                                    }

                                    override fun getOriginalCommentReplyMap(): Map<String, List<Comment>> {

                                        val json = sharedPreferences.getString(
                                            "originalCommentReplyMap",
                                            ""
                                        )
                                        return if (json!!.isNotEmpty()) {
                                            val type = object :
                                                TypeToken<Map<String, List<Comment>>>() {}.type
                                            gson.fromJson<Map<String, List<Comment>>>(json, type)
                                        } else {
                                            HashMap() // Return an empty map if no originalCommentReplyMap is found
                                        }
                                    }

                                    override fun saveCommentCount(count: Int) {
                                        sharedPreferences.edit()
                                            .putInt("commentCount", count)
                                            .apply()
                                    }

                                    override fun getCommentCount(): Int {
                                        return sharedPreferences.getInt("commentCount", 0)
                                    }
                                })
                        binding.commentview.setSession(activeCommentSession!!, reactionSession)
                    }
                }
            }
        }
    }

}