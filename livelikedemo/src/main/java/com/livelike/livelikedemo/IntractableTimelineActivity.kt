package com.livelike.livelikedemo

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.LiveLikeContentSession
import com.livelike.engagementsdk.core.services.messaging.proxies.WidgetInterceptor
import com.livelike.engagementsdk.widget.timeline.IntractableWidgetTimelineViewModel
import com.livelike.engagementsdk.widget.timeline.WidgetsTimeLineView
import com.livelike.livelikedemo.customwidgets.timeline.TimeLineWidgetFactory
import com.livelike.livelikedemo.databinding.ActivityLiveBlogBinding
import com.livelike.livelikedemo.utils.ThemeRandomizer

class IntractableTimelineActivity : AppCompatActivity() {

    private lateinit var contentSession: LiveLikeContentSession
    private lateinit var engagementSDK: EngagementSDK
    private var themeCurrent: Int? = null

    //    var newTimelineViewModel: NewIntractableTimelineViewModel? = null
    private lateinit var binding: ActivityLiveBlogBinding
    var timeLineViewModel: IntractableWidgetTimelineViewModel? = null
    var publicSession: LiveLikeContentSession? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        themeCurrent =  R.style.MMLChatTheme
        this.setTheme(themeCurrent!!)


        binding = ActivityLiveBlogBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /**
         * Intractable view model initialized
         **/
        binding.timeoutLayout.visibility = View.GONE
//        newTimelineViewModel = ViewModelProvider(
//            this,
//            IntractableTimelineViewModelFactory(this.application)
//        )[NewIntractableTimelineViewModel::class.java]
        engagementSDK = (application as LiveLikeApplication).sdk
        contentSession =
            createPublicSession((application as LiveLikeApplication).channelManager.selectedChannel.llProgram.toString())
        binding.radio1.visibility = View.GONE
        binding.radio2.visibility = View.GONE

        /*  radio_group.setOnCheckedChangeListener { group, checkedId ->
              newTimelineViewModel?.showAlertOnly = (checkedId == R.id.radio2)
              createTimeLineView()
          }*/

    }

    private fun createPublicSession(
        sessionId: String,
        widgetInterceptor: WidgetInterceptor? = null,
    ): LiveLikeContentSession {
        if (publicSession == null || publicSession?.contentSessionId() != sessionId) {
            publicSession?.close()
            publicSession = engagementSDK.createContentSession(sessionId)
        }
        publicSession!!.widgetInterceptor = widgetInterceptor
        return publicSession as LiveLikeContentSession
    }

    override fun onResume() {
        super.onResume()
        createTimeLineView()
    }

    /**
     * create timeline view
     * for both the filter and non filtered widgets new instance of timeline and timelineviewmodel are created.
     **/
    private fun createTimeLineView() {
        binding.timelineContainer.removeAllViews()
        timeLineViewModel = IntractableWidgetTimelineViewModel(publicSession!!) { widget ->
//            if (showAlertOnly)
//                widget.getWidgetType() == WidgetType.ALERT
//            else
            true
        }

        val timeLineView = WidgetsTimeLineView(
            this,
            timeLineViewModel!!,
            engagementSDK
        )

        // added test theme
        /*  val themeFileName = "themes/test.json"
          val bufferReader = application.assets.open(themeFileName).bufferedReader()
          val data = bufferReader.use {
              it.readText()
          }
          val element =
              LiveLikeEngagementTheme.instanceFrom(JsonParser.parseString(data).asJsonObject)
          if (element is Result.Success)
              timeLineView.applyTheme(element.data)*/

        // adding custom separator between widgets in timeline
        timeLineView.setSeparator(ContextCompat.getDrawable(this, R.drawable.white_separator))

        if (LiveLikeApplication.showCustomWidgetsUI) {
            timeLineView.widgetViewFactory =
                TimeLineWidgetFactory(
                    this,
                    timeLineViewModel!!.timeLineWidgets
                )
        } else {
            if (ThemeRandomizer.themesList.size > 0) {
                timeLineView.applyTheme(ThemeRandomizer.themesList.last())
            }
        }
        binding.timelineContainer.addView(timeLineView)
    }


    override fun onDestroy() {
        super.onDestroy()
        timeLineViewModel?.clear()
    }
}
