package com.livelike.livelikedemo

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.livelikedemo.customchat.ChatFragment
import com.livelike.livelikedemo.customchat.HomeChat
import com.livelike.livelikedemo.customchat.HomeFragment
import com.livelike.livelikedemo.databinding.ActivityCustomChatBinding
import java.util.Calendar

class CustomChatActivity : AppCompatActivity() {

    var selectedHomeChat: HomeChat? = null
    var sdk: EngagementSDK? = null
    private lateinit var binding: ActivityCustomChatBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding=ActivityCustomChatBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportFragmentManager.beginTransaction()
            .replace(binding.container.id, HomeFragment.newInstance())
            .commit()
        sdk = (application as LiveLikeApplication).sdk
    }

    fun showChatScreen(homeChat: HomeChat) {
        selectedHomeChat = homeChat
        supportFragmentManager.beginTransaction()
            .replace(binding.container.id, ChatFragment.newInstance())
            .addToBackStack(homeChat.toString())
            .commit()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            selectedHomeChat?.let {
                val sharedPref =
                    application.getSharedPreferences(LiveLikeApplication.PREFERENCES_APP_ID, Context.MODE_PRIVATE)
                sharedPref.edit().putLong(
                    "msg_time_${it.channel.llProgram}",
                    Calendar.getInstance().timeInMillis
                ).apply()
            }
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }
}
