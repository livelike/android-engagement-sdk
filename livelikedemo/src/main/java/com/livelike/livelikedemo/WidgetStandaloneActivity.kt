package com.livelike.livelikedemo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.livelike.livelikedemo.databinding.ActivityStandaloneWidgetBinding

class WidgetStandaloneActivity : AppCompatActivity() {
    private lateinit var binding: ActivityStandaloneWidgetBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding=ActivityStandaloneWidgetBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}
