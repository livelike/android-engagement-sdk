package com.livelike.livelikedemo

import android.os.Bundle
import android.widget.RadioButton
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import com.livelike.engagementsdk.EarnableReward
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.OptionReward
import com.livelike.engagementsdk.WidgetAttribute
import com.livelike.engagementsdk.WidgetStatus
import com.livelike.engagementsdk.WidgetsRequestOrdering
import com.livelike.engagementsdk.WidgetsRequestParameters
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.widget.UnsupportedWidgetType
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.livelikedemo.databinding.ActivityGetWidgetTestBinding

class GetWidgetTestActivity : AppCompatActivity() {

    private val orderingToViewLookup: MutableMap<WidgetsRequestOrdering, RadioButton> = mutableMapOf()
    private val stateToViewLookup: MutableMap<WidgetStatus?, RadioButton> = mutableMapOf()
    private val typeToViewLookup: MutableMap<WidgetType, SwitchCompat> = mutableMapOf()
    private lateinit var binding: ActivityGetWidgetTestBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = ActivityGetWidgetTestBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val channelManager = (application as LiveLikeApplication).channelManager
        val session = (application as LiveLikeApplication).createPublicSession(
            channelManager.selectedChannel?.llProgram.toString(), allowTimeCodeGetter = false,
        )

        stateToViewLookup[null] = RadioButton(this).apply {
            text = "Any"
        }
        binding.getWidgetStatusFilters.addView(stateToViewLookup[null])

        for (it in WidgetStatus.values()) {
            val radioButton = RadioButton(this)
            radioButton.text = it.parameterValue
            binding.getWidgetStatusFilters.addView(radioButton)
            stateToViewLookup[it] = radioButton
        }

        for (it in WidgetsRequestOrdering.values()) {
            val radioButton = RadioButton(this)
            radioButton.text = it.name
            binding.getWidgetOrdering.addView(radioButton)
            orderingToViewLookup[it] = radioButton
        }

        for (it in WidgetType.values().filter {
            //remove unsupported types
            it.declaringClass.getField(it.name)
                .getAnnotation(UnsupportedWidgetType::class.java) == null
        }) {
            val switch = SwitchCompat(this)
            switch.text = it.event
                .replace("-created", "")
                .replace("-updated", "")
            binding.getWidgetTypeFilters.addView(switch)
            typeToViewLookup[it] = switch
        }

        binding.runFilterButton.setOnClickListener {
            var interactiveStatus: Boolean? = null
            if (binding.radioBtnTrue.isChecked) {
                interactiveStatus = true
            } else if (binding.radioBtnFalse.isChecked) {
                interactiveStatus = false
            }
            var sinceDateTime: String? = null
            val time = binding.sinceDateTimeEdt.text.toString()
            if (time.isNotEmpty()) sinceDateTime = time
            session.getWidgets(
                LiveLikePagination.FIRST,
                WidgetsRequestParameters(
                    typeToViewLookup.filterValues { it.isChecked }.keys,
                    stateToViewLookup.filterValues { it.isChecked }.keys.let {
                        if (it.isEmpty()) {
                            null
                        } else {
                            it.first()
                        }
                    },
                    orderingToViewLookup.filterValues { it.isChecked }.keys.let {
                        if (it.isNotEmpty()) {
                            it.first()
                        } else {
                            null
                        }
                    },
                    interactive = interactiveStatus,
                    since = sinceDateTime
                )
            ) { result, error ->
                buildResultDialog(result, error)
            }
        }

    }

    private fun buildResultDialog(result: List<LiveLikeWidget>?, error: String?) {
        error?.let {
            AlertDialog.Builder(this)
                .setMessage(it)
                .create()
                .show()
        }
        result?.let {
            AlertDialog.Builder(this)
                .setTitle("Total Widgets -> ${result.size}")
                .setItems(it.map { widget ->
                    "type: ${widget.getWidgetType()}, widget-Id: ${widget.id}, status: ${widget.status}, interactiveUntil : ${widget.interactiveUntil}"
                }.toTypedArray()) { dialog, which ->
                    dialog.dismiss()

                    AlertDialog.Builder(this)
                        .setTitle("data requested")
                        .setPositiveButton("Earned Rewards") { dialog2, _ ->
                            dialog2.dismiss()
                            showEarnedRewards(result[which])
                        }
                        .setNegativeButton("Attributes") { dialog2, _ ->
                            dialog2.dismiss()
                            showAttributes(result[which])
                        }
                        .create()
                        .show()

                }
                .create()
                .show()
        }
    }

    private fun showAttributes(liveLikeWidget: LiveLikeWidget) {
        AlertDialog.Builder(this)
            .setItems(
                liveLikeWidget.widgetAttributes
                    ?.map(WidgetAttribute::toString)
                    ?.toTypedArray()
                    ?: emptyArray()
            ) { _, _ -> }
            .create()
            .show()
    }

    private fun showEarnedRewards(likeLikeWidget: LiveLikeWidget) {
        val items: Array<String> = likeLikeWidget.earnableRewards
            ?.map(EarnableReward::toString)
            ?.toTypedArray()
            ?.plus(
                likeLikeWidget.options?.flatMap { option ->
                    option?.earnableRewards?.map(OptionReward::toString) ?: emptyList()
                } ?: emptyList()
            ) ?: emptyArray()

        AlertDialog.Builder(this)
            .setItems(items) { _, _ -> }
            .create()
            .show()
    }
}