package com.livelike.livelikedemo.ui.main

import android.content.Context
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.livelike.engagementsdk.LiveLikeContentSession
import com.livelike.engagementsdk.chat.LiveLikeChatSession
import com.livelike.livelikedemo.R
import com.livelike.livelikedemo.databinding.MmlChatViewBinding

class MMLChatView(
    context: Context,
    var chatSession: LiveLikeChatSession,
    var widgetSession: LiveLikeContentSession
) : ConstraintLayout(context) {

    private lateinit var binding: MmlChatViewBinding


    init {
//        val contextThemeWrapper: Context =
//            ContextThemeWrapper(context, R.style.MMLChatTheme)
        binding = MmlChatViewBinding.inflate(LayoutInflater.from(context))
        addView(binding.root)

        binding.root.layoutTransition.setAnimateParentHierarchy(
            false
        )
        chatSession.let {
            binding.customChatView.setSession(it)
            binding.customChatView.isChatInputVisible = false
            val emptyView =
                LayoutInflater.from(context).inflate(R.layout.empty_chat_data_view, null)
            binding.customChatView.emptyChatBackgroundView = emptyView
            binding.customChatView.allowMediaFromKeyboard = false
        }
    }

    override fun onStartTemporaryDetach() {
        super.onStartTemporaryDetach()
        binding.customChatView.hidePopUpReactionPanel()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        binding.customChatView.hidePopUpReactionPanel()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        widgetSession.let {
            binding.widgetView.setSession(it)
        }
    }

    fun dismissReactionPanel() {
        binding.customChatView.hidePopUpReactionPanel()
    }

//    override fun onDetachedFromWindow() {
//        super.onDetachedFromWindow()
//        chatSession?.close()
//    }
}
