package com.livelike.livelikedemo.ui.main

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.livelike.engagementsdk.publicapis.SmartContractDetails
import java.util.ArrayList


class SmartContractsAdapter(var list: ArrayList<SmartContractDetails?>) : RecyclerView.Adapter<SmartContractsAdapter.ViewHolder>(){

    var onItemClick: ((SmartContractDetails?) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(android.R.layout.simple_list_item_1, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.nameTextView.setTextColor(Color.parseColor("#ffffff"))
        holder.nameTextView.setText(list.get(position)?.contractName)
    }

    inner class ViewHolder(@NonNull view: View) : RecyclerView.ViewHolder(view) {
        val nameTextView: TextView

        init {
            nameTextView = view
                .findViewById<View>(android.R.id.text1) as TextView
            nameTextView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }
        }
    }
}
