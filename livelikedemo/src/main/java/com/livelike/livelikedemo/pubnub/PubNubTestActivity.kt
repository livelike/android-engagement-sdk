package com.livelike.livelikedemo.pubnub

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.livelike.common.AccessTokenDelegate
import com.livelike.engagementsdk.ContentSession
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.livelikedemo.databinding.ActvityTestPubnubSubscribeBinding
import com.livelike.network.NetworkResult
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.json.JSONObject

class PubNubTestActivity : AppCompatActivity() {

    lateinit var binding: ActvityTestPubnubSubscribeBinding

    private val CLIENT_ID = "mOBYul18quffrBDuq2IACKtVuLbUzXIPye5S3bq5"
    private val PROGRAM_ID = "7b7aa007-4a9b-497a-9c55-ec819a91b475"
    private val  token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJibGFzdHJ0IiwiaWF0IjoxNzAyNjgyNTUwLCJpZCI6ImU0OGJiMmY3LTZjYzktNDJjMS04MmJlLTI1NjhmODkxNWExYyIsImNsaWVudF9pZCI6Im1PQll1bDE4cXVmZnJCRHVxMklBQ0t0VnVMYlV6WElQeWU1UzNicTUiLCJhY2Nlc3NfdG9rZW4iOiI0ZDFhMGMxODNjZTNkNDc4MmY2YjBmYTA0NTI0Y2NjMTQ3YTc2Zjg0In0.EhH9xo0D6QaM5X5TihonhhMMqx08q7Ep8SKdOgLJtzM"
    private val pubNubTestChannel = "embet.test.channel"
    private val jsonObject = JsonObject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = ActvityTestPubnubSubscribeBinding.inflate(layoutInflater)

        setContentView(binding.root)

        binding.punnubMessage.text = "Sending Pubnub package.."
        binding.punnubMessagePayload.text = "Waiting for pubnub payload .."



        initConnection()
    }

    val url =
        "https://ps.pndsn.com/publish/pub-c-6e7847f6-4694-4bda-ae56-5b622911e35d/sub-c-b3c4bff2-16cd-11ea-a1d5-ea5a03b00545/0/embet.test.channel/0"

    private fun sendPubNubEvent(sdk: EngagementSDK) {

        val map = mutableMapOf<String, Any>()
        map["widgetId"] = "widget.1424"
        map["year"] = "2025"
        map["sponsor"] = "Atletiko Madrid"


        jsonObject.add("payload", Gson().toJsonTree(map))

        lifecycleScope.launch {

            val result = sdk.networkClient.post(
                url,
                jsonObject.toString(),
                headers = listOf(
                    Pair("Accept", "application/json"),
                    Pair("Content-Type", "application/json")
                )
            )

            println(result)

            if(result is NetworkResult.Success){
                binding.punnubMessage.text = "Successfully Sent Package \n on custom channel with content : ${map}"
            }else{
                binding.punnubMessage.text = "Error on Sent Package}"
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)


    }

    private fun initConnection() {

        val sdk = EngagementSDK(
            clientId = CLIENT_ID,
            applicationContext = this,
            accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return token
                }

                override fun storeAccessToken(accessToken: String?) {

                }
            }

        )

        val session =
            sdk.createContentSession(programId = PROGRAM_ID, connectToDefaultChatRoom = false)

        lifecycleScope.launch {

            delay(2000)
            session.subscribeToChannels(listOf(pubNubTestChannel))?.collectLatest {
                println(it.payload)

                binding.punnubMessagePayload.animate().scaleY(1.2f).translationY(130f).setDuration(1000).start()
                binding.punnubMessagePayload.text = "Message Received \n ${it.payload}"
            }
        }

        lifecycleScope.launch {
            binding.punnubMessagePayload.animate().scaleY(1.5f).translationY(100f).setDuration(7000).start()
            delay(7000)
            sendPubNubEvent(sdk)

        }


    }


}