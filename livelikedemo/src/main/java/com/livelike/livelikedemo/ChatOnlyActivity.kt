package com.livelike.livelikedemo

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.livelike.common.TimeCodeGetter
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.EpochTime
import com.livelike.engagementsdk.chat

import com.livelike.engagementsdk.chat.ChatRoomInfo
import com.livelike.engagementsdk.chat.LiveLikeChatSession
import com.livelike.engagementsdk.chat.Visibility
import com.livelike.engagementsdk.core.utils.isNetworkConnected
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.livelikedemo.chatonly.ChatOnlyFragment
import com.livelike.livelikedemo.chatonly.ChatOnlyHomeFragment
import com.livelike.livelikedemo.databinding.ActivityChatOnlyBinding


class ChatOnlyActivity : AppCompatActivity() {
    internal var privateGroupChatsession: LiveLikeChatSession? = null
    internal var chatRoomInfo: ChatRoomInfo? = null
    internal val sessionMap: HashMap<String, LiveLikeChatSession> = hashMapOf()

    private lateinit var binding: ActivityChatOnlyBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChatOnlyBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportFragmentManager.beginTransaction()
            .replace(binding.container.id, ChatOnlyHomeFragment.newInstance())
            .addToBackStack("chatHome")
            .commit()
    }

    internal fun changeChatRoom(
        chatRoomId: String,
        url: String? = null,
        showFiltered: Boolean
    ) {
        binding.prgChat.visibility = View.VISIBLE
        val session = sessionMap[chatRoomId]
        privateGroupChatsession =
            session ?: (application as LiveLikeApplication).createPrivateSessionForMultiple(
                errorDelegate = object : ErrorDelegate() {
                    override fun onError(error: String) {
                        checkForNetworkToRecreateActivity()
                    }
                },
                timecodeGetter = object : TimeCodeGetter {
                    override fun invoke(): EpochTime {
                        return EpochTime(0)
                    }
                },
                showFiltered
            )
//        showAvatar?.let {
//            privateGroupChatsession?.shouldDisplayAvatar = it
//        }
        url?.let {
            privateGroupChatsession?.avatarUrl = it
        }
        sessionMap[chatRoomId] = privateGroupChatsession!!
        // empty chat room id case verified
        (application as LiveLikeApplication).sdk.chat().getChatRoom(chatRoomId) { result, error ->
            chatRoomInfo = result
            binding.prgChat.visibility = View.INVISIBLE
            supportFragmentManager.beginTransaction()
                .replace(binding.container.id, ChatOnlyFragment.newInstance(chatRoomId))
                .addToBackStack("chat")
                .commit()
        }
    }

    private fun checkForNetworkToRecreateActivity() {
        binding.container.postDelayed(
            {
                if (isNetworkConnected()) {
                    binding.container.post {
                        startActivity(intent)
                        finish()
                    }
                } else {
                    checkForNetworkToRecreateActivity()
                }
            },
            1000
        )
    }

    internal fun selectVisibility(visibilityInterface: ChatOnlyFragment.VisibilityInterface) {
        AlertDialog.Builder(this).apply {
            setTitle("Select Visibility")
            setItems(Visibility.values().map { it.toString() }.toTypedArray()) { _, which ->
                val visibility = Visibility.values()[which]
                visibilityInterface.onSelectItem(visibility)
            }
            create()
        }.show()
    }

    override fun onDestroy() {
        super.onDestroy()

        sessionMap.forEach { entry ->
            entry.value.close()
        }
        sessionMap.clear()
    }
}
