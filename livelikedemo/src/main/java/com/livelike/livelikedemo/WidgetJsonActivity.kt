package com.livelike.livelikedemo

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.livelike.engagementsdk.fetchWidgetDetails
import com.livelike.engagementsdk.widget.widgetModel.CheerMeterWidgetmodel
import com.livelike.livelikedemo.customwidgets.CustomCheerMeter
import com.livelike.livelikedemo.databinding.ActivityWidgetJsonBinding


class WidgetJsonActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWidgetJsonBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityWidgetJsonBinding.inflate(layoutInflater)
        setContentView(binding.root)

        (application as LiveLikeApplication).channelManager.let { manager ->
            val session = (application as LiveLikeApplication).createPublicSession(
                manager.selectedChannel.llProgram.toString(),
            )
            binding.editTextTextPersonName.setText("df50c665-aff9-4a93-85b2-bc9aa9488b26")
            binding.editTextTextPersonName2.setText("cheer-meter")

            binding.button3.setOnClickListener {
                val widgetId = binding.editTextTextPersonName.text.toString()
                val widgetKind = binding.editTextTextPersonName2.text.toString()
                (application as LiveLikeApplication).sdk.fetchWidgetDetails(widgetId,
                    widgetKind
                ) { result, error ->
                    result?.let {
                        val widgetModel = session.getWidgetModelFromLiveLikeWidget(it)
                        val widget = CustomCheerMeter(applicationContext)
                        widget.cheerMeterWidgetModel = widgetModel as CheerMeterWidgetmodel
                        binding.frameWidget.removeAllViews()
                        binding.frameWidget.addView(widget)
                    }
                    error?.let {
                        Toast.makeText(applicationContext, it, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }
}