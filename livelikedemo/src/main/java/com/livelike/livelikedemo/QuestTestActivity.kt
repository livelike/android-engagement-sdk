package com.livelike.livelikedemo

import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.gamification.GetQuestsRequestOptions
import com.livelike.engagementsdk.gamification.GetUserQuestsRequestOptions
import com.livelike.engagementsdk.gamification.IQuestsClient
import com.livelike.engagementsdk.gamification.Quest
import com.livelike.engagementsdk.gamification.QuestReward
import com.livelike.engagementsdk.gamification.QuestRewardStatus
import com.livelike.engagementsdk.gamification.QuestTask
import com.livelike.engagementsdk.gamification.UserQuest
import com.livelike.engagementsdk.gamification.UserQuestReward
import com.livelike.engagementsdk.gamification.UserQuestStatus
import com.livelike.engagementsdk.gamification.UserQuestTask
import com.livelike.engagementsdk.quests
import com.livelike.livelikedemo.databinding.ActivityQuestTestBinding

class QuestTestActivity : AppCompatActivity() {

    private lateinit var quests: IQuestsClient
    private lateinit var binding: ActivityQuestTestBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityQuestTestBinding.inflate(layoutInflater)
        setContentView(binding.root)


        quests = (application as LiveLikeApplication).sdk.quests()

        binding.getQuests.setOnClickListener {
            if (binding.questIdFilter.text.toString() == "" && binding.questIdFilter2.text.toString() != "") {
                showToast("fill first id first if testing multiple filters")
                return@setOnClickListener
            }

            quests.getQuests(
                if (binding.questIdFilter.text.toString() == "") {
                    GetQuestsRequestOptions()
                } else {
                    GetQuestsRequestOptions(
                        if (binding.questIdFilter2.text.toString() == "") {
                            listOf(binding.questIdFilter.text.toString())
                        } else {
                            listOf(
                                binding.questIdFilter.text.toString(),
                                binding.questIdFilter2.text.toString()
                            )
                        }
                    )
                }, LiveLikePagination.FIRST
            ) { result, error ->
                error?.let {
                    showToast(it)
                }

                result?.let {
                    it.let {
                        showQuests(it)
                    }
                }
            }
        }

        binding.getUserQuests.setOnClickListener {
            if (binding.questIdUser.text.toString() == "" && binding.questIdFilter2.text.toString() != "") {
                showToast("fill first id first if testing multiple filters")
                return@setOnClickListener
            }


            quests.getUserQuests(
                GetUserQuestsRequestOptions(
                    userQuestIds = if (binding.questIdUser.text.toString() == "") {
                        emptyList()
                    } else {
                        if (binding.questIdUser2.text.toString() == "") {
                            listOf(binding.questIdUser.text.toString())
                        } else {
                            listOf(
                                binding.questIdUser.text.toString(),
                                binding.questIdUser2.text.toString(),
                            )
                        }
                    }, status = when (binding.statusSpinner.selectedItem.toString()) {
                        UserQuestStatus.COMPLETED.key -> UserQuestStatus.COMPLETED
                        UserQuestStatus.INCOMPLETE.name -> UserQuestStatus.INCOMPLETE
                        else -> null
                    }, profileID = if (binding.profileIdUser.text.toString() == "") {
                        null as String?
                    } else {
                        binding.profileIdUser.text.toString()
                    }, rewardStatus = null
                ), LiveLikePagination.FIRST
            ) { result, error ->
                error?.let(this@QuestTestActivity::showToast)
                result?.let {
                    showUserQuests(it)
                }
            }
        }

        binding.newUserQuest.setOnClickListener {
            quests.startUserQuest(binding.questId.text.toString()) { result, error ->
                error?.let(this@QuestTestActivity::showToast)
                result?.let {
                    showUserQuests(listOf(it))
                }
            }
        }

        binding.completeTask.setOnClickListener {
            if (binding.completeTaskQuestId.text.toString() == "" && binding.completeTaskTaskId2.text.toString() != "") {
                showToast("fill first id first if testing multiple filters")
                return@setOnClickListener
            }

            quests.updateUserQuestTask(
                binding.completeTaskQuestId.text.toString(),
                if (binding.completeTaskTaskId2.text.toString() == "") {
                    listOf(binding.completeTaskTaskId.text.toString())
                } else {
                    listOf(
                        binding.completeTaskTaskId.text.toString(),
                        binding.completeTaskTaskId2.text.toString()
                    )
                }, UserQuestStatus.COMPLETED
            ) { result, error ->
                error?.let(this@QuestTestActivity::showToast)
                result?.let {
                    showUserQuests(listOf(it))
                }
            }
        }

        binding.inprogressTask.setOnClickListener {
            if (binding.completeTaskQuestId.text.toString() == "" && binding.completeTaskTaskId2.text.toString() != "") {
                showToast("fill first id first if testing multiple filters")
                return@setOnClickListener
            }

            quests.updateUserQuestTask(
                binding.completeTaskQuestId.text.toString(),
                if (binding.completeTaskTaskId2.text.toString() == "") {
                    listOf(binding.completeTaskTaskId.text.toString())
                } else {
                    listOf(
                        binding.completeTaskTaskId.text.toString(),
                        binding.completeTaskTaskId2.text.toString()
                    )
                },
                UserQuestStatus.INCOMPLETE
            ) { result, error ->
                error?.let(this@QuestTestActivity::showToast)
                result?.let {
                    showUserQuests(listOf(it))
                }
            }
        }

        binding.incrementProgressTask.setOnClickListener {
            try {
                quests.incrementUserQuestTaskProgress(
                    binding.progressTaskId.text.toString(),
                    if (binding.progressTaskCustomAmount.text.toString() == "") {
                        null
                    } else {
                        binding.progressTaskCustomAmount.text.toString().toFloat()
                    }
                ) { result, error ->
                    error?.let(this@QuestTestActivity::showToast)
                    result?.let {
                        showUserQuests(listOf(result))
                    }
                }
            } catch (e: NumberFormatException) {
                showToast("use a number for progress")
            }
        }

        binding.setProgressTask.setOnClickListener {
            try {
                quests.setUserQuestTaskProgress(
                    binding.progressTaskId.text.toString(),
                    binding.progressTaskSetAmount.text.toString().toFloat()
                ) { result, error ->
                    error?.let(this@QuestTestActivity::showToast)
                    result?.let {
                        showUserQuests(listOf(it))
                    }
                }
            } catch (e: NumberFormatException) {
                showToast("use a number for progress")
            }
        }

        val adapter = ArrayAdapter(this,
            android.R.layout.simple_spinner_item,
            QuestRewardStatus.values().map { it.key })
        binding.spinnerRewardStatus.adapter = adapter
        binding.btnUserQuest.setOnClickListener {
            quests.getUserQuestRewards(
                binding.edUserQuestId.text.toString(),
                QuestRewardStatus.values()[binding.spinnerRewardStatus.selectedItemPosition],
                LiveLikePagination.FIRST
            ) { result, error ->
                result?.let {
                    showUserQuestRewards(it)
                }
                error?.let(this@QuestTestActivity::showToast)
            }
        }
        binding.btnQuestReward.setOnClickListener {
            quests.getQuestRewards(
                binding.edRewardQuestId.text.toString(),
                LiveLikePagination.FIRST
            ) { result, error ->
                result?.let {
                    showQuestRewards(it)
                }
                error?.let(this@QuestTestActivity::showToast)
            }
        }
        binding.btnClaimUserQuest.setOnClickListener {
            quests.claimUserQuestRewards(
                binding.edClaimUserQuestId.text.toString()
            ) { result, error ->
                result?.let {
                    showUserQuests(listOf(it))
                }
                error?.let(this@QuestTestActivity::showToast)
            }
        }
    }

    private fun showQuests(list: List<Quest>) {
        runOnUiThread {
            AlertDialog.Builder(this).setTitle("quests").setItems(list.map {
                """
                        $it
                        
                    """.trimIndent()
            }.toTypedArray()) { dialog, index ->
                dialog.dismiss()
                showTasks(list[index].questTasks)

                binding.questIdFilter.setText(list[index].id ?: "")
                binding.questId.setText(list[index].id ?: "")

            }.create().show()
        }
    }

    private fun showTasks(tasks: List<QuestTask>?) {
        runOnUiThread {
            AlertDialog.Builder(this).setTitle("Tasks").setItems(tasks?.map {
                """
                    $it
                    
                """.trimIndent()
            }?.toTypedArray()) { _, _ -> }.create().show()
        }
    }

    private fun showToast(message: String?) {
        message?.let {
            runOnUiThread {
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun showUserQuests(list: List<UserQuest>) {
        runOnUiThread {
            AlertDialog.Builder(this).setTitle("user quests").setItems(list.map {
                """
                       $it
                       
                    """.trimIndent()
            }.toTypedArray()) { dialog, index ->
                dialog.dismiss()
                showUserTasks(list[index].userQuestTasks)

                binding.questIdUser.setText(list[index].id)
                binding.completeTaskQuestId.setText(list[index].id)

            }.create().show()
        }
    }

    private fun showUserQuestRewards(list: List<UserQuestReward>) {
        AlertDialog.Builder(this).setTitle("user quests Rewards").setItems(list.map {
            it.toString()
        }.toTypedArray()) { dialog, index ->
            dialog.dismiss()
        }.create().show()
    }

    private fun showQuestRewards(list: List<QuestReward>) {
        AlertDialog.Builder(this).setTitle("quests Rewards").setItems(list.map {
            """
                   $it
                    
                """.trimIndent()
        }.toTypedArray()) { dialog, index ->
            dialog.dismiss()
        }.create().show()
    }

    private fun showUserTasks(tasks: List<UserQuestTask>?) {
        runOnUiThread {
            AlertDialog.Builder(this).setTitle("User tasks").setItems(tasks?.map {
                """
                    $it
                    
                """.trimIndent()
            }?.toTypedArray()) { dialog, index ->
                dialog.dismiss()
                tasks?.get(index)?.questTask?.let {
                    showTasks(listOf(it))

                    binding.completeTaskTaskId.setText(tasks[index].id)
                    binding.progressTaskId.setText(tasks[index].id)
                }
            }.create().show()
        }
    }

}