package com.livelike.livelikedemo

import android.app.AlertDialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.gamification.models.GetInvokedRewardActionParams
import com.livelike.engagementsdk.gamification.models.InvokedRewardAction
import com.livelike.engagementsdk.gamification.models.RewardActionInfo
import com.livelike.engagementsdk.rewards
import com.livelike.livelikedemo.databinding.InvokedActionsHistoryBinding


class InvokedActionsHistoryActivity : AppCompatActivity() {
    private lateinit var binding: InvokedActionsHistoryBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = InvokedActionsHistoryBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val rewardClient = (applicationContext as LiveLikeApplication).sdk.rewards()

        binding.rewardActions.setOnClickListener {
            rewardClient.getRewardActions(LiveLikePagination.FIRST) { result, error ->
                buildResultDialog(result, error)
            }
        }


        binding.getInvokedRewardActions.setOnClickListener {
            val rewardActionList = binding.invokedActionUser.text.toString()
                .split(",").map { it.trim() }

            val profilesList = binding.invokedActionProfileID.text.toString()
                .split(",").map { it.trim() }

            val temp = binding.programId.text.toString().trim()
            val programId = if (temp != "") {
                null
            } else {
                temp
            }

            val attributes = binding.attributesFilter.text.toString()
                .split(",")
                .map { it.trim() }
                .zipWithNext()
                .filterIndexed { index, _ ->
                    index % 2 == 0
                }
                .toMap()

            rewardClient.getInvokedRewardActions(
                LiveLikePagination.FIRST,
                GetInvokedRewardActionParams(
                    programId,
                    rewardActionList,
                    profilesList,
                    attributes
                )
            ) { result, error ->
                buildActionHistoryDialog(result, error)
            }
        }
    }


    private fun buildResultDialog(result: List<RewardActionInfo>?, error: String?) {
        error?.let {
            AlertDialog.Builder(this)
                .setMessage(it)
                .create()
                .show()
        }

        result?.let {
            AlertDialog.Builder(this)
                .setTitle("Total count: " + result.size)
                .setItems(it.map { rewardActions ->
                    "\nreward action name: ${rewardActions.name}, reward action key : ${rewardActions.key}, " +
                            "reward action description: ${rewardActions.description}"
                }.toTypedArray()) { _, _ -> }
                .create()
                .show()
        }
    }


    private fun buildActionHistoryDialog(
        result: List<InvokedRewardAction>?,
        error: String?
    ) {
        error?.let {
            AlertDialog.Builder(this)
                .setMessage(it)
                .create()
                .show()
        }

        result?.let {
            AlertDialog.Builder(this)
                .setTitle("Total count: " + result.size)
                .setItems(it.map { invokedActions ->
                    "\nprogram id: ${invokedActions.programId}, reward action key : ${invokedActions.rewardActionKey}, " +
                            "rewards: ${invokedActions.rewards}, attributes:${invokedActions.attributes}"
                }.toTypedArray()) { _, which ->
                    binding.invokedActionProfileID.setText(it[which].profileId)
                    binding.invokedActionUser.setText(it[which].rewardActionKey)
                    binding.programId.setText(it[which].programId)
                    binding.attributesFilter.setText(
                        it[which].attributes.joinToString { entry -> "${entry.key},${entry.value}" }
                    )
                }
                .create()
                .show()
        }
    }

}