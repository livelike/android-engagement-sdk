package com.livelike.livelikedemo

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.livelike.common.user
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.livelikedemo.databinding.ActivitySocialClientTestBinding
import com.livelike.mobile.social.LiveLikeSocialGraphClient
import com.livelike.mobile.social.models.CreateProfileRelationshipRequestParams
import com.livelike.mobile.social.models.DeleteProfileRelationshipsRequestParams
import com.livelike.mobile.social.models.GetProfileRelationshipRequestParams
import com.livelike.mobile.social.models.GetProfileRelationshipsRequestParams
import com.livelike.mobile.social.models.ProfileRelationship
import com.livelike.mobile.social.models.ProfileRelationshipType
import com.livelike.mobile.social.socialGraphClient
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SocialClientTestActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySocialClientTestBinding
    private lateinit var socialGraphClient: LiveLikeSocialGraphClient
    private var selectedRelationship: ProfileRelationship? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySocialClientTestBinding.inflate(layoutInflater)
        setContentView(binding.root)

        socialGraphClient = (application as LiveLikeApplication).sdk.socialGraphClient()

        binding.loadAllUser.setOnClickListener {
            socialGraphClient.getProfileRelationships(
                GetProfileRelationshipsRequestParams(),
                LiveLikePagination.FIRST
            ) { result, error ->
                error?.let(::showToast)
                result?.let(::showSocialRelationship)
            }
        }

        binding.getFiltered.setOnClickListener {
            socialGraphClient.getProfileRelationships(
                GetProfileRelationshipsRequestParams(
                    binding.profileIdFrom.text.toString().trim().emptyToNull(),
                    binding.relationType.text.toString().trim().emptyToNull(),
                    binding.profileIdTo.text.toString().trim().emptyToNull(),
                ),
                LiveLikePagination.FIRST
            ) { result, error ->
                error?.let(::showToast)
                result?.let(::showSocialRelationship)
            }
        }

        binding.getRelationships.setOnClickListener {
            socialGraphClient.getProfileRelationshipTypes(
                LiveLikePagination.FIRST
            ) { result, error ->
                error?.let(::showToast)
                result?.let(::showSocialRelationshipType)
            }
        }

        binding.deleteRelationship.setOnClickListener {
            selectedRelationship?.let {
                socialGraphClient.deleteProfileRelationship(
                    DeleteProfileRelationshipsRequestParams(it.id)
                ) { result, error ->
                    error?.let(::showToast)
                    result?.run { "deleted" }?.let(::showToast)
                    selectedRelationship = null
                    updateButtons()
                }
            }
        }

        binding.createRelation.setOnClickListener {
            socialGraphClient.createProfileRelationship(
                CreateProfileRelationshipRequestParams(
                    binding.profileIdFrom.text.toString().trim(),
                    binding.relationType.text.toString().trim(),
                    binding.profileIdTo.text.toString().trim(),
                )
            ) { result, error ->
                error?.let(::showToast)
                result?.let { listOf(it) }?.let(::showSocialRelationship)
            }
        }

        binding.selectedRelationship.setOnClickListener {
            selectedRelationship?.let { relationship ->
                socialGraphClient.getProfileRelationship(
                    GetProfileRelationshipRequestParams(relationship.id)
                ) { result, error ->
                    error?.let(::showToast)
                    result?.let { listOf(it) }?.let(::showSocialRelationship)
                    updateButtons()
                }
            }
        }

        val textWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                updateButtons()
            }
        }

        binding.setToMe.setOnClickListener {
            GlobalScope.launch {
                binding.profileIdFrom.setText(
                    (application as LiveLikeApplication).sdk.user().currentProfileOnce.invoke().id
                )
            }
        }

        binding.profileIdTo.addTextChangedListener(textWatcher)
        binding.relationType.addTextChangedListener(textWatcher)
        binding.profileIdFrom.addTextChangedListener(textWatcher)

    }

    private fun showSocialRelationshipType(result: List<ProfileRelationshipType>) {
        runOnUiThread {
            AlertDialog.Builder(this)
                .setTitle("relationships types")
                .setItems(result.map { it.key }?.toTypedArray()) { dialog, index ->
                    dialog.dismiss()
                    binding.profileIdFrom.setText("")
                    binding.relationType.setText(result[index].key)
                    binding.profileIdTo.setText("")
                    selectedRelationship = null
                    updateButtons()
                }
                .create()
                .show()
        }
    }

    private fun showSocialRelationship(result: List<ProfileRelationship>) {
        runOnUiThread {
            AlertDialog.Builder(this)
                .setTitle("relationships")
                .setItems(result.map {
                    """                    
                    from: ${it.fromProfile.nickname}
                    relation: ${it.profileRelationshipType.key}
                    to: ${it.toProfile.nickname}
                    
                """.trimIndent()
                }.toTypedArray()) { dialog, index ->
                    dialog.dismiss()
                    binding.profileIdFrom.setText(result[index].fromProfile.id)
                    binding.relationType.setText(result[index].profileRelationshipType.key)
                    binding.profileIdTo.setText(result[index].toProfile.id)
                    selectedRelationship = result[index]
                    updateButtons()
                }
                .create()
                .show()
        }
    }

    private fun updateButtons() {
        binding.deleteRelationship.isEnabled = selectedRelationship != null
        binding.createRelation.isEnabled =
            !(binding.profileIdFrom.text.toString().trim() == ""
                    || binding.relationType.text.toString().trim() == ""
                    || binding.profileIdTo.text.toString().trim() == "")
        binding.selectedRelationship.isEnabled = binding.createRelation.isEnabled
    }


    private fun showToast(message: String?) {
        message?.let {
            runOnUiThread {
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            }
        }
    }
}


private fun String.emptyToNull(): String? {
    return if (this == "") {
        null
    } else {
        this
    }
}