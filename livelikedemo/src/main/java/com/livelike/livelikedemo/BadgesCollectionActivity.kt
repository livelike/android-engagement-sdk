package com.livelike.livelikedemo

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.livelike.engagementsdk.badges
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.gamification.models.Badge
import com.livelike.engagementsdk.gamification.models.BadgeProgress
import com.livelike.engagementsdk.gamification.models.ProfileBadge
import com.livelike.engagementsdk.gamification.models.ProfilesByBadge
import com.livelike.livelikedemo.databinding.ActivityBadgesCollectionBinding
import com.livelike.livelikedemo.databinding.ActivityBadgesCollectionListItemBinding

var isProfileBadges = false


class BadgesCollectionActivity : AppCompatActivity() {


    private lateinit var binding: ActivityBadgesCollectionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityBadgesCollectionBinding.inflate(layoutInflater)
        setContentView(binding.root)

// 0c141991-daea-47ad-93fc-4b4a48738929
// profile id with more than 20 badges in qa env.
//        profile_id_tv.setText("909c060d-1a92-47d6-b91a-f3e4911529f8")

        val badgeListAdapter = BadgeListAdapter()
        binding.badgesListRv.layoutManager = LinearLayoutManager(this)
        binding.badgesListRv.adapter = badgeListAdapter
        val badgesClient = (applicationContext as LiveLikeApplication).sdk.badges()

        badgeListAdapter.badgeClickListener = { badge ->
            binding.progressBar.visibility = View.VISIBLE
            badgesClient.getProfileBadgeProgress(
                binding.profileIdTv.text.toString(),
                mutableListOf(badge.id)
            ) { result, error ->
                result?.let {
                    if (result.isNotEmpty()) {
                        showBadgeProgressDialog(
                            this@BadgesCollectionActivity,
                            it.get(0)
                        )
                    }
                }
                binding.progressBar.visibility = View.GONE
                showError(error)
            }
        }

        binding.fetchBadgesProfile.setOnClickListener {
            isProfileBadges = true
            binding.progressBar.visibility = View.VISIBLE
            badgesClient.getProfileBadges(
                binding.profileIdTv.text.toString(), LiveLikePagination.FIRST
            ) { result, error ->
                binding.progressBar.visibility = View.GONE
                showError(error)
                result?.let {
                    badgeListAdapter.badges.clear()
                    val elements: List<ProfileBadge> =
                        result
                    badgeListAdapter.badges.addAll(elements.map { it.badge })
                    badgeListAdapter.notifyDataSetChanged()
                }
            }
        }

        binding.fetchApplicationBadge.setOnClickListener {
            isProfileBadges = false
            binding.progressBar.visibility = View.VISIBLE
            badgesClient.getApplicationBadges(LiveLikePagination.FIRST) { result, error ->
                badgeListAdapter.badges.clear()
                addBadgesToAdapter(error, result, badgeListAdapter)
            }
        }

        binding.loadMore.setOnClickListener {
            binding.progressBar.visibility = View.VISIBLE
            if (isProfileBadges) {
                badgesClient.getProfileBadges(
                    binding.profileIdTv.text.toString(), LiveLikePagination.NEXT
                ) { result, error ->
                    binding.progressBar.visibility = View.GONE
                    showError(error)
                    result?.let {
                        val elements: List<ProfileBadge> = result
                        badgeListAdapter.badges.addAll(elements.map { it.badge })
                        badgeListAdapter.notifyDataSetChanged()
                    }
                }
            } else {
                badgesClient.getApplicationBadges(LiveLikePagination.NEXT) { result, error ->
                    addBadgesToAdapter(error, result, badgeListAdapter)
                }
            }
        }

        binding.profileByBadge.setOnClickListener {
            val editText = EditText(this)
//            editText.setText( "f8ead40f-8832-4b27-b84f-e413d036f843")
            AlertDialog.Builder(this)
                .setTitle("enter badge id(s)")
                .setMessage("use commas to separate multiples")
                .setView(editText)
                .setPositiveButton("proceed") { _, _ ->
                    loadProfiles(editText.text.toString())
                }
                .create().show()
        }
    }

    private fun loadProfiles(badges: String) {
        (applicationContext as LiveLikeApplication).sdk.badges()
            .getBadgeProfiles(badges, LiveLikePagination.FIRST) { result, error ->
                result?.let {
                    showProfiles(it)
                }
                showError(error)
            }
    }

    private fun showProfiles(profilesByBadges: List<ProfilesByBadge>?) {
        AlertDialog.Builder(this)
            .setTitle("profiles Count : ${profilesByBadges?.size}")
            .setItems(profilesByBadges?.map {
                "id: ${it.profile}\nbadge id: ${it.badgeId}\nawarded: ${it.awardedAt}"
            }?.toTypedArray()) { _, _ -> }
            .create().show()
    }

    private fun addBadgesToAdapter(
        error: String?,
        result: List<Badge>?,
        badgeListAdapter: BadgeListAdapter
    ) {
        runOnUiThread {
            binding.progressBar.visibility = View.GONE
            showError(error)
            result?.let {
                val elements: List<Badge> = result
                badgeListAdapter.badges.addAll(elements)
                badgeListAdapter.notifyDataSetChanged()
            }
        }
    }

    private fun showError(error: String?) {
        error?.let {
            Toast.makeText(
                applicationContext,
                error,
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun showBadgeProgressDialog(context: Context, badgeProgress: BadgeProgress) {
        AlertDialog.Builder(context).apply {
            setTitle("Progress towards ${badgeProgress.badge.name}")
            val progression = badgeProgress.progressionList.get(0)
            val progress: String =
                progression.currentRewardAmount.toString() + " out of " + progression.rewardItemThreshold + " " + progression.rewardItemName
            setMessage(progress)
        }.show()
    }

    class BadgeListAdapter : RecyclerView.Adapter<BadgeListAdapter.BadgeVH>() {

        lateinit var badgeClickListener: (badge: Badge) -> Unit
        internal val badges = mutableListOf<Badge>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BadgeVH {
            val itemBinding = ActivityBadgesCollectionListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            return BadgeVH(

                itemBinding
            )
        }

        override fun onBindViewHolder(holder: BadgeVH, position: Int) {
            holder.badgeName.text = badges[position].name
            Glide.with(holder.itemView.context).load(badges[position].badgeIconUrl)
                .into(holder.badgeIcon)
            if (isProfileBadges) {
                holder.tickIcon.visibility = View.VISIBLE
            } else {
                holder.tickIcon.visibility = View.GONE
            }

            holder.itemView.setOnClickListener {
                badgeClickListener.invoke(badges[position])
            }

            holder.itemBinding.links.setOnClickListener { _ ->
                android.app.AlertDialog.Builder(holder.itemView.context)
                    .setTitle("Registered Links")
                    .setItems(
                        badges[position].registeredLinks.map {
                            it.name
                        }.toTypedArray()
                    ) { _, item ->
                        holder.itemView.context.startActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse(badges[position].registeredLinks[item].linkUrl)
                            )
                        )
                    }.create().show()
            }

        }

        override fun getItemCount(): Int {
            return badges.size
        }

        class BadgeVH(var itemBinding: ActivityBadgesCollectionListItemBinding) :
            RecyclerView.ViewHolder(itemBinding.root) {
            val badgeIcon: ImageView = itemBinding.badgeIc
            val badgeName: TextView = itemBinding.badgeNameTv
            val tickIcon: ImageView = itemBinding.tickIc
        }
    }
}
