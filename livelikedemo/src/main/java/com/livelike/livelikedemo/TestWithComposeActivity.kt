package com.livelike.livelikedemo

import android.os.Bundle
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.livelike.engagementsdk.LiveLikeContentSession
import com.livelike.engagementsdk.chat.ChatView
import com.livelike.engagementsdk.widget.view.WidgetView
import com.livelike.livelikedemo.ui.theme.AndroidengagementsdkTheme

class TestWithComposeActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val programId =
            (application as LiveLikeApplication).channelManager?.selectedChannel?.llProgram
        val sdk = (application as LiveLikeApplication).sdk
        val session = sdk.createContentSession(programId!!)
        setContent {
            AndroidengagementsdkTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Screen(session = session)
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Screen(session: LiveLikeContentSession) {
    Scaffold {
        Box(modifier = Modifier.padding(it)) {
            AndroidView(factory = { context ->
                ChatView(context, null).apply {
                    layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
                    setSession(session.chatSession)
                }
            }, modifier = Modifier.padding(all = 10.dp))
            AndroidView(factory = { ctx ->
                WidgetView(ctx, null).apply {
                    layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                    setSession(session)
                }
            }, modifier = Modifier.padding(all = 15.dp))
        }
    }
}