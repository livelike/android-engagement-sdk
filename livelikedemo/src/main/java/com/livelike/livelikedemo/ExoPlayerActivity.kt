package com.livelike.livelikedemo

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.constraintlayout.widget.Constraints
import androidx.lifecycle.lifecycleScope
import androidx.media3.ui.PlayerView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.livelike.engagementsdk.*
import com.livelike.engagementsdk.chat.*
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.chat.data.remote.PinMessageInfo
import com.livelike.engagementsdk.core.services.messaging.proxies.LiveLikeWidgetEntity
import com.livelike.engagementsdk.core.services.messaging.proxies.WidgetInterceptor
import com.livelike.engagementsdk.core.services.messaging.proxies.WidgetLifeCycleEventsListener
import com.livelike.engagementsdk.core.utils.isNetworkConnected
import com.livelike.engagementsdk.publicapis.BlockedInfo
import com.livelike.engagementsdk.publicapis.ChatMessageType
import com.livelike.engagementsdk.publicapis.ChatRoomAdd
import com.livelike.engagementsdk.publicapis.ChatRoomDelegate
import com.livelike.engagementsdk.publicapis.ChatRoomInvitation
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.engagementsdk.publicapis.LiveLikeCallback
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage
import com.livelike.engagementsdk.widget.LiveLikeWidgetViewFactory
import com.livelike.engagementsdk.widget.domain.Reward
import com.livelike.engagementsdk.widget.domain.RewardSource
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.model.GetPublishedWidgetsRequestOptions
import com.livelike.engagementsdk.widget.viewModel.WidgetStates
import com.livelike.engagementsdk.widget.widgetModel.*
import com.livelike.livelikedemo.LiveLikeApplication.Companion.PREFERENCES_APP_ID
import com.livelike.livelikedemo.channel.Channel
import com.livelike.livelikedemo.channel.ChannelManager
import com.livelike.livelikedemo.customwidgets.CustomCheerMeter
import com.livelike.livelikedemo.customwidgets.CustomQuizWidget
import com.livelike.livelikedemo.databinding.ActivityExoPlayerBinding
import com.livelike.livelikedemo.databinding.CustomMsgItemBinding
import com.livelike.livelikedemo.databinding.DemoChatDateViewBinding
import com.livelike.livelikedemo.databinding.DemoSnapToLiveBinding
import com.livelike.livelikedemo.utils.DialogUtils
import com.livelike.livelikedemo.utils.DialogUtils.showDialog
import com.livelike.livelikedemo.utils.ThemeRandomizer
import com.livelike.livelikedemo.video.PlayerState
import com.livelike.livelikedemo.video.VideoPlayer
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.livelike.utils.parseISODateTime
import com.livelike.utils.registerLogsHandler
import com.livelike.utils.unregisterLogsHandler
import kotlinx.coroutines.launch
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter
import java.util.*


class ExoPlayerActivity : AppCompatActivity() {

    private var showPicker: Boolean = false
    private var showingDialog: Boolean = false
    private var allowDefaultLoadChatRoom: Boolean = true
    private var allowDelete: Boolean = true
    private var customLink: String? = null
    private var showLink: Boolean = false
    private var showImagePicker: Boolean = false
    private var enableReplies: Boolean = false
    private var hideDeletedMessage:Boolean = false
    private var includeFilteredChatMessages: Boolean = false
    private val themeRadomizerHandler = Handler(Looper.getMainLooper())
    private var jsonTheme: String? = null
    private var showNotification: Boolean = true
    private var themeCurrent: Int? = null
    private var player: VideoPlayer? = null
    private var session: LiveLikeContentSession? = null
    private var startingState: PlayerState? = null
    private var channelManager: ChannelManager? = null
    private var myWidgetsList: ArrayList<LiveLikeWidget> = arrayListOf()
    private lateinit var binding: ActivityExoPlayerBinding
    private var chatMessageListener: MessageListener?=null


    private var adsPlaying = false
        set(adsPlaying) {
            field = adsPlaying

            if (adsPlaying) {
                binding.startAd.text = "Stop Ads"
                player?.stop()
                session?.pause()
            } else {
                binding.startAd.text = "Start Ads"
                player?.start()
                session?.resume()
            }
        }
    private val timer = Timer()
    private lateinit var chatRoomLastTimeStampMap: MutableMap<String, Long>
    private var showChatAvatar = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        themeCurrent = intent.getIntExtra("theme", R.style.AppTheme)
        this.setTheme(themeCurrent!!)

        binding = ActivityExoPlayerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        jsonTheme = intent.getStringExtra("jsonTheme")
        if (isNetworkConnected()) {
            chatRoomLastTimeStampMap = GsonBuilder().create().fromJson(
                getSharedPreferences(PREFERENCES_APP_ID, Context.MODE_PRIVATE).getString(
                    PREF_CHAT_ROOM_LAST_TIME,
                    null
                ),
                object : TypeToken<MutableMap<String, Long>>() {}.type
            ) ?: mutableMapOf()
            testKeyboardDismissUseCase(themeCurrent!!)
            binding.playerView.layoutParams.width = Constraints.LayoutParams.MATCH_PARENT

            player = (application as LiveLikeApplication).createPlayer(binding.playerView as PlayerView)
            channelManager = (application as LiveLikeApplication).channelManager
            binding.openLogs.setOnClickListener {
                binding.fullLogs.visibility =
                    if (binding.fullLogs.visibility == View.GONE) View.VISIBLE else View.GONE
            }
            binding.fullLogs.movementMethod = ScrollingMovementMethod()

            binding.liveBlog.setOnClickListener {
                startActivity(Intent(this, LiveBlogActivity::class.java))
            }

            val chatSnapToLiveDelegate = object : ChatSnapToLiveDelegate {
                val snapToLiveCard = DemoSnapToLiveBinding.inflate(
                    LayoutInflater.from(this@ExoPlayerActivity),
                    null,
                    false
                )

                init {
//                    snapToLiveCard.root.layoutParams = ViewGroup.LayoutParams(
//                        200,
//                        50
//                    )
                }

                override fun getSnapToLiveView(): View {
                    return snapToLiveCard.root
                }

                override fun applyConstraintsOnChatViewForSnapToLiveView(
                    constraintSet: ConstraintSet,
                    rootViewId: Int
                ) {
                    constraintSet.connect(
                        getSnapToLiveView().id,
                        ConstraintSet.START,
                        ConstraintSet.PARENT_ID,
                        ConstraintSet.START
                    )
                    constraintSet.connect(
                        getSnapToLiveView().id,
                        ConstraintSet.BOTTOM,
                        ConstraintSet.PARENT_ID,
                        ConstraintSet.BOTTOM
                    )
                    constraintSet.connect(
                        getSnapToLiveView().id,
                        ConstraintSet.END,
                        ConstraintSet.PARENT_ID,
                        ConstraintSet.END
                    )
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        constraintSet.setHorizontalBias(
                            getSnapToLiveView().id,
                            0.5f
                        )
                    } else {
//                        val outValue = TypedValue()
//                        resources.getValue(com.livelike.engagementsdk.R.dimen.livelike_snap_live_horizontal_bias, outValue, true)
                        constraintSet.setHorizontalBias(
                            getSnapToLiveView().id,
                            0.5f
                        )
                    }
                    constraintSet.setMargin(
                        getSnapToLiveView().id,
                        ConstraintSet.START,
                        resources.getDimensionPixelSize(com.livelike.engagementsdk.R.dimen.livelike_snap_live_margin_start)
                    )
                    constraintSet.setMargin(
                        getSnapToLiveView().id,
                        ConstraintSet.LEFT,
                        resources.getDimensionPixelSize(com.livelike.engagementsdk.R.dimen.livelike_snap_live_margin_left)
                    )
                    constraintSet.setMargin(
                        getSnapToLiveView().id,
                        ConstraintSet.END,
                        0
                    )
                    constraintSet.setMargin(
                        getSnapToLiveView().id,
                        ConstraintSet.RIGHT,
                        0
                    )
                    constraintSet.setMargin(
                        getSnapToLiveView().id,
                        ConstraintSet.BOTTOM,
                        350
                    )
                }
            }
            binding.chatWidget.chatView.setChatSnapToLiveDelegate(chatSnapToLiveDelegate)

            binding.chatWidget.chatView.setScrollBarEnabled(true)
            showNotification = intent.getBooleanExtra("showNotification", true)
            showLink = intent.getBooleanExtra("showLink", false)
            showImagePicker = intent.getBooleanExtra("showImagePicker", false)
            customLink = intent.getStringExtra("customLink")
            allowDefaultLoadChatRoom = intent.getBooleanExtra("allowDefaultLoadChatRoom", true)
            allowDelete = intent.getBooleanExtra("allowDelete", true)
            showPicker = intent.getBooleanExtra("showPicker", true)

            enableReplies = intent.getBooleanExtra("enableReplies", false)
            hideDeletedMessage = intent.getBooleanExtra("hideDeletedMessage", false)
            includeFilteredChatMessages =
                intent.getBooleanExtra("includeFilteredChatMessages", false)
            adsPlaying = savedInstanceState?.getBoolean(AD_STATE) ?: false
            val position = savedInstanceState?.getLong(POSITION) ?: 0
            startingState = PlayerState(0, position, !adsPlaying)

            showingDialog = savedInstanceState?.getBoolean(SHOWING_DIALOG) ?: false
            if (showingDialog) {
                dialog.showDialog(this@ExoPlayerActivity) {
                    showingDialog = it
                }
            }

            timer.schedule(
                object : TimerTask() {
                    override fun run() {
                        runOnUiThread {
                            val pdtTime = player?.getPDT() ?: 0
                            binding.videoTimestamp.text = Date(pdtTime).toString()
                        }
                    }
                },
                0, 100
            )

            setUpAdClickListeners()

            showChatAvatar = intent.getBooleanExtra("showAvatar", true)
            if (intent.getBooleanExtra("customCheerMeter", false))
                binding.chatWidget.widgetView.widgetViewFactory =
                    object : LiveLikeWidgetViewFactory {
                        override fun createCheerMeterView(cheerMeterWidgetModel: CheerMeterWidgetmodel): View? {
                            return CustomCheerMeter(this@ExoPlayerActivity).apply {
                                this.cheerMeterWidgetModel = cheerMeterWidgetModel
                            }
                        }

                        override fun createAlertWidgetView(alertWidgetModel: AlertWidgetModel): View? {
                            return null
                        }

                        override fun createQuizWidgetView(
                            quizWidgetModel: QuizWidgetModel,
                            isImage: Boolean
                        ): View? {
                            return CustomQuizWidget(this@ExoPlayerActivity).apply {
                                this.quizWidgetModel = quizWidgetModel
                                this.isImage = isImage
                            }
                        }

                        override fun createPredictionWidgetView(
                            predictionViewModel: PredictionWidgetViewModel,
                            isImage: Boolean
                        ): View? {
                            return null
                        }

                        override fun createPredictionFollowupWidgetView(
                            followUpWidgetViewModel: FollowUpWidgetViewModel,
                            isImage: Boolean
                        ): View? {
                            return null
                        }

                        override fun createPollWidgetView(
                            pollWidgetModel: PollWidgetModel,
                            isImage: Boolean
                        ): View? {
                            return null
                        }

                        override fun createImageSliderWidgetView(imageSliderWidgetModel: ImageSliderWidgetModel): View? {
                            return null
                        }

                        override fun createVideoAlertWidgetView(videoAlertWidgetModel: VideoAlertWidgetModel): View? {
                            return null
                        }

                        override fun createTextAskWidgetView(imageSliderWidgetModel: TextAskWidgetModel): View? {
                            return null
                        }

                        override fun createNumberPredictionWidgetView(
                            numberPredictionWidgetModel: NumberPredictionWidgetModel,
                            isImage: Boolean
                        ): View? {
                            return null
                        }

                        override fun createNumberPredictionFollowupWidgetView(
                            followUpWidgetViewModel: NumberPredictionFollowUpWidgetModel,
                            isImage: Boolean
                        ): View? {
                            return null
                        }

                        override fun createSocialEmbedWidgetView(socialEmbedWidgetModel: SocialEmbedWidgetModel): View? {
                            return null
                        }
                    }

            binding.selectChannelButton.setOnClickListener {
                channelManager?.let { cm ->
                    val channels = cm.getChannels()
                    AlertDialog.Builder(this).apply {
                        setTitle("Choose a channel to watch!")
                        if (cm.nextUrl?.isNotEmpty() == true)
                            setPositiveButton(
                                "Load Next"
                            ) { dialog, _ ->
                                cm.loadClientConfig(cm.nextUrl)
                                dialog.dismiss()
                            }
                        if (cm.previousUrl?.isNotEmpty() == true)
                            setNeutralButton(
                                "Load Previous"
                            ) { dialog, _ ->
                                cm.loadClientConfig(cm.previousUrl)
                                dialog.dismiss()
                            }
                        setItems(channels.map { it.name }.toTypedArray()) { _, which ->
                            cm.selectedChannel = channels[which]
                            privateGroupRoomId = null
                            selectChannel(channels[which])
                        }
                        create()
                    }.show()
                }
            }
            myWidgetsList = GsonBuilder().create()
                .fromJson(
                    getSharedPreferences(PREFERENCES_APP_ID, Context.MODE_PRIVATE).getString(
                        PREF_MY_WIDGETS,
                        null
                    ),
                    object : TypeToken<List<LiveLikeWidget>>() {}.type
                ) ?: arrayListOf()

            binding.btnMyWidgets.setOnClickListener {

                AlertDialog.Builder(this).apply {
                    setTitle("Choose Option")
                        .setItems(
                            arrayListOf(
                                "Live Widgets",
                                "Published Widgets"
                            ).toTypedArray()
                        ) { _, which ->
                            if (which == 0) {
                                DialogUtils.showMyWidgetsDialog(
                                    context,
                                    (application as LiveLikeApplication).sdk,
                                    myWidgetsList,
                                    object : com.livelike.common.LiveLikeCallback<LiveLikeWidget> {
                                        override fun invoke(
                                            result: LiveLikeWidget?,
                                            error: String?
                                        ) {
                                            result?.let {
                                                binding.chatWidget.widgetView?.displayWidget(
                                                    (application as LiveLikeApplication).sdk,
                                                    result
                                                )
                                            }
                                        }
                                    }
                                )
                            } else {
                                session?.getPublishedWidgets(
                                    GetPublishedWidgetsRequestOptions(liveLikePagination = LiveLikePagination.FIRST)
                                ) { result, error ->
                                    result?.map { it }.let {
                                        DialogUtils.showMyWidgetsDialog(
                                            context,
                                            (application as LiveLikeApplication).sdk,
                                            ArrayList(it ?: emptyList()),
                                            object : com.livelike.common.LiveLikeCallback<LiveLikeWidget> {
                                                override fun invoke(
                                                    result: LiveLikeWidget?,
                                                    error: String?
                                                ) {
                                                    result?.let {
                                                        binding.chatWidget.widgetView?.displayWidget(
                                                            (application as LiveLikeApplication).sdk,
                                                            result
                                                        )
                                                    }
                                                }
                                            }
                                        )
                                    }
                                }
                            }
                        }
                    create()
                }.show()
            }
            channelManager?.let {
                selectChannel(it.selectedChannel)
            }
        } else {
            // checkForNetworkToRecreateActivity()
        }
        if (themeCurrent == R.style.MMLChatTheme) {
            val emptyView =
                LayoutInflater.from(this).inflate(R.layout.empty_chat_data_view, null)
            binding.chatWidget.chatView.emptyChatBackgroundView = emptyView
            binding.chatWidget.chatView.allowMediaFromKeyboard = false
        }
        if (isHideChatInput) {
            binding.chatWidget.chatView.isChatInputVisible = false
        }


        (applicationContext as LiveLikeApplication).sdk.userProfileDelegate =
            object : UserProfileDelegate {
                override fun userProfile(
                    userProfile: LiveLikeUser,
                    reward: Reward,
                    rewardSource: RewardSource
                ) {
                    val text =
                        "rewards recieved from ${rewardSource.name} : id is ${reward.rewardItem}, amount is ${reward.amount}"
                    binding.logsPreview.text = "$text \n\n ${binding.logsPreview.text}"
                    binding.fullLogs.text = "$text \n\n ${binding.fullLogs.text}"
                    println(text)
                }
            }

        binding.btnSendMessage?.setOnClickListener {
            session?.chatSession?.sendMessage(message = "Sample ${Random().nextInt()}",
                parentMessageId = null,
                imageUrl = null,
                imageWidth = null,
                imageHeight = null,
                liveLikePreCallback = { result, error ->
                    result?.let {
                        Toast.makeText(
                            applicationContext,
                            it.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    error?.let {
                        Toast.makeText(applicationContext, it, Toast.LENGTH_SHORT).show()
                    }
                }, chatInterceptor = {
                    kotlin.random.Random.nextBoolean()
                })
        }

        binding.btnScrollPosition?.setOnClickListener {
            val alert = AlertDialog.Builder(this)
            val edittext = EditText(this)
            alert.setTitle("Enter Message ID")

            alert.setView(edittext)

            alert.setPositiveButton("Done") { dialog, _ -> //What ever you want to do with the value
                val text = edittext.text.toString()
                dialog.dismiss()
                binding.chatWidget.chatView?.scrollToMessage(text)
            }

            alert.show()
        }

        binding.btnCustomMessage.setOnClickListener {
            session?.chatSession?.sendCustomChatMessage(
                "{" +
                        "\"custom_message\": \"${getRandomString((10..50).random())}\"" +
                        "}"
            ) { result, error ->
                result?.let {
                    println("ExoPlayerActivity.onResponse> ${it.id}")
                }
                error?.let {
                    Toast.makeText(applicationContext, error, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    class MyCustomMsgViewHolder(var itemBinding: CustomMsgItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root)

    private fun getRandomString(sizeOfRandomString: Int): String {
        val random = Random()
        val sb = StringBuilder(sizeOfRandomString)
        for (ignored in 0 until sizeOfRandomString) {
            sb.append(ALLOWED_CHARACTERS[random.nextInt(ALLOWED_CHARACTERS.length)])
            if (random.nextBoolean()) {
                sb.append(" ")
            }
        }
        return sb.toString()
    }

    private val ALLOWED_CHARACTERS = "0123456789qwertyuiopasdfghjklzxcvbnm"

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        if (this.isTaskRoot) {
            this.finishAfterTransition()
        } else {
            (application as LiveLikeApplication).removePublicSession()
            (application as LiveLikeApplication).removePrivateSession()
            super.onBackPressed()
        }
    }

    private fun testKeyboardDismissUseCase(themeCurrent: Int) {
        if (themeCurrent == R.style.MMLChatTheme) {
            binding.chatWidget.chatView.sentMessageListener = {
                binding.chatWidget.chatView.dismissKeyboard()
            }
        }
    }

    private fun setUpAdClickListeners() {
        binding.startAd.setOnClickListener {
            adsPlaying = !adsPlaying
        }
    }

    private fun selectChannel(channel: Channel) {
        player?.stop()
        player?.release()
        startingState = PlayerState(0, 0, !adsPlaying)
        initializeLiveLikeSDK(channel)
    }

    private val dialog = object : WidgetInterceptor() {
        override fun widgetWantsToShow(widgetData: LiveLikeWidgetEntity) {
            val widgetDataJson = GsonBuilder().create().toJson(widgetData)
            addLogs("widgetWantsToShow : $widgetDataJson")
            showDialog(this@ExoPlayerActivity) {
                showingDialog = it
            }
        }
    }

    private fun initializeLiveLikeSDK(channel: Channel) {
        registerLogsHandler(object :
                (String) -> Unit {
            override fun invoke(text: String) {
                Handler(mainLooper).post {
                    binding.logsPreview.text = "$text \n\n ${binding.logsPreview.text}"
                    binding.fullLogs.text = "$text \n\n ${binding.fullLogs.text}"
                }
            }
        })

        if (channel != ChannelManager.NONE_CHANNEL) {
            val session = (application as LiveLikeApplication).createPublicSession(
                channel.llProgram.toString(),
                when (showNotification) {
                    true -> dialog
                    else -> null
                },
                allowDefaultLoadChatRoom = allowDefaultLoadChatRoom,
                includeFilteredChatMessages = includeFilteredChatMessages
            )
            if (!allowDefaultLoadChatRoom) {
                session.chatSession.connectToChatRoom("38043523-920c-47eb-afb4-128d27f5a849")
            }

            binding.chatWidget.chatView.errorDelegate = object : ErrorDelegate() {
                override fun onError(error: String) {
                    Toast.makeText(this@ExoPlayerActivity, error, Toast.LENGTH_SHORT).show()
                }
            }

            binding.chatWidget.widgetView.allowWidgetSwipeToDismiss = true
            if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                binding.chatWidget.widgetView.removeSession()
                binding.chatWidget.widgetView.setSession(session)
            } else {
                binding.chatWidget.widgetView.removeSession()
                binding.chatWidget.widgetView.setSession(session)
            }

            //binding.chatWidget.widgetView.setSession(session)
            binding.chatWidget.widgetView.widgetLifeCycleEventsListener =
                object : WidgetLifeCycleEventsListener() {
                    override fun onWidgetStateChange(
                        state: WidgetStates,
                        widgetData: LiveLikeWidgetEntity
                    ) {
                    }

                    override fun onUserInteract(widgetData: LiveLikeWidgetEntity) {
                        println("onUser Interact :$widgetData")
                    }

                    override fun onWidgetPresented(widgetData: LiveLikeWidgetEntity) {
                        val widgetDataJson = GsonBuilder().create().toJson(widgetData)
                        addLogs("onWidgetPresented : $widgetDataJson")
                        playThemeRandomizer()
                    }

                    override fun onWidgetInteractionCompleted(widgetData: LiveLikeWidgetEntity) {
                        val widgetDataJson = GsonBuilder().create().toJson(widgetData)
                        addLogs("onWidgetInteractionCompleted : $widgetDataJson")
                    }

                    override fun onWidgetDismissed(widgetData: LiveLikeWidgetEntity) {
                        val widgetDataJson = GsonBuilder().create().toJson(widgetData)
                        addLogs("onWidgetDismissed : $widgetDataJson")
                        stopThemeRandomizer()
                    }
                }
            this.session = session
            lifecycleScope.launch {
                session.widgetFlow.collect { liveLikeWidget ->
                    if (myWidgetsList.find { it.id == liveLikeWidget.id } == null) {
                        myWidgetsList.add(0, liveLikeWidget)
                        getSharedPreferences(PREFERENCES_APP_ID, Context.MODE_PRIVATE)
                            .edit().putString(PREF_MY_WIDGETS, Gson().toJson(myWidgetsList)).apply()
                    }
                }
            }
            player?.playMedia(Uri.parse(channel.video.toString()), startingState ?: PlayerState())
        }

        if (ThemeRandomizer.themesList.size > 0) {
            binding.chatWidget.widgetView.applyTheme(ThemeRandomizer.themesList.last())
        }


        val avatarUrl = intent.getStringExtra("avatarUrl")
        if (session != null) {
            session?.chatSession?.avatarUrl = avatarUrl
            binding.chatWidget.txtChatRoomId.visibility = View.INVISIBLE
            binding.chatWidget.txtChatRoomTitle.visibility = View.INVISIBLE
            binding.chatWidget.chatView.shouldDisplayAvatar = showChatAvatar
            binding.chatWidget.chatView.enableChatMessageURLs = showLink
            if (showLink) {
                binding.chatWidget.chatView.chatMessageUrlPatterns = customLink
            }

            if (showImagePicker) {

                binding.chatWidget.chatView.chatImagePickerDelegate =
                    object : ChatImagePickerDelegate {
                        override fun onImageButtonClicked() {
                            openPickerDialog()
                        }

                        val pickMedia =
                            registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri: Uri? ->
                                if (uri != null) {
                                    logDebug { "pickMedia : $uri" }
                                    binding.chatWidget.chatView.chatImagePickerListener.onImagePick(
                                        uri
                                    )
                                } else {
                                    logError { "No media Selected" }
                                }
                            }

                        private fun openPickerDialog() {
                            val builder = AlertDialog.Builder(this@ExoPlayerActivity)
                            builder.setView(R.layout.demo_bottomsheet_imagepicker)
                            val dialog = builder.create()
                            dialog.window?.decorView?.setBackgroundResource(com.livelike.engagementsdk.R.drawable.dialog_background) // setting the background
                            dialog.show()
                            dialog.findViewById<Button>(R.id.demo_photo_tv)?.setOnClickListener {
                                dialog.dismiss()
                                pickMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
                            }
                        }
                    }
            }

            binding.chatWidget.chatView.enableQuoteMessage = enableReplies
            binding.chatWidget.chatView.hideDeletedMessage = hideDeletedMessage
            binding.chatWidget.chatView.chatViewDelegate = ExoPlayerChatViewDelegate()
            var lastMessageTime: String? = null

            chatMessageListener = object : MessageListener {
                var isFistTimeDone = false
                override fun onNewMessage(message: LiveLikeChatMessage) {

                }

                override fun onHistoryMessage(messages: List<LiveLikeChatMessage>) {
                    if (!isFistTimeDone) {
                        if (messages.isNotEmpty()) {
                            lastMessageTime = messages.last().createdAt
                            isFistTimeDone = true
                        }

                    }
                }

                override fun onDeleteMessage(messageId: String) {

                }

                override fun onPinMessage(message: PinMessageInfo) {

                }

                override fun onUnPinMessage(pinMessageId: String) {

                }

                override fun onErrorMessage(error: String, clientMessageId: String?) {

                }
            }
            session?.chatSession?.setMessageListener(chatMessageListener)
            binding.chatWidget.chatView.chatViewSeparatorContentDelegate =
                object : ChatViewSeparatorContentDelegate {
                    override fun getSeparatorView(
                        messageTop: LiveLikeChatMessage?,
                        messageBottom: LiveLikeChatMessage?
                    ): View? {
                        var separatorViewBinding: DemoChatDateViewBinding? =
                            DemoChatDateViewBinding.inflate(
                                LayoutInflater.from(this@ExoPlayerActivity),
                                null,
                                false
                            )

                        val today = ZonedDateTime.now()
                        val yesterday = today.minusDays(1)
                        val b = messageBottom?.createdAt?.parseISODateTime()
                        val mBottom = if (b != null) ZonedDateTime.ofInstant(
                            b.toInstant(),
                            ZoneId.systemDefault()
                        )?.dayOfMonth
                        else null
                        val t = messageTop?.createdAt?.parseISODateTime()
                        val mTop = if (t != null)
                            ZonedDateTime.ofInstant(
                                t.toInstant(),
                                ZoneId.systemDefault()
                            )?.dayOfMonth
                        else null
                        //println("ExoPlayerActivity.getSeparatorView>>$mBottom >> $mTop >> ${today.dayOfMonth} >>${yesterday.dayOfMonth} >> ${messageTop?.message} >> ${messageBottom?.message}")
                        if (messageTop == null) {
                            separatorViewBinding?.txtDate?.text = when {
                                today.dayOfMonth == mBottom -> "Today"
                                yesterday.dayOfMonth == mBottom -> "Yesterday"
                                else -> messageBottom?.createdAt?.parseISODateTime()
                                    ?.toLocalDateTime()?.format(
                                        DateTimeFormatter.ISO_LOCAL_DATE
                                    )
                            }
                        } else if (mTop == mBottom) {
                            separatorViewBinding = null
                        } else {
                            when {
                                today.dayOfMonth == mBottom -> separatorViewBinding?.txtDate?.text =
                                    "Today"

                                yesterday.dayOfMonth == mBottom -> separatorViewBinding?.txtDate?.text =
                                    "Yesterday"

                                messageBottom?.createdAt != null -> separatorViewBinding?.txtDate?.text =
                                    messageBottom?.createdAt?.parseISODateTime()?.format(
                                        DateTimeFormatter.ISO_LOCAL_DATE
                                    )

                                else -> {
                                    separatorViewBinding = null
                                }
                            }
                        }
                        if (lastMessageTime != null) {
                            if (messageBottom?.createdAt?.parseISODateTime()?.toLocalDateTime()
                                    ?.isAfter(
                                        lastMessageTime?.parseISODateTime()?.toLocalDateTime()
                                    ) == true
                            ) {
                                if (messageTop?.createdAt?.parseISODateTime()?.toLocalDateTime()
                                        ?.isAfter(
                                            (lastMessageTime?.parseISODateTime()?.toLocalDateTime())
                                        ) != true
                                ) {
                                    val separatorViewBinding2 = DemoChatDateViewBinding.inflate(
                                        LayoutInflater.from(this@ExoPlayerActivity),
                                        null,
                                        false
                                    )
                                    separatorViewBinding2.txtDate.text = "New Messages"
                                    val linearLayout = LinearLayout(this@ExoPlayerActivity)
                                    linearLayout.orientation = LinearLayout.VERTICAL
                                    separatorViewBinding?.let {
                                        linearLayout.addView(
                                            separatorViewBinding.root
                                        )
                                    }
                                    linearLayout.addView(separatorViewBinding2.root)
                                    return linearLayout
                                }
                            }
                        }
                        return separatorViewBinding?.root
                    }
                }
            binding.chatWidget.chatView.enableDeleteMessage = allowDelete
//            binding.chatWidget.chatView.deleteDialogInterceptor = null
            binding.chatWidget.chatView.setSession(session!!.chatSession)
            binding.chatWidget.chatView.supportImagePicker = showPicker
//            binding.chatWidget.chatView.chatInterceptor = object : ChatInterceptor {
//                override fun invoke(p1: LiveLikeChatMessage): Boolean {
//                    return kotlin.random.Random.nextBoolean()
//                }
//            }

            binding.chatWidget.chatView.findViewById<RecyclerView>(R.id.chatdisplay).isVerticalScrollBarEnabled =
                true

            (application as LiveLikeApplication).sdk.chat().chatRoomDelegate =
                object : ChatRoomDelegate() {
                    override fun onNewChatRoomAdded(chatRoomAdd: ChatRoomAdd) {
                        println("chatRoomAdd = [${chatRoomAdd}]")
                    }

                    override fun onReceiveInvitation(invitation: ChatRoomInvitation) {
                        println("invitation = [${invitation}]")
                    }

                    override fun onBlockProfile(blockedInfo: BlockedInfo) {
                        println("blockedInfo = [${blockedInfo}]")
                    }

                    override fun onUnBlockProfile(blockInfoId: String, blockProfileId: String) {
                        println("blockInfoId = [${blockInfoId}], blockProfileId = [${blockProfileId}]")
                    }
                }

        }

        player?.playMedia(Uri.parse(channel.video.toString()), startingState ?: PlayerState())
    }

    private fun stopThemeRandomizer() {
        themeRadomizerHandler.removeCallbacksAndMessages(null)
    }

    private fun playThemeRandomizer() {
        themeRadomizerHandler.postDelayed(
            {
                ThemeRandomizer.nextTheme()?.let {
                    binding.chatWidget.widgetView?.applyTheme(it)
                }
                playThemeRandomizer()
            },
            5000
        )
    }

    private fun addLogs(logs: String?) {
        binding.logsPreview.text = "$logs \n\n ${binding.logsPreview.text}"
        binding.fullLogs.text = "$logs \n\n ${binding.fullLogs.text}"
    }


    override fun onStart() {
        super.onStart()
        if (!adsPlaying)
            player?.start()
    }

    override fun onStop() {
        super.onStop()
        player?.stop()
    }

    override fun onDestroy() {
        (application as LiveLikeApplication).player = null
        timer.cancel()
        timer.purge()
        player?.release()
        session?.chatSession?.setMessageListener(null)
        session?.widgetInterceptor = null
        window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        (applicationContext as LiveLikeApplication).sdk.userProfileDelegate = null
        binding.chatWidget.widgetView?.widgetLifeCycleEventsListener = null
        timer.cancel()
        unregisterLogsHandler()
        binding.chatWidget.chatView?.clearSession()
        super.onDestroy()
    }

    override fun onPause() {
        session?.pause()
        player?.stop()
        super.onPause()
    }

    override fun onResume() {
        session?.resume()
        if (!adsPlaying) {
            player?.start()
        }
        super.onResume()
    }

    @SuppressLint("MissingSuperCall")
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(CHANNEL_NAME, channelManager?.selectedChannel?.name ?: "")
        outState.putBoolean(AD_STATE, adsPlaying)
        outState.putBoolean(SHOWING_DIALOG, showingDialog)
        outState.putLong(POSITION, player?.position() ?: 0)
    }

    companion object {
        const val AD_STATE = "adstate"
        const val SHOWING_DIALOG = "showingDialog"
        const val POSITION = "position"
        const val CHANNEL_NAME = "channelName"
        var privateGroupRoomId: String? = null
        var isHideChatInput: Boolean = false
    }
}

class ExoPlayerChatViewDelegate : ChatViewDelegate {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: ChatMessageType
    ): RecyclerView.ViewHolder {
        return ExoPlayerActivity.MyCustomMsgViewHolder(
            CustomMsgItemBinding.inflate(
                LayoutInflater.from(
                    parent.context
                ), parent, false
            )
        )
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        liveLikeChatMessage: LiveLikeChatMessage,
        chatViewThemeAttributes: ChatViewThemeAttributes,
        showChatAvatar: Boolean
    ) {
        println("ExoPlayerActivity.onBindView>> ${holder is ExoPlayerActivity.MyCustomMsgViewHolder}")
        /*
        chatViewThemeAttributes.chatBubbleBackgroundRes?.let {
            if (it < 0) {
                holder.itemView.lay_msg_back.setBackgroundColor(it)
            } else {
                val value = TypedValue()
                try {
                    holder.itemView.lay_msg_back.context.resources.getValue(
                        it,
                        value,
                        true
                    )
                    when (value.type) {
                        TypedValue.TYPE_REFERENCE, TypedValue.TYPE_STRING -> holder.itemView.lay_msg_back.setBackgroundResource(
                            it
                        )
                        TypedValue.TYPE_NULL -> holder.itemView.lay_msg_back.setBackgroundColor(
                            Color.TRANSPARENT
                        )
                        else -> holder.itemView.lay_msg_back.setBackgroundColor(Color.TRANSPARENT)
                    }
                } catch (e: Resources.NotFoundException) {
                    holder.itemView.lay_msg_back.setBackgroundColor(it)
                }
            }
        }
        */
        if (holder is ExoPlayerActivity.MyCustomMsgViewHolder) {
            if (showChatAvatar) {
                holder.itemBinding.imgSenderMsg.visibility = View.VISIBLE
                Glide.with(holder.itemView.context)
                    .load(liveLikeChatMessage.profilePic)
                    .error(R.drawable.coin)
                    .placeholder(R.drawable.coin)
                    .into(holder.itemBinding.imgSenderMsg)
            } else {
                holder.itemBinding.imgSenderMsg.visibility = View.GONE
            }
            holder.itemBinding.txtMsg.text = liveLikeChatMessage.customData
            holder.itemBinding.txtMsgSenderName.text = liveLikeChatMessage.nickname
            holder.itemBinding.txtMsgTime.text = liveLikeChatMessage.timestamp
            if (liveLikeChatMessage.isDeleted) {
                holder.itemBinding.txtMsg.text = "This msg is deleted"
                holder.itemBinding.txtMsg.setTypeface(null, Typeface.ITALIC)
            } else {
                holder.itemBinding.txtMsg.setTypeface(null, Typeface.NORMAL)
            }
        }
    }
}
