package com.livelike.livelikedemo.reaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.reaction
import com.livelike.livelikedemo.LiveLikeApplication
import com.livelike.livelikedemo.R
import com.livelike.livelikedemo.databinding.FragmentReactionHomeBinding
import com.livelike.reaction.CreateReactionSpaceRequest
import com.livelike.reaction.GetReactionSpaceRequest
import com.livelike.reaction.UpdateReactionSpaceRequest


class ReactionHomeFragment : Fragment() {

    private lateinit var binding: FragmentReactionHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentReactionHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.editTextTextPersonName6.setText("ce5f3f8b-ca5b-4157-8c62-f76fda5a3b83")

        binding.button7.setOnClickListener {
            val name = binding.editTextTextPersonName4.text.toString().trim()
            val targetGroupId = binding.editTextTextPersonName5.text.toString().trim()
            val reactionPack = binding.editTextTextPersonName6.text.toString().trim().split(",")
            if (targetGroupId.isEmpty()) {
                Toast.makeText(activity, "Target Group Id cannot be empty", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }
            if (reactionPack.isEmpty()) {
                Toast.makeText(activity, "Reaction Pack cannot be empty", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }
            (activity?.application as? LiveLikeApplication)?.sdk?.reaction()?.createReactionSpace(
                CreateReactionSpaceRequest(
                    name,
                    targetGroupId,
                    reactionsPackIds = reactionPack
                ),
                liveLikeCallback = { result, error ->
                    result?.let {
                        binding.textView12.text = it.toString()
                        binding.editTextTextPersonName7.setText(it.id.toString())
                    }
                    error?.let { binding.textView12.text = it }
                }
            )
        }

        binding.button8.setOnClickListener {
            val targetGroupId = binding.editTextTextPersonName5.text.toString().trim()
            val reactionPack = binding.editTextTextPersonName6.text.toString().trim().split(",")
            val reactionSpaceId = binding.editTextTextPersonName7.text.toString().trim()
            if (targetGroupId.isEmpty()) {
                Toast.makeText(activity, "Target Group Id cannot be empty", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }
            if (reactionPack.isEmpty()) {
                Toast.makeText(activity, "Reaction Pack cannot be empty", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }
            (activity?.application as? LiveLikeApplication)?.sdk?.reaction()?.updateReactionSpace(
                UpdateReactionSpaceRequest(
                    reactionSpaceId,
                    reactionsPackIds = reactionPack
                ),
                liveLikeCallback = { result, error ->
                    result?.let { binding.textView12.text = it.toString() }
                    error?.let { binding.textView12.text = it }
                }
            )
        }

        binding.button9.setOnClickListener {
            val reactionSpaceId = binding.editTextTextPersonName7.text.toString().trim()
            if (reactionSpaceId.isEmpty()) {
                Toast.makeText(activity, "Reaction Space Id cannot be empty", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }
            (activity?.application as? LiveLikeApplication)?.sdk?.reaction()?.deleteReactionSpace(
                reactionSpaceId,
                liveLikeCallback = { result, error ->
                    result?.let { binding.textView12.text = "Success" }
                    error?.let { binding.textView12.text = it }
                }
            )
        }

        binding.button10.setOnClickListener {
            val reactionSpaceId = binding.editTextTextPersonName7.text.toString()
            if (reactionSpaceId.isEmpty()) {
                Toast.makeText(activity, "Reaction Space Id cannot be empty", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }
            println("ReactionHomeFragment.onViewCreated>>>")
            (activity?.application as? LiveLikeApplication)?.sdk?.reaction()
                ?.getReactionSpaceDetails(
                    reactionSpaceId,
                    liveLikeCallback = { result, error ->
                        result?.let { binding.textView12.text = it.toString() }
                        error?.let { binding.textView12.text = it }
                    }
                )
        }

        binding.button11.setOnClickListener {
            val targetGroupId = binding.editTextTextPersonName5.text.toString().trim()
            val reactionSpaceId = binding.editTextTextPersonName7.text.toString().trim()
            (activity?.application as? LiveLikeApplication)?.sdk?.reaction()?.getReactionSpaces(
                GetReactionSpaceRequest(
                    reactionSpaceId = reactionSpaceId,
                    targetGroupId = targetGroupId,
                    liveLikePagination = LiveLikePagination.FIRST,
                ),
                liveLikeCallback = { result, error ->
                    result?.let { binding.textView12.text = it.toString() }
                    error?.let { binding.textView12.text = it }
                }
            )
        }

        binding.button12.setOnClickListener {
            val targetGroupId = binding.editTextTextPersonName5.text.toString()
            val reactionSpaceId = binding.editTextTextPersonName7.text.toString()
            (activity?.application as? LiveLikeApplication)?.sdk?.reaction()?.getReactionSpaces(
                GetReactionSpaceRequest(
                    reactionSpaceId = reactionSpaceId,
                    targetGroupId = targetGroupId,
                    liveLikePagination = LiveLikePagination.NEXT
                ),
                liveLikeCallback = { result, error ->
                    result?.let { binding.textView12.text = it.toString() }
                    error?.let { binding.textView12.text = it }
                }
            )
        }

        binding.button13.setOnClickListener {
            val targetGroupId = binding.editTextTextPersonName5.text.toString()
            val reactionSpaceId = binding.editTextTextPersonName7.text.toString()

            (activity?.application as? LiveLikeApplication)?.sdk?.reaction()?.getReactionSpaces(
                GetReactionSpaceRequest(
                    reactionSpaceId = reactionSpaceId,
                    targetGroupId = targetGroupId,
                    liveLikePagination = LiveLikePagination.PREVIOUS
                ),
                liveLikeCallback = { result, error ->
                    result?.let { binding.textView12.text = it.toString() }
                    error?.let { binding.textView12.text = it }
                }
            )
        }

        binding.button14.setOnClickListener {
            val targetGroupId = binding.editTextTextPersonName5.text.toString()
            val reactionSpaceId = binding.editTextTextPersonName7.text.toString()
            if (targetGroupId.isEmpty() && reactionSpaceId.isEmpty()) {
                Toast.makeText(
                    context,
                    "Provide reaction Space Id or Target Group Id",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }
            parentFragmentManager.beginTransaction()
                .replace(
                    R.id.container,
                    ReactionFragment.newInstance(reactionSpaceId, targetGroupId)
                )
                .addToBackStack("reactionList")
                .commit()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = ReactionHomeFragment()
    }
}