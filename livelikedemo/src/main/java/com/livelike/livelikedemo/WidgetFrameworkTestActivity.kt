package com.livelike.livelikedemo

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.JsonParser
import com.livelike.engagementsdk.core.services.messaging.proxies.LiveLikeWidgetEntity
import com.livelike.engagementsdk.core.services.messaging.proxies.WidgetLifeCycleEventsListener
import com.livelike.engagementsdk.fetchWidgetDetails
import com.livelike.engagementsdk.widget.data.models.CheerMeterUserInteraction
import com.livelike.engagementsdk.widget.data.models.EmojiSliderUserInteraction
import com.livelike.engagementsdk.widget.data.models.PollWidgetUserInteraction
import com.livelike.engagementsdk.widget.data.models.PredictionWidgetUserInteraction
import com.livelike.engagementsdk.widget.data.models.QuizWidgetUserInteraction
import com.livelike.engagementsdk.widget.data.models.WidgetKind
import com.livelike.engagementsdk.widget.viewModel.WidgetStates
import com.livelike.livelikedemo.databinding.ActivityWidgetFrameworkBinding
import com.livelike.livelikedemo.utils.ThemeRandomizer

//import kotlinx.android.synthetic.main.activity_widget_framework.*

class WidgetFrameworkTestActivity : AppCompatActivity() {
    private lateinit var binding: ActivityWidgetFrameworkBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWidgetFrameworkBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        /* var json =
             "{ \"id\": \"65301dde-17cc-4d13-95b9-989fb20842cb\", \"url\": \"https://cf-blast-staging.livelikecdn.com/api/v1/text-polls/65301dde-17cc-4d13-95b9-989fb20842cb/\", \"kind\": \"text-poll\", \"program_id\": \"b4dd284b-45bf-45c7-ba95-10416e370cea\", \"created_at\": \"2020-05-01T07:21:19.578Z\", \"published_at\": null, \"scheduled_at\": null, \"question\": \"sas\", \"timeout\": \"P0DT00H00M25S\", \"options\": [{ \"id\": \"ea859e7b-1c44-4b0c-aeef-154efe9d2af9\", \"description\": \"sas\", \"vote_count\": 0, \"vote_url\": \"https://blastrt-staging-us-east-1.livelikecdn.com/services/interactions/text-poll/65301dde-17cc-4d13-95b9-989fb20842cb/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcHRpb25zIjpbImVhODU5ZTdiLTFjNDQtNGIwYy1hZWVmLTE1NGVmZTlkMmFmOSIsImU5ZDBhZTk2LWJmODItNDkzOC1hZTMwLWFkMWM2NDMzMTQ4NiJdLCJvcHRpb25fdXVpZCI6ImVhODU5ZTdiLTFjNDQtNGIwYy1hZWVmLTE1NGVmZTlkMmFmOSIsInJld2FyZHMiOltdLCJpc3MiOiJibGFzdCIsImlhdCI6MTU4ODMxNzY4MX0.OpEoVL4hg6cmTnKX_Xme5FELkd5LjgJY0Kq0k27RDMk\", \"translations\": {}, \"translatable_fields\": [\"description\"] }, { \"id\": \"e9d0ae96-bf82-4938-ae30-ad1c64331486\", \"description\": \"sas\", \"vote_count\": 0, \"vote_url\": \"https://blastrt-staging-us-east-1.livelikecdn.com/services/interactions/text-poll/65301dde-17cc-4d13-95b9-989fb20842cb/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcHRpb25zIjpbImVhODU5ZTdiLTFjNDQtNGIwYy1hZWVmLTE1NGVmZTlkMmFmOSIsImU5ZDBhZTk2LWJmODItNDkzOC1hZTMwLWFkMWM2NDMzMTQ4NiJdLCJvcHRpb25fdXVpZCI6ImU5ZDBhZTk2LWJmODItNDkzOC1hZTMwLWFkMWM2NDMzMTQ4NiIsInJld2FyZHMiOltdLCJpc3MiOiJibGFzdCIsImlhdCI6MTU4ODMxNzY4MX0.bJIRnWgmrqZ0LP0sXFNTR1tTjJI1zdbpkKpr6s3B2ZU\", \"translations\": {}, \"translatable_fields\": [\"description\"] }], \"subscribe_channel\": \"widget.text-poll.65301dde-17cc-4d13-95b9-989fb20842cb\", \"program_date_time\": \"2020-05-01T07:21:03.396000Z\", \"publish_delay\": \"P0DT00H00M00S\", \"interaction_url\": \"https://cf-blast-staging.livelikecdn.com/api/v1/text-polls/65301dde-17cc-4d13-95b9-989fb20842cb/interactions/\", \"impression_url\": \"https://s93qlqrqw5.execute-api.us-east-1.amazonaws.com/services/impressions/text-poll/65301dde-17cc-4d13-95b9-989fb20842cb\", \"impression_count\": 0, \"unique_impression_count\": 0, \"engagement_count\": 0, \"engagement_percent\": \"0.000\", \"status\": \"pending\", \"created_by\": { \"id\": \"0cdffb74-e382-41f2-bb81-a5a9dba67888\", \"name\": \"Shivansh Mittal\", \"image_url\": \"https://cf-blast-storage-staging.livelikecdn.com/assets/e1ad9075-b59d-4825-9d23-17cdfb351a9a.png\" }, \"schedule_url\": \"https://cf-blast-staging.livelikecdn.com/api/v1/text-polls/65301dde-17cc-4d13-95b9-989fb20842cb/schedule/\", \"rewards_url\": null, \"translations\": {}, \"translatable_fields\": [\"question\"], \"reactions\": [], \"custom_data\": null }"
 */
        var json = "{\"id\":\"e4c17908-070c-4f40-9e9e-a903cf8d2256\",\"pubnub_enabled\":true,\"url\":\"https://cf-blast.livelikecdn.com/api/v1/emoji-sliders/e4c17908-070c-4f40-9e9e-a903cf8d2256/\",\"kind\":\"emoji-slider\",\"program_id\":\"e1323b28-3d90-47fa-9da7-22540493516f\",\"client_id\":\"8PqSNDgIVHnXuJuGte1HdvOjOqhCFE1ZCR3qhqaS\",\"created_at\":\"2023-12-14T07:36:35.191185Z\",\"published_at\":\"2023-12-14T07:36:41.314736Z\",\"scheduled_at\":\"2023-12-14T07:36:41.162215Z\",\"interactive_until\":null,\"question\":\"How good does this dish look?\",\"timeout\":\"P0DT00H02M30S\",\"options\":[{\"id\":\"69eef12b-c57b-4281-82a3-392321671d24\",\"image_url\":\"https://d2gcqbfjyyxb8n.cloudfront.net/image1701173559078Neutral.png\",\"translations\":{},\"translatable_fields\":[]},{\"id\":\"7af8384d-1de0-4f40-8cb7-97fc038b58b0\",\"image_url\":\"https://d2gcqbfjyyxb8n.cloudfront.net/image1701173559078Neutral.png\",\"translations\":{},\"translatable_fields\":[]},{\"id\":\"14958bd9-0841-4466-8a0d-357660345af0\",\"image_url\":\"https://cf-blast-storage.livelikecdn.com/assets/db5209fa-108a-4a46-84bb-3218c5f65b73.png\",\"translations\":{},\"translatable_fields\":[]}],\"subscribe_channel\":\"program.widget.e1323b28-3d90-47fa-9da7-22540493516f\",\"program_date_time\":null,\"initial_magnitude\":\"0.500\",\"average_magnitude\":\"0.500\",\"vote_url\":\"https://blast-prod-us-east-1.livelikeblast.com/api/v1/widget-interactions/emoji-slider/e4c17908-070c-4f40-9e9e-a903cf8d2256/votes/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbmNyeXB0ZWQiOiJnQUFBQUFCbGVyRVg1eHpnamlHR3lWS09XcmNCMFJHeGZnV2tTNmt1QUVUOU90bkNvNEhCN0dxNmNobDRCWHE0Zkh6RjMxNFNybkJ0ZmxoeWt0eEM2T19JV2U0ZjVvR1ZJdz09IiwiaXNzIjoiYmxhc3QiLCJpYXQiOjE3MDI1Mzk1NDN9.8X9loE36XYRAbE4u850YR3cTUCgFSxOgu8ZfvGOXthU/\",\"publish_delay\":\"P0DT00H00M00S\",\"interaction_url\":\"https://cf-blast.livelikecdn.com/api/v1/emoji-sliders/e4c17908-070c-4f40-9e9e-a903cf8d2256/interactions/\",\"impression_url\":\"https://blast-prod-us-east-1.livelikeblast.com/api/v1/widget-impressions/emoji-slider/e4c17908-070c-4f40-9e9e-a903cf8d2256/\",\"impression_count\":0,\"unique_impression_count\":0,\"engagement_count\":0,\"engagement_percent\":\"0.000\",\"status\":\"published\",\"created_by\":{\"id\":\"9a5de1a5-50e9-41fb-b979-135df170a06b\",\"name\":null,\"image_url\":null},\"schedule_url\":\"https://cf-blast.livelikecdn.com/api/v1/emoji-sliders/e4c17908-070c-4f40-9e9e-a903cf8d2256/schedule/\",\"rewards_url\":null,\"translations\":{},\"translatable_fields\":[\"question\"],\"reactions\":[],\"custom_data\":null,\"sponsors\":[],\"earnable_rewards\":[{\"reward_item_id\":\"5e1016ef-9cf8-450f-9ad6-5e3996f1cbb0\",\"reward_item_name\":\"LiveLike Points\",\"reward_action_key\":\"poll-voted\",\"reward_item_amount\":5}],\"widget_interactions_url_template\":\"https://cf-blast.livelikecdn.com/api/v1/profiles/{profile_id}/widget-interactions/?emoji_slider_id=e4c17908-070c-4f40-9e9e-a903cf8d2256\",\"widget_attributes\":[],\"playback_time_ms\":50}"
        binding.inputWidgetJson.setText(json)

        val channelManager = (application as LiveLikeApplication).channelManager
        val channel = channelManager.selectedChannel
        val session = (application as LiveLikeApplication).createPublicSession(
            channel.llProgram.toString(),
            null,
        )
        binding.showMyWidget.setOnClickListener {
//            session.getPublishedWidgets(
//                LiveLikePagination.FIRST,
//                object : LiveLikeCallback<List<LiveLikeWidget>>() {
//                    override fun onResponse(
//                        result: List<LiveLikeWidget>?,
//                        error: String?
//                    ) {
//                        error?.let {
//                            Toast.makeText(applicationContext, "$it", Toast.LENGTH_SHORT).show()
//                        }
//                        result?.map { it!! }?.let { list ->
//                            DialogUtils.showMyWidgetsDialog(
//                                this@WidgetFrameworkTestActivity,
//                                (application as LiveLikeApplication).sdk,
//                                ArrayList(list),
//                                object : LiveLikeCallback<LiveLikeWidget>() {
//                                    override fun onResponse(
//                                        result: LiveLikeWidget?,
//                                        error: String?
//                                    ) {
//                                        result?.let {
//                                            widget_view.displayWidget(
//                                                (application as LiveLikeApplication).sdk,
//                                                result
//                                            )
//                                        }
//                                    }
//                                }
//                            )
//                        }
//                    }
//                }
//            )
            (application as LiveLikeApplication).sdk.fetchWidgetDetails(
                "4d034738-9e62-4a5e-8547-4c560f034ea2",
                "text-prediction"
            ) { result, error ->
                result?.let {
                    binding.widgetView.displayWidget(
                        (application as LiveLikeApplication).sdk,
                        result, showWithInteractionData = true
                    )
                    binding.widgetView.postDelayed({
                        binding.widgetView.setState(WidgetStates.RESULTS)
                    }, 5000)
                }
                error?.let {
                    Toast.makeText(applicationContext, it, Toast.LENGTH_SHORT).show()
                }
            }
        }
        binding.showWidget.setOnClickListener {
            try {
                val jsonObject =
                    JsonParser.parseString(binding.inputWidgetJson.text.toString()).asJsonObject
                binding.widgetView.displayWidget(
                    (application as LiveLikeApplication).sdk,
                    jsonObject
                )
            } catch (_: Exception) {
                Toast.makeText(this, "Invalid json", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
        }

        binding.edWidgetId.setText("59f34f87-4ae4-4df3-97e8-75b9929f3821")
        binding.edWidgetKind.setText("text-poll")

        binding.moveToNextState.setOnClickListener {
            binding.widgetView.moveToNextState()
        }
        binding.widgetView.enableDefaultWidgetTransition = false
        binding.widgetView.widgetLifeCycleEventsListener =
            object : WidgetLifeCycleEventsListener() {
                override fun onWidgetPresented(widgetData: LiveLikeWidgetEntity) {
                }

                override fun onWidgetInteractionCompleted(widgetData: LiveLikeWidgetEntity) {
                }

                override fun onWidgetDismissed(widgetData: LiveLikeWidgetEntity) {
                }

                override fun onWidgetStateChange(
                    state: WidgetStates,
                    widgetData: LiveLikeWidgetEntity
                ) {

                }


                override fun onUserInteract(widgetData: LiveLikeWidgetEntity) {

                }
            }

        binding.radioReady.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.widgetView.setState(WidgetStates.READY)
            }
        }
        binding.radioInteraction.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.widgetView.setState(WidgetStates.INTERACTING)
            }
        }
        binding.radioResult.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.widgetView.setState(WidgetStates.RESULTS)
            }
        }
        binding.radioFinished.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.widgetView.setState(WidgetStates.FINISHED)
            }
        }
        if (ThemeRandomizer.themesList.size > 0) {
            binding.widgetView.applyTheme(ThemeRandomizer.themesList.last())
        }
        binding.btnFetch.setOnClickListener {
            val id = binding.edWidgetId.text.toString()
            val kind = binding.edWidgetKind.text.toString()
            (application as LiveLikeApplication).sdk.let { sdk ->
                if (id.isNotEmpty() && kind.isNotEmpty()) {
                    sdk.fetchWidgetDetails(id, kind) { result, error ->
                        result?.let { widget ->
                            widget.id?.let { widgetId ->
                                widget.kind?.let { widgetKind ->
                                    session.getWidgetInteraction(
                                        widgetId = widgetId,
                                        widgetKind = widgetKind,
                                        widgetInteractionUrl = widget.widgetInteractionUrl!!
                                    ) { result, error ->
                                        result?.let {
                                            binding.txtResult.text = it.let {
                                                when (it) {
                                                    is PredictionWidgetUserInteraction -> "User Correct: ${it.isCorrect}"
                                                    is PollWidgetUserInteraction -> "User Selected OptionId :${it.optionId}"
                                                    is QuizWidgetUserInteraction -> "User Selected: ${it.choiceId}"
                                                    is EmojiSliderUserInteraction -> "User data: ${it.magnitude}"
                                                    is CheerMeterUserInteraction -> "Total Score: ${it.totalScore}"
                                                    else -> "No Response"
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            binding.widgetViewFetch.enableDefaultWidgetTransition = false
                            binding.widgetViewFetch.setState(WidgetStates.RESULTS)

                            binding.widgetViewFetch.displayWidget(
                                sdk,
                                widget,
                                showWithInteractionData = true
                            )

                        }
                        error?.let {
                            Toast.makeText(applicationContext, it, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                } else if (kind.isNotEmpty()) {
                    session.getWidgetInteractions(
                        widgetKinds = kind.split(",")
                    ) { result, error ->
                        result?.let { map ->
                            binding.txtResult.text = map.entries.joinToString { entry ->
                                "${entry.key} : ${
                                    entry.value.joinToString {
                                        when (it) {
                                            is PredictionWidgetUserInteraction -> "User Correct: ${it.isCorrect}"
                                            is PollWidgetUserInteraction -> "User Selected OptionId :${it.optionId}"
                                            is QuizWidgetUserInteraction -> "User Selected: ${it.choiceId}"
                                            is EmojiSliderUserInteraction -> "User data: ${it.magnitude}"
                                            is CheerMeterUserInteraction -> "Total Score: ${it.totalScore}"
                                            else -> "No Response"
                                        } + "\n"
                                    }
                                }\n"
                            }
                        }
                        error?.let {
                            Toast.makeText(applicationContext, it, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }
            }
        }
    }
}
