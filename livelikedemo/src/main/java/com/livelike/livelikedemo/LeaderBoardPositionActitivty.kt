package com.livelike.livelikedemo

import android.os.Build
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.livelike.engagementsdk.LiveLikeContentSession
import com.livelike.engagementsdk.core.data.models.LeaderBoardForClient
import com.livelike.engagementsdk.core.data.models.LeaderboardPlacement
import com.livelike.engagementsdk.leaderBoardDelegate
import com.livelike.engagementsdk.leaderboard
import com.livelike.engagementsdk.widget.domain.LeaderBoardDelegate
import com.livelike.livelikedemo.channel.ChannelManager
import com.livelike.livelikedemo.databinding.ActivityLeaderBoardPositionActitivtyBinding

class LeaderBoardPositionActitivty : AppCompatActivity() {

    private lateinit var session: LiveLikeContentSession
    private lateinit var channelManager: ChannelManager
    private lateinit var binding: ActivityLeaderBoardPositionActitivtyBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLeaderBoardPositionActitivtyBinding.inflate(layoutInflater)
        setContentView(binding.root)

        channelManager = (application as LiveLikeApplication).channelManager
        session = (application as LiveLikeApplication).createPublicSession(
            channelManager.selectedChannel.llProgram.toString(), allowTimeCodeGetter = false,
        )

        (application as LiveLikeApplication).sdk.leaderboard().getLeaderBoardsForProgram(
            channelManager.selectedChannel.llProgram.toString()
        ) { result, error ->
            result?.let { list ->
                val listOfLeaderBoardIds: ArrayList<String> = ArrayList()
                list.map {
                    listOfLeaderBoardIds.add(it.id)
                }
                (application as LiveLikeApplication).sdk.leaderboard().getLeaderboardClients(listOfLeaderBoardIds) { result, error ->
                    result?.let {
                        result
                    }
                    error?.let {
                    }
                }
                (applicationContext as LiveLikeApplication).sdk.leaderBoardDelegate =
                    object :
                        LeaderBoardDelegate {
                        override fun leaderBoard(
                            leaderBoard: LeaderBoardForClient,
                            currentUserPlacementDidChange: LeaderboardPlacement
                        ) {
                            runOnUiThread {
                                // Stuff that updates the UI
                                val textViewRow = TextView(this@LeaderBoardPositionActitivty)
                                textViewRow.setTextColor(
                                    resources.getColor(
                                        R.color.colorAccent,
                                        theme
                                    )
                                )
                                textViewRow.text =
                                    "\"LeaderBoardName\" + ${leaderBoard.name} + \"Rank: \"+ ${currentUserPlacementDidChange.rank}+ \"Percentile\"+ ${currentUserPlacementDidChange.rankPercentile} + \"Score \"+ ${currentUserPlacementDidChange.score}"
                                binding.leaderBoardLayout.addView(textViewRow)
                            }
                        }
                    }
            }
            error?.let {
            }
        }
    }
}
