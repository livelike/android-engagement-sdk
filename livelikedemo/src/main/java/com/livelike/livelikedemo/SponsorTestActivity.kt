package com.livelike.livelikedemo

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.sponsor
import com.livelike.engagementsdk.sponsorship.ISponsor
import com.livelike.livelikedemo.databinding.ActivitySponsorTestBinding


class SponsorTestActivity : AppCompatActivity() {

    private lateinit var sponsor: ISponsor
    private lateinit var binding: ActivitySponsorTestBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sponsor = (application as LiveLikeApplication).sdk.sponsor()

        binding = ActivitySponsorTestBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.programIdEt.setText("0fddc166-b8c3-4ce9-990e-848bde12188b")
        binding.editTextTextPersonName3.setText("b6bbcbd4-1d25-40bc-9cb5-c67cceb923d1")
        binding.fetchSponsor.setOnClickListener {
            if (binding.programIdEt.text.toString().isNotEmpty()) {
                binding.progressBar.visibility = View.VISIBLE
                sponsor.fetchByProgramId(
                    binding.programIdEt.text.toString(),
                    LiveLikePagination.FIRST
                ) { result, error ->
                    binding.progressBar.visibility = View.GONE
                    error?.let {
                        binding.sponsorResult.text = it
                    }
                    result?.let {
                        for (sponsor in it) {
                            binding.sponsorResult.text =
                                "${binding.sponsorResult.text} $sponsor \n "
                        }
                    }
                }
            }
        }
        binding.fetchSponsorProgramNext.setOnClickListener {
            if (binding.programIdEt.text.toString().isNotEmpty()) {
                binding.progressBar.visibility = View.VISIBLE
                sponsor.fetchByProgramId(
                    binding.programIdEt.text.toString(),
                    LiveLikePagination.NEXT
                ) { result, error ->
                    binding.progressBar.visibility = View.GONE
                    error?.let {
                        binding.sponsorResult.text = it
                    }
                    result?.let {
                        for (sponsor in it) {
                            binding.sponsorResult.text =
                                "${binding.sponsorResult.text} $sponsor \n "
                        }
                    }
                }
            }
        }

        binding.fetchSponsorProgramPrevious.setOnClickListener {
            if (binding.programIdEt.text.toString().isNotEmpty()) {
                binding.progressBar.visibility = View.VISIBLE
                sponsor.fetchByProgramId(
                    binding.programIdEt.text.toString(), LiveLikePagination.PREVIOUS
                ) { result, error ->
                    binding.progressBar.visibility = View.GONE
                    error?.let {
                        binding.sponsorResult.text = it
                    }
                    result?.let {
                        for (sponsor in it) {
                            binding.sponsorResult.text =
                                "${binding.sponsorResult.text} $sponsor \n "
                        }
                    }
                }
            }
        }

        binding.btPin.setOnClickListener {
            if (binding.editTextTextPersonName3.text.toString().isNotEmpty()) {
                binding.progressBar.visibility = View.VISIBLE
                sponsor.fetchByChatRoomId(
                    binding.editTextTextPersonName3.text.toString(),
                    LiveLikePagination.FIRST
                ) { result, error ->
                    binding.progressBar.visibility = View.GONE
                    result?.let {
                        for (sponsor in it) {
                            binding.textView9.text =
                                "${binding.textView9.text} $sponsor \n "
                        }
                    }
                    error?.let {
                        binding.textView9.text = it
                    }
                }
            }
        }

        binding.button5Next.setOnClickListener {
            if (binding.editTextTextPersonName3.text.toString().isNotEmpty()) {
                binding.progressBar.visibility = View.VISIBLE
                sponsor.fetchByChatRoomId(
                    binding.editTextTextPersonName3.text.toString(),
                    LiveLikePagination.NEXT
                ) { result, error ->
                    binding.progressBar.visibility = View.GONE
                    result?.let {
                        for (sponsor in it) {
                            binding.textView9.text =
                                "${binding.textView9.text} $sponsor \n "
                        }
                    }
                    error?.let {
                        binding.textView9.text = it
                    }
                }
            }
        }

        binding.button5Previous.setOnClickListener {
            if (binding.editTextTextPersonName3.text.toString().isNotEmpty()) {
                binding.progressBar.visibility = View.VISIBLE
                sponsor.fetchByChatRoomId(
                    binding.editTextTextPersonName3.text.toString(),
                    LiveLikePagination.PREVIOUS
                ) { result, error ->
                    binding.progressBar.visibility = View.GONE
                    result?.let {
                        for (sponsor in it) {
                            binding.textView9.text =
                                "${binding.textView9.text} $sponsor \n "
                        }
                    }
                    error?.let {
                        binding.textView9.text = it
                    }
                }
            }
        }

        binding.button6.setOnClickListener {
            binding.progressBar.visibility = View.VISIBLE
            sponsor.fetchForApplication(LiveLikePagination.FIRST) { result, error ->
                binding.progressBar.visibility = View.GONE
                result?.let {
                    for (sponsor in it) {
                        binding.textView10.text =
                            "${binding.textView10.text} $sponsor \n "
                    }
                }
                error?.let {
                    binding.textView10.text = it
                }
            }

        }
        binding.button6Next.setOnClickListener {
            binding.progressBar.visibility = View.VISIBLE
            sponsor.fetchForApplication(LiveLikePagination.NEXT) { result, error ->
                binding.progressBar.visibility = View.GONE
                result?.let {
                    for (sponsor in it) {
                        binding.textView10.text =
                            "${binding.textView10.text} $sponsor \n "
                    }
                }
                error?.let {
                    binding.textView10.text = it
                }
            }

        }
        binding.button6Previous.setOnClickListener {
            binding.progressBar.visibility = View.VISIBLE
            sponsor.fetchForApplication(LiveLikePagination.PREVIOUS) { result, error ->
                binding.progressBar.visibility = View.GONE
                result?.let {
                    for (sponsor in it) {
                        binding.textView10.text =
                            "${binding.textView10.text} $sponsor \n "
                    }
                }
                error?.let {
                    binding.textView10.text = it
                }
            }

        }
    }
}
