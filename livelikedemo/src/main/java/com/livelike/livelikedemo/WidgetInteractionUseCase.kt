package com.livelike.livelikedemo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.livelike.common.AccessTokenDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.engagementsdk.ContentSession
import com.livelike.engagementsdk.EpochTime
import com.livelike.engagementsdk.createContentSession
import com.livelike.engagementsdk.fetchWidgetDetails
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.engagementsdk.widget.viewModel.CheerMeterViewModel
import com.livelike.engagementsdk.widget.widgetModel.PollWidgetModel
import kotlinx.coroutines.Dispatchers

class WidgetInteractionUseCase : AppCompatActivity() {
    lateinit var contentSession: ContentSession
    private val _livelike = MutableLiveData<LiveLikeKotlin>()
    val liveLike: LiveData<LiveLikeKotlin>
        get() = _livelike
    val viewModel = MutableLiveData<PollWidgetModel>()
    val cheerMeterViewModel = MutableLiveData<CheerMeterViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_widget_interaction_use_case)


        _livelike.value = LiveLikeKotlin(
            clientId = "8PqSNDgIVHnXuJuGte1HdvOjOqhCFE1ZCR3qhqaS",
            accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return null
                }

                override fun storeAccessToken(accessToken: String?) {
                }

            },
            errorDelegate = object : ErrorDelegate() {
                override fun onError(error: String) {
                    TODO("Not yet implemented")
                }
            }
            //uiDispatcher = Dispatchers.Default
        )


        fun fetchWidget() {
            contentSession = liveLike.value?.createContentSession(
                programId = "09d93835-ee52-4757-976c-ea09d6a5798c",
                timecodeGetter = object : LiveLikeKotlin.TimecodeGetterCore {
                    override fun getTimecode(): EpochTime {
                        return EpochTime(0)
                    }

                },
                isPlatformLocalContentImageUrl = { false },
                uiDispatcher = Dispatchers.Default
            ) as ContentSession

            liveLike.value?.fetchWidgetDetails(
                "b6d08e72-00d3-4827-be66-46ce5cc5d411",
                "text-poll"
            ) { result, error ->
                result?.let {
                    viewModel.value =
                        contentSession.getWidgetModelFromLiveLikeWidget(it) as PollWidgetModel
//                    viewModel.value!!.getUserInteraction()
                }
                error?.let {
                    println("error---$it")
                }
            }

            liveLike.value?.fetchWidgetDetails(
                "4efbb87b-afef-45c8-a767-25efaf5e0256",
                "cheer-meter"
            ) { result, error ->
                result?.let {
                    cheerMeterViewModel.value =
                        contentSession.getWidgetModelFromLiveLikeWidget(it) as CheerMeterViewModel
                }
                error?.let {
                    println("error---" + it)
                }
            }
        }
    }
}