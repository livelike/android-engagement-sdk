package com.livelike.livelikedemo

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.livelike.engagementsdk.LiveLikeContentSession
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.livelikedemo.adapters.UnclaimedInteractionAdapter
import com.livelike.livelikedemo.databinding.ActivityUnclaimedInteractionBinding

class UnclaimedInteractionActivity : AppCompatActivity() {

    private var session: LiveLikeContentSession? = null
    var adapter: UnclaimedInteractionAdapter? = null
    private lateinit var binding: ActivityUnclaimedInteractionBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityUnclaimedInteractionBinding.inflate(layoutInflater)
        setContentView(binding.root)


        session =
            (application as LiveLikeApplication).createPublicSession((application as LiveLikeApplication).channelManager.selectedChannel.llProgram.toString())

        binding.unclaimedBtn.setOnClickListener { fetchUnclaimedInteractions() }
    }

    private fun fetchUnclaimedInteractions() {
        binding.progressBar.visibility = View.VISIBLE
        session?.getWidgetInteractionsWithUnclaimedRewards(LiveLikePagination.FIRST) { result, error ->
            binding.progressBar.visibility = View.GONE
            result?.let {
                Log.d("unclaimed", "interaction list loaded-> ${result.size}")
                // set adapter
                adapter = UnclaimedInteractionAdapter(
                    this@UnclaimedInteractionActivity,
                    (application as LiveLikeApplication).sdk, result
                )
                binding.claimRv.layoutManager =
                    LinearLayoutManager(this@UnclaimedInteractionActivity)
                binding.claimRv.adapter = adapter
            }
        }
    }
}
