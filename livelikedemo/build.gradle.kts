@Suppress("DSL_SCOPE_VIOLATION")

plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.detekt)
    alias(libs.plugins.kotlin.kapt)
}

android {
    namespace = "com.livelike.livelikedemo"
    compileSdk = libs.versions.compile.sdk.version.get().toInt()

    defaultConfig {
        applicationId = "com.livelike.livelikedemo"
        minSdk = 23
        targetSdk = libs.versions.target.sdk.version.get().toInt()
        multiDexEnabled = true
        versionCode = 1
        versionName = "1.1"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildFeatures {
        dataBinding = true
        viewBinding = true
        compose = true
    }

    buildTypes {
        debug {}
        release {
            isMinifyEnabled = false
            isShrinkResources = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    kotlinOptions {
        jvmTarget = "17"
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }

}

dependencies {
    implementation(libs.appcompat)
    implementation(libs.constraintlayout)
    implementation(libs.multidex)

    // Tests frameworks
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.4.1")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.4.1")
    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    implementation(libs.media3.exoplayer.hls)
    implementation(libs.lifecycle.runtime.ktx)
    implementation(libs.activity.compose)
    implementation(platform(libs.compose.bom))
    implementation(libs.ui)
    implementation(libs.ui.graphics)
    implementation(libs.ui.tooling.preview)
    implementation(libs.material3)
    implementation(platform(libs.compose.bom))
    androidTestImplementation(platform(libs.compose.bom))
    androidTestImplementation(libs.ui.test.junit4)
    androidTestImplementation(platform(libs.compose.bom))
    debugImplementation(libs.ui.tooling)
    debugImplementation(libs.ui.test.manifest)

    val nav_version = "2.5.3"
    implementation("androidx.navigation:navigation-fragment-ktx:$nav_version")
    implementation("androidx.navigation:navigation-ui-ktx:$nav_version")


    testImplementation(libs.junit)
    androidTestImplementation(libs.androidXJunit)
    androidTestImplementation("androidx.test:rules:1.4.0")
    androidTestImplementation(libs.espresso)

    implementation(libs.material)

    //players
    implementation(libs.media3.exoplayer)
    implementation(libs.media3.exoplayer.dash)
    implementation(libs.media3.ui)

    implementation("androidx.media:media:1.6.0")

    implementation(libs.glide)
    kapt(libs.glide.compiler)

    implementation(libs.activity.x)
    implementation(libs.fragment.x)

    //gson
    implementation(libs.gson)
    implementation(libs.three.ten)

    //sdk
    debugApi(project(":engagementsdk"))
    releaseApi(project(":engagementsdk"))
    implementation(project(":livelike-sceenic-plugin"))
    implementation(project(":livelike-ui:comments"))
    implementation(project(":livelike-ui:reactions"))
    //TODO  define configuration: 'release' for release variant currently it is not building in it.

    implementation(project(":pluginexoplayer"))
    implementation(libs.bundles.coroutines)

    implementation("com.github.angads25:filepicker:1.1.1")
    implementation(libs.lottie)
    implementation("com.github.anastr:speedviewlib:1.6.0")
    debugImplementation(libs.leakcanary.android)
}