@Suppress("DSL_SCOPE_VIOLATION")

plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.detekt)
    `maven-publish`
}

group = "com.livelike"


android {
    namespace = "com.livelike.sceenic.plugin"
    compileSdk = libs.versions.compile.sdk.version.get().toInt()

    defaultConfig {
        minSdk = 23
        targetSdk = libs.versions.target.sdk.version.get().toInt()
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    buildFeatures {
        viewBinding = true
    }

}


publishing {
    publications {
        // creating a release publication
        create<MavenPublication>("maven") {
            groupId = "com.livelike.android-engagement-sdk"
            artifactId = "sceenic-plugin"
            version = "0.0.2"

            afterEvaluate {
                from(components["release"])
            }
        }
    }
}

dependencies {
    implementation(libs.android.ktx)
    implementation(libs.appcompat)
    implementation(libs.material)
    implementation(project(":engagementsdk"))
    implementation(libs.bundles.coroutines)
    // Watch together SDK
    api(project(":sceenic-video-sdk"))
    testImplementation(libs.junit)
    androidTestImplementation(libs.androidXJunit)
    androidTestImplementation(libs.espresso)
}
