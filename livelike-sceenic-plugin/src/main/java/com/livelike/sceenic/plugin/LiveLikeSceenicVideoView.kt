package com.livelike.sceenic.plugin

import android.content.Context
import android.content.res.Configuration
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import com.livelike.sceenic.plugin.databinding.DefaultVideoViewBinding
import com.livelike.sceenic.plugin.utils.VideoConnectionState
import com.livelike.sceenic.plugin.utils.showError
import com.wt.sdk.data.participant.OnParticipantStatsListener
import com.wt.sdk.data.participant.Participant
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import org.json.JSONObject

class LiveLikeSceenicVideoView(context: Context, attrs: AttributeSet?) :
    ConstraintLayout(context, attrs) {
    private var session: LiveLikeSceenicVideoSession? = null
    private val binding = DefaultVideoViewBinding.inflate(LayoutInflater.from(context), this, true)
    private val uiScope = MainScope()

    init {
        (binding.listVideoCallParticipants.layoutManager as GridLayoutManager).spanCount =
            if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) 2 else 4
    }

    fun setSession(session: LiveLikeSceenicVideoSession) {
        Log.d(TAG, "setSession: $session")
        this.session = session
        binding.callEnd.setOnClickListener {
            session.leaveVideoRoom()
        }
        binding.listVideoCallParticipants.adapter = session.getAdapter()

        uiScope.launch {
            launch {
                session.errorEventFlow.collect {
                    it?.let {
                        it.description?.let { it1 -> showError(this@LiveLikeSceenicVideoView, it1) }
                    }
                }
            }
            launch { session.remoteParticipantJoinedEventFlow.collect { updateSpanCount() } }
            launch { session.remoteParticipantLeftEventFlow.collect { updateSpanCount() } }
            launch { session.localParticipantEventFlow.collect {
                    binding.layLocalControl.visibility = if (it != null) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }
                    it?.let { participant ->
                        participant.enableStats()
                        participant.setParticipantStatsListener(object :
                            OnParticipantStatsListener {
                            override fun onStats(stats: JSONObject?, participant: Participant) {
                                if (stats != null) {
                                    updateStats(stats, participant)
                                }
                            }
                        })
                        binding.mic.setOnClickListener {
                            if (participant.isAudioEnabled) {
                                participant.disableAudio()
                            } else {
                                participant.enableAudio()
                            }
                        }
                        binding.cam.setOnClickListener {
                            if (participant.isVideoEnabled) {
                                participant.disableVideo()
                            } else {
                                participant.enableVideo()
                            }
                            session.getAdapter().notifyItemChanged(0)
                        }
                        binding.switchCam.setOnClickListener {
                            session.onSwitchCameraClicked()
                        }
                    }
                    updateSpanCount()
                } }
            launch { session.videoConnectionStateEventFlow.collect {
                    when (it) {
                        VideoConnectionState.CONNECTING -> {
                            binding.spinnerVideo.visibility = VISIBLE
                        }
                        VideoConnectionState.CONNECTED, VideoConnectionState.DISCONNECTED -> {
                            binding.spinnerVideo.visibility = GONE
                            if (it == VideoConnectionState.DISCONNECTED)
                                Toast.makeText(context, "Disconnected", Toast.LENGTH_SHORT).show()
                        }
                        else -> {
                            binding.spinnerVideo.visibility = View.VISIBLE
                        }
                    }
                } }
        }
    }

    private fun updateSpanCount() {
        session?.let {
            (binding.listVideoCallParticipants.layoutManager as GridLayoutManager).spanCount =
                when (it.getAdapter().itemCount) {
                    1, 2 -> 1
                    else -> 2
                }
            it.getAdapter().notifyDataSetChanged()
        }
    }

    private fun updateStats(
        jsonStats: JSONObject?,
        participant: Participant
    ) {
        val strVideoStats = "${jsonStats?.get("videoAvg")} -> ${jsonStats?.get("videoQosMos")}"
        val strAudioStats = "${jsonStats?.get("audioAvg")} -> ${jsonStats?.get("audioQosMos")}"
        val connectionId = "${jsonStats?.get("connectionId")}"
        Log.i(
            "ParticipantsAdapter",
            "strVideoStats=$strVideoStats, " +
                    "strAudioStats=$strAudioStats, " +
                    "connectionId=${connectionId}"
        )
        binding.mic.setImageResource(
            when (participant.isAudioEnabled) {
                true -> R.drawable.ic_mic_on
                else -> R.drawable.ic_mic_off
            }
        )
        binding.cam.setImageResource(
            when (participant.isVideoEnabled) {
                true -> R.drawable.ic_camera_on
                else -> R.drawable.ic_camera_off
            }
        )
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        uiScope.cancel()
        session?.leaveVideoRoom()
    }

    companion object {
        const val TAG = "LiveLikeSceenic"
    }
}