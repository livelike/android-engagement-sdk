package com.livelike.sceenic.plugin.utils

enum class VideoRoomErrorType {
    USER_ALREADY_EXIST,
    CONNECTION_LOST,
    INVALID_VIDEO_ROOM_ID,
    EXPIRED_VIDEO_ROOM_ID,
    MAX_PARTICIPANT_LIMIT_REACHED,
    VIDEO_PERMISSION_DENIED,
    AUDIO_PERMISSION_DENIED,
    PLUGIN_ERROR,
}

data class VideoRoomErrorDetails(val errorType: VideoRoomErrorType, val description: String?)