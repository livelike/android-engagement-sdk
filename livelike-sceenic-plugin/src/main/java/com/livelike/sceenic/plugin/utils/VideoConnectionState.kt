package com.livelike.sceenic.plugin.utils

enum class VideoConnectionState {
    CONNECTING, CONNECTED, DISCONNECTED
}

interface VideoReconnectionStateListener {
    fun onReconnectingParticipant(participantId: String)
    fun onReconnectedParticipant(participantId: String)
}