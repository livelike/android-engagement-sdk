package com.livelike.sceenic.plugin.extensions

import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.video
import com.livelike.sceenic.plugin.LiveLikeSceenicVideoSession


fun EngagementSDK.createVideoSession(): LiveLikeSceenicVideoSession {
    return LiveLikeSceenicVideoSession(
        this.applicationContext,
        this.video()
    )
}

fun String.getInitials(): String {
    if (isNotEmpty()) {
        return this.replace("^\\s*([a-zA-Z]).*\\s+([a-zA-Z])\\S+$".toRegex(), "$1$2")
    }
    return this
}