package com.livelike.sceenic.plugin

import android.content.Context
import android.util.Log
import com.livelike.common.toNewCallback
import com.livelike.common.utils.toStream
import com.livelike.engagementsdk.Stream
import com.livelike.engagementsdk.publicapis.LiveLikeCallback
import com.livelike.engagementsdk.video.LiveLikeVideoClient
import com.livelike.sceenic.plugin.adapter.ParticipantsAdapter
import com.livelike.sceenic.plugin.utils.VideoConnectionState
import com.livelike.sceenic.plugin.utils.VideoReconnectionStateListener
import com.livelike.sceenic.plugin.utils.VideoRoomErrorDetails
import com.livelike.sceenic.plugin.utils.VideoRoomErrorType
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.wt.sdk.Session
import com.wt.sdk.SessionConnectionListener
import com.wt.sdk.SessionError
import com.wt.sdk.SessionListener
import com.wt.sdk.SessionReconnectListener
import com.wt.sdk.data.participant.Participant
import com.wt.sdk.data.participant.ParticipantType
import com.wt.sdk.params.MediaConfiguration
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class LiveLikeSceenicVideoSession(
    private val context: Context,
    private val client: LiveLikeVideoClient
) {
    private var mParticipantAdapter: ParticipantsAdapter = ParticipantsAdapter()
    private var mSceenicSession: Session? = null
    private var currentRoomId: String? = null
    internal val uiScope = MainScope()
    private var isFinishing = false
    private val jobMap = hashMapOf<String, Job>()

    internal val videoConnectionStateEventFlow = MutableStateFlow<VideoConnectionState?>(null)
    val videoConnectionStateEventStream: Stream<VideoConnectionState> =
        videoConnectionStateEventFlow.toStream(uiScope, false)

    internal val remoteParticipantJoinedEventFlow = MutableStateFlow<Participant?>(null)
    val remoteParticipantJoinedEventStream: Stream<Participant> =
        remoteParticipantJoinedEventFlow.toStream(uiScope)

    internal val remoteParticipantLeftEventFlow = MutableStateFlow<String?>(null)
    val remoteParticipantLeftEventStream: Stream<String> =
        remoteParticipantLeftEventFlow.toStream(uiScope)

    internal val localParticipantEventFlow = MutableStateFlow<Participant?>(null)
    val localParticipantEventStream: Stream<Participant> =
        localParticipantEventFlow.toStream(uiScope)

    internal val errorEventFlow = MutableStateFlow<VideoRoomErrorDetails?>(null)
    val errorEventStream: Stream<VideoRoomErrorDetails> = errorEventFlow.toStream(uiScope)

    var reconnectionState: VideoReconnectionStateListener? = null

    private val sessionListener = object : SessionListener {
        override fun onConnected(participants: List<Participant>) {
            Log.d(TAG, "onConnected: ${participants.size}")
            videoConnectionStateEventFlow.value = VideoConnectionState.CONNECTED
        }

        override fun onDisconnected() {
            Log.d(TAG, "onDisconnected: ")
            videoConnectionStateEventFlow.value = VideoConnectionState.DISCONNECTED
            mSceenicSession?.resetLocalAudioVideoState()
        }

        override fun onError(error: SessionError) {
            Log.d(TAG, "onError: $error")
            when (error.code) {
                101 -> mSceenicSession?.disconnect()
                106 -> mSceenicSession?.disconnect()
            }
            val errorType = when (error.code) {
                104 -> VideoRoomErrorType.USER_ALREADY_EXIST
                107 -> VideoRoomErrorType.VIDEO_PERMISSION_DENIED
                108 -> VideoRoomErrorType.AUDIO_PERMISSION_DENIED
                206 -> VideoRoomErrorType.MAX_PARTICIPANT_LIMIT_REACHED
                1100 -> VideoRoomErrorType.EXPIRED_VIDEO_ROOM_ID
                else -> VideoRoomErrorType.PLUGIN_ERROR
            }
            errorEventFlow.value = VideoRoomErrorDetails(errorType, error.message)
        }

        override fun onLocalParticipantJoined(participant: Participant) {
            Log.d(TAG, "onLocalParticipantJoined: $participant")
            mParticipantAdapter.addLocalParticipant(participant)
            localParticipantEventFlow.value = participant
        }

        override fun onMessageReceived(participantId: String, message: String) {
            Log.d(TAG, "onMessageReceived: $participantId,message:$message")
        }

        override fun onParticipantMediaStateChanged(
            participantId: String,
            mediaType: MediaConfiguration.MediaType?,
            mediaState: MediaConfiguration.MediaState?
        ) {
            Log.d(
                TAG,
                "onParticipantMediaStateChanged: $participantId,mediaType:$mediaType,state:$mediaState"
            )
            mParticipantAdapter.updateParticipantMedia(participantId, mediaType, mediaState)
        }

        override fun onRemoteParticipantJoined(participant: Participant) {
            Log.d(TAG, "onRemoteParticipantJoined: $participant")
            mParticipantAdapter.addRemoteParticipant(participant)
            remoteParticipantJoinedEventFlow.value = participant
        }

        override fun onRemoteParticipantLeft(participantId: String) {
            Log.d(TAG, "onRemoteParticipantLeft: $participantId")
            mParticipantAdapter.removeParticipant(participantId)
            remoteParticipantLeftEventFlow.value = participantId
        }

        override fun onUpdateParticipant(participantId: String, participant: Participant) {
            Log.d(TAG, "onUpdateParticipant: $participantId, details:$participant")
            mParticipantAdapter.updateParticipant(participantId, participant)
        }

    }
    private val sessionReconnectListener = object : SessionReconnectListener {
        override fun onParticipantReconnected(participantId: String, oldParticipantId: String) {
            Log.d(TAG, "onParticipantReconnected: $participantId,Old: $oldParticipantId")
            jobMap[oldParticipantId]?.cancel()
            reconnectionState?.onReconnectedParticipant(participantId)
            if (isFinishing) {
                return
            }
            mParticipantAdapter.progressConnection(oldParticipantId, false)
        }

        override fun onParticipantReconnecting(participantId: String) {
            Log.d(TAG, "onParticipantReconnecting: $participantId")
            mParticipantAdapter.progressConnection(participantId, true)
            reconnectionState?.onReconnectingParticipant(participantId)
            jobMap[participantId]?.cancel()
            jobMap[participantId] = uiScope.launch {
                delay(3000)
                mParticipantAdapter.removeParticipant(participantId)
                remoteParticipantLeftEventFlow.value = participantId
            }
        }
    }
    private val sessionConnectionListener = object : SessionConnectionListener {
        /**
         * SessionConnectionListener's callbacks
         *
         * */
        override fun onLocalConnectionLost() {
            Log.d(TAG, "onLocalConnectionLost: ")
            errorEventFlow.value =
                VideoRoomErrorDetails(VideoRoomErrorType.CONNECTION_LOST, "Connection Lost")
        }

        override fun onLocalConnectionResumed() {
            Log.d(TAG, "onLocalConnectionResumed: ")
        }

        override fun onRemoteConnectionLost(participantId: String) {
            Log.d(TAG, "onRemoteConnectionLost: $participantId")
            if (isFinishing) {
                return
            }
            mParticipantAdapter.remoteParticipantConnectionLost(participantId)
            jobMap[participantId]?.cancel()
            jobMap[participantId] = uiScope.launch {
                delay(3000)
                mParticipantAdapter.removeParticipant(participantId)
                remoteParticipantLeftEventFlow.value = participantId
            }
        }
    }

    private fun initSession() {
        Log.d(TAG, "initSession: ")
        mSceenicSession = Session.SessionBuilder(sessionListener)
            .setReconnectListener(sessionReconnectListener)
            .setConnectionListener(sessionConnectionListener)
            .build(context)
    }

    fun getAdapter() = mParticipantAdapter

    fun joinVideoRoom(roomId: String) {
        joinVideoRoom(roomId) { result, error ->
            result?.let { logDebug { it.toString() } }
            error?.let { logError { it } }
        }
    }

    fun joinVideoRoom(roomId: String, callback: LiveLikeCallback<Unit>? = null) {
        joinVideoRoom(roomId, callback?.toNewCallback())
    }

    fun joinVideoRoom(
        roomId: String,
        callback: com.livelike.common.LiveLikeCallback<Unit>? = null
    ) {
        Log.d(TAG, "joinVideoRoom: $roomId")
        videoConnectionStateEventFlow.value = VideoConnectionState.CONNECTING
        mSceenicSession?.disconnect()
        mSceenicSession = null
        initSession()
        currentRoomId = roomId
        client.getVideoRoom(roomId) { result, error ->
            result?.let {
                run {
                    mSceenicSession?.connect(
                        it.videoRoomSessionIdentifier,
                        ParticipantType.FULL_PARTICIPANT
                    )
                    callback?.invoke(Unit, null)
                }
            }
            error?.let {
                callback?.invoke(null, it)
            }
        }
    }

    fun setDisplayName(displayName: String) {
        Log.d(TAG, "setDisplayName: $displayName")
        mSceenicSession?.displayName = displayName
    }

    fun leaveVideoRoom() {
        Log.d(TAG, "disconnect: ")
        isFinishing = true
        clearAdapters()
        mSceenicSession?.disconnect()
    }

    fun pause() {
        Log.d(TAG, "pause: ")
        isFinishing = true
        uiScope.cancel()
        clearAdapters()
        mSceenicSession?.disconnect()
    }

    fun resume() {
        Log.d(TAG, "resume: ")
        isFinishing = false
        currentRoomId?.let {
            joinVideoRoom(it)
        }
    }

    fun onSwitchCameraClicked() {
        Log.d(TAG, "onSwitchCameraClicked: ")
        mSceenicSession?.switchCamera()
    }

    private fun clearAdapters() {
        Log.d(TAG, "clearAdapters: ")
        // Clear participants and disconnect from the session
        mParticipantAdapter.clearParticipants()
    }

    companion object {
        const val TAG = "LiveLikeSceenic"
    }
}