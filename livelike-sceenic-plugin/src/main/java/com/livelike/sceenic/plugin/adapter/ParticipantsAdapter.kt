package com.livelike.sceenic.plugin.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.livelike.sceenic.plugin.R
import com.livelike.sceenic.plugin.databinding.ItemParticipantBinding
import com.livelike.sceenic.plugin.extensions.getInitials
import com.wt.sdk.data.participant.LocalParticipant
import com.wt.sdk.data.participant.OnParticipantStatsListener
import com.wt.sdk.data.participant.Participant
import com.wt.sdk.params.MediaConfiguration
import org.json.JSONObject


class ParticipantsAdapter : RecyclerView.Adapter<ParticipantsAdapter.ViewHolder>() {

    private val mParticipants = ArrayList<Participant>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val binding =
            ItemParticipantBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        val height: Int = viewGroup.measuredHeight / 2
        binding.root.maxHeight = height
        return ViewHolder(binding)
    }

    private fun checkPosition(position: Int): Boolean {
        var size = position + 1
        if (size % 2 != 0) {
            size += 1
        }
        val matrix = Array(size / 2) { BooleanArray(2) }
        for (i in 0 until (size / 2)) {
            if (i % 2 == 0) {
                matrix[i] = booleanArrayOf(false, true)
            } else {
                matrix[i] = booleanArrayOf(true, false)
            }
        }

        var pos = 0
        for (i in matrix.indices) {
            for (j in matrix[i].indices) {
                if (position == pos) {
                    return matrix[i][j]
                }
                pos++
            }
        }
        return false
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val participant = mParticipants[position]
        participant.setRenderer(viewHolder.binding.videoRenderer)
        viewHolder.binding.participantLay.setBackgroundColor(
            ContextCompat.getColor(
                viewHolder.itemView.context, when (checkPosition(position)) {
                    true -> R.color.participant_background_color_1
                    else -> R.color.participant_background_color_2
                }
            )
        )
        if (participant !is LocalParticipant) {
            participant.enableStats()
            participant.setParticipantStatsListener(object : OnParticipantStatsListener {
                override fun onStats(stats: JSONObject?, participant: Participant) {
                    viewHolder.binding.participantName.visibility =
                        when (participant.isVideoEnabled) {
                            true -> View.GONE
                            false -> View.VISIBLE
                        }
                    viewHolder.binding.videoRenderer.visibility =
                        when (participant.isVideoEnabled) {
                            true -> View.VISIBLE
                            false -> View.INVISIBLE
                        }
                }
            })
        } else {
            viewHolder.binding.participantName.visibility =
                when (participant.isVideoEnabled) {
                    true -> View.GONE
                    false -> {
                        viewHolder.binding.videoRenderer.clearImage()
                        View.VISIBLE
                    }
                }
            viewHolder.binding.videoRenderer.visibility =
                when (participant.isVideoEnabled) {
                    true -> View.VISIBLE
                    false -> View.INVISIBLE
                }
        }
        viewHolder.binding.txtConnectionState.visibility =
            if (participant.isConnectionLost) View.VISIBLE else View.GONE
        viewHolder.binding.progressConnection.visibility =
            if (participant.isProgressReconnection) View.VISIBLE else View.GONE
        viewHolder.binding.participantName.text = participant.name.getInitials()
    }

    override fun getItemCount(): Int {
        return mParticipants.size
    }

    fun remoteParticipantConnectionLost(participantId: String?) {
        for (i in mParticipants.indices) {
            val participant = mParticipants[i]
            if (participant.id == participantId) {
                val itemIndex = mParticipants.indexOf(participant)
                participant.isConnectionLost = true
                notifyItemChanged(itemIndex, participant)
            }
        }
    }

    fun updateParticipant(participantId: String, participant: Participant) {
        if (!isExistParticipant(participantId)) {
            addRemoteParticipant(participant)
            return
        }
        for (index in mParticipants.indices) {
            val participantExist = mParticipants[index]
            if (participantExist.id == participantId) {
                participantExist.update(participant)
                notifyItemChanged(index, participantExist)
                break
            }
        }
    }

    fun progressConnection(participantId: String?, isProgress: Boolean) {
        mParticipants.forEachIndexed { index, participant ->
            if (participant.id == participantId || participant.oldParticipantId == participantId) {
                participant.isProgressReconnection = isProgress
                mParticipants[index] = participant
                notifyItemChanged(index, participant)
            }
        }
    }

    fun addLocalParticipant(participant: Participant) {
        if (mParticipants.size > 0) {
            mParticipants[0].update(participant)
            notifyItemChanged(0, participant)
        } else {
            mParticipants.add(participant)
            notifyItemInserted(itemCount - 1)
        }
    }

    fun addRemoteParticipant(participant: Participant) {
        mParticipants.add(participant)
        notifyItemInserted(itemCount - 1)
    }

    fun removeParticipant(participantId: String) {
        for (index in mParticipants.indices) {
            val participant = mParticipants[index]
            if (participant.id == participantId) {
                participant.dispose()
                mParticipants.remove(participant)
                notifyItemRemoved(index)
                break
            }
        }
    }

    fun clearParticipants() {
        for (participant in mParticipants) {
            participant.dispose()
        }
        mParticipants.clear()
        notifyDataSetChanged()
    }

    fun updateParticipantMedia(
        participantId: String,
        mediaType: MediaConfiguration.MediaType?,
        mediaState: MediaConfiguration.MediaState?
    ) {
        if (mParticipants.isNotEmpty()) {
            mParticipants.forEachIndexed { index, participant ->
                if (participant.id == participantId) {
                    val isEnableMediaState = mediaState == MediaConfiguration.MediaState.ENABLED
                    when (mediaType) {
                        MediaConfiguration.MediaType.AUDIO -> participant.isAudioEnabled =
                            isEnableMediaState
                        MediaConfiguration.MediaType.VIDEO -> participant.isVideoEnabled =
                            isEnableMediaState
                        else -> {
                        }
                    }
                    // Update exactly view after its changed
                    notifyItemChanged(index, participant)
                    return@forEachIndexed
                }
            }
        }
    }

    private fun isExistParticipant(participantId: String): Boolean {
        var isExistParticipant = false
        for (participant in mParticipants) {
            if (participant.oldParticipantId == participantId || participant.id == participantId) {
                isExistParticipant = true
            }
        }
        return isExistParticipant
    }

    class ViewHolder(var binding: ItemParticipantBinding) : RecyclerView.ViewHolder(binding.root)
}