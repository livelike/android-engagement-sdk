package com.livelike.sceenic.plugin.utils

import android.annotation.SuppressLint
import android.graphics.Color
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.google.android.material.snackbar.Snackbar
import com.livelike.sceenic.plugin.databinding.VideoToastBinding
import java.util.*


val queue = PriorityQueue<String>()
var isSnackBarShowing = false

@SuppressLint("RestrictedApi")
fun showError(view: View, message: String) {
    if (isSnackBarShowing) {
        queue.add(message)
        return
    }
    isSnackBarShowing = true
    val snackBar = Snackbar.make(view, "", Snackbar.LENGTH_LONG)
    val customView = VideoToastBinding.inflate(LayoutInflater.from(view.context))
    customView.txtVideoToast.text = message
    snackBar.view.setBackgroundColor(Color.TRANSPARENT)
    // now change the layout of the snackBar
    val snackBarLayout = snackBar.view as Snackbar.SnackbarLayout
    // set padding of the all corners as 0
    val params = snackBar.view.layoutParams as FrameLayout.LayoutParams
    snackBar.addCallback(object : Snackbar.Callback() {
        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
            super.onDismissed(transientBottomBar, event)
            isSnackBarShowing = false
            if (queue.isNotEmpty()) {
                showError(view, queue.poll()!!)
            }
        }
    })
    params.gravity = Gravity.TOP
    val margin =
        TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20F, view.resources.displayMetrics)
            .toInt()
    snackBar.view.layoutParams = params
    snackBarLayout.setPadding(margin, margin, margin, margin)
    snackBarLayout.addView(customView.root, 0)
    snackBar.show()
}