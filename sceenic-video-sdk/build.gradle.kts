@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.detekt)
    `maven-publish`
}

configurations.maybeCreate("default")
artifacts.add("default", file("wtsdk_v2.4.3.aar"))

publishing {
    publications {
        // creating a release publication
        create<MavenPublication>("maven") {
            groupId = "com.livelike.android-engagement-sdk"
            artifactId = "sceenic-video-sdk"
            version = "2.4.3"
        }
    }
}