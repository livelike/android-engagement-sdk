## Contributing
To contribute to the Android SDK, please make sure you have a JIRA ticket associated with the task.
If none exist, feel free to create one in the [Mobile Backlog JIRA board](https://livelike.atlassian.net/jira/software/c/projects/MBLG/issues/)

### Please refer to the following documents:
- [Development Workflow](https://livelike.quip.com/lssMAw29fE6d/Android-Development-Workflow)
- [SDK Coding Style Guide](https://livelike.quip.com/vkuUAVWBMO9c/Coding-Conventions-for-Android-Source-code-organization)
- [Pull Request Requirements / Best Practices](https://livelike.quip.com/aaY2A0DHi4p9/Android-PR-Requirements-Best-Practices)




