package com.livelike.realtime.internal

import com.google.gson.JsonObject
import com.livelike.realtime.RealTimeClientMessage
import com.livelike.realtime.RealTimeMessagingClient
import com.livelike.realtime.RealTimeMessagingClientConfig
import com.livelike.serialization.extractStringOrEmpty
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.pubnub.api.PNConfiguration
import com.pubnub.api.PubNub
import com.pubnub.api.UserId
import com.pubnub.api.callbacks.SubscribeCallback
import com.pubnub.api.enums.PNReconnectionPolicy
import com.pubnub.api.models.consumer.PNStatus
import com.pubnub.api.models.consumer.pubsub.PNMessageResult
import com.pubnub.api.models.consumer.pubsub.message_actions.PNMessageActionResult
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.onFailure
import kotlinx.coroutines.channels.onSuccess
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.shareIn
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

internal class InternalPubnubRealTimeMessagingClientImpl(
    realTimeMessagingClientConfig: RealTimeMessagingClientConfig,
    scope: CoroutineScope
) : RealTimeMessagingClient {

    private var pubnub: PubNub
    private val subscribedChannels = mutableSetOf<String>()
    private val pubnubConfiguration: PNConfiguration =
        PNConfiguration(UserId(realTimeMessagingClientConfig.uuid))

    init {
        pubnubConfiguration.authKey = realTimeMessagingClientConfig.authKey
        realTimeMessagingClientConfig.publishKey?.let {
            pubnubConfiguration.publishKey = it
        }
        realTimeMessagingClientConfig.origin?.let {
            pubnubConfiguration.origin = it
        }
        pubnubConfiguration.reconnectionPolicy = PNReconnectionPolicy.EXPONENTIAL
        pubnubConfiguration.subscribeKey = realTimeMessagingClientConfig.subscriberKey
        pubnubConfiguration.heartbeatInterval =
            realTimeMessagingClientConfig.pubnubHeartbeatInterval
        pubnubConfiguration.presenceTimeout = realTimeMessagingClientConfig.pubnubPresenceTimeout
        pubnub = PubNub(pubnubConfiguration)
        logDebug { "PubNub is being initialized!" }
    }

    override fun subscribe(channels: List<String>) {
        logDebug { "Real Time Subscribe:${channels.size}" }
        val newChannels = channels.filter { !subscribedChannels.contains(it) }
        pubnub.subscribe(channels = newChannels)
        subscribedChannels.addAll(newChannels)
    }

    override fun unsubscribe(channels: List<String>) {
        logDebug { "Real Time UnSubscribe:${channels.size}" }
        pubnub.unsubscribe(channels = channels)
        for (it in channels) {
            subscribedChannels.remove(it)
        }
    }

    override fun unsubscribeAll() {
        logDebug { "Real Time UnSubscribeAll" }
        pubnub.unsubscribeAll()
        subscribedChannels.clear()
    }

    override fun stop() {
        pubnub.disconnect()
    }

    override fun start() {
        pubnub.reconnect()
    }

    override fun destroy() {
        unsubscribeAll()
        pubnub.destroy()
    }

    override suspend fun removeMessageAction(
        channel: String,
        messageTimeToken: Long,
        pubnubActionToken: Long
    ) = suspendCoroutine { cont ->
        pubnub.removeMessageAction(
            channel = channel,
            messageTimetoken = messageTimeToken,
            actionTimetoken = pubnubActionToken
        ).async { _, status ->
            if (!status.error) {
                logDebug { "own message action removed" }
                cont.resume(Unit)
            } else {
                status.exception?.printStackTrace()
                cont.resumeWithException(
                    status.exception ?: Exception("Error Removing Pubnub Action")
                )
            }
        }
    }

    override val messageClientFlow: Flow<RealTimeClientMessage> = callbackFlow {
        val listener = object : SubscribeCallback() {
            override fun status(pubnub: PubNub, pnStatus: PNStatus) {
                logDebug { "Pubnub Status:$pnStatus" }
            }

            override fun message(pubnub: PubNub, pnMessageResult: PNMessageResult) {
                super.message(pubnub, pnMessageResult)
                logDebug { "Pubnub message:${pnMessageResult.message.asJsonObject}" }

                trySendBlocking(
                    RealTimeClientMessage(
                        event = pnMessageResult.message.asJsonObject.extractStringOrEmpty("event"),
                        payload = pnMessageResult.message.asJsonObject.getAsJsonObject("payload"),
                        timeToken = pnMessageResult.timetoken ?: 0L,
                        category = "chat-message",
                        channel = pnMessageResult.channel
                    )
                )
                    .onSuccess { logDebug { "pubnub event flow send" } }
                    .onFailure { logError { "pubnub event flow send error: ${it?.message}" } }
            }
        }
        pubnub.addListener(listener)
        logDebug { "Real Time Add Listener" }
        awaitClose {
            pubnub.removeListener(listener)
            logDebug { "Real Time Remove Listener" }
        }
    }.shareIn(scope, SharingStarted.WhileSubscribed())

    override val messageActionFlow: Flow<RealTimeClientMessage> = callbackFlow {
        val listener = object : SubscribeCallback() {
            override fun status(pubnub: PubNub, pnStatus: PNStatus) {
                logDebug { "Pubnub Status:$pnStatus" }
            }

            override fun messageAction(
                pubnub: PubNub,
                pnMessageActionResult: PNMessageActionResult
            ) {
                super.messageAction(pubnub, pnMessageActionResult)
                logDebug { "Pubnub messageAction: ${pnMessageActionResult.messageAction}" }
                trySendBlocking(
                    RealTimeClientMessage(
                        event = pnMessageActionResult.event,
                        payload = JsonObject().apply {
                            addProperty(
                                "isOwnReaction",
                                pubnub.configuration.userId == pnMessageActionResult.messageAction.uuid?.let {
                                    UserId(
                                        it
                                    )
                                }
                            )
                            addProperty(
                                "actionPubnubToken",
                                pnMessageActionResult.messageAction.actionTimetoken
                            )
                            addProperty(
                                "messagePubnubToken",
                                pnMessageActionResult.messageAction.messageTimetoken
                            )
                            addProperty(
                                "emojiId",
                                pnMessageActionResult.messageAction.value
                            )
                            addProperty("userId", pnMessageActionResult.messageAction.uuid)
                        },
                        timeToken = pnMessageActionResult.timetoken ?: 0L,
                        category = "chat-message-reaction",
                        channel = pnMessageActionResult.channel
                    )
                )
                    .onSuccess { logDebug { "pubnub event flow send" } }
                    .onFailure { logError { it?.message } }
            }
        }
        pubnub.addListener(listener)
        awaitClose {
            pubnub.removeListener(listener)
        }
    }.shareIn(scope, SharingStarted.WhileSubscribed())

}