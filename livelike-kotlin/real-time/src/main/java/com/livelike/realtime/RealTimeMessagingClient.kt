package com.livelike.realtime

import com.google.gson.JsonObject
import com.livelike.realtime.internal.InternalPubnubRealTimeMessagingClientImpl
import com.livelike.serialization.extractStringOrEmpty
import com.livelike.utils.parseISODateTime
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow

/**
 * Interface for managing real-time messaging.
 */
interface RealTimeMessagingClient {
    /**
     * Subscribe to a list of channels.
     *
     * @param channels the list of channels to subscribe to
     */
    fun subscribe(channels: List<String>)

    /**
     * Unsubscribe from a list of channels.
     *
     * @param channels the list of channels to unsubscribe from
     */
    fun unsubscribe(channels: List<String>)

    /**
     * Unsubscribe from all channels.
     */
    fun unsubscribeAll()

    /**
     * Stop the real-time messaging client.
     */
    fun stop()

    /**
     * Start the real-time messaging client.
     */
    fun start()

    /**
     * Destroy the real-time messaging client.
     */
    fun destroy()

    /**
     * Remove a message action.
     *
     * @param channel the channel from which to remove the message action
     * @param messageTimeToken the time token of the message
     * @param pubnubActionToken the action token of the PubNub action
     */
    suspend fun removeMessageAction(
        channel: String,
        messageTimeToken: Long,
        pubnubActionToken: Long
    )

    /**
     * Flow of real-time client messages.
     */
    val messageClientFlow: Flow<RealTimeClientMessage>

    /**
     * Flow of real-time client message actions.
     */
    val messageActionFlow: Flow<RealTimeClientMessage>

    companion object {
        /**
         * Get an instance of the real-time messaging client.
         *
         * @param config the configuration for the real-time messaging client
         * @param scope the coroutine scope
         * @return an instance of the real-time messaging client
         */
        fun getInstance(
            config: RealTimeMessagingClientConfig,
            scope: CoroutineScope
        ): RealTimeMessagingClient =
            InternalPubnubRealTimeMessagingClientImpl(config, scope)
    }
}

data class RealTimeMessagingClientConfig(
    val subscriberKey: String,
    val authKey: String,
    val uuid: String,
    val publishKey: String? = null,
    val origin: String? = null,
    val pubnubHeartbeatInterval: Int,
    val pubnubPresenceTimeout: Int,
)

data class RealTimeClientMessage(
    var event: String,
    val payload: JsonObject,
    val timeToken: Long,
    val category: String?,
    val channel: String,
    val priority: Int = 0
) {

    fun getId(): String? {
        return payload.get("id")?.asString
    }

    fun getProgramDateTime(): Long {
        val pdtString = payload.extractStringOrEmpty("program_date_time")
        var epochTimeMs = 0L
        pdtString.parseISODateTime()?.let {
            epochTimeMs = it.toInstant().toEpochMilli()
        }
        return epochTimeMs
    }
}