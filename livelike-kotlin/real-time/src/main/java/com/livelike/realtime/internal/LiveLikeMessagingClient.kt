package com.livelike.realtime.internal

import com.google.gson.JsonNull
import com.google.gson.JsonObject
import com.livelike.network.NetworkApiClient
import com.livelike.realtime.RealTimeClientMessage
import com.livelike.realtime.RealTimeMessagingClient
import com.livelike.serialization.processResult
import com.livelike.utils.PaginationResponse
import com.livelike.utils.isoDateTimeFormat
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.shareIn
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime
import java.util.Calendar

class LiveLikeMessagingClient(
    private val liveLikeMessagingClientParam: LiveLikeMessagingClientParam,
    private val networkApiClient: NetworkApiClient,
    private val chatRoomId: StateFlow<String?>,
    scope: CoroutineScope
) : RealTimeMessagingClient {

    override fun subscribe(channels: List<String>) {

    }

    override fun unsubscribe(channels: List<String>) {

    }

    override fun unsubscribeAll() {

    }

    override fun stop() {

    }

    override fun start() {

    }

    override fun destroy() {

    }

    override suspend fun removeMessageAction(
        channel: String,
        messageTimeToken: Long,
        pubnubActionToken: Long
    ) {
        return
    }

    private var nextEventUrl: String? = null

    @OptIn(ExperimentalCoroutinesApi::class)
    override val messageClientFlow: Flow<RealTimeClientMessage> =
        channelFlow {
            logDebug { "livelike messsaing run ,looping:${liveLikeMessagingClientParam.apiPollingInterval}" }
            while (!isClosedForSend) {
                chatRoomId.value?.let { id ->
                    val params = arrayListOf<Pair<String, Any>>()
                    if (nextEventUrl == null) {
                        params.add("chat_room_id" to id)
                        params.add(
                            "since" to ZonedDateTime.ofInstant(
                                Instant.ofEpochMilli(Calendar.getInstance().timeInMillis),
                                ZoneId.of("UTC")
                            ).isoDateTimeFormat()
                        )
                    }
                    networkApiClient.get(
                        nextEventUrl ?: liveLikeMessagingClientParam.chatRoomEventsUrl,
                        queryParameters = params
                    ).processResult<PaginationResponse<JsonObject>>()
                        .run {
                            nextEventUrl = this.next
                            results.map {
                                val payload = it.getAsJsonObject("payload")
                                RealTimeClientMessage(
                                    payload.get("message_event").asString,
                                    payload,
                                    when (payload.get("pubnub_timetoken")) {
                                        JsonNull.INSTANCE -> 0L
                                        else -> payload.get("pubnub_timetoken")?.asLong ?: 0L
                                    },
                                    it.get("category").asString,
                                    ""
                                )
                            }.forEach {
                                send(it)
                            }
                        }
                } ?: logError { "Trying to fetch events but no active chatRoom found" }
                delay(liveLikeMessagingClientParam.apiPollingInterval * 1000L)
            }
            logDebug { "livelike messsaing stops" }
        }.catch {
            it.printStackTrace()
        }.shareIn(scope, SharingStarted.WhileSubscribed())


    override val messageActionFlow: Flow<RealTimeClientMessage> = flowOf()
}

data class LiveLikeMessagingClientParam(
    val chatRoomEventsUrl: String,
    val apiPollingInterval: Int
)