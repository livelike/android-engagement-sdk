package com.livelike.realtime.internal

import com.livelike.realtime.RealTimeClientMessage
import com.livelike.realtime.RealTimeMessagingClient
import com.livelike.utils.CoreEpochTime
import com.livelike.utils.logDebug
import com.livelike.utils.logVerbose
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.launch
import java.util.Queue
import java.util.concurrent.PriorityBlockingQueue

internal class InternalSynchronizationMessagingClient(
    private val messagingClient: RealTimeMessagingClient,
    var timeSource: () -> CoreEpochTime,
    private val validEventBufferMs: Long,
    private val dispatcher: CoroutineDispatcher,
    sessionScope: CoroutineScope
) : RealTimeMessagingClient {

    private fun shouldPublishEvent(event: RealTimeClientMessage): Boolean {
        val timeStamp = timeSource()
        val eventTimeStamp = CoreEpochTime(event.getProgramDateTime())
        logDebug { "shouldPublishEvent:${timeStamp.timeSinceEpochInMs}, event:${eventTimeStamp.timeSinceEpochInMs}" }
        return timeStamp <= CoreEpochTime(0) || // Timesource return 0 - sync disabled
                eventTimeStamp <= CoreEpochTime(0) || // Event time is 0 - bypass sync
                (eventTimeStamp <= timeStamp && eventTimeStamp >= timeStamp - validEventBufferMs)
    }

    private fun shouldDismissEvent(event: RealTimeClientMessage): Boolean {
        val eventTimeStamp = CoreEpochTime(event.getProgramDateTime())
        return eventTimeStamp > CoreEpochTime(0) &&
                (eventTimeStamp < timeSource() - validEventBufferMs)
    }

    private fun logDismissedEvent(event: RealTimeClientMessage) {
        val eventTimeStamp = CoreEpochTime(event.getProgramDateTime())
        logVerbose {
            "Dismissed Client Message: ${event.payload} -- the message was too old! eventTime" +
                    eventTimeStamp.timeSinceEpochInMs +
                    " : timeSourceTime" + timeSource().timeSinceEpochInMs
        }
    }

    private val queueMap: MutableMap<String, Queue<RealTimeClientMessage>> = mutableMapOf()
    private val messageComparator: Comparator<RealTimeClientMessage> = Comparator { o1, o2 ->
        val eventTimeStamp1 = CoreEpochTime(o1.getProgramDateTime())
        val eventTimeStamp2 = CoreEpochTime(o2.getProgramDateTime())

        when {
            eventTimeStamp1.timeSinceEpochInMs > eventTimeStamp2.timeSinceEpochInMs -> {
                return@Comparator 1
            }

            eventTimeStamp2.timeSinceEpochInMs > eventTimeStamp1.timeSinceEpochInMs -> {
                return@Comparator -1
            }

            else -> {
                return@Comparator 0
            }
        }
    }

    private fun addMessageToQueue(event: RealTimeClientMessage) {
        // Adding the check for similar message ,it is occuring if user get message
        // from history and also receive message from pubnub listener
        // checking right now is based on id
        val queue = queueMap[event.channel] ?: PriorityBlockingQueue(
            DEFAULT_QUEUE_CAPACITY,
            messageComparator
        )
        val currentChatMessageId = event.payload.get("id")?.asString

        val foundMsg = queue.find {
            val msgId = it.payload.get("id")?.asString
            return@find msgId != null && currentChatMessageId != null && msgId == currentChatMessageId
        }
        logDebug {
            "add msg to queue1 > foundMsg:${foundMsg?.payload?.get("message")}, message:${
                event.payload.get(
                    "message"
                )
            } queue:${queue.size}"
        }
        if (foundMsg == null) {
            queue.add(event)
        }
        logDebug {
            "add msg to queue2 > foundMsg:${foundMsg?.payload?.get("message")}, message:${
                event.payload.get(
                    "message"
                )
            } queue:${queue.size}"
        }
        queueMap[event.channel] = queue
    }

    override val messageClientFlow: Flow<RealTimeClientMessage> = callbackFlow {
        val scope = CoroutineScope(dispatcher)
        scope.launch {
            launch {
                while (true) {
                    delay(
                        when (queueMap.isEmpty()) {
                            true -> 5 * SYNC_TIME_FIDELITY
                            else -> SYNC_TIME_FIDELITY
                        }
                    )
                    for (it in queueMap.keys) {
                        logDebug { "while loop queue size:${queueMap[it]?.size}" }
                        val queue = queueMap[it]
                        queue?.let {
                            var event = queue.peek()
                            while (event != null && shouldPublishEvent(event)) {
                                this@callbackFlow.trySendBlocking(event)
                                queue.remove(event)
                                event = queue.peek()
                            }
                        }
                    }
                }
            }
            launch {
                logDebug { "sync message flow" }
                messagingClient.messageClientFlow.collect { event ->
                    logDebug {
                        "sync client message: ${shouldPublishEvent(event)}: message:${
                            event.payload.get(
                                "message"
                            )
                        }"
                    }
                    when {
                        shouldPublishEvent(event) -> this@callbackFlow.trySendBlocking(event)
                        shouldDismissEvent(event) -> logDismissedEvent(event)
                        else -> addMessageToQueue(event)
                    }
                }
            }
        }
        awaitClose {
            scope.cancel()
            logDebug { "Sync callback flow closed" }
        }
    }.shareIn(sessionScope, SharingStarted.WhileSubscribed())

    override val messageActionFlow: Flow<RealTimeClientMessage> = messagingClient.messageActionFlow

    override fun subscribe(channels: List<String>) {
        messagingClient.subscribe(channels)
    }

    override fun unsubscribe(channels: List<String>) {
        messagingClient.unsubscribe(channels)
    }

    override fun unsubscribeAll() {
        messagingClient.unsubscribeAll()
    }

    override fun stop() {
        messagingClient.stop()
    }

    override fun start() {
        messagingClient.start()
    }

    override fun destroy() {
        messagingClient.destroy()
    }

    override suspend fun removeMessageAction(
        channel: String,
        messageTimeToken: Long,
        pubnubActionToken: Long
    ) {
        return messagingClient.removeMessageAction(channel, messageTimeToken, pubnubActionToken)
    }

    companion object {
        private const val SYNC_TIME_FIDELITY = 500L
        private const val DEFAULT_QUEUE_CAPACITY = 20
    }
}

fun RealTimeMessagingClient.syncTo(
    timeSource: () -> CoreEpochTime,
    validEventBufferMs: Long = Long.MAX_VALUE,
    dispatcher: CoroutineDispatcher,
    sessionScope: CoroutineScope
): RealTimeMessagingClient {
    return InternalSynchronizationMessagingClient(
        this,
        timeSource,
        validEventBufferMs,
        dispatcher,
        sessionScope
    )
}