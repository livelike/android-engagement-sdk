package com.livelike.end2end

import app.cash.turbine.test
import com.livelike.common.AccessTokenDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.common.profile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.createReactionSession
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.engagementsdk.reaction
import com.livelike.engagementsdk.reaction.LiveLikeReactionClient
import com.livelike.reaction.CreateReactionSpaceRequest
import com.livelike.reaction.GetReactionSpaceRequest
import com.livelike.utils.LiveLikeException
import com.livelike.utils.LogLevel
import com.livelike.utils.logInfo
import com.livelike.utils.minimumLogLevel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine
import kotlin.random.Random
import kotlin.time.Duration.Companion.seconds


class LiveLikeReactionEnd2EndTest {
    private val testReactionSpaceTitle: String = "Test Reaction Space Prop"
    private lateinit var reactionClient: LiveLikeReactionClient
    private lateinit var programId: String
    private lateinit var liveLikeKotlin: LiveLikeKotlin
    private val testDispatcher = StandardTestDispatcher()
    private lateinit var clientId: String
    private lateinit var producerToken: String
    private val testTargetGroupId = "Test Target Group ID: ${Random.nextDouble()}"
    private lateinit var reactionPackIds: List<String>
    private val testTargetId = "target-user-1"

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        minimumLogLevel = LogLevel.Debug
        producerToken = System.getenv("producerToken")
        clientId = System.getenv("clientId")
        programId = System.getenv("programId")
        val reactionsPacks = System.getenv("reactionPacks")
        reactionPackIds = reactionsPacks.split(",")
        liveLikeKotlin = LiveLikeKotlin(
            clientId,
            accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return producerToken
                }

                override fun storeAccessToken(accessToken: String?) {

                }

            },
            networkDispatcher = testDispatcher,
            sdkDispatcher = testDispatcher,
            mainDispatcher = testDispatcher
        )
        reactionClient = liveLikeKotlin.reaction()
    }

    @After
    fun tearDown() {
        testDispatcher.cancel()
    }

    @Test
    fun testReactionSpace() = runTest(timeout = 20.seconds) {
        val reactionSpace = suspendCoroutine { cont ->
            reactionClient.createReactionSpace(
                CreateReactionSpaceRequest(
                    name = testReactionSpaceTitle,
                    targetGroupId = testTargetGroupId,
                    reactionsPackIds = reactionPackIds
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(reactionSpace.name == testReactionSpaceTitle)
        assert(reactionSpace.targetGroupId == testTargetGroupId)
        assert(reactionSpace.reactionPackIds == reactionPackIds)

        val reactionSession = liveLikeKotlin.createReactionSession(
            targetGroupId = testTargetGroupId,
            reactionSpaceId = reactionSpace.id,
            errorDelegate = object : ErrorDelegate() {
                override fun onError(error: String) {
                    println("error: $error")
                }
            },
            sessionDispatcher = testDispatcher,
            uiDispatcher = testDispatcher,
        )

        suspendCoroutine { cont ->
            reactionSession.callback = { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }


        val packs = suspendCoroutine { cont ->
            reactionSession.getReactionPacks { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(packs.size == reactionSpace.reactionPackIds.size)
        assert(packs[0].emojis.isNotEmpty())

        logInfo { "GET Reaction Packs" }

        val getReactionSpace = suspendCoroutine { cont ->
            reactionClient.getReactionSpaceDetails(reactionSpace.id) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(getReactionSpace.name == testReactionSpaceTitle)
        assert(getReactionSpace.targetGroupId == testTargetGroupId)
        assert(getReactionSpace.reactionPackIds == reactionPackIds)

        logInfo { "GET Reaction Space" }

        val reactionSpaces = suspendCoroutine { cont ->
            reactionClient.getReactionSpaces(
                GetReactionSpaceRequest(
                    targetGroupId = testTargetGroupId,
                    liveLikePagination = LiveLikePagination.FIRST
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(reactionSpaces.isNotEmpty())

        logInfo { "GET Reaction Spaces" }


        reactionSession.addUserReactionFlow.test {

            val userReaction = suspendCoroutine { cont ->
                reactionSession.addUserReaction(
                    "target-user-1",
                    packs[0].emojis[0].id,
                    liveLikeCallback = { result, error ->
                        result?.let { cont.resume(it) }
                        error?.let { cont.resumeWithException(LiveLikeException(it)) }
                    })
            }
            assert(userReaction.reactionSpaceId == reactionSpace.id)
            assert(userReaction.reactionId == packs[0].emojis[0].id)
            assert(userReaction.targetId == testTargetId)
            assert(userReaction.reactedById == liveLikeKotlin.profile().currentProfileOnce().id)

            packs[0].emojis.forEachIndexed { index, reaction ->
                if (index > 0) {
                    suspendCoroutine { cont ->
                        reactionSession.addUserReaction(
                            testTargetId,
                            reaction.id,
                            liveLikeCallback = { result, error ->
                                result?.let { cont.resume(it) }
                                error?.let { cont.resumeWithException(LiveLikeException(it)) }
                            })
                    }
                }
            }

            logInfo { "Add User Reaction" }

            val item = awaitItem()
            assert(item.reactionSpaceId == reactionSpace.id)
            assert(packs[0].emojis.any { it.id == item.reactionId })
            assert(item.targetId == testTargetId)
            assert(item.reactedById == liveLikeKotlin.profile().currentProfileOnce().id)


            val userReactions = suspendCoroutine { cont ->
                reactionSession.getUserReactions(
                    LiveLikePagination.FIRST,
                    targetId = testTargetId,
                    liveLikeCallback = { result, error ->
                        result?.let { cont.resume(it) }
                        error?.let { cont.resumeWithException(LiveLikeException(it)) }
                    })
            }

            assert(userReactions.isNotEmpty() && userReactions.size == packs[0].emojis.size)

            val userReactionCount = suspendCoroutine { cont ->
                reactionSession.getUserReactionsCount(
                    targetIds = listOf(testTargetId),
                    LiveLikePagination.FIRST,
                    liveLikeCallback = { result, error ->
                        result?.let { cont.resume(it) }
                        error?.let { cont.resumeWithException(LiveLikeException(it)) }
                    })
            }
            assert(userReactionCount.sumOf { count -> count.reactions.sumOf { it.count } } == packs[0].emojis.size)

            this.cancelAndIgnoreRemainingEvents()
        }


        val delete = suspendCoroutine { cont ->
            reactionClient.deleteReactionSpace(reactionSpace.id) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

    }
}