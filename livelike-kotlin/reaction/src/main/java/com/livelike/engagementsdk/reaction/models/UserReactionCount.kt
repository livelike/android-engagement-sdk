package com.livelike.engagementsdk.reaction.models

import com.google.gson.annotations.SerializedName

data class UserReactionCount(
    @SerializedName("reaction_id") var reactionId: String,
    @SerializedName("count") var count: Int,
    @SerializedName("self_reacted_user_reaction_id") var selfReactedUserReactionId: String? = null
)
