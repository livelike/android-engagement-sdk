package com.livelike.engagementsdk.reaction.models

import com.google.gson.annotations.SerializedName

data class ReactionSpace(
    @SerializedName("id") var id: String,
    @SerializedName("url") var url: String,
    @SerializedName("name") var name: String,
    @SerializedName("target_group_id") var targetGroupId: String,
    @SerializedName("client_id") var clientId: String,
    @SerializedName("reaction_pack_ids") var reactionPackIds: List<String> = emptyList(),
    @SerializedName("reaction_space_channel") internal var reactionSpaceChannel: String,
    @SerializedName("created_by") var createdBy: String,
    @SerializedName("user_reactions_url") internal var userReactionsUrl: String,
    @SerializedName("user_reaction_detail_url_template") internal var userReactionDetailUrlTemplate: String,
    @SerializedName("user_reactions_count_url") internal var userReactionsCountUrl: String
)
