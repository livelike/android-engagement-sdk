package com.livelike.engagementsdk

import com.livelike.common.LiveLikeKotlin
import com.livelike.common.profile
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.engagementsdk.reaction.LiveLikeReactionClient
import com.livelike.engagementsdk.reaction.LiveLikeReactionSession
import com.livelike.engagementsdk.reaction.ReactionSession
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

/**
 *  Creates a reaction client and returns that.
 */
fun LiveLikeKotlin.reaction(): LiveLikeReactionClient {
    val key = this.hashCode()
    return if (sdkInstanceWithReactionClient.containsKey(key)) {
        sdkInstanceWithReactionClient[key]!!
    } else {
        LiveLikeReactionClient.getInstance(
            sdkConfigurationOnce,
            profile().currentProfileOnce,
            sdkScope,
            uiScope,
            networkClient
        ).let {
            sdkInstanceWithReactionClient[key] = it
            it
        }
    }
}


private val sdkInstanceWithReactionClient = mutableMapOf<Int, LiveLikeReactionClient>()


/**
 *  Creates a reaction session
 *  @reactionSpaceId reaction space id
 *  @targetGroupId target group id
 *  @errorDelegate error delegate
 */
fun LiveLikeKotlin.createReactionSession(
    reactionSpaceId: String?, targetGroupId: String?,
    errorDelegate: ErrorDelegate? = null,
    sessionDispatcher: CoroutineDispatcher = Dispatchers.Default,
    uiDispatcher: CoroutineDispatcher = Dispatchers.Main,
): LiveLikeReactionSession {
    return ReactionSession(
        sdkConfigurationOnce,
        profile().currentProfileOnce,
        errorDelegate,
        reactionSpaceId,
        targetGroupId,
        networkClient,
        sessionDispatcher,
        uiDispatcher,
    ).also {
        addSession(it)
    }
}