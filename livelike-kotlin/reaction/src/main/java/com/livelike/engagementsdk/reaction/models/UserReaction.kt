package com.livelike.engagementsdk.reaction.models

import com.google.gson.annotations.SerializedName

data class UserReaction(
    @SerializedName("id") var id: String,
    @SerializedName("target_id") var targetId: String,
    @SerializedName("client_id") var clientId: String,
    @SerializedName("reacted_by_id") var reactedById: String,
    @SerializedName("created_at") var createdAt: String,
    @SerializedName("reaction_id") var reactionId: String,
    @SerializedName("reaction_space_id") var reactionSpaceId: String,
    @SerializedName("custom_data") var customData: String? = null
)
