package com.livelike.engagementsdk.reaction

import com.google.gson.reflect.TypeToken
import com.livelike.common.model.PubnubChatEventType
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.model.toPubnubChatEventType
import com.livelike.common.utils.BaseSession
import com.livelike.common.utils.safeCallBack
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.chatreaction.ReactionPack
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.engagementsdk.reaction.models.ReactionSpace
import com.livelike.engagementsdk.reaction.models.TargetUserReactionCount
import com.livelike.engagementsdk.reaction.models.UserReaction
import com.livelike.network.NetworkApiClient
import com.livelike.reaction.NO_REACTION_SPACE_FOUND
import com.livelike.reaction.PROVIDE_REACTION_SPACE_ID
import com.livelike.reaction.ReactionPackRepository
import com.livelike.reaction.ReactionRepository
import com.livelike.reaction.USER_REACTION_ID
import com.livelike.realtime.RealTimeMessagingClient
import com.livelike.realtime.RealTimeMessagingClientConfig
import com.livelike.serialization.gson
import com.livelike.utils.Once
import com.livelike.utils.PaginationResponse
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.livelike.utils.suspendLazy
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch

internal class ReactionSession(
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    internal val errorDelegate: ErrorDelegate? = null,
    private val reactionSpaceId: String? = null,
    private val targetGroupId: String? = null,
    private val networkApiClient: NetworkApiClient,
    sessionDispatcher: CoroutineDispatcher,
    uiDispatcher: CoroutineDispatcher,
) : BaseSession(
    configurationOnce,
    currentProfileOnce,
    sessionDispatcher,
    uiDispatcher,
    errorDelegate
), LiveLikeReactionSession {

    private val _externalReactionChannel = Channel<UserReaction>()
    private var reactionMessagingClient: RealTimeMessagingClient? = null


    private val reactionSpaceDelegateMap = hashMapOf<String, ReactionSpaceDelegate>()
    private val userReactionDelegateMap = hashMapOf<String, UserReactionDelegate>()

    override var callback: com.livelike.common.LiveLikeCallback<Unit>? = null

    override val addUserReactionFlow = callbackFlow {
        val job = sessionScope.launch {
            logDebug { "add user reaction flow launch" }
            launch {
                reactionMessagingClient?.messageClientFlow?.collect { message ->
                    when (message.event.toPubnubChatEventType()) {
                        PubnubChatEventType.USER_REACTION_CREATED -> {
                            val reaction: UserReaction =
                                gson.fromJson(
                                    message.payload,
                                    object : TypeToken<UserReaction>() {}.type
                                )
                            send(reaction)
                        }
                        else -> {
                            logError { "Event not required here" }
                        }
                    }
                }
            }

            launch {
                _externalReactionChannel.consumeAsFlow().collect { reaction ->
                    send(reaction)
                }
            }
        }
        awaitClose {
            logDebug { "add user reaction flow cancel" }
            job.cancel()
        }
    }

    private val _externalRemoveReactionChannel = Channel<UserReaction>()

    override val removeUserReactionFlow = callbackFlow {
        val job = sessionScope.launch {
            launch {
                reactionMessagingClient?.messageClientFlow?.collect { message ->
                    when (message.event.toPubnubChatEventType()) {
                        PubnubChatEventType.USER_REACTION_REMOVED -> {
                            val reaction: UserReaction =
                                gson.fromJson(
                                    message.payload,
                                    object : TypeToken<UserReaction>() {}.type
                                )
                            send(reaction)
                        }
                        else -> {
                            logError { "Event not required here" }
                        }
                    }
                }
            }
            launch {
                _externalRemoveReactionChannel.consumeAsFlow().collect { reaction ->
                    send(reaction)
                }
            }
        }
        awaitClose {
            job.cancel()
            logDebug { "remove user reaction flow cancel" }
        }
    }

    override val reactionSpaceUpdateFlow = callbackFlow {
        val job = sessionScope.launch {
            reactionMessagingClient?.messageClientFlow?.collect { message ->
                when (message.event.toPubnubChatEventType()) {
                    PubnubChatEventType.REACTION_SPACE_UPDATED -> {
                        val reactionSpace: ReactionSpace =
                            gson.fromJson(
                                message.payload,
                                object : TypeToken<ReactionSpace>() {}.type
                            )
                        send(reactionSpace)
                    }

                    else -> {
                        logError { "Event not required here" }
                    }
                }
            }
        }
        awaitClose { job.cancel() }
    }

    init {
        if (targetGroupId.isNullOrEmpty() && reactionSpaceId.isNullOrEmpty()) {
            errorDelegate?.onError(PROVIDE_REACTION_SPACE_ID)
        } else {
            safeCallBack(sessionScope) {
                configurationProfilePairOnce().let { pair ->
                    pair.second.pubNubKey?.let { subscribeKey ->
                        reactionMessagingClient = RealTimeMessagingClient.getInstance(
                            RealTimeMessagingClientConfig(
                                subscribeKey,
                                pair.first.accessToken,
                                pair.first.id,
                                pair.second.pubnubPublishKey,
                                pair.second.pubnubOrigin,
                                pair.second.pubnubHeartbeatInterval,
                                pair.second.pubnubPresenceTimeout
                            ),
                            sessionScope
                        )


                    } ?: logDebug { "SDk config pubnubKey is null" }
                }

                configurationProfilePairOnce().let { pair ->
                    pair.second.pubNubKey?.let {
                        setPubnubClient()
                    }
                } ?: logDebug { "SDk config pubnubKey is null" }

            }
        }
    }

    private fun setPubnubClient() {
        sessionErrorHandlerScope.launch {
            reactionMessagingClient.let { client ->
                launch {
                    addUserReactionFlow.collect { reaction ->
                        uiScope.launch {
                            for (it in userReactionDelegateMap.values) {
                                it.onReactionAdded(reaction)
                            }
                        }
                    }
                }
                launch {
                    removeUserReactionFlow.collect { reaction ->
                        uiScope.launch {
                            for (it in userReactionDelegateMap.values) {
                                it.onReactionRemoved(reaction)
                            }
                        }
                    }
                }
                launch {
                    reactionSpaceUpdateFlow.collect { reactionSpace ->
                        uiScope.launch {
                            for (it in reactionSpaceDelegateMap.values) {
                                it.onReactionSpaceUpdated(reactionSpace)
                            }
                        }
                    }
                }
                launch {
                    reactionSpaceDetailOnce().let { pair ->
                        pair.second.reactionSpaceChannel.let {
                            client?.subscribe(listOf(it))
                            callback?.let {
                                logDebug { "Callback with subscribe done" }
                                it(Unit, null)
                            }
                        }
                    }
                }
            }
        }
    }

    private val reactionRepositoryOnce = suspendLazy {
        val pair = configurationProfilePairOnce()
        ReactionRepository(
            authKey = pair.first.accessToken,
            clientId = pair.second.clientId,
            reactionSpacesUrl = pair.second.reactionSpacesUrl,
            reactionSpaceDetailUrlTemplate = pair.second.reactionSpaceDetailUrlTemplate,
            networkApiClient = networkApiClient,
        )
    }

    private val reactionPackRepositoryOnce = suspendLazy {
        val pair = configurationProfilePairOnce()
        ReactionPackRepository(
            authKey = pair.first.accessToken,
            reactionPackDetailUrlTemplate = pair.second.reactionPackDetailUrlTemplate,
            reactionPackUrl = pair.second.reactionPacksUrl,
            networkApiClient = networkApiClient
        )
    }

    private val reactionSpaceDetailOnce: Once<Pair<ReactionRepository, ReactionSpace>> =
        suspendLazy {
            val reactionRepository = reactionRepositoryOnce()
            if (!reactionSpaceId.isNullOrEmpty()) {
                reactionRepository.reactionSpaceDetail(reactionSpaceId).run {
                    Pair(reactionRepository, this)
                }
            } else {
                reactionRepository.getListOfReactionSpaces(targetGroupId = targetGroupId).run {
                    return@run if (this.results.isNotEmpty()) Pair(
                        reactionRepository, this.results.first()
                    ) else throw Exception(
                        NO_REACTION_SPACE_FOUND
                    )
                }
            }
        }

    private val userReactionListResponseMap: HashMap<String, PaginationResponse<UserReaction>> =
        hashMapOf()
    private val userReactionCountListResponseMap: HashMap<String, PaginationResponse<TargetUserReactionCount>> =
        hashMapOf()


   /* override fun addUserReaction(
        targetId: String,
        reactionId: String,
        customData: String?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<UserReaction>
    ) {
        safeCallBack(liveLikeCallback) {
            val pair = reactionSpaceDetailOnce()
            pair.second.userReactionsUrl.let { userReactionsUrl ->
                pair.second.id.let { id ->
                    pair.first.addUserReaction(
                        userReactionsUrl, id, targetId, reactionId, customData
                    )
                }
            }
        }
    }*/



    override fun addUserReaction(
        targetId: String,
        reactionId: String,
        customData: String?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<UserReaction>
    ) {
        safeCallBack(liveLikeCallback) {
            val pair = reactionSpaceDetailOnce()
            pair.second.userReactionsUrl.let { userReactionsUrl ->
                pair.second.id.let { id ->
                    val userReaction = pair.first.addUserReaction(
                        userReactionsUrl, id, targetId, reactionId, customData
                    )

                    // Check if subscribeKey is not null before sending to _externalReactionChannel
                    // this is done for non-pubnub support for reactions
                    configurationProfilePairOnce().let { configPair ->
                        if(configPair.second.pubNubKey.isNullOrEmpty()){
                            // Only send to _externalReactionChannel if subscribeKey is null
                            sessionScope.launch {
                                _externalReactionChannel.send(userReaction)
                            }
                        }
                    }

                    // Return the UserReaction for the callback
                    userReaction
                }
            }
        }
    }

    override fun removeUserReaction(
        userReactionId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack(liveLikeCallback) {
            val pair = reactionSpaceDetailOnce()
            pair.second.userReactionDetailUrlTemplate.let { userReactionDetailUrlTemplate ->
                return@safeCallBack pair.first.deleteUserReaction(
                    userReactionDetailUrlTemplate.replace(
                        USER_REACTION_ID, userReactionId
                    )
                )
            }
        }
    }


    override fun removeUserReaction(
        userReactionId: String,
        userReaction: UserReaction?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack(liveLikeCallback) {
            val pair = reactionSpaceDetailOnce()
            pair.second.userReactionDetailUrlTemplate.let { userReactionDetailUrlTemplate ->
                val response = pair.first.deleteUserReaction(
                    userReactionDetailUrlTemplate.replace(
                        USER_REACTION_ID, userReactionId
                    )
                )

                // If the deletion was successful, send to _externalRemoveReactionChannel
                    configurationProfilePairOnce().let { configPair ->
                        if(configPair.second.pubNubKey.isNullOrEmpty()){
                            // Create a UserReaction object with the available information

                            val removedReaction = userReaction

                            sessionScope.launch {
                                if (removedReaction != null) {
                                    _externalRemoveReactionChannel.send(removedReaction)
                                }
                            }
                        }
                    }
                response
            }
        }
    }


    override fun getUserReactions(
        liveLikePagination: LiveLikePagination,
        targetId: String?,
        reactionId: String?,
        reactionById: String?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<UserReaction>>
    ) {
        safeCallBack(liveLikeCallback) {
            val pair = reactionSpaceDetailOnce()
            pair.second.userReactionsUrl.let { firstUrl ->
                val key =
                    "${reactionSpaceId}_${targetGroupId}_${targetId}_${reactionId}_${reactionById}"
                val url = when (liveLikePagination) {
                    LiveLikePagination.FIRST -> null
                    LiveLikePagination.NEXT -> userReactionListResponseMap[key]?.next
                    LiveLikePagination.PREVIOUS -> userReactionListResponseMap[key]?.previous
                }
                pair.first.getListOfUserReactions(
                    url = url ?: firstUrl,
                    reactionSpaceId = reactionSpaceId,
                    targetGroupId = targetGroupId,
                    targetId = targetId,
                    reactionId = reactionId,
                    reactedById = reactionById
                ).also {
                    userReactionListResponseMap[key] = it
                }.results
            }
        }
    }


    override fun getUserReactionsCount(
        targetIds: List<String>,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<TargetUserReactionCount>>
    ) {
        safeCallBack(liveLikeCallback) {
            val pair = reactionSpaceDetailOnce()
            pair.second.userReactionsCountUrl.let { firstUrl ->
                pair.second.id.let { reactionSpaceId ->
                    var key = "${reactionSpaceId}_${targetGroupId}"
                    for (id in targetIds) {
                        key += "_$id"
                    }
                    val url = when (liveLikePagination) {
                        LiveLikePagination.FIRST -> null
                        LiveLikePagination.NEXT -> userReactionListResponseMap[key]?.next
                        LiveLikePagination.PREVIOUS -> userReactionListResponseMap[key]?.previous
                    }
                    pair.first.getReactionsCounts(
                        url = url ?: firstUrl,
                        reactionSpaceId = reactionSpaceId,
                        targetIds = targetIds
                    ).also {
                        userReactionCountListResponseMap[key] = it
                    }.results
                }
            }
        }
    }


    override fun subscribeToReactionSpaceDelegate(
        key: String, reactionSpaceDelegate: ReactionSpaceDelegate
    ) {
        reactionSpaceDelegateMap[key] = reactionSpaceDelegate
    }

    override fun unSubscribeToReactionSpaceDelegate(key: String) {
        reactionSpaceDelegateMap.remove(key)
    }

    override fun subscribeToUserReactionDelegate(
        key: String, userReactionDelegate: UserReactionDelegate
    ) {
        userReactionDelegateMap[key] = userReactionDelegate
    }

    override fun unSubscribeToUserReactionDelegate(key: String) {
        userReactionDelegateMap.remove(key)
    }

    override fun getReactionPacks(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ReactionPack>>) {
        safeCallBack(liveLikeCallback) {
            val pair = reactionSpaceDetailOnce()
            val reactionPackRepository = reactionPackRepositoryOnce()
            pair.second.reactionPackIds.map { reactionPackRepository.getReactionPackDetails(it) }
        }
    }


    override fun close() {
        sessionErrorHandlerScope.cancel()
        sessionScope.cancel()
        uiScope.cancel()
    }
}