package com.livelike.engagementsdk.reaction

import com.livelike.engagementsdk.reaction.models.ReactionSpace


interface UserReactionDelegate {
    fun onReactionAdded(reaction: com.livelike.engagementsdk.reaction.models.UserReaction)
    fun onReactionRemoved(reaction: com.livelike.engagementsdk.reaction.models.UserReaction)
}

interface ReactionSpaceDelegate {
    fun onReactionSpaceUpdated(reactionSpace: ReactionSpace)
}