package com.livelike.engagementsdk.reaction

import com.livelike.engagementsdk.chat.chatreaction.ReactionPack
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.engagementsdk.reaction.models.ReactionSpace
import com.livelike.engagementsdk.reaction.models.UserReaction
import kotlinx.coroutines.flow.Flow

interface LiveLikeReactionSession {

    /**
     * Callback to ensure the setup is completed
     */
    var callback: com.livelike.common.LiveLikeCallback<Unit>?
    val addUserReactionFlow: Flow<UserReaction>
    val removeUserReactionFlow: Flow<UserReaction>
    val reactionSpaceUpdateFlow: Flow<ReactionSpace>

    /**
     * Add a user reaction.
     *
     * @param targetId the id of the target to which the reaction is added
     * @param reactionId the id of the reaction
     * @param customData optional custom data
     * @param liveLikeCallback callback to receive the added UserReaction
     */
    fun addUserReaction(
        targetId: String,
        reactionId: String,
        customData: String? = null,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<UserReaction>
    )


    /**
     * Remove a user reaction.
     *
     * @param userReactionId the id of the UserReaction to remove
     * @param liveLikeCallback callback to receive the response of the removal operation
     */
    fun removeUserReaction(
        userReactionId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    )

    /**
     * Remove a user reaction.
     *
     * @param userReactionId the id of the UserReaction to remove
     * @param liveLikeCallback callback to receive the response of the removal operation
     */
    fun removeUserReaction(
        userReactionId: String,
        userReaction:UserReaction?=null,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    )

    /**
     * Get user reactions.
     *
     * @param liveLikePagination pagination object for pagination
     * @param targetId optional id of the target for which to get reactions
     * @param reactionId optional id of the reaction to get
     * @param reactionById optional id of the user by whom the reaction was made
     * @param liveLikeCallback callback to receive the list of UserReactions
     */
    fun getUserReactions(
        liveLikePagination: LiveLikePagination,
        targetId: String? = null,
        reactionId: String? = null,
        reactionById: String? = null,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<UserReaction>>
    )


    /**
     * Get the count of user reactions.
     *
     * @param targetIds list of ids of the targets for which to get the reaction count
     * @param liveLikePagination pagination object for pagination
     * @param liveLikeCallback callback to receive the list of TargetUserReactionCount
     */
    fun getUserReactionsCount(
        targetIds: List<String>,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<com.livelike.engagementsdk.reaction.models.TargetUserReactionCount>>
    )


    /**
     * Subscribe to a ReactionSpaceDelegate.
     *
     * @param key the key to identify the delegate
     * @param reactionSpaceDelegate the delegate to subscribe
     */
    fun subscribeToReactionSpaceDelegate(key: String, reactionSpaceDelegate: ReactionSpaceDelegate)
    /**
     * Unsubscribe from a ReactionSpaceDelegate.
     *
     * @param key the key identifying the delegate to unsubscribe
     */
    fun unSubscribeToReactionSpaceDelegate(key: String)

    /**
     * Subscribe to a UserReactionDelegate.
     *
     * @param key the key to identify the delegate
     * @param userReactionDelegate the delegate to subscribe
     */
    fun subscribeToUserReactionDelegate(key: String, userReactionDelegate: UserReactionDelegate)
    /**
     * Unsubscribe from a UserReactionDelegate.
     *
     * @param key the key identifying the delegate to unsubscribe
     */
    fun unSubscribeToUserReactionDelegate(key: String)

    /**
     * Get reaction packs.
     *
     * @param liveLikeCallback callback to receive the list of ReactionPack
     */
    fun getReactionPacks(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ReactionPack>>)

    fun close()
}