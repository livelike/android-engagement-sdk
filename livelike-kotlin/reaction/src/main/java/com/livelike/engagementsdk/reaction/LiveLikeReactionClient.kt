package com.livelike.engagementsdk.reaction


import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.chatreaction.ReactionPack
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.engagementsdk.reaction.models.ReactionSpace
import com.livelike.network.NetworkApiClient
import com.livelike.reaction.CreateReactionSpaceRequest
import com.livelike.reaction.GetReactionSpaceRequest
import com.livelike.reaction.InternalReactionClient
import com.livelike.reaction.UpdateReactionSpaceRequest
import com.livelike.utils.Once
import kotlinx.coroutines.CoroutineScope


interface LiveLikeReactionClient {
    /**
     * Create a reaction space
     * @param name name of the reaction space
     * @param targetGroupId unique identifier of the content referencing collection of items
     * @param reactionsPackIds  reaction pack ids is a collection of reactions to be used by your users
     **/
    fun createReactionSpace(
        createReactionSpaceRequest: CreateReactionSpaceRequest,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ReactionSpace>
    )


    /**
     * Get reaction space detail from reactionSpaceId or targetGroupId
     * @param id reaction space id
     **/
    fun getReactionSpaceDetails(
        id: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ReactionSpace>
    )


    /**
     * Used to remove Reaction Space for the given reactionSpaceId in the object parameter.
     * @param id reaction space id
     **/
    fun deleteReactionSpace(
        id: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    )

    /**
     * Used to update name and reaction pack Ids of an existing reaction space.
     * @param id reaction space id
     * @param reactionsPackIds reaction pack ids
     **/
    fun updateReactionSpace(
        updateReactionSpaceRequest: UpdateReactionSpaceRequest,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ReactionSpace>
    )

    /**
     * Fetches list of reaction spaces in an application.
     * @param reactionSpaceId reaction space id
     * @param targetGroupId unique identifier of the content referencing collection of items
     **/
    fun getReactionSpaces(
        request: GetReactionSpaceRequest,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ReactionSpace>>
    )

    /**
     * Used to get list of reaction pack created through producer suite.
     **/
    fun getReactionPacks(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ReactionPack>>
    )

    /**
     * Used to get reaction pack details using reaction pack Id.
     * @param reactionPackId reaction pack id
     **/
    fun getReactionPackDetails(
        reactionPackId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ReactionPack>
    )



    companion object {

        fun getInstance(
            configurationOnce: Once<SdkConfiguration>,
            currentProfileOnce: Once<LiveLikeProfile>,
            sdkScope: CoroutineScope,
            uiScope: CoroutineScope,
            networkApiClient: NetworkApiClient
        ): LiveLikeReactionClient = InternalReactionClient(
            configurationOnce, currentProfileOnce, uiScope, sdkScope, networkApiClient
        )
    }

}