package com.livelike.engagementsdk.reaction.models

import com.google.gson.annotations.SerializedName

data class TargetUserReactionCount(
    @SerializedName("target_id") var targetId: String? = null,
    @SerializedName("reactions") var reactions: List<UserReactionCount> = emptyList()
)
