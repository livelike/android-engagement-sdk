package com.livelike.engagementsdk.chat.chatreaction

data class Reaction(
    val mimetype: String,
    val name: String,
    var file: String,
    val id: String
)

data class ReactionPack(val name: String, val file: String, val emojis: List<Reaction>)
