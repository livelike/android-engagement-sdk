package com.livelike.reaction

import com.livelike.BaseClient
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.chatreaction.ReactionPack
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.engagementsdk.reaction.LiveLikeReactionClient
import com.livelike.engagementsdk.reaction.models.ReactionSpace
import com.livelike.network.NetworkApiClient
import com.livelike.utils.Once
import com.livelike.utils.PaginationResponse
import com.livelike.utils.suspendLazy
import kotlinx.coroutines.CoroutineScope

internal class InternalReactionClient(
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    uiScope: CoroutineScope,
    sdkScope: CoroutineScope,
    private val networkApiClient: NetworkApiClient,
) : BaseClient(configurationOnce, currentProfileOnce, sdkScope, uiScope),
    LiveLikeReactionClient {
    private var reactionSpaceListResponse: HashMap<String, PaginationResponse<ReactionSpace>> =
        hashMapOf()
    private var reactionPackResults: PaginationResponse<ReactionPack>? = null

    private val reactionRepositoryOnce = suspendLazy {
        val pair = configurationProfilePairOnce()
        ReactionRepository(
            authKey = pair.first.accessToken,
            clientId = pair.second.clientId,
            reactionSpacesUrl = pair.second.reactionSpacesUrl,
            reactionSpaceDetailUrlTemplate = pair.second.reactionSpaceDetailUrlTemplate,
            networkApiClient = networkApiClient
        )
    }

    private val reactionPackRepositoryOnce = suspendLazy {
        val pair = configurationProfilePairOnce()
        ReactionPackRepository(
            authKey = pair.first.accessToken,
            reactionPackDetailUrlTemplate = pair.second.reactionPackDetailUrlTemplate,
            reactionPackUrl = pair.second.reactionPacksUrl,
            networkApiClient
        )
    }

    override fun createReactionSpace(
        createReactionSpaceRequest: CreateReactionSpaceRequest,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ReactionSpace>
    ) {
        safeCallBack(liveLikeCallback) {
            val repository = reactionRepositoryOnce()
            repository.createReactionSpace(
                name = createReactionSpaceRequest.name,
                targetGroupId = createReactionSpaceRequest.targetGroupId,
                reactionPackIds = createReactionSpaceRequest.reactionsPackIds
            )
        }
    }


    override fun getReactionSpaceDetails(
        id: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ReactionSpace>
    ) {
        safeCallBack(liveLikeCallback) {
            val repository = reactionRepositoryOnce()
            repository.reactionSpaceDetail(id)
        }
    }

    override fun deleteReactionSpace(
        id: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack(liveLikeCallback) {
            val repository = reactionRepositoryOnce()
            repository.deleteReactionSpace(id)
        }
    }


    override fun updateReactionSpace(
        updateReactionSpaceRequest: UpdateReactionSpaceRequest,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ReactionSpace>
    ) {
        safeCallBack(liveLikeCallback) {
            val repository = reactionRepositoryOnce()
            repository.updateReactionSpace(
                updateReactionSpaceRequest.id,
                reactionPackIds = updateReactionSpaceRequest.reactionsPackIds
            )
        }
    }



    override fun getReactionSpaces(
        request: GetReactionSpaceRequest,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ReactionSpace>>
    ) {
        safeCallBack(liveLikeCallback) {
            val repository = reactionRepositoryOnce()
            val key = "${request.reactionSpaceId}_${request.targetGroupId}"
            val url = when (request.liveLikePagination) {
                LiveLikePagination.FIRST -> null
                LiveLikePagination.NEXT -> reactionSpaceListResponse[key]?.next
                LiveLikePagination.PREVIOUS -> reactionSpaceListResponse[key]?.previous
            }
            repository.getListOfReactionSpaces(
                url = url,
                targetGroupId = request.targetGroupId,
                reactionSpaceId = request.reactionSpaceId
            ).also {
                reactionSpaceListResponse[key] = it
            }.results
        }
    }



    override fun getReactionPacks(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ReactionPack>>
    ) {
        safeCallBack(liveLikeCallback) {
            val reactionPackRepository = reactionPackRepositoryOnce()
            val url = when (liveLikePagination) {
                LiveLikePagination.FIRST -> null
                LiveLikePagination.NEXT -> reactionPackResults?.next
                LiveLikePagination.PREVIOUS -> reactionPackResults?.previous
            }
            reactionPackRepository.getReactionPacks(url).also {
                reactionPackResults = it
            }.results
        }
    }



    override fun getReactionPackDetails(
        reactionPackId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ReactionPack>
    ) {
        safeCallBack(liveLikeCallback) {
            val reactionPackRepository = reactionPackRepositoryOnce()
            reactionPackRepository.getReactionPackDetails(reactionPackId)
        }
    }

}

data class CreateReactionSpaceRequest(
    val name: String?,
    val targetGroupId: String,
    val reactionsPackIds: List<String>
)

data class UpdateReactionSpaceRequest(
    val id: String,
    val reactionsPackIds: List<String>,
)

data class GetReactionSpaceRequest(
    val reactionSpaceId: String? = null,
    val targetGroupId: String? = null,
    val liveLikePagination: LiveLikePagination,
)