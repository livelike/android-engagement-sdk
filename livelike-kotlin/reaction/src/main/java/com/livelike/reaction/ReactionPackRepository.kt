package com.livelike.reaction


import com.livelike.engagementsdk.chat.chatreaction.ReactionPack
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.processResult
import com.livelike.utils.PaginationResponse

internal class ReactionPackRepository(
    private val authKey: String,
    private val reactionPackDetailUrlTemplate: String,
    private val reactionPackUrl: String,
    private val networkApiClient: NetworkApiClient,
) {
    suspend fun getReactionPackDetails(reactionPackId: String): ReactionPack {
        return networkApiClient.get(
            reactionPackDetailUrlTemplate.replace(
                REACTION_PACK_ID, reactionPackId
            ), accessToken = authKey
        ).processResult()
    }

    suspend fun getReactionPacks(url: String?): PaginationResponse<ReactionPack> {
        return networkApiClient.get(url ?: reactionPackUrl, accessToken = authKey).processResult()
    }
}