package com.livelike.reaction

internal const val PROVIDE_REACTION_SPACE_ID = "Please provide value of ReactionSpaceId or TargetGroupId"
internal const val NO_REACTION_SPACE_FOUND = "No Reaction Space Found for the provided ID"
internal const val REACTION_SPACE_ID_ERROR = "Reaction Space Id is null"
internal const val USER_REACTION_URL_NULL = "User Reaction Url is null"
internal const val USER_REACTION_DETAIL_URL_NULL = "User Reaction Detail Url is null"
internal const val USER_REACTION_COUNT_URL_NULL = "User Reaction Count Url is null"

// url templates
internal const val USER_REACTION_ID = "{user_reaction_id}"
internal const val REACTION_PACK_ID = "{reaction_pack_id}"
internal const  val TEMPLATE_REACTION_SPACE_ID = "{reaction_space_id}"