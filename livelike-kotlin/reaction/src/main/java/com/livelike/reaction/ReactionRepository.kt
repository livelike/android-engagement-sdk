package com.livelike.reaction

import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.engagementsdk.reaction.models.ReactionSpace
import com.livelike.engagementsdk.reaction.models.TargetUserReactionCount
import com.livelike.engagementsdk.reaction.models.UserReaction
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import com.livelike.utils.PaginationResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class ReactionRepository(
    private val authKey: String,
    private val clientId: String,
    private val reactionSpacesUrl: String,
    private val reactionSpaceDetailUrlTemplate: String,
    private val networkApiClient: NetworkApiClient,
) {
    suspend fun createReactionSpace(
        name: String? = null,
        targetGroupId: String,
        reactionPackIds: List<String>
    ): ReactionSpace {
        return networkApiClient.post(
            reactionSpacesUrl, accessToken = authKey, body = hashMapOf(
                "target_group_id" to targetGroupId, "reaction_pack_ids" to reactionPackIds
            ).apply {
                name?.let {
                    this["name"] = it
                }
            }.toJsonString()
        ).processResult()
    }

    suspend fun reactionSpaceDetail(id: String): ReactionSpace {
        val url = reactionSpaceDetailUrlTemplate.replace(TEMPLATE_REACTION_SPACE_ID, id)
        return networkApiClient.get(url, accessToken = authKey).processResult()
    }

    suspend fun deleteReactionSpace(id: String): LiveLikeEmptyResponse {
        val url = reactionSpaceDetailUrlTemplate.replace(TEMPLATE_REACTION_SPACE_ID, id)
        return networkApiClient.delete(url, accessToken = authKey).processResult()
    }

    suspend fun getListOfReactionSpaces(
        url: String? = null,
        targetGroupId: String? = null,
        reactionSpaceId: String? = null
    ): PaginationResponse<ReactionSpace> {
        val finalUrl: String
        val queryParams = arrayListOf<Pair<String, Any>>()
        if (url != null) {
            finalUrl = url
        } else {
            finalUrl = reactionSpacesUrl
            queryParams.add("client_id" to clientId)
            if (targetGroupId != null) {
                queryParams.add("target_group_id" to targetGroupId)
            }
            if (reactionSpaceId != null) {
                queryParams.add("reaction_space_id" to reactionSpaceId)
            }
        }
        return networkApiClient.get(finalUrl, accessToken = authKey, queryParameters = queryParams)
            .processResult()
    }

    suspend fun updateReactionSpace(
        id: String,
        reactionPackIds: List<String>
    ): ReactionSpace {
        val url = reactionSpaceDetailUrlTemplate.replace(TEMPLATE_REACTION_SPACE_ID, id)
        return networkApiClient.patch(
            url, accessToken = authKey, body = mapOf(
                "reaction_pack_ids" to reactionPackIds
            ).toJsonString()
        ).processResult()
    }

    suspend fun addUserReaction(
        url: String,
        reactionSpaceId: String,
        targetId: String,
        reactionId: String,
        customData: String? = null
    ): UserReaction {
        return networkApiClient.post(
            url, accessToken = authKey,
            body = hashMapOf(
                "target_id" to targetId,
                "client_id" to clientId,
                "reaction_id" to reactionId,
                "reaction_space_id" to reactionSpaceId,
            ).apply {
                customData?.let {
                    this["custom_data"] = it
                }
            }.toJsonString()
        ).processResult()
    }

    suspend fun getListOfUserReactions(
        url: String,
        reactionSpaceId: String?,
        targetGroupId: String?,
        targetId: String?,
        reactionId: String?,
        reactedById: String?
    ): PaginationResponse<UserReaction> {
        val queryParams = arrayListOf<Pair<String, Any>>()
        queryParams.add("client_id" to clientId)
        if (!reactionSpaceId.isNullOrEmpty()) {
            queryParams.add("reaction_space_id" to reactionSpaceId)
        }
        if (!targetGroupId.isNullOrEmpty()) {
            queryParams.add("target_group_id" to targetGroupId)
        }
        if (!targetId.isNullOrEmpty()) {
            queryParams.add("target_id" to targetId)
        }
        if (!reactionId.isNullOrEmpty()) {
            queryParams.add("reaction_id" to reactionId)
        }
        if (!reactedById.isNullOrEmpty()) {
            queryParams.add("reacted_by_id" to reactedById)
        }
        return networkApiClient.get(url, accessToken = authKey, queryParameters = queryParams)
            .processResult()
    }

    suspend fun deleteUserReaction(
        url: String,
    ): LiveLikeEmptyResponse = withContext(Dispatchers.IO) {
        networkApiClient.delete(url, accessToken = authKey).processResult()
    }

    suspend fun getReactionsCounts(
        url: String,
        reactionSpaceId: String,
        targetIds: List<String>
    ): PaginationResponse<TargetUserReactionCount> {
        val queryParams = arrayListOf<Pair<String, Any>>()
        queryParams.add("client_id" to clientId)
        queryParams.add("reaction_space_id" to reactionSpaceId)
        for (target in targetIds) {
            queryParams.add("target_id" to target)
        }
        return networkApiClient.get(url, accessToken = authKey, queryParameters = queryParams)
            .processResult()
    }


}