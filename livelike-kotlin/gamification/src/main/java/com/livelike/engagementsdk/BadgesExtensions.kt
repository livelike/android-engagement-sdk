package com.livelike.engagementsdk

import com.livelike.common.LiveLikeKotlin
import com.livelike.common.profile
import com.livelike.engagementsdk.gamification.Badges

/**
 * Create a badges client and returns that
 **/
fun LiveLikeKotlin.badges(): Badges {
    val key = this.hashCode()
    return if (sdkInstanceWithBadgeClient.containsKey(key)) {
        sdkInstanceWithBadgeClient[key]!!
    } else {
        Badges.getInstance(
            sdkConfigurationOnce,
            profile().currentProfileOnce,
            uiScope,
            sdkScope,
            networkClient
        ).let {
            sdkInstanceWithBadgeClient[key] = it
            it
        }
    }
}

private val sdkInstanceWithBadgeClient = mutableMapOf<Int, Badges>()
