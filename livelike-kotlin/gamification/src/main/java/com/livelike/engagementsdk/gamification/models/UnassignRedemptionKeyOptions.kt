package com.livelike.engagementsdk.gamification.models

data class UnassignRedemptionKeyOptions(
val redemptionKeyId:String
)
