package com.livelike.engagementsdk

import com.livelike.common.LiveLikeKotlin
import com.livelike.common.profile
import com.livelike.engagementsdk.gamification.LiveLikeDigitalGoodsClient

/**
 * Create a redemption client and returns that
 **/
fun LiveLikeKotlin.digitalGoods(): LiveLikeDigitalGoodsClient {
    val key = this.hashCode()
    return if (sdkInstanceWithDigitalGoodsClient.containsKey(key)) {
        sdkInstanceWithDigitalGoodsClient[key]!!
    } else {
        LiveLikeDigitalGoodsClient.getInstance(
            sdkConfigurationOnce,
            profile().currentProfileOnce,
            sdkScope,
            uiScope,
            networkClient
        ).let {
            sdkInstanceWithDigitalGoodsClient[key] = it
            it
        }
    }
}


private val sdkInstanceWithDigitalGoodsClient = mutableMapOf<Int, LiveLikeDigitalGoodsClient>()
