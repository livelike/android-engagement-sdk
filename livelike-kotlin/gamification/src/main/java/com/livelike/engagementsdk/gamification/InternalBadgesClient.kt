package com.livelike.engagementsdk.gamification

import com.livelike.BaseClient
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.utils.validateUuid
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.gamification.models.Badge
import com.livelike.engagementsdk.gamification.models.BadgeProgress
import com.livelike.engagementsdk.gamification.models.ProfileBadge
import com.livelike.engagementsdk.gamification.models.ProfilesByBadge
import com.livelike.gamification.TEMPLATE_PROFILE_ID
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.processResult
import com.livelike.utils.LiveLikeException
import com.livelike.utils.NO_MORE_DATA
import com.livelike.utils.Once
import com.livelike.utils.PaginationResponse
import kotlinx.coroutines.CoroutineScope

internal class InternalBadgesClient constructor(
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    sdkScope: CoroutineScope,
    uiScope: CoroutineScope,
    private val networkApiClient: NetworkApiClient,
) : BaseClient(configurationOnce, currentProfileOnce, sdkScope, uiScope), Badges {

    private var profileBadgesResultMap = mutableMapOf<String, PaginationResponse<ProfileBadge>>()
    private var badgeProfilesResultMap = mutableMapOf<String, PaginationResponse<ProfilesByBadge>>()
    private var lastApplicationBadgePage: PaginationResponse<Badge>? = null

    /**
     * fetch all the badges associated to provided profile id in pages
     * to fetch next page function need to be called again with LiveLikePagination.NEXT and for first call as LiveLikePagination.FIRST
     **/
    override fun getProfileBadges(
        profileId: String,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ProfileBadge>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            if (!validateUuid(profileId)) {
                throw LiveLikeException("Invalid Profile ID")
            }
            val result = profileBadgesResultMap[profileId]
            val url = if (result == null || liveLikePagination == LiveLikePagination.FIRST) {
                networkApiClient.get(
                    pair.second.profileDetailUrlTemplate.replace(
                        TEMPLATE_PROFILE_ID, profileId
                    )
                ).processResult<LiveLikeProfile>().run {
                    this.badgesUrl
                }
            } else if (liveLikePagination == LiveLikePagination.PREVIOUS) {
                result.previous
            } else {
                result.next
            }
            url?.let { fetchUrl ->
                networkApiClient.get(fetchUrl).processResult<PaginationResponse<ProfileBadge>>()
                    .also {
                        profileBadgesResultMap[profileId] = it
                    }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }


    /**
     * fetch all the badges associated to the client id passed at initialization of sdk
     * to fetch next page function need to be called again with LiveLikePagination.NEXT and for first call as LiveLikePagination.FIRST
     **/
    override fun getApplicationBadges(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<Badge>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val url =
                if (lastApplicationBadgePage == null || liveLikePagination == LiveLikePagination.FIRST) {
                    pair.second.badgesUrl
                } else if (liveLikePagination == LiveLikePagination.PREVIOUS) {
                    lastApplicationBadgePage?.previous
                } else {
                    lastApplicationBadgePage?.next
                }
            url?.let {
                networkApiClient.get(url).processResult<PaginationResponse<Badge>>().also {
                    lastApplicationBadgePage = it
                }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }


    /**
     * fetch all the profile badges progressions for the passed badge Ids
     * @param profileId : id of the profile for which progressions to be looked
     * @param badgeIds : list of all badge-ids, it has a hard limit checkout rest api documentation for latest limit
     **/
    override fun getProfileBadgeProgress(
        profileId: String,
        badgeIds: List<String>,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<BadgeProgress>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            if (!validateUuid(profileId)) {
                throw LiveLikeException("Invalid Profile ID")
            }
            val result = networkApiClient.get(
                pair.second.profileDetailUrlTemplate.replace(
                    TEMPLATE_PROFILE_ID, profileId
                )
            ).processResult<LiveLikeProfile>()
            val badgeProgressURL = result.badgeProgressUrl
            val params = arrayListOf<Pair<String, String>>()
            for (id in badgeIds) {
                params.add("badge_id" to id)
            }
            networkApiClient.get(badgeProgressURL, queryParameters = params).processResult()
        }
    }


    override fun getBadgeProfiles(
        badgeId: String,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ProfilesByBadge>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val result = badgeProfilesResultMap[badgeId]
            if (result == null || liveLikePagination == LiveLikePagination.FIRST) {
                pair.second.badgeProfilesUrl
            } else {
                when (liveLikePagination) {
                    LiveLikePagination.NEXT -> result.next
                    LiveLikePagination.PREVIOUS -> result.previous
                    else -> null

                }
            }?.let { url ->
                val params = arrayListOf<Pair<String, String>>()
                params.add("badge_id" to badgeId)
                networkApiClient.get(url, accessToken = pair.first.accessToken, params)
                    .processResult<PaginationResponse<ProfilesByBadge>>().also {
                        badgeProfilesResultMap[badgeId] = it
                    }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }
}