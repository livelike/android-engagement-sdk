package com.livelike.engagementsdk.gamification.models

import com.google.gson.annotations.SerializedName


data class RewardActionInfo(
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    val url: String,
    @SerializedName("client_id")
    val clientId: String,
    @SerializedName("key")
    val key: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String? = null,
)