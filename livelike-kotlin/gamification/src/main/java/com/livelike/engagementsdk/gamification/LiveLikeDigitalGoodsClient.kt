package com.livelike.engagementsdk.gamification

import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.gamification.models.UnassignRedemptionKeyOptions
import com.livelike.engagementsdk.publicapis.LiveLikeCallback
import com.livelike.network.NetworkApiClient
import com.livelike.utils.Once
import kotlinx.coroutines.CoroutineScope

/**
All the apis related to redeemption, exposed here
 */
interface LiveLikeDigitalGoodsClient {
    /**
     * Retrieve all redemption codes associated with the current user profile.
     *
     * @param liveLikePagination pagination object for pagination
     * @param liveLikeCallback callback to receive the list of redemption keys
     **/
    fun getRedemptionKeys(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<RedemptionKey>>
    )

    /**
     * Retrieve all redemption codes associated with the current user profile.
     *
     * @param liveLikePagination pagination object for pagination
     * @param requestParams request parameters for applying filters
     * @param liveLikeCallback callback to receive the list of redemption keys
     **/
    fun getRedemptionKeys(
        liveLikePagination: LiveLikePagination,
        requestParams: GetRedemptionKeyRequestOptions?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<RedemptionKey>>
    )

    /**
     * Redeem a redemption code.
     *
     * @param redemptionKeyId id of the code being submitted
     * @param liveLikeCallback callback to receive the redeemed redemption key
     **/
    fun redeemKeyWithId(
        redemptionKeyId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<RedemptionKey>
    )

    /**
     * Redeem a redemption code.
     *
     * @param code the redemption code to be redeemed
     * @param liveLikeCallback callback to receive the redeemed redemption key
     **/
    fun redeemKeyWithCode(
        code: String, liveLikeCallback: com.livelike.common.LiveLikeCallback<RedemptionKey>
    )

    /**
     * Unassign a redemption key.
     *
     * @param requestParams parameters for unassigning the redemption key
     * @param liveLikeCallback callback to receive the unassigned redemption key
     **/
    fun unassignRedemptionKey(
        requestParams: UnassignRedemptionKeyOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<RedemptionKey>
    )

    companion object {

        fun getInstance(
            configurationOnce: Once<SdkConfiguration>,
            currentProfileOnce: Once<LiveLikeProfile>,
            sdkScope: CoroutineScope,
            uiScope: CoroutineScope,
            networkApiClient: NetworkApiClient
        ): LiveLikeDigitalGoodsClient = InternalDigitalGoodsClient(
            configurationOnce, currentProfileOnce, sdkScope, uiScope, networkApiClient
        )
    }
}