package com.livelike.engagementsdk

import com.livelike.common.LiveLikeKotlin
import com.livelike.common.profile
import com.livelike.engagementsdk.gamification.IQuestsClient

/**
 * Create a quest client and returns that
 **/
fun LiveLikeKotlin.quests(): IQuestsClient {
    val key = this.hashCode()
    return if (sdkInstanceWithQuestClient.containsKey(key)) {
        sdkInstanceWithQuestClient[key]!!
    } else {
        IQuestsClient.getInstance(
            sdkConfigurationOnce,
            profile().currentProfileOnce,
            uiScope,
            sdkScope,
            networkClient
        ).let {
            sdkInstanceWithQuestClient[key] = it
            it
        }
    }
}


private val sdkInstanceWithQuestClient = mutableMapOf<Int, IQuestsClient>()
