package com.livelike.engagementsdk.gamification

import com.google.gson.annotations.SerializedName

data class GetQuestsRequestOptions(
    val questIds: List<String> = emptyList(),
    val status: QuestStatus? = null
)

data class GetUserQuestsRequestOptions(
    val userQuestIds: List<String> = emptyList(),
    val status: UserQuestStatus? = null,
    val profileID: String? = null,
    val rewardStatus: QuestRewardStatus?,
    val questId: String? = null,
)

data class Quest(
    @field:SerializedName("id")
    val id: String,
    @field:SerializedName("client_id")
    val clientId: String,
    @field:SerializedName("name")
    val name: String,
    @field:SerializedName("description")
    val description: String? = null,
    @field:SerializedName("quest_tasks")
    val questTasks: List<QuestTask>,
    @field:SerializedName("created_at")
    val createdAt: String,
    @field:SerializedName("quest_rewards_url")
    val questRewardsUrl: String
)


enum class QuestStatus(val key: String) {
    @SerializedName("active")
    ACTIVE("active"),
    @SerializedName("not_started")
    NOT_STARTED("not_started"),
    @SerializedName("expired")
    EXPIRED("expired")
}

enum class QuestRewardStatus(val key: String) {
    @SerializedName("claimed")
    CLAIMED("claimed"),

    @SerializedName("unclaimed")
    UNCLAIMED("unclaimed")
}

data class QuestTask(
    @field:SerializedName("id")
    val id: String,
    @field:SerializedName("name")
    val name: String,
    @field:SerializedName("description")
    val description: String? = null,
    @field:SerializedName("quest_id")
    val questId: String,
    @field:SerializedName("created_at")
    val createdAt: String,
    @field:SerializedName("default_progress_increment")
    val defaultProgressIncrement: Int,
    @field:SerializedName("target_value")
    val targetValue: Int,
)

data class UserQuest(
    @field:SerializedName("id")
    val id: String,
    @field:SerializedName("profile_id")
    val profileId: String,
    @field:SerializedName("created_at")
    val createdAt: String,
    @field:SerializedName("completed_at")
    val completedAt: String? = null,
    @field:SerializedName("status")
    val status: UserQuestStatus,
    @field:SerializedName("user_quest_tasks")
    val userQuestTasks: List<UserQuestTask>,
    @field:SerializedName("quest")
    val quest: Quest,
    @field:SerializedName("user_quest_rewards_url")
    val userQuestRewardsUrl: String,
    @field:SerializedName("rewards_status")
    val rewardStatus: QuestRewardStatus,
    @field:SerializedName("rewards_claimed_at")
    val rewardClaimedAt: String? = null
)

enum class UserQuestStatus(val key: String) {
    @SerializedName("completed")
    COMPLETED("completed"),

    @SerializedName("incomplete")
    INCOMPLETE("incomplete")
}

data class UserQuestTask(
    @field:SerializedName("id")
    val id: String,
    @field:SerializedName("status")
    val status: UserQuestStatus,
    @field:SerializedName("created_at")
    val createdAt: String,
    @field:SerializedName("completed_at")
    val completedAt: String? = null,
    @field:SerializedName("progress")
    val progress: Float,
    @field:SerializedName("user_quest_id")
    val userQuestId: String,
    @field:SerializedName("quest_task")
    val questTask: QuestTask,
)

data class QuestReward(
    @SerializedName("id") val id: String,
    @SerializedName("quest_id") val questId: String,
    @SerializedName("reward_item_id") val rewardItemId: String,
    @SerializedName("reward_item_amount") val rewardItemAmount: Int,
    @SerializedName("reward_item_name") val rewardItemName: String,
)

data class UserQuestReward(
    @SerializedName("id") val id: String,
    @SerializedName("quest_reward") val questReward: QuestReward,
    @SerializedName("reward_status") val rewardStatus: QuestRewardStatus
)

internal data class ProgressResponse(
    @field:SerializedName("user_quest")
    val userQuest: UserQuest? = null
)