package com.livelike.engagementsdk.gamification

import com.google.gson.annotations.SerializedName
import com.livelike.BaseClient
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.core.data.models.RewardAttribute
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.gamification.models.GetInvokedRewardActionParams
import com.livelike.engagementsdk.gamification.models.InvokedRewardAction
import com.livelike.engagementsdk.gamification.models.RewardActionInfo
import com.livelike.network.NetworkApiClient
import com.livelike.realtime.RealTimeMessagingClient
import com.livelike.realtime.RealTimeMessagingClientConfig
import com.livelike.serialization.gson
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import com.livelike.utils.NO_MORE_DATA
import com.livelike.utils.Once
import com.livelike.utils.PaginationResponse
import com.livelike.utils.UNABLE_TO_LOAD_DATA
import com.livelike.utils.isoDateTimeFormat
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.threeten.bp.ZonedDateTime
import java.net.URLEncoder

private const val CLIENT_ID = "client_id"
private const val PROFILE_ID = "profile_id"
private const val REWARD_ACTION_KEY = "reward_action_key"
private const val PROGRAM_ID = "program_id"
private const val ATTRIBUTES = "attributes"
private const val ENCODING = "UTF-8"

internal class Rewards(
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    private val sdkScope: CoroutineScope,
    private val uiScope: CoroutineScope,
    private val networkApiClient: NetworkApiClient
) : BaseClient(configurationOnce, currentProfileOnce, sdkScope, uiScope),
    IRewardsClient {

    private var rewardClient: RealTimeMessagingClient? = null
    private var lastRewardItemsPage: PaginationResponse<RewardItem>? = null
    private var lastRewardActionsPage: PaginationResponse<RewardActionInfo>? = null

    /*map of rewardITemRequestOptions and last response*/
    private var lastRewardTransfersPageMap: MutableMap<RewardItemTransferRequestParams, PaginationResponse<TransferRewardItem>?> =
        mutableMapOf()

    private var rewardTransactionsPageMap: MutableMap<RewardTransactionsRequestParameters?, PaginationResponse<RewardTransaction>?> =
        mutableMapOf()

    private var invokedRewardActionPageMap: MutableMap<GetInvokedRewardActionParams?, PaginationResponse<InvokedRewardAction>?> =
        mutableMapOf()


    override var rewardEventsListener: RewardEventsListener? = null
        set(value) {
            value?.let {
                field = value
                subscribeToRewardEvents()
            } ?: run {
                unsubscribeToRewardEvents()
                field = null
            }
        }

    private fun subscribeToRewardEvents() {
        sdkScope.launch {
            configurationProfilePairOnce().let { pair ->
                pair.second.pubNubKey?.let { subscribeKey ->
                    rewardClient = RealTimeMessagingClient.getInstance(
                        RealTimeMessagingClientConfig(
                            subscribeKey,
                            pair.first.accessToken,
                            pair.first.id,
                            pair.second.pubnubPublishKey,
                            pair.second.pubnubOrigin,
                            pair.second.pubnubHeartbeatInterval,
                            pair.second.pubnubPresenceTimeout
                        ),
                        sdkScope
                    )
                    launch {
                        rewardClient!!.messageClientFlow.collect { message ->
                            rewardEventsListener?.let { listener ->
                                safeCallBack({ result, error ->
                                    result?.let { listener.onReceiveNewRewardItemTransfer(it) }
                                    error?.let { logError { it } }
                                }) {
                                    val eventType = message.event
                                    val payload = message.payload
                                    return@safeCallBack if (eventType == RewardEvent.REWARD_ITEM_TRANSFER_RECEIVED.key) {
                                        gson.fromJson(
                                            payload.toString(),
                                            TransferRewardItem::class.java
                                        )
                                    } else {
                                        throw Exception("Event Type received on different observer:$eventType,payload:$payload")
                                    }
                                }
                            }
                        }
                    }
                    pair.first.subscribeChannel.let {
                        rewardClient!!.subscribe(listOf(it))
                    }
                } ?: logDebug { "SDk config pubnubKey is null" }
            }
        }
    }

    private fun unsubscribeToRewardEvents() {
        safeCallBack { pair ->
            pair.first.subscribeChannel.let {
                rewardClient?.unsubscribe(listOf(it))
            }
        }
    }

    override fun getApplicationRewardItems(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<RewardItem>>
    ) {
        getApplicationRewardItems(
            liveLikePagination,
            ApplicationRewardItemsRequestParams(),
            liveLikeCallback
        )
    }


    override fun getApplicationRewardItems(
        liveLikePagination: LiveLikePagination,
        requestParams: ApplicationRewardItemsRequestParams,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<RewardItem>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val url =
                if (lastRewardItemsPage == null || liveLikePagination == LiveLikePagination.FIRST) {
                    pair.second.rewardItemsUrl
                } else if (liveLikePagination == LiveLikePagination.PREVIOUS) {
                    lastRewardItemsPage?.previous
                } else {
                    lastRewardItemsPage?.next
                }
            url?.let { fetchUrl ->
                val params = arrayListOf<Pair<String, String>>()
                requestParams.attributes?.let { attributes ->
                    for (entry in attributes) {
                        params.add(ATTRIBUTES to "${entry.key}:${entry.value}")
                    }
                }
                networkApiClient.get(fetchUrl, queryParameters = params)
                    .processResult<PaginationResponse<RewardItem>>()
                    .also {
                        lastRewardItemsPage = it
                    }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }

    override fun getRewardItemBalances(
        liveLikePagination: LiveLikePagination,
        rewardItemIds: List<String>,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<RewardItemBalance>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val result = rewardItemBalancesPagination[rewardItemIds]
            val requestPair =
                if (liveLikePagination == LiveLikePagination.FIRST || result == null) {
                    pair.first.rewardItemBalancesUrl?.let { url ->
                        val params = arrayListOf<Pair<String, String>>()
                        for (id in rewardItemIds) {
                            params.add("reward_item_id" to id)
                        }
                        params to url
                    }
                } else if (liveLikePagination == LiveLikePagination.PREVIOUS) {
                    emptyList<Pair<String, String>>() to result.previous
                } else {
                    emptyList<Pair<String, String>>() to result.next
                }
            requestPair?.second?.let {
                networkApiClient.get(it, pair.first.accessToken, requestPair.first)
                    .processResult<PaginationResponse<RewardItemBalance>>()
                    .results
            } ?: throw Exception(UNABLE_TO_LOAD_DATA)
        }
    }

    private val rewardItemBalancesPagination =
        mutableMapOf<List<String>, PaginationResponse<RewardItemBalance>>()


    override fun transferAmountToProfileId(
        rewardItemId: String,
        amount: Int,
        recipientProfileId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<TransferRewardItem>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.first.rewardItemTransferUrl?.let { url ->
                networkApiClient.post(
                    url, body = TransferRewardItemRequest(
                        recipientProfileId,
                        amount,
                        rewardItemId
                    ).toJsonString(), accessToken = pair.first.accessToken
                ).processResult()
            } ?: throw Exception(UNABLE_TO_LOAD_DATA)
        }
    }


    override fun getRewardItemTransfers(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<TransferRewardItem>>
    ) {
        return getRewardItemTransfers(
            liveLikePagination,
            RewardItemTransferRequestParams(null), liveLikeCallback
        )
    }


    override fun getRewardItemTransfers(
        liveLikePagination: LiveLikePagination,
        requestParams: RewardItemTransferRequestParams,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<TransferRewardItem>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val url =
                if (lastRewardTransfersPageMap[requestParams] == null || liveLikePagination == LiveLikePagination.FIRST) {
                    pair.first.rewardItemTransferUrl
                } else if (liveLikePagination == LiveLikePagination.PREVIOUS) {
                    lastRewardTransfersPageMap[requestParams]?.previous
                } else {
                    lastRewardTransfersPageMap[requestParams]?.next
                }
            url?.let { fetchUrl ->
                networkApiClient.get(
                    fetchUrl,
                    pair.first.accessToken,
                    queryParameters = requestParams.transferType?.let { listOf("transfer_type" to it.key) }
                        ?: emptyList()
                ).processResult<PaginationResponse<TransferRewardItem>>().also {
                    lastRewardTransfersPageMap[requestParams] = it
                }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }


    override fun getRewardTransactions(
        liveLikePagination: LiveLikePagination,
        requestParams: RewardTransactionsRequestParameters?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<RewardTransaction>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val url =
                if (rewardTransactionsPageMap[requestParams] == null || liveLikePagination == LiveLikePagination.FIRST) {
                    pair.second.rewardTransactionsUrl
                } else if (liveLikePagination == LiveLikePagination.PREVIOUS) {
                    rewardTransactionsPageMap[requestParams]?.previous
                } else {
                    rewardTransactionsPageMap[requestParams]?.next
                }
            url?.let { fetchUrl ->
                val params = arrayListOf<Pair<String, String>>()
                for (it in requestParams?.widgetIds ?: emptySet()) {
                    params.add("widget_id" to it)
                }
                for (it in requestParams?.widgetKindFilter ?: emptySet()) {
                    params.add("widget_kind" to it)
                }
                for (it in requestParams?.profileIds ?: emptyList()) {
                    params.add(PROFILE_ID to it)
                }
                for (it in requestParams?.rewardItemIds ?: emptyList()) {
                    params.add("reward_item_id" to it)
                }
                for (it in requestParams?.rewardActionKeys ?: emptyList()) {
                    params.add(REWARD_ACTION_KEY to it)
                }
                requestParams?.createdSince?.let {
                    params.add("created_since" to it.isoDateTimeFormat())
                }
                requestParams?.createdUntil?.let {
                    params.add("created_until" to it.isoDateTimeFormat())
                }
                networkApiClient.get(
                    fetchUrl,
                    pair.first.accessToken,
                    queryParameters = params
                )
                    .processResult<PaginationResponse<RewardTransaction>>()
                    .also {
                        rewardTransactionsPageMap[requestParams] = it
                    }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }


    override fun getRewardActions(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<RewardActionInfo>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val url =
                if (lastRewardActionsPage == null || liveLikePagination == LiveLikePagination.FIRST) {
                    pair.second.rewardActionsUrl
                } else if (liveLikePagination == LiveLikePagination.PREVIOUS) {
                    lastRewardActionsPage?.previous
                } else {
                    lastRewardActionsPage?.next
                }
            url?.let { fetchUrl ->
                val queryParams = arrayListOf<Pair<String, Any>>()
                if (liveLikePagination == LiveLikePagination.FIRST) {
                    queryParams.add(CLIENT_ID to pair.second.clientId)
                }
                networkApiClient.get(fetchUrl, queryParameters = queryParams)
                    .processResult<PaginationResponse<RewardActionInfo>>()
                    .also {
                        lastRewardActionsPage = it
                    }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }

    override fun getInvokedRewardActions(
        liveLikePagination: LiveLikePagination,
        invokedActionRequestParams: GetInvokedRewardActionParams?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<InvokedRewardAction>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val url =
                if (invokedRewardActionPageMap[invokedActionRequestParams] == null || liveLikePagination == LiveLikePagination.FIRST) {
                    pair.second.invokedActionsUrl
                } else if (liveLikePagination == LiveLikePagination.PREVIOUS) {
                    invokedRewardActionPageMap[invokedActionRequestParams]?.previous
                } else {
                    invokedRewardActionPageMap[invokedActionRequestParams]?.next
                }
            url?.let { fetchUrl ->
                val params = arrayListOf<Pair<String, String>>()
                params.add(CLIENT_ID to pair.second.clientId)

                for (it in invokedActionRequestParams?.profileIDs?.map { it.trim() }
                    ?.filter { it != "" } ?: emptyList()) {
                    params.add(PROFILE_ID to it)
                }
                for (it in invokedActionRequestParams?.rewardActionKeys?.map { it.trim() }
                    ?.filter { it != "" } ?: emptyList()) {
                    params.add(REWARD_ACTION_KEY to it)
                }
                invokedActionRequestParams?.programId?.let {
                    params.add(PROGRAM_ID to it)
                }

                invokedActionRequestParams?.attributes?.entries?.forEach {
                    val key = URLEncoder.encode(it.key, ENCODING)
                    val value = URLEncoder.encode(it.value, ENCODING)
                    params.add(ATTRIBUTES to "${key}:${value}")
                }

                networkApiClient.get(
                    fetchUrl,
                    pair.first.accessToken,
                    queryParameters = params
                ).processResult<PaginationResponse<InvokedRewardAction>>()
                    .also {
                        invokedRewardActionPageMap[invokedActionRequestParams] = it
                    }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }
}

/**
All the apis related to rewards item discovery, balance and transfer exposed here
 */
interface IRewardsClient {

    var rewardEventsListener: RewardEventsListener?

    /**
     * fetch all the rewards item associated to the client id passed at initialization of sdk
     * to fetch next page function need to be called again with LiveLikePagination.NEXT and for first call as LiveLikePagination.FIRST
     **/
    fun getApplicationRewardItems(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<RewardItem>>
    )

    /**
     * fetch all the rewards item associated to the client id passed at initialization of sdk
     * to fetch next page function need to be called again with LiveLikePagination.NEXT and for first call as LiveLikePagination.FIRST
     * @param requestParams allows filtering of rewards based off attribute tags
     **/
    fun getApplicationRewardItems(
        liveLikePagination: LiveLikePagination,
        requestParams: ApplicationRewardItemsRequestParams,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<RewardItem>>
    )

    /**
     * fetch all the current user's balance for the passed rewardItemIDs
     * in callback you will receive a map of rewardItemId and balance
     **/
    fun getRewardItemBalances(
        liveLikePagination: LiveLikePagination,
        rewardItemIds: List<String>,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<RewardItemBalance>>
    )

    /**
     * transfer specified reward item amount to the profileID
     * amount should be a positive whole number
     **/
    fun transferAmountToProfileId(
        rewardItemId: String,
        amount: Int,
        recipientProfileId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<TransferRewardItem>
    )

    /**
     * Retrieve all reward item transfers associated
     * with the current user profile
     **/
    fun getRewardItemTransfers(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<TransferRewardItem>>
    )

    /**
     * Retrieve all reward item transfers associated
     * with the current user profile
     * @param requestParams allows to filter transfer based on transferType i.e sent or received
     **/
    fun getRewardItemTransfers(
        liveLikePagination: LiveLikePagination,
        requestParams: RewardItemTransferRequestParams,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<TransferRewardItem>>
    )

    /**
     * Retrieve all reward transactions associated
     * with the current user profile
     * @param requestParams allows to filter reward transactions based on widget id or widget kind
     **/
    fun getRewardTransactions(
        liveLikePagination: LiveLikePagination,
        requestParams: RewardTransactionsRequestParameters?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<RewardTransaction>>
    )

    /**
     * Retrieve all reward actions for a client id
     *
     **/
    fun getRewardActions(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<RewardActionInfo>>
    )

    /**
     * Fetch all the invoked actions with the current user prpfile
     * @param invokedActionRequestParams allows to filter invoked actions based on program id or profile id or reward action keys
     **/
    fun getInvokedRewardActions(
        liveLikePagination: LiveLikePagination,
        invokedActionRequestParams: GetInvokedRewardActionParams?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<InvokedRewardAction>>
    )

    companion object {

        fun getInstance(
            configurationOnce: Once<SdkConfiguration>,
            currentProfileOnce: Once<LiveLikeProfile>,
            uiScope: CoroutineScope,
            sdkScope: CoroutineScope,
            networkApiClient: NetworkApiClient
        ): IRewardsClient = Rewards(
            configurationOnce,
            currentProfileOnce,
            sdkScope,
            uiScope,
            networkApiClient
        )
    }
}


data class ApplicationRewardItemsRequestParams(
    val attributes: List<RewardAttribute>? = null
)

data class RewardTransaction(
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    val url: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("widget_kind")
    val widgetKind: String,
    @SerializedName("widget_id")
    val widgetId: String,
    @SerializedName("reward_item_id")
    val rewardItemId: String,
    @SerializedName("reward_item_name")
    val rewardItemName: String,
    @SerializedName("reward_action_id")
    val rewardActionId: String,
    @SerializedName(REWARD_ACTION_KEY)
    val rewardActionKey: String,
    @SerializedName("reward_action_name")
    val rewardActionName: String,
    @SerializedName(PROFILE_ID)
    val profileId: String,
    @SerializedName("profile_nickname")
    val profileNickname: String,
    @SerializedName("reward_item_amount")
    val rewardItemAmount: String
)

data class RewardTransactionsRequestParameters(
    val widgetIds: Set<String> = emptySet(),
    //val widgetKindFilter: Set<WidgetType> = emptySet(),
    val widgetKindFilter: Set<String> = emptySet(),
    val profileIds: List<String> = emptyList(),
    val rewardItemIds: List<String> = emptyList(),
    val rewardActionKeys: List<String> = emptyList(),
    val createdSince: ZonedDateTime? = null,
    val createdUntil: ZonedDateTime? = null,
)

class RewardItemTransferRequestParams(internal val transferType: RewardItemTransferType?) {

    override fun hashCode(): Int {
        return transferType?.key?.hashCode() ?: 0
    }

    override fun equals(other: Any?): Boolean {
        return transferType?.equals(other) ?: true
    }
}


enum class RewardItemTransferType(val key: String) {
    SENT("sent"),
    RECEIVED("received")
}

data class RewardItemBalance(
    @SerializedName("reward_item_balance")
    val rewardItemBalance: Int,
    @SerializedName("reward_item_id")
    val rewardItemId: String,
    @SerializedName("reward_item_name")
    val rewardItemName: String
)

internal data class TransferRewardItemRequest(
    @SerializedName("recipient_profile_id")
    val receiverProfileId: String,
    @SerializedName("reward_item_amount")
    val rewardItemAmount: Int,
    @SerializedName("reward_item_id")
    val rewardItemId: String
)

data class TransferRewardItem(
    @SerializedName("id")
    val id: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("recipient_profile_id")
    val recipientProfileId: String,
    @SerializedName("reward_item_amount")
    val rewardItemAmount: Int,
    @SerializedName("reward_item_id")
    val rewardItemId: String,
    @SerializedName("sender_profile_id")
    val senderProfileId: String,
    @SerializedName("recipient_profile_name")
    val recipientProfileName: String,
    @SerializedName("reward_item_name")
    val rewardItemName: String,
    @SerializedName("sender_profile_name")
    val senderProfileName: String
)
