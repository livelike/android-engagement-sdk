package com.livelike.engagementsdk.gamification

import com.google.gson.annotations.SerializedName

data class GetRedemptionKeyRequestOptions(
    val status: RedemptionKeyStatus,
    @SerializedName("is_assigned")
    val isAssigned:Boolean? = true
)