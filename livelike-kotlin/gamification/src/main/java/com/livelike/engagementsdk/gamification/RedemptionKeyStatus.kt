package com.livelike.engagementsdk.gamification

enum class RedemptionKeyStatus {
    active, redeemed, inactive;
}