package com.livelike.engagementsdk.gamification

import com.livelike.BaseClient
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.gamification.models.UnassignRedemptionKeyOptions
import com.livelike.gamification.TEMPLATE_CODE
import com.livelike.gamification.TEMPLATE_REDEMPTION_KEY_ID
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import com.livelike.utils.NO_MORE_DATA
import com.livelike.utils.Once
import com.livelike.utils.PaginationResponse
import kotlinx.coroutines.CoroutineScope

internal class InternalDigitalGoodsClient(
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    sdkScope: CoroutineScope,
    uiScope: CoroutineScope,
    private val networkApiClient: NetworkApiClient
) : BaseClient(
    configurationOnce, currentProfileOnce, sdkScope, uiScope
), LiveLikeDigitalGoodsClient {

    private var redemptionCodesPageMap: MutableMap<GetRedemptionKeyRequestOptions?, PaginationResponse<RedemptionKey>?> =
        mutableMapOf()

    override fun getRedemptionKeys(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<RedemptionKey>>
    ) {
        return getRedemptionKeys(liveLikePagination, null, liveLikeCallback)
    }


    override fun getRedemptionKeys(
        liveLikePagination: LiveLikePagination,
        requestParams: GetRedemptionKeyRequestOptions?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<RedemptionKey>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val url =
                if (redemptionCodesPageMap[requestParams] == null || liveLikePagination == LiveLikePagination.FIRST) {
                    pair.second.redemptionKeysUrl
                } else if (liveLikePagination == LiveLikePagination.PREVIOUS) {
                    redemptionCodesPageMap[requestParams]?.previous
                } else {
                    redemptionCodesPageMap[requestParams]?.next
                }
            url?.let {
                val queryParams =
                    requestParams?.let {
                        listOf(
                            "status" to it.status.name,
                            "is_assigned" to it.isAssigned
                        )
                    } ?: listOf()
                networkApiClient.get(
                    url, accessToken = pair.first.accessToken, queryParameters = queryParams
                ).processResult<PaginationResponse<RedemptionKey>>().also {
                    redemptionCodesPageMap[requestParams] = it
                }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }

    override fun redeemKeyWithId(
        redemptionKeyId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<RedemptionKey>
    ) {
        internalRedeemCode({
            val pair = configurationProfilePairOnce()
            pair.second.redemptionKeyDetailUrlTemplate.replace(
                TEMPLATE_REDEMPTION_KEY_ID,
                redemptionKeyId
            )
        }, liveLikeCallback)
    }


    override fun redeemKeyWithCode(
        code: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<RedemptionKey>
    ) {
        internalRedeemCode({
            val pair = configurationProfilePairOnce()
            pair.second.redemptionKeyDetailByCodeUrlTemplate.replace(TEMPLATE_CODE, code)
        }, liveLikeCallback)
    }


    private fun internalRedeemCode(
        fetchUrl: suspend () -> String?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<RedemptionKey>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            networkApiClient.patch(
                fetchUrl() ?: throw Exception(NO_MORE_DATA),
                body = mapOf("status" to "redeemed").toJsonString(),
                accessToken = pair.first.accessToken
            ).processResult()
        }
    }


    override fun unassignRedemptionKey(
        requestParams: UnassignRedemptionKeyOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<RedemptionKey>
    ) {

        safeCallBack(liveLikeCallback) { pair ->
            val url = pair.second.redemptionKeyDetailUrlTemplate.replace(
                TEMPLATE_REDEMPTION_KEY_ID,
                requestParams.redemptionKeyId
            )
            networkApiClient.patch(
                url,
                body = mapOf("assigned_to" to null).toJsonString(supportNull = true),
                accessToken = pair.first.accessToken
            ).processResult()
        }
    }
}