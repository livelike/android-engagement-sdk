package com.livelike.engagementsdk.core.data.models

import com.google.gson.annotations.SerializedName

data class RewardItem(
    @SerializedName("id") val id: String,
    @SerializedName("url") val url: String,
    @SerializedName("client_id") val client_id: String,
    @SerializedName("name") val name: String,
    @SerializedName("attributes") val attributes: List<RewardAttribute>,
    @SerializedName("images") val images: List<RewardItemImage>
)

class RewardAttribute(
    key: String,
    value: String
) : Attribute(key, value)

open class Attribute(
    @SerializedName("key") val key: String,
    @SerializedName("value") val value: String
) {
    override fun toString(): String {
        return "Attribute(key='$key', value='$value')"
    }
}

data class RewardItemImage(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("image_url") val imageUrl: String,
    @SerializedName("mimetype") val mimetype: String
)