package com.livelike.engagementsdk

import com.livelike.common.LiveLikeKotlin
import com.livelike.common.profile
import com.livelike.engagementsdk.gamification.IRewardsClient

fun LiveLikeKotlin.rewards(): IRewardsClient {
    val key = this.hashCode()
    return if (sdkInstanceWithRewardsClient.containsKey(key)) {
        sdkInstanceWithRewardsClient[key]!!
    } else {
        IRewardsClient.getInstance(
            sdkConfigurationOnce,
            profile().currentProfileOnce,
            uiScope,
            sdkScope,
            networkClient
        ).let {
            sdkInstanceWithRewardsClient[key] = it
            it
        }
    }
}


private val sdkInstanceWithRewardsClient = mutableMapOf<Int, IRewardsClient>()
