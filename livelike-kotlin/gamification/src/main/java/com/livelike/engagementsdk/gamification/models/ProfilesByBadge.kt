package com.livelike.engagementsdk.gamification.models

import com.google.gson.annotations.SerializedName
import com.livelike.engagementsdk.LiveLikeProfile

data class ProfilesByBadge(
    @SerializedName("awarded_at")
    val awardedAt: String,
    @SerializedName("badge_id")
    val badgeId: String,
    @SerializedName("profile")
    val profile: LiveLikeProfile
)