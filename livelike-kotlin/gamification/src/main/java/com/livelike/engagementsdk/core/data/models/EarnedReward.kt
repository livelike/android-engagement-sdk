package com.livelike.engagementsdk.core.data.models

import com.google.gson.annotations.SerializedName

data class EarnedReward(
    @SerializedName("reward_item_id")
    val rewardId: String,
    @SerializedName("reward_item_amount")
    val rewardItemAmount: Int,
    @SerializedName("reward_item_name")
    val rewardItemName: String,
    @SerializedName("reward_action")
    val rewardAction: String,
    @SerializedName("new_score")
    val newScore: Int,
    @SerializedName("new_rank")
    val newRank: Int,
    @SerializedName("leaderboard_id")
    val leaderboardId: String
)