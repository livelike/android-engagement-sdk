package com.livelike.engagementsdk.gamification

import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.LiveLikeCallback
import com.livelike.gamification.InternalLiveLikeQuestClient
import com.livelike.network.NetworkApiClient
import com.livelike.utils.Once
import kotlinx.coroutines.CoroutineScope

interface IQuestsClient {

    /**
     * Get a list of quests based on the provided request options and pagination settings.
     *
     * @param requestOptions Request options to filter quests.
     * @param liveLikePagination Pagination options for fetching quests.
     * @param liveLikeCallback Callback to receive the list of quests or handle errors.
     */
    fun getQuests(
        requestOptions: GetQuestsRequestOptions,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<Quest>>
    )

    /**
     * Get a list of quests for a specific user based on the provided request options and pagination settings.
     *
     * @param requestOptions Request options to filter user quests.
     * @param liveLikePagination Pagination options for fetching user quests.
     * @param liveLikeCallback Callback to receive the list of user quests or handle errors.
     */
    fun getUserQuests(
        requestOptions: GetUserQuestsRequestOptions,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<UserQuest>>
    )


    /**
     * Start a user quest with the given quest ID.
     *
     * @param questId The unique identifier of the quest to start.
     * @param liveLikeCallback Callback to receive information about the started user quest or handle errors.
     */
    fun startUserQuest(
        questId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<UserQuest>
    )

    /**
     * Update the status of tasks within a user quest.
     *
     * @param userQuestID The unique identifier of the user quest to update.
     * @param userQuestTaskIDs The list of unique identifiers of tasks to update within the user quest.
     * @param status The new status to set for the specified tasks.
     * @param liveLikeCallback Callback to receive information about the updated user quest or handle errors.
     */
    fun updateUserQuestTask(
        userQuestID: String,
        userQuestTaskIDs: List<String>,
        status: UserQuestStatus,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<UserQuest>
    )


    /**
     * Increment the progress of a specific user quest task.
     *
     * @param userQuestTaskID The unique identifier of the user quest task to update.
     * @param customIncrement A custom value by which to increment the task's progress (optional).
     * @param liveLikeCallback Callback to receive information about the updated user quest or handle errors.
     */
    fun incrementUserQuestTaskProgress(
        userQuestTaskID: String,
        customIncrement: Float? = null,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<UserQuest>
    )


    /**
     * Set the progress of a specific user quest task to a given value.
     *
     * @param userQuestTaskID The unique identifier of the user quest task to update.
     * @param progress The new progress value to set for the task.
     * @param liveLikeCallback Callback to receive information about the updated user quest or handle errors.
     */
    fun setUserQuestTaskProgress(
        userQuestTaskID: String,
        progress: Float,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<UserQuest>
    )

    /**
     * Claim rewards associated with a specific user quest.
     *
     * @param userQuestID The unique identifier of the user quest for which rewards are to be claimed.
     * @param liveLikeCallback Callback to receive information about the claimed rewards or handle errors.
     */
    fun claimUserQuestRewards(userQuestID: String, liveLikeCallback: com.livelike.common.LiveLikeCallback<UserQuest>)


    /**
     * Get the rewards associated with a specific quest.
     *
     * @param questId The unique identifier of the quest for which rewards are to be retrieved.
     * @param liveLikePagination Pagination options for fetching the rewards.
     * @param liveLikeCallback Callback to receive the list of quest rewards or handle errors.
     */
    fun getQuestRewards(
        questId: String,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<QuestReward>>
    )

    /**
     * Retrieve rewards associated with a specific user quest.
     *
     * @param userQuestId The unique identifier of the user's quest to retrieve rewards for.
     * @param rewardStatus Filter rewards by status (e.g., claimed, pending).
     * @param liveLikePagination Pagination options for fetching user quest rewards.
     * @param liveLikeCallback Callback to receive the list of user quest rewards or handle errors.
     */
    fun getUserQuestRewards(
        userQuestId: String,
        rewardStatus: QuestRewardStatus?,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<UserQuestReward>>
    )

    /**
     * Retrieve rewards associated with a list of user quests.
     *
     * @param userQuestIds The list of unique identifiers of user quests to retrieve rewards for.
     * @param rewardStatus Filter rewards by status (e.g., claimed, pending).
     * @param liveLikePagination Pagination options for fetching user quest rewards.
     * @param liveLikeCallback Callback to receive the list of user quest rewards or handle errors.
     */
    fun getUserQuestRewards(
        userQuestIds: List<String>,
        rewardStatus: QuestRewardStatus?,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<UserQuestReward>>
    )

    companion object {

        fun getInstance(
            configurationOnce: Once<SdkConfiguration>,
            currentProfileOnce: Once<LiveLikeProfile>,
            uiScope: CoroutineScope,
            sdkScope: CoroutineScope,
            networkApiClient: NetworkApiClient
        ): IQuestsClient = InternalLiveLikeQuestClient(
            configurationOnce, currentProfileOnce, sdkScope, uiScope, networkApiClient
        )
    }
}