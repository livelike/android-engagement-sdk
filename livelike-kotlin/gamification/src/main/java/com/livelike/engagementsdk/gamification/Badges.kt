package com.livelike.engagementsdk.gamification

import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.gamification.models.Badge
import com.livelike.engagementsdk.gamification.models.BadgeProgress
import com.livelike.engagementsdk.gamification.models.ProfileBadge
import com.livelike.engagementsdk.gamification.models.ProfilesByBadge
import com.livelike.engagementsdk.publicapis.LiveLikeCallback
import com.livelike.network.NetworkApiClient
import com.livelike.utils.Once
import kotlinx.coroutines.CoroutineScope

interface Badges {

    /**
     * Get a list of profile badges associated with a specific profile.
     *
     * @param profileId The ID of the profile for which badges are requested.
     * @param liveLikePagination Pagination configuration for fetching the badges.
     * @param liveLikeCallback Callback to receive the list of profile badges or handle errors.
     */
    fun getProfileBadges(
        profileId: String,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ProfileBadge>>
    )

    /**
     * Get a list of application badges.
     *
     * @param liveLikePagination Pagination configuration for fetching the application badges.
     * @param liveLikeCallback Callback to receive the list of application badges or handle errors.
     */
    fun getApplicationBadges(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<Badge>>
    )
    /**
     * Get badge progress for a specific profile.
     *
     * @param profileId The ID of the profile for which badge progress is being fetched.
     * @param badgeIds List of badge IDs to retrieve progress for.
     * @param liveLikeCallback Callback to receive the list of badge progress or handle errors.
     */
    fun getProfileBadgeProgress(
        profileId: String,
        badgeIds: List<String>,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<BadgeProgress>>
    )

    /**
     * Get profiles associated with a specific badge.
     *
     * @param badgeId The ID of the badge for which associated profiles are being fetched.
     * @param liveLikePagination Pagination options for fetching profiles.
     * @param liveLikeCallback Callback to receive the list of profiles associated with the badge or handle errors.
     */
    fun getBadgeProfiles(
        badgeId: String,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ProfilesByBadge>>
    )

    companion object {

        fun getInstance(
            configurationOnce: Once<SdkConfiguration>,
            currentProfileOnce: Once<LiveLikeProfile>,
            uiScope: CoroutineScope,
            sdkScope: CoroutineScope,
            networkApiClient: NetworkApiClient
        ): Badges = InternalBadgesClient(
            configurationOnce, currentProfileOnce, sdkScope, uiScope, networkApiClient
        )
    }

}
