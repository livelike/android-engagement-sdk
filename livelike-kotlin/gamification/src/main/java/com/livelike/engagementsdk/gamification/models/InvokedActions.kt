package com.livelike.engagementsdk.gamification.models

import com.google.gson.annotations.SerializedName
import com.livelike.engagementsdk.core.data.models.Attribute
import com.livelike.engagementsdk.core.data.models.EarnedReward


data class InvokedRewardAction(
    @SerializedName("client_id")
    val clientId: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("created_at")
    val createdAt:String,
    @SerializedName("program_id")
    val programId:String,
    @SerializedName("profile_id")
    val profileId:String,
    @SerializedName("profile_nickname")
    val profileNickname:String,
    @SerializedName("reward_action_key")
    val rewardActionKey:String,
    @SerializedName("code")
    val code:String? = null,
    @SerializedName("rewards")
    val rewards: List<EarnedReward>,
    @SerializedName("attributes")
    val attributes: List<Attribute>,
)

data class GetInvokedRewardActionParams(
    val programId: String? = null,
    val rewardActionKeys: List<String> = emptyList(),
    val profileIDs: List<String> = emptyList(),
    val attributes: Map<String, String> = emptyMap(),
)