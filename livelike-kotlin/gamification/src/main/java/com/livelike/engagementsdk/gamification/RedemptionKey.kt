package com.livelike.engagementsdk.gamification

import com.google.gson.annotations.SerializedName

data class RedemptionKey(
    @SerializedName("id") val id: String,
    @SerializedName("client_id") val clientId: String,
    @SerializedName("name") val name: String,
    @SerializedName("description") val description: String? = null,
    @SerializedName("code") val code: String,
    @SerializedName("created_at") val createdAt: String,
    @SerializedName("redeemed_at") val redeemedAt: String? = null,
    @SerializedName("redeemed_by") val redeemedBy: String? = null,
    @SerializedName("status") val status: RedemptionKeyStatus
)