package com.livelike.gamification


import com.livelike.BaseClient
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.gamification.GetQuestsRequestOptions
import com.livelike.engagementsdk.gamification.GetUserQuestsRequestOptions
import com.livelike.engagementsdk.gamification.IQuestsClient
import com.livelike.engagementsdk.gamification.ProgressResponse
import com.livelike.engagementsdk.gamification.Quest
import com.livelike.engagementsdk.gamification.QuestReward
import com.livelike.engagementsdk.gamification.QuestRewardStatus
import com.livelike.engagementsdk.gamification.UserQuest
import com.livelike.engagementsdk.gamification.UserQuestReward
import com.livelike.engagementsdk.gamification.UserQuestStatus
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import com.livelike.utils.LiveLikeException
import com.livelike.utils.NO_MORE_DATA
import com.livelike.utils.Once
import com.livelike.utils.PaginationResponse
import com.livelike.utils.getUrlForPaginationWithRequestParam
import kotlinx.coroutines.CoroutineScope

internal class InternalLiveLikeQuestClient(
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    sdkScope: CoroutineScope,
    uiScope: CoroutineScope,
    private val networkApiClient: NetworkApiClient
) : BaseClient(configurationOnce, currentProfileOnce, sdkScope, uiScope),
    IQuestsClient {

    private var questResultMap = mutableMapOf<String, PaginationResponse<Quest>>()
    private var userQuestResultMap = mutableMapOf<String, PaginationResponse<UserQuest>>()
    private val questRewardsMap = mutableMapOf<String, PaginationResponse<QuestReward>>()
    private val userQuestRewardsMap = mutableMapOf<String, PaginationResponse<UserQuestReward>>()
    override fun getQuests(
        requestOptions: GetQuestsRequestOptions,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<Quest>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val key = requestOptions.toJsonString()
            val url = getUrlForPaginationWithRequestParam(
                liveLikePagination,
                pair.second.questsUrl,
                key,
                questResultMap
            )
            url?.let { fetchUrl ->
                val params = arrayListOf<Pair<String, String>>()
                requestOptions.questIds.let { ids ->
                    for (id in ids) {
                        params.add("id" to id)
                    }
                }
                requestOptions.status?.let {
                    params.add("status" to it.key)
                }

                networkApiClient.get(
                    fetchUrl, pair.first.accessToken, queryParameters = params
                ).processResult<PaginationResponse<Quest>>().also {
                    questResultMap[key] = it
                }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }

    override fun getUserQuests(
        requestOptions: GetUserQuestsRequestOptions,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<UserQuest>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val key = requestOptions.toJsonString()
            val url = getUrlForPaginationWithRequestParam(
                liveLikePagination,
                pair.second.userQuestsUrlTemplate,
                key,
                userQuestResultMap
            )
            url?.let { fetchUrl ->
                val newFetchUrl = requestOptions.profileID?.let {
                    fetchUrl.replace(TEMPLATE_PROFILE_ID, it)
                } ?: fetchUrl.replace(TEMPLATE_PROFILE_ID, pair.first.id)
                val params = arrayListOf<Pair<String, String>>()
                // params.add("client_id" to pair.second.clientId)
                requestOptions.userQuestIds.let { ids ->
                    for (id in ids) {
                        params.add("id" to id)
                    }
                }
                requestOptions.status?.let {
                    params.add("status" to it.name)
                }
                requestOptions.rewardStatus?.let {
                    params.add("rewards_status" to it.name)
                }
                requestOptions.questId?.let {
                    params.add("quest_id" to it)
                }
                networkApiClient.get(
                    newFetchUrl, pair.first.accessToken, queryParameters = params
                ).processResult<PaginationResponse<UserQuest>>().also {
                    userQuestResultMap[key] = it
                }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }

    override fun startUserQuest(
        questId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<UserQuest>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.userQuestsUrl.let {
                networkApiClient.post(
                    it, body = mapOf(
                        "profile_id" to pair.first.id, "quest_id" to questId
                    ).toJsonString(), accessToken = pair.first.accessToken
                ).processResult()
            }
        }
    }

    override fun updateUserQuestTask(
        userQuestID: String,
        userQuestTaskIDs: List<String>,
        status: UserQuestStatus,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<UserQuest>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.userQuestDetailUrlTemplate.replace(
                TEMPLATE_USER_QUEST_ID, userQuestID
            ).let {
                networkApiClient.patch(
                    it,
                    body = mapOf("user_quest_tasks" to userQuestTaskIDs.map { task ->
                        mapOf("id" to task, "status" to status.key)
                    }).toJsonString(),
                    accessToken = pair.first.accessToken
                ).processResult()
            }
        }
    }


    override fun incrementUserQuestTaskProgress(
        userQuestTaskID: String,
        customIncrement: Float?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<UserQuest>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.userQuestTaskProgressUrl.let { fetchUrl ->
                val result = networkApiClient.post(fetchUrl,
                    body = (customIncrement?.let {
                        mapOf(
                            "user_quest_task_id" to userQuestTaskID,
                            "custom_increment" to it,
                        )
                    } ?: mapOf("user_quest_task_id" to userQuestTaskID)).toJsonString(),
                    accessToken = pair.first.accessToken).processResult<ProgressResponse>()
                result.userQuest ?: throw Exception("No quest task returned")
            }
        }
    }


    override fun setUserQuestTaskProgress(
        userQuestTaskID: String,
        progress: Float,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<UserQuest>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.userQuestTaskProgressUrl.let { fetchUrl ->
                val result = networkApiClient.post(
                    fetchUrl, body = mapOf(
                        "user_quest_task_id" to userQuestTaskID,
                        "custom_progress" to progress,
                    ).toJsonString(), accessToken = pair.first.accessToken
                ).processResult<ProgressResponse>()
                result.userQuest ?: throw Exception("No quest task returned")
            }
        }
    }


    override fun claimUserQuestRewards(
        userQuestID: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<UserQuest>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.userQuestDetailUrlTemplate.let {
                val fetchUrl = it.replace(TEMPLATE_USER_QUEST_ID, userQuestID)
                networkApiClient.patch(
                    fetchUrl,
                    body = mapOf("rewards_status" to "claimed").toJsonString(),
                    accessToken = pair.first.accessToken
                ).processResult()
            }
        }
    }


    override fun getQuestRewards(
        questId: String,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<QuestReward>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val url = getUrlForPaginationWithRequestParam(
                liveLikePagination,
                pair.second.questRewardsUrl!!,
                questId,
                questRewardsMap
            )
            url?.let { fetchUrl ->
                networkApiClient.get(
                    fetchUrl,
                    accessToken = pair.first.accessToken,
                    queryParameters = listOf(QUEST_ID_KEY to questId)
                ).processResult<PaginationResponse<QuestReward>>()
                    .also {
                        questRewardsMap[questId] = it
                    }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }


    override fun getUserQuestRewards(
        userQuestId: String,
        rewardStatus: QuestRewardStatus?,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<UserQuestReward>>
    ) {
        getUserQuestRewards(listOf(userQuestId), rewardStatus, liveLikePagination, liveLikeCallback)
    }

    override fun getUserQuestRewards(
        userQuestIds: List<String>,
        rewardStatus: QuestRewardStatus?,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<UserQuestReward>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val params = arrayListOf<Pair<String, String>>()
            for (userQuestId in userQuestIds)
                params.add(USER_QUEST_ID_KEY to userQuestId)
            if (rewardStatus != null) {
                params.add(REWARD_STATUS_KEY to rewardStatus.key)
            }
            val key = params.toJsonString()
            val url = getUrlForPaginationWithRequestParam(
                liveLikePagination,
                pair.second.userQuestRewardsUrl,
                key,
                userQuestRewardsMap
            )
            url?.let { fetchUrl ->
                networkApiClient.get(
                    fetchUrl, accessToken = pair.first.accessToken, queryParameters = params
                ).processResult<PaginationResponse<UserQuestReward>>()
                    .also {
                        userQuestRewardsMap[params.toJsonString()] = it
                    }.results
            } ?: throw LiveLikeException(NO_MORE_DATA)
        }
    }
}