package com.livelike.gamification

internal const val TEMPLATE_CODE = "{code}"
internal const val TEMPLATE_REDEMPTION_KEY_ID = "{redemption_key_id}"

internal const val TEMPLATE_PROFILE_ID = "{profile_id}"
internal const val TEMPLATE_USER_QUEST_ID = "{user_quest_id}"

internal const val USER_QUEST_ID_KEY = "user_quest_id"
internal const val QUEST_ID_KEY = "quest_id"
internal const val REWARD_STATUS_KEY = "reward_status"