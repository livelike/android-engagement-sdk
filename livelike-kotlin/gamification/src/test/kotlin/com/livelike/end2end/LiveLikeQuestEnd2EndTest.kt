package com.livelike.end2end

import com.livelike.common.AccessTokenDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.common.profile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.gamification.GetQuestsRequestOptions
import com.livelike.engagementsdk.gamification.GetUserQuestsRequestOptions
import com.livelike.engagementsdk.gamification.Quest
import com.livelike.engagementsdk.gamification.QuestTask
import com.livelike.engagementsdk.gamification.UserQuestStatus
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.engagementsdk.quests
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import com.livelike.utils.LiveLikeException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.UUID
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class LiveLikeQuestEnd2EndTest {

    private val testDispatcher = StandardTestDispatcher()
    private lateinit var clientId: String
    private lateinit var producerToken: String

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        producerToken = System.getenv("producerToken")
        clientId = System.getenv("clientId")
    }

    @After
    fun tearDown() {
        testDispatcher.cancel()
    }


//      This test goes through a basic flow of Creatig a Quest, assigning QuestTasks,
//       starting a Quest, updating the UserQuest status, deleting UserQuest and Quest

    @Test
    fun testQuest() = runTest {
        val liveLikeKotlin = LiveLikeKotlin(
            clientId,
            accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return producerToken
                }

                override fun storeAccessToken(accessToken: String?) {

                }

            })
        val sdkConfiguration = liveLikeKotlin.sdkConfigurationOnce()
        val profile = liveLikeKotlin.profile().currentProfileOnce()

        //Step 1A : Create Quest
        val quest: Quest = liveLikeKotlin.networkClient.post(
            sdkConfiguration.questsUrl, mapOf(
                "client_id" to clientId,
                "name" to "Quest End2EndTest ${UUID.randomUUID()}"
            ).toJsonString(), accessToken = producerToken
        ).processResult()

        assert(quest.id.isNotEmpty())

        //Step 1B: Verify Quest with Backend
        val getQuest = suspendCoroutine { cont ->
            liveLikeKotlin.quests().getQuests(
                GetQuestsRequestOptions(questIds = listOf(quest.id)), LiveLikePagination.FIRST
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(getQuest.isNotEmpty() && getQuest[0].id == quest.id)

        //STEP 2: Create Quests Tasks and Verify their existence

        //Creating quest task 1
        val questTask1: QuestTask = liveLikeKotlin.networkClient.post(
            sdkConfiguration.questTasksUrl,
            mapOf("quest_id" to quest.id, "name" to "e2eQuestTask1").toJsonString(),
            producerToken
        ).processResult()

        val questTask2: QuestTask = liveLikeKotlin.networkClient.post(
            sdkConfiguration.questTasksUrl,
            mapOf("quest_id" to quest.id, "name" to "e2eQuestTask2").toJsonString(),
            producerToken
        ).processResult()


        //STEP 3: Test starting a Quest (creation of UserQuest)

        //STEP 3A - Start a Quest (Create `UserQuest`)

        val userQuest = suspendCoroutine { cont ->
            liveLikeKotlin.quests().startUserQuest(quest.id) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        //STEP 3B - Verify that a `UserQuest` has been created and assigned to current profile
        val userQuests = suspendCoroutine { cont ->
            liveLikeKotlin.quests().getUserQuests(
                GetUserQuestsRequestOptions(listOf(userQuest.id), rewardStatus = null),
                LiveLikePagination.FIRST
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        assert(userQuests.isNotEmpty() && userQuests[0].id.isNotEmpty() && userQuests[0].profileId == profile.id)

        val firstUserQuestTask = userQuest.userQuestTasks.first()

        //STEP 4 - Test UserQuestTask.status change
        //STEP 4A - Change state of UsetQuestTak to `.completed`
        val updateTask = suspendCoroutine { cont ->
            liveLikeKotlin.quests().updateUserQuestTask(
                userQuest.id, listOf(firstUserQuestTask.id), UserQuestStatus.COMPLETED
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        //STEP 4B - Verify that `UserQuestTask` status has been changed to `.completed`
        val newUserQuests = suspendCoroutine { cont ->
            liveLikeKotlin.quests().getUserQuests(
                GetUserQuestsRequestOptions(listOf(userQuest.id), rewardStatus = null),
                LiveLikePagination.FIRST
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        assert(newUserQuests.isNotEmpty() && newUserQuests[0].userQuestTasks.first { it.id == firstUserQuestTask.id }.status == UserQuestStatus.COMPLETED)

        //STEP 5 - Testing deletion of a UserQuest
        val deleteUserQuest: LiveLikeEmptyResponse = liveLikeKotlin.networkClient.delete(
            "${sdkConfiguration.userQuestsUrl}${userQuest.id}/",
            producerToken
        ).processResult()

        //STEP 5B - Verify deletion of a UserQuest from backend
        val newUserQuests2 = suspendCoroutine { cont ->
            liveLikeKotlin.quests().getUserQuests(
                GetUserQuestsRequestOptions(listOf(userQuest.id), rewardStatus = null),
                LiveLikePagination.FIRST
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        assert(newUserQuests2.isEmpty())

        val userQuestRewards = suspendCoroutine { cont ->
            liveLikeKotlin.quests().getUserQuestRewards(
                listOf(userQuest.profileId),
                rewardStatus = null,
                LiveLikePagination.FIRST
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(userQuestRewards.isEmpty())

        //STEP 6 - Test deleting Quest
        val questDelete: LiveLikeEmptyResponse = liveLikeKotlin.networkClient.delete(
            sdkConfiguration.questDetailUrlTemplate.replace(
                "quest_id",
                quest.id
            ), producerToken
        ).processResult()

    }
}