package com.livelike.engagementsdk.gamification

import com.livelike.common.AccessTokenDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.common.getNetworkSuccessResultForFile
import com.livelike.common.initSDK
import com.livelike.common.user
import com.livelike.engagementsdk.badges
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.network.NetworkApiClient
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

@OptIn(ExperimentalCoroutinesApi::class)
class InternalBadgesClientTest {
    private val testDispatcher = StandardTestDispatcher()

    @MockK
    lateinit var client: NetworkApiClient

    @Before
    fun setUp() {
        // do the setup
        MockKAnnotations.init(this)
        Dispatchers.setMain(testDispatcher)
    }

    private suspend fun setUpSDK(): LiveLikeKotlin {
//        if (this::sdk.isInitialized) return sdk
        coEvery {
            client.get(
                "http://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4"
            )
        } returns "application_success.json".getNetworkSuccessResultForFile(
            javaClass.classLoader
        )
        coEvery {
            client.get(
                "https://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4/profile/",
                accessToken = any()
            )
        } returns "profile_fetch_success.json".getNetworkSuccessResultForFile(javaClass.classLoader)
        val sdk =
            initSDK(client, testDispatcher, accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJibGFzdHJ0IiwiaWQiOiJlZGFiMzNlZS0yODgwLTRkZWUtOTIxZC03ZjRkM2Q3YTQ1MDQiLCJjbGllbnRfaWQiOiJDZHNkTEtMM21TbGx5U3h5TzJValNSUkg0RXFGbVVGYmZTWFRaV1c0IiwiaWF0IjoxNjY5NzE2NjYxLCJhY2Nlc3NfdG9rZW4iOiIwNmMwNjMwZjJhY2UxYzliMmIyNWU5ZDk2NmRlOTcxZjUyODE1ODg0In0.OfxWOLCozRhL3aKzxwQMc2Fn7XmAcpuJhcdEReCwIBo"
                }

                override fun storeAccessToken(accessToken: String?) {

                }
            })
        sdk.user().currentProfileOnce()
        return sdk
    }

    @After
    fun tearDown() {
        testDispatcher.cancel()
    }

    @Test
    fun `getProfileBadges test`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/profiles/93464785-5972-4f11-8ee9-fd60c560f072/",
                accessToken = any(),
                queryParameters = any()
            )
        } answers { "profile.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
        coEvery {
            client.get(
                "https://localhost/api/v1/profiles/93464785-5972-4f11-8ee9-fd60c560f072/badges/",
                accessToken = any(),
                queryParameters = any()
            )
        } answers { "profile_badges.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.badges().getProfileBadges("93464785-5972-4f11-8ee9-fd60c560f072",
                LiveLikePagination.FIRST){ result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.isNotEmpty())
//        assert(res.size == res.count)
        assert(res[0].badge.id == "f3f85706-5fab-43b9-a94d-69fcccf23154")

    }

    @Test
    fun `getApplicationBadges test`() = runTest {
        val sdk = setUpSDK()

        coEvery {
            client.get(
                "https://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4/badges/",
                accessToken = any(),
                queryParameters = any()
            )
        } answers { "application_badges.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.badges().getApplicationBadges(
                LiveLikePagination.FIRST){ result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.isNotEmpty())
//        assert(res.size == 20)
//        assert(res.count == 33)
        assert(res[0].id == "2f4b2559-124c-40e4-a4d9-17e778c6384f")

    }

    @Test
    fun `getProfileBadgeProgress test`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/profiles/93464785-5972-4f11-8ee9-fd60c560f072/",
                accessToken = any(),
                queryParameters = any()
            )
        } answers { "profile.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
        coEvery {
            client.get(
                "https://localhost/api/v1/profiles/93464785-5972-4f11-8ee9-fd60c560f072/badge-progress/",
                accessToken = any(),
                queryParameters = listOf("badge_id" to "f3f85706-5fab-43b9-a94d-69fcccf23154")
            )
        } answers { "badge_progress.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.badges().getProfileBadgeProgress(
                "93464785-5972-4f11-8ee9-fd60c560f072",
                listOf("f3f85706-5fab-43b9-a94d-69fcccf23154")){ result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }

        }
        advanceUntilIdle()
        assert(res.isNotEmpty())
        assert(res.size == 1)
        assert(res[0].badge.id == "f3f85706-5fab-43b9-a94d-69fcccf23154")

    }

    @Test
    fun `getBadgeProfiles test`() = runTest {
        val sdk = setUpSDK()
        sdk.user().currentProfileOnce()
        advanceUntilIdle()
        coEvery {
            client.get(
                "https://localhost/api/v1/badge-profiles/",
                accessToken = any(),
                queryParameters = listOf("badge_id" to "f3f85706-5fab-43b9-a94d-69fcccf23154")
            )
        } answers { "badge-profiles.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->

            sdk.badges().getBadgeProfiles(
                "f3f85706-5fab-43b9-a94d-69fcccf23154",
                LiveLikePagination.FIRST,){ result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.isNotEmpty())
        assert(res.size == 1)
        assert(res[0].profile.id == "7b30e359-72d6-46b4-97fb-73b5b4a99c31")
    }
}
