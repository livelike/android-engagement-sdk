package com.livelike.engagementsdk.gamification

import com.livelike.common.AccessTokenDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.common.getNetworkSuccessResultForFile
import com.livelike.common.initSDK
import com.livelike.common.user
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.gamification.models.GetInvokedRewardActionParams
import com.livelike.engagementsdk.rewards
import com.livelike.network.NetworkApiClient
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

@OptIn(ExperimentalCoroutinesApi::class)
class RewardsTest {
    private val testDispatcher = StandardTestDispatcher()

    @MockK
    lateinit var client: NetworkApiClient

    @Before
    fun setUp() {
        // do the setup
        MockKAnnotations.init(this)
        Dispatchers.setMain(testDispatcher)
    }

    private suspend fun setUpSDK(): LiveLikeKotlin {
//        if (this::sdk.isInitialized) return sdk
        coEvery { client.get("http://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4") } returns "application_success.json".getNetworkSuccessResultForFile(
            javaClass.classLoader
        )
        coEvery {
            client.get(
                "https://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4/profile/",
                accessToken = any()
            )
        } returns "profile_fetch_success.json".getNetworkSuccessResultForFile(javaClass.classLoader)
        val sdk =
            initSDK(client, testDispatcher, accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJibGFzdHJ0IiwiaWQiOiJlZGFiMzNlZS0yODgwLTRkZWUtOTIxZC03ZjRkM2Q3YTQ1MDQiLCJjbGllbnRfaWQiOiJDZHNkTEtMM21TbGx5U3h5TzJValNSUkg0RXFGbVVGYmZTWFRaV1c0IiwiaWF0IjoxNjY5NzE2NjYxLCJhY2Nlc3NfdG9rZW4iOiIwNmMwNjMwZjJhY2UxYzliMmIyNWU5ZDk2NmRlOTcxZjUyODE1ODg0In0.OfxWOLCozRhL3aKzxwQMc2Fn7XmAcpuJhcdEReCwIBo"
                }

                override fun storeAccessToken(accessToken: String?) {

                }
            })
        sdk.user().currentProfileOnce()
        return sdk
    }

    @After
    fun tearDown() {
        testDispatcher.cancel()
    }


    @Test
    fun `test same client instance from sdk`() = runTest {
        val sdk = setUpSDK()
        assert(sdk.rewards() == sdk.rewards())
    }

    @Test
    fun `test different client instance from different sdk instance`() = runTest {
        val sdk1 = setUpSDK()
        val sdk2 = setUpSDK()
        assert(sdk1.rewards() != sdk2.rewards() && sdk1.rewards() == sdk1.rewards() && sdk2.rewards() == sdk2.rewards())
    }

    @Test
    fun `can get All Invoked Reward Actions`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/invoked-actions/",
                accessToken = any(),
                queryParameters = listOf("client_id" to "CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4")
            )
        } answers { "invoked-action-history.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->

            sdk.rewards().getInvokedRewardActions(LiveLikePagination.FIRST,
                GetInvokedRewardActionParams()) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.isNotEmpty())
        assert(res[0].id == "2792060f-0aa5-4e71-b2b8-57c3f05914bd")
    }

    @Test
    fun `can get 'register' Invoked Reward Actions`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/invoked-actions/",
                accessToken = any(),
                queryParameters = listOf(
                    "client_id" to "CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4",
                    "reward_action_key" to "register"
                )
            )
        } answers {
            "invoked-action-history-register-only.json".getNetworkSuccessResultForFile(
                javaClass.classLoader
            )
        }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->

            sdk.rewards().getInvokedRewardActions(LiveLikePagination.FIRST,
                GetInvokedRewardActionParams(
                    rewardActionKeys = listOf("register"),
                )) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.isNotEmpty())
        assert(res.size == 5)
        assert(res[0].id == "2792060f-0aa5-4e71-b2b8-57c3f05914bd")
    }

    @Test
    fun `can get 'register' and 'makeAPromise' Invoked Reward Actions`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/invoked-actions/",
                accessToken = any(),
                queryParameters = listOf(
                    "client_id" to "CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4",
                    "reward_action_key" to "makeAPromise",
                    "reward_action_key" to "login"
                )
            )
        } answers {
            "invoked-action-history-login-makeAPromise.json".getNetworkSuccessResultForFile(
                javaClass.classLoader
            )
        }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.rewards().getInvokedRewardActions(LiveLikePagination.FIRST,
                GetInvokedRewardActionParams(
                    rewardActionKeys = listOf("makeAPromise", "login"),
                )) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.isNotEmpty())
        assert(res.size == 15)
        assert(res[0].id == "345f1631-803f-43c2-aef9-b4b4c052a157")
    }

    @Test
    fun `can get by profile id Invoked Reward Actions`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/invoked-actions/",
                accessToken = any(),
                queryParameters = listOf(
                    "client_id" to "CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4",
                    "profile_id" to "93c2000a-d202-41cf-9de7-8310bffc8c8c",
                    "profile_id" to "88eaacf9-ae0e-446b-9bb3-c47df3766889",
                )
            )
        } answers { "invoked-action-history-2-guys.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->

            sdk.rewards().getInvokedRewardActions( LiveLikePagination.FIRST,
                GetInvokedRewardActionParams(
                    profileIDs = listOf(
                        "93c2000a-d202-41cf-9de7-8310bffc8c8c",
                        "88eaacf9-ae0e-446b-9bb3-c47df3766889"
                    ),
                )) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.isNotEmpty())
        assert(res.size == 2)
        assert(res[0].profileId == "93c2000a-d202-41cf-9de7-8310bffc8c8c")
        assert(res[1].profileId == "88eaacf9-ae0e-446b-9bb3-c47df3766889")
    }

}