package com.livelike.gamification

import com.livelike.common.AccessTokenDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.common.getNetworkSuccessResultForFile
import com.livelike.common.initSDK
import com.livelike.common.user
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.digitalGoods
import com.livelike.engagementsdk.gamification.GetRedemptionKeyRequestOptions
import com.livelike.engagementsdk.gamification.RedemptionKeyStatus
import com.livelike.network.NetworkApiClient
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

@OptIn(ExperimentalCoroutinesApi::class)
class InternalDigitalGoodsClientTest {
    private val testDispatcher = StandardTestDispatcher()

    @MockK
    lateinit var client: NetworkApiClient

    @Before
    fun setUp() {
        // do the setup
        MockKAnnotations.init(this)
        Dispatchers.setMain(testDispatcher)
    }

    private suspend fun setUpSDK(): LiveLikeKotlin {
//        if (this::sdk.isInitialized) return sdk
        coEvery {
            client.get(
                "http://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4"
            )
        } returns "application_success.json".getNetworkSuccessResultForFile(
            javaClass.classLoader
        )
        coEvery {
            client.get(
                "https://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4/profile/",
                accessToken = any()
            )
        } returns "profile_fetch_success.json".getNetworkSuccessResultForFile(javaClass.classLoader)
        val sdk =
            initSDK(client, testDispatcher, accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJibGFzdHJ0IiwiaWQiOiJlZGFiMzNlZS0yODgwLTRkZWUtOTIxZC03ZjRkM2Q3YTQ1MDQiLCJjbGllbnRfaWQiOiJDZHNkTEtMM21TbGx5U3h5TzJValNSUkg0RXFGbVVGYmZTWFRaV1c0IiwiaWF0IjoxNjY5NzE2NjYxLCJhY2Nlc3NfdG9rZW4iOiIwNmMwNjMwZjJhY2UxYzliMmIyNWU5ZDk2NmRlOTcxZjUyODE1ODg0In0.OfxWOLCozRhL3aKzxwQMc2Fn7XmAcpuJhcdEReCwIBo"
                }

                override fun storeAccessToken(accessToken: String?) {

                }
            })
        sdk.user().currentProfileOnce()
        return sdk
    }

    @After
    fun tearDown() {
        testDispatcher.cancel()
    }

    @Test
    fun `can get redemption keys`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/redemption-keys/?client_id=CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4",
                accessToken = any(),
                queryParameters = any()
            )
        } answers { "get_redemption_keys.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.digitalGoods().getRedemptionKeys(
                LiveLikePagination.FIRST){ result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.isNotEmpty())
        assert(res[0].id == "waffles_id")
    }

    @Test
    fun `can get filtered redemption keys`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/redemption-keys/?client_id=CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4",
                accessToken = any(),
                queryParameters = listOf("status" to "active", "is_assigned" to true)
            )
        } answers { "get_redemption_keys.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.digitalGoods().getRedemptionKeys(
                LiveLikePagination.FIRST,
                GetRedemptionKeyRequestOptions(RedemptionKeyStatus.active)){ result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.isNotEmpty())
        assert(res[0].id == "waffles_id")
        assert(res[0].status == RedemptionKeyStatus.active)
    }


    @Test
    fun `can redeem with code`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.patch(
                "https://localhost/api/v1/redemption-keys-by-code/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4/I heart waffles/",
                accessToken = any(),
                body = "{\"status\":\"redeemed\"}"
            )
        } answers { "redeemed_code_response.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.digitalGoods().redeemKeyWithCode(
                "I heart waffles",){ result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.id == "waffles_id")
        assert(res.status == RedemptionKeyStatus.redeemed)

    }

    @Test
    fun `can redeem with id`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.patch(
                "https://localhost/api/v1/redemption-keys/waffles_id/",
                accessToken = any(),
                body = "{\"status\":\"redeemed\"}"
            )
        } answers { "redeemed_code_response.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->

            sdk.digitalGoods().redeemKeyWithId(
                "waffles_id"){ result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.id == "waffles_id")
        assert(res.status == RedemptionKeyStatus.redeemed)

    }


}
