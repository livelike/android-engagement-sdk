package com.livelike.rbac

import com.livelike.common.AccessTokenDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.rbac.models.ListAssignedRoleRequestOptions
import com.livelike.utils.LiveLikeException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class LiveLikeRBACClientE2ETest {
    private lateinit var rbacClient: LiveLikeRBACClient
    private lateinit var liveLikeKotlin: LiveLikeKotlin
    private val testDispatcher = StandardTestDispatcher()
    private var clientId: String = "8PqSNDgIVHnXuJuGte1HdvOjOqhCFE1ZCR3qhqaS"
    private var producerToken: String =
        "kpq3LJ9X-0yD7sdPGKe59j8YwtUrOUeesJj9-kbEdtw4daCLdhb0Jg"  //producer token

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        /* producerToken = System.getenv("producerToken")
         clientId = System.getenv("clientId")*/
        liveLikeKotlin = LiveLikeKotlin(
            clientId,
            accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return producerToken
                }

                override fun storeAccessToken(accessToken: String?) {

                }

            })
        rbacClient = liveLikeKotlin.rbac()
    }


    @After
    fun tearDown() {
        testDispatcher.cancel()
    }


    @Test
    fun testGetRoles() = runTest {
        suspendCancellableCoroutine { cont ->
            rbacClient.getRoles(LiveLikePagination.FIRST) { result, error ->
                result?.let { paginationResponse ->
                    cont.resume(paginationResponse.results)
                }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }.let { roles ->
            println(roles)
            assert(roles.isNotEmpty())
        }
    }


    @Test
    fun testGetResources() = runTest {
        suspendCancellableCoroutine { cont ->
            rbacClient.getResources(LiveLikePagination.FIRST) { result, error ->
                result?.let { paginationResponse ->
                    cont.resume(paginationResponse.results)
                }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }.let { resources ->
            println(resources)
            assert(resources.isNotEmpty())
            assert(resources[0].id == "4f564423-635d-4e1e-b96c-9990c998ad49")
            assert(resources.size == 20)
        }


        suspendCancellableCoroutine { cont ->
            rbacClient.getResources(LiveLikePagination.NEXT) { result, error ->
                result?.let { paginationResponse ->
                    cont.resume(paginationResponse.results)
                }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }.let { resources ->
            println("----PAGINATION NEXT--------")
            println(resources)
            assert(resources.isNotEmpty())
            assert(resources.size == 4)
        }
    }


    @Test
    fun testGetAssignedRoles() = runTest {
         val rbacRolesDetail = suspendCoroutine { cont ->
           rbacClient.getAssignedRoles(
               ListAssignedRoleRequestOptions(profileId = "9a42c0a9-1f2c-4144-b619-318715f0baed"),
                LiveLikePagination.FIRST
            ) { result, error ->
                result?.let {
                    cont.resume(it)
                }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(rbacRolesDetail.results.isNotEmpty())
    }


    @Test
    fun test_list_scopes() = runTest {
        val scopeDetail = suspendCoroutine { cont ->
            rbacClient.getScopes(
                liveLikePagination = LiveLikePagination.FIRST
            ) { result, error ->
                result?.let {
                    cont.resume(it)
                }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(scopeDetail.results.isNotEmpty())
    }

    @Test
    fun test_list_permissions() = runTest {
        val permissionDetail = suspendCoroutine { cont->
            rbacClient.getPermissions(
                liveLikePagination = LiveLikePagination.FIRST
            ) { result , error ->
                result?.let {
                    cont.resume(it)
                }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(permissionDetail.results.isNotEmpty())
    }


    @Test
    fun test_create_role_assignment() = runTest {
        //change the resourceKind and resourceKey and then test this
        /*suspendCancellableCoroutine { cont ->
            var assignRoleRequest = AssignRoleRequest(
                profileId = "99a9c1d7-ad4b-4bdd-a860-321d4ae8a484",
                roleKey = "widget-reporter",
                resourceKind = "emoji-slider", //can use other things also like programs
                resourceKey = "emoji-slider_da160802-8fda-41c7-b8f7-3127910784e1"   //change text_poll id to any other id taken from cms
            )
            rbacClient.assignRole(assignRoleRequest) { result, error ->
                result?.let {
                    cont.resume(it)
                }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }

        }.let {roleAssignment->
            println(roleAssignment)
            assert(roleAssignment.role.isActive)
            assert(roleAssignment.role.key == "widget-reporter")
        }*/
    }

}