package com.livelike.rbac

import com.livelike.common.LiveLikeCallback
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.rbac.models.AssignRoleRequestOptions
import com.livelike.rbac.models.ListAssignedRoleRequestOptions
import com.livelike.rbac.models.ListScopesRequest
import com.livelike.rbac.models.RBACPermission
import com.livelike.rbac.models.RBACResource
import com.livelike.rbac.models.RBACRole
import com.livelike.rbac.models.RoleAssignment
import com.livelike.rbac.models.RBACScope
import com.livelike.rbac.models.UnAssignRoleRequestOptions
import com.livelike.utils.PaginationResponse

interface LiveLikeRBACClient {

    /**
     * Assign a role to a profile.Producer Access Token is required for this.
     * Application Profile Tokens are not allowed to create this resource.
     *
     * @param assignRoleRequestOptions Arguments used to assign the role.
     * @param liveLikeCallback A callback to handle the result of assigning the role to a profile.
     */
    fun createRoleAssignment(assignRoleRequestOptions: AssignRoleRequestOptions, liveLikeCallback: LiveLikeCallback<RoleAssignment>)


    /**
     * Deletes a role assigned to a profile.Producer Access Token is required for this.
     * Application Profile Tokens are not allowed to create this resource.
     *
     * @param unAssignRoleRequestOptions Arguments used to un-assign the role.
     * @param liveLikeCallback A callback to handle the result of deleting the role assigned to a profile.
     */
    fun deleteRoleAssignment(
        unAssignRoleRequestOptions: UnAssignRoleRequestOptions,
        liveLikeCallback: LiveLikeCallback<LiveLikeEmptyResponse>
    )


    /**
     * List all the roles assigned to the current user.
     * @param liveLikeCallback A callback to handle the result of all the roles assigned to the current user.
     */
    fun getAssignedRoles(
        listAssignedRoleRequestOptions: ListAssignedRoleRequestOptions,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: LiveLikeCallback<PaginationResponse<RoleAssignment>>
    )

    /**
     * List all the permissions available
     * @param liveLikeCallback A callback to handle the result of all the roles assigned to the current user.
     */
    fun getPermissions(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: LiveLikeCallback<PaginationResponse<RBACPermission>>
    )

    /**
     * List all the roles available in LiveLike system.
     * @param liveLikeCallback A callback to handle the result of all the roles available.
     */
    fun getRoles(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: LiveLikeCallback<PaginationResponse<RBACRole>>
    )

    /**
     * List all the resources available in LiveLike system.
     * @param liveLikeCallback A callback to handle the result of all the resources available.
     */
    fun getResources(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: LiveLikeCallback<PaginationResponse<RBACResource>>
    )


    /**
     * List all the scopes available.
     * @param listScopesRequest Arguments used to get particular scope.
     * @param liveLikeCallback A callback to handle the result of all the resources available.
     */
    fun getScopes(
        listScopesRequest: ListScopesRequest?=null,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: LiveLikeCallback<PaginationResponse<RBACScope>>
    )

}