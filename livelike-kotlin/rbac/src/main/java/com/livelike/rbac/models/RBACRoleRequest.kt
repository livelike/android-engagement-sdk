package com.livelike.rbac.models

import com.google.gson.annotations.SerializedName

data class AssignRoleRequestOptions(
    @SerializedName("profile_id") val profileId: String,
    @SerializedName("role_key") val roleKey: String,
    @SerializedName("resource_kind") val resourceKind: String,
    @SerializedName("resource_key") val resourceKey: String
)

data class UnAssignRoleRequestOptions(val assignedRoleId: String)


data class ListAssignedRoleRequestOptions(@SerializedName("profile_id") val profileId: String?=null, @SerializedName("resource_key") val resourceKey: String?=null)


data class ListScopesRequest(@SerializedName("resource_id") val resourceId: String? = null)