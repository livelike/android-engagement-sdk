package com.livelike.rbac

import com.livelike.common.LiveLikeKotlin
import com.livelike.common.profile

/**
To access LiveLikeRBACClient
 */
fun LiveLikeKotlin.rbac(): LiveLikeRBACClient {
    return InternalLiveLikeRBACClient(
        sdkConfigurationOnce,
        profile().currentProfileOnce,
        sdkScope,
        uiScope,
        networkClient
    )
}