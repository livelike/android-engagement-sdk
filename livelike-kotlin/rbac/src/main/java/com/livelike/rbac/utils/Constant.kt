package com.livelike.rbac.utils

internal const val PROFILE_ID = "profile_id"
internal const val RESOURCE_KEY = "resource_key"
internal const val CLIENT_ID = "client_id"
internal const val RESOURCE_ID = "resource_id"
internal const val TEMPLATE_PROFILE_ID = "{profile_id}"
internal const val TEMPLATE_ASSIGNED_ROLE_ID = "{role_assignment_id}"