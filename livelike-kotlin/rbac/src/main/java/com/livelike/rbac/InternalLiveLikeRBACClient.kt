package com.livelike.rbac

import com.livelike.BaseClient
import com.livelike.common.LiveLikeCallback
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.network.NetworkApiClient
import com.livelike.rbac.models.AssignRoleRequestOptions
import com.livelike.rbac.models.ListAssignedRoleRequestOptions
import com.livelike.rbac.models.ListScopesRequest
import com.livelike.rbac.models.RBACPermission
import com.livelike.rbac.models.RBACResource
import com.livelike.rbac.models.RBACRole
import com.livelike.rbac.models.RoleAssignment
import com.livelike.rbac.models.RBACScope
import com.livelike.rbac.models.UnAssignRoleRequestOptions
import com.livelike.rbac.utils.CLIENT_ID
import com.livelike.rbac.utils.RESOURCE_ID
import com.livelike.rbac.utils.RESOURCE_KEY
import com.livelike.rbac.utils.TEMPLATE_ASSIGNED_ROLE_ID
import com.livelike.rbac.utils.TEMPLATE_PROFILE_ID
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import com.livelike.utils.NO_MORE_DATA
import com.livelike.utils.Once
import com.livelike.utils.PaginationResponse
import kotlinx.coroutines.CoroutineScope

internal class InternalLiveLikeRBACClient(
    sdkConfigurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    sdkScope: CoroutineScope,
    uiScope: CoroutineScope,
    private val networkApiClient: NetworkApiClient,
) : BaseClient(
    sdkConfigurationOnce,
    currentProfileOnce,
    sdkScope,
    uiScope
), LiveLikeRBACClient {
    private var RBACRoleResult: PaginationResponse<RBACRole>? = null
    private var RBACResourceResult: PaginationResponse<RBACResource>? = null
    private var roleAssignmentPaginatedResult: MutableMap<String, PaginationResponse<RoleAssignment>> =
        mutableMapOf()
    private var RBACPermissionResult: PaginationResponse<RBACPermission>? = null

    private var scopesPaginatedResult: MutableMap<String, PaginationResponse<RBACScope>> =
        mutableMapOf()


    override fun createRoleAssignment(
        assignRoleRequestOptions: AssignRoleRequestOptions,
        liveLikeCallback: LiveLikeCallback<RoleAssignment>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            networkApiClient.post(
                pair.second.profileRoleAssignmentsUrl,
                body = assignRoleRequestOptions.toJsonString(),
                accessToken = pair.first.accessToken
            ).processResult()
        }
    }

    override fun deleteRoleAssignment(
        unAssignRoleRequestOptions: UnAssignRoleRequestOptions,
        liveLikeCallback: LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            networkApiClient.delete(
                pair.second.profileRoleAssignmentDetailUrlTemplate.replace(
                    TEMPLATE_ASSIGNED_ROLE_ID,
                    unAssignRoleRequestOptions.assignedRoleId
                ),
                accessToken = pair.first.accessToken
            ).processResult()
        }
    }


    override fun getAssignedRoles(
        listAssignedRoleRequestOptions: ListAssignedRoleRequestOptions,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: LiveLikeCallback<PaginationResponse<RoleAssignment>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val result = roleAssignmentPaginatedResult[listAssignedRoleRequestOptions.toString()]
            val url = if (result == null || liveLikePagination == LiveLikePagination.FIRST) {
               pair.second.profileRoleAssignmentsUrlTemplate.replace(
                   TEMPLATE_PROFILE_ID,
                    listAssignedRoleRequestOptions.profileId ?: pair.first.id
                )
            } else {
                when (liveLikePagination) {
                    LiveLikePagination.NEXT -> result.next
                    LiveLikePagination.PREVIOUS -> result.previous
                    else -> null
                }
            }
            url?.let {
                val queryParams = arrayListOf<Pair<String, String?>>()
                listAssignedRoleRequestOptions.resourceKey?.let { key ->
                    queryParams.add(RESOURCE_KEY to key)
                }
                networkApiClient.get(
                    it,
                    accessToken = pair.first.accessToken,
                    queryParameters = queryParams
                )
                    .processResult<PaginationResponse<RoleAssignment>>()
                    .also { assignedRolesPaginatedResponse ->
                        roleAssignmentPaginatedResult[listAssignedRoleRequestOptions.toString()] =
                            assignedRolesPaginatedResponse
                    }
            } ?: throw Exception(NO_MORE_DATA)
        }

    }


    override fun getPermissions(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: LiveLikeCallback<PaginationResponse<RBACPermission>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val url =
                if (RBACPermissionResult == null || liveLikePagination == LiveLikePagination.FIRST) {
                    pair.second.permissionsUrl

                } else {
                    when (liveLikePagination) {
                        LiveLikePagination.NEXT -> RBACPermissionResult?.next
                        LiveLikePagination.PREVIOUS -> RBACPermissionResult?.previous
                        else -> null
                    }
                }

            url?.let {
                networkApiClient.get(it, accessToken = pair.first.accessToken)
                    .processResult<PaginationResponse<RBACPermission>>()
                    .also { permissionResponse ->
                        RBACPermissionResult = permissionResponse
                    }
            } ?: throw Exception(NO_MORE_DATA)
        }
    }


    override fun getRoles(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: LiveLikeCallback<PaginationResponse<RBACRole>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val url = if (RBACRoleResult == null || liveLikePagination == LiveLikePagination.FIRST) {
                pair.second.rolesUrl

            } else {
                when (liveLikePagination) {
                    LiveLikePagination.NEXT -> RBACRoleResult?.next
                    LiveLikePagination.PREVIOUS -> RBACRoleResult?.previous
                    else -> null
                }
            }

            url?.let {
                val queryParams = arrayListOf<Pair<String, String?>>()
                queryParams.add(CLIENT_ID to pair.second.clientId)
                networkApiClient.get(
                    it,
                    accessToken = pair.first.accessToken,
                    queryParameters = queryParams
                )
                    .processResult<PaginationResponse<RBACRole>>()
                    .also { paginatedResponse ->
                        RBACRoleResult = paginatedResponse
                    }
            } ?: throw Exception(NO_MORE_DATA)
        }
    }


    override fun getResources(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: LiveLikeCallback<PaginationResponse<RBACResource>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val url =
                if (RBACResourceResult == null || liveLikePagination == LiveLikePagination.FIRST) {
                    pair.second.resourcesUrl
                } else {
                    when (liveLikePagination) {
                        LiveLikePagination.NEXT -> RBACResourceResult?.next
                        LiveLikePagination.PREVIOUS -> RBACResourceResult?.previous
                        else -> null
                    }
                }

            url?.let {
                networkApiClient.get(it, accessToken = pair.first.accessToken)
                    .processResult<PaginationResponse<RBACResource>>()
                    .also { resourcesPaginatedResponse ->
                        RBACResourceResult = resourcesPaginatedResponse
                    }
            } ?: throw Exception(NO_MORE_DATA)
        }
    }


    override fun getScopes(
        listScopesRequest: ListScopesRequest?,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: LiveLikeCallback<PaginationResponse<RBACScope>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val result = scopesPaginatedResult[listScopesRequest.toString()]
            val url = if (result == null || liveLikePagination == LiveLikePagination.FIRST) {
                pair.second.scopesUrl
            } else {
                when (liveLikePagination) {
                    LiveLikePagination.NEXT -> result.next
                    LiveLikePagination.PREVIOUS -> result.previous
                    else -> null
                }
            }

            url?.let {
                val queryParams = arrayListOf<Pair<String, String?>>()
                listScopesRequest?.resourceId?.let { id ->
                    queryParams.add(RESOURCE_ID to id)
                }
                networkApiClient.get(
                    it,
                    accessToken = pair.first.accessToken,
                    queryParameters = queryParams
                )
                    .processResult<PaginationResponse<RBACScope>>()
                    .also { scopePaginatedResponse ->
                        scopesPaginatedResult[listScopesRequest.toString()] = scopePaginatedResponse
                    }
            } ?: throw Exception(NO_MORE_DATA)
        }
    }

}