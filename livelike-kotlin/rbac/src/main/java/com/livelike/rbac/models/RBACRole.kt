package com.livelike.rbac.models

import com.google.gson.annotations.SerializedName

data class RoleAssignment(
    val id: String,
    val url: String,
    val rbacProfile:RBACProfile,
    @SerializedName("created_at")val createdAt: String,
    val rbacRole: RBACRole,
    val rbacScope: RBACScope
)


data class RBACRole(
    val id: String,
    val url: String,
    val name: String,
    @SerializedName("client_id") val clientId:String,
    val description: String,
    val key: String,
    val rbacPermissions: List<RBACPermission>,
    @SerializedName("created_at")val createdAt: String,
    @SerializedName("is_active")val isActive: Boolean )



data class RBACPermission(
    val id: String,
    val name: String,
    val description: String,
    val key: String,
    @SerializedName("created_at")val createdAt: String
    )


data class RBACScope(
    val id: String,
    val url: String,
    @SerializedName("client_id") val clientId:String,
    val rbacResource: RBACResource,
    @SerializedName("resource_key") val resourceKey:String,
    @SerializedName("created_at") val createdAt: String,
)

data class RBACResource(
    val id: String,
    val url: String,
    val name: String,
    val description: String,
    val kind: String,
    @SerializedName("created_at") val createdAt: String,
    @SerializedName("scopes_url") val scopesUrl: String
)

data class RBACProfile(
    val id: String,
    val url: String,
    @SerializedName("created_at") val createdAt: String,
    val nickname: String,
    @SerializedName("custom_id") val customId: String? = null,
    @SerializedName("custom_data") val customData: String? = null
)

