package com.livelike.mobile.social.models

import com.google.gson.annotations.SerializedName
import com.livelike.engagementsdk.LiveLikeProfile

data class ProfileRelationship(
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    internal val url: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("relationship_type")
    val profileRelationshipType: ProfileRelationshipType,
    @SerializedName("from_profile")
    val fromProfile: LiveLikeProfile,
    @SerializedName("to_profile")
    val toProfile: LiveLikeProfile,
)