package com.livelike.mobile.social.models

data class GetProfileRelationshipsRequestParams(
    val fromProfileId: String? = null,
    val relationshipTypeKey: String? = null,
    val toProfileId: String? = null
)