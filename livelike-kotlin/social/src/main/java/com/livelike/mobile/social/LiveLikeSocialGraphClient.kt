package com.livelike.mobile.social

import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.LiveLikeCallback
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.mobile.social.models.CreateProfileRelationshipRequestParams
import com.livelike.mobile.social.models.DeleteProfileRelationshipsRequestParams
import com.livelike.mobile.social.models.GetProfileRelationshipRequestParams
import com.livelike.mobile.social.models.GetProfileRelationshipsRequestParams
import com.livelike.mobile.social.models.ProfileRelationship
import com.livelike.mobile.social.models.ProfileRelationshipType
import com.livelike.network.NetworkApiClient
import com.livelike.utils.Once
import kotlinx.coroutines.CoroutineScope

interface LiveLikeSocialGraphClient {

    /**
     * This can query relationships by any combination of the three
     * parameters supplied to `GetProfileRelationshipsRequestParams`
     *
     * example set this to get all profiles who follow you
     *
     * GetProfileRelationshipsRequestParams(
     *  relationshipTypeKey = "follow",
     *  toProfileId = "my profile id",
     * )
     *
     *
     */
    fun getProfileRelationships(
        requestParams: GetProfileRelationshipsRequestParams,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ProfileRelationship>>
    )

    /**
     * All relations between profiles must fall into a predefined relationship type.
     * These relationship types can be Queried via this interface.
     */
    fun getProfileRelationshipTypes(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ProfileRelationshipType>>
    )

    /**
     * delete an existing profile relationship
     *
     * NOTE: the access token profile must be the same as the from_profile_id of the
     * underlying relationship or the api will return 403
     */
    fun deleteProfileRelationship(
        requestParams: DeleteProfileRelationshipsRequestParams,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    )


    /**
     * create a profile relationship
     *
     * NOTE: the access token profile must be the same as the from_profile_id or api
     * will return 403
     */
    fun createProfileRelationship(
        requestParams: CreateProfileRelationshipRequestParams,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ProfileRelationship>
    )


    /**
     * get an existing profile relationship
     *
     */
    fun getProfileRelationship(
        requestParams: GetProfileRelationshipRequestParams,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ProfileRelationship>
    )

    companion object {
        fun getInstance(
            configurationOnce: Once<SdkConfiguration>,
            currentProfileOnce: Once<LiveLikeProfile>,
            uiScope: CoroutineScope,
            sdkScope: CoroutineScope,
            networkApiClient: NetworkApiClient
        ): LiveLikeSocialGraphClient = InternalSocialGraphClient(
            configurationOnce, currentProfileOnce, sdkScope, uiScope, networkApiClient
        )
    }

}

