package com.livelike.mobile.social

import com.livelike.BaseClient
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.mobile.social.models.CreateProfileRelationshipRequestParams
import com.livelike.mobile.social.models.DeleteProfileRelationshipsRequestParams
import com.livelike.mobile.social.models.GetProfileRelationshipRequestParams
import com.livelike.mobile.social.models.GetProfileRelationshipsRequestParams
import com.livelike.mobile.social.models.ProfileRelationship
import com.livelike.mobile.social.models.ProfileRelationshipType
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import com.livelike.utils.NO_MORE_DATA
import com.livelike.utils.Once
import com.livelike.utils.PaginationResponse
import kotlinx.coroutines.CoroutineScope


internal class InternalSocialGraphClient(
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    sdkScope: CoroutineScope,
    uiScope: CoroutineScope,
    private val networkApiClient: NetworkApiClient,
) : BaseClient(
    configurationOnce, currentProfileOnce, sdkScope, uiScope
), LiveLikeSocialGraphClient {

    private var getRelationshipsPaginatedResponse: PaginationResponse<ProfileRelationship>? = null
    private var getProfileRelationshipTypesPaginatedResponse: PaginationResponse<ProfileRelationshipType>? =
        null

    override fun getProfileRelationships(
        requestParams: GetProfileRelationshipsRequestParams,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ProfileRelationship>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            if (getRelationshipsPaginatedResponse == null || liveLikePagination == LiveLikePagination.FIRST) {
                pair.second.profileRelationshipsUrl
            } else {
                when (liveLikePagination) {
                    LiveLikePagination.NEXT -> getRelationshipsPaginatedResponse!!.next
                    LiveLikePagination.PREVIOUS -> getRelationshipsPaginatedResponse!!.previous
                    else -> null
                }
            }?.let { fetchUrl ->
                val params: List<Pair<String, String>> = buildList {
                    requestParams.fromProfileId?.let {
                        add(FROM_PROFILE_ID to it)
                    }
                    requestParams.relationshipTypeKey?.let {
                        add(RELATIONSHIP_TYPE_KEY to it)
                    }
                    requestParams.toProfileId?.let {
                        add(TO_PROFILE_ID to it)
                    }
                }

                networkApiClient.get(
                    fetchUrl,
                    pair.first.accessToken,
                    params
                ).processResult<PaginationResponse<ProfileRelationship>>().also {
                    getRelationshipsPaginatedResponse = it
                }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }

    override fun getProfileRelationshipTypes(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ProfileRelationshipType>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            if (getProfileRelationshipTypesPaginatedResponse == null || liveLikePagination == LiveLikePagination.FIRST) {
                pair.second.profileRelationshipTypesUrl
            } else {
                when (liveLikePagination) {
                    LiveLikePagination.NEXT -> getProfileRelationshipTypesPaginatedResponse!!.next
                    LiveLikePagination.PREVIOUS -> getProfileRelationshipTypesPaginatedResponse!!.previous
                    else -> null
                }
            }?.let { fetchUrl ->
                networkApiClient.get(
                    fetchUrl,
                    pair.first.accessToken,
                ).processResult<PaginationResponse<ProfileRelationshipType>>().also {
                    getProfileRelationshipTypesPaginatedResponse = it
                }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }

    override fun deleteProfileRelationship(
        requestParams: DeleteProfileRelationshipsRequestParams,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.profileRelationshipDetailUrlTemplate.replace(
                PROFILE_RELATIONSHIP_ID,
                requestParams.socialRelationshipId
            ).let { url ->
                networkApiClient.delete(
                    url, accessToken = pair.first.accessToken
                ).processResult()
            }
        }
    }


    override fun createProfileRelationship(
        requestParams: CreateProfileRelationshipRequestParams,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ProfileRelationship>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.profileRelationshipsUrl.let { url ->
                requestParams.clientId = pair.second.clientId
                networkApiClient.post(
                    url,
                    requestParams.toJsonString(),
                    pair.first.accessToken
                ).processResult()
            }
        }
    }


    override fun getProfileRelationship(
        requestParams: GetProfileRelationshipRequestParams,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ProfileRelationship>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.profileRelationshipDetailUrlTemplate.replace(
                PROFILE_RELATIONSHIP_ID,
                requestParams.socialRelationshipId
            ).let { url ->
                networkApiClient.get(
                    url, accessToken = pair.first.accessToken
                ).processResult()
            }
        }
    }

}