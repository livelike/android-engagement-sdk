package com.livelike.mobile.social.models

data class DeleteProfileRelationshipsRequestParams (
    val socialRelationshipId: String,
)

typealias GetProfileRelationshipRequestParams = DeleteProfileRelationshipsRequestParams
