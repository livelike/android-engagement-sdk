package com.livelike.mobile.social.models

import com.google.gson.annotations.SerializedName

class CreateProfileRelationshipRequestParams (
    @SerializedName("from_profile_id")
    val fromProfileId: String,
    @SerializedName("relationship_type_key")
    val relationshipTypeKey: String,
    @SerializedName("to_profile_id")
    val toProfileId: String,
) {
    @SerializedName("client_id")
    lateinit var clientId: String
}
