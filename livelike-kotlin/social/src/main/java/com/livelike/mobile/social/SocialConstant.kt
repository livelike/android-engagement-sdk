package com.livelike.mobile.social

const val FROM_PROFILE_ID = "from_profile_id"
const val RELATIONSHIP_TYPE_KEY = "relationship_type_key"
const val TO_PROFILE_ID = "to_profile_id"
const val PROFILE_RELATIONSHIP_ID = "{profile_relationship_id}"