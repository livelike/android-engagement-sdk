package com.livelike.mobile.social.models

import com.google.gson.annotations.SerializedName

data class ProfileRelationshipType(
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    internal val url: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("client_id")
    val clientId: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("key")
    val key: String,
)
