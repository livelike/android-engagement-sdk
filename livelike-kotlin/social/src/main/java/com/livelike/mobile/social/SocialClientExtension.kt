package com.livelike.mobile.social

import com.livelike.common.LiveLikeKotlin
import com.livelike.common.profile

fun LiveLikeKotlin.socialGraphClient(): LiveLikeSocialGraphClient {
    val key = this.hashCode()
    return if (sdkInstanceWithSocialClient.containsKey(key)) {
        sdkInstanceWithSocialClient[key]!!
    } else {
        LiveLikeSocialGraphClient.getInstance(
            sdkConfigurationOnce,
            profile().currentProfileOnce,
            uiScope,
            sdkScope,
            networkClient,
        ).let {
            sdkInstanceWithSocialClient[key] = it
            it
        }
    }
}


private val sdkInstanceWithSocialClient = mutableMapOf<Int, LiveLikeSocialGraphClient>()
