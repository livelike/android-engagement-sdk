package com.livelike.mobile.social

import com.livelike.common.AccessTokenDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.common.getNetworkSuccessResultForFile
import com.livelike.common.initSDK
import com.livelike.common.user
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.mobile.social.models.GetProfileRelationshipsRequestParams
import com.livelike.network.NetworkApiClient
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

@OptIn(ExperimentalCoroutinesApi::class)
class InternalSocialClientUnitTests {
    private val testDispatcher = StandardTestDispatcher()

    @MockK
    lateinit var client: NetworkApiClient

    @Before
    fun setUp() {
        // do the setup
        MockKAnnotations.init(this)
        Dispatchers.setMain(testDispatcher)
    }

    private suspend fun setUpSDK(): LiveLikeKotlin {
//        if (this::sdk.isInitialized) return sdk
        coEvery {
            client.get(
                "http://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4"
            )
        } returns "application_success.json".getNetworkSuccessResultForFile(
            javaClass.classLoader
        )
        coEvery {
            client.get(
                "https://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4/profile/",
                accessToken = any()
            )
        } returns "profile_fetch_success.json".getNetworkSuccessResultForFile(javaClass.classLoader)
        val sdk =
            initSDK(client, testDispatcher, accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJibGFzdHJ0IiwiaWQiOiJlZGFiMzNlZS0yODgwLTRkZWUtOTIxZC03ZjRkM2Q3YTQ1MDQiLCJjbGllbnRfaWQiOiJDZHNkTEtMM21TbGx5U3h5TzJValNSUkg0RXFGbVVGYmZTWFRaV1c0IiwiaWF0IjoxNjY5NzE2NjYxLCJhY2Nlc3NfdG9rZW4iOiIwNmMwNjMwZjJhY2UxYzliMmIyNWU5ZDk2NmRlOTcxZjUyODE1ODg0In0.OfxWOLCozRhL3aKzxwQMc2Fn7XmAcpuJhcdEReCwIBo"
                }

                override fun storeAccessToken(accessToken: String?) {

                }
            })
        sdk.user().currentProfileOnce()
        return sdk
    }

    @After
    fun tearDown() {
        testDispatcher.cancel()
    }

    @Test
    fun `can get relations from default profile`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/profile-relationships/?client_id=pnODbVXg0UI80s0l2aH5Y7FOuGbftoAdSNqpdvo6",
                accessToken = any(),
                queryParameters = any()
            )
        } answers { "get_social_connections.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->

            sdk.socialGraphClient().getProfileRelationships(GetProfileRelationshipsRequestParams(),
                LiveLikePagination.FIRST) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.isNotEmpty())
        assert(res[0].fromProfile.nickname == "Giant Rider")
        assert(res[0].profileRelationshipType.key == "follow")
        assert(res[0].toProfile.nickname == "Nimble Hawk")
    }

    @Test
    fun `can get relationship types`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/profile-relationship-types/?client_id=pnODbVXg0UI80s0l2aH5Y7FOuGbftoAdSNqpdvo6",
                accessToken = any(),
                queryParameters = any()
            )
        } answers { "get_relationship_types.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->

            sdk.socialGraphClient().getProfileRelationshipTypes(
                LiveLikePagination.FIRST) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.isNotEmpty())
        assert(res[0].name == "Follow")
        assert(res[0].key == "follow")
    }

}