package com.livelike.common

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test

class LiveLikeKotlinTest {
    private val testDispatcher = StandardTestDispatcher()

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        testDispatcher.cancel()
    }

    @Test
    fun test() = runTest {
        val liveLikeKotlin = LiveLikeKotlin("8PqSNDgIVHnXuJuGte1HdvOjOqhCFE1ZCR3qhqaS")
        val profile = liveLikeKotlin.profile()

        val currentProfile = profile.currentProfileOnce()
        val sdkConfiguration = liveLikeKotlin.sdkConfigurationOnce()

        assert(sdkConfiguration.clientId == "8PqSNDgIVHnXuJuGte1HdvOjOqhCFE1ZCR3qhqaS" && currentProfile.id.isNotEmpty())
    }
}