package com.livelike.engagementsdk

/** A simple representation of an observable stream.
 * Subscription will requires a key to avoid multiple subscription of the same observable.
 */
interface Stream<T> {
// TODO replace all usage of Stream by Flow
    /** Post data to the stream */
    fun onNext(data1: T?)

    /** Add an observable to receive future values of the stream */
    fun subscribe(key: Any, observer: (T?) -> Unit)

    /** Stop the observable at {key} from receiving events */
    fun unsubscribe(key: Any)

    /** Remove all the observable from this stream */
    fun clear()

    /** Get the latest value of the stream */
    fun latest(): T?
}
