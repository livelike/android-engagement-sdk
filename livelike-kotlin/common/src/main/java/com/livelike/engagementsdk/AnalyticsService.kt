package com.livelike.engagementsdk


import com.google.gson.JsonNull
import com.google.gson.JsonObject
import com.livelike.common.DataStoreDelegate
import com.livelike.engagementsdk.core.analytics.AnalyticsSuperProperties
import com.livelike.utils.logDebug

import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


/**
 * The base interface for the analytics. This will log events to any remote analytics provider.
 *
 */
interface AnalyticsService {
    /**
     * Set an analytics event observer {eventObserver}. It will intercept analytics events.
     *
     * @param eventObserver
     */
    fun setEventObserver(eventObserver: (String, Map<String, Any?>) -> Unit)

    fun registerSuperProperty(analyticsSuperProperties: AnalyticsSuperProperties, value: Any?)
    fun registerSuperAndPeopleProperty(event: Pair<String, String>)

    fun trackConfiguration(internalAppName: String) // add more info if required in the future
    fun trackWidgetInteraction(
        kind: String,
        id: String,
        programId: String,
        interactionInfo: AnalyticsWidgetInteractionInfo,
        widgetPrompt: String
    )

    fun trackWidgetEngaged(
        kind: String,
        id: String,
        programId: String,
        widgetPrompt:String
    )

    fun trackSessionStarted()
    fun trackMessageSent(
        msgId: String,
        msg: String?,
        hasExternalImage: Boolean = false,
        chatRoomId: String
    )

    fun trackMessageDisplayed(msgId: String, msg: String?, hasExternalImage: Boolean = false)
    fun trackLastChatStatus(status: Boolean)
    fun trackLastWidgetStatus(status: Boolean)
    fun trackWidgetReceived(kind: String, id: String)
    fun trackWidgetDisplayed(kind: String, id: String, programId: String, linkUrl: String? = null)
    fun trackWidgetBecameInteractive(
        kind: String,
        id: String,
        programId: String,
        linkUrl: String? = null
    )

    fun trackWidgetDismiss(
        kind: String,
        id: String,
        programId: String,
        interactionInfo: AnalyticsWidgetInteractionInfo?,
        interactable: Boolean?,
        action: DismissAction
    )

    fun trackInteraction(
        kind: String,
        id: String,
        interactionType: String,
        interactionCount: Int = 1,
        widgetPrompt: String
    )

    fun trackOrientationChange(isPortrait: Boolean)
    fun trackSession(sessionId: String)
    fun trackButtonTap(buttonName: String, extra: JsonObject)
    fun trackUsername(username: String)
    fun trackKeyboardOpen(keyboardType: KeyboardType)
    fun trackKeyboardClose(
        keyboardType: KeyboardType,
        hideMethod: KeyboardHideReason,
        chatMessageId: String? = null
    )

    fun trackFlagButtonPressed()
    fun trackReportingMessage()
    fun trackBlockingUser()
    fun trackCancelFlagUi()
    fun trackPointTutorialSeen(completionType: String, secondsSinceStart: Long)
    fun trackPointThisProgram(points: Int)
    fun trackBadgeCollectedButtonPressed(badgeId: String, badgeLevel: Int)
    fun trackChatReactionPanelOpen(messageId: String)
    fun trackAlertLinkOpened(
        alertId: String,
        programId: String,
        linkUrl: String,
        widgetType: String?
    )

    fun trackChatReactionSelected(
        chatRoomId: String,
        messageId: String,
        reactionId: String,
        isRemoved: Boolean
    )

    fun trackVideoAlertPlayed(
        kind: String,
        id: String,
        programId: String,
        videoUrl: String
    )

    fun trackMessageLinkClicked(
        chatRoomId: String,
        chatRoomName: String?,
        messageId: String?,
        link: String
    )

    fun destroy()
}

class MockAnalyticsService(
    private val clientId: String = "",
    val dataStoreDelegate: DataStoreDelegate
) : AnalyticsService {

    private var parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())

    override fun setEventObserver(eventObserver: (String, Map<String, Any?>) -> Unit) {
        eventObservers[clientId] = eventObserver
    }

    override fun trackBadgeCollectedButtonPressed(badgeId: String, badgeLevel: Int) {
        val properties = hashMapOf<String, Any?>()
        properties["Badge ID"] = badgeId
        properties["Level"] = badgeLevel
        eventObservers[clientId]?.invoke(KEY_EVENT_BADGE_COLLECTED_BUTTON_PRESSED, properties)
        logDebug { "[Analytics] -[${object {}.javaClass.enclosingMethod?.name}]$badgeId $badgeLevel]" }
    }

    override fun trackChatReactionPanelOpen(messageId: String) {
        val properties = hashMapOf<String, Any?>()
        properties[CHAT_MESSAGE_ID] = messageId
        eventObservers[clientId]?.invoke(KEY_EVENT_CHAT_REACTION_PANEL_OPEN, properties)
        logDebug { "[Analytics] -[${object {}.javaClass.enclosingMethod?.name}]$messageId]" }
    }

    override fun trackChatReactionSelected(
        chatRoomId: String,
        messageId: String,
        reactionId: String,
        isRemoved: Boolean
    ) {
        val properties = hashMapOf<String, Any?>()
        properties[CHAT_MESSAGE_ID] = messageId
        properties[CHAT_REACTION_ID] = reactionId
        properties[CHAT_ROOM_ID] = chatRoomId
        val event = when (isRemoved) {
            true -> KEY_EVENT_CHAT_REACTION_REMOVED
            else -> KEY_EVENT_CHAT_REACTION_ADDED
        }
        eventObservers[clientId]?.invoke(event, properties)
        logDebug { "[Analytics] -[${object {}.javaClass.enclosingMethod?.name}]$messageId $reactionId $isRemoved]" }
    }

    override fun trackVideoAlertPlayed(
        kind: String,
        id: String,
        programId: String,
        videoUrl: String
    ) {
        val properties = hashMapOf<String, Any?>()
        properties["Widget ID"] = id
        properties["Widget Type"] = kind
        properties[PROGRAM_ID] = programId
        properties[VIDEO_URL] = videoUrl
        eventObservers[clientId]?.invoke(KEY_EVENT_VIDEO_ALERT_PLAY_STARTED, properties)
        logDebug { "[Analytics] -[${object {}.javaClass.enclosingMethod?.name}]$kind $programId $videoUrl]" }
    }

    override fun trackMessageLinkClicked(
        chatRoomId: String,
        chatRoomName: String?,
        messageId: String?,
        link: String
    ) {
        val properties = hashMapOf<String, Any?>()
        properties[CHAT_ROOM_ID] = chatRoomId
        properties["Chat Room Title"] = chatRoomName
        properties[CHAT_MESSAGE_ID] = messageId
        properties["Chat Message Link"] = link
        eventObservers[clientId]?.invoke(KEY_EVENT_CHAT_MESSAGE_LINK_CLICKED, properties)
        logDebug { "[Analytics] -[${object {}.javaClass.enclosingMethod?.name}]$chatRoomId $chatRoomName $messageId $link]" }
    }

    override fun destroy() {
        logDebug { "[Analytics] -[${object {}.javaClass.enclosingMethod?.name}]]" }
    }

    override fun trackAlertLinkOpened(
        alertId: String,
        programId: String,
        linkUrl: String,
        widgetType: String?
    ) {
        val properties = hashMapOf<String, Any?>()
        properties[ALERT_ID] = alertId
        properties[PROGRAM_ID] = programId
        properties[LINK_URL] = linkUrl
        properties[WIDGET_TYPE] = widgetType ?: ""
        eventObservers[clientId]?.invoke(KEY_EVENT_ALERT_LINK_OPENED, properties)
        logDebug { "[Analytics] -[${object {}.javaClass.enclosingMethod?.name}]$alertId $programId $linkUrl ${widgetType}]" }
    }

    override fun registerSuperProperty(
        analyticsSuperProperties: AnalyticsSuperProperties,
        value: Any?
    ) {
        hashMapOf<String, Any?>().apply {
            put(analyticsSuperProperties.key, value ?: JsonNull.INSTANCE)
            eventObservers[clientId]?.invoke(analyticsSuperProperties.key, this)
            logDebug { "[Analytics] -[${object {}.javaClass.enclosingMethod?.name}]$value]" }
        }

    }

    override fun trackPointThisProgram(points: Int) {
        hashMapOf<String, Any?>().apply {
            put(AnalyticsSuperProperties.POINTS_THIS_PROGRAM.key, points)
            eventObservers[clientId]?.invoke(
                AnalyticsSuperProperties.POINTS_THIS_PROGRAM.key,
                this
            )
        }
        logDebug { "[Analytics] -[${object {}.javaClass.enclosingMethod?.name}]$points]" }
    }

    override fun trackPointTutorialSeen(completionType: String, secondsSinceStart: Long) {
        val properties = hashMapOf<String, Any?>()
        properties["Completion Type"] = completionType
        properties["Dismiss Seconds Since Start"] = secondsSinceStart
        eventObservers[clientId]?.invoke(KEY_POINT_TUTORIAL_COMPLETED, properties)
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]" }
    }

    override fun trackFlagButtonPressed() {
        eventObservers[clientId]?.invoke(KEY_KEYBOARD_SELECTED, hashMapOf())
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]" }
    }

    override fun trackReportingMessage() {
        val properties = hashMapOf<String, Any?>()
        properties[KEY_REASON] = "Reporting Message"
        eventObservers[clientId]?.invoke(KEY_FLAG_ACTION_SELECTED, properties)
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]" }
    }

    override fun trackCancelFlagUi() {
        val properties = hashMapOf<String, Any?>()
        properties[KEY_REASON] = "Blocking User"
        eventObservers[clientId]?.invoke(KEY_FLAG_ACTION_SELECTED, properties)
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]" }
    }

    override fun trackBlockingUser() {
        val properties = hashMapOf<String, Any?>()
        properties[KEY_REASON] = "Blocking User"
        eventObservers[clientId]?.invoke(KEY_FLAG_ACTION_SELECTED, properties)
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]" }
    }

    override fun registerSuperAndPeopleProperty(event: Pair<String, String>) {
        hashMapOf<String, Any?>().apply {
            put(event.first, event.second)
            eventObservers[clientId]?.invoke(event.first, this)
        }
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]" }
    }

    override fun trackLastChatStatus(status: Boolean) {
        hashMapOf<String, Any?>().apply {
            put("Last Chat Status", if (status) "Enabled" else "Disabled")
            eventObservers[clientId]?.invoke("Last Chat Status", this)
        }
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]$status" }
    }

    override fun trackLastWidgetStatus(status: Boolean) {
        hashMapOf<String, Any?>().apply {
            put("Last Widget Status", if (status) "Enabled" else "Disabled")
            eventObservers[clientId]?.invoke("Last Widget Status", this)
        }
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]$status" }
    }

    override fun trackConfiguration(internalAppName: String) {
        hashMapOf<String, Any?>().apply {
            put("Internal App Name", internalAppName)
        }
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]$internalAppName" }
    }

    override fun trackWidgetInteraction(
        kind: String,
        id: String,
        programId: String,
        interactionInfo: AnalyticsWidgetInteractionInfo,
        widgetPrompt:String
    ) {
        val properties = hashMapOf<String, Any?>()
        properties["Widget Type"] = kind
        properties["Widget ID"] = id
        properties[PROGRAM_ID] = programId
        properties["Widget Prompt"]= widgetPrompt
        // this properties are not being used, and has been decided to remove (https://livelike.atlassian.net/browse/ES-2471)
        /* properties.put(
        "First Tap Time",
        parser.format(Date(interactionInfo.timeOfFirstInteraction))
        )
        properties.put("Last Tap Time", timeOfLastInteraction)
        properties.put("No of Taps", interactionInfo.interactionCount)*/
        properties["Points Earned"] = interactionInfo.pointEarned

        interactionInfo.badgeEarned?.let {
            properties["Badge Earned"] = interactionInfo.badgeEarned
            properties["Badge Level Earned"] = interactionInfo.badgeLevelEarned
        }
        interactionInfo.pointsInCurrentLevel?.let { properties.put("Points In Current Level", it) }
        interactionInfo.pointsToNextLevel?.let { properties.put("Points To Next Level", it) }
        eventObservers[clientId]?.invoke(KEY_WIDGET_INTERACTION, properties)
        logDebug { "[Analytics-Widget Interacted]-[${object {}.javaClass.enclosingMethod?.name}] $kind $programId $interactionInfo $widgetPrompt" }
    }

    override fun trackWidgetEngaged(kind: String, id: String, programId: String,widgetPrompt:String) {
        val properties = hashMapOf<String, Any?>()
        properties["Widget Type"] = kind
        properties["Widget ID"] = id
        properties[PROGRAM_ID] = programId
        properties["Widget Prompt"] = widgetPrompt
        eventObservers[clientId]?.invoke(KEY_WIDGET_ENGAGED, properties)
        logDebug { "[Analytics-Widget Engaged]-[${KEY_WIDGET_ENGAGED}]  $kind $programId $id $widgetPrompt" }
    }

    override fun trackSessionStarted() {
        val firstTimeProperties = hashMapOf<String, Any?>()
        val timeNow = parser.format(Date(System.currentTimeMillis()))
        firstTimeProperties["Session started"] = timeNow
        eventObservers[clientId]?.invoke("Session started", firstTimeProperties)

        val properties = hashMapOf<String, Any?>()
        properties["Last Session started"] = timeNow
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]" }
    }

    override fun trackMessageSent(
        msgId: String,
        msg: String?,
        hasExternalImage: Boolean,
        chatRoomId: String
    ) {
        val properties = hashMapOf<String, Any?>()
        properties[CHAT_MESSAGE_ID] = msgId
        properties["Character Length"] = (if (hasExternalImage) 0 else msg?.length ?: 0)
        //properties.put("Sticker Count", msg?.findStickers()?.countMatches())
        properties["Sticker Shortcodes"] =
            if (hasExternalImage) arrayListOf() else msg?.findStickerCodes()?.allMatches()
        properties["Has External Image"] = hasExternalImage
        properties["Chat Room ID"] = chatRoomId
        eventObservers[clientId]?.invoke(KEY_CHAT_MESSAGE_SENT, properties)

        val superProp = hashMapOf<String, Any?>()
        val timeNow = parser.format(Date(System.currentTimeMillis()))
        superProp["Time of Last Chat Message"] = timeNow
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]  $msgId " }
    }

    override fun trackMessageDisplayed(msgId: String, msg: String?, hasExternalImage: Boolean) {
        val properties = hashMapOf<String, Any?>()
        properties[CHAT_MESSAGE_ID] = msgId
        properties["Message ID"] = msgId
        properties["Sticker Shortcodes"] = msg?.findStickerCodes()?.allMatches() ?: listOf<String>()
        eventObservers[clientId]?.invoke(KEY_CHAT_MESSAGE_DISPLAYED, properties)
        logDebug { "[${object {}.javaClass.enclosingMethod?.name}] $msgId" }
    }

    override fun trackWidgetReceived(kind: String, id: String) {
        val properties = hashMapOf<String, Any?>()
        properties["Widget Type"] = kind
        properties["Widget Id"] = id
        eventObservers[clientId]?.invoke(KEY_WIDGET_RECEIVED, properties)
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]  $kind " }
    }

    override fun trackWidgetDisplayed(
        kind: String,
        id: String,
        programId: String,
        linkUrl: String?
    ) {
        val properties = hashMapOf<String, Any?>()
        properties["Widget Type"] = kind
        properties["Widget ID"] = id
        properties[PROGRAM_ID] = programId
        linkUrl?.let { properties.put(LINK_URL, it) }
        eventObservers[clientId]?.invoke(KEY_WIDGET_DISPLAYED, properties)
        logDebug { "[Analytics]-[trackWidgetDisplayed]  $kind $programId " }
    }

    override fun trackWidgetBecameInteractive(
        kind: String,
        id: String,
        programId: String,
        linkUrl: String?
    ) {
        val properties = hashMapOf<String, Any?>()
        properties["Widget Type"] = kind
        properties["Widget ID"] = id
        properties[PROGRAM_ID] = programId
        linkUrl?.let { properties.put(LINK_URL, it) }
        eventObservers[clientId]?.invoke(KEY_WIDGET_BECAME_INTERACTIVE, properties)
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]  $kind $programId " }
    }

    override fun trackWidgetDismiss(
        kind: String,
        id: String,
        programId: String,
        interactionInfo: AnalyticsWidgetInteractionInfo?,
        interactable: Boolean?,
        action: DismissAction
    ) {
        if (action == DismissAction.TIMEOUT) {
            return
        }
        val dismissAction = when (action) {
            DismissAction.TAP_X -> "Tap X"
            DismissAction.SWIPE -> "Swipe"
            else -> ""
        }

        val interactionState =
            if (interactable == null) null else (if (interactable) "Open To Interaction" else "Closed To Interaction")
        //context.getSharedPreferences("$packageName-analytics", Context.MODE_PRIVATE).apply {
        val properties = hashMapOf<String, Any?>()
        properties["Widget Type"] = kind
        properties["Widget ID"] = id
        properties["Dismiss Action"] = dismissAction
        properties[PROGRAM_ID] = programId

        interactionInfo?.let {
            properties["Number Of Taps"] = interactionInfo.interactionCount
            val timeNow = System.currentTimeMillis()
            val timeSinceLastTap =
                (timeNow - interactionInfo.timeOfLastInteraction).toFloat() / 1000
            val timeSinceStart = (timeNow - interactionInfo.timeOfFirstDisplay).toFloat() / 1000
            properties["Dismiss Seconds Since Last Tap"] = timeSinceLastTap
            properties["Dismiss Seconds Since Start"] = timeSinceStart
        }
        properties["Interactable State"] = interactionState
        //properties["Last Widget Type"] = dataStoreDelegate.getLastWidgetType()
        eventObservers[clientId]?.invoke(KEY_WIDGET_USER_DISMISS, properties)

        // edit().putString("lastWidgetType", kind).apply()
       // dataStoreDelegate.setLastWidgetType(kind)
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]  $kind $action $programId $interactionInfo" }
        // }
    }

    private var lastOrientation: Boolean? = null

    override fun trackInteraction(
        kind: String,
        id: String,
        interactionType: String,
        interactionCount: Int,
        widgetPrompt: String
    ) {
        val properties = hashMapOf<String, Any?>()
        properties["kind"] = kind
        properties["id"] = id
        properties["interactionType"] = interactionType
        properties["interactionCount"] = interactionCount
        properties["widget Prompt"] = widgetPrompt
        eventObservers[clientId]?.invoke(KEY_WIDGET_INTERACTION, properties)
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]  $kind $interactionType $widgetPrompt" }
    }

    override fun trackOrientationChange(isPortrait: Boolean) {
        if (lastOrientation == isPortrait) return // return if the orientation stays the same
        lastOrientation = isPortrait
        hashMapOf<String, Any?>().apply {
            put("Device Orientation", if (isPortrait) "Portrait" else "Landscape")
        }
        hashMapOf<String, Any?>().apply {
            put("Last Device Orientation", if (isPortrait) "Portrait" else "Landscape")
        }
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]  $isPortrait" }
    }

    override fun trackSession(sessionId: String) {
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]  $sessionId" }
    }

    override fun trackButtonTap(buttonName: String, extra: JsonObject) {
        val properties = hashMapOf<String, Any?>()
        properties["buttonName"] = buttonName
        properties["extra"] = extra
        eventObservers[clientId]?.invoke(KEY_ACTION_TAP, properties)
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]  $buttonName" }
    }

    override fun trackUsername(username: String) {
        val properties = hashMapOf<String, Any?>()
        properties["Nickname"] = username
        eventObservers[clientId]?.invoke("Nickname", properties)
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]  $username" }
    }

    private fun getKeyboardType(kType: KeyboardType): String {
        return when (kType) {
            KeyboardType.STANDARD -> "Standard"
            KeyboardType.STICKER -> "Sticker"
        }
    }

    override fun trackKeyboardOpen(keyboardType: KeyboardType) {
        val properties = hashMapOf<String, Any?>()
        properties["Keyboard Type"] = getKeyboardType(keyboardType)
        eventObservers[clientId]?.invoke(KEY_KEYBOARD_SELECTED, properties)
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]  $keyboardType" }
    }

    override fun trackKeyboardClose(
        keyboardType: KeyboardType,
        hideMethod: KeyboardHideReason,
        chatMessageId: String?
    ) {
        val properties = hashMapOf<String, Any?>()
        properties["Keyboard Type"] = getKeyboardType(keyboardType)

        val hideReason = when (hideMethod) {
            KeyboardHideReason.TAP_OUTSIDE -> "Dismissed Via Tap Outside"
            KeyboardHideReason.MESSAGE_SENT -> "Sent Message"
            KeyboardHideReason.CHANGING_KEYBOARD_TYPE -> "Dismissed Via Changing Keyboard Type"
            KeyboardHideReason.BACK_BUTTON -> "Dismissed Via Back Button"
            KeyboardHideReason.EXPLICIT_CALL -> "Dismissed Via explicit call"
        }
        properties["Keyboard Hide Method"] = hideReason
        chatMessageId?.apply {
            properties[CHAT_MESSAGE_ID] = chatMessageId
        }
        eventObservers[clientId]?.invoke(KEY_KEYBOARD_HIDDEN, properties)
        logDebug { "[Analytics]-[${object {}.javaClass.enclosingMethod?.name}]  $keyboardType $hideMethod" }
    }

    companion object {
        const val KEY_CHAT_MESSAGE_SENT = "Chat Message Sent"
        const val KEY_CHAT_MESSAGE_DISPLAYED = "Chat Message Displayed"
        const val KEY_WIDGET_RECEIVED = "Widget_Received"
        const val KEY_WIDGET_DISPLAYED = "Widget Displayed"
        const val KEY_WIDGET_BECAME_INTERACTIVE = "Widget Became Interactive"
        const val KEY_WIDGET_INTERACTION = "Widget Interacted"
        const val KEY_WIDGET_ENGAGED = "Widget Engaged"
        const val KEY_WIDGET_USER_DISMISS = "Widget Dismissed"
        const val KEY_ORIENTATION_CHANGED = "Orientation_Changed"
        const val KEY_ACTION_TAP = "Action_Tap"
        const val KEY_KEYBOARD_SELECTED = "Keyboard Selected"
        const val KEY_KEYBOARD_HIDDEN = "Keyboard Hidden"
        const val KEY_FLAG_BUTTON_PRESSED = "Chat Flag Button Pressed"
        const val KEY_FLAG_ACTION_SELECTED = "Chat Flag Action Selected"
        const val KEY_POINT_TUTORIAL_COMPLETED = "Points Tutorial Completed"
        const val KEY_REASON = "Reason"
        const val KEY_EVENT_BADGE_COLLECTED_BUTTON_PRESSED = "Badge Collected Button Pressed"
        const val KEY_EVENT_CHAT_REACTION_PANEL_OPEN = "Chat Reaction Panel Opened"
        const val KEY_EVENT_CHAT_REACTION_ADDED = "Chat Reaction Added"
        const val KEY_EVENT_CHAT_REACTION_REMOVED = "Chat Reaction Removed"
        const val KEY_EVENT_ALERT_LINK_OPENED = "Alert Link Opened"
        const val KEY_EVENT_VIDEO_ALERT_PLAY_STARTED = "Video Alert Play Started"
        const val KEY_EVENT_CHAT_MESSAGE_LINK_CLICKED = "Chat Message Link Clicked"
    }
}

class AnalyticsWidgetInteractionInfo {
    var interactionCount: Int = 0
    var timeOfFirstInteraction: Long = -1
    var timeOfLastInteraction: Long = 0
    var timeOfFirstDisplay: Long = -1

    // gamification
    var pointEarned: Int = 0
    var badgeEarned: String? = null
    var badgeLevelEarned: Int? = null
    var pointsInCurrentLevel: Int? = null
    var pointsToNextLevel: Int? = null

    fun incrementInteraction() {
        interactionCount += 1

        val timeNow = System.currentTimeMillis()
        if (timeOfFirstInteraction < 0) {
            timeOfFirstInteraction = timeNow
        }
        timeOfLastInteraction = timeNow
    }

    fun widgetDisplayed() {
        timeOfFirstDisplay = System.currentTimeMillis()
    }

    fun reset() {
        interactionCount = 0
        timeOfFirstInteraction = -1
        timeOfLastInteraction = -1
    }

    override fun toString(): String {
        return "interactionCount: $interactionCount, timeOfFirstInteraction:$timeOfFirstInteraction, timeOfLastInteraction: $timeOfLastInteraction"
    }
}

class AnalyticsWidgetSpecificInfo {
    var responseChanges: Int = 0
    var finalAnswerIndex: Int = -1
    var totalOptions: Int = 0
    var userVotePercentage: Int = 0
    var votePosition: Int = 0
    var widgetResult: String = ""

    fun reset() {
        responseChanges = 0
        finalAnswerIndex = -1
        totalOptions = 0
        userVotePercentage = 0
        votePosition = 0
        widgetResult = ""
    }
}

internal var eventObservers: MutableMap<String, ((String, Map<String, Any?>) -> Unit)?> =
    mutableMapOf()

enum class KeyboardHideReason {
    MESSAGE_SENT,
    CHANGING_KEYBOARD_TYPE,
    TAP_OUTSIDE,
    BACK_BUTTON,
    EXPLICIT_CALL // it was added to expose control to integrators.
}

enum class KeyboardType {
    STANDARD,
    STICKER
}

enum class DismissAction {
    TIMEOUT,
    SWIPE,
    TAP_X
}

fun String.findStickerCodes(): Matcher {
    val regex = "(?<=:)[^ :\\s]+(?=:)"
    val pattern = Pattern.compile(regex)
    return pattern.matcher(this)
}

fun Matcher.allMatches(): List<String> {
    val allMatches = mutableListOf<String>()
    while (find()) {
        allMatches.add(":${group()}:")
    }
    return allMatches
}

fun String.findStickers(): Matcher {
    val regex = ":[^ :\\s]*:"
    val pattern = Pattern.compile(regex)
    return pattern.matcher(this)
}

const val CHAT_MESSAGE_ID = "Chat Message ID"
const val ALERT_ID = "Alert Id"
const val PROGRAM_ID = "Program ID"
const val LINK_URL = "Link URL"
const val VIDEO_URL = "Video URL"
const val CHAT_REACTION_ID = "Chat Reaction ID"
const val CHAT_ROOM_ID = "Chat Room ID"
const val WIDGET_TYPE = "Widget Type"
