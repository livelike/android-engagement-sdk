package com.livelike.engagementsdk.core.data.models

import com.google.gson.annotations.SerializedName

data class NumberPredictionVotes(
    @SerializedName("option_id")
    val optionId: String?,
    @SerializedName("number")
    var number: Int
)