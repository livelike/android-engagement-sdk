package com.livelike.engagementsdk.publicapis

import com.livelike.utils.logDebug
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch


@Deprecated("Use com.livelike.common.LiveLikeCallback")
abstract class LiveLikeCallback<T : Any> {

    abstract fun onResponse(result: T?, error: String?)

   /* internal fun processResult(result: Result<T>) {
        if (result is Result.Success) {
            onResponse(result.data, null)
        } else if (result is Result.Error) {
            onResponse(null, result.exception.message ?: "Error in fetching data")
        }
    }*/
}


fun <T : Any, R : Any> LiveLikeCallback<T>.map(transform: (R) -> T?): LiveLikeCallback<R> {
    return object : LiveLikeCallback<R>() {
        override fun onResponse(result: R?, error: String?) {
            try {
                this@map.onResponse(result?.let(transform), error)
            } catch (e: Exception) {
                e.printStackTrace()
                this@map.onResponse(null, e.message)
            }
        }
    }
}

fun <T : Any, R : Any> LiveLikeCallback<T>.map(
    scope: CoroutineScope,
    transform: (R) -> T?
): LiveLikeCallback<R> {
    return object : LiveLikeCallback<R>() {
        override fun onResponse(result: R?, error: String?) {
            try {
                val data = result?.let { transform.invoke(it) }
                logDebug { "LiveLikeCallback map: $data, error:$error" }
                scope.launch {
                    this@map.onResponse(data, error)
                }
            } catch (e: Exception) {
                scope.launch {
                    this@map.onResponse(null, e.message)
                }
            }
        }
    }
}

fun <T : Any> LiveLikeCallback<T>.map(scope: CoroutineScope): LiveLikeCallback<T> {
    return map(scope) {
        it
    }
}

abstract class ErrorDelegate {

    /**
    Called when the given object has failed to setup properly
    Upon receiving this call, the `sdk` should be considered invalid and unuseable.
    If caused by some transient failure like a poor network, a new `EngagementSDK` should
    be created.
     */
    abstract fun onError(error: String)
}

class LiveLikeEmptyResponse
