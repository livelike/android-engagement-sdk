package com.livelike.engagementsdk.publicapis

import com.livelike.engagementsdk.LiveLikeUser

/**
 * User pojo to be exposed, should be minimal in terms of fields
 */

typealias LiveLikeUserApi = LiveLikeUser
