package com.livelike

import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.utils.Once
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.livelike.utils.suspendLazy
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

open class BaseClient(
    protected val configurationOnce: Once<SdkConfiguration>,
    protected val currentProfileOnce: Once<LiveLikeProfile>,
    private val sdkScope: CoroutineScope,
    private val uiScope: CoroutineScope
) {

    protected val configurationProfilePairOnce = suspendLazy {
        val config = configurationOnce()
        val profile = currentProfileOnce()
        profile to config
    }

    protected fun <T : Any> safeCallBack(
        liveLikeCallBack: com.livelike.common.LiveLikeCallback<T>? = null,
        call: suspend (Pair<LiveLikeProfile, SdkConfiguration>) -> T?
    ) {
        sdkScope.launch {
            try {
                val config = configurationOnce()
                val profile = currentProfileOnce()
                val data = call(profile to config)
                uiScope.launch {
                    liveLikeCallBack?.invoke(data, null)
                }
            } catch (e: Exception) {
                logError { e.message }
                uiScope.launch {
                    liveLikeCallBack?.invoke(null, e.message)
                }
            }
            logDebug { "safeCallback invoke end" }
        }
    }

    protected fun <T : Any> safeCallBack(
        call: suspend (Pair<LiveLikeProfile, SdkConfiguration>) -> T?
    ) {
        sdkScope.launch {
            try {
                val config = configurationOnce()
                val profile = currentProfileOnce()
                call(profile to config)
            } catch (e: Exception) {
                logError { e.message }
            }
        }
    }
}