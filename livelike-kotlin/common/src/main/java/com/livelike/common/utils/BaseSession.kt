package com.livelike.common.utils

import com.livelike.common.ErrorDetails
import com.livelike.common.ErrorSeverity
import com.livelike.common.LiveLikeCallbackWithSeverity
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.utils.Once
import com.livelike.utils.UNKNOWN_ERROR
import com.livelike.utils.logError
import com.livelike.utils.suspendLazy
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

abstract class BaseSession(
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    sessionDispatcher: CoroutineDispatcher,
    uiDispatcher: CoroutineDispatcher,
    errorDelegate: ErrorDelegate?
) {
    private val supervisorJob = SupervisorJob()
    private val exceptionHandler = CoroutineExceptionHandler { _, exception ->
        exception.printStackTrace()
        logError { exception }
        errorDelegate?.onError(exception.message ?: UNKNOWN_ERROR)
    }

    protected val configurationProfilePairOnce: Once<Pair<LiveLikeProfile, SdkConfiguration>> =
        suspendLazy {
            val configuration = configurationOnce()
            val user = currentProfileOnce()
            user to configuration
        }

    protected val sessionErrorHandlerScope =
        CoroutineScope(sessionDispatcher + supervisorJob + exceptionHandler)
    val sessionScope = CoroutineScope(sessionDispatcher + supervisorJob)
    protected val uiScope = CoroutineScope(uiDispatcher + supervisorJob)

    fun destroy() {
        supervisorJob.cancel()
        uiScope.cancel()
        sessionScope.cancel()
        sessionErrorHandlerScope.cancel()
    }

    protected fun <T : Any> safeCallBack(
        liveLikeCallBack: com.livelike.common.LiveLikeCallback<T>? = null,
        liveLikeCallbackWithSeverity: LiveLikeCallbackWithSeverity<T>? = null,
        call: suspend (Pair<LiveLikeProfile, SdkConfiguration>) -> T?
    ) {
        sessionScope.launch {
            try {
                val data = call(configurationProfilePairOnce())
                uiScope.launch {
                    liveLikeCallBack?.invoke(data, null)
                    liveLikeCallbackWithSeverity?.invoke(data,null)
                }
            } catch (e: Exception) {
                logError { e.message }
                e.printStackTrace()
                uiScope.launch {
                    liveLikeCallBack?.invoke(null, e.message)
                    liveLikeCallbackWithSeverity?.invoke(null, ErrorDetails(e.message,ErrorSeverity.HIGH))
                }
            }
        }
    }


    protected fun <T : Any> safeCallBack(
        call: suspend (Pair<LiveLikeProfile, SdkConfiguration>) -> T?
    ) {
        sessionScope.launch {
            try {
                call(configurationProfilePairOnce())
            } catch (e: Exception) {
                logError { e.message }
                e.printStackTrace()
            }
        }
    }

}