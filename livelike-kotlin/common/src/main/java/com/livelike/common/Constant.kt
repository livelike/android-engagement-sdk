package com.livelike.common

internal const val ENGAGEMENT_SDK_PROFILE_CREATION_ERROR = "The EngagementSDK is creating a new User Profile because it was initialized without an existing Access Token.\n" + "The created User Profile will be counted towards the Monthly Active Users (MAU) calculation.\n" + "For more information: https://docs.livelike.com/docs/user-profiles"
internal const val TEMPLATE_CHAT_ROOM_ID = "{chat_room_id}"
internal const val TEMPLATE_BLOCKED_PROFILE_ID = "{blocked_profile_id}"

internal const val PROFILE_ID = "profile_id"