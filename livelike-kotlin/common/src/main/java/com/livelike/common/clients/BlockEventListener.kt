package com.livelike.common.clients

import com.livelike.common.model.BlockedInfo


/* The `BlockEventListener` interface defines methods for handling events related to blocking and
unblocking profiles. */
interface BlockEventListener {
    fun onBlockProfile(blockedInfo: BlockedInfo)
    fun onUnBlockProfile(blockInfoId: String, blockProfileId: String)
}