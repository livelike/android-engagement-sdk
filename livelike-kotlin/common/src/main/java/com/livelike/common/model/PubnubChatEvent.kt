package com.livelike.common.model


enum class PubnubChatEventType(val key: String) {
    MESSAGE_CREATED("message-created"),
    MESSAGE_DELETED("message-deleted"),
    IMAGE_CREATED("image-created"),
    IMAGE_DELETED("image-deleted"),
    CUSTOM_MESSAGE_CREATED("custom-message-created"),
    CHATROOM_UPDATED("chatroom-updated"),
    CHATROOM_ADDED("chat-room-add"),
    BLOCK_PROFILE("block-profile"),
    UNBLOCK_PROFILE("unblock-profile"),
    CHATROOM_INVITE("chat-room-invite"),
    MESSAGE_PINNED("message-pinned"),
    MESSAGE_UNPINNED("message-unpinned"),
    USER_REACTION_CREATED("user-reaction-added"),
    USER_REACTION_REMOVED("user-reaction-removed"),
    REACTION_SPACE_UPDATED("reaction-space-updated"),
}

fun String.toPubnubChatEventType(): PubnubChatEventType? =
    when (this) {
        PubnubChatEventType.IMAGE_CREATED.key -> PubnubChatEventType.IMAGE_CREATED
        PubnubChatEventType.IMAGE_DELETED.key -> PubnubChatEventType.IMAGE_DELETED
        PubnubChatEventType.MESSAGE_DELETED.key -> PubnubChatEventType.MESSAGE_DELETED
        PubnubChatEventType.MESSAGE_CREATED.key -> PubnubChatEventType.MESSAGE_CREATED
        PubnubChatEventType.CUSTOM_MESSAGE_CREATED.key -> PubnubChatEventType.CUSTOM_MESSAGE_CREATED
        PubnubChatEventType.CHATROOM_UPDATED.key -> PubnubChatEventType.CHATROOM_UPDATED
        PubnubChatEventType.CHATROOM_ADDED.key -> PubnubChatEventType.CHATROOM_ADDED
        PubnubChatEventType.CHATROOM_INVITE.key -> PubnubChatEventType.CHATROOM_INVITE
        PubnubChatEventType.MESSAGE_PINNED.key -> PubnubChatEventType.MESSAGE_PINNED
        PubnubChatEventType.MESSAGE_UNPINNED.key -> PubnubChatEventType.MESSAGE_UNPINNED
        PubnubChatEventType.BLOCK_PROFILE.key -> PubnubChatEventType.BLOCK_PROFILE
        PubnubChatEventType.USER_REACTION_CREATED.key -> PubnubChatEventType.USER_REACTION_CREATED
        PubnubChatEventType.USER_REACTION_REMOVED.key -> PubnubChatEventType.USER_REACTION_REMOVED
        PubnubChatEventType.REACTION_SPACE_UPDATED.key -> PubnubChatEventType.REACTION_SPACE_UPDATED
        else -> null
    }
