package com.livelike.common

import com.livelike.common.model.RequestType
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.utils.BaseSession
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.EpochTime
import com.livelike.engagementsdk.MockAnalyticsService
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.network.NetworkApiClient
import com.livelike.network.NetworkClientConfig
import com.livelike.utils.SDKLoggerBridge
import com.livelike.utils.UNKNOWN_ERROR
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.livelike.utils.registerLoggerBridge
import com.livelike.utils.suspendLazy
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

open class LiveLikeKotlin(
    clientId: String,
    val errorDelegate: ErrorDelegate? = null,
    originURL: String = "https://cf-blast.livelikecdn.com",
    val dataStoreDelegate: DataStoreDelegate = DataStoreDelegateKotlinImpl(),
    internal var accessTokenDelegate: AccessTokenDelegate? = object : AccessTokenDelegate {
        override fun getAccessToken(): String? = dataStoreDelegate.getAccessToken()

        override fun storeAccessToken(accessToken: String?) {
            accessToken?.let { dataStoreDelegate.setAccessToken(accessToken) }
        }
    },
    userAgent: String? = "LiveLikeKotlin/2.98.8",
    networkDispatcher: CoroutineDispatcher = Dispatchers.IO,
    sdkDispatcher: CoroutineDispatcher = Dispatchers.Default,
    mainDispatcher: CoroutineDispatcher = Dispatchers.Main,
    val networkClient: NetworkApiClient = NetworkApiClient.getInstance(
        NetworkClientConfig(userAgent), networkDispatcher
    )
) : ILiveLikeKotlin {
    val baseUrl = originURL.plus("/api/v1/")
    private val sdkJob = SupervisorJob()
    private val handler = CoroutineExceptionHandler { _, exception ->
        exception.printStackTrace()
        logError { exception }
        errorDelegate?.onError(exception.message ?: UNKNOWN_ERROR)
    }

    val sdkScope = CoroutineScope(sdkDispatcher + sdkJob)
    private val sdkWithErrorHandlerScope = CoroutineScope(sdkDispatcher + sdkJob + handler)
    val uiScope = CoroutineScope(mainDispatcher + sdkJob)
    var rewardTypeFlow: MutableStateFlow<String?> = MutableStateFlow("none")

    override var analyticService: AnalyticsService =
        MockAnalyticsService(dataStoreDelegate = dataStoreDelegate)

    protected val baseSessionList = arrayListOf<BaseSession>()

    fun addSession(session: BaseSession) {
        baseSessionList.add(session)
    }

    val sdkConfigurationOnce = suspendLazy<SdkConfiguration> {
        val deferred = CompletableDeferred<SdkConfiguration>()
        request<String, SdkConfiguration>(
            "applications/$clientId",
            null,
            RequestType.GET,
        ) { result, error ->
            if (error != null) {
                logError { error }
                errorDelegate?.onError(error ?: UNKNOWN_ERROR)
                deferred.complete(SdkConfiguration.empty())
            } else {
                deferred.complete(result as SdkConfiguration)
            }
        }
        deferred.await()
    }

    init {
        registerLoggerBridge(SDKLoggerBridge(exceptionLogger = { _, _, message, throwable ->
            // Log.println(level.code, tag, "$message\n${Log.getStackTraceString(throwable)}")
            println(message)
            throwable.printStackTrace()
        }, logger = { _, _, message ->
            // Log.println(level.code, tag, "$message")
            println(message)
        }))
        sdkWithErrorHandlerScope.launch {
            logDebug { "Initializing SDK" }
            profile().currentProfileOnce()
        }
    }

    /**
     * Closing all the services , stream and clear the variable
     * TODO: all stream close,instance clear
     */
    override fun close() {
        accessTokenDelegate = null
        destroyProfile()
        logDebug { "closing SDK" }
        sdkJob.cancel()
        sdkWithErrorHandlerScope.cancel()
        sdkScope.cancel()
        uiScope.cancel()
        for (it in baseSessionList) {
            it.destroy()
        }
        baseSessionList.clear()
    }

    @Deprecated(
        "Use TimeCodeGetter",
        ReplaceWith("com.livelike.common.TimeCodeGetter")
    )
    interface TimecodeGetterCore {
        fun getTimecode(): EpochTime
    }

}

typealias TimeCodeGetter = () -> EpochTime

