package com.livelike.common.utils

import com.livelike.engagementsdk.Stream
import com.livelike.utils.Once
import com.livelike.utils.logDebug
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import java.util.concurrent.ConcurrentHashMap

class SubscriptionManager<T>(private val emitOnSubscribe: Boolean = true) :
    Stream<T> {
    override fun latest(): T? {
        return currentData
    }

    private val observerMap = ConcurrentHashMap<Any, (T?) -> Unit>()
    private var currentData: T? = null
        private set

    override fun onNext(data1: T?) {
        // TODO add debug log with class name appended
        logDebug { "subscription Manger: ${observerMap.size},data:$data1" }
        // Important change if not working properly revert back
        currentData = data1
        safeCodeBlockCall({
            for (it in observerMap) {
                it.value.invoke(data1)
            }
        })
    }

    override fun subscribe(key: Any, observer: (T?) -> Unit) {
        observerMap[key] = observer
        if (emitOnSubscribe) observer.invoke(currentData)
    }

    override fun unsubscribe(key: Any) {
        if (observerMap.containsKey(key))
            observerMap.remove(key)
    }

    override fun clear() {
        currentData = null
        onNext(null)
        observerMap.clear()
    }
}

@Suppress("UNUSED_PARAMETER")
private fun safeCodeBlockCall(call: () -> Unit, errorMessage: String? = null) {

    return try {
        call()
    } catch (ex: Throwable) {
        ex.cause?.printStackTrace() ?: ex.printStackTrace()
    }
}

/**
 * Applies the given function on the same thread to each value emitted by source stream and returns stream, which emits resulting values.
 */
internal fun <X, Y> Stream<X>.map(applyTransformation: (x: X) -> Y): Stream<Y> {

    val out = SubscriptionManager<Y>()
    this.subscribe(out.hashCode()) {
        it?.let {
            out.onNext(applyTransformation(it))
        }
    }

    return out
}

fun <X> Flow<X?>.toStream(
    scope: CoroutineScope,
    emitOnSubscribe: Boolean = true
): Stream<X> {
    val stream = SubscriptionManager<X>(emitOnSubscribe)
    scope.launch {
        this@toStream.collect {
            stream.onNext(it)
        }
    }
    return stream
}

fun <X : Any> Once<X>.toStream(scope: CoroutineScope, emitOnSubscribe: Boolean = true): Stream<X> {
    val stream = SubscriptionManager<X>(emitOnSubscribe)
    scope.launch {
        val item = this@toStream.invoke(true)
        stream.onNext(item)
    }
    return stream
}
