package com.livelike.common.model

enum class RequestType {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE
}