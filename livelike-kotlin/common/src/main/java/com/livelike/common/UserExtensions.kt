package com.livelike.common

import com.livelike.common.clients.LiveLikeProfileClient
import com.livelike.common.clients.LiveLikeUserClient


private val sdkInstanceWithChatClient = mutableMapOf<Int, LiveLikeProfileClient?>()

/* The `fun LiveLikeKotlin.profile(): LiveLikeProfileClient` function is a Kotlin extension function
that is defined on the `LiveLikeKotlin` class. */
@Synchronized
fun LiveLikeKotlin.profile(): LiveLikeProfileClient {
   /* val key = this.hashCode()
    return if (sdkInstanceWithChatClient.containsKey(key)) {
        sdkInstanceWithChatClient[key]!!
    } else {
        LiveLikeProfileClient.getInstance(
            sdkConfigurationOnce,
            uiScope,
            sdkScope,
            networkClient,
            accessTokenDelegate,
        ).let {
            sdkInstanceWithChatClient[key] = it
            it
        }
    }*/

    val key = this.hashCode()
    return sdkInstanceWithChatClient[key] ?: synchronized(this) {
        sdkInstanceWithChatClient[key] ?: LiveLikeProfileClient.getInstance(
            sdkConfigurationOnce,
            uiScope,
            sdkScope,
            networkClient,
            accessTokenDelegate,
        ).also { sdkInstanceWithChatClient[key] = it }
    }
}

fun LiveLikeKotlin.destroyProfile() {
    val key = this.hashCode()
    if (sdkInstanceWithChatClient.containsKey(key)) {
        sdkInstanceWithChatClient[key]?.cleanup()
        sdkInstanceWithChatClient[key] = null
    }
}

@Deprecated("Use profile()", replaceWith = ReplaceWith("com.livelike.common.LiveLikeKotlin.profile()"))
fun LiveLikeKotlin.user(): LiveLikeUserClient = profile()