package com.livelike.common

import com.livelike.engagementsdk.core.data.models.NumberPredictionVotes


interface DataStoreDelegate {


    /**
     * returns User Access Token for a user profile
     */
    fun getAccessToken(): String?

    /**
     * store User Access Token for a user profile
     */
    fun setAccessToken(accessToken: String)

    /**
     * returns json string containing map of chatroom with its message timestamps
     */
    fun getChatRoomMessageReceived(): String?

    /**
     * store json string containing map of chatroom with its message timestamps
     * @param data argument used to store json string containing map of chatroom with its message timestamps
     */
    fun setChatRoomMessageReceived(data: String)

    /**
     * returns json string containing map of widgetId associated with claim token
     */
    fun getWidgetClaimToken(): String?

    /**
     * store json string containing map of widgetId associated with claim token
     * @param token argument used to set json string containing map of widgetId associated with claim token
     */
    fun setWidgetClaimToken(token: String)

    /**
     * add published message in messageList in current channel
     * @param channel argument passed to get current channel
     * @param messageId argument passed to get new published message
     */
    fun addPublishedMessage(channel: String, messageId: String)

    /**
     * removes list of published messages for given channel
     * @param channels argument whose message list has to be removed
     */
    fun flushPublishedMessage(vararg channels: String)

    /**
     * returns list of published messages for given channel
     * @param channel argument whose message list has to be fetched
     */
    fun getPublishedMessages(channel: String): MutableSet<String>

    /**
     *  store array of SavedWidgetVote
     *  @param id represents widgetId
     *  @param optionId represents voted option
     */
    fun addWidgetPredictionVoted(id: String, optionId: String)

    /**
     * returns array of SavedWidgetVote containing widgetId and optionId
     */
    fun getWidgetPredictionVoted(): Array<SavedWidgetVote>

    /**
     * returns widget selected option id
     * @param id represents widgetId
     */
    fun getWidgetPredictionVotedAnswerIdOrEmpty(id: String?): String

    /**
     * add points to the total points
     * @param points argument to add new points
     */
    fun addPoints(points: Int)

    /**
     * returns total points for a user
     */
    fun getTotalPoints(): Int

    /**
     * returns boolean for showing PointTutorial or not
     */
    fun shouldShowPointTutorial(): Boolean

    /**
     * returns boolean for checking status if PointTutorial is seen or not
     */
    fun pointTutorialSeen()

    /**
     * store array of NumberPredictionSavedWidgetVote containing widgetId and optionId and number of votes
     * @param id argument represents widgetId
     * @param widgetVoteList argument represents list of NumberPredictionVotes
     */
    fun addWidgetNumberPredictionVoted(id: String, widgetVoteList: List<NumberPredictionVotes>)

    /**
     * returns array of NumberPredictionSavedWidgetVote
     */
    fun getWidgetNumberPredictionVoted(): Array<NumberPredictionSavedWidgetVote>

}


data class SavedWidgetVote(
    val id: String,
    val optionId: String
)


data class NumberPredictionSavedWidgetVote(
    val id: String,
    val optionId: String,
    val number: Int
)