package com.livelike.common.model

import com.google.gson.annotations.SerializedName
import com.livelike.common.TEMPLATE_CHAT_ROOM_ID


data class SdkConfiguration(
    val url: String,
    val name: String,
    @SerializedName("client_id")
    val clientId: String,
    @SerializedName("media_url")
    val mediaUrl: String,
    @SerializedName("pubnub_subscribe_key")
    val pubNubKey: String?,
    @SerializedName("pubnub_publish_key")
    val pubnubPublishKey: String?,
    @SerializedName("sendbird_app_id")
    val sendBirdAppId: String?,
    @SerializedName("sendbird_api_endpoint")
    val sendBirdEndpoint: String?,
    @SerializedName("programs_url")
    val programsUrl: String,
    @SerializedName("sessions_url")
    val sessionsUrl: String,
    @SerializedName("sticker_packs_url")
    val stickerPackUrl: String,
    @SerializedName("reaction_packs_url")
    val reactionPacksUrl: String,
    @SerializedName("mixpanel_token")
    val mixpanelToken: String,
    @SerializedName("analytics_properties")
    val analyticsProps: Map<String, String>,
    @SerializedName("chat_room_detail_url_template")
    val chatRoomDetailUrlTemplate: String,
    @SerializedName("create_chat_room_url")
    val createChatRoomUrl: String,
    @SerializedName("profile_url")
    val profileUrl: String,
    @SerializedName("profile_detail_url_template")
    val profileDetailUrlTemplate: String,
    @SerializedName("program_detail_url_template")
    val programDetailUrlTemplate: String,
    @SerializedName("pubnub_origin")
    val pubnubOrigin: String? = null,
    @SerializedName("leaderboard_detail_url_template")
    val leaderboardDetailUrlTemplate: String,
    @SerializedName("pubnub_heartbeat_interval")
    val pubnubHeartbeatInterval: Int,
    @SerializedName("pubnub_presence_timeout")
    val pubnubPresenceTimeout: Int,
    @SerializedName("badges_url")
    val badgesUrl: String,
    @SerializedName("reward_items_url") val rewardItemsUrl: String,
    @SerializedName("reward_actions_url") val rewardActionsUrl: String,
    @SerializedName("invoked_actions_url") val invokedActionsUrl: String,
    @SerializedName("user_search_url")
    val userSearchUrl: String,
    @SerializedName("chat_rooms_invitations_url")
    val chatRoomsInvitationsUrl: String,
    @SerializedName("chat_room_invitation_detail_url_template")
    val chatRoomInvitationDetailUrlTemplate: String,
    @SerializedName("create_chat_room_invitation_url")
    val createChatRoomInvitationUrl: String,
    @SerializedName("profile_chat_room_received_invitations_url_template")
    val profileChatRoomReceivedInvitationsUrlTemplate: String,
    @SerializedName("profile_chat_room_sent_invitations_url_template")
    val profileChatRoomSentInvitationsUrlTemplate: String,
    @SerializedName("pinned_messages_url")
    val pinnedMessageUrl: String,
    @SerializedName("reward_transactions_url")
    val rewardTransactionsUrl: String,
    @SerializedName("sponsors_url")
    val sponsorsUrl: String,
    @SerializedName("redemption_keys_url")
    val redemptionKeysUrl: String,
    @SerializedName("quests_url")
    val questsUrl: String,
    @SerializedName("quest_detail_url_template")
    val questDetailUrlTemplate: String,
    @SerializedName("user_quests_url")
    val userQuestsUrl: String,
    @SerializedName("user_quests_url_template")
    val userQuestsUrlTemplate: String,
    @SerializedName("user_quest_detail_url_template")
    val userQuestDetailUrlTemplate: String,
    @SerializedName("quest_tasks_url")
    val questTasksUrl: String,
    @SerializedName("user_quest_task_progress_url")
    val userQuestTaskProgressUrl: String,
    @SerializedName("user_quest_rewards_url")
    val userQuestRewardsUrl: String,
    @SerializedName("quest_rewards_url")
    val questRewardsUrl: String?,
    @SerializedName("redemption_key_detail_url_template")
    val redemptionKeyDetailUrlTemplate: String,
    @SerializedName("redemption_key_detail_by_code_url_template")
    val redemptionKeyDetailByCodeUrlTemplate: String,
    @SerializedName("widget_detail_url_template")
    val widgetDetailUrlTemplate: String,
    @SerializedName("video_room_url")
    val videoRoomUrl: String,
    @SerializedName("video_room_detail_url_template")
    val videoRoomDetailUrlTemplate: String,
    @SerializedName("reaction_space_detail_url_template")
    val reactionSpaceDetailUrlTemplate: String,
    @SerializedName("reaction_spaces_url")
    val reactionSpacesUrl: String,
    @SerializedName("reaction_pack_detail_url_template")
    val reactionPackDetailUrlTemplate: String,
    @SerializedName("profile_leaderboards_url_template")
    val profileLeaderboardsUrlTemplate: String,
    @SerializedName("profile_leaderboard_views_url_template")
    val profileLeaderboardsViewsUrlTemplate: String,
    @SerializedName("chat_room_events_url")
    val chatRoomEventsUrl: String,
    @SerializedName("api_polling_interval")
    val apiPollingInterval: Int,
    @SerializedName("badge_profiles_url")
    val badgeProfilesUrl: String,
    @SerializedName("program_custom_id_url_template")
    val programCustomIdUrlTemplate: String,
    @SerializedName("comment_boards_url")
    val commentBoardsUrl: String,
    @SerializedName("comment_board_detail_url_template")
    val commentBoardDetailUrlTemplate: String,
    @SerializedName("comment_board_ban_url")
    val commentBoardBanUrl: String,
    @SerializedName("comment_board_ban_detail_url_template")
    val commentBoardBanUrlTemplate: String,
    @SerializedName("profile_relationship_types_url")
    val profileRelationshipTypesUrl: String,
    @SerializedName("profile_relationships_url")
    val profileRelationshipsUrl: String,
    @SerializedName("profile_relationship_detail_url_template")
    val profileRelationshipDetailUrlTemplate: String,
    @SerializedName("comment_report_url")
    val commentReportUrl: String,
    @SerializedName("autoclaim_interaction_rewards")
    val autoClaimInteractionRewards: Boolean = false,
    @SerializedName("comment_report_detail_url_template")
    val commentReportDetailUrlTemplate: String,
    @SerializedName("saved_contract_addresses_url")
    val savedContractAddressesUrl: String,
    @SerializedName("chat_room_memberships_url")
    val chatRoomMembershipUrl: String,
    @SerializedName("text_polls_url") val textPollUrl: String,
    @SerializedName("image_polls_url") val imagePollUrl: String,
    @SerializedName("cheer_meters_url") val cheerMeterUrl: String,
    @SerializedName("emoji_sliders_url") val emojiSliderUrl: String,
    @SerializedName("text_quizzes_url") val textQuizUrl: String,
    @SerializedName("image_quizzes_url") val imageQuizUrl: String,
    @SerializedName("text_predictions_url") val textPredictionUrl: String,
    @SerializedName("image_predictions_url") val imagePredictionUrl: String,
    @SerializedName("image_number_predictions_url") val imageNumberPredictionUrl: String,
    @SerializedName("image_number_prediction_followUps_url") val imageNumberPredictionFollowUpsUrl: String,
    @SerializedName("text_asks_url") val textAskUrl: String,
    @SerializedName("alerts_url") val alertsUrl: String,
    @SerializedName("rich_posts_url") val richPostsUrl: String,
    @SerializedName("roles_url") val rolesUrl: String,
    @SerializedName("role_assignments_url") val profileRoleAssignmentsUrl: String,
    @SerializedName("resources_url") val resourcesUrl: String,
    @SerializedName("permissions_url") val permissionsUrl: String,
    @SerializedName("scopes_url") val scopesUrl: String,
    @SerializedName("profile_role_assignments_url_template")val profileRoleAssignmentsUrlTemplate: String,
    @SerializedName("role_assignments_url_template") val profileRoleAssignmentDetailUrlTemplate: String,

) {

    companion object {
        fun empty() = SdkConfiguration(
            url = "",
            name = "",
            clientId = "",
            mediaUrl = "",
            pubNubKey = null,
            pubnubPublishKey = null,
            sendBirdAppId = null,
            sendBirdEndpoint = null,
            programsUrl = "",
            sessionsUrl = "",
            stickerPackUrl = "",
            reactionPacksUrl = "",
            mixpanelToken = "",
            analyticsProps = emptyMap(),
            chatRoomDetailUrlTemplate = "",
            createChatRoomUrl = "",
            profileUrl = "",
            profileDetailUrlTemplate = "",
            programDetailUrlTemplate = "",
            pubnubOrigin = null,
            leaderboardDetailUrlTemplate = "",
            pubnubHeartbeatInterval = 0,
            pubnubPresenceTimeout = 0,
            badgesUrl = "",
            rewardItemsUrl = "",
            rewardActionsUrl = "",
            invokedActionsUrl = "",
            userSearchUrl = "",
            chatRoomsInvitationsUrl = "",
            chatRoomInvitationDetailUrlTemplate = "",
            createChatRoomInvitationUrl = "",
            profileChatRoomReceivedInvitationsUrlTemplate = "",
            profileChatRoomSentInvitationsUrlTemplate = "",
            pinnedMessageUrl = "",
            rewardTransactionsUrl = "",
            sponsorsUrl = "",
            redemptionKeysUrl = "",
            questsUrl = "",
            questDetailUrlTemplate = "",
            userQuestsUrl = "",
            userQuestsUrlTemplate = "",
            userQuestDetailUrlTemplate = "",
            questTasksUrl = "",
            userQuestTaskProgressUrl = "",
            userQuestRewardsUrl = "",
            questRewardsUrl = null,
            redemptionKeyDetailUrlTemplate = "",
            redemptionKeyDetailByCodeUrlTemplate = "",
            widgetDetailUrlTemplate = "",
            videoRoomUrl = "",
            videoRoomDetailUrlTemplate = "",
            reactionSpaceDetailUrlTemplate = "",
            reactionSpacesUrl = "",
            reactionPackDetailUrlTemplate = "",
            profileLeaderboardsUrlTemplate = "",
            profileLeaderboardsViewsUrlTemplate = "",
            chatRoomEventsUrl = "",
            apiPollingInterval = 0,
            badgeProfilesUrl = "",
            programCustomIdUrlTemplate = "",
            commentBoardsUrl = "",
            commentBoardDetailUrlTemplate = "",
            commentBoardBanUrl = "",
            commentBoardBanUrlTemplate = "",
            profileRelationshipTypesUrl = "",
            profileRelationshipsUrl = "",
            profileRelationshipDetailUrlTemplate = "",
            commentReportUrl = "",
            autoClaimInteractionRewards = false,
            commentReportDetailUrlTemplate = "",
            savedContractAddressesUrl = "",
            chatRoomMembershipUrl = "",
            textPollUrl = "",
            imagePollUrl = "",
            cheerMeterUrl = "",
            emojiSliderUrl = "",
            textQuizUrl = "",
            imageQuizUrl = "",
            textPredictionUrl = "",
            imagePredictionUrl = "",
            imageNumberPredictionUrl = "",
            imageNumberPredictionFollowUpsUrl = "",
            textAskUrl = "",
            alertsUrl = "",
            richPostsUrl = "",
            rolesUrl = "",
            profileRoleAssignmentsUrl = "",
            resourcesUrl = "",
            permissionsUrl = "",
            scopesUrl = "",
            profileRoleAssignmentsUrlTemplate = "",
            profileRoleAssignmentDetailUrlTemplate = ""
        )
    }

    fun getChatRoomUrlFromTemplate(chatRoomId: String): String {
        return chatRoomDetailUrlTemplate.replace(TEMPLATE_CHAT_ROOM_ID, chatRoomId)
    }
}