package com.livelike.common

import com.livelike.utils.logDebug
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

typealias LiveLikeCallback<T> = (result: T?, error: String?) -> Unit

typealias LiveLikeCallbackWithSeverity<T> =  (result: T?, error: ErrorDetails?) -> Unit


fun <T : Any> com.livelike.engagementsdk.publicapis.LiveLikeCallback<T>.toNewCallback(): LiveLikeCallback<T> {
    val callback: LiveLikeCallback<T> = { result, error ->
        this.onResponse(result, error)
    }
    return callback
}

fun <T : Any, R : Any> LiveLikeCallback<T>.map(
    scope: CoroutineScope,
    transform: (R) -> T?
): LiveLikeCallback<R> {
    return { result, error ->
        try {
            val data = result?.let { transform.invoke(it) }
            logDebug { "LiveLikeCallback map: $data, error:$error" }
            scope.launch {
                this@map.invoke(data, error)
            }
        } catch (e: Exception) {
            scope.launch {
                this@map.invoke(null, e.message)
            }
        }
    }
}

fun <T : Any, R : Any> LiveLikeCallback<T>.map(
    transform: (R) -> T?
): LiveLikeCallback<R> {
    return { result, error ->
        try {
            val data = result?.let { transform.invoke(it) }
            logDebug { "LiveLikeCallback map: $data, error:$error" }
            this@map.invoke(data, error)
        } catch (e: Exception) {
            this@map.invoke(null, e.message)
        }
    }
}

fun <T : Any> LiveLikeCallback<T>.map(scope: CoroutineScope): LiveLikeCallback<T> {
    return map(scope) { it }
}

/**
 * used for error severity
 *all api errors are treated as high severity
 */
enum class ErrorSeverity{
    LOW,
    HIGH
}

/**
 * used in callback for returning error severity
 *
 */
data class ErrorDetails(var errorMessage: String?, val errorSeverity:ErrorSeverity?)
