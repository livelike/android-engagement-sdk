package com.livelike.common.utils


import com.livelike.engagementsdk.publicapis.LiveLikeCallback
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


/*@Deprecated("Use SafeCallback with com.livelike.common.LiveLikeCallback")
fun <T : Any> safeCallBack(
    scope: CoroutineScope,
    coreLiveLikeCallBack: LiveLikeCallback<T>? = null,
    call: suspend () -> T?
): Job {
    return scope.launch {
        try {
            logDebug { "before safeCallback invoke" }
            val data = call.invoke()
            logDebug { "after safeCallback invoke" }
            coreLiveLikeCallBack?.onResponse(data, null)
        } catch (e: Exception) {
            logError { e.message }
            e.printStackTrace()
            coreLiveLikeCallBack?.onResponse(null, e.message)
        }
    }
}*/

fun <T : Any> safeCallBack(
    scope: CoroutineScope,
    coreLiveLikeCallBack: com.livelike.common.LiveLikeCallback<T>? = null,
    call: suspend () -> T
): Job {
    return scope.launch {
        try {
            logDebug { "before safeCallback invoke" }
            val data = call.invoke()
            logDebug { "after safeCallback invoke" }
            coreLiveLikeCallBack?.invoke(data, null)
        } catch (e: Exception) {
            logError { e.message }
            e.printStackTrace()
            coreLiveLikeCallBack?.invoke(null, e.message)
        }
    }
}

fun <T : Any> safeCallBack(
    scope: CoroutineScope,
    call: suspend () -> T?
): Job {
    return scope.launch {
        try {
            logDebug { "before safeCallback invoke" }
            call.invoke()
            logDebug { "after safeCallback invoke" }
        } catch (e: Exception) {
            logError { e.message }
            e.printStackTrace()
        }
    }
}