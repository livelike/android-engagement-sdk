package com.livelike.common.clients

/**
 * Represents a MessagingClient error that can be sent from a MessagingClient
 */
data class Error(val type: String, val message: String, val clientMessageId: String?)