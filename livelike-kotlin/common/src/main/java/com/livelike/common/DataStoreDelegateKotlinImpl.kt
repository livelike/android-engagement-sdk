package com.livelike.common

import com.livelike.engagementsdk.core.data.models.NumberPredictionVotes

class DataStoreDelegateKotlinImpl : DataStoreDelegate {

    private var mAccessToken: String? = ""
    private var mChatRoomMessageReceived: String? = ""
    private var mClaimToken: String? = ""
    private val mPublishedMessage = mutableMapOf<String, MutableSet<String>>()
    private var mPoint = 0
    private var mShowPointTutorial = false
    private val arrPredictionWidgetVote = mutableListOf<SavedWidgetVote>()
    private var listNumberPredictionVote = mutableListOf<NumberPredictionSavedWidgetVote>()

    override fun getAccessToken(): String? {
        return mAccessToken
    }

    override fun setAccessToken(accessToken: String) {
        mAccessToken = accessToken
    }

    override fun getChatRoomMessageReceived(): String? {
        return mChatRoomMessageReceived
    }

    override fun setChatRoomMessageReceived(data: String) {
        mChatRoomMessageReceived = data
    }

    override fun getWidgetClaimToken(): String? {
        return mClaimToken
    }

    override fun setWidgetClaimToken(token: String) {
        mClaimToken = token
    }

    override fun addPublishedMessage(channel: String, messageId: String) {
        val msgList = getPublishedMessages(channel)
        msgList.add(messageId)
        mPublishedMessage[channel] = msgList
    }

    override fun flushPublishedMessage(vararg channels: String) {
        for (channel in channels) {
            mPublishedMessage.remove(channel)
        }
    }

    override fun getPublishedMessages(channel: String): MutableSet<String> {
        return mPublishedMessage[channel] ?: mutableSetOf()
    }

    override fun addWidgetPredictionVoted(id: String, optionId: String) {
        arrPredictionWidgetVote.add(SavedWidgetVote(id, optionId))

    }

    override fun getWidgetPredictionVoted(): Array<SavedWidgetVote> {
        return arrPredictionWidgetVote.toTypedArray()
    }

    override fun getWidgetPredictionVotedAnswerIdOrEmpty(id: String?): String {
        return getWidgetPredictionVoted().find { it.id == id }?.optionId ?: ""
    }

    override fun addPoints(points: Int) {
        mPoint = points
    }

    override fun getTotalPoints(): Int {
        return mPoint
    }

    override fun shouldShowPointTutorial(): Boolean {
        return mShowPointTutorial
    }

    override fun pointTutorialSeen() {
        mShowPointTutorial = false
    }

    override fun addWidgetNumberPredictionVoted(
        id: String,
        widgetVoteList: List<NumberPredictionVotes>
    ) {
        val idsList = getWidgetNumberPredictionVoted().toMutableSet()
        for (item in widgetVoteList) {
            idsList.add(NumberPredictionSavedWidgetVote(id, item.optionId ?: "", item.number))
        }
        listNumberPredictionVote = idsList.toMutableList()
    }

    override fun getWidgetNumberPredictionVoted(): Array<NumberPredictionSavedWidgetVote> {
        return listNumberPredictionVote.toTypedArray()
    }
}