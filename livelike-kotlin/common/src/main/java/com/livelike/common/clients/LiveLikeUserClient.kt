package com.livelike.common.clients

import com.livelike.common.AccessTokenDelegate
import com.livelike.common.model.BlockedInfo
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.Stream
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.network.NetworkApiClient
import com.livelike.utils.Once
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

interface LiveLikeProfileClient {
    /* The `currentProfileOnce` property is a `Once` object that holds the current `LiveLikeProfile`
    instance. The `Once` class is a utility class that ensures that the value it holds is only
    computed once and then cached for subsequent access. In this case, `currentProfileOnce` holds the
    current user profile information and provides a way to access it. */
    val currentProfileOnce: Once<LiveLikeProfile>

    /* `val currentProfileFlow: StateFlow<LiveLikeProfile?>` is declaring a property named
    `currentProfileFlow` of type `StateFlow<LiveLikeProfile?>`. */
    val currentProfileFlow: StateFlow<LiveLikeProfile?>

    /**
     * Returns public user stream.
     */
    /* The `val profileStream: Stream<LiveLikeProfile>` is declaring a property named `profileStream`
    of type `Stream<LiveLikeProfile>`. This property represents a stream of `LiveLikeProfile`
    objects. A stream is a sequence of values that can be observed and reacted to. In this case, the
    `profileStream` property provides a way to observe and react to changes in the user profiles. */
    val profileStream: Stream<LiveLikeProfile>

    /**
     * blocks profile
     * @param profile id for blocking
     */
    /**
     * The function `blockProfile` takes a profile ID and a callback function as parameters and blocks
     * the specified profile.
     * 
     * @param profileId A string representing the ID of the profile that needs to be blocked.
     * @param liveLikeCallback The `liveLikeCallback` parameter is a callback function that will be
     * called after the `blockProfile` function completes. It is of type
     * `com.livelike.common.LiveLikeCallback<BlockedInfo>`. This callback function will receive a
     * `BlockedInfo` object as a parameter, which contains information
     */
    fun blockProfile(
        profileId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<BlockedInfo>
    )


    /**
     * unblocks profile
     * @param profile id for unblocking
     */
    /**
     * The function `unBlockProfile` takes a blockId and a callback as parameters and is used to
     * unblock a profile.
     * 
     * @param blockId A string representing the ID of the profile that needs to be unblocked.
     * @param liveLikeCallback The `liveLikeCallback` parameter is a callback function that will be
     * called after the `unBlockProfile` function completes. It is of type
     * `com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>`. This callback function is used to
     * handle the response or error returned by the `
     */
    fun unBlockProfile(
        blockId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    )


    /**
     * get block profile list
     * @param liveLikePagination Pagination
     */
    /**
     * The function `getBlockedProfileList` takes in a pagination object and a callback, and returns a
     * list of blocked profiles.
     * 
     * @param liveLikePagination The `liveLikePagination` parameter is an object that contains
     * information about the pagination settings for retrieving a list of blocked profiles. It may
     * include properties such as the page number, page size, and sorting options.
     * @param liveLikeCallback The liveLikeCallback parameter is a callback function that will be
     * called with the result of the getBlockedProfileList function. It takes a List<BlockedInfo> as a
     * parameter, which represents the list of blocked profiles.
     */
    fun getBlockedProfileList(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<BlockedInfo>>
    )


    fun getProfileBlockIds(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<String>>)

    fun getProfileBlockInfo(profileId: String, liveLikeCallback: com.livelike.common.LiveLikeCallback<BlockedInfo>)


    /** Override the default auto-generated chat nickname **/
  /**
   * The function `updateChatNickname` takes a nickname and a callback function as parameters and
   * updates the chat nickname.
   * 
   * @param nickname A string representing the new nickname for the chat user.
   * @param callback A callback function that takes a LiveLikeProfile object as a parameter. This
   * callback function will be called after the chat nickname is updated.
   */
    fun updateChatNickname(
        nickname: String,
        callback: com.livelike.common.LiveLikeCallback<LiveLikeProfile>
    )

    /** Override the default auto-generated chat userpic **/
    /**
     * The function `updateChatUserPic` updates the user's profile picture in a chat application.
     * 
     * @param url A nullable String parameter that represents the URL of the user's profile picture.
     * @param callback A callback function that will be called after the user's profile picture has
     * been updated. The callback function should take a parameter of type `LiveLikeProfile`, which
     * represents the updated user profile.
     */
   /**
    * The function `updateChatUserPic` is used to update the profile picture of a chat user and takes a
    * URL and a callback as parameters.
    * 
    * @param url A string representing the URL of the user's profile picture.
    * @param callback A callback function that will be called after the user's profile picture has been
    * updated. The callback function should take a parameter of type `LiveLikeProfile`, which
    * represents the updated user profile.
    */
    fun updateChatUserPic(
        url: String?,
        callback: com.livelike.common.LiveLikeCallback<LiveLikeProfile>
    )


    /** Fetch current user details **/
    /**
     * The function getCurrentUserDetails takes a LiveLikeCallback as a parameter and retrieves the
     * current user's details.
     * 
     * @param liveLikeCallback A callback function that will be called with the user's profile details.
     * The callback function should have a parameter of type `LiveLikeProfile`, which represents the
     * user's profile information.
     */
    fun getCurrentUserDetails(liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeProfile>)


    /** Update the user custom data **/
  /**
   * The function `updateUserCustomData` updates the custom data of a user and invokes a callback with
   * the updated user profile.
   * 
   * @param customData A string representing the custom data that needs to be updated for the user.
   * This can be any additional information or metadata associated with the user's profile.
   * @param liveLikeCallback This parameter is an instance of the `LiveLikeCallback` interface. It is
   * used to handle the response from the server after updating the user's custom data. The
   * `LiveLikeCallback` interface typically has two methods: `onSuccess` and `onError`. The `onSuccess`
   * method is
   */
    fun updateUserCustomData(
        customData: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeProfile>
    )


    fun cleanup()

    companion object {

        fun getInstance(
            configurationOnce: Once<SdkConfiguration>,
            uiScope: CoroutineScope,
            sdkScope: CoroutineScope,
            networkApiClient: NetworkApiClient,
            accessTokenDelegate: AccessTokenDelegate?,
        ): LiveLikeProfileClient = InternalLiveLikeProfileClientImpl(
            configurationOnce,
            sdkScope,
            uiScope,
            networkApiClient,
            accessTokenDelegate
        )
    }

}

@Deprecated("Use LiveLikeProfileClient", replaceWith = ReplaceWith("com.livelike.common.clients.LiveLikeProfileClient"))
typealias LiveLikeUserClient = LiveLikeProfileClient