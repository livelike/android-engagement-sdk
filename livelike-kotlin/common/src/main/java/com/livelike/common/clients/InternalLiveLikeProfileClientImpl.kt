package com.livelike.common.clients

import com.livelike.common.AccessTokenDelegate
import com.livelike.common.ENGAGEMENT_SDK_PROFILE_CREATION_ERROR
import com.livelike.common.TEMPLATE_BLOCKED_PROFILE_ID
import com.livelike.common.model.BlockedInfo
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.utils.toStream
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.Stream
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import com.livelike.utils.LiveLikeException
import com.livelike.utils.NO_MORE_DATA
import com.livelike.utils.Once
import com.livelike.utils.PaginationResponse
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.livelike.utils.suspendLazy
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

internal class InternalLiveLikeProfileClientImpl(
    sdkConfigurationOnce: Once<SdkConfiguration>,
    sdkScope: CoroutineScope,
    uiScope: CoroutineScope,
    private val networkApiClient: NetworkApiClient,
    private var accessTokenDelegate: AccessTokenDelegate?,
) : InternalLiveLikeProfileBaseClient(sdkConfigurationOnce, sdkScope, uiScope),
    LiveLikeProfileClient {

    private var blockedProfileResponse: PaginationResponse<BlockedInfo>? = null
    private var blockProfileIdsResponse: PaginationResponse<String>? = null
    private var currentProfilePic: String? = null


    override val currentProfileOnce: Once<LiveLikeProfile> = suspendLazy {
        val deferred = CompletableDeferred<LiveLikeProfile>()
        val accessToken = accessTokenDelegate?.getAccessToken()
        val configuration = sdkConfigurationOnce()
        logDebug { "Access Token: $accessToken" }

        if (accessToken.isNullOrEmpty()) {
            logError { ENGAGEMENT_SDK_PROFILE_CREATION_ERROR }
            safeCallBack({ result, error ->
                if (error != null) {
                    deferred.complete(LiveLikeProfile.empty())
                } else {
                    result?.let {
                        deferred.complete(it)
                    }
                }
            }) { _ ->
                networkApiClient.post(
                    configuration.profileUrl
                ).processResult<LiveLikeProfile>().also {
                    accessTokenDelegate?.storeAccessToken(it.accessToken)
                }.copy(profilePic = currentProfilePic)
            }
        } else {
            safeCallBack({ result, error ->
                if (error != null) {
                    deferred.complete(LiveLikeProfile.empty())
                } else {
                    result?.let {
                        deferred.complete(it)
                    }
                }
            }) { _ ->
                networkApiClient.get(
                    configuration.profileUrl,
                    accessToken = accessToken
                ).processResult<LiveLikeProfile>()
                    .copy(accessToken = accessToken, profilePic = currentProfilePic)
            }
        }
        deferred.await()
    }



    override val currentProfileFlow: StateFlow<LiveLikeProfile?> = this.currentProfileOnce.flow()


    override val profileStream: Stream<LiveLikeProfile> =
        this.currentProfileOnce.flow().toStream(uiScope)


    override fun blockProfile(
        profileId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<BlockedInfo>
    ) {
        safeCallBack(liveLikeCallback) {
            val profile = currentProfileOnce()
            networkApiClient.post(
                profile.blockProfileUrl,
                accessToken = profile.accessToken,
                body = mapOf(
                    "blocked_profile_id" to profileId
                ).toJsonString()
            ).processResult()
        }
    }


    override fun unBlockProfile(
        blockId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack(liveLikeCallback) {
            val profile = currentProfileOnce()
            networkApiClient.delete(
                "${profile.blockProfileUrl}$blockId/",
                accessToken = profile.accessToken
            ).processResult()
        }
    }

    override fun getBlockedProfileList(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<BlockedInfo>>
    ) {
        safeCallBack(liveLikeCallback) {
            val profile = currentProfileOnce()
            when (liveLikePagination) {
                LiveLikePagination.FIRST -> profile.blockProfileListTemplate.replace(
                    TEMPLATE_BLOCKED_PROFILE_ID, ""
                )

                LiveLikePagination.NEXT -> blockedProfileResponse?.next
                LiveLikePagination.PREVIOUS -> blockedProfileResponse?.previous
            }?.let { url ->
                networkApiClient.get(url, accessToken = profile.accessToken)
                    .processResult<PaginationResponse<BlockedInfo>>().also {
                        blockedProfileResponse = it
                    }.results
            } ?: throw LiveLikeException(NO_MORE_DATA)
        }
    }



    override fun getProfileBlockIds(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<String>>) {
        safeCallBack(liveLikeCallback) {
            val profile = currentProfileOnce()
            networkApiClient.get(
                profile.blockedProfileIdsUrl,
                accessToken = profile.accessToken
            ).processResult<PaginationResponse<String>>().also {
                blockProfileIdsResponse = it
            }.results
        }
    }



    override fun getProfileBlockInfo(
        profileId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<BlockedInfo>
    ) {
        safeCallBack(liveLikeCallback) {
            val profile = currentProfileOnce()
            val url = profile.blockProfileListTemplate.replace(
                TEMPLATE_BLOCKED_PROFILE_ID, profileId
            )
            networkApiClient.get(url, accessToken = profile.accessToken)
                .processResult<PaginationResponse<BlockedInfo>>().results.firstOrNull()
        }
    }


    override fun updateChatNickname(
        nickname: String,
        callback: com.livelike.common.LiveLikeCallback<LiveLikeProfile>
    ) {
        safeCallBack(callback) { pair ->
            logDebug { "Update chat NickName: $nickname" }
            val profile = currentProfileOnce()
            networkApiClient.patch(
                pair.profileUrl, body = mapOf(
                    "id" to profile.id, "nickname" to nickname
                ).toJsonString(), accessToken = profile.accessToken
            ).processResult<LiveLikeProfile>()
            currentProfileOnce(true)
        }
    }

    override fun updateChatUserPic(
        url: String?,
        callback: com.livelike.common.LiveLikeCallback<LiveLikeProfile>
    ) {
        safeCallBack(callback) {
            currentProfilePic = url
            currentProfileOnce(true)
        }
    }


    override fun getCurrentUserDetails(liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeProfile>) {
        safeCallBack(liveLikeCallback) {
            currentProfileOnce(true)
        }
    }


    override fun updateUserCustomData(
        customData: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeProfile>
    ) {
        safeCallBack(liveLikeCallback) { configuration ->
            val profile = currentProfileOnce()
            networkApiClient.patch(
                configuration.profileUrl, accessToken = profile.accessToken, body = mapOf(
                    "id" to profile.id, "custom_data" to customData
                ).toJsonString()
            ).processResult<LiveLikeProfile>()
            currentProfileOnce(true)
        }
    }



    override fun cleanup() {
        accessTokenDelegate = null
    }
}

internal open class InternalLiveLikeProfileBaseClient(
    private val sdkConfigurationOnce: Once<SdkConfiguration>,
    private val sdkScope: CoroutineScope,
    private val uiScope: CoroutineScope,
) {
    protected fun <T : Any> safeCallBack(
        liveLikeCallBack: com.livelike.common.LiveLikeCallback<T>? = null,
        call: suspend (SdkConfiguration) -> T?
    ) {
        sdkScope.launch {
            try {
                val config = sdkConfigurationOnce()
                val data = call(config)
                uiScope.launch {
                    liveLikeCallBack?.invoke(data, null)
                }
            } catch (e: Exception) {
                logError { e.message }
                e.printStackTrace()
                uiScope.launch {
                    liveLikeCallBack?.invoke(null, e.message)
                }
            }
        }
    }
}