package com.livelike.common

interface AccessTokenDelegate {
    /***
     * returns user access token
     */
    fun getAccessToken(): String?

    /***
     * store user access token
     * @param accessToken argument to store user access token
     */
    fun storeAccessToken(accessToken: String?)
}