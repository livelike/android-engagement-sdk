package com.livelike.common

import com.livelike.common.model.RequestType
import com.livelike.common.utils.safeCallBack
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import com.livelike.utils.LiveLikeException


interface ILiveLikeKotlin {

    /** The analytics services **/
    /** Changed analytics service from stream **/
    val analyticService: AnalyticsService


    fun close()
}

inline fun <B : Any, reified R : Any> LiveLikeKotlin.request(
    path: String?,
    url: String? = null,
    requestType: RequestType,
    queryParameters: List<Pair<String, Any?>> = emptyList(),
    body: B? = null,
    accessToken: String? = null,
    headers: List<Pair<String, String>> = emptyList(),
    noinline callback: LiveLikeCallback<R>
) {
    safeCallBack(sdkScope, callback) {
        val pathUrl =
            url ?: (baseUrl + (path ?: throw LiveLikeException("Either provide path or url")))
        when (requestType) {
            RequestType.GET -> networkClient.get(pathUrl, accessToken, queryParameters, headers)
            RequestType.POST -> networkClient.post(
                pathUrl,
                body?.toJsonString(),
                accessToken,
                headers
            )

            RequestType.PUT -> networkClient.put(
                pathUrl,
                body?.toJsonString(),
                accessToken,
                headers
            )

            RequestType.PATCH -> networkClient.patch(
                pathUrl,
                body?.toJsonString(),
                accessToken,
                headers
            )

            RequestType.DELETE -> networkClient.delete(pathUrl, accessToken, headers)
        }.processResult()
    }
}