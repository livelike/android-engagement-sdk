package com.livelike.common.model

import com.google.gson.annotations.SerializedName
import com.livelike.engagementsdk.publicapis.LiveLikeUserApi

data class BlockedInfo(
    val id: String,
    internal val url: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("blocked_profile")
    val blockedProfile: LiveLikeUserApi,
    @SerializedName("blocked_by_profile")
    val blockedByProfile: LiveLikeUserApi,
    @SerializedName("blocked_profile_id")
    val blockedProfileID: String,
    @SerializedName("blocked_by_profile_id")
    val blockedByProfileId: String
)