package com.livelike.common

import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.network.NetworkApiClient
import com.livelike.network.NetworkResult
import kotlinx.coroutines.CoroutineDispatcher
import java.io.InputStream


fun String.readFromFile(classLoader: ClassLoader): String? {
    return classLoader.getResourceAsStream(this)?.readAll()
}

fun String.getNetworkSuccessResultForFile(classLoader: ClassLoader): NetworkResult {
    return this.readFromFile(classLoader)?.let { NetworkResult.Success(it) }
        ?: NetworkResult.Error.UnknownError(
            Exception("Json file not found: $this")
        )
}


fun String.getNetworkErrorResultForFile(classLoader: ClassLoader): NetworkResult {
    return this.readFromFile(classLoader)?.let { NetworkResult.Error.HttpError(404, it) }
        ?: NetworkResult.Error.UnknownError(
            Exception("Json file not found: $this")
        )
}

fun InputStream.readAll(): String {
    val bufferedReader = this.bufferedReader()
    val content = StringBuilder()
    bufferedReader.use { br ->
        var line = br.readLine()
        while (line != null) {
            content.append(line)
            line = br.readLine()
        }
    }
    return content.toString()
}


fun initSDK(
    client: NetworkApiClient,
    dispatcher: CoroutineDispatcher,
    errorDelegate: ErrorDelegate? = null,
    accessTokenDelegate: AccessTokenDelegate = object : AccessTokenDelegate {
        override fun getAccessToken(): String? {
            return null
        }

        override fun storeAccessToken(accessToken: String?) {

        }

    }
): LiveLikeKotlin {
    return LiveLikeKotlin(
        "CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4",
        originURL = "http://localhost",
        dataStoreDelegate = DataStoreDelegateKotlinImpl(),
        networkClient = client,
        errorDelegate = errorDelegate,
        accessTokenDelegate = accessTokenDelegate,
        networkDispatcher = dispatcher,
        sdkDispatcher = dispatcher,
//        uiDispatcher = dispatcher
    )
}
