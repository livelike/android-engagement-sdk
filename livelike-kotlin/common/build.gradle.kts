@Suppress("DSL_SCOPE_VIOLATION")

plugins {
    `java-library`
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.detekt)
    `maven-publish`
//    alias(libs.plugins.dokka)
    `java-test-fixtures`
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}


publishing {
    publications {
        // creating a release publication
        create<MavenPublication>("maven") {
            groupId = "com.livelike.android-engagement-sdk"
            artifactId = "livelike-kotlin-common"
            version = "0.0.1"

            from(components["java"])
        }
    }
}


dependencies {
    implementation(project(":livelike-core:utils"))
    implementation(project(":livelike-core:network"))
    implementation(project(":livelike-core:serialization"))

    //coroutines
    implementation(libs.coroutines.core)
    //unit test
    testFixturesApi(libs.mockk)
    testFixturesApi(libs.coroutines.test)
    testFixturesApi(libs.junit)
    testFixturesApi(libs.turbine)

    testFixturesApi(project(":livelike-core:utils"))
    testFixturesApi(project(":livelike-core:network"))
    testFixturesApi(project(":livelike-core:serialization"))
}