package com.livelike.end2end

import com.livelike.comment.LiveLikeCommentBoardClient
import com.livelike.comment.commentBoard
import com.livelike.comment.models.CreateCommentBanRequestOptions
import com.livelike.comment.models.CreateCommentBoardRequestOptions
import com.livelike.comment.models.DeleteCommentBanRequestOption
import com.livelike.comment.models.GetBanDetailsRequestOptions
import com.livelike.comment.models.ListCommentBoardBanRequestOptions
import com.livelike.comment.models.UpdateCommentBoardRequestOptions
import com.livelike.common.AccessTokenDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.common.profile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.utils.LiveLikeException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.UUID
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class LiveLikeCommentE2ETest {
    private lateinit var commentBoardClient: LiveLikeCommentBoardClient
    private lateinit var liveLikeKotlin: LiveLikeKotlin
    private val testDispatcher = StandardTestDispatcher()
    private lateinit var clientId: String
    private lateinit var producerToken: String

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        producerToken = System.getenv("producerToken")
        clientId = System.getenv("clientId")
        liveLikeKotlin = LiveLikeKotlin(
            clientId,
            accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return producerToken
                }

                override fun storeAccessToken(accessToken: String?) {

                }

            })
        commentBoardClient = liveLikeKotlin.commentBoard()
    }

    @After
    fun tearDown() {
        testDispatcher.cancel()
    }

    @Test
    fun testCommentBoardClient() = runTest {
       // val profile = liveLikeKotlin.profile().currentProfileOnce()
        suspendCoroutine { cont ->
            commentBoardClient.getCommentBoards(LiveLikePagination.FIRST) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        val commentBoard = suspendCoroutine { cont ->
            commentBoardClient.createCommentBoard(
                CreateCommentBoardRequestOptions(
                    allowComments = true,
                    replyDepth = 2,
                    title = "Test Comment Board",
                    customData = null,
                    description = null,
                    customId = "test_comment_board_${UUID.randomUUID()}"
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(commentBoard.allowComments)
        assert(commentBoard.repliesDepth == 2)
        assert(commentBoard.title == "Test Comment Board")

        val customBoardId = "test_comment_board_update_${UUID.randomUUID()}"
        val updateCommentBoard = suspendCoroutine { cont ->
            commentBoardClient.updateCommentBoard(
                UpdateCommentBoardRequestOptions(
                    commentBoardId = commentBoard.id,
                    title = "Test Comment Board Updated",
                    customId = customBoardId,
                    allowComments = false,
                    customData = "custom data",
                    replyDepth = 3
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(updateCommentBoard.repliesDepth == 3)
        assert(updateCommentBoard.title == "Test Comment Board Updated")
        assert(updateCommentBoard.customId == customBoardId)
        assert(updateCommentBoard.customData == "custom data")
        assert(!updateCommentBoard.allowComments)

        val commentBoardBan = suspendCoroutine { cont ->
            CoroutineScope(Dispatchers.IO).launch {
                try {
                    val profileId = liveLikeKotlin.profile().currentProfileOnce().id
                    commentBoardClient.createCommentBoardBan(
                        CreateCommentBanRequestOptions(
                            profileId,
                            commentBoardId = updateCommentBoard.id,
                            description = "Ban description"
                        )
                    ) { result, error ->
                        result?.let { cont.resume(it) }
                        error?.let { cont.resumeWithException(LiveLikeException(it + "commentBoard:${updateCommentBoard.id}")) }
                    }
                } catch (e: Exception) {
                    cont.resumeWithException(e)
                }
            }
        }

        assert(commentBoardBan.commentBoardId == updateCommentBoard.id)
        assert(commentBoardBan.bannedById == liveLikeKotlin.profile().currentProfileOnce().id)
        assert(commentBoardBan.description == "Ban description")

        suspendCoroutine { cont ->
            commentBoardClient.getCommentBoardBan(GetBanDetailsRequestOptions(commentBoardBanId = commentBoardBan.id)) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        suspendCoroutine { cont ->
            CoroutineScope(Dispatchers.IO).launch {
                try{
                val profileId = liveLikeKotlin.profile().currentProfileOnce().id
                commentBoardClient.getCommentBoardBans(
                    ListCommentBoardBanRequestOptions(
                        profileId,
                        commentBoardId = updateCommentBoard.id
                    ), LiveLikePagination.FIRST
                ) { result, error ->
                    result?.let { cont.resume(it) }
                    error?.let { cont.resumeWithException(LiveLikeException(it)) }
                }
                } catch (e: Exception) {
                    cont.resumeWithException(e)
                }
            }

        }

        val isCommentBoardBanDeleted = suspendCoroutine { cont ->
            commentBoardClient.deleteCommentBoardBan(DeleteCommentBanRequestOption(commentBoardBan.id)) { result, error ->
                result?.let { cont.resume(true) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        assert(isCommentBoardBanDeleted)
    }

    //commented this, since the profile id used is banned from this comment board
   /* @Test
    fun testCommentClient() = runTest {
        val commentCustomId = "test_comment_board_${UUID.randomUUID()}"
        val commentBoard = suspendCoroutine { cont ->
            commentBoardClient.createCommentBoard(
                CreateCommentBoardRequestOptions(
                    commentCustomId,
                    title = "Test Comment Board",
                    allowComments = true,
                    replyDepth = 2,
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        println("Comment Board: ${commentBoard.id}")
        val commentClient = liveLikeKotlin.comment(commentBoard.id)
        val comment = suspendCoroutine { cont ->
            commentClient.addComment(AddCommentRequestOptions(text = "My Comment")) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(comment.text == "My Comment")

        val commentReply = suspendCoroutine { cont ->
            commentClient.addCommentReply(
                AddCommentReplyRequestOptions(
                    parentCommentId = comment.id,
                    text = "My Comment Reply"
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(commentReply.parentCommentId == comment.id)

       *//* val editedComment = suspendCoroutine { cont ->
            commentClient.editComment(
                UpdateCommentRequestOptions(
                    commentId = comment.id,
                    text = "Edited Comment"
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(editedComment.text == "Edited Comment")

        suspendCoroutine<Comment> { cont ->
            commentClient.getComment(GetCommentRequestOptions(comment.id)) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }*//*

        suspendCoroutine<List<Comment>> { cont ->
            commentClient.getComments(
                null,
                LiveLikePagination.FIRST
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        suspendCoroutine { cont ->
            commentClient.getCommentReplies(
                GetCommentRepliesRequestOptions(comment.id, null),
                LiveLikePagination.FIRST
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        val commentReport = suspendCoroutine { cont ->
            commentClient.createCommentReport(
                CreateCommentReportRequestOptions(
                    commentId = comment.id,
                    description = "report description"
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        assert(commentReport.description == "report description")
        assert(commentReport.commentId == comment.id)

        val commentReportDetail = suspendCoroutine { cont ->
            commentClient.getCommentReport(GetCommentReportRequestOptions(commentReportId = commentReport.id)) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        assert(commentReportDetail.id == commentReport.id)

        suspendCoroutine<List<CommentReport>> { cont ->
            commentClient.getCommentReports(
                GetCommentReportsRequestOptions(),
                LiveLikePagination.FIRST
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        suspendCoroutine<CommentReport> { cont ->
            commentClient.dismissCommentReport(DismissCommentReportRequestOptions(commentReportId = commentReport.id)) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it + "id:${commentReport.id}")) }
            }
        }

        *//*suspendCoroutine<DismissAllReports> { cont ->
            commentClient.dismissAllCommentReports(
                DismissAllReportsForACommentRequestOptions(
                    commentId = comment.id
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }*//*

        *//*suspendCoroutine<LiveLikeEmptyResponse> { cont ->
            commentClient.deleteCommentReport(DeleteCommentReportRequestOptions(commentReportId = commentReport.id)) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }*//*

        suspendCoroutine<LiveLikeEmptyResponse> { cont ->
            commentClient.deleteComment(DeleteCommentRequestOptions(commentId = comment.id)) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

    }*/
}