package com.livelike.comment

import com.livelike.comment.models.CreateCommentBanRequestOptions
import com.livelike.comment.models.CreateCommentBoardRequestOptions
import com.livelike.comment.models.DeleteCommentBanRequestOption
import com.livelike.comment.models.DeleteCommentBoardRequestOptions
import com.livelike.comment.models.GetBanDetailsRequestOptions
import com.livelike.comment.models.GetCommentBoardDetailRequestOptions
import com.livelike.comment.models.ListCommentBoardBanRequestOptions
import com.livelike.comment.models.UpdateCommentBoardRequestOptions
import com.livelike.common.AccessTokenDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.common.getNetworkSuccessResultForFile
import com.livelike.common.initSDK
import com.livelike.common.user
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.network.NetworkApiClient
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

@OptIn(ExperimentalCoroutinesApi::class)
class InternalLiveLikeCommentBoardClientImplTest {
    private val testDispatcher = StandardTestDispatcher()

    @MockK
    lateinit var client: NetworkApiClient

    @Before
    fun setUp() {
        // do the setup
        MockKAnnotations.init(this)
        Dispatchers.setMain(testDispatcher)
    }

    private suspend fun setUpSDK(): LiveLikeKotlin {
//        if (this::sdk.isInitialized) return sdk
        coEvery {
            client.get(
                "http://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4"
            )
        } returns "application_success.json".getNetworkSuccessResultForFile(
            javaClass.classLoader
        )
        coEvery {
            client.get(
                "https://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4/profile/",
                accessToken = any()
            )
        } returns "profile_fetch_success.json".getNetworkSuccessResultForFile(javaClass.classLoader)
        val sdk =
            initSDK(client, testDispatcher, accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJibGFzdHJ0IiwiaWQiOiJlZGFiMzNlZS0yODgwLTRkZWUtOTIxZC03ZjRkM2Q3YTQ1MDQiLCJjbGllbnRfaWQiOiJDZHNkTEtMM21TbGx5U3h5TzJValNSUkg0RXFGbVVGYmZTWFRaV1c0IiwiaWF0IjoxNjY5NzE2NjYxLCJhY2Nlc3NfdG9rZW4iOiIwNmMwNjMwZjJhY2UxYzliMmIyNWU5ZDk2NmRlOTcxZjUyODE1ODg0In0.OfxWOLCozRhL3aKzxwQMc2Fn7XmAcpuJhcdEReCwIBo"
                }

                override fun storeAccessToken(accessToken: String?) {

                }
            })
        sdk.user().currentProfileOnce()
        return sdk
    }

    @After
    fun tearDown() {
        testDispatcher.cancel()
    }

    @Test
    fun `test create comment board success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.post(
                "https://localhost/api/v1/comment-boards/", accessToken = any(), body = any()
            )
        } answers { "create_comment_board.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->

            sdk.commentBoard().createCommentBoard(CreateCommentBoardRequestOptions(
                "br231", "User_Test", true, 2, "abc", "hello demo"
            )) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.title == "User_Test")
    }

    @Test
    fun `test update comment board success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.patch(
                "https://localhost/api/v1/comment-boards/8cc7732f-a3cc-41a0-889c-49eb1f91d52e/",
                accessToken = any(),
                body = any()
            )
        } answers { "update_comment_board.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->

            sdk.commentBoard().updateCommentBoard(UpdateCommentBoardRequestOptions(
                "8cc7732f-a3cc-41a0-889c-49eb1f91d52e",
                "User_Test",
                "yup",
                true,
                3,
                "hello demo"
            )) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.title == "yup")
    }


    @Test
    fun `test get comment boards success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/comment-boards/",
                accessToken = any(),
                queryParameters = listOf(
                    "client_id" to "CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4"
                )
            )
        } answers { "get_comment_boards.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->

            sdk.commentBoard().getCommentBoards(LiveLikePagination.FIRST) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res[0].title == "User_Test")

    }

    @Test
    fun `test delete comment board success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.delete(
                "https://localhost/api/v1/comment-boards/8cc7732f-a3cc-41a0-889c-49eb1f91d52e/",
                accessToken = any()
            )
        } answers { "delete_comment_boards.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->

            sdk.commentBoard().deleteCommentBoards(DeleteCommentBoardRequestOptions("8cc7732f-a3cc-41a0-889c-49eb1f91d52e")) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.toString().contains("LiveLikeEmptyResponse"))
    }

    @Test
    fun `test get comment board details success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/comment-boards/8cc7732f-a3cc-41a0-889c-49eb1f91d52e/",
                accessToken = any()
            )
        } answers { "get_comment_board_detail.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.commentBoard().getCommentBoardDetails(GetCommentBoardDetailRequestOptions("8cc7732f-a3cc-41a0-889c-49eb1f91d52e")) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.title == "User_Test")

    }

    @Test
    fun `test create comment board ban success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.post(
                "https://localhost/api/v1/comment-board-bans/", accessToken = any(), body = any()
            )
        } answers { "create_comment_board_ban.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.commentBoard().createCommentBoardBan(CreateCommentBanRequestOptions(
                "br231", "User_Test", "hello demo"
            )) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.commentBoardId == "7fe49f7c-f3fe-47ac-b855-142713e3277d")

    }

    @Test
    fun `test get comment board bans success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/comment-board-bans/", accessToken = any(),
                queryParameters = listOf(
                    "comment_board_id" to "7fe49f7c-f3fe-47ac-b855-142713e3277d"
                )
            )
        } answers { "get_comment_board_bans.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.commentBoard().getCommentBoardBans(ListCommentBoardBanRequestOptions(
                commentBoardId = "7fe49f7c-f3fe-47ac-b855-142713e3277d"
            ), LiveLikePagination.FIRST) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res[0].commentBoardId == "7fe49f7c-f3fe-47ac-b855-142713e3277d")
        assert(res[0].clientId == "lom9db0XtQUhOZQq1vz8QPfSpiyyxppiUVGMcAje")
        assert(res[0].description == "ban test 2")
        assert(res[0].url == "https://cf-blast-dig.livelikecdn.com/api/v1/comment-board-bans/3ea4fd06-6088-4f89-ba2e-e26de54d3ca6/")
        assert(res[0].createdAt == "2023-03-10T09:54:43.230648Z")
        assert(res[0].id == "3ea4fd06-6088-4f89-ba2e-e26de54d3ca6")
        assert(res[0].bannedById == "888eafd0-699b-487b-99cd-3cde219d7808")

    }

    @Test
    fun `test get comment board ban success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/comment-board-bans/6f02c194-49aa-4d21-8de3-f433a6670808/",
                accessToken = any(),
            )
        } answers { "get_comment_board_ban.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.commentBoard().getCommentBoardBan(GetBanDetailsRequestOptions(
                commentBoardBanId = "6f02c194-49aa-4d21-8de3-f433a6670808"
            )) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.id == "6f02c194-49aa-4d21-8de3-f433a6670808")
        assert(res.commentBoardId == "7fe49f7c-f3fe-47ac-b855-142713e3277d")
        assert(res.clientId == "lom9db0XtQUhOZQq1vz8QPfSpiyyxppiUVGMcAje")
        assert(res.bannedById == "bcdfff51-a9ac-48fa-ac98-bd2fcc0836ba")
        assert(res.createdAt == "2023-03-10T10:34:40.136959Z")
        assert(res.url == "https://cf-blast-dig.livelikecdn.com/api/v1/comment-board-bans/6f02c194-49aa-4d21-8de3-f433a6670808/")
        assert(res.description == "sdk ban")

    }


    @Test
    fun `test delete comment board ban success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.delete(
                "https://localhost/api/v1/comment-board-bans/3ea4fd06-6088-4f89-ba2e-e26de54d3ca6/",
                accessToken = any(),
            )
        } answers { "delete_comment_board_ban.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->

            sdk.commentBoard().deleteCommentBoardBan(DeleteCommentBanRequestOption(
                "3ea4fd06-6088-4f89-ba2e-e26de54d3ca6"
            )) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(com.livelike.utils.LiveLikeException(it)) }
            }
        }
        advanceUntilIdle()
        assert(res.toString().contains("LiveLikeEmptyResponse"))

    }

    @Test
    fun `compare modal object to string with same value`() {
        var obj1 = ListCommentBoardBanRequestOptions(null, null)
        var obj2 = ListCommentBoardBanRequestOptions(null, null)
        assert(obj1.toString() == obj2.toString())
    }
}
