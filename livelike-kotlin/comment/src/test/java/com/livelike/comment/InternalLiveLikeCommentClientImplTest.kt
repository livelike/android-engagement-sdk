package com.livelike.comment

import com.livelike.comment.models.AddCommentReplyRequestOptions
import com.livelike.comment.models.AddCommentRequestOptions
import com.livelike.comment.models.CreateCommentReportRequestOptions
import com.livelike.comment.models.DeleteCommentReportRequestOptions
import com.livelike.comment.models.DeleteCommentRequestOptions
import com.livelike.comment.models.DismissAllReportsForACommentRequestOptions
import com.livelike.comment.models.DismissCommentReportRequestOptions
import com.livelike.comment.models.GetCommentReportRequestOptions
import com.livelike.comment.models.GetCommentReportsRequestOptions
import com.livelike.comment.models.GetCommentRequestOptions
import com.livelike.comment.models.ReportStatusOptions
import com.livelike.comment.models.UpdateCommentRequestOptions
import com.livelike.common.AccessTokenDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.common.getNetworkSuccessResultForFile
import com.livelike.common.initSDK
import com.livelike.common.user
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.network.NetworkApiClient
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


@OptIn(ExperimentalCoroutinesApi::class)
class InternalLiveLikeCommentClientImplTest {
    private val testDispatcher = StandardTestDispatcher()

    @MockK
    lateinit var client: NetworkApiClient

    @Before
    fun setUp() {
        // do the setup
        MockKAnnotations.init(this)
        Dispatchers.setMain(testDispatcher)
    }

    private suspend fun setUpSDK(): LiveLikeKotlin {
//        if (this::sdk.isInitialized) return sdk
        coEvery {
            client.get(
                "http://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4"
            )
        } returns "application_success.json".getNetworkSuccessResultForFile(
            javaClass.classLoader
        )
        coEvery {
            client.get(
                "https://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4/profile/",
                accessToken = any()
            )
        } returns "profile_fetch_success.json".getNetworkSuccessResultForFile(javaClass.classLoader)
        val sdk =
            initSDK(client, testDispatcher, accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String {
                    return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJibGFzdHJ0IiwiaWQiOiJlZGFiMzNlZS0yODgwLTRkZWUtOTIxZC03ZjRkM2Q3YTQ1MDQiLCJjbGllbnRfaWQiOiJDZHNkTEtMM21TbGx5U3h5TzJValNSUkg0RXFGbVVGYmZTWFRaV1c0IiwiaWF0IjoxNjY5NzE2NjYxLCJhY2Nlc3NfdG9rZW4iOiIwNmMwNjMwZjJhY2UxYzliMmIyNWU5ZDk2NmRlOTcxZjUyODE1ODg0In0.OfxWOLCozRhL3aKzxwQMc2Fn7XmAcpuJhcdEReCwIBo"
                }

                override fun storeAccessToken(accessToken: String?) {

                }
            })
        sdk.user().currentProfileOnce()
        return sdk
    }

    @After
    fun tearDown() {
        testDispatcher.cancel()
    }

    @Test
    fun `test create comment success`() = runTest {
        val sdk = setUpSDK()

        coEvery {
            client.get(
                "https://localhost/api/v1/comment-boards/7fe49f7c-f3fe-47ac-b855-142713e3277d/",
                accessToken = any()
            )
        } answers { "get_comment_board_detail.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()
        coEvery {
            client.post(
                "https://localhost/api/v1/comments/?comment_board_id=7fe49f7c-f3fe-47ac-b855-142713e3277d", accessToken = any(), body = any()
            )
        } answers { "create_comment.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.comment("7fe49f7c-f3fe-47ac-b855-142713e3277d")
                .addComment(AddCommentRequestOptions(
                    "y1dop", "User_Test"
                )){result , error ->
                    result?.let { cont.resume(it) }
                }
        }
        advanceUntilIdle()
        assert(res.text == "y1dop")

    }

//    @Test
//    fun `test get comments success`() = runTest {
//        val sdk = setUpSDK()
//
//        coEvery {
//            client.get(
//                "https://localhost/api/v1/comment-boards/7fe49f7c-f3fe-47ac-b855-142713e3277d/",
//                accessToken = any()
//            )
//        } answers { "get_comment_board_detail.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
//
//        advanceUntilIdle()
//        coEvery {
//            client.get(
//                "https://localhost/api/v1/comments/?comment_board_id=7fe49f7c-f3fe-47ac-b855-142713e3277d", accessToken = any(), listOf(
//                    "ordering" to CommentSortingOptions.OLDEST_REPLIES.serializedName,
//                    "replied_since" to "2020-09-01T13:16:33.114Z",
//                    "top_level" to "true"
//                )
//            )
//        } answers { "get_comments.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
//
//        advanceUntilIdle()
//
//        val res = suspendCoroutine { cont ->
//            sdk.comment("7fe49f7c-f3fe-47ac-b855-142713e3277d")
//                .getComments(GetCommentsRequestOptions(
//                    CommentSortingOptions.OLDEST_REPLIES,
//                    repliedSince = "2020-09-01T13:16:33.114Z",
//                    topLevel=true
//                ), LiveLikePagination.FIRST, object : LiveLikeCallback<List<Comment>>() {
//                    override fun onResponse(result: List<Comment>?, error: String?) {
//                        result?.let { cont.resume(it) }
//                    }
//
//
//                })
//        }
//        advanceUntilIdle()
//        assert(res[0].text == "y1dop")
//    }

    @Test
    fun `test get comment success`() = runTest {
        val sdk = setUpSDK()

        coEvery {
            client.get(
                "https://localhost/api/v1/comment-boards/7fe49f7c-f3fe-47ac-b855-142713e3277d/",
                accessToken = any()
            )
        } answers { "get_comment_board_detail.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()
        coEvery {
            client.get(
                "https://localhost/api/v1/comments/3437f488-5c45-468b-8a14-c9b67ee77337/",
                accessToken = any(),
            )
        } answers { "get_comment.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.comment("7fe49f7c-f3fe-47ac-b855-142713e3277d")
                .getComment(GetCommentRequestOptions(
                    "3437f488-5c45-468b-8a14-c9b67ee77337"
                )){
                    result, error ->
                    result?.let { cont.resume(it) }
                }
        }
        advanceUntilIdle()
        assert(res.id == "3437f488-5c45-468b-8a14-c9b67ee77337")
        assert(!res.isReported)
        assert(res.text == "test_comment")
        assert(res.isDeleted)
        assert(res.commentBoardId == "d849e096-d36d-406e-bff0-0627f13b19d4")
        assert(res.commentDepth == 0)
        assert(res.commentBoardId == "d849e096-d36d-406e-bff0-0627f13b19d4")
        assert(res.createdAt == "2022-12-09T13:06:39.379146Z")
        assert(res.customData == "{\"a\": \"b\"}")
        assert(res.deletedAt == "2023-01-10T12:14:53.420511Z")
        assert(res.deletedBy == "96819c56-063d-4fad-929a-79a0648122f4")
        assert(res.parentCommentId == null)
        assert(res.threadCommentId == "3437f488-5c45-468b-8a14-c9b67ee77337")
        assert(res.commentReportsCount == 0)
        assert(res.url == "https://cf-blast-dig.livelikecdn.com/api/v1/comments/3437f488-5c45-468b-8a14-c9b67ee77337/")
        assert(res.repliesUrl == "https://cf-blast-dig.livelikecdn.com/api/v1/comments/?comment_board_id=d849e096-d36d-406e-bff0-0627f13b19d4&parent_comment_id=3437f488-5c45-468b-8a14-c9b67ee77337")
        assert(res.dismissReportedCommentUrl == "https://cf-blast-dig.livelikecdn.com/api/v1/comments/3437f488-5c45-468b-8a14-c9b67ee77337/dismiss-reports/")
    }


    @Test
    fun `test edit comment success`() = runTest {
        val sdk = setUpSDK()

        coEvery {
            client.get(
                "https://localhost/api/v1/comment-boards/7fe49f7c-f3fe-47ac-b855-142713e3277d/",
                accessToken = any()
            )
        } answers { "get_comment_board_detail.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()
        coEvery {
            client.patch(
                "https://localhost/api/v1/comments/5ae9acf0-3bf2-40e4-b70a-1331a020300f/",
                accessToken = any(),
                body = any()
            )
        } answers { "edit_comments.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.comment("7fe49f7c-f3fe-47ac-b855-142713e3277d")
                .editComment(UpdateCommentRequestOptions(
                    "5ae9acf0-3bf2-40e4-b70a-1331a020300f"
                )){result, error ->
                    result?.let { cont.resume(it) }
                }
        }
        advanceUntilIdle()
        assert(res.text == "test_commentoo")

    }

    @Test
    fun `test delete comment success`() = runTest {
        val sdk = setUpSDK()

        coEvery {
            client.get(
                "https://localhost/api/v1/comment-boards/7fe49f7c-f3fe-47ac-b855-142713e3277d/",
                accessToken = any()
            )
        } answers { "get_comment_board_detail.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()
        coEvery {
            client.delete(
                "https://localhost/api/v1/comments/5ae9acf0-3bf2-40e4-b70a-1331a020300f/",
                accessToken = any()
            )
        } answers { "delete_comments.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.comment("7fe49f7c-f3fe-47ac-b855-142713e3277d")
                .deleteComment(DeleteCommentRequestOptions(
                    "5ae9acf0-3bf2-40e4-b70a-1331a020300f"
                )){result, error->
                    result?.let { cont.resume(it) }
                }
        }
        advanceUntilIdle()
        assert(res.toString().contains("LiveLikeEmptyResponse"))

    }

    @Test
    fun `test add comment reply success`() = runTest {
        val sdk = setUpSDK()

        coEvery {
            client.get(
                "https://localhost/api/v1/comment-boards/7fe49f7c-f3fe-47ac-b855-142713e3277d/",
                accessToken = any()
            )
        } answers { "get_comment_board_detail.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()
        coEvery {
            client.post(
                "https://localhost/api/v1/comments/?comment_board_id=7fe49f7c-f3fe-47ac-b855-142713e3277d", accessToken = any(), body = any()
            )
        } answers { "add_comment_reply.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()


        val res = suspendCoroutine { cont ->
            sdk.comment("7fe49f7c-f3fe-47ac-b855-142713e3277d")
                .addCommentReply(AddCommentReplyRequestOptions(
                    "4b1eb445-ee56-4c6a-a8ff-9c9aa0b152f2"
                )){ result,error->
                    result?.let { cont.resume(it) }
                }
        }
        advanceUntilIdle()
        assert(res.text == "y1dop")

    }

//    @Test
//    fun `test get comment reply success`() = runTest {
//        val sdk = setUpSDK()
//
//        coEvery {
//            client.get(
//                "https://localhost/api/v1/comment-boards/7fe49f7c-f3fe-47ac-b855-142713e3277d/",
//                accessToken = any()
//            )
//        } answers { "get_comment_board_detail.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
//
//        advanceUntilIdle()
//        coEvery {
//            client.get(
//                "https://localhost/api/v1/comments/4b1eb445-ee56-4c6a-a8ff-9c9aa0b152f2/",
//                accessToken = any()
//
//            )
//        } answers { "add_comment_reply.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
//
//        advanceUntilIdle()
//
//        coEvery {
//            client.get(
//                "https://cf-blast-dig.livelikecdn.com/api/v1/comments/?comment_board_id=7fe49f7c-f3fe-47ac-b855-142713e3277d&parent_comment_id=4b1eb445-ee56-4c6a-a8ff-9c9aa0b152f2",
//                accessToken = any(),
//                listOf(
//                    "ordering" to CommentSortingOptions.OLDEST.serializedName
//                )
//
//            )
//        } answers { "get_comment_reply.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
//
//        advanceUntilIdle()
//
//        val res = suspendCoroutine { cont ->
//            sdk.comment("7fe49f7c-f3fe-47ac-b855-142713e3277d")
//                .getCommentReplies(GetCommentRepliesRequestOptions(
//                    "4b1eb445-ee56-4c6a-a8ff-9c9aa0b152f2", CommentSortingOptions.OLDEST
//                ), LiveLikePagination.FIRST, object : LiveLikeCallback<List<Comment>>() {
//                    override fun onResponse(result: List<Comment>?, error: String?) {
//                        result?.let { cont.resume(it) }
//                    }
//
//
//                })
//        }
//        advanceUntilIdle()
//        assert(res[0].text == "y1dop")
//
//    }


//    @Test
//    fun `test get comment with ordering newest reply`() = runTest {
//        val sdk = setUpSDK()
//
//        coEvery {
//            client.get(
//                "https://localhost/api/v1/comment-boards/7fe49f7c-f3fe-47ac-b855-142713e3277d/",
//                accessToken = any()
//            )
//        } answers { "get_comment_board_detail.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
//
//        advanceUntilIdle()
//        coEvery {
//            client.get(
//                "https://localhost/api/v1/comments/?comment_board_id=7fe49f7c-f3fe-47ac-b855-142713e3277d", accessToken = any(), listOf(
//                    "ordering" to CommentSortingOptions.NEWEST_REPLIES.serializedName
//                )
//            )
//        } answers { "get_comments_ordering.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
//
//        advanceUntilIdle()
//
//        val res = suspendCoroutine { cont ->
//            sdk.comment("7fe49f7c-f3fe-47ac-b855-142713e3277d")
//                .getComments(GetCommentsRequestOptions(
//                    CommentSortingOptions.NEWEST_REPLIES
//                ), LiveLikePagination.FIRST, object : LiveLikeCallback<List<Comment>>() {
//                    override fun onResponse(result: List<Comment>?, error: String?) {
//                        result?.let { cont.resume(it) }
//                    }
//
//
//                })
//        }
//        advanceUntilIdle()
//        assert(res[0].text == "6")
//    }

//    @Test
//    fun `test get comment with ordering oldest reply`() = runTest {
//        val sdk = setUpSDK()
//
//        coEvery {
//            client.get(
//                "https://localhost/api/v1/comment-boards/7fe49f7c-f3fe-47ac-b855-142713e3277d/",
//                accessToken = any()
//            )
//        } answers { "get_comment_board_detail.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
//
//        advanceUntilIdle()
//        coEvery {
//            client.get(
//                "https://localhost/api/v1/comments/?comment_board_id=7fe49f7c-f3fe-47ac-b855-142713e3277d", accessToken = any(), listOf(
//                    "ordering" to CommentSortingOptions.NEWEST_REPLIES.serializedName
//                )
//            )
//        } answers { "get_comments.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
//
//        advanceUntilIdle()
//        val res = suspendCoroutine { cont ->
//            sdk.comment("7fe49f7c-f3fe-47ac-b855-142713e3277d")
//                .getComments(GetCommentsRequestOptions(
//                    CommentSortingOptions.NEWEST_REPLIES
//                ), LiveLikePagination.FIRST, object : LiveLikeCallback<List<Comment>>() {
//                    override fun onResponse(result: List<Comment>?, error: String?) {
//                        result?.let { cont.resume(it) }
//                    }
//
//
//                })
//        }
//        advanceUntilIdle()
//        assert(res[0].text == "y1dop")
//
//    }

    @Test
    fun `test dismiss all comment reports success`() = runTest {
        val sdk = setUpSDK()

        coEvery {
            client.get(
                "https://localhost/api/v1/comment-boards/7fe49f7c-f3fe-47ac-b855-142713e3277d/",
                accessToken = any(),
            )
        } answers { "get_comment_board_detail.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()
        coEvery {
            client.patch(
                "https://localhost/api/v1/comments/77c1bc71-e91f-4434-a12e-01bfdced1351/dismiss-reports/",
                accessToken = any(),
                body = any()
            )
        } answers { "dismiss_all_comment_reports.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.comment("7fe49f7c-f3fe-47ac-b855-142713e3277d")
                .dismissAllCommentReports(DismissAllReportsForACommentRequestOptions(
                    "77c1bc71-e91f-4434-a12e-01bfdced1351"
                )){result, error ->
                    result?.let { cont.resume(it) }

                }
        }
        advanceUntilIdle()
        assert(res.detail == "All reports for this comment will be dismissed")
    }

    @Test
    fun `test dismiss comment reports success`() = runTest {
        val sdk = setUpSDK()

        coEvery {
            client.get(
                "https://localhost/api/v1/comment-boards/7fe49f7c-f3fe-47ac-b855-142713e3277d/",
                accessToken = any(),
            )
        } answers { "get_comment_board_detail.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()
        coEvery {
            client.patch(
                "https://localhost/api/v1/comment-reports/ffd8ce13-8db4-4183-93ff-78acc1d9ee66/",
                accessToken = any(),
                body = any()
            )
        } answers { "dismiss_comment_reports.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.comment("7fe49f7c-f3fe-47ac-b855-142713e3277d")
                .dismissCommentReport(DismissCommentReportRequestOptions(
                    "ffd8ce13-8db4-4183-93ff-78acc1d9ee66"
                )){result, error->
                    result?.let { cont.resume(it) }
                }
        }
        advanceUntilIdle()
        assert(res.id == "ffd8ce13-8db4-4183-93ff-78acc1d9ee66")
        assert(res.commentBoardId == "684b3cbc-4004-4aa4-b20e-46b1b3f0b998")
        assert(res.url == "https://cf-blast-dig.livelikecdn.com/api/v1/comment-reports/ffd8ce13-8db4-4183-93ff-78acc1d9ee66/")
        assert(res.commentId == "77c1bc71-e91f-4434-a12e-01bfdced1351")
        assert(res.description == null)
        assert(res.reportStatus == "dismissed")
        assert(res.reportedAt == "2023-04-12T08:01:22.580927Z")
        assert(res.reportedById == "31157c97-4275-4c71-8595-919ef3d5349f")
        assert(res.comment?.id == "77c1bc71-e91f-4434-a12e-01bfdced1351")
        assert(res.comment?.isReported == true)
        assert(res.comment?.isDeleted == false)
    }

    @Test
    fun `test get comment reports success`() = runTest {
        val sdk = setUpSDK()

        coEvery {
            client.get(
                "https://localhost/api/v1/comment-boards/7fe49f7c-f3fe-47ac-b855-142713e3277d/",
                accessToken = any(),
            )
        } answers { "get_comment_board_detail.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        coEvery {
            client.get(
                "https://localhost/api/v1/comment-reports/",
                accessToken = any(),
                queryParameters = any()
            )
        } answers { "get_comment_reports.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.comment("7fe49f7c-f3fe-47ac-b855-142713e3277d").getCommentReports(
                GetCommentReportsRequestOptions(
                    ReportStatusOptions.PENDING,
                    "7fe49f7c-f3fe-47ac-b855-142713e3277d",
                    "77c1bc71-e91f-4434-a12e-01bfdced1351"
                ),
                LiveLikePagination.FIRST){result, error->
                result?.let { cont.resume(it) }
            }
        }
        advanceUntilIdle()
        assert(res[0].id == "b78fafaf-15e2-4c03-92b7-be0cee551b10")
        assert(res[0].commentBoardId == "7fe49f7c-f3fe-47ac-b855-142713e3277d")
        assert(res[0].url == "https://cf-blast-dig.livelikecdn.com/api/v1/comment-reports/b78fafaf-15e2-4c03-92b7-be0cee551b10/")
        assert(res[0].commentId == "77c1bc71-e91f-4434-a12e-01bfdced1351")
        assert(res[0].description == null)
        assert(res[0].reportStatus == "pending")
        assert(res[0].reportedAt == "2023-04-12T07:22:16.125145Z")
        assert(res[0].reportedById == "6ab2de7d-138b-465a-abe5-6f4497c1607c")
        assert(res[0].comment?.isReported == true)
    }

    @Test
    fun `test get comment report success`() = runTest {
        val sdk = setUpSDK()

        coEvery {
            client.get(
                "https://localhost/api/v1/comment-boards/7fe49f7c-f3fe-47ac-b855-142713e3277d/",
                accessToken = any(),
            )
        } answers { "get_comment_board_detail.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        coEvery {
            client.get(
                "https://localhost/api/v1/comment-reports/ffd8ce13-8db4-4183-93ff-78acc1d9ee66/",
                accessToken = any(),
                queryParameters = any()
            )
        } answers { "get_comment_report.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.comment("7fe49f7c-f3fe-47ac-b855-142713e3277d").getCommentReport(
                GetCommentReportRequestOptions(
                    "ffd8ce13-8db4-4183-93ff-78acc1d9ee66"
                )){
                  result, error->
                result?.let { cont.resume(it) }

            }
        }
        advanceUntilIdle()
        assert(res.id == "ffd8ce13-8db4-4183-93ff-78acc1d9ee66")
        assert(res.commentBoardId == "684b3cbc-4004-4aa4-b20e-46b1b3f0b998")
        assert(res.url == "https://cf-blast-dig.livelikecdn.com/api/v1/comment-reports/ffd8ce13-8db4-4183-93ff-78acc1d9ee66/")
        assert(res.commentId == "77c1bc71-e91f-4434-a12e-01bfdced1351")
        assert(res.description == null)
        assert(res.reportStatus == "dismissed")
        assert(res.reportedAt == "2023-04-12T08:01:22.580927Z")
        assert(res.reportedById == "31157c97-4275-4c71-8595-919ef3d5349f")
        assert(res.comment?.isReported == true)
    }

    @Test
    fun `test create comment report success`() = runTest {
        val sdk = setUpSDK()

        coEvery {
            client.get(
                "https://localhost/api/v1/comment-boards/7fe49f7c-f3fe-47ac-b855-142713e3277d/",
                accessToken = any(),
            )
        } answers { "get_comment_board_detail.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()
        coEvery {
            client.post(
                "https://localhost/api/v1/comment-reports/", accessToken = any(), body = any()
            )
        } answers { "create_comment_report.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.comment("7fe49f7c-f3fe-47ac-b855-142713e3277d")
                .createCommentReport(CreateCommentReportRequestOptions(
                    "87c72282-88c5-411c-a1fb-a3880ab86710"
                )){result, error ->
                    result?.let { cont.resume(it) }

                }
        }
        advanceUntilIdle()
        assert(res.id == "3f176ed5-3fdc-470a-964d-e39f864e648a")
        assert(res.comment?.isReported == true)
        assert(res.reportedById == "31157c97-4275-4c71-8595-919ef3d5349f")
        assert(res.reportedAt == "2023-04-13T12:24:39.843842Z")
        assert(res.commentId == "87c72282-88c5-411c-a1fb-a3880ab86710")
        assert(res.reportStatus == "pending")
        assert(res.description == "testing1")
        assert(res.url == "https://cf-blast-dig.livelikecdn.com/api/v1/comment-reports/3f176ed5-3fdc-470a-964d-e39f864e648a/")
        assert(res.commentBoardId == "49af9014-7b1a-41c2-8348-5b791a3f54e1")
    }

    @Test
    fun `test delete comment report success`() = runTest {
        val sdk = setUpSDK()

        coEvery {
            client.get(
                "https://localhost/api/v1/comment-boards/7fe49f7c-f3fe-47ac-b855-142713e3277d/",
                accessToken = any(),
            )
        } answers { "get_comment_board_detail.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()
        coEvery {
            client.delete(
                "https://localhost/api/v1/comment-reports/87c72282-88c5-411c-a1fb-a3880ab86710/",
                accessToken = any(),
            )
        } answers { "delete_comment_report.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.comment("7fe49f7c-f3fe-47ac-b855-142713e3277d")
                .deleteCommentReport(DeleteCommentReportRequestOptions(
                    "87c72282-88c5-411c-a1fb-a3880ab86710"
                )){result, error->
                    result?.let { cont.resume(it) }

                }
        }
        advanceUntilIdle()
        assert(res.toString().contains("LiveLikeEmptyResponse"))

    }
}