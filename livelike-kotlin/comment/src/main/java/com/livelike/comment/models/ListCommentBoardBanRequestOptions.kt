package com.livelike.comment.models

import com.google.gson.annotations.SerializedName

data class ListCommentBoardBanRequestOptions(
    @SerializedName("profile_id")
    val profileId: String?=null,
    @SerializedName("comment_board_id")
    val commentBoardId: String? = null
)
