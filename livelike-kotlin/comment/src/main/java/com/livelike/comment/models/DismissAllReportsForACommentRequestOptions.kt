package com.livelike.comment.models

data class DismissAllReportsForACommentRequestOptions(
    val commentId: String
)