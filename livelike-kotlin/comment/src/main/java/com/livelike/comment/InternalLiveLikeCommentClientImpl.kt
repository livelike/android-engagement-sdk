package com.livelike.comment

import com.livelike.BaseClient
import com.livelike.comment.models.AddCommentReplyRequestOptions
import com.livelike.comment.models.AddCommentRequestOptions
import com.livelike.comment.models.Comment
import com.livelike.comment.models.CommentReport
import com.livelike.comment.models.CreateCommentReportRequestOptions
import com.livelike.comment.models.DeleteCommentReportRequestOptions
import com.livelike.comment.models.DeleteCommentRequestOptions
import com.livelike.comment.models.DismissAllReports
import com.livelike.comment.models.DismissAllReportsForACommentRequestOptions
import com.livelike.comment.models.DismissCommentReportRequestOptions
import com.livelike.comment.models.GetCommentBoardDetailRequestOptions
import com.livelike.comment.models.GetCommentRepliesRequestOptions
import com.livelike.comment.models.GetCommentReportRequestOptions
import com.livelike.comment.models.GetCommentReportsRequestOptions
import com.livelike.comment.models.GetCommentRequestOptions
import com.livelike.comment.models.GetCommentsRequestOptions
import com.livelike.comment.models.UpdateCommentRequestOptions
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import com.livelike.utils.NO_MORE_DATA
import com.livelike.utils.Once
import com.livelike.utils.PaginationResponse
import com.livelike.utils.logError
import com.livelike.utils.suspendLazy
import kotlinx.coroutines.CoroutineScope
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

/**
 * To access comment APIs
 */
internal class InternalLiveLikeCommentClientImpl(
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    sdkScope: CoroutineScope,
    uiScope: CoroutineScope,
    private val networkApiClient: NetworkApiClient,
    private val commentClient: LiveLikeCommentBoardClient,
    private val commentBoardId: String
) : BaseClient(configurationOnce, currentProfileOnce, sdkScope, uiScope),
    LiveLikeCommentClient {

    private val commentBoardDetailOnce = suspendLazy {
        val commentBoard = suspendCoroutine { cont ->

            commentClient.getCommentBoardDetails(GetCommentBoardDetailRequestOptions(commentBoardId),
                ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let {
                    logError { it }
                    cont.resumeWithException(Exception(it))
                }
            }
        }
        commentBoard
    }
    private var commentsPaginatedResponseReport: MutableMap<String, PaginationResponse<CommentReport>> =
        mutableMapOf()
    private var commentPaginatedResponse: MutableMap<String, PaginationResponse<Comment>> =
        mutableMapOf()
    private var commentListPaginatedResponse: MutableMap<String, PaginationResponse<Comment>> =
        mutableMapOf()
    private var commentRepliesPaginatedResponse: MutableMap<String, PaginationResponse<Comment>> =
        mutableMapOf()

    override fun addComment(
        addCommentRequestOptions: AddCommentRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<Comment>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val board = commentBoardDetailOnce()
            board.commentsUrl.let<String, Comment> { url ->
                networkApiClient.post(
                    url, addCommentRequestOptions.apply {
                        commentBoardId =
                            board.id
                    }.toJsonString(), pair.first.accessToken
                ).processResult()
            }.run {
                copy(
                    isFromMe = author.id == pair.first.id,
                    isDeletedByMe = deletedBy == pair.first.id
                )
            }
        }
    }





    override fun getComments(
        getCommentsRequestOptions: GetCommentsRequestOptions?,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<Comment>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val result = commentPaginatedResponse[getCommentsRequestOptions.toString()]
            val board = commentBoardDetailOnce()
            if (result == null || liveLikePagination == LiveLikePagination.FIRST) {
                board.commentsUrl
            } else {
                when (liveLikePagination) {
                    LiveLikePagination.NEXT -> result.next
                    LiveLikePagination.PREVIOUS -> result.previous
                    else -> null
                }
            }?.let { url ->
                val queryParams = arrayListOf<Pair<String, String?>>()
                // queryParams.add(COMMENT_BOARD_ID to board.id)
                getCommentsRequestOptions?.let {
                    it.sorting?.let { sorting ->
                        queryParams.add(ORDERING to sorting.serializedName)
                    }
                    it.repliedSince?.let { repliedSince ->
                        queryParams.add(REPLIED_SINCE to repliedSince)
                    }
                    it.repliedUntil?.let { repliedUntil ->
                        queryParams.add(REPLIED_UNTIL to repliedUntil)
                    }

                    it.since?.let { since ->
                        queryParams.add(SINCE to since)
                    }
                    it.until?.let { until ->
                        queryParams.add(UNTIL to until)
                    }

                    it.isReported?.let { isReported ->
                        queryParams.add(IS_REPORTED to isReported.toString())
                    }

                    it.topLevel?.let { topLevel ->
                        queryParams.add(TOP_LEVEL to topLevel.toString())
                    }

                    it.withoutDeletedThread?.let { withoutDeletedThread ->
                        queryParams.add(WITHOUT_DELETED_THREAD to withoutDeletedThread.toString())
                    }
                    it.reactionId?.let{ reactionId ->
                        queryParams.add(REACTION_ID to reactionId)
                    }

                }
                networkApiClient.get(
                    url, accessToken = pair.first.accessToken, queryParams
                ).processResult<PaginationResponse<Comment>>().also {
                    commentPaginatedResponse[getCommentsRequestOptions.toString()] =
                        it
                }.results.map {
                    it.copy(
                        isFromMe = it.author.id == pair.first.id,
                        isDeletedByMe = it.deletedBy == pair.first.id
                    )
                }
            } ?: throw Exception(NO_MORE_DATA)
        }
    }



    override fun getCommentsPage(
        getCommentsRequestOptions: GetCommentsRequestOptions?,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<PaginationResponse<Comment>>
    ){
        safeCallBack(liveLikeCallback) { pair ->
            val result = commentListPaginatedResponse[getCommentsRequestOptions.toString()]
            val board = commentBoardDetailOnce()
            val url = if (result == null || liveLikePagination == LiveLikePagination.FIRST) {
                board.commentsUrl
            } else {
                when (liveLikePagination) {
                    LiveLikePagination.NEXT -> result.next
                    LiveLikePagination.PREVIOUS -> result.previous
                    else -> null
                }
            }

            url?.let { url ->
                val queryParams = arrayListOf<Pair<String, String?>>()
                getCommentsRequestOptions?.let {
                    it.sorting?.let { sorting ->
                        queryParams.add(ORDERING to sorting.serializedName)
                    }
                    it.repliedSince?.let { repliedSince ->
                        queryParams.add(REPLIED_SINCE to repliedSince)
                    }
                    it.repliedUntil?.let { repliedUntil ->
                        queryParams.add(REPLIED_UNTIL to repliedUntil)
                    }
                    it.since?.let { since ->
                        queryParams.add(SINCE to since)
                    }
                    it.until?.let { until ->
                        queryParams.add(UNTIL to until)
                    }
                    it.isReported?.let { isReported ->
                        queryParams.add(IS_REPORTED to isReported.toString())
                    }
                    it.topLevel?.let { topLevel ->
                        queryParams.add(TOP_LEVEL to topLevel.toString())
                    }
                    it.withoutDeletedThread?.let { withoutDeletedThread ->
                        queryParams.add(WITHOUT_DELETED_THREAD to withoutDeletedThread.toString())
                    }
                    it.reactionId?.let{ reactionId ->
                        queryParams.add(REACTION_ID to reactionId)
                    }
                }

                networkApiClient.get(
                    url, accessToken = pair.first.accessToken, queryParams
                ).processResult<PaginationResponse<Comment>>().also {
                    commentListPaginatedResponse[getCommentsRequestOptions.toString()] = it
                }.let { paginatedResponse ->
                    // Update each comment with additional information
                    paginatedResponse.results.map {
                        it.copy(
                            isFromMe = it.author.id == pair.first.id,
                            isDeletedByMe = it.deletedBy == pair.first.id
                        )
                    }
                    paginatedResponse
                }
            } ?: throw Exception(NO_MORE_DATA)
        }
    }

    override fun getCommentsCount(
        getCommentsRequestOptions: GetCommentsRequestOptions?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<Int>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val board = commentBoardDetailOnce()
            board.commentsUrl.let { url ->
                val queryParams = arrayListOf<Pair<String, String?>>()
                //queryParams.add(COMMENT_BOARD_ID to board.id)
                getCommentsRequestOptions?.let {
                    it.sorting?.let { sorting ->
                        queryParams.add(ORDERING to sorting.serializedName)
                    }
                    it.repliedSince?.let { repliedSince ->
                        queryParams.add(REPLIED_SINCE to repliedSince)
                    }
                    it.repliedUntil?.let { repliedUntil ->
                        queryParams.add(REPLIED_UNTIL to repliedUntil)
                    }

                    it.since?.let { since ->
                        queryParams.add(SINCE to since)
                    }
                    it.until?.let { until ->
                        queryParams.add(UNTIL to until)
                    }

                    it.isReported?.let { isReported ->
                        queryParams.add(IS_REPORTED to isReported.toString())
                    }
                    it.topLevel?.let { topLevel ->
                        queryParams.add(TOP_LEVEL to topLevel.toString())
                    }
                    it.withoutDeletedThread?.let { withoutDeletedThread ->
                        queryParams.add(WITHOUT_DELETED_THREAD to withoutDeletedThread.toString())
                    }

                    it.topLevel?.let { topLevel ->
                        if (topLevel) {
                            queryParams.add(TOP_LEVEL to "true")
                        } else queryParams.add(TOP_LEVEL to "false")
                    }

                    it.withoutDeletedThread?.let { withoutDeletedThread ->
                        if (withoutDeletedThread) {
                            queryParams.add(WITHOUT_DELETED_THREAD to "true")
                        } else queryParams.add(WITHOUT_DELETED_THREAD to "false")
                    }
                    it.reactionId?.let{ reactionId ->
                        queryParams.add(REACTION_ID to reactionId)
                    }
                }
                networkApiClient.get(
                    url, accessToken = pair.first.accessToken, queryParams
                ).processResult<PaginationResponse<Comment>>().count
            }
        }
    }


    override fun editComment(
        updateCommentRequestOptions: UpdateCommentRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<Comment>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val board = commentBoardDetailOnce()
            board.commentDetailUrlTemplate.replace(
                COMMENT_TEMPLATE, updateCommentRequestOptions.commentId
            ).let { url ->
                networkApiClient.patch(
                    url,
                    updateCommentRequestOptions.toJsonString(),
                    pair.first.accessToken
                ).processResult()
            }
        }
    }

    override fun deleteComment(
        deleteCommentRequestOptions: DeleteCommentRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val board = commentBoardDetailOnce()
            board.commentDetailUrlTemplate.replace(
                COMMENT_TEMPLATE, deleteCommentRequestOptions.commentId
            ).let { url ->
                networkApiClient.delete(
                    url, accessToken = pair.first.accessToken
                ).processResult()
            }
        }
    }



    override fun addCommentReply(
        addCommentReplyRequestOptions: AddCommentReplyRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<Comment>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val board = commentBoardDetailOnce()
            board.commentsUrl.let { url ->
                networkApiClient.post(
                    url, addCommentReplyRequestOptions.apply {
                        commentBoardId =
                            board.id
                    }.toJsonString(), pair.first.accessToken
                ).processResult()
            }
        }
    }




    override fun getCommentReplies(
        getCommentRepliesRequestOptions: GetCommentRepliesRequestOptions,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<Comment>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val result = commentRepliesPaginatedResponse[getCommentRepliesRequestOptions.toString()]
            val board = commentBoardDetailOnce()
            if (result == null || liveLikePagination == LiveLikePagination.FIRST) {
                board.commentDetailUrlTemplate.replace(
                    COMMENT_TEMPLATE, getCommentRepliesRequestOptions.commentId
                ).let {
                    val comment =
                        networkApiClient.get(it, accessToken = pair.first.accessToken)
                            .processResult<Comment>()
                    comment.repliesUrl
                }
            } else {
                when (liveLikePagination) {
                    LiveLikePagination.NEXT -> result.next
                    LiveLikePagination.PREVIOUS -> result.previous
                    else -> null
                }
            }?.let { url ->

                val queryParams = mutableListOf(
                    ORDERING to getCommentRepliesRequestOptions.sorting?.serializedName,
                    WITHOUT_DELETED_THREAD to getCommentRepliesRequestOptions.withoutDeletedThread
                )

                // Add reaction_id to query params if it's not null
                getCommentRepliesRequestOptions.reactionId?.let { reactionId ->
                    queryParams.add(REACTION_ID to reactionId)
                }

                networkApiClient.get(
                    url, accessToken = pair.first.accessToken, queryParameters = queryParams
                ).processResult<PaginationResponse<Comment>>().also {
                    commentRepliesPaginatedResponse[getCommentRepliesRequestOptions.toString()] =
                        it
                }.results.map {
                    it.copy(
                        isFromMe = it.author.id == pair.first.id,
                        isDeletedByMe = it.deletedBy == pair.first.id
                    )
                }
            } ?: throw Exception(NO_MORE_DATA)
        }
    }

    override fun getCommentRepliesCount(
        getCommentRepliesRequestOptions: GetCommentRepliesRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<Int>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val board = commentBoardDetailOnce()
            board.commentDetailUrlTemplate.replace(
                COMMENT_TEMPLATE, getCommentRepliesRequestOptions.commentId
            ).let {
                val comment =
                    networkApiClient.get(it, accessToken = pair.first.accessToken)
                        .processResult<Comment>()
                comment.repliesUrl
            }.let { url ->
                val queryParams = mutableListOf(
                    ORDERING to getCommentRepliesRequestOptions.sorting?.serializedName,
                    WITHOUT_DELETED_THREAD to getCommentRepliesRequestOptions.withoutDeletedThread
                )

                // Add reaction_id to query params if it's not null
                getCommentRepliesRequestOptions.reactionId?.let { reactionId ->
                    queryParams.add(REACTION_ID to reactionId)
                }
                networkApiClient.get(
                    url, accessToken = pair.first.accessToken, queryParameters = queryParams
                ).processResult<PaginationResponse<Comment>>().count
            }
        }
    }


    override fun getCommentReports(
        getCommentReportsRequestOptions: GetCommentReportsRequestOptions,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<CommentReport>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val result = commentsPaginatedResponseReport[getCommentReportsRequestOptions.toString()]
            if (result == null || liveLikePagination == LiveLikePagination.FIRST) {
                pair.second.commentReportUrl
            } else {
                when (liveLikePagination) {
                    LiveLikePagination.NEXT -> result.next
                    LiveLikePagination.PREVIOUS -> result.previous
                    else -> null
                }
            }?.let { url ->
                val queryParams = arrayListOf<Pair<String, String?>>()
                queryParams.add(CLIENT_ID to pair.second.clientId)
                getCommentReportsRequestOptions.let {
                    it.commentBoardId?.let { commentBoardId ->
                        queryParams.add(COMMENT_BOARD_ID to commentBoardId)
                    }
                    it.commentId?.let { commentId ->
                        queryParams.add(COMMENT_ID to commentId)

                    }
                    it.reportStatus?.let { status ->
                        queryParams.add(REPORT_STATUS to status.serializedName)
                    }

                }
                networkApiClient.get(
                    url, accessToken = pair.first.accessToken, queryParams
                ).processResult<PaginationResponse<CommentReport>>().also {
                    commentsPaginatedResponseReport[getCommentReportsRequestOptions.toString()] =
                        it
                }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }



    override fun createCommentReport(
        createCommentReportRequestOptions: CreateCommentReportRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<CommentReport>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.commentReportUrl.let { url ->
                networkApiClient.post(
                    url,
                    createCommentReportRequestOptions.toJsonString(),
                    pair.first.accessToken
                ).processResult()
            }
        }
    }


    override fun deleteCommentReport(
        deleteCommentReportRequestOptions: DeleteCommentReportRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.commentReportDetailUrlTemplate.replace(
                COMMENT_REPORT_TEMPLATE,
                deleteCommentReportRequestOptions.commentReportId
            ).let { url ->
                networkApiClient.delete(
                    url, accessToken = pair.first.accessToken
                ).processResult()
            }
        }
    }


    override fun dismissAllCommentReports(
        dismissAllReportsForACommentRequestOptions: DismissAllReportsForACommentRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<DismissAllReports>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val board = commentBoardDetailOnce()
            board.dismissReportedCommentUrlTemplate.replace(
                COMMENT_TEMPLATE, dismissAllReportsForACommentRequestOptions.commentId
            ).let { url ->
                networkApiClient.patch(
                    url,
                    accessToken = pair.first.accessToken,
                    body = mapOf("report_status" to "dismissed").toJsonString()
                ).processResult()
            }
        }
    }


    override fun dismissCommentReport(
        dismissCommentReportRequestOptions: DismissCommentReportRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<CommentReport>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.commentReportDetailUrlTemplate.replace(
                COMMENT_REPORT_TEMPLATE, dismissCommentReportRequestOptions.commentReportId
            ).let { url ->
                networkApiClient.patch(
                    url,
                    accessToken = pair.first.accessToken,
                    body = mapOf("report_status" to "dismissed").toJsonString()
                ).processResult()
            }
        }
    }

    override fun getComment(
        getCommentRequestOptions: GetCommentRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<Comment>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val board = commentBoardDetailOnce()
            board.commentDetailUrlTemplate.replace(
                COMMENT_TEMPLATE, getCommentRequestOptions.commentId
            ).let {
                networkApiClient.get(it, accessToken = pair.first.accessToken)
                    .processResult()
            }
        }
    }

    override fun getCommentReport(
        getCommentReportRequestOptions: GetCommentReportRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<CommentReport>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val board = commentBoardDetailOnce()
            board.commentReportDetailUrlTemplate.replace(
                COMMENT_REPORT_TEMPLATE, getCommentReportRequestOptions.commentReportId
            ).let {
                networkApiClient.get(it, accessToken = pair.first.accessToken)
                    .processResult()
            }
        }
    }

}