package com.livelike.comment.models

data class GetCommentRequestOptions(
    val commentId: String
)