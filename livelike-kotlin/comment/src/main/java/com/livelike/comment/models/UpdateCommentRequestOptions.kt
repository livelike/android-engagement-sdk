package com.livelike.comment.models

import com.google.gson.annotations.SerializedName

data class UpdateCommentRequestOptions(
    @Transient val commentId: String,
    @SerializedName("text")
    val text: String? = null,
    @SerializedName("custom_data")
    val customData: String? = null
)