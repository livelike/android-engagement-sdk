package com.livelike.comment.models

import com.google.gson.annotations.SerializedName

data class CommentBoardBanDetails(
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    val url: String,
    @SerializedName("comment_board_id")
    val commentBoardId: String? = null,
    @SerializedName("client_id")
    val clientId: String,
    @SerializedName("banned_by_id")
    val bannedById: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("description")
    val description: String? = null,
)
