package com.livelike.comment

const val COMMENT_BOARD_TEMPLATE = "{comment_board_id}"
const val COMMENT_TEMPLATE = "{comment_id}"
const val COMMENT_REPORT_TEMPLATE = "{comment_report_id}"
const val COMMENT_BOARD_BAN_TEMPLATE = "{comment_board_ban_id}"
const val COMMENT_BOARD_ID_NOT_FOUND = "Comment Board id not found"
const val COMMENT_DETAILS_NOT_FETCHED = "Unable to fetch comment details"
const val COMMENT_BOARD_ID = "comment_board_id"
const val ORDERING = "ordering"
const val REPLIED_SINCE = "replied_since"
const val REPLIED_UNTIL = "replied_until"
const val PROFILE_ID = "profile_id"
const val REPORT_STATUS = "report_status"
const val IS_REPORTED = "is_reported"
const val CLIENT_ID = "client_id"
const val COMMENT_ID="comment_id"
const val TOP_LEVEL="top_level"
const val WITHOUT_DELETED_THREAD="without_deleted_thread"
const val SINCE = "since"
const val UNTIL = "until"
const val EXCLUDE_BLOCKED_PROFILE_COMMENT = "exclude_blocked_profile_comment"
const val DETAILS_NOT_FOUND="Details not found"
const val FILTERED="filtered"
const val DELETED_BY_YOU="This comment was deleted by you!"
const val DELETED_BY_OTHERS="This comment was deleted!"
const val MAX_DEPTH_REACHED="Maximum Depth Reached"
const val REACHED_TOP_LEVEL="Reached Top Level for comments"
const val REACTION_ID = "reaction_id"

enum class ProfaneComment{
    FILTERED,
    MASK,
    CUSTOM
}


