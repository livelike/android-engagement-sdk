package com.livelike.comment

import com.livelike.comment.models.AddCommentReplyRequestOptions
import com.livelike.comment.models.AddCommentRequestOptions
import com.livelike.comment.models.Comment
import com.livelike.comment.models.CommentReport
import com.livelike.comment.models.CreateCommentReportRequestOptions
import com.livelike.comment.models.DeleteCommentReportRequestOptions
import com.livelike.comment.models.DeleteCommentRequestOptions
import com.livelike.comment.models.DismissAllReports
import com.livelike.comment.models.DismissAllReportsForACommentRequestOptions
import com.livelike.comment.models.DismissCommentReportRequestOptions
import com.livelike.comment.models.GetCommentRepliesRequestOptions
import com.livelike.comment.models.GetCommentReportRequestOptions
import com.livelike.comment.models.GetCommentReportsRequestOptions
import com.livelike.comment.models.GetCommentRequestOptions
import com.livelike.comment.models.GetCommentsRequestOptions
import com.livelike.comment.models.UpdateCommentRequestOptions
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.utils.PaginationResponse

/**
 * Interface for managing and interacting with comments and comment-related operations.
 */
interface LiveLikeCommentClient {

    /**
     * Adds a new comment.
     *
     * @param addCommentRequestOptions Arguments used to create the comment.
     * @param liveLikeCallback A callback to handle the result of adding the comment.
     */
    fun addComment(
        addCommentRequestOptions: AddCommentRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<Comment>
    )


    fun getComments(
        getCommentsRequestOptions: GetCommentsRequestOptions?,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<Comment>>
    )

    /**
     * Retrieves pagination response of comments with count, next and previous.
     *
     * @param getCommentsRequestOptions A configurable request object used to query for comments.
     * @param liveLikePagination a `Pagination` typed parameter passed to allow paginated responses.
     * @param liveLikeCallback A callback to handle the result of retrieving the list of comments.
     */

    fun getCommentsPage(
        getCommentsRequestOptions: GetCommentsRequestOptions?,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<PaginationResponse<Comment>>,
    )

    /**
     * Retrieves the count of comments based on the provided comment request options.
     *
     * @param getCommentsRequestOptions A configurable request object used to query for comments.
     * @param liveLikeCallback A callback to handle the result of retrieving the count of comments.
     */
    fun getCommentsCount(
        getCommentsRequestOptions: GetCommentsRequestOptions?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<Int>
    )

    /**
     * Updates an existing comment with the provided comment update request options.
     *
     * @param updateCommentRequestOptions Arguments used to update the comment.
     * @param liveLikeCallback A callback to handle the result of updating the comment.
     */
    fun editComment(
        updateCommentRequestOptions: UpdateCommentRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<Comment>
    )

    /**
     * Deletes a comment based on the provided comment delete request options.
     *
     * @param deleteCommentRequestOptions Arguments used to delete the comment.
     * @param liveLikeCallback A callback to handle the result of deleting the comment.
     */
    fun deleteComment(
        deleteCommentRequestOptions: DeleteCommentRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    )


    /**
     * Adds a reply to a comment using the provided comment reply request options.
     *
     * @param addCommentReplyRequestOptions Arguments used to add a reply to a comment.
     * @param liveLikeCallback A callback to handle the result of adding the reply.
     */
    fun addCommentReply(
        addCommentReplyRequestOptions: AddCommentReplyRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<Comment>
    )


    /**
     * Retrieves a list of replies for a comment based on the provided comment replies request options and pagination settings.
     *
     * @param getCommentRepliesRequestOptions Arguments used to get a list of replies for a comment.
     * @param liveLikePagination Pagination parameters to control the result set.
     * @param liveLikeCallback A callback to handle the result of retrieving the list of comment replies.
     */
    fun getCommentReplies(
        getCommentRepliesRequestOptions: GetCommentRepliesRequestOptions,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<Comment>>
    )

    /**
     * Retrieves a list of comment reports based on the provided comment reports request options and pagination settings.
     *
     * @param getCommentReportsRequestOptions Arguments used to get a list of comment reports.
     * @param liveLikePagination Pagination parameters to control the result set.
     * @param liveLikeCallback A callback to handle the result of retrieving the list of comment reports.
     */
    fun getCommentReports(
        getCommentReportsRequestOptions: GetCommentReportsRequestOptions,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<CommentReport>>
    )

    /**
     * Creates a comment report using the provided comment report request options.
     *
     * @param createCommentReportRequestOptions Arguments used to report a comment.
     * @param liveLikeCallback A callback to handle the result of creating the comment report.
     */
    fun createCommentReport(
        createCommentReportRequestOptions: CreateCommentReportRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<CommentReport>
    )

    /**
     * Deletes a comment report using the provided comment report delete request options.
     *
     * @param deleteCommentReportRequestOptions Arguments used to delete a comment report.
     * @param liveLikeCallback A callback to handle the result of deleting the comment report.
     */
    fun deleteCommentReport(
        deleteCommentReportRequestOptions: DeleteCommentReportRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    )

    /**
     * Dismisses all comment reports for a comment using the provided request options.
     *
     * @param dismissAllReportsForACommentRequestOptions Arguments used to dismiss all comment reports for a comment.
     * @param liveLikeCallback A callback to handle the result of dismissing all comment reports.
     */
    fun dismissAllCommentReports(
        dismissAllReportsForACommentRequestOptions: DismissAllReportsForACommentRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<DismissAllReports>
    )

    /**
     * Dismisses a specific comment report using the provided request options.
     *
     * @param dismissCommentReportRequestOptions Arguments used to dismiss a comment report.
     * @param liveLikeCallback A callback to handle the result of dismissing the comment report.
     */
    fun dismissCommentReport(
        dismissCommentReportRequestOptions: DismissCommentReportRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<CommentReport>
    )

    /**
     * Retrieves a specific comment by its unique identifier using the provided request options.
     *
     * @param getCommentRequestOptions Arguments used to get a comment by ID.
     * @param liveLikeCallback A callback to handle the result of retrieving the comment.
     */
    fun getComment(
        getCommentRequestOptions: GetCommentRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<Comment>
    )

    /**
     * Retrieves a specific comment report by its unique identifier using the provided request options.
     *
     * @param getCommentReportRequestOptions Arguments used to get a comment report by ID.
     * @param liveLikeCallback A callback to handle the result of retrieving the comment report.
     */
    fun getCommentReport(
        getCommentReportRequestOptions: GetCommentReportRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<CommentReport>
    )

    /**
     * Retrieves the count of comment replies for a specific comment by its unique identifier using the provided request options.
     *
     * @param getCommentRepliesRequestOptions Arguments used to get the count of comment replies for a comment.
     * @param liveLikeCallback A callback to handle the result of retrieving the count of comment replies.
     */
    fun getCommentRepliesCount(
        getCommentRepliesRequestOptions: GetCommentRepliesRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<Int>
    )

}