package com.livelike.comment

import com.livelike.BaseClient
import com.livelike.comment.models.CommentBoard
import com.livelike.comment.models.CommentBoardBanDetails
import com.livelike.comment.models.CreateCommentBanRequestOptions
import com.livelike.comment.models.CreateCommentBoardRequestOptions
import com.livelike.comment.models.DeleteCommentBanRequestOption
import com.livelike.comment.models.DeleteCommentBoardRequestOptions
import com.livelike.comment.models.GetBanDetailsRequestOptions
import com.livelike.comment.models.GetCommentBoardDetailRequestOptions
import com.livelike.comment.models.ListCommentBoardBanRequestOptions
import com.livelike.comment.models.UpdateCommentBoardRequestOptions
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import com.livelike.utils.NO_MORE_DATA
import com.livelike.utils.Once
import com.livelike.utils.PaginationResponse
import kotlinx.coroutines.CoroutineScope

internal class InternalLiveLikeCommentBoardClientImpl(
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    sdkScope: CoroutineScope,
    uiScope: CoroutineScope,
    private val networkApiClient: NetworkApiClient
) : BaseClient(configurationOnce, currentProfileOnce, sdkScope, uiScope),
    LiveLikeCommentBoardClient {

    private var commentBoardsResult: MutableMap<String, PaginationResponse<CommentBoard>> =
        mutableMapOf()
    private var commentBoardsBanResult: MutableMap<String, PaginationResponse<CommentBoardBanDetails>> =
        mutableMapOf()

    override fun createCommentBoard(
        createCommentBoardRequestOptions: CreateCommentBoardRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<CommentBoard>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.commentBoardsUrl.let {
                networkApiClient.post(
                    it,
                    createCommentBoardRequestOptions.toJsonString(),
                    pair.first.accessToken
                ).processResult()
            }
        }
    }


    override fun updateCommentBoard(
        updateCommentBoardRequestOptions: UpdateCommentBoardRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<CommentBoard>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            updateCommentBoardRequestOptions.commentBoardId?.let {
                pair.second.commentBoardDetailUrlTemplate.replace(
                    COMMENT_BOARD_TEMPLATE, it
                ).let { url ->
                    networkApiClient.patch(
                        url,
                        updateCommentBoardRequestOptions.toJsonString(),
                        pair.first.accessToken
                    ).processResult()
                }
            } ?: throw Exception(COMMENT_BOARD_ID_NOT_FOUND)
        }
    }


    override fun getCommentBoards(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<CommentBoard>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val result = commentBoardsResult[pair.second.clientId]
            if (result == null || liveLikePagination == LiveLikePagination.FIRST) {
                pair.second.commentBoardsUrl
            } else {
                when (liveLikePagination) {
                    LiveLikePagination.NEXT -> result.next
                    LiveLikePagination.PREVIOUS -> result.previous
                    else -> null

                }
            }?.let { url ->
                networkApiClient.get(
                    url, accessToken = pair.first.accessToken, listOf(
                        "client_id" to pair.second.clientId
                    )
                ).processResult<PaginationResponse<CommentBoard>>().also {
                    commentBoardsResult[pair.second.clientId] = it
                }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }

    override fun deleteCommentBoards(
        deleteCommentBoardRequestOptions: DeleteCommentBoardRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            deleteCommentBoardRequestOptions.commentBoardId?.let {
                pair.second.commentBoardDetailUrlTemplate.replace(
                    COMMENT_BOARD_TEMPLATE, it
                ).let { url ->
                    networkApiClient.delete(
                        url, accessToken = pair.first.accessToken
                    ).processResult()
                }
            } ?: throw Exception(COMMENT_BOARD_ID_NOT_FOUND)
        }
    }

    override fun getCommentBoardDetails(
        getCommentBoardDetailRequestOptions: GetCommentBoardDetailRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<CommentBoard>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.commentBoardDetailUrlTemplate.replace(
                COMMENT_BOARD_TEMPLATE, getCommentBoardDetailRequestOptions.commentBoardId
            ).let {
                networkApiClient.get(it, accessToken = pair.first.accessToken)
                    .processResult()
            }
        }
    }


    override fun createCommentBoardBan(
        banCommentBoardRequestOptions: CreateCommentBanRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<CommentBoardBanDetails>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.commentBoardBanUrl.let {
                networkApiClient.post(
                    it,
                    banCommentBoardRequestOptions.toJsonString(),
                    pair.first.accessToken
                ).processResult()
            }
        }
    }


    override fun getCommentBoardBans(
        listCommentBoardBanRequestOptions: ListCommentBoardBanRequestOptions?,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<CommentBoardBanDetails>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val result = commentBoardsBanResult[listCommentBoardBanRequestOptions.toString()]
            if (result == null || liveLikePagination == LiveLikePagination.FIRST) {
                pair.second.commentBoardBanUrl
            } else {
                when (liveLikePagination) {
                    LiveLikePagination.NEXT -> result.next
                    LiveLikePagination.PREVIOUS -> result.previous
                    else -> null
                }
            }?.let { url ->
                val queryParams = arrayListOf<Pair<String, String?>>()
                listCommentBoardBanRequestOptions?.let {
                    it.commentBoardId?.let { commentBoardId ->
                        queryParams.add(COMMENT_BOARD_ID to commentBoardId)
                    }
                    it.profileId?.let { profileId ->
                        queryParams.add(PROFILE_ID to profileId)
                    }

                }
                networkApiClient.get(
                    url, accessToken = pair.first.accessToken, queryParams
                ).processResult<PaginationResponse<CommentBoardBanDetails>>().also {
                    commentBoardsBanResult[listCommentBoardBanRequestOptions.toString()] =
                        it
                }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }


    override fun deleteCommentBoardBan(
        deleteCommentBanRequestOptions: DeleteCommentBanRequestOption,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.commentBoardBanUrlTemplate.replace(
                COMMENT_BOARD_BAN_TEMPLATE, deleteCommentBanRequestOptions.commentBoardBanId
            ).let { url ->
                networkApiClient.delete(
                    url, accessToken = pair.first.accessToken
                ).processResult()
            }
        }
    }


    override fun getCommentBoardBan(
        getBanDetailsRequestOptions: GetBanDetailsRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<CommentBoardBanDetails>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.commentBoardBanUrlTemplate.replace(
                COMMENT_BOARD_BAN_TEMPLATE, getBanDetailsRequestOptions.commentBoardBanId
            ).let { url ->
                networkApiClient.get(
                    url, accessToken = pair.first.accessToken
                ).processResult()
            }
        }
    }
}