package com.livelike.comment.models

import com.google.gson.annotations.SerializedName

/**
 * arguments used to create new board
 * @param customId integrator defined name of new board
 * @param title human readable name if new comment board
 * @param allowComments are comments accepted on this board
 * @param replyDepth how many levels deep are comments allows on this new board
 */
data class CreateCommentBoardRequestOptions(
    @SerializedName("custom_id")
    val customId: String?,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("allow_comments")
    val allowComments: Boolean? = null,
    @SerializedName("replies_depth")
    val replyDepth: Int? = null,
    @SerializedName("custom_data")
    val customData: String? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("content_filter")
    val contentFilter: String?=null
)
