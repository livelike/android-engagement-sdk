package com.livelike.comment.models

data class DismissCommentReportRequestOptions(
    val commentReportId: String
)