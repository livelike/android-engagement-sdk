package com.livelike.comment

import com.livelike.comment.models.AddCommentReplyRequestOptions
import com.livelike.comment.models.AddCommentRequestOptions
import com.livelike.comment.models.Comment
import com.livelike.comment.models.CommentBoard
import com.livelike.comment.models.CommentReport
import com.livelike.comment.models.CommentSortingOptions
import com.livelike.comment.models.CreateCommentReportRequestOptions
import com.livelike.comment.models.DeleteCommentReportRequestOptions
import com.livelike.comment.models.DeleteCommentRequestOptions
import com.livelike.comment.models.GetCommentBoardDetailRequestOptions
import com.livelike.comment.models.GetCommentRepliesRequestOptions
import com.livelike.comment.models.GetCommentReportsRequestOptions
import com.livelike.comment.models.GetCommentsRequestOptions
import com.livelike.comment.models.ReportStatusOptions
import com.livelike.common.LiveLikeCallback
import com.livelike.common.clients.LiveLikeProfileClient
import com.livelike.common.map
import com.livelike.common.model.BlockedInfo
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.utils.BaseSession
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.utils.LiveLikeException
import com.livelike.utils.Once
import com.livelike.utils.logError
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

internal class CommentSession(
    configurationOnce: Once<SdkConfiguration>,
    liveLikeUserOnce: Once<LiveLikeProfile>,
    private val commentClient: LiveLikeCommentClient,
    sessionDispatcher: CoroutineDispatcher,
    uiDispatcher: CoroutineDispatcher,
    private val errorDelegate: ErrorDelegate? = null,
    private val commentBoardClient: LiveLikeCommentBoardClient,
    private val commentBoardId: String,
    private val user: LiveLikeProfileClient,
    private val commentLocalCacheDelegate: CommentLocalCacheDelegate? = null,
    override val profaneComment: ProfaneComment = ProfaneComment.FILTERED,
    private val filteredTextForComment: ((Comment) -> String)? = null
) : BaseSession(
    configurationOnce, liveLikeUserOnce, sessionDispatcher, uiDispatcher, errorDelegate
), LiveLikeCommentSession {

    private lateinit var commentBoard: CommentBoard
    private var commentReplyMap = HashMap<String, List<Comment>>()
    private var originalCommentReplyMap = HashMap<String, List<Comment>>()
    private var updatedBySend: Boolean = false

    private val _commentsListFlow = MutableStateFlow<List<Comment>>(emptyList())
    override val commentListFlow: StateFlow<List<Comment>> = _commentsListFlow.asStateFlow()

    private val _commentsReportsListFlow = MutableStateFlow<List<CommentReport>>(emptyList())
    override val commentsReportsListFlow: StateFlow<List<CommentReport>> =
        _commentsReportsListFlow.asStateFlow()

    private val _commentsBlockedListFlow = MutableStateFlow<List<BlockedInfo>>(emptyList())
    override val commentsBlockedListFlow: StateFlow<List<BlockedInfo>> =
        _commentsBlockedListFlow.asStateFlow()

    private val _commentsCountFlow = MutableStateFlow(0)
    override val commentCountFlow: StateFlow<Int> = _commentsCountFlow.asStateFlow()

    private val _commentsReplyCount = MutableStateFlow(0)
    override val commentsReplyCountFlow: StateFlow<Int> = _commentsReplyCount.asStateFlow()

    private val _commentsLoadedFlow = MutableStateFlow(false)
    override val commentsLoadedFlow: StateFlow<Boolean> = _commentsLoadedFlow.asStateFlow()

    private val commentReplyStack = ArrayDeque<CommentReplyStack>(emptyList())

    private val _topCommentFlow = MutableStateFlow<Comment?>(null)
    override val topCommentFlow = _topCommentFlow.asStateFlow()

    private val _commentSortFlow = MutableStateFlow(CommentSortingOptions.NEWEST)
    override val commentSortFlow = _commentSortFlow.asStateFlow()

    private val _commentSortByReactionIDFlow = MutableStateFlow<String?>(null)
    override val commentSortByReactionIDFlow = _commentSortByReactionIDFlow.asStateFlow()

    init {
        getCommentReports()
        getBlockedProfileList()

        val commentReplyMap = commentLocalCacheDelegate?.getCommentReplyMap() ?: emptyMap()
        val originalCommentReplyMap =
            commentLocalCacheDelegate?.getOriginalCommentReplyMap() ?: emptyMap()
        this.commentReplyMap = HashMap(commentReplyMap)
        this.originalCommentReplyMap = HashMap(originalCommentReplyMap)

        commentClient.getCommentsCount(
            GetCommentsRequestOptions(
                sorting = commentSortFlow.value,
                reactionId = commentSortByReactionIDFlow.value,
                topLevel = true,
                withoutDeletedThread = true
            )
        ) { result, error ->
            result?.let {
                if (commentLocalCacheDelegate?.getCommentCount() == 0 || commentLocalCacheDelegate?.getCommentCount() == null) {
                    _commentsCountFlow.value = it
                } else {
                    _commentsCountFlow.value = commentLocalCacheDelegate.getCommentCount()
                }
            }
            error?.let { errorDelegate?.onError(it) }
        }

        commentBoardClient.getCommentBoardDetails(
            GetCommentBoardDetailRequestOptions(commentBoardId)
        ) { result, error ->
            result?.let {
                commentBoard = it
                openCommentReplies(null)
            }
            error?.let {
                logError { it }
                errorDelegate?.onError(DETAILS_NOT_FOUND)
            }
        }

    }

    override fun close() {
        commentLocalCacheDelegate?.saveHashmaps(commentReplyMap, originalCommentReplyMap)
        commentLocalCacheDelegate?.saveCommentCount(_commentsCountFlow.value)
    }

    override fun getCommentReports() {
        commentClient.getCommentReports(
            GetCommentReportsRequestOptions(
                ReportStatusOptions.PENDING, commentBoardId
            ), liveLikePagination = LiveLikePagination.FIRST
        ) { result: List<CommentReport>?, error: String? ->
            result?.let {
                _commentsReportsListFlow.value = it
            }
            error?.let {
                errorDelegate?.onError(it)
            }
        }
    }

    override fun getBlockedProfileList() {
        user.getBlockedProfileList(LiveLikePagination.FIRST) { result: List<BlockedInfo>?, error: String? ->
            result?.let {
                _commentsBlockedListFlow.value = it
            }
            error?.let {
                errorDelegate?.onError(it)
            }
        }
    }

    override fun loadHistory(pagination: LiveLikePagination) {
        val cRStack = commentReplyStack.lastOrNull()
        fun callback(result: List<Comment>?, error: String?) {
            result?.let { comments ->
                if (originalCommentReplyMap[topCommentFlow.value?.id.toString()] != result) {
                    if (pagination.name != "NEXT") {
                        originalCommentReplyMap[topCommentFlow.value?.id.toString()] = result
                    }
                    val filteredComments = comments.filter { comment ->
                        val isCommentBlocked =
                            comment.author.id in _commentsBlockedListFlow.value.map { blockedInfo ->
                                blockedInfo.blockedProfileID
                            }

                        val isCommentFiltered = comment.contentFilter?.contains(FILTERED) == true
                        val isMyComment = comment.isFromMe
                        if (profaneComment == ProfaneComment.FILTERED && !isMyComment) {
                            !(isCommentBlocked || isCommentFiltered)
                        } else {
                            !isCommentBlocked
                        }
                    }.map { comment ->
                        // Check if the comment is deleted by the user
                        val deletedByMe = comment.isDeletedByMe

                        // Check if the comment is deleted by others
                        val deletedByOthers = comment.isDeleted && !deletedByMe

                        // Determine the appropriate message
                        val updatedText = when {
                            deletedByMe -> DELETED_BY_YOU
                            deletedByOthers -> DELETED_BY_OTHERS
                            else -> comment.text // Use the original text if not deleted
                        }

                        val filteredTextFromIntegrator = filteredTextForComment?.let { it(comment) }
                        if (filteredTextFromIntegrator != null && profaneComment == ProfaneComment.CUSTOM) {
                            comment.copy(
                                text = updatedText,
                                filteredText = filteredTextFromIntegrator
                            )
                        } else {
                            comment.copy(text = updatedText)
                        }
                    }

                    val uniqueComments = filteredComments.filter { comment ->
                        comment.id !in _commentsListFlow.value.map { it.id }
                    }

                    _commentsLoadedFlow.value = false
                    _commentsListFlow.value = _commentsListFlow.value + uniqueComments
                    commentReplyMap[topCommentFlow.value?.id.toString()] = _commentsListFlow.value
                    _commentsLoadedFlow.value = true

                    if (cRStack?.comment != null) {
                        getCommentReplyCount(cRStack.comment.id) { result, error ->
                            result?.let {
                                val removedBlockedCount = comments.size - filteredComments.size

                                val resultF = if (updatedBySend || pagination.name == "NEXT") {
                                    topCommentFlow.value?.repliesCount!! - removedBlockedCount
                                } else {
                                    result - removedBlockedCount
                                }
                                cRStack.comment.let { originalComment ->
                                    val updatedComment =
                                        originalComment.copy(repliesCount = resultF)
                                    _topCommentFlow.value = updatedComment
                                    val updatedList =
                                        this.commentReplyStack[this.commentReplyStack.size - 2].list.map { commentInList ->
                                            if (commentInList.id == originalComment.id) {
                                                updatedComment
                                            } else {
                                                commentInList
                                            }
                                        }

                                    val secondLastComment =
                                        commentReplyStack[commentReplyStack.size - 2].comment
                                    commentReplyStack.removeLast()
                                    commentReplyStack.removeLast()
                                    commentReplyStack.add(
                                        CommentReplyStack(
                                            secondLastComment,
                                            updatedList
                                        )
                                    )
                                    commentReplyStack.add(
                                        CommentReplyStack(
                                            updatedComment,
                                            emptyList()
                                        )
                                    )
                                    updatedBySend = false
                                }
                            }
                            error?.let {
                                errorDelegate?.onError(it)
                            }
                        }
                    } else {
                        val removedBlockedCount = comments.size - filteredComments.size
                        commentClient.getCommentsCount(
                            GetCommentsRequestOptions(
                                sorting = commentSortFlow.value,
                                reactionId = commentSortByReactionIDFlow.value,
                                topLevel = true,
                                withoutDeletedThread = true
                            )
                        ) { result, error ->
                            result?.let {
                                _commentsCountFlow.value = it
                                if (removedBlockedCount > 0) {
                                    _commentsCountFlow.value -= removedBlockedCount
                                }
                            }
                            error?.let { errorDelegate?.onError(it) }
                        }
                    }
                } else {
                    _commentsLoadedFlow.value = false
                    _commentsListFlow.value = commentReplyMap[topCommentFlow.value?.id.toString()]!!
                    _commentsLoadedFlow.value = true
                }
            }
            error?.let {
                errorDelegate?.onError(it)
            }
        }
        if (cRStack?.comment != null) {
            commentClient.getCommentReplies(
                GetCommentRepliesRequestOptions(
                    cRStack.comment.id, commentSortFlow.value,  reactionId = commentSortByReactionIDFlow.value, withoutDeletedThread = true
                ), pagination, ::callback
            )
        } else {
            commentClient.getComments(
                GetCommentsRequestOptions(
                    sorting = commentSortFlow.value, reactionId = commentSortByReactionIDFlow.value, topLevel = true, withoutDeletedThread = true
                ), pagination, ::callback
            )

        }
    }

    override fun reloadComments() {
        _commentsListFlow.value = emptyList()
        loadHistory(LiveLikePagination.FIRST)
    }

    override fun getCommentBoard(callback: LiveLikeCallback<CommentBoard>) {
        if (this::commentBoard.isInitialized) {
            callback.invoke(commentBoard, null)
        } else {
            commentBoardClient.getCommentBoardDetails(
                GetCommentBoardDetailRequestOptions(
                    commentBoardId
                ), callback
            )
        }
    }

    override fun setCommentSortingOption(sortingOption: CommentSortingOptions) {
        _commentSortFlow.value = sortingOption
    }

    override fun setCommentSortingReactionIDOption(reactionId: String) {
        _commentSortByReactionIDFlow.value = reactionId
    }

    override fun loadPreviousHistory() {
        loadHistory(LiveLikePagination.PREVIOUS)
    }

    override fun loadNextHistory() {
        loadHistory(LiveLikePagination.NEXT)
    }

    override fun openCommentReplies(comment: Comment?) {
        if (isReplyAllowed()) {
            var crStack = commentReplyStack.lastOrNull()
            if (crStack != null) {
                crStack = crStack.copy(list = commentListFlow.value)
                commentReplyStack[commentReplyStack.size - 1] = crStack
            }
            commentReplyStack.addLast(CommentReplyStack(comment))
            _topCommentFlow.value = commentReplyStack.last().comment
            _commentsListFlow.value = emptyList()
            loadHistory(LiveLikePagination.FIRST)
        } else {
            errorDelegate?.onError(MAX_DEPTH_REACHED)
            logError { MAX_DEPTH_REACHED }
        }

    }


    override fun closeLastCommentReplies() {
        _commentsLoadedFlow.value = false
        if (commentReplyStack.size == 1) {
            logError { REACHED_TOP_LEVEL }
            return
        }
        commentReplyStack.removeLast()
        //TODO: use removedCRStack for scrolling back to last comment in the list
        val crStack = commentReplyStack.last()
        _topCommentFlow.value = commentReplyStack.last().comment
        _commentsListFlow.value = crStack.list
        _commentsLoadedFlow.value = true
    }

    override fun isTopComment(comment: Comment): Boolean {
        return comment == commentReplyStack.lastOrNull()?.comment
    }

    override fun closeAllCommentReplies() {
        _commentsLoadedFlow.value = false
        while (commentReplyStack.last().comment != null) {
            commentReplyStack.removeLast()
        }
        val crStack = commentReplyStack.last()
        _topCommentFlow.value = commentReplyStack.last().comment
        _commentsListFlow.value = crStack.list
        _commentsLoadedFlow.value = true

    }

    override fun sendComment(
        text: String, profileImageUrl: String?, liveLikeCallback: LiveLikeCallback<Unit>
    ) {
        safeCallBack(liveLikeCallback) {
            val cRStack = commentReplyStack.last()
            val comment = suspendCancellableCoroutine { cont ->
                fun callback(result: Comment?, error: String?) {
                    result?.let { cont.resume(it) }
                    error?.let { cont.resumeWithException(LiveLikeException(it)) }
                }
                _commentsLoadedFlow.value = false
                if (cRStack.comment != null) {
                    commentClient.addCommentReply(
                        AddCommentReplyRequestOptions(
                            parentCommentId = cRStack.comment.id,
                            text = text,
                            authorImageUrl = profileImageUrl
                        ), ::callback
                    )
                } else {
                    commentClient.addComment(
                        AddCommentRequestOptions(
                            text = text, authorImageUrl = profileImageUrl
                        ), ::callback
                    )
                }
            }

            _commentsListFlow.value = listOf(comment) + _commentsListFlow.value
            commentReplyMap[_topCommentFlow.value?.id.toString()] = _commentsListFlow.value
            _commentsLoadedFlow.value = true
            if (cRStack.comment?.id != null) {
                val fResult = topCommentFlow.value?.repliesCount!!.plus(1)
                cRStack.comment.let { originalComment ->
                    val updatedComment = originalComment.copy(repliesCount = fResult)
                    _topCommentFlow.value = updatedComment
                    val updatedList =
                        commentReplyStack[commentReplyStack.size - 2].list.map { commentInList ->
                            if (commentInList.id == originalComment.id) {
                                updatedComment
                            } else {
                                commentInList
                            }
                        }
                    val secondLastComment = commentReplyStack[commentReplyStack.size - 2].comment
                    commentReplyStack.removeLast()
                    commentReplyStack.removeLast()
                    commentReplyStack.add(CommentReplyStack(secondLastComment, updatedList))
                    commentReplyStack.add(CommentReplyStack(updatedComment, emptyList()))
                    updatedBySend = true
                    return@let
                }
            } else {
                _commentsCountFlow.value = _commentsCountFlow.value + 1
            }
        }
    }

    override fun reportComment(commentId: String, callback: LiveLikeCallback<CommentReport>) {
        commentClient.createCommentReport(
            CreateCommentReportRequestOptions(
                commentId,
            ), callback.map {
                _commentsReportsListFlow.value = _commentsReportsListFlow.value + it
                it
            }
        )
    }


    override fun deleteComment(
        commentId: String, callback: LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        commentClient.deleteComment(
            DeleteCommentRequestOptions(
                commentId,
            ), callback
        )
        if (_topCommentFlow.value == null) {
            val updatedComments = _commentsListFlow.value.map { comment ->
                if (comment.id == commentId) {
                    // Update the text and isDeleted properties here
                    comment.copy(
                        text = DELETED_BY_YOU,
                        isDeleted = true
                    )
                } else {
                    comment
                }
            }
            _commentsLoadedFlow.value = false
            _commentsListFlow.value = updatedComments
            commentReplyMap[topCommentFlow.value?.id.toString()] = _commentsListFlow.value
            _commentsLoadedFlow.value = true
        } else {
            if (_topCommentFlow.value!!.id == commentId) {
                val updatedComment = _topCommentFlow.value!!.copy(
                    text = DELETED_BY_YOU,
                    isDeleted = true
                )
                _topCommentFlow.value = updatedComment

                val updatedList =
                    commentReplyStack[commentReplyStack.size - 2].list.map { commentInList ->
                        if (commentInList.id == _topCommentFlow.value!!.id) {
                            updatedComment
                        } else {
                            commentInList
                        }
                    }
                val secondLastComment = commentReplyStack[commentReplyStack.size - 2].comment
                commentReplyStack.removeLast()
                commentReplyStack.removeLast()
                commentReplyStack.add(
                    CommentReplyStack(
                        secondLastComment,
                        updatedList
                    )
                )
                commentReplyStack.add(CommentReplyStack(updatedComment, emptyList()))
            } else if (_topCommentFlow.value!!.id != commentId) {
                val updatedComments = _commentsListFlow.value.map { comment ->
                    if (comment.id == commentId) {
                        // Update the text and isDeleted properties here
                        comment.copy(
                            text = DELETED_BY_YOU,
                            isDeleted = true
                        )
                    } else {
                        comment
                    }
                }
                _commentsLoadedFlow.value = false
                _commentsListFlow.value = updatedComments
                commentReplyMap[topCommentFlow.value?.id.toString()] = _commentsListFlow.value
                _commentsLoadedFlow.value = true
            }

        }

    }

    override fun blockUser(profileId: String, callback: LiveLikeCallback<BlockedInfo>) {
        user.blockProfile(
            profileId, callback
        )
    }

    override fun unBlockUser(profileId: String, callback: LiveLikeCallback<LiveLikeEmptyResponse>) {
        fun callbackInfo(result: BlockedInfo?, error: String?) {
            result?.let {
                user.unBlockProfile(result.id, callback)
            }
            error?.let { callback(null, it) }
        }
        user.getProfileBlockInfo(profileId, ::callbackInfo)
    }


    override fun getCommentReplies(
        getCommentRepliesRequestOptions: GetCommentRepliesRequestOptions,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: LiveLikeCallback<List<Comment>>
    ) {
        commentClient.getCommentReplies(
            getCommentRepliesRequestOptions,
            liveLikePagination,
            liveLikeCallback
        )
    }

    override fun unReportComment(
        commentReportId: String, callback: LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        commentClient.deleteCommentReport(
            DeleteCommentReportRequestOptions(commentReportId), callback.map { response ->
                _commentsReportsListFlow.value =
                    _commentsReportsListFlow.value.filter { it.id != commentReportId }
                response
            }
        )
    }

    override fun getCommentReplyCount(
        commentId: String, callback: LiveLikeCallback<Int>
    ) {
        commentClient.getCommentRepliesCount(
            GetCommentRepliesRequestOptions(
                commentId = commentId, CommentSortingOptions.NEWEST, withoutDeletedThread = true
            ), callback
        )
    }

    override fun isReplyAllowed(): Boolean {
        return try {
            commentReplyStack.size <= commentBoard.repliesDepth
        } catch (e: ArithmeticException) {
            logError { e }
            e.printStackTrace()
            false
        }
    }
}

