package com.livelike.comment.models

data class GetCommentReportRequestOptions(
    val commentReportId: String
)