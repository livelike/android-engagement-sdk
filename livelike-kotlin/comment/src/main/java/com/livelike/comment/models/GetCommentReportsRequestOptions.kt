package com.livelike.comment.models

import com.google.gson.annotations.SerializedName

data class GetCommentReportsRequestOptions(
    val reportStatus: ReportStatusOptions?=null,
    @SerializedName("comment_board_id")
    val commentBoardId: String? = null,
    @SerializedName("comment_id")
    val commentId: String? = null
)

