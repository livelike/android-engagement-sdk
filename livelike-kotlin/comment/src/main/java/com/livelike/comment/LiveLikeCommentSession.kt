package com.livelike.comment

import com.livelike.comment.models.Comment
import com.livelike.comment.models.CommentBoard
import com.livelike.comment.models.CommentReport
import com.livelike.comment.models.CommentSortingOptions
import com.livelike.comment.models.GetCommentRepliesRequestOptions
import com.livelike.common.LiveLikeCallback
import com.livelike.common.model.BlockedInfo
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import kotlinx.coroutines.flow.StateFlow

/**
 * An interface representing a session for managing and interacting with comments.
 * Provides functions and properties for loading, sorting, and interacting with comments in a session.
 */
interface LiveLikeCommentSession {

    // Properties for observing comment-related data flows
    val commentListFlow: StateFlow<List<Comment>>
    val commentsReportsListFlow: StateFlow<List<CommentReport>>
    val commentsBlockedListFlow: StateFlow<List<BlockedInfo>>
    val commentCountFlow: StateFlow<Int>
    val commentsLoadedFlow: StateFlow<Boolean>
    val commentSortFlow: StateFlow<CommentSortingOptions>
    val topCommentFlow: StateFlow<Comment?>
    val commentsReplyCountFlow: StateFlow<Int>
    val commentSortByReactionIDFlow: StateFlow<String?>

    // Profanity filter for comments
    val profaneComment: ProfaneComment

    // Functions for managing comments and interactions
    fun loadHistory(pagination: LiveLikePagination)
    fun getCommentReports()
    fun getBlockedProfileList()
    fun loadPreviousHistory()
    fun loadNextHistory()
    fun openCommentReplies(comment: Comment?)
    fun closeLastCommentReplies()
    fun closeAllCommentReplies()
    fun sendComment(text: String, profileImageUrl: String?, liveLikeCallback: LiveLikeCallback<Unit>)
    fun isReplyAllowed(): Boolean
    fun setCommentSortingOption(sortingOption: CommentSortingOptions)
    fun setCommentSortingReactionIDOption(reactionId: String)
    fun reloadComments()
    fun isTopComment(comment: Comment): Boolean
    fun reportComment(commentId: String, callback: LiveLikeCallback<CommentReport>)
    fun unReportComment(commentReportId: String, callback: LiveLikeCallback<LiveLikeEmptyResponse>)
    fun deleteComment(commentId: String, callback: LiveLikeCallback<LiveLikeEmptyResponse>)
    fun getCommentReplyCount(commentId: String, callback: LiveLikeCallback<Int>)
    fun getCommentBoard(callback: LiveLikeCallback<CommentBoard>)
    fun blockUser(profileId: String, callback: LiveLikeCallback<BlockedInfo>)
    fun unBlockUser(profileId: String, callback: LiveLikeCallback<LiveLikeEmptyResponse>)
    fun getCommentReplies(
        getCommentRepliesRequestOptions: GetCommentRepliesRequestOptions,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback:LiveLikeCallback<List<Comment>>
    )

    // Function for closing the comment session
    fun close()
}


data class CommentReplyStack(val comment: Comment?, val list: List<Comment> = emptyList())