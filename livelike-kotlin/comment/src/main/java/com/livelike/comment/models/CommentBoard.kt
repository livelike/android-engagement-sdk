package com.livelike.comment.models

import com.google.gson.annotations.SerializedName

data class CommentBoard(
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    internal val url: String,
    @SerializedName("custom_id")
    val optionalCustomId: String?,
    @SerializedName("client_id")
    val clientId: String,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("allow_comments")
    val allowComments: Boolean,
    @SerializedName("replies_depth")
    val repliesDepth: Int,
    @SerializedName("comments_url")
    internal val commentsUrl: String,
    @SerializedName("comment_detail_url_template")
    internal val commentDetailUrlTemplate: String,
    @SerializedName("created_by_id")
    val createdById: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("custom_data")
    val customData: String? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("comment_report_url")
    internal val commentReportUrl: String,
    @SerializedName("dismiss_reported_comment_url_template")
    internal val dismissReportedCommentUrlTemplate: String,
    @SerializedName("comment_report_detail_url_template")
    internal val commentReportDetailUrlTemplate: String,
    @SerializedName("content_filter")
    val contentFilter: String? = null
) {
    @Deprecated(
        "customId is no longer required and will return an empty string if nil",
        ReplaceWith("optionalCustomId")
    )
    val customId: String
        get() {
            return optionalCustomId ?: ""
        }
}