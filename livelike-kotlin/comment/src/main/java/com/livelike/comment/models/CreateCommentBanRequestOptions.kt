package com.livelike.comment.models

import com.google.gson.annotations.SerializedName

data class CreateCommentBanRequestOptions(
    @SerializedName("profile_id")
    val profileId: String,
    @SerializedName("comment_board_id")
    val commentBoardId: String? = null,
    @SerializedName("description")
    val description: String? = null,
)
