package com.livelike.comment.models

import com.google.gson.annotations.SerializedName

data class GetCommentRepliesRequestOptions(
    val commentId: String,
    val sorting: CommentSortingOptions?,
    @SerializedName("without_deleted_thread")
    val withoutDeletedThread:Boolean?=null,
    @SerializedName("reaction_id")
    val reactionId: String? = null, //The reaction id to sort when using ordering `reactionCountAscending` or `reactionCountDescending`
)
