package com.livelike.comment.models

data class DismissAllReports(
    val detail: String
)