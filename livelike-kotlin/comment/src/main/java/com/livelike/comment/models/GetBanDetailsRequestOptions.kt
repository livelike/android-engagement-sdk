package com.livelike.comment.models

import com.google.gson.annotations.SerializedName

data class GetBanDetailsRequestOptions(
    @SerializedName("comment-board-ban-id")
    val commentBoardBanId: String
)