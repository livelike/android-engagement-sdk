package com.livelike.comment.models

import com.google.gson.annotations.SerializedName

data class CreateCommentReportRequestOptions(
    @SerializedName("comment_id")
    val commentId: String,
    val description: String? = null
)