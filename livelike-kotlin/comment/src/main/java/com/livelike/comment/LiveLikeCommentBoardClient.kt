package com.livelike.comment

import com.livelike.comment.models.*
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.LiveLikeCallback
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse

/**
Provide CommentBoard Apis
 */
interface LiveLikeCommentBoardClient {

    /**
     * Creates a new comment board.
     *
     * @param createCommentBoardRequestOptions Arguments used to create a comment board.
     * @param liveLikeCallback Callback to handle the response.
     */
    fun createCommentBoard(
        createCommentBoardRequestOptions: CreateCommentBoardRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<CommentBoard>
    )

    /**
     * Updates an existing comment board.
     *
     * @param updateCommentBoardRequestOptions Arguments used to update the comment board.
     * @param liveLikeCallback Callback to handle the response.
     */
    fun updateCommentBoard(
        updateCommentBoardRequestOptions: UpdateCommentBoardRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<CommentBoard>
    )

    /**
     * Retrieves a list of comment boards.
     *
     * @param liveLikePagination A configuration for paginating the results.
     * @param liveLikeCallback Callback to handle the response with a list of comment boards.
     */

    fun getCommentBoards(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<CommentBoard>>
    )

    /**
     * Deletes a comment board based on the provided options.
     *
     * @param deleteCommentBoardRequestOptions The arguments used to delete the comment board.
     * @param liveLikeCallback Callback to handle the response after deleting the comment board.
     */
    fun deleteCommentBoards(
        deleteCommentBoardRequestOptions: DeleteCommentBoardRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    )

    /**
     * Retrieves details of a comment board based on the provided options.
     *
     * @param getCommentBoardDetailRequestOptions The arguments used to specify the comment board for which details are requested.
     * @param liveLikeCallback Callback to handle the response containing the comment board details.
     */
    fun getCommentBoardDetails(
        getCommentBoardDetailRequestOptions: GetCommentBoardDetailRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<CommentBoard>
    )

    /**
     * Creates a ban for a specific profile on a comment board based on the provided options.
     *
     * @param banCommentBoardRequestOptions The arguments used to specify the ban on a comment board.
     * @param liveLikeCallback Callback to handle the response containing the details of the created ban.
     */
    fun createCommentBoardBan(
        banCommentBoardRequestOptions: CreateCommentBanRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<CommentBoardBanDetails>
    )

    /**
     * Retrieves a list of bans from a comment board, optionally filtered by request options and paginated.
     *
     * @param listCommentBoardBanRequestOptions (Optional) Arguments used to filter the list of bans.
     * @param liveLikePagination The pagination settings for retrieving the list of bans.
     * @param liveLikeCallback Callback to handle the response containing the list of ban details.
     */
    fun getCommentBoardBans(
        listCommentBoardBanRequestOptions: ListCommentBoardBanRequestOptions?,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<CommentBoardBanDetails>>
    )

    /**
     * Deletes a ban from a comment board for a specific ID.
     *
     * @param deleteCommentBanRequestOptions Arguments used to remove a ban from the comment board.
     * @param liveLikeCallback Callback to handle the response indicating the success of the operation.
     */
    fun deleteCommentBoardBan(
        deleteCommentBanRequestOptions: DeleteCommentBanRequestOption,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    )

    /**
     * Get details about a ban from a comment board for a specific ID.
     *
     * @param getBanDetailsRequestOptions Arguments used to get ban details from the comment board.
     * @param liveLikeCallback Callback to handle the response with details about the ban.
     */
    fun getCommentBoardBan(
        getBanDetailsRequestOptions : GetBanDetailsRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<CommentBoardBanDetails>
    )
}

