package com.livelike.comment.models

import com.google.gson.annotations.SerializedName

data class DeleteCommentBanRequestOption(
    @SerializedName("comment-board-ban-id")
    val commentBoardBanId: String
)
