package com.livelike.comment.models

enum class ReportStatusOptions(val serializedName: String) {
    PENDING("pending"),
    DISMISSED("dismissed")
}