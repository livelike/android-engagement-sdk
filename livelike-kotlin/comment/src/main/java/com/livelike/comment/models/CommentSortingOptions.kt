package com.livelike.comment.models

enum class CommentSortingOptions(val serializedName: String) {
    OLDEST("created_at"),
    NEWEST("-created_at"),
    OLDEST_REPLIES("reply"),
    NEWEST_REPLIES("-reply"),
    REACTION_COUNT_ASCENDING("reaction_count"), //Orders comments in ascending order of reaction counts. To use this, it is mandatory to set a `reactionID` in request options
    REACTION_COUNT_DECENDING("-reaction_count") //Orders comments in descending order of reaction counts. To use this, it is mandatory to set a `reactionID` in request options
}
