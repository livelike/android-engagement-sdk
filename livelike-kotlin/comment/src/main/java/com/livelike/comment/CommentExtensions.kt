package com.livelike.comment

import com.livelike.comment.models.Comment
import com.livelike.common.LiveLikeKotlin
import com.livelike.common.profile
import kotlinx.coroutines.Dispatchers


/**
To access LiveLikeCommentBoardClient
 */
fun LiveLikeKotlin.commentBoard(): LiveLikeCommentBoardClient {
    return InternalLiveLikeCommentBoardClientImpl(
        sdkConfigurationOnce,
        profile().currentProfileOnce,
        sdkScope,
        uiScope,
        networkClient
    )
}

/**
To access LiveLikeCommentClient
 */
fun LiveLikeKotlin.comment(
    commentBoardId: String
): LiveLikeCommentClient {
    return InternalLiveLikeCommentClientImpl(
        sdkConfigurationOnce,
        profile().currentProfileOnce,
        sdkScope,
        uiScope,
        networkClient,
        commentBoard(),
        commentBoardId
    )
}

fun LiveLikeKotlin.createCommentSession(
    commentBoardId: String,
    profaneComment: ProfaneComment = ProfaneComment.FILTERED,
    filteredTextForComment: ((Comment) -> String)?=null,
    delegate: CommentLocalCacheDelegate?=null
): LiveLikeCommentSession {
    return CommentSession(
        sdkConfigurationOnce,
        profile().currentProfileOnce,
        comment(commentBoardId),
        sessionDispatcher = Dispatchers.Default,
        uiDispatcher = Dispatchers.Main,
        errorDelegate,
        commentBoard(),
        commentBoardId,
        profile(),
        delegate,
        profaneComment,
        filteredTextForComment
    )
}