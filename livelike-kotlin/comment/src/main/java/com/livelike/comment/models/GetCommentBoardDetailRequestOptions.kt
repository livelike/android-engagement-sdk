package com.livelike.comment.models

data class GetCommentBoardDetailRequestOptions(val commentBoardId: String)
