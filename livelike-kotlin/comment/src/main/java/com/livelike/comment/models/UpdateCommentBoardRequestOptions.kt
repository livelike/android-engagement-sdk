package com.livelike.comment.models

import com.google.gson.annotations.SerializedName

/**
 * used to update a board
 */
data class UpdateCommentBoardRequestOptions(
    @Transient
    val commentBoardId: String? = null,
    @SerializedName("custom_id")
    val customId: String?,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("allow_comments")
    val allowComments: Boolean? = null,
    @SerializedName("replies_depth")
    val replyDepth: Int? = null,
    @SerializedName("custom_data")
    val customData: String? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("content_filter")
    val contentFilter: String?=null
)
