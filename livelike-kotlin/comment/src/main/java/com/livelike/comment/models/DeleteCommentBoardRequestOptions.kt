package com.livelike.comment.models

/**
 * used to delete boards
 */
data class DeleteCommentBoardRequestOptions(
    val commentBoardId: String? = null
)