package com.livelike.comment.models

import com.google.gson.annotations.SerializedName

data class DeleteCommentReportRequestOptions(
    @SerializedName("report_id")
    val commentReportId: String
)