package com.livelike.comment.models

import com.google.gson.annotations.SerializedName

data class AddCommentRequestOptions(
    @SerializedName("text")
    val text: String? = null,
    //"images": as of ESINTEG-324 only text is in scope
    @SerializedName("custom_data")
    val customData: String? = null,
    @SerializedName("author_image_url")
    val authorImageUrl : String?=null
) {
    @SerializedName("comment_board_id")
    internal var commentBoardId: String? = null

}

