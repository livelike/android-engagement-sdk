package com.livelike.comment.models

import com.google.gson.annotations.SerializedName

data class GetCommentsRequestOptions(
    val sorting: CommentSortingOptions? = null,
    val repliedSince: String? = null,
    val repliedUntil: String? = null,
    @SerializedName("is_reported")
    val isReported: Boolean? = null,
    @SerializedName("top_level")
    val topLevel: Boolean? = null,
    val since: String? = null,
    val until: String? = null,
    @SerializedName("without_deleted_thread")
    val withoutDeletedThread: Boolean? = null,
    @SerializedName("reaction_id")
    val reactionId: String? = null, //The reaction id to sort when using ordering `reactionCountAscending` or `reactionCountDescending`
)


