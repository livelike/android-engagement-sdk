package com.livelike.comment

import com.livelike.comment.models.Comment

/**
 * Interface for managing local caching of comment-related data.
 */
interface CommentLocalCacheDelegate {

    /**
     * Save or update two maps: one containing comment replies and another containing original comment replies.
     *
     * @param commentReplyMap Map of comment reply data.
     * @param originalCommentReplyMap Map of original comment reply data.
     */
    fun saveHashmaps(
        commentReplyMap: Map<String, List<Comment>>,
        originalCommentReplyMap: Map<String, List<Comment>>
    )

    /**
     * Retrieve the stored comment reply map.
     *
     * @return Map of comment reply data.
     */
    fun getCommentReplyMap(): Map<String, List<Comment>>

    /**
     * Retrieve the stored original comment reply map.
     *
     * @return Map of original comment reply data.
     */
    fun getOriginalCommentReplyMap(): Map<String, List<Comment>>

    /**
     * Save the count of comments.
     *
     * @param count Count of comments to be saved.
     */
    fun saveCommentCount(count: Int)

    /**
     * Retrieve the count of comments.
     *
     * @return Count of comments.
     */
    fun getCommentCount(): Int
}
