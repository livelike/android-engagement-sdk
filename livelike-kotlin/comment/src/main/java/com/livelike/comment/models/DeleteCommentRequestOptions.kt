package com.livelike.comment.models

/**
 * used to delete comment
 * @param id the id that wil be deleted
 */
data class DeleteCommentRequestOptions(
    val commentId: String
)

