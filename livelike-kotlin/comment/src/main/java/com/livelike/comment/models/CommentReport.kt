package com.livelike.comment.models

import com.google.gson.annotations.SerializedName

data class CommentReport(
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    internal val url: String? = null,
    @SerializedName("comment_id")
    internal val commentId: String? = null,
    val comment: Comment? = null,
    @SerializedName("reported_by_id")
    val reportedById: String? = null,
    @SerializedName("reported_at")
    val reportedAt: String,
    val description: String? = null,
    @SerializedName("report_status")
    val reportStatus: String? = null,
    @SerializedName("comment_board_id")
    val commentBoardId: String
)


