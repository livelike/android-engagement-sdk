package com.livelike.comment.models

import com.google.gson.annotations.SerializedName

data class Comment(
    @SerializedName("id") val id: String,
    @SerializedName("url") internal val url: String,
    @SerializedName("comment_board_id") val commentBoardId: String? = null,
    @SerializedName("parent_comment_id") val parentCommentId: String? = null,
    @SerializedName("thread_comment_id") val threadCommentId: String? = null,
    @SerializedName("text") val text: String? = null,
    //"images":  as of ESINTEG-324 only text is in scope
    @SerializedName("comment_depth") val commentDepth: Int,
    @SerializedName("custom_data") val customData: String? = null,
    @SerializedName("created_at") val createdAt: String,
    @SerializedName("replies_url") internal val repliesUrl: String,
    @SerializedName("replies_count") val repliesCount: Int,
    @SerializedName("is_deleted") val isDeleted: Boolean,
    @SerializedName("deleted_by") val deletedBy: String? = null,
    @SerializedName("deleted_at") val deletedAt: String? = null,
    @SerializedName("is_reported") val isReported: Boolean,
    @SerializedName("comment_reports_count") val commentReportsCount: Int,
    @SerializedName("dismiss_reported_comment_url") internal val dismissReportedCommentUrl: String,
    val author: Profile,
    @SerializedName("author_image_url") val authorImageUrl: String? = null,
    @SerializedName("filtered_text") val filteredText: String? = null,
    @SerializedName("content_filter") val contentFilter: List<String>? = null,
    val isFromMe: Boolean = false,
    val isDeletedByMe: Boolean = false
)

data class Profile(
    @SerializedName("id") val id: String, @SerializedName("nickname") val nickname: String
)