package com.livelike.engagementsdk

import com.livelike.common.AccessTokenDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.common.getNetworkSuccessResultForFile
import com.livelike.common.initSDK
import com.livelike.common.profile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.widget.model.GetPublishedWidgetsRequestOptions
import com.livelike.network.NetworkApiClient
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

@OptIn(ExperimentalCoroutinesApi::class)
class ContentSessionCoreTest {
    private val testDispatcher = StandardTestDispatcher()

    @MockK
    lateinit var client: NetworkApiClient

    @Before
    fun setUp() {
        // do the setup
        MockKAnnotations.init(this)
        Dispatchers.setMain(testDispatcher)
    }

    private suspend fun setUpSDK(): LiveLikeKotlin {
        coEvery { client.get("http://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4") } returns "application_success.json".getNetworkSuccessResultForFile(
            javaClass.classLoader
        )
        coEvery {
            client.get(
                "https://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4/profile/",
                accessToken = any()
            )
        } returns "profile_fetch_success.json".getNetworkSuccessResultForFile(javaClass.classLoader)
        val sdk =
            initSDK(client, testDispatcher, accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJibGFzdHJ0IiwiaWQiOiJlZGFiMzNlZS0yODgwLTRkZWUtOTIxZC03ZjRkM2Q3YTQ1MDQiLCJjbGllbnRfaWQiOiJDZHNkTEtMM21TbGx5U3h5TzJValNSUkg0RXFGbVVGYmZTWFRaV1c0IiwiaWF0IjoxNjY5NzE2NjYxLCJhY2Nlc3NfdG9rZW4iOiIwNmMwNjMwZjJhY2UxYzliMmIyNWU5ZDk2NmRlOTcxZjUyODE1ODg0In0.OfxWOLCozRhL3aKzxwQMc2Fn7XmAcpuJhcdEReCwIBo"
                }

                override fun storeAccessToken(accessToken: String?) {

                }
            })
        sdk.profile().currentProfileOnce()
        return sdk
    }

    @After
    fun tearDown() {
        testDispatcher.cancel()
    }

    @Test
    fun `test content session success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/chat-rooms/84f53947-efa5-4c8b-bbd3-ba14b825f7ed/",
                accessToken = null,
                queryParameters = any()
            )
        } answers { "fetch-chat-room-success.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
        coEvery {
            client.get(
                "https://localhost/api/v1/blocked-profile-ids/",
                accessToken = any(),
                queryParameters = any()
            )
        } answers { "block-profile-ids-success.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        coEvery {
            client.get(
                "https://localhost/api/v1/chatroom-messages/",
                accessToken = any(),
                queryParameters = any()
            )
        } answers { "chat-message-history-success.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        coEvery {
            client.get(
                "https://localhost/api/v1/programs/99124763-4aba-47e0-a2a5-603fd336f891/program.json",
                accessToken = null
            )
        } answers { "program-details-success.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        val session = sdk.createContentSession("99124763-4aba-47e0-a2a5-603fd336f891",
            object : LiveLikeKotlin.TimecodeGetterCore {
                override fun getTimecode(): EpochTime {
                    return EpochTime(0)
                }
            },
            sessionDispatcher = testDispatcher,
            chatSessionDispatcher = testDispatcher,
            uiDispatcher = testDispatcher,
            chatMainDispatcher = testDispatcher,
            chatMessageDeletedMessage = "",
            quoteChatMessageDeletedMessage = "",
            isPlatformLocalContentImageUrl = { false })
        (session as ContentSession).programOnce().let { item ->
            println("item: $item")
            assert(item.id == "99124763-4aba-47e0-a2a5-603fd336f891")
        }
        session.close()
    }


    @Test
    fun `test published widget success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/chat-rooms/84f53947-efa5-4c8b-bbd3-ba14b825f7ed/",
                accessToken = null,
                queryParameters = any()
            )
        } answers { "fetch-chat-room-success.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
        coEvery {
            client.get(
                "https://localhost/api/v1/blocked-profile-ids/",
                accessToken = any(),
                queryParameters = any()
            )
        } answers { "block-profile-ids-success.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        coEvery {
            client.get(
                "https://localhost/api/v1/chatroom-messages/",
                accessToken = any(),
                queryParameters = any()
            )
        } answers { "chat-message-history-success.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        coEvery {
            client.get(
                "https://localhost/api/v1/programs/99124763-4aba-47e0-a2a5-603fd336f891/program.json",
                accessToken = null
            )
        } answers { "program-details-success.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        coEvery {
            client.get(
                "https://localhost/api/v1/programs/99124763-4aba-47e0-a2a5-603fd336f891/widgets/?ordering=recent&status=published&page=1",
                accessToken = any(),
                queryParameters = any()
            )
        } answers {
            "get-published-widgets-with-vod-usecase.json".getNetworkSuccessResultForFile(
                javaClass.classLoader
            )
        }

        coEvery {
            client.get(
                "https://localhost/api/v1/profiles/edab33ee-2880-4dee-921d-7f4d3d7a4504/widget-interactions/?text_quiz_id=2f84b985-db3a-453c-9705-daef32b6e120&alert_id=d5b7ec00-c6f6-4867-9c41-fde449831dfa&alert_id=263c0f19-b550-48ee-8c36-916a0bbfe004&alert_id=f7b4a20b-94e8-434a-ba33-28c5317c883e",
                accessToken = any(),
                queryParameters = any()
            )
        } answers { "widget-interaction.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        val session = sdk.createContentSession("99124763-4aba-47e0-a2a5-603fd336f891",
            { EpochTime(0) },
            sessionDispatcher = testDispatcher,
            chatSessionDispatcher = testDispatcher,
            uiDispatcher = testDispatcher,
            chatMainDispatcher = testDispatcher,
            chatMessageDeletedMessage = "",
            quoteChatMessageDeletedMessage = "",
            isPlatformLocalContentImageUrl = { false })

        val res = suspendCoroutine { cont ->
            session.getPublishedWidgets(
                GetPublishedWidgetsRequestOptions(
                    sincePlaybackTimeMs = 10000,
                    untilPlaybackTimeMs = 60000,
                    liveLikePagination = LiveLikePagination.FIRST
                )
            ) { result, _ -> result?.let { cont.resume(it) } }
        }
        advanceUntilIdle()
        assert(res[0].kind == "alert")
        // assert(res[0].playbackTimeMs.toString() == "40000")
        session.close()
    }

}