package com.livelike.end2end

import app.cash.turbine.test
import com.livelike.common.AccessTokenDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.engagementsdk.EpochTime
import com.livelike.engagementsdk.LiveLikeContentSession
import com.livelike.engagementsdk.core.data.models.NumberPredictionVotes
import com.livelike.engagementsdk.createContentSession
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.engagementsdk.widget
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.widgetModel.CheerMeterWidgetmodel
import com.livelike.engagementsdk.widget.widgetModel.NumberPredictionWidgetModel
import com.livelike.engagementsdk.widget.widgetModel.PollWidgetModel
import com.livelike.engagementsdk.widget.widgetModel.PredictionWidgetViewModel
import com.livelike.engagementsdk.widget.widgetModel.QuizWidgetModel
import com.livelike.engagementsdk.widget.widgetModel.TextAskWidgetModel
import com.livelike.utils.LiveLikeException
import com.livelike.utils.LogLevel
import com.livelike.utils.minimumLogLevel
import com.livelike.widget.CreateAlertRequest
import com.livelike.widget.CreateCheerMeterRequest
import com.livelike.widget.CreateImageNumberPredictionRequest
import com.livelike.widget.CreateImagePollRequest
import com.livelike.widget.CreateImagePredictionRequest
import com.livelike.widget.CreateImageQuizRequest
import com.livelike.widget.CreateRichPostRequest
import com.livelike.widget.CreateTextAskRequest
import com.livelike.widget.CreateTextPollRequest
import com.livelike.widget.CreateTextPredictionRequest
import com.livelike.widget.CreateTextQuizRequest
import com.livelike.widget.DeleteWidgetRequest
import com.livelike.widget.GetWidgetRequest
import com.livelike.widget.LiveLikeWidgetClient
import com.livelike.widget.PublishWidgetRequest
import com.livelike.widget.UpdateImageNumberPredictionOptionRequest
import com.livelike.widget.UpdateImagePredictionOptions
import com.livelike.widget.UpdateTextPredictionOptions
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

class LiveLikeWidgetEnd2EndTest {
    private lateinit var session: LiveLikeContentSession
    private lateinit var programId: String
    private lateinit var widgetClient: LiveLikeWidgetClient
    private lateinit var liveLikeKotlin: LiveLikeKotlin
    private val testDispatcher = StandardTestDispatcher()
    private lateinit var clientId: String
    private lateinit var producerToken: String
    private lateinit var sponsorIds: List<String>

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        minimumLogLevel = LogLevel.Debug
        producerToken = System.getenv("producerToken")
        clientId = System.getenv("clientId")
        programId = System.getenv("programId")
        val sponsors = System.getenv("sponsors")
        sponsorIds = sponsors.split(",")
        liveLikeKotlin = LiveLikeKotlin(
            clientId,
            accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return producerToken
                }

                override fun storeAccessToken(accessToken: String?) {

                }

            },
            networkDispatcher = testDispatcher,
            sdkDispatcher = testDispatcher,
            mainDispatcher = testDispatcher
        )
        widgetClient = liveLikeKotlin.widget()
        val errorDelegate = object : ErrorDelegate() {
            override fun onError(error: String) {

            }
        }
        session = liveLikeKotlin.createContentSession(programId, { EpochTime(0L) }, errorDelegate)
    }

    @After
    fun tearDown() {
        testDispatcher.cancel()
    }

    @Test
    fun testAlert() = runTest {
        val alert = suspendCoroutine { cont ->
            widgetClient.createAlert(
                CreateAlertRequest(
                    timeout = "P0DT00H00M20S",
                    title = "Test Alert",
                    text = "Test Alert Message",
                    imageURL = "https://wallpapers.com/images/hd/football-background-8543042efgz33p6a.jpg",
                    linkLabel = "Check out",
                    linkURL = "https://www.google.com",
                    programId = programId,
                    sponsorIds = sponsorIds
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        println("Timeout: ${alert.timeout}")
        assert(alert.title == "Test Alert")
        assert(alert.text == "Test Alert Message")
        assert(alert.imageUrl == "https://wallpapers.com/images/hd/football-background-8543042efgz33p6a.jpg")
        assert(alert.linkUrl == "https://www.google.com")
        assert(alert.linkLabel == "Check out")
        println("Sponsors: ${alert.sponsors?.size}")
        assert(alert.sponsors?.size == 1 && alert.sponsors!![0].id == "f02950ab-5c1b-4303-b6e3-40a9f12e7260")

        val publishWidgetResponse = suspendCoroutine { cont ->
            widgetClient.publishWidget(
                PublishWidgetRequest(
                    WidgetType.ALERT, alert.id, publishDelay = null, programDateTime = null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        println("STATUS: ${publishWidgetResponse.status}")
        val deleteWidget = suspendCoroutine { cont ->
            widgetClient.deleteWidget(
                DeleteWidgetRequest(
                    WidgetType.ALERT, alert.id
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
    }

    @Test
    fun testCheerMeter() = runTest(timeout = 30.seconds) {
        val cheerMeter = suspendCoroutine { cont ->
            widgetClient.createCheerMeter(
                CreateCheerMeterRequest(
                    timeout = "P0DT00H00M20S",
                    programId = programId,
                    question = "Test Question",
                    cheerType = "tap",
                    options = listOf(
                        CreateCheerMeterRequest.Option(
                            "Mike",
                            "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg"
                        ),
                        CreateCheerMeterRequest.Option(
                            "Jelzon", "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg"
                        ),
                    ),
                    customData = "Custom Data",
                    sponsorIds = sponsorIds
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        val getWidget = suspendCoroutine { cont ->
            widgetClient.getWidget(
                GetWidgetRequest(
                    cheerMeter.id, WidgetType.CHEER_METER
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        assert(cheerMeter.question == getWidget.question && getWidget.question == "Test Question")
        assert(cheerMeter.cheerType == getWidget.cheerType && getWidget.cheerType == "tap")
        assert(cheerMeter.options?.size == getWidget.options?.size && getWidget.options?.size == 2)
        assert(cheerMeter.programId == getWidget.programId && getWidget.programId == programId)
        assert(cheerMeter.customData == getWidget.customData && getWidget.customData == "Custom Data")
        assert(cheerMeter.sponsors?.size == 1 && cheerMeter.sponsors!![0].id == "f02950ab-5c1b-4303-b6e3-40a9f12e7260")

        val publishWidgetResponse = suspendCoroutine { cont ->
            widgetClient.publishWidget(
                PublishWidgetRequest(
                    WidgetType.CHEER_METER,
                    cheerMeter.id,
                    publishDelay = null,
                    programDateTime = null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        println("STATUS: ${publishWidgetResponse.status}")

        val cheerMeterWidgetModel: CheerMeterWidgetmodel =
            session.getWidgetModelFromLiveLikeWidget(cheerMeter) as CheerMeterWidgetmodel

        cheerMeterWidgetModel.submitVote(cheerMeter.options!![0].id)
        cheerMeterWidgetModel.submitVote(cheerMeter.options!![0].id)
        cheerMeterWidgetModel.submitVote(cheerMeter.options!![0].id)

        cheerMeterWidgetModel.submitVote(cheerMeter.options!![1].id)
        cheerMeterWidgetModel.submitVote(cheerMeter.options!![1].id)

        var interactionSize = 0
        while (interactionSize < 5) {
            val interactionHistory = suspendCoroutine { cont ->
                cheerMeterWidgetModel.loadInteractionHistory { result, error ->
                    result?.let { cont.resume(it) }
                    error?.let { cont.resumeWithException(LiveLikeException(it)) }
                }
            }
            interactionSize = interactionHistory.sumOf { it.totalScore }
        }
        assert(interactionSize == 5)

        cheerMeterWidgetModel.voteResultsFlow.test {
            var item = awaitItem()
            while (item?.choices?.sumOf { it.voteCount ?: 0 } != 5) item = awaitItem()
            val count1 = item.choices?.get(0)?.voteCount
            val count2 = item.choices?.get(1)?.voteCount
            if (count1 != null && count2 != null) {
                assert(count1 + count2 == 5)
            }
        }

        val deleteWidget = suspendCoroutine { cont ->
            widgetClient.deleteWidget(
                DeleteWidgetRequest(
                    WidgetType.CHEER_METER, cheerMeter.id
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
    }

    @Test
    fun testPoll() = runTest(timeout = 40.seconds) {
        val textPoll = suspendCoroutine { cont ->
            widgetClient.createTextPoll(
                CreateTextPollRequest(
                    "Test Text Poll",
                    options = listOf(
                        CreateTextPollRequest.Option("option a"),
                        CreateTextPollRequest.Option("option b"),
                    ),
                    timeout = "P0DT00H00M20S",
                    programId = programId,
                    sponsorIds = sponsorIds
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(textPoll.question == "Test Text Poll")
        assert(textPoll.options?.size == 2 && textPoll.options?.any { it.imageUrl == null && it.description != null } == true)
        assert(textPoll.sponsors?.size == 1 && textPoll.sponsors!![0].id == "f02950ab-5c1b-4303-b6e3-40a9f12e7260")
        println("Text Poll Created")

        val imagePoll = suspendCoroutine { cont ->
            widgetClient.createImagePoll(
                CreateImagePollRequest(
                    question = "Test Image Poll", options = listOf(
                        CreateImagePollRequest.Option(
                            "option a", "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg"
                        ),
                        CreateImagePollRequest.Option(
                            "option b", "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg"
                        ),
                    ), timeout = "P0DT00H00M20S", programId = programId
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(imagePoll.question == "Test Image Poll")
        assert(imagePoll.options?.size == 2 && imagePoll.options?.any { it.imageUrl != null && it.description != null } == true)
        println("Image Poll Created")
        //Voting
        val pollWidgetModel: PollWidgetModel =
            session.getWidgetModelFromLiveLikeWidget(textPoll) as PollWidgetModel
        //First Option Vote
        suspendCoroutine { cont ->
            pollWidgetModel.submitVote(textPoll.options!![0].id) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        println("Text Poll First Option Selected")
        pollWidgetModel.voteResultFlow.test {
            var item = awaitItem()
            while (item?.choices?.get(0)?.voteCount != 1) item = awaitItem()
            assert(item.choices?.get(0)?.voteCount == 1)
            assert(item.choices?.get(1)?.voteCount == 0)
        }
        //Change to second option vote
        suspendCoroutine { cont ->
            pollWidgetModel.submitVote(textPoll.options!![1].id) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        println("Text Poll Second Option Selected")
        pollWidgetModel.voteResultFlow.test {
            var item = awaitItem()
            while (item?.choices?.get(1)?.voteCount != 1) {
                item = awaitItem()
                println("item:${item?.choices?.get(1)?.voteCount} >> ${item?.choices?.get(0)?.voteCount}")
            }
            assert(item!!.choices?.get(1)?.voteCount == 1)
            assert(item!!.choices?.get(0)?.voteCount == 0)
        }
        println("Published widget text Poll")
        val publishWidgetResponse = suspendCoroutine { cont ->
            widgetClient.publishWidget(
                PublishWidgetRequest(
                    WidgetType.TEXT_POLL, textPoll.id, publishDelay = null, programDateTime = null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        println("Published widget Image Poll")
        val publishWidgetResponse1 = suspendCoroutine { cont ->
            widgetClient.publishWidget(
                PublishWidgetRequest(
                    WidgetType.IMAGE_POLL, imagePoll.id, publishDelay = null, programDateTime = null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        suspendCoroutine<LiveLikeEmptyResponse> { cont ->
            widgetClient.deleteWidget(
                DeleteWidgetRequest(
                    WidgetType.TEXT_POLL, textPoll.id
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        suspendCoroutine<LiveLikeEmptyResponse> { cont ->
            widgetClient.deleteWidget(
                DeleteWidgetRequest(
                    WidgetType.IMAGE_POLL, imagePoll.id
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
    }

    @Test
    fun testQuiz() = runTest(timeout = 2.minutes) {
        val textQuiz = suspendCoroutine { cont ->
            widgetClient.createTextQuiz(
                CreateTextQuizRequest(
                    question = "Test Quiz",
                    timeout = "P0DT00H00M20S",
                    programId = programId,
                    choices = listOf(
                        CreateTextQuizRequest.Choice("option 1", false),
                        CreateTextQuizRequest.Choice("option 2", true),
                    ),
                    sponsorIds = sponsorIds
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(textQuiz.question == "Test Quiz")
        assert(textQuiz.programId == programId)
        assert(textQuiz.choices?.size == 2)
        assert(textQuiz.choices?.get(0)?.description == "option 1")
        assert(textQuiz.choices?.get(0)?.isCorrect == false)

        assert(textQuiz.choices?.get(1)?.description == "option 2")
        assert(textQuiz.choices?.get(1)?.isCorrect == true)

        assert(textQuiz.sponsors?.size == 1 && textQuiz.sponsors!![0].id == "f02950ab-5c1b-4303-b6e3-40a9f12e7260")

        val imageQuiz = suspendCoroutine { cont ->
            widgetClient.createImageQuiz(
                CreateImageQuizRequest(
                    question = "Test Quiz",
                    timeout = "P0DT00H00M20S",
                    programId = programId,
                    choices = listOf(
                        CreateImageQuizRequest.Choice(
                            "option 1",
                            false,
                            "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg"
                        ),
                        CreateImageQuizRequest.Choice(
                            "option 2",
                            true,
                            "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg"
                        ),
                    ),
                    sponsorIds = sponsorIds
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        assert(imageQuiz.question == "Test Quiz")
        assert(imageQuiz.programId == programId)
        assert(imageQuiz.choices?.size == 2)
        assert(imageQuiz.choices?.get(0)?.description == "option 1")
        assert(imageQuiz.choices?.get(0)?.isCorrect == false)
        assert(imageQuiz.choices?.get(0)?.imageUrl == "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")

        assert(imageQuiz.choices?.get(1)?.description == "option 2")
        assert(imageQuiz.choices?.get(1)?.isCorrect == true)
        assert(imageQuiz.choices?.get(1)?.imageUrl == "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")

        assert(imageQuiz.sponsors?.size == 1 && textQuiz.sponsors!![0].id == "f02950ab-5c1b-4303-b6e3-40a9f12e7260")
        val publishWidgetResponse = suspendCoroutine { cont ->
            widgetClient.publishWidget(
                PublishWidgetRequest(
                    WidgetType.TEXT_QUIZ, textQuiz.id, publishDelay = null, programDateTime = null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        val publishWidgetResponse1 = suspendCoroutine { cont ->
            widgetClient.publishWidget(
                PublishWidgetRequest(
                    WidgetType.IMAGE_QUIZ, imageQuiz.id, publishDelay = null, programDateTime = null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }


        // Voting
        val inCorrectChoiceId = textQuiz.choices?.get(0)?.id!!
        val correctChoiceId = textQuiz.choices?.get(1)?.id!!
        val quizWidgetModel: QuizWidgetModel =
            session.getWidgetModelFromLiveLikeWidget(textQuiz) as QuizWidgetModel

        suspendCoroutine { cont ->
            quizWidgetModel.lockInAnswer(inCorrectChoiceId) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        quizWidgetModel.voteResultFlow.test {
            var item = awaitItem()
            while (item == null) item = awaitItem()
            item.choices?.forEach {
                println("Choice1: ${it.id}> ${it.answerCount}> ${it.isCorrect}")
            }
            assert(item.choices?.get(0)?.answerCount == 1)
            assert(item.choices?.get(1)?.answerCount == 0)
            assert(item.choices?.get(0)?.isCorrect == false)
            assert(item.choices?.get(1)?.isCorrect == true)
        }

        suspendCoroutine { cont ->
            quizWidgetModel.loadInteractionHistory { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        val interactionHistory = suspendCoroutine { cont ->
            quizWidgetModel.loadInteractionHistory { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(interactionHistory.size == 1)
        assert(interactionHistory[0].choiceId == inCorrectChoiceId)

        //Check multiple vote
        suspendCoroutine { cont ->
            quizWidgetModel.lockInAnswer(correctChoiceId) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        quizWidgetModel.voteResultFlow.test {
            var item = awaitItem()
            while (item?.choices?.get(1)?.answerCount != 1) {
                item = awaitItem()
            }
            item?.choices?.forEach {
                println("Choice2: ${it.id}> ${it.answerCount}> ${it.isCorrect}")
            }
            //assert(item!!.choices?.get(0)?.answerCount == 0)
           // assert(item!!.choices?.get(1)?.answerCount == 1)
        }

        suspendCoroutine { cont ->
            quizWidgetModel.loadInteractionHistory { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        val interactionHistory1 = suspendCoroutine { cont ->
            quizWidgetModel.loadInteractionHistory { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(interactionHistory1.size == 1)
        println("IN>>${interactionHistory1[0].choiceId}")
        assert(interactionHistory1[0].choiceId == correctChoiceId)

        suspendCoroutine<LiveLikeEmptyResponse> { cont ->
            widgetClient.deleteWidget(
                DeleteWidgetRequest(
                    WidgetType.TEXT_QUIZ, textQuiz.id
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        suspendCoroutine<LiveLikeEmptyResponse> { cont ->
            widgetClient.deleteWidget(
                DeleteWidgetRequest(
                    WidgetType.IMAGE_QUIZ, imageQuiz.id
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

    }

    @Test
    fun testPrediction() = runTest(timeout = 1.minutes) {
        val textPrediction = suspendCoroutine { cont ->
            widgetClient.createTextPrediction(
                CreateTextPredictionRequest(
                    question = "Test Text Prediction",
                    timeout = "P0DT00H00M20S",
                    programId = programId,
                    options = listOf(
                        CreateTextPredictionRequest.Option("op1"),
                        CreateTextPredictionRequest.Option("op2")
                    ),
                    confirmationMessage = "Thanks",
                    sponsorIds = sponsorIds
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(textPrediction.question == "Test Text Prediction")
        assert(textPrediction.programId == programId)
        assert(textPrediction.options?.size == 2)
        assert(textPrediction.confirmationMessage == "Thanks")
        assert(textPrediction.sponsors?.size == 1 && textPrediction.sponsors!![0].id == "f02950ab-5c1b-4303-b6e3-40a9f12e7260")

        val textPredictionOption1Correct = suspendCoroutine { cont ->
            widgetClient.updateTextPredictionOption(
                UpdateTextPredictionOptions(
                    textPrediction.id, textPrediction.options!![0].id, null, true, null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        val textPredictionOption2Correct = suspendCoroutine { cont ->
            widgetClient.updateTextPredictionOption(
                UpdateTextPredictionOptions(
                    textPrediction.id, textPrediction.options!![1].id, null, true, null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        val predictionWidgetModel: PredictionWidgetViewModel =
            session.getWidgetModelFromLiveLikeWidget(textPrediction) as PredictionWidgetViewModel
        predictionWidgetModel.lockInVote(textPrediction.options!![0].id)
        predictionWidgetModel.voteResultsFlow.test {
            var item = awaitItem()
            while (item?.choices?.get(0)?.voteCount != 1) {
                item = awaitItem()
            }
            item.choices?.forEach {
                println("CC>>${it.voteCount}> ${it.id}")
            }
            println("MIO")
            assert(item.choices!![0].voteCount == 1)
        }
        println("MIOOKKL")
        val predictionInteractionHistory = suspendCoroutine { cont ->
            predictionWidgetModel.loadInteractionHistory { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(predictionInteractionHistory[0].optionId == textPrediction.options!![0].id && predictionInteractionHistory[0].isCorrect)

        val followUpTextPredictionWidget = suspendCoroutine { cont ->
            widgetClient.getWidget(
                GetWidgetRequest(
                    textPrediction.followUps!![0].id, WidgetType.TEXT_PREDICTION_FOLLOW_UP
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(followUpTextPredictionWidget.options?.get(0)?.isCorrect == true)
        assert(followUpTextPredictionWidget.options?.get(1)?.isCorrect == true)


        val imagePrediction = suspendCoroutine { cont ->
            widgetClient.createImagePrediction(
                CreateImagePredictionRequest(
                    question = "Test Image Prediction",
                    timeout = "P0DT00H00M20S",
                    programId = programId,
                    options = listOf(
                        CreateImagePredictionRequest.Option(
                            "op1",
                            "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg"
                        ), CreateImagePredictionRequest.Option(
                            "op2",
                            "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg"
                        )
                    ),
                    confirmationMessage = "Thanks",
                    sponsorIds = sponsorIds
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(imagePrediction.question == "Test Image Prediction")
        assert(imagePrediction.programId == programId)
        assert(imagePrediction.options?.size == 2 && imagePrediction.options!![0].imageUrl != null)
        assert(imagePrediction.confirmationMessage == "Thanks")
        assert(imagePrediction.sponsors?.size == 1 && textPrediction.sponsors!![0].id == "f02950ab-5c1b-4303-b6e3-40a9f12e7260")


        val imagePredictionOption1Correct = suspendCoroutine { cont ->
            widgetClient.updateImagePredictionOption(
                UpdateImagePredictionOptions(
                    imagePrediction.id, imagePrediction.options!![0].id, null, true, null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        val imagePredictionOption2Correct = suspendCoroutine { cont ->
            widgetClient.updateImagePredictionOption(
                UpdateImagePredictionOptions(
                    imagePrediction.id, imagePrediction.options!![1].id, null, true, null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        val followUpImagePredictionWidget = suspendCoroutine { cont ->
            widgetClient.getWidget(
                GetWidgetRequest(
                    imagePrediction.followUps!![0].id, WidgetType.IMAGE_PREDICTION_FOLLOW_UP
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(followUpImagePredictionWidget.options?.get(0)?.isCorrect == true)
        assert(followUpImagePredictionWidget.options?.get(1)?.isCorrect == true)
        println("IUHJYYY")
        suspendCoroutine { cont ->
            widgetClient.publishWidget(
                PublishWidgetRequest(
                    WidgetType.TEXT_PREDICTION,
                    textPrediction.id,
                    publishDelay = null,
                    programDateTime = null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        suspendCoroutine { cont ->
            widgetClient.publishWidget(
                PublishWidgetRequest(
                    WidgetType.IMAGE_PREDICTION,
                    imagePrediction.id,
                    publishDelay = null,
                    programDateTime = null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        suspendCoroutine { cont ->
            widgetClient.publishWidget(
                PublishWidgetRequest(
                    WidgetType.TEXT_PREDICTION_FOLLOW_UP,
                    followUpTextPredictionWidget.id,
                    publishDelay = null,
                    programDateTime = null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        suspendCoroutine { cont ->
            widgetClient.publishWidget(
                PublishWidgetRequest(
                    WidgetType.IMAGE_PREDICTION_FOLLOW_UP,
                    followUpImagePredictionWidget.id,
                    publishDelay = null,
                    programDateTime = null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        println("MIHHH54")
        suspendCoroutine { cont ->
            widgetClient.deleteWidget(
                DeleteWidgetRequest(
                    WidgetType.TEXT_PREDICTION, textPrediction.id
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        println("MIHHH4")
        suspendCoroutine { cont ->
            widgetClient.deleteWidget(
                DeleteWidgetRequest(
                    WidgetType.IMAGE_PREDICTION, imagePrediction.id
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        println("MIHHH3")
        suspendCoroutine { cont ->
            widgetClient.deleteWidget(
                DeleteWidgetRequest(
                    WidgetType.TEXT_PREDICTION_FOLLOW_UP, followUpTextPredictionWidget.id
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        println("MIHHH2")
        suspendCoroutine<LiveLikeEmptyResponse> { cont ->
            widgetClient.deleteWidget(
                DeleteWidgetRequest(
                    WidgetType.IMAGE_PREDICTION_FOLLOW_UP, followUpImagePredictionWidget.id
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        println("MIHHH")
    }

    @Test
    fun testImageNumberPrediction() = runTest {
        val prediction = suspendCoroutine { cont ->
            widgetClient.createImageNumberPrediction(
                CreateImageNumberPredictionRequest(
                    question = "Test Image Number Prediction",
                    timeout = "P0DT00H00M20S",
                    programId = programId,
                    options = listOf(
                        CreateImageNumberPredictionRequest.Option(
                            1,
                            "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg",
                            "Option 1"
                        ), CreateImageNumberPredictionRequest.Option(
                            1,
                            "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg",
                            "Option 2"
                        )
                    ),
                    confirmationMessage = "Thanks",
                    sponsorIds = sponsorIds
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(prediction.question == "Test Image Number Prediction")
        assert(prediction.programId == programId)
        assert(prediction.confirmationMessage == "Thanks")
        assert(prediction.options?.size == 2)
        assert(prediction.options!![0].correctNumber == 1)
        assert(prediction.options!![1].correctNumber == 1)
        assert(prediction.sponsors?.size == 1 && prediction.sponsors!![0].id == "f02950ab-5c1b-4303-b6e3-40a9f12e7260")


        val option1Update = suspendCoroutine { cont ->
            widgetClient.updateImageNumberPredictionOption(
                UpdateImageNumberPredictionOptionRequest(
                    prediction.id, prediction.options!![0].id, 5, null, null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        val option2Update = suspendCoroutine { cont ->
            widgetClient.updateImageNumberPredictionOption(
                UpdateImageNumberPredictionOptionRequest(
                    prediction.id, prediction.options!![1].id, 12, null, null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }


        suspendCoroutine { cont ->
            widgetClient.publishWidget(
                PublishWidgetRequest(
                    WidgetType.IMAGE_NUMBER_PREDICTION,
                    prediction.id,
                    publishDelay = null,
                    programDateTime = null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        val widgetModel: NumberPredictionWidgetModel =
            session.getWidgetModelFromLiveLikeWidget(prediction) as NumberPredictionWidgetModel

        val votes = suspendCoroutine { cont ->
            widgetModel.lockInVote(
                listOf(
                    NumberPredictionVotes(prediction.options!![0].id, 5),
                    NumberPredictionVotes(prediction.options!![1].id, 12)
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        val followUpImageNumberPredictionWidget = suspendCoroutine { cont ->
            widgetClient.getWidget(
                GetWidgetRequest(
                    prediction.followUps!![0].id, WidgetType.IMAGE_NUMBER_PREDICTION_FOLLOW_UP
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(followUpImageNumberPredictionWidget.options!![0].correctNumber == 5)
        assert(followUpImageNumberPredictionWidget.options!![1].correctNumber == 12)
        suspendCoroutine { cont ->
            widgetClient.publishWidget(
                PublishWidgetRequest(
                    WidgetType.IMAGE_NUMBER_PREDICTION_FOLLOW_UP,
                    followUpImageNumberPredictionWidget.id,
                    publishDelay = null,
                    programDateTime = null
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        val interactions = suspendCoroutine { cont ->
            widgetModel.loadInteractionHistory { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(interactions[0].votes?.size == 2)
        assert(interactions[0].votes?.find { it.optionId == prediction.options!![0].id }?.number == 5)
        assert(interactions[0].votes?.find { it.optionId == prediction.options!![1].id }?.number == 12)


        suspendCoroutine { cont ->
            widgetClient.deleteWidget(
                DeleteWidgetRequest(
                    WidgetType.IMAGE_NUMBER_PREDICTION, prediction.id
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        suspendCoroutine<LiveLikeEmptyResponse> { cont ->
            widgetClient.deleteWidget(
                DeleteWidgetRequest(
                    WidgetType.IMAGE_NUMBER_PREDICTION_FOLLOW_UP,
                    followUpImageNumberPredictionWidget.id
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
    }

    @Test
    fun testTextAsk() = runTest(timeout = 20.seconds) {
        val textAsk = suspendCoroutine { cont ->
            widgetClient.createTextAsk(
                CreateTextAskRequest(
                    title = "Test Text Ask",
                    timeout = "P0DT00H00M20S",
                    programId = programId,
                    confirmationMessage = "Thanks",
                    prompt = "Test Prompt",
                    sponsorIds = sponsorIds
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(textAsk.title == "Test Text Ask")
        assert(textAsk.programId == programId)
        assert(textAsk.confirmationMessage == "Thanks")
        assert(textAsk.prompt == "Test Prompt")
        assert(textAsk.sponsors?.size == 1 && textAsk.sponsors!![0].id == "f02950ab-5c1b-4303-b6e3-40a9f12e7260")

        val widgetModel: TextAskWidgetModel =
            session.getWidgetModelFromLiveLikeWidget(textAsk) as TextAskWidgetModel
        val reply = suspendCoroutine { cont ->
            widgetModel.submitReply("Reply") { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(reply.text == "Reply")
        assert(reply.widgetId == textAsk.id)
        val widget = suspendCoroutine { cont ->
            widgetClient.getWidget(
                GetWidgetRequest(
                    textAsk.id, WidgetType.TEXT_ASK
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(widget.replyCount == 1)
    }

    @Test
    fun testRichPost() = runTest {
        val content =
            "<p>Hii,<br><br><u>Testing</u> the rich post from <b>android<br></b></p><p><b><br></b></p><ol><li><b>Testing</b></li><li><b><br></b></li></ol><p><span style=\\\"font-family: Impact;\\\">\uFEFFTesting</span></p><p><span style=\\\"font-family: Impact;\\\"><br></span></p><p><span style=\\\"font-family: &quot;Times New Roman&quot;;\\\">\uFEFF</span><span style=\\\"font-family: Impact;\\\"><br></span></p><ul><li><span style=\\\"font-family: Impact;\\\"><span style=\\\"font-family: &quot;Comic Sans MS&quot;;\\\">Testing</span><br></span><b><br></b></li></ul>"
        val richPost = suspendCoroutine { cont ->
            widgetClient.createRichPost(
                CreateRichPostRequest(
                    content,
                    programId,
//                    sponsorIds = sponsorIds
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(richPost.programId == programId)
        //TODO: Uncomment post backend fix
//        assert(richPost.content == content)
//        println("Sponsors: ${richPost.sponsors?.size}")
//        assert(richPost.sponsors?.size == 1 && richPost.sponsors!![0].id == "f02950ab-5c1b-4303-b6e3-40a9f12e7260")

    }
}