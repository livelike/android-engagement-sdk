package com.livelike.end2end

import com.livelike.common.AccessTokenDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.engagementsdk.EpochTime
import com.livelike.engagementsdk.createContentSession
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.engagementsdk.widget.WidgetType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class LiveLikeContentSessionEnd2EndTest {
    private val testDispatcher = StandardTestDispatcher()
    private lateinit var clientId: String
    private lateinit var producerToken: String
    private lateinit var programId: String
    private val accessToken = null

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        producerToken = System.getenv("producerToken")
        clientId = System.getenv("clientId")
        programId = System.getenv("programId")
    }

    @After
    fun tearDown() {
        testDispatcher.cancel()
    }

    @Test
    fun testGetWidgetInteractions() = runTest {
        val liveLikeKotlin = LiveLikeKotlin(
            clientId,
            accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return accessToken
                }

                override fun storeAccessToken(accessToken: String?) {

                }

            })
        val errorDelegate = object : ErrorDelegate() {
            override fun onError(error: String) {

            }
        }
        val session = liveLikeKotlin.createContentSession(programId, { EpochTime(0L) }, errorDelegate)
        val interactions = suspendCoroutine { cont ->
            session.getWidgetInteractions(listOf(WidgetType.TEXT_POLL.getKindName())) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(Exception(it)) }
            }
        }
        assert(interactions.isNotEmpty())
    }
}