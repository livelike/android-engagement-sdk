package com.livelike.end2end

import app.cash.turbine.test
import com.google.gson.JsonObject
import com.livelike.common.AccessTokenDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.engagementsdk.EpochTime
import com.livelike.engagementsdk.createContentSession
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import kotlin.time.Duration.Companion.minutes


@OptIn(kotlinx.coroutines.ExperimentalCoroutinesApi::class)
class LiveLikePubnubEnd2EndTest {
    private val testDispatcher = StandardTestDispatcher()
    private lateinit var clientId: String
    private lateinit var producerToken: String
    private lateinit var programId: String
    private val accessToken = null
    private val message: JsonObject = JsonObject()
    private val channel = "embetTest"

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        producerToken = System.getenv("producerToken")
        clientId = System.getenv("clientId")
        programId = System.getenv("programId")

        message.addProperty("name", "Embet")
        message.addProperty("age", "1")
    }

    @After
    fun tearDown() {
        testDispatcher.cancel()
    }

    @Test
    fun testSubsribeToChannelPubNub() = runTest(testDispatcher, timeout = 1.minutes) {
        val liveLikeKotlin = LiveLikeKotlin(
            clientId,
            accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return accessToken
                }

                override fun storeAccessToken(accessToken: String?) {

                }

            },
            networkDispatcher = testDispatcher,
            sdkDispatcher = testDispatcher,
            mainDispatcher = testDispatcher
        )

        val errorDelegate = object : ErrorDelegate() {
            override fun onError(error: String) {

            }
        }

        val session =
            liveLikeKotlin.createContentSession(
                programId,
                { EpochTime(0L) },
                errorDelegate,
                connectToDefaultChatRoom = false
            )



        delay(3000)
        session.subscribeToChannels(listOf(channel))?.shareIn(this, SharingStarted.Eagerly)
            ?.test {
                awaitComplete()
                val it = awaitItem()
                assert(it.channel == channel)
                Assert.assertEquals(it.payload, message)

            }


        val pubData = liveLikeKotlin.sdkConfigurationOnce.invoke(false)
        val pubKey = pubData.pubnubPublishKey
        val subKey = pubData.pubNubKey

        val url = "https://ps.pndsn.com/publish/$pubKey/$subKey/0/$channel/0"
        val headers = listOf("Accept" to "application/json", "Content-Type" to "application/json")
        val body = JsonObject().apply { add("payload", message) }.toString()

        liveLikeKotlin.networkClient.post(url = url, body = body, headers = headers)



    }
}