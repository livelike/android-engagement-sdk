package com.livelike.widget

import org.threeten.bp.Duration
import org.threeten.bp.format.DateTimeParseException

fun parseDuration(durationString: String): Long {
    var timeout = 7000L
    try {
        timeout = Duration.parse(durationString).toMillis()
    } catch (e: DateTimeParseException) {
        println( "Duration $durationString can't be parsed." )
    }
    return timeout
}