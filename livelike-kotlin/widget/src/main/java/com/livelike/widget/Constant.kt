package com.livelike.widget

//url templates
internal const val TEMPLATE_PROGRAM_ID = "{program_id}"
internal const val TEMPLATE_LEADER_BOARD_ID = "{leaderboard_id}"
internal const val TEMPLATE_PROFILE_ID = "{profile_id}"

internal const val TEMPLATE_WIDGET_ID = "{id}"
internal const val TEMPLATE_WIDGET_KIND = "{kind}"

internal const val SPONSOR_URL_NOT_FOUND_ERROR = "Sponsor Url not found"
internal const val INVALID_PROGRAM_ID = "invalid program ID"
internal const val LEADERBOARD_URL_NOT_FOUND_ERROR = "LeaderBoard Url is Null"

internal const val PROGRAM_TIMELINE_URL_ERROR = "Program TimeLine Url is Null"
internal const val PROGRAM_WIDGETS_URL_ERROR = "Program Widgets Url is Null"

//errors
internal const val NO_CURRENT_USER = "No Current User"
internal const val RECYCLER_NO_POSITION: Int = -1
internal const val WIDGET_ATTRIBUTES = "widget_attribute"