package com.livelike.widget

import com.livelike.BaseClient
import com.livelike.common.LiveLikeCallback
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.utils.validateUuid
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.WidgetsRequestParameters
import com.livelike.engagementsdk.getWidgetDetailUrl
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.data.models.PublishedWidgetListResponse
import com.livelike.engagementsdk.widget.data.respository.WidgetInteractionRepository
import com.livelike.engagementsdk.widget.model.Option
import com.livelike.engagementsdk.widget.repository.ProgramRepository
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import com.livelike.utils.LiveLikeException
import com.livelike.utils.Once
import kotlinx.coroutines.CoroutineScope
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

internal class InternalLiveLikeWidgetClient(
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    sdkScope: CoroutineScope,
    uiScope: CoroutineScope,
    private val networkApiClient: NetworkApiClient,
) : BaseClient(configurationOnce, currentProfileOnce, sdkScope, uiScope), LiveLikeWidgetClient {

    private var publishedWidgetListResponse: PublishedWidgetListResponse? = null
    lateinit var widgetInteractionRepository: WidgetInteractionRepository

    override fun publishWidget(
        request: PublishWidgetRequest,
        liveLikeCallback: LiveLikeCallback<PublishWidgetResponse>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val widget = suspendCoroutine { cont ->
                getWidget(GetWidgetRequest(request.id, request.type)) { result, error ->
                    result?.let { cont.resume(it) }
                    error?.let { cont.resumeWithException(LiveLikeException(it)) }
                }
            }
            networkApiClient.put(
                widget.scheduleUrl ?: throw LiveLikeException("No Schedule Url"),
                request.toJsonString(),
                pair.first.accessToken
            ).processResult()
        }
    }

    override fun deleteWidget(
        request: DeleteWidgetRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val widget = suspendCoroutine { cont ->
                getWidget(GetWidgetRequest(request.id, request.type)) { result, error ->
                    result?.let { cont.resume(it) }
                    error?.let { cont.resumeWithException(LiveLikeException(it)) }
                }
            }
            networkApiClient.delete(widget.url, accessToken = pair.first.accessToken).processResult()
        }
    }

    override fun getWidget(
        request: GetWidgetRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            getWidgetDetails(pair, request)
        }
    }

    private suspend fun getWidgetDetails(
        pair: Pair<LiveLikeProfile, SdkConfiguration>,
        request: GetWidgetRequest,
    ): LiveLikeWidget =
        networkApiClient.get(
            pair.second.getWidgetDetailUrl(request.id, request.type),
            accessToken = pair.first.accessToken
        ).processResult()


    private suspend fun createWidget(
        request: Any,
        url: String,
        accessToken: String
    ): LiveLikeWidget = networkApiClient.post(url, request.toJsonString(), accessToken)
        .processResult()


    override fun createTextPoll(
        request: CreateTextPollRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            createWidget(request, pair.second.textPollUrl, pair.first.accessToken)
        }
    }

    override fun createImagePoll(
        request: CreateImagePollRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            createWidget(request, pair.second.imagePollUrl, pair.first.accessToken)
        }
    }

    override fun createAlert(
        request: CreateAlertRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            createWidget(request, pair.second.alertsUrl, pair.first.accessToken)
        }
    }

    override fun createTextPrediction(
        request: CreateTextPredictionRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            createWidget(request, pair.second.textPredictionUrl, pair.first.accessToken)
        }
    }

    override fun createImagePrediction(
        request: CreateImagePredictionRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            createWidget(request, pair.second.imagePredictionUrl, pair.first.accessToken)
        }
    }

    override fun updateTextPredictionOption(
        request: UpdateTextPredictionOptions,
        liveLikeCallback: LiveLikeCallback<Option>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            updatePredictionFollowUp(
                request.predictionId,
                WidgetType.TEXT_PREDICTION,
                request.optionId,
                request.rewardItemId,
                request.isCorrect,
                request.rewardItemAmount,
                pair.first.accessToken
            )
        }
    }

    override fun updateImagePredictionOption(
        request: UpdateImagePredictionOptions,
        liveLikeCallback: LiveLikeCallback<Option>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            updatePredictionFollowUp(
                request.predictionId,
                WidgetType.IMAGE_PREDICTION,
                request.optionId,
                request.rewardItemId,
                request.isCorrect,
                request.rewardItemAmount,
                pair.first.accessToken
            )
        }
    }

    override fun createPredictionFollowUpWidget(
        request: CreatePredictionFollowUpWidgetRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val widget =
                getWidgetDetails(pair, GetWidgetRequest(request.widgetId, request.widgetType))
            widget.followUpUrl?.let {
                val widgetIDPropName = when (request.widgetType) {
                    WidgetType.TEXT_PREDICTION -> "text_prediction_id"
                    WidgetType.IMAGE_PREDICTION -> "image_prediction_id"
                    else -> throw LiveLikeException("Widget Type Incorrect")
                }
                networkApiClient.post(
                    it,
                    mapOf(
                        widgetIDPropName to request.widgetId,
                        "correct_option_id" to request.correctOptionId
                    ).toJsonString(),
                    pair.first.accessToken
                ).processResult()
            } ?: throw LiveLikeException("No Follow up url found")
        }
    }

    override fun createTextQuiz(
        request: CreateTextQuizRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            createWidget(request, pair.second.textQuizUrl, pair.first.accessToken)
        }
    }

    override fun createImageQuiz(
        request: CreateImageQuizRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            createWidget(request, pair.second.imageQuizUrl, pair.first.accessToken)
        }
    }


    override fun createRichPost(
        request: CreateRichPostRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            createWidget(request, pair.second.richPostsUrl, pair.first.accessToken)
        }
    }


    override fun createCheerMeter(
        request: CreateCheerMeterRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            createWidget(request, pair.second.cheerMeterUrl, pair.first.accessToken)
        }
    }

    /*
    override fun createSocialEmbed(
        request: CreateSocialEmbedRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
//            createWidget(request, pair.second.soc, pair.first.accessToken)
            throw LiveLikeException("No Implementation")
        }
    }

    override fun createImageSlider(
        request: CreateImageSliderRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            createWidget(request, pair.second.emojiSliderUrl, pair.first.accessToken)
        }
    }

    override fun createVideoAlert(
        request: CreateVideoAlertRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            createWidget(request, pair.second.textQuizUrl, pair.first.accessToken)
//            throw LiveLikeException("No Implementation")
        }
    }
*/
    override fun createImageNumberPrediction(
        request: CreateImageNumberPredictionRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            createWidget(request, pair.second.imageNumberPredictionUrl, pair.first.accessToken)
        }
    }

    override fun updateImageNumberPredictionOption(
        request: UpdateImageNumberPredictionOptionRequest,
        liveLikeCallback: LiveLikeCallback<Option>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val getWidgetRequest = GetWidgetRequest(
                request.imageNumberPredictionId,
                WidgetType.IMAGE_NUMBER_PREDICTION
            )
            val widget = getWidgetDetails(pair, getWidgetRequest)

            networkApiClient.patch(
                widget.options?.find { it.id == request.optionId }?.url
                    ?: throw LiveLikeException("No Option Url found"),
                body = request.toJsonString(),
                accessToken = pair.first.accessToken
            ).processResult()

        }
    }

    override fun createImageNumberPredictionFollowUp(
        request: CreateImageNumberPredictionFollowUpRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            createWidget(
                request,
                pair.second.imageNumberPredictionFollowUpsUrl,
                pair.first.accessToken
            )
        }
    }

    override fun createTextAsk(
        request: CreateTextAskRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            createWidget(request, pair.second.textAskUrl, pair.first.accessToken)
        }
    }

    //this will only be fetching the first page (pagination support has not been added)
    override fun getWidgets(
        programId: String,
        requestParams: WidgetsRequestParameters?,
        liveLikeCallback: LiveLikeCallback<List<LiveLikeWidget>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            if (!validateUuid(programId)) {
                throw LiveLikeException(INVALID_PROGRAM_ID)
            }
            val programRepository =
                ProgramRepository(programId, currentProfileOnce, networkApiClient)
            val result = programRepository.getProgramData(pair.second.programDetailUrlTemplate)
            result.widgetsUrl?.let { url ->
                // fetch widgets
                val params = arrayListOf<Pair<String, String>>()
                requestParams?.run {
                    widgetStatus?.let {
                        params.add("status" to it.parameterValue)
                    }
                    ordering?.let {
                        params.add("ordering" to it.parameterValue)
                    }
                    for (type in widgetTypeFilter) {
                        params.add("kind" to type.getKindName())
                    }
                    interactive?.let {
                        params.add("interactive" to it.toString())
                    }
                    since?.let {
                        params.add("since" to it)
                    }
                }
                buildWidgetList(programId, url, params)
            } ?: throw LiveLikeException(PROGRAM_WIDGETS_URL_ERROR)
        }
    }


    private suspend fun buildWidgetList(
        programId: String,
        url: String,
        params: List<Pair<String, String>> = emptyList()
    ): List<LiveLikeWidget> {
        publishedWidgetListResponse = networkApiClient.get(url, queryParameters = params).processResult()

        widgetInteractionRepository = WidgetInteractionRepository(
            programID = programId,
            currentProfileOnce = currentProfileOnce,
            configurationOnce = configurationOnce,
            networkApiClient,
        )

        publishedWidgetListResponse?.results?.let {
            if (it.isNotEmpty()) {
                currentProfileOnce().let { user ->
                    widgetInteractionRepository.fetchAndStoreWidgetInteractions(
                        publishedWidgetListResponse?.widgetInteractionsUrlTemplate?.replace(
                            TEMPLATE_PROFILE_ID, user.id
                        ) ?: "", user.accessToken
                    )
                }
            }
        }

        return publishedWidgetListResponse?.results?.filter {
            var widgetType = it.kind
            widgetType = if (widgetType.contains("follow-up")) {
                "$widgetType-updated"
            } else {
                "$widgetType-created"
            }
            WidgetType.fromString(widgetType) != null
        } ?: emptyList()
    }



    private suspend fun updatePredictionFollowUp(
        predictionId: String,
        widgetType: WidgetType,
        optionId: String,
        rewardItemId: String?,
        isCorrect: Boolean?,
        rewardItemAmount: Double?,
        accessToken: String
    ): Option {
        val prediction = suspendCoroutine { cont ->
            getWidget(
                GetWidgetRequest(
                    predictionId,
                    widgetType
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(Exception(it)) }
            }
        }
        val result: Option = prediction.options?.find { it.id == optionId }?.let {
            networkApiClient.patch(
                it.url,
                it.copy(
                    isCorrect = isCorrect,
                    rewardItemId = rewardItemId,
                    rewardItemAmount = rewardItemAmount
                ).toJsonString(),
                accessToken
            )
                .processResult()
        } ?: throw LiveLikeException("No Prediction Option Url")
        return result
    }
}