package com.livelike.widget

import com.google.gson.annotations.SerializedName
import com.livelike.common.LiveLikeCallback
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.WidgetsRequestParameters
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.model.Option

interface LiveLikeWidgetClient {
    fun publishWidget(
        request: PublishWidgetRequest, liveLikeCallback: LiveLikeCallback<PublishWidgetResponse>
    )

    fun deleteWidget(
        request: DeleteWidgetRequest, liveLikeCallback: LiveLikeCallback<LiveLikeEmptyResponse>
    )

    fun getWidget(
        request: GetWidgetRequest, liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    )

    fun createTextPoll(
        request: CreateTextPollRequest, liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    )

    fun createImagePoll(
        request: CreateImagePollRequest, liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    )

    fun createAlert(request: CreateAlertRequest, liveLikeCallback: LiveLikeCallback<LiveLikeWidget>)

    fun createTextPrediction(
        request: CreateTextPredictionRequest, liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    )

    fun createImagePrediction(
        request: CreateImagePredictionRequest, liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    )

    fun updateTextPredictionOption(
        request: UpdateTextPredictionOptions, liveLikeCallback: LiveLikeCallback<Option>
    )

    fun updateImagePredictionOption(
        request: UpdateImagePredictionOptions, liveLikeCallback: LiveLikeCallback<Option>
    )

    fun createPredictionFollowUpWidget(
        request: CreatePredictionFollowUpWidgetRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    )

    fun createTextQuiz(
        request: CreateTextQuizRequest, liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    )

    fun createImageQuiz(
        request: CreateImageQuizRequest, liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    )

    fun createRichPost(
        request: CreateRichPostRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    )

    fun createCheerMeter(
        request: CreateCheerMeterRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    )

    /*
    fun createSocialEmbed(
        request: CreateSocialEmbedRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    )

    fun createImageSlider(
        request: CreateImageSliderRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    )

    fun createVideoAlert(
        request: CreateVideoAlertRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    )*/

    fun createImageNumberPrediction(
        request: CreateImageNumberPredictionRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    )

    fun updateImageNumberPredictionOption(
        request: UpdateImageNumberPredictionOptionRequest,
        liveLikeCallback: LiveLikeCallback<Option>
    )

    fun createImageNumberPredictionFollowUp(
        request: CreateImageNumberPredictionFollowUpRequest,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    )

    fun createTextAsk(
        request: CreateTextAskRequest, liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    )


    fun getWidgets(
        programId:String,
        requestParams: WidgetsRequestParameters?,
        liveLikeCallback: LiveLikeCallback<List<LiveLikeWidget>>
    )

}

data class CreateImageNumberPredictionFollowUpRequest(
    @SerializedName("image_number_prediction_id") val imageNumberPredictionId: String,
    @SerializedName("sponsor_ids") val sponsorIds: List<String>?,
)

data class UpdateImageNumberPredictionOptionRequest(
    val imageNumberPredictionId: String,
    val optionId: String,
    @SerializedName("correct_number") val correctNumber: Int,
    @SerializedName("reward_item_id") val rewardItemId: String?,
    @SerializedName("reward_item_amount") val rewardItemAmount: Double?,
)


data class GetWidgetRequest(
    val id: String,
    val type: WidgetType,
)

data class PublishWidgetResponse(
    @SerializedName("status") var status: String,
    @SerializedName("scheduled_at") var scheduledAt: String,
    @SerializedName("published_at") var publishedAt: String,
    @SerializedName("publish_delay") var publishDelay: String,
    @SerializedName("program_date_time") var programDateTime: String? = null
)

data class DeleteWidgetRequest(
    val type: WidgetType,
    val id: String,
)

data class PublishWidgetRequest(
    val type: WidgetType,
    val id: String,
    val publishDelay: String?,
    val programDateTime: String?,
)

data class CreateTextPollRequest(
    val question: String,
    val options: List<Option>,
    val timeout: String,
    @SerializedName("program_id") val programId: String,
    @SerializedName("custom_data") val customData: String? = null,
    @SerializedName("program_date_time") val programDateTime: String? = null,
    @SerializedName("sponsor_ids") val sponsorIds: List<String>? = null
) {
    class Option(
        val description: String
    )
}


data class CreateImagePollRequest(
    val options: List<Option>,
    val question: String,
    val timeout: String,
    @SerializedName("program_id") val programId: String,
    @SerializedName("custom_data") val customData: String? = null,
    @SerializedName("program_date_time") val programDateTime: String? = null,
    @SerializedName("sponsor_ids") val sponsorIds: List<String>? = null
) {
    class Option(
        val description: String,
        @SerializedName("image_url") val imageUrl: String
    )
}


data class CreateAlertRequest(
    val timeout: String,
    val title: String?,
    val text: String?,
    @SerializedName("image_url") val imageURL: String?,
    @SerializedName("link_label") val linkLabel: String?,
    @SerializedName("link_url") val linkURL: String?,
    @SerializedName("custom_data") val customData: String? = null,
    @SerializedName("program_id") val programId: String,
    @SerializedName("program_date_time") val programDateTime: String? = null,
    @SerializedName("sponsor_ids") val sponsorIds: List<String>? = null
)

data class CreateTextPredictionRequest(
    val options: List<Option>,
    val question: String,
    val timeout: String,
    val interactiveUntil: String? = null,
    @SerializedName("program_id") val programId: String,
    @SerializedName("custom_data") val customData: String? = null,
    @SerializedName("confirmation_message") val confirmationMessage: String?,
    @SerializedName("program_date_time") val programDateTime: String? = null,
    @SerializedName("sponsor_ids") val sponsorIds: List<String>? = null
) {
    class Option(
        val description: String,
    )
}

data class CreateImagePredictionRequest(
    val options: List<Option>,
    val question: String,
    val timeout: String,
    val interactiveUntil: String? = null,
    @SerializedName("program_id") val programId: String,
    @SerializedName("custom_data") val customData: String? = null,
    @SerializedName("confirmation_message") val confirmationMessage: String?,
    @SerializedName("program_date_time") val programDateTime: String? = null,
    @SerializedName("sponsor_ids") val sponsorIds: List<String>? = null
) {
    class Option(
        val description: String, @SerializedName("image_url") val imageUrl: String
    )
}

data class UpdateTextPredictionOptions(
    val predictionId: String,
    val optionId: String,
    val rewardItemId: String?,
    val isCorrect: Boolean?,
    val rewardItemAmount: Double?,
)

data class UpdateImagePredictionOptions(
    val predictionId: String,
    val optionId: String,
    val rewardItemId: String?,
    val isCorrect: Boolean?,
    val rewardItemAmount: Double?,
)

data class CreateTextQuizRequest(
    val question: String,
    val choices: List<Choice>,
    val timeout: String,
    @SerializedName("program_id") val programId: String,
    @SerializedName("custom_data") val customData: String? = null,
    @SerializedName("program_date_time") val programDateTime: String? = null,
    @SerializedName("sponsor_ids") val sponsorIds: List<String>? = null
) {
    class Choice(
        val description: String, @SerializedName("is_correct") val isCorrect: Boolean
    )
}

data class CreateImageQuizRequest(
    val question: String,
    val choices: List<Choice>,
    val timeout: String,
    @SerializedName("program_id") val programId: String,
    @SerializedName("custom_data") val customData: String? = null,
    @SerializedName("program_date_time") val programDateTime: String? = null,
    @SerializedName("sponsor_ids") val sponsorIds: List<String>? = null
) {
    class Choice(
        val description: String,
        @SerializedName("is_correct") val isCorrect: Boolean,
        @SerializedName("image_url") val imageUrl: String
    )
}


data class CreateCheerMeterRequest(
    val question: String,
    val timeout: String,
    val options: List<Option>,
    @SerializedName("cheer_type") val cheerType: String,
    @SerializedName("program_id") val programId: String,
    @SerializedName("custom_data") val customData: String? = null,
    @SerializedName("program_date_time") val programDateTime: String? = null,
    @SerializedName("sponsor_ids") val sponsorIds: List<String>? = null
) {
    class Option(
        val description: String,
        @SerializedName("image_url") val imageUrl: String
    )
}

/*data class CreateSocialEmbedRequest(
    val timeout: String,
    val comment: String,
    val items: List<Item>,
    @SerializedName("program_id") val programId: String,
    @SerializedName("custom_data") val customData: String? = null,
    @SerializedName("program_date_time") val programDateTime: String? = null,
    @SerializedName("sponsor_ids") val sponsorIds: List<String>? = null
) {
    inner class Item(
        val url: String
    )
}*/

/*data class CreateImageSliderRequest(
    val question: String,
    val timeout: String,
    val options: List<Option>,
    @SerializedName("initial_magnitude") val initialMagnitude: String,
    @SerializedName("program_id") val programId: String,
    @SerializedName("custom_data") val customData: String? = null,
    @SerializedName("program_date_time") val programDateTime: String? = null,
    @SerializedName("sponsor_ids") val sponsorIds: List<String>? = null
) {
    inner class Option(
        val description: String,
        @SerializedName("image_url") val imageUrl: String
    )
}*/

/*data class CreateVideoAlertRequest(
    val timeout: String,
    val title: String,
    val text: String?,
    @SerializedName("video_url") val videoUrl: String,
    @SerializedName("program_id") val programId: String,
    @SerializedName("custom_data") val customData: String? = null,
    @SerializedName("program_date_time") val programDateTime: String? = null,
    @SerializedName("sponsor_ids") val sponsorIds: List<String>? = null
)
*/
data class CreateTextAskRequest(
    val timeout: String,
    val title: String,
    val prompt: String?,
    @SerializedName("confirmation_message") val confirmationMessage: String?,
    @SerializedName("program_id") val programId: String,
    @SerializedName("custom_data") val customData: String? = null,
    @SerializedName("program_date_time") val programDateTime: String? = null,
    @SerializedName("sponsor_ids") val sponsorIds: List<String>? = null
)


data class CreateImageNumberPredictionRequest(
    val question: String,
    val timeout: String,
    val options: List<Option>,
    @SerializedName("program_id") val programId: String,
    @SerializedName("confirmation_message") val confirmationMessage: String?,
    @SerializedName("custom_data") val customData: String? = null,
    @SerializedName("program_date_time") val programDateTime: String? = null,
    @SerializedName("sponsor_ids") val sponsorIds: List<String>? = null
) {
    class Option(
        @SerializedName("correct_number") val correctNumber: Int,
        @SerializedName("image_url") val imageUrl: String,
        val description: String,
    )
}


data class CreatePredictionFollowUpWidgetRequest(
    val widgetId: String,
    val widgetType: WidgetType,
    val correctOptionId: String,
    @SerializedName("sponsor_ids") val sponsorIds: List<String>? = null
)

data class CreateRichPostRequest(
    val content: String,
    @SerializedName("program_id") val programId: String,
    @SerializedName("sponsor_ids") val sponsorIds: List<String>? = null
)