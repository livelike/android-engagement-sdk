package com.livelike.engagementsdk.widget.viewModel

import com.livelike.common.DataStoreDelegate
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.AnalyticsWidgetInteractionInfo
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.WidgetInfos
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.model.Alert
import com.livelike.engagementsdk.widget.model.WidgetImpressions
import com.livelike.engagementsdk.widget.utils.toAnalyticsString
import com.livelike.engagementsdk.widget.widgetModel.VideoAlertWidgetModel
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.gson
import com.livelike.utils.Once
import com.livelike.utils.logDebug
import com.livelike.widget.parseDuration
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class VideoWidgetViewModel(
    val widgetInfos: WidgetInfos,
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    analyticsService: AnalyticsService,
    networkApiClient: NetworkApiClient,
    private var onDismiss: (() -> Unit)?,
    rewardItemMapCache: Map<String, RewardItem>,
    userProfileRewardDelegate: UserProfileDelegate?,
    dataStoreDelegate: DataStoreDelegate,
    viewModelDispatcher: CoroutineDispatcher,
    uiDispatcher: CoroutineDispatcher
) : BaseViewModel(
    configurationOnce,
    currentProfileOnce,
    analyticsService,
    networkApiClient,
    rewardItemMapCache,
    userProfileRewardDelegate,
    dataStoreDelegate,
    viewModelDispatcher,
    uiDispatcher
), VideoAlertWidgetModel {

    private var timeoutStarted = false
    val dataFlow = MutableStateFlow<Alert?>(null)

    private var currentWidgetId: String = ""
    private var programId: String = ""
    var currentWidgetType: WidgetType? = null
    private val interactionData = AnalyticsWidgetInteractionInfo()

    init {
        dataFlow.value = gson.fromJson(widgetInfos.payload.toString(), Alert::class.java) ?: null
        widgetStateFlow.value = WidgetStates.READY
        interactionData.widgetDisplayed()
        currentWidgetId = widgetInfos.widgetId
        programId = dataFlow.value?.program_id.toString()
        currentWidgetType = WidgetType.fromString(widgetInfos.type)
    }

    override fun videoAlertLinkClicked(url: String) {
        onVideoAlertClickLink(url)
        dataFlow.value?.program_id?.let {
            trackWidgetEngagedAnalytics(
                currentWidgetType,
                currentWidgetId,
                it,
                dataFlow.value?.text?:""
            )
        } ?: logDebug { "Video data or video programId is null" }
    }

    override fun registerPlayStarted() {
        trackPlayStarted()
    }

    override val widgetData: LiveLikeWidget
        get() = gson.fromJson(widgetInfos.payload, LiveLikeWidget::class.java)

    override fun markAsInteractive() {
        trackWidgetBecameInteractive(currentWidgetType, currentWidgetId, programId)
    }

    override fun registerImpression(
        url: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<WidgetImpressions>
    ) {
        registerImpressionApi(url, liveLikeCallback)
    }

    fun startDismissTimeout(timeout: String, onDismiss: () -> Unit) {
        if (!timeoutStarted && timeout.isNotEmpty()) {
            timeoutStarted = true
            uiScope.launch {
                delay(parseDuration(timeout))
                dismissWidget(DismissAction.TIMEOUT)
                onDismiss?.invoke()
                timeoutStarted = false
            }
        }
    }

    fun dismissWidget(action: DismissAction) {
        currentWidgetType?.let {
            analyticsService.trackWidgetDismiss(
                it.toAnalyticsString(),
                currentWidgetId,
                programId,
                interactionData,
                false,
                action
            )
        } ?: logDebug { "current widget type is null" }
        logDebug { "dismiss Video Widget, reason:${action.name}" }
        onDismiss?.invoke()
        onClear()
    }

    fun onVideoAlertClickLink(linkUrl: String) {
        interactionData.incrementInteraction()
        currentWidgetType?.let { widgetType ->
            dataFlow.value?.program_id?.let {
                analyticsService.trackAlertLinkOpened(
                    currentWidgetId,
                    it,
                    linkUrl,
                    currentWidgetType?.toAnalyticsString()
                )
            } ?: logDebug { "Video data or video programId is null" }
            dataFlow.value?.program_id?.let {
                analyticsService.trackWidgetInteraction(
                    widgetType.toAnalyticsString(),
                    currentWidgetId,
                    it,
                    interactionData,
                    widgetPrompt = dataFlow.value?.text ?: ""
                )
            } ?: logDebug { "Video data or video programId is null" }
        } ?: logDebug { "current widget type is null" }
    }

    private fun trackPlayStarted() {
        dataFlow.value?.program_id?.let {
            currentWidgetType?.toAnalyticsString()?.let { widgetType ->
                analyticsService.trackVideoAlertPlayed(
                    widgetType,
                    currentWidgetId,
                    it,
                    dataFlow.value?.videoUrl.toString()
                )
            }
        } ?: logDebug { "Video data or video programId is null" }
    }

    override fun finish() {
        onDismiss?.invoke()
        onClear()
    }

    override fun onClear() {
        super.onClear()
        dataFlow.value = null
        timeoutStarted = false
        currentWidgetType = null
        currentWidgetId = ""
        interactionData.reset()
        onDismiss = null
    }
}
