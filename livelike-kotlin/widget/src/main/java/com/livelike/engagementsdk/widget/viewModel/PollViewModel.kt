package com.livelike.engagementsdk.widget.viewModel


import com.livelike.common.DataStoreDelegate
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.utils.safeCallBack
import com.livelike.common.utils.toStream
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.AnalyticsWidgetInteractionInfo
import com.livelike.engagementsdk.AnalyticsWidgetSpecificInfo
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.Stream
import com.livelike.engagementsdk.WidgetInfos
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.core.data.models.RewardsType
import com.livelike.engagementsdk.core.data.models.VoteResponse
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.data.models.PollWidgetUserInteraction
import com.livelike.engagementsdk.widget.data.models.ProgramGamificationProfile
import com.livelike.engagementsdk.widget.data.respository.WidgetInteractionRepository
import com.livelike.engagementsdk.widget.domain.GamificationManager
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.model.LiveLikeWidgetResult
import com.livelike.engagementsdk.widget.model.Option
import com.livelike.engagementsdk.widget.model.Resource
import com.livelike.engagementsdk.widget.model.WidgetImpressions
import com.livelike.engagementsdk.widget.repository.ProgramRepository
import com.livelike.engagementsdk.widget.utils.addGamificationAnalyticsData
import com.livelike.engagementsdk.widget.utils.toAnalyticsString
import com.livelike.engagementsdk.widget.widgetModel.PollWidgetModel
import com.livelike.network.NetworkApiClient
import com.livelike.realtime.RealTimeClientMessage
import com.livelike.serialization.gson
import com.livelike.utils.LiveLikeException
import com.livelike.utils.Once
import com.livelike.utils.formatIsoZoned8601
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.livelike.widget.parseDuration
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime


class PollWidget(
    val type: WidgetType, val resource: Resource
)

class PollViewModel(
    private val widgetInfos: WidgetInfos,
    analyticsService: AnalyticsService,
    configurationOnce: Once<SdkConfiguration>,
    var onDismiss: (() -> Unit)?,
    currentProfileOnce: Once<LiveLikeProfile>,
    private val programRepository: ProgramRepository? = null,
    private val widgetMessagingClient: ((RealTimeClientMessage) -> Unit)? = null,
    val widgetInteractionRepository: WidgetInteractionRepository?,
    networkApiClient: NetworkApiClient,
    rewardItemMapCache: Map<String, RewardItem>,
    userProfileRewardDelegate: UserProfileDelegate?,
    dataStoreDelegate: DataStoreDelegate,
    viewModelDispatcher: CoroutineDispatcher,
    uiDispatcher: CoroutineDispatcher
) : BaseViewModel(
    configurationOnce,
    currentProfileOnce,
    analyticsService,
    networkApiClient,
    rewardItemMapCache,
    userProfileRewardDelegate,
    dataStoreDelegate,
    viewModelDispatcher,
    uiDispatcher
), PollWidgetModel {
    lateinit var onWidgetInteractionCompleted: () -> Unit

    val pointsFlow = MutableStateFlow<Int?>(null)
    val gamificationProfile: StateFlow<ProgramGamificationProfile?>
        get() = programRepository?.programGamificationProfileFlow ?: MutableStateFlow(null)
    val rewardsType: RewardsType
        get() = programRepository?.rewardType ?: RewardsType.NONE

    val dataFlow = MutableStateFlow<PollWidget?>(null)
    val resultsFlow = MutableStateFlow<Resource?>(null)
    val currentVoteIdFlow = MutableStateFlow<String?>(null)
    val disableInteractionFlow = MutableStateFlow<Boolean?>(null)

    @OptIn(FlowPreview::class)
    private val debouncer = currentVoteIdFlow.debounce(300L)
    private var latestVotedOptionId: String? = ""


    private var timeoutStarted = false
    private var animationResultsProgress = 0f
    private var animationPath = ""
    private var voteUrl: String? = null
    var animationEggTimerProgress = 0f
    var currentWidgetId: String = ""
    var programId: String = ""
    private var currentWidgetType: WidgetType? = null

    private val interactionData = AnalyticsWidgetInteractionInfo()
    private val widgetSpecificInfo = AnalyticsWidgetSpecificInfo()
    var selectedOptionId: String? = ""
    private var interactiveUntilTimeout = false

    init {
        uiScope.launch {
            debouncer.collect {
                if (it != null) vote()
            }
        }
        widgetObserver(widgetInfos)
    }

    private fun vote() {
        if (selectedPositionFlow.value == -1) return // Nothing has been clicked
        //this is done to update vote count locally for responsive UI
        updateLocalVoteCount() //increments decrements vote count locally
        dataFlow.value?.resource?.let { originalResource -> //ui updates
            resultsFlow.value = originalResource.copy()
        }

        // Create a deep copy of the resource to trigger collect
       /* dataFlow.value?.resource?.let { originalResource ->
            resultsFlow.value = Resource(
                clientId = originalResource.clientId,
                createdAt = originalResource.createdAt,
                id = originalResource.id,
                kind = originalResource.kind,
                programId = originalResource.programId,
                url = originalResource.url,
                options = originalResource.options,
                choices = originalResource.choices
            )
        }*/
        safeCallBack({ result, error ->
            result?.let { logDebug { it } }
            error?.let { logError { it } }
        }) {
            val resource =
                dataFlow.value?.resource ?: throw LiveLikeException("Poll Data not found")
            val option = resource.getMergedOptions()?.get(selectedPositionFlow.value)
                ?: throw LiveLikeException("No Option found for selected position : ${selectedPositionFlow.value}")
            saveInteraction(option)
            if (latestVotedOptionId != option.id) {
                val url = option.getMergedVoteUrl() ?: throw LiveLikeException("No Vote Url")
                latestVotedOptionId = option.id
                voteAsync(
                    url,
                    option.id,
                    patchVoteUrl = getUserInteraction()?.url,
                    rewardItemMapCache = rewardItemMapCache,
                    userProfileRewardDelegate = userProfileRewardDelegate,
                    currentProfileOnce = currentProfileOnce
                )
            } else {
                throw LiveLikeException("Latest Voted Option id is same as last option id")
            }
        }
    }

    override val widgetData: LiveLikeWidget
        get() = gson.fromJson(widgetInfos.payload, LiveLikeWidget::class.java)

    internal fun saveInteraction(option: Option) {
        selectedOptionId = option.id
        widgetInteractionRepository?.saveWidgetInteraction(
            PollWidgetUserInteraction(
                option.id ?: "",
                "",
                ZonedDateTime.now().formatIsoZoned8601(),
                getUserInteraction()?.url,
                widgetInfos.widgetId,
                widgetInfos.type
            )
        ) ?: logDebug { "widget interaction repository is null" }
    }

    private fun widgetObserver(widgetInfos: WidgetInfos?) {
        if (widgetInfos != null && (WidgetType.fromString(widgetInfos.type) == WidgetType.TEXT_POLL || WidgetType.fromString(
                widgetInfos.type
            ) == WidgetType.IMAGE_POLL)
        ) {
            val resource =
                gson.fromJson(widgetInfos.payload.toString(), Resource::class.java) ?: null
            resource?.apply {
                if (resource.pubnubEnabled) {
                    subscribeChannel?.let { it1 ->
                        subscribeWidgetResults(
                            it1, widgetInfos.widgetId, resultsFlow
                        )
                    } ?: logError { "subscribeChannel is null" }
                } else {
                    logDebug { "Pubnub not enabled" }
                }
                dataFlow.value =
                    WidgetType.fromString(widgetInfos.type)?.let { PollWidget(it, resource) }
            } ?: logDebug { "poll resource is null" }
            currentWidgetId = widgetInfos.widgetId
            programId = dataFlow.value?.resource?.programId.toString()
            currentWidgetType = WidgetType.fromString(widgetInfos.type)
            interactionData.widgetDisplayed()
        }
    }

    fun startDismissTimout(timeout: String) {
        if (!timeoutStarted && timeout.isNotEmpty()) {
            timeoutStarted = true
            uiScope.launch {
                delay(parseDuration(timeout))
                widgetStateFlow.value = WidgetStates.RESULTS
            }
        }
    }

    //this starts timer for interactive until set
    fun startInteractiveUntilTimeout(timeout: Long) {
        if (!interactiveUntilTimeout) {
            interactiveUntilTimeout = true
            uiScope.launch {
                val timeDiff =
                    timeout - Instant.now().atZone(ZoneId.of("UTC")).toInstant().toEpochMilli()
                delay(timeDiff)
                disableInteractionFlow.value = true
            }
        }
    }

    fun dismissWidget(action: DismissAction) {
        currentWidgetType?.let {
            analyticsService.trackWidgetDismiss(
                it.toAnalyticsString(), currentWidgetId, programId, interactionData,
                selectionLockedFlow.value, action
            )
        } ?: logDebug { "current widget type is null" }
        widgetStateFlow.value = WidgetStates.FINISHED
        logDebug { "dismiss Poll Widget, reason:${action.name}" }
        onDismiss?.invoke()
        onClear()
    }

    fun confirmationState() {
        if (selectedPositionFlow.value == -1) {
            // If the user never selected an option dismiss the widget with no confirmation
            dismissWidget(DismissAction.TIMEOUT)
            return
        }
        selectionLockedFlow.value = true
        onWidgetInteractionCompleted()

        safeCallBack(uiScope) {
            dataFlow.value?.resource?.rewardsUrl?.let {
                getGamificationReward(
                    it, analyticsService, currentProfileOnce().accessToken
                )?.let { pts ->
                    programRepository?.programGamificationProfileFlow?.value = pts
                    publishPoints(pts.newPoints)
                    widgetMessagingClient?.let { widgetMessagingClient ->
                        GamificationManager.checkForNewBadgeEarned(pts, widgetMessagingClient)
                    }
                    interactionData.addGamificationAnalyticsData(pts)
                }
            } ?: logError { "Rewards Url is null" }

            currentWidgetType?.let {
                analyticsService.trackWidgetInteraction(
                    it.toAnalyticsString(),
                    currentWidgetId,
                    programId,
                    interactionData,
                    widgetPrompt = dataFlow.value?.resource?.question ?: ""
                )
            } ?: logDebug { "Current widget type is null" }
            delay(3000)
            dismissWidget(DismissAction.TIMEOUT)
        }
    }

    private fun publishPoints(pts: Int) {
        this.pointsFlow.value = pts
    }



    private var firstClick = true

    fun onOptionClicked() {
        if (firstClick) {
            firstClick = false
        }
        interactionData.incrementInteraction()
    }

    override val voteResults: Stream<LiveLikeWidgetResult>
        get() = resultsFlow.map { it?.toLiveLikeWidgetResult() }.toStream(uiScope)
    override val voteResultFlow: Flow<LiveLikeWidgetResult?>
        get() = resultsFlow.map { it?.toLiveLikeWidgetResult() }

    override fun submitVote(
        optionID: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<VoteResponse>?
    ) {
        trackWidgetEngagedAnalytics(
            currentWidgetType = currentWidgetType,
            currentWidgetId = currentWidgetId,
            programId = programId,
            widgetPrompt = dataFlow.value?.resource?.question ?:""
        )
        updateVoteCount(optionID) //increments decrements vote count locally
        resultsFlow.value = dataFlow.value?.resource

        safeCallBack(liveLikeCallback) {
            val widget = dataFlow.value ?: throw LiveLikeException("Poll data is null")
            val option = widget.resource.getMergedOptions()?.find { it.id == optionID }
                ?: throw LiveLikeException("No Option is Found")
            saveInteraction(option)
            voteAsync(
                option.getMergedVoteUrl() ?: throw LiveLikeException("No Vote Url"),
                option.id,
                currentProfileOnce = currentProfileOnce,
                patchVoteUrl = getUserInteraction()?.url,
                rewardItemMapCache = rewardItemMapCache,
                userProfileRewardDelegate = userProfileRewardDelegate
            )
        }
    }

    override fun getUserInteraction(): PollWidgetUserInteraction? {
        return widgetInteractionRepository?.getWidgetInteraction(
            widgetInfos.widgetId
        )
    }

    override fun loadInteractionHistory(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<PollWidgetUserInteraction>>) {
        safeCallBack(uiScope, liveLikeCallback) {
            val results = widgetInteractionRepository?.fetchRemoteInteractions(
                widgetId = widgetInfos.widgetId,
                widgetKind = widgetInfos.type,
                programUrlTemplate = configurationOnce().programDetailUrlTemplate
            ) ?: throw Exception("Remote Interaction not found")
            if (WidgetType.fromString(widgetInfos.type) == WidgetType.TEXT_POLL) {
                results.interactions.textPoll ?: emptyList()
            } else if (WidgetType.fromString(widgetInfos.type) == WidgetType.IMAGE_POLL) {
                results.interactions.imagePoll ?: emptyList()
            } else {
                emptyList()
            }
        }
    }

    private fun updateLocalVoteCount() {
        (dataFlow.value?.resource)?.apply {
            val optionResults = this.getMergedOptions() ?: return

            //check if previous user vote exists then decrement vote count for that option id
            if (selectedOptionId?.isNotEmpty() == true) {
                optionResults.find {
                    it.id == selectedOptionId
                }?.apply {
                    this.voteCount = this.voteCount?.minus(1)
                    if (this.voteCount!! < 0) this.voteCount = 0
                } ?: logDebug { "no option found" }
            }
            //incrementVoteCountLocally()

            val selectedOption = this.getMergedOptions()?.get(selectedPositionFlow.value)
                ?: throw LiveLikeException("No Option found for selected position : ${selectedPositionFlow.value}")
            optionResults.find {
                //it.id == userSelectedOptionIdFlow.value
                it.id == selectedOption.id
            }?.apply {
                this.voteCount = this.voteCount?.plus(1)
            }
        }
    }


    // increment vote count locally for the option id
    private fun incrementVoteCountLocally() {
        dataFlow.value?.resource?.apply {
            val optionResults = this.getMergedOptions() ?: return
            userSelectedOptionIdFlow.value = selectedPositionFlow.value.let { it1 ->
                if (it1 > -1) return@let this.getMergedOptions()?.get(it1)?.id
                return@let null
            } ?: ""
            optionResults.find {
                it.id == userSelectedOptionIdFlow.value
            }?.apply {
                this.voteCount = this.voteCount?.plus(1)
            }
        } ?: logDebug { "poll data is null" }
    }


    // increment vote count locally for the option id
    // to be used in custom UI
    private fun updateVoteCount(optionId:String) {
        (dataFlow.value?.resource)?.apply {
            val optionResults = this.getMergedOptions() ?: return

            //check if previous user vote exists then decrement vote count for that option id
            if (selectedOptionId?.isNotEmpty() == true) {
                optionResults.find {
                    it.id == selectedOptionId
                }?.apply {
                    this.voteCount = this.voteCount?.minus(1)
                    if (this.voteCount!! < 0) this.voteCount = 0
                } ?: logDebug { "no option found" }
            }

            //if previous vote is not present increment vote count only
            optionResults.find {
                it.id == optionId
            }?.apply {
                this.voteCount = this.voteCount?.plus(1)
            }?: logDebug { "no option found" }
        }
    }




    override fun finish() {
        onDismiss?.invoke()
        onClear()
    }

    override fun markAsInteractive() {
        trackWidgetBecameInteractive(currentWidgetType, currentWidgetId, programId)
    }

    override fun registerImpression(
        url: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<WidgetImpressions>
    ) {
        registerImpressionApi(url, liveLikeCallback)
    }

    override fun onClear() {
        super.onClear()
        vote() // Vote on dismiss
        unsubscribeWidgetResults()
        timeoutStarted = false
        animationResultsProgress = 0f
        animationPath = ""
        voteUrl = null
        dataFlow.value = null
        resultsFlow.value = null
        animationEggTimerProgress = 0f
        currentVoteIdFlow.value = null
        latestVotedOptionId = ""
        interactionData.reset()
        widgetSpecificInfo.reset()
        currentWidgetId = ""
        currentWidgetType = null
        onDismiss = null
    }
}
