package com.livelike.engagementsdk.widget.repository


import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.core.data.models.Program
import com.livelike.engagementsdk.core.data.models.ProgramModel
import com.livelike.engagementsdk.core.data.models.RewardsType
import com.livelike.engagementsdk.widget.data.models.ProgramGamificationProfile
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.processResult
import com.livelike.utils.Once
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.livelike.widget.TEMPLATE_PROGRAM_ID
import kotlinx.coroutines.flow.MutableStateFlow
import java.util.Locale

/**
 * Repository that handles program and program-user data.
 * Program is an event in CMS App.
 */
class ProgramRepository(
    val programId: String,
    private val currentProfileOnce: Once<LiveLikeProfile>,
    private val networkApiClient: NetworkApiClient,
) {

    internal var program: Program? = null

    val rewardType: RewardsType by lazy {
        RewardsType.valueOf(
            program?.rewardsType?.uppercase(Locale.getDefault()) ?: "none"
        )
    }

    val programGamificationProfileFlow =
        MutableStateFlow<ProgramGamificationProfile?>(null)

    /**
     * responsible for fetching program resource
     * @param programDetailUrlTemplate (received in engagement configuration resource)
     */
    suspend fun getProgramData(programDetailUrlTemplate: String): ProgramModel {
        return networkApiClient.get(
            programDetailUrlTemplate.replace(
                TEMPLATE_PROGRAM_ID,
                programId
            ),
            accessToken = currentProfileOnce().accessToken
        ).processResult<ProgramModel>().also {
            this@ProgramRepository.program = it
        }
    }


    /**
     * responsible for fetching program rank
     */
    internal suspend fun fetchProgramRank() {
        program?.let { program ->
            if (program.rewardsType == RewardsType.NONE.key) {
                logError { "Should not call if Gamification is disabled" }
                return
            }
            networkApiClient.get(
                program.rankUrl,
                accessToken = currentProfileOnce().accessToken
            )
                .processResult<ProgramGamificationProfile>()
                .run {
                    logDebug { "points update : ${points}, rank update: $rank" }
                    programGamificationProfileFlow.value = this@run
                }
        } ?: logDebug { "Program is null" }
    }
}
