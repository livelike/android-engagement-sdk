package com.livelike.engagementsdk.widget.viewModel

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

open class ViewModel(viewModelDispatcher: CoroutineDispatcher, uiDispatcher: CoroutineDispatcher) {
    /**
     * This is the job for all coroutines started by this ViewModel.
     * Cancelling this job will cancel all coroutines started by this ViewModel.
     */
    private val viewModelJob = SupervisorJob()
    val viewModelScope = CoroutineScope(viewModelDispatcher + viewModelJob)

    /**
     * This is the main scope for all coroutines launched by MainViewModel.
     * Since we pass viewModelJob, you can cancel all coroutines
     * launched by uiScope by calling viewModelJob.cancel()
     */
    val uiScope = CoroutineScope(uiDispatcher + viewModelJob)
}
