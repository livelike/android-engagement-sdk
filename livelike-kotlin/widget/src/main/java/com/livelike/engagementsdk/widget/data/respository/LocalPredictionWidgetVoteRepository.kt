package com.livelike.engagementsdk.widget.data.respository

import com.google.gson.reflect.TypeToken
import com.livelike.common.DataStoreDelegate
import com.livelike.serialization.gson

class LocalPredictionWidgetVoteRepository(private val dataStoreDelegate: DataStoreDelegate) :
    PredictionWidgetVoteRepository {

    override fun add(vote: PredictionWidgetVote, completion: () -> Unit) {
        val json = dataStoreDelegate.getWidgetClaimToken()
        var map = gson.fromJson<MutableMap<String, String>>(
            json, object : TypeToken<MutableMap<String, String>>() {}.type
        )
        if (map == null) {
            map = mutableMapOf()
        }
        map[vote.widgetId] = vote.claimToken
        dataStoreDelegate.setWidgetClaimToken(gson.toJson(map))
    }

    override fun get(predictionWidgetID: String): String? {
        val json = dataStoreDelegate.getWidgetClaimToken()
        json?.let {
            val map = gson.fromJson<MutableMap<String, String>>(
                json, object : TypeToken<MutableMap<String, String>>() {}.type
            )
            return map[predictionWidgetID]
        }
        return null
    }
}

interface PredictionWidgetVoteRepository {
    fun add(vote: PredictionWidgetVote, completion: () -> Unit)
    fun get(predictionWidgetID: String): String?
}

data class PredictionWidgetVote(val widgetId: String, val claimToken: String)
