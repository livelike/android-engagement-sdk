package com.livelike.engagementsdk.widget.viewModel

import com.livelike.common.DataStoreDelegate
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.AnalyticsWidgetInteractionInfo
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.WidgetInfos
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.core.data.models.TextReply
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.data.models.TextAskUserInteraction
import com.livelike.engagementsdk.widget.data.respository.WidgetInteractionRepository
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.model.Resource
import com.livelike.engagementsdk.widget.model.WidgetImpressions
import com.livelike.engagementsdk.widget.utils.toAnalyticsString
import com.livelike.engagementsdk.widget.widgetModel.TextAskWidgetModel
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.gson
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import com.livelike.utils.LiveLikeException
import com.livelike.utils.Once
import com.livelike.utils.formatIsoZoned8601
import com.livelike.utils.logDebug
import com.livelike.widget.parseDuration
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import org.threeten.bp.ZonedDateTime

class TextAskWidget(
    val type: WidgetType,
    val resource: Resource
)

class TextAskViewModel(
    val widgetInfos: WidgetInfos,
    analyticsService: AnalyticsService,
    var onDismiss: (() -> Unit)?,
    currentProfileOnce: Once<LiveLikeProfile>,
    configurationOnce: Once<SdkConfiguration>,
    val widgetInteractionRepository: WidgetInteractionRepository?,
    networkApiClient: NetworkApiClient,
    rewardItemMapCache: Map<String, RewardItem>,
    userProfileRewardDelegate: UserProfileDelegate?,
    dataStoreDelegate: DataStoreDelegate,
    viewModelDispatcher: CoroutineDispatcher,
    uiDispatcher: CoroutineDispatcher
) : BaseViewModel(
    configurationOnce,
    currentProfileOnce,
    analyticsService,
    networkApiClient,
    rewardItemMapCache,
    userProfileRewardDelegate,
    dataStoreDelegate,
    viewModelDispatcher,
    uiDispatcher
), TextAskWidgetModel {

    val dataFlow = MutableStateFlow<TextAskWidget?>(null)
    val resultsFlow = MutableStateFlow<Resource?>(null)

    private var timeoutStarted = false
    private var currentWidgetId: String = ""
    private var programId: String = ""
    private var currentWidgetType: WidgetType? = null
    private val interactionData = AnalyticsWidgetInteractionInfo()
    var animationEggTimerProgress = 0f

    init {
        widgetObserver(widgetInfos)
    }

    /** responsible for submitting the user response */
    override fun submitReply(
        response: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<TextReply>
    ) {
        trackWidgetEngagedAnalytics(
            currentWidgetType = currentWidgetType,
            currentWidgetId = currentWidgetId,
            programId = programId,
            widgetPrompt = dataFlow.value?.resource?.prompt ?: ""
        )
        lockAndSubmitReply(response, liveLikeCallback)
    }

    fun lockAndSubmitReply(response: String,liveLikeCallback: com.livelike.common.LiveLikeCallback<TextReply>) {
        safeCallBack(liveLikeCallback) {
            val widget = dataFlow.value?.resource ?: throw LiveLikeException("No TextAsk widget data found")
            networkApiClient.post(
                widget.replyUrl ?: throw LiveLikeException("Reply Url is Null"),
                accessToken = currentProfileOnce().accessToken,
                body = mapOf("text" to response).toJsonString()
            ).processResult()
        }
    }

    override fun getUserInteraction(): TextAskUserInteraction? {
        return widgetInteractionRepository?.getWidgetInteraction(
            widgetInfos.widgetId
        )
    }

    override fun loadInteractionHistory(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<TextAskUserInteraction>>) {
        safeCallBack(liveLikeCallback) {
            val results =
                widgetInteractionRepository?.fetchRemoteInteractions(
                    widgetId = widgetInfos.widgetId,
                    widgetKind = widgetInfos.type,
                    programUrlTemplate = configurationOnce().programDetailUrlTemplate
                )
            results?.interactions?.textAsk ?: emptyList()
        }
    }

    override val widgetData: LiveLikeWidget
        get() = gson.fromJson(widgetInfos.payload, LiveLikeWidget::class.java)

    private fun widgetObserver(widgetInfos: WidgetInfos?) {
        if (widgetInfos != null) {
            val resource =
                gson.fromJson(widgetInfos.payload.toString(), Resource::class.java) ?: null
            resource?.apply {
                dataFlow.value =
                    WidgetType.fromString(widgetInfos.type)?.let { TextAskWidget(it, resource) }
            } ?: logDebug { "text ask resource is null" }
            logDebug { "reply url ${dataFlow.value?.resource?.replyUrl}" }
            currentWidgetId = widgetInfos.widgetId
            programId = dataFlow.value?.resource?.programId.toString()
            currentWidgetType = WidgetType.fromString(widgetInfos.type)
            interactionData.widgetDisplayed()
        }
    }

    override fun finish() {
        onDismiss?.invoke()
        cleanUp()
    }

    override fun markAsInteractive() {
        trackWidgetBecameInteractive(currentWidgetType, currentWidgetId, programId)
    }

    override fun registerImpression(
        url: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<WidgetImpressions>
    ) {
        registerImpressionApi(url, liveLikeCallback)
    }

    fun startDismissTimout(timeout: String) {
        if (!timeoutStarted && timeout.isNotEmpty()) {
            timeoutStarted = true
            uiScope.launch {
                delay(parseDuration(timeout))
                widgetStateFlow.value = WidgetStates.RESULTS
            }
        }
    }

     fun saveInteraction(userResponse: String) {
        widgetInteractionRepository?.saveWidgetInteraction(
            TextAskUserInteraction(
                "",
                ZonedDateTime.now().formatIsoZoned8601(),
                getUserInteraction()?.url,
                userResponse,
                widgetInfos.widgetId,
                widgetInfos.type
            )
        ) ?: logDebug { "widgetInteractionRepository is null" }
    }

    /** logic to be added here, when claiming rewards/ points for submitting response
     * presently not available on first iteration */
    fun confirmationState() {
        // to be added auto claim rewards logic here
        uiScope.launch {
            delay(2000)
            dismissWidget(DismissAction.TIMEOUT)
        }
    }

    /** track widget interacted event*/
    fun trackWidgetInteractedEvent(){
        currentWidgetType?.let {
            analyticsService.trackWidgetInteraction(
                it.toAnalyticsString(),
                currentWidgetId,
                programId,
                interactionData,
                widgetPrompt = dataFlow.value?.resource?.prompt ?: ""
            )
        } ?: logDebug { "current widget type is null" }
    }

    fun dismissWidget(action: DismissAction) {
        currentWidgetType?.let {
            analyticsService.trackWidgetDismiss(
                it.toAnalyticsString(),
                currentWidgetId,
                programId,
                interactionData,
                false,
                action
            )
        } ?: logDebug { "current widget type is null" }
        widgetStateFlow.value = WidgetStates.FINISHED
        logDebug { "dismiss AMA Widget, reason:${action.name}" }
        onDismiss?.invoke()
        cleanUp()
    }

    private fun cleanUp() {
        dataFlow.value = null
        timeoutStarted = false
        currentWidgetType = null
        animationEggTimerProgress = 0f
        interactionData.reset()
    }

    override fun onClear() {
        cleanUp()
        onDismiss = null
    }
}
