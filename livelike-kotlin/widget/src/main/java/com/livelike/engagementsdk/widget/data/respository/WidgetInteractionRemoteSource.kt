package com.livelike.engagementsdk.widget.data.respository


import com.livelike.engagementsdk.widget.data.models.UserWidgetInteractionApi
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.processResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Widget Interaction Remote source responsible for fetching the interaction data from our rest apis
 **/
internal class WidgetInteractionRemoteSource(private val networkApiClient: NetworkApiClient) {

    internal suspend fun getWidgetInteractions(
        url: String,
        accessToken: String
    ): UserWidgetInteractionApi = withContext(Dispatchers.IO) {
        networkApiClient.get(url, accessToken).processResult()
    }
}
