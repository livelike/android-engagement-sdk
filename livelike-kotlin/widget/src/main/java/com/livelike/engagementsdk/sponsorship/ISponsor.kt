package com.livelike.engagementsdk.sponsorship


import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.LiveLikeChatClient
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.network.NetworkApiClient
import com.livelike.utils.Once
import kotlinx.coroutines.CoroutineScope

interface ISponsor {

    /**
     * Fetch sponsor associated to the specified program Id
     * @param programId id of the program
     * @param pagination : pagination for the entries
     **/
    fun fetchByProgramId(
        programId: String,
        pagination: LiveLikePagination,
        callback: com.livelike.common.LiveLikeCallback<List<SponsorModel>>
    )


    /**
     * Fetch sponsor associated with the specified application
     * @param pagination pagination for the entries
     **/
    fun fetchForApplication(
        pagination: LiveLikePagination,
        callback: com.livelike.common.LiveLikeCallback<List<SponsorModel>>
    )

    /**
     * Fetch sponsor associated to the specified chatRoom Id
     * @param chatRoomId id of the chatroom for which sponsor is associated
     * @param pagination pagination for the entries
     **/
    fun fetchByChatRoomId(
        chatRoomId: String,
        pagination: LiveLikePagination,
        callback: com.livelike.common.LiveLikeCallback<List<SponsorModel>>
    )

    companion object {

        fun getInstance(
            configurationOnce: Once<SdkConfiguration>,
            sdkScope: CoroutineScope,
            uiScope: CoroutineScope,
            networkApiClient: NetworkApiClient,
            currentProfileOnce: Once<LiveLikeProfile>,
            chatClient: LiveLikeChatClient
        ): ISponsor = Sponsor(
            configurationOnce,
            currentProfileOnce,
            sdkScope,
            chatClient,
            uiScope,
            networkApiClient
        )
    }
}
