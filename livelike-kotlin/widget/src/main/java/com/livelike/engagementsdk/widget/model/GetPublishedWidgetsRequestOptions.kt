package com.livelike.engagementsdk.widget.model

import com.google.gson.annotations.SerializedName
import com.livelike.engagementsdk.WidgetAttribute
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination

data class GetPublishedWidgetsRequestOptions(
    /** since playback time used for video on demand widgets list */
    @SerializedName("since_playback_time")
    val sincePlaybackTimeMs: Long? = null,
    /** until playback time used for video on demand widgets list */
    @SerializedName("until_playback_time")
    val untilPlaybackTimeMs: Long? = null,
    val liveLikePagination: LiveLikePagination,
    val widgetAttributes: List<WidgetAttribute>? = null
)


