package com.livelike.engagementsdk.widget.viewModel

import com.livelike.common.DataStoreDelegate
import com.livelike.common.model.RequestType
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.utils.safeCallBack
import com.livelike.common.utils.toStream
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.AnalyticsWidgetInteractionInfo
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.Stream
import com.livelike.engagementsdk.WidgetInfos
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.data.models.CheerMeterUserInteraction
import com.livelike.engagementsdk.widget.data.respository.WidgetInteractionRepository
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.model.LiveLikeWidgetResult
import com.livelike.engagementsdk.widget.model.Resource
import com.livelike.engagementsdk.widget.model.WidgetImpressions
import com.livelike.engagementsdk.widget.utils.toAnalyticsString
import com.livelike.engagementsdk.widget.widgetModel.CheerMeterWidgetmodel
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.gson
import com.livelike.utils.Once
import com.livelike.utils.formatIsoZoned8601
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.livelike.widget.parseDuration
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime

class CheerMeterWidget(
    val type: WidgetType, val resource: Resource
)

class CheerMeterViewModel(
    val widgetInfos: WidgetInfos,
    analyticsService: AnalyticsService,
    configurationOnce: Once<SdkConfiguration>,
    var onDismiss: (() -> Unit)?,
    currentProfileOnce: Once<LiveLikeProfile>,
    val widgetInteractionRepository: WidgetInteractionRepository?,
    networkApiClient: NetworkApiClient,
    rewardItemMapCache: Map<String, RewardItem>,
    userProfileRewardDelegate: UserProfileDelegate?,
    dataStoreDelegate: DataStoreDelegate,
    viewModelDispatcher: CoroutineDispatcher,
    uiDispatcher: CoroutineDispatcher
) : BaseViewModel(
    configurationOnce,
    currentProfileOnce,
    analyticsService,
    networkApiClient,
    rewardItemMapCache,
    userProfileRewardDelegate,
    dataStoreDelegate,
    viewModelDispatcher,
    uiDispatcher
), CheerMeterWidgetmodel {

    var totalVoteCount = 0

    /**
     *this is equal to size of list of options containing vote count to synced with server for each option
     *first request is post to create the vote then after to update the count on that option, patch request will be used
     **/
    private var voteStateList: MutableList<CheerMeterVoteState> = mutableListOf()
    val resultsFlow = MutableStateFlow<Resource?>(null)
    val voteEndFlow = MutableStateFlow<Boolean?>(null)
    val dataFlow = MutableStateFlow<CheerMeterWidget?>(null)
    val disableInteractionFlow = MutableStateFlow<Boolean?>(null)

    private var currentWidgetId: String = ""
    private var programId: String = ""
    private var currentWidgetType: WidgetType? = null
    private val interactionData = AnalyticsWidgetInteractionInfo()
    var isWidgetInteractedEventLogged = false
    var animationEggTimerProgress = 0f
    var animationProgress = 0f

    private val voteFlow = MutableStateFlow<Int?>(null)

    @OptIn(FlowPreview::class)
    private val debounceVote = voteFlow.debounce(300L)
    private var interactiveUntilTimeoutStarted = false

    init {

        widgetObserver(widgetInfos)
        // restoring the cheer meter score from interaction history
        totalVoteCount = getUserInteraction()?.totalScore ?: 0
        uiScope.launch {
            debounceVote.collect {
                safeCallBack(uiScope) {
                    wouldSendVote()
                }
            }
        }
    }

    fun incrementVoteCount(teamIndex: Int, optionID: String?) {
        logDebug { "post body:1 $totalVoteCount -- $teamIndex" }

        interactionData.incrementInteraction()
        totalVoteCount++
        voteStateList.getOrNull(teamIndex)?.let {
            logDebug { "post body:2 ${it.voteCount} -- $teamIndex" }
            it.voteCount++
            logDebug { "post body:3 ${it.voteCount} -- $teamIndex" }
        }
        logDebug { "post body:4 $totalVoteCount -- $teamIndex " }
        voteFlow.value = totalVoteCount
        println("cheermeter-total-vote-count-${totalVoteCount}")
        saveInteraction(totalVoteCount, null, optionID)
    }

    private suspend fun wouldSendVote() {
        for (it in voteStateList) {
            pushVoteStateData(it)
        }
    }

    private suspend fun pushVoteStateData(voteState: CheerMeterVoteState) {
        if (voteState.voteCount > 0) {
            val count = voteState.voteCount
            val voteUrl = voteAsync(
                voteState.voteUrl,
                body = mapOf(
                    "vote_count" to voteState.voteCount
                ),
                type = voteState.requestType,
                useVoteUrl = false,
                rewardItemMapCache = rewardItemMapCache,
                userProfileRewardDelegate = userProfileRewardDelegate,
                currentProfileOnce = currentProfileOnce
            ).url
            voteUrl.let {
                voteState.voteUrl = it
                voteState.requestType = RequestType.PATCH
            }

            voteState.voteCount -= count
        }
    }

    fun voteEnd() {
        currentWidgetType?.let {
            // interaction event will only be fired if interaction cunt is more than 0 and if not logged before
            if (interactionData.interactionCount > 0 && !isWidgetInteractedEventLogged) {
                isWidgetInteractedEventLogged = true
                dataFlow.value?.resource?.programId?.let { programId ->
                    analyticsService.trackWidgetInteraction(
                        it.toAnalyticsString(),
                        currentWidgetId,
                        programId,
                        interactionData,
                        widgetPrompt = dataFlow.value?.resource?.question ?: ""
                    )
                }
            }
        }
    }

    private fun widgetObserver(widgetInfos: WidgetInfos?) {
        if (widgetInfos != null) {
            val resource =
                gson.fromJson(widgetInfos.payload.toString(), Resource::class.java) ?: null
            resource?.apply {
                for (option in resource.getMergedOptions() ?: emptyList()) {
                    voteStateList.add(
                        CheerMeterVoteState(
                            0,
                            option.voteUrl ?: "",
                            RequestType.POST
                        )
                    )
                }
                if (resource.pubnubEnabled) {
                    subscribeChannel?.let { it1 ->
                        subscribeWidgetResults(
                            it1,
                            widgetInfos.widgetId,
                            resultsFlow
                        )
                    } ?: logError { "subscribeChannel is null" }
                } else {
                    logDebug { "Pubnub not enabled" }
                }

                dataFlow.value = WidgetType.fromString(widgetInfos.type)?.let {
                    CheerMeterWidget(it, resource)
                }
            }
            currentWidgetId = widgetInfos.widgetId
            programId = dataFlow.value?.resource?.programId.toString()
            currentWidgetType = WidgetType.fromString(widgetInfos.type)
            interactionData.widgetDisplayed()
        }
    }

    fun startDismissTimout(timeout: String) {
        if (timeout.isNotEmpty()) {
            uiScope.launch {
                delay(parseDuration(timeout))
                if (totalVoteCount == 0) {
                    dismissWidget(DismissAction.TIMEOUT)
                } else {
                    widgetStateFlow.value = WidgetStates.RESULTS
                }
            }
        }
    }

    fun startInteractiveUntilTimeout(timeout: Long) {
        if (!interactiveUntilTimeoutStarted) {
            interactiveUntilTimeoutStarted = true
            uiScope.launch {
                val timeDiff =
                    timeout - Instant.now().atZone(ZoneId.of("UTC")).toInstant().toEpochMilli()
                delay(timeDiff)
                disableInteractionFlow.value = true
            }
        }
    }

    override fun finish() {
        onDismiss?.invoke()
        onClear()
    }

    override fun markAsInteractive() {
        trackWidgetBecameInteractive(currentWidgetType, currentWidgetId, programId)
    }

    override fun registerImpression(
        url: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<WidgetImpressions>
    ) {
        registerImpressionApi(url, liveLikeCallback)
    }

    fun dismissWidget(action: DismissAction) {
        currentWidgetType?.let {
            dataFlow.value?.resource?.programId?.let { programId ->
                analyticsService.trackWidgetDismiss(
                    it.toAnalyticsString(),
                    currentWidgetId,
                    programId,
                    interactionData,
                    false,
                    action
                )
            }
        }
        logDebug { "dismiss Alert Widget, reason:${action.name}" }
        onDismiss?.invoke()
        onClear()
    }

    override fun onClear() {
        super.onClear()
        unsubscribeWidgetResults()
        dataFlow.value = null
        resultsFlow.value = null
        animationEggTimerProgress = 0f
        interactionData.reset()
        currentWidgetId = ""
        currentWidgetType = null
        voteFlow.value = null
        onDismiss = null
    }

    override val widgetData: LiveLikeWidget
        get() = gson.fromJson(widgetInfos.payload, LiveLikeWidget::class.java)

    override val voteResults: Stream<LiveLikeWidgetResult>
        get() = resultsFlow.map { it?.toLiveLikeWidgetResult() }.toStream(uiScope)
    override val voteResultsFlow: Flow<LiveLikeWidgetResult?>
        get() = resultsFlow.map { it?.toLiveLikeWidgetResult() }

    override fun submitVote(optionID: String) {
        trackWidgetEngagedAnalytics(
            currentWidgetType = currentWidgetType,
            currentWidgetId = currentWidgetId,
            programId = programId,
            widgetPrompt = dataFlow.value?.resource?.question?:""
        )
        widgetData.let { widget ->
            widget.getMergedOptions()?.indexOfFirst { it.id == optionID }?.let {
                if (it >= 0) incrementVoteCount(it, optionID)
            }
        }
        if (resultsFlow.value == null) {
            resultsFlow.value = widgetData.copy(
                options = widgetData.options?.map { option ->
                    if (option.id == optionID) {
                        option.copy(voteCount = (option.voteCount ?: 0) + 1)
                    } else {
                        option
                    }
                }
            )
        } else {
            resultsFlow.update { currentData ->
                currentData?.copy(
                    options = currentData.options?.map { option ->
                        if (option.id == optionID) {
                            option.copy(voteCount = (option.voteCount ?: 0) + 1)
                        } else {
                            option
                        }
                    }
                )
            }
        }
    }

    override fun getUserInteraction(): CheerMeterUserInteraction? {
        return widgetInteractionRepository?.getWidgetInteraction(
            widgetInfos.widgetId
        )
    }

    override fun loadInteractionHistory(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<CheerMeterUserInteraction>>) {
        safeCallBack(uiScope, liveLikeCallback) {
            val results = widgetInteractionRepository?.fetchRemoteInteractions(
                widgetId = widgetInfos.widgetId,
                widgetKind = widgetInfos.type,
                programUrlTemplate = configurationOnce().programDetailUrlTemplate
            )
            results?.interactions?.cheerMeter ?: emptyList()
        }
    }

    internal fun saveInteraction(score: Int, url: String?, optionID: String?) {
        widgetInteractionRepository?.saveWidgetInteraction(
            CheerMeterUserInteraction(
                score,
                "",
                ZonedDateTime.now().formatIsoZoned8601(),
                url,
                widgetInfos.widgetId,
                widgetInfos.type,
                optionID
            )
        )
    }
}

data class CheerMeterVoteState(
    var voteCount: Int, var voteUrl: String, var requestType: RequestType
)
