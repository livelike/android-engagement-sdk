package com.livelike.engagementsdk

import com.livelike.common.LiveLikeKotlin
import com.livelike.common.profile
import com.livelike.engagementsdk.sponsorship.ISponsor

/**
 * Create a sponsor client and returns that
 **/
fun LiveLikeKotlin.sponsor(): ISponsor {
    val key = this.hashCode()
    return if (sdkInstanceWithSponsorClient.containsKey(key)) {
        sdkInstanceWithSponsorClient[key]!!
    } else {
        ISponsor.getInstance(
            sdkConfigurationOnce,
            sdkScope,
            uiScope,
            networkClient,
            profile().currentProfileOnce,
            chat()
        ).let {
            sdkInstanceWithSponsorClient[key] = it
            it
        }
    }
}


private val sdkInstanceWithSponsorClient = mutableMapOf<Int, ISponsor>()
