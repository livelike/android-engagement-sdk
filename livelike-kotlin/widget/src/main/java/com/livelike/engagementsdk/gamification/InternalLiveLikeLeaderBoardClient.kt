package com.livelike.engagementsdk.gamification

import com.livelike.BaseClient
import com.livelike.common.map
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.core.data.models.LeaderBoard
import com.livelike.engagementsdk.core.data.models.LeaderBoardEntry
import com.livelike.engagementsdk.core.data.models.LeaderBoardForClient
import com.livelike.engagementsdk.core.data.models.LeaderBoardResource
import com.livelike.engagementsdk.core.data.models.LeaderboardClient
import com.livelike.engagementsdk.core.data.models.LeaderboardPlacement
import com.livelike.engagementsdk.core.data.models.Program
import com.livelike.engagementsdk.widget.domain.LeaderBoardDelegate
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.processResult
import com.livelike.utils.NO_MORE_DATA
import com.livelike.utils.Once
import com.livelike.utils.PaginationResponse
import com.livelike.widget.TEMPLATE_LEADER_BOARD_ID
import com.livelike.widget.TEMPLATE_PROFILE_ID
import com.livelike.widget.TEMPLATE_PROGRAM_ID
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

/**
 * LeaderBoard client allowing to fetch all leaderboard related stuff
 */

internal class InternalLiveLikeLeaderBoardClient(
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    private val sdkScope: CoroutineScope,
    private val uiScope: CoroutineScope,
    private val networkApiClient: NetworkApiClient
) : BaseClient(configurationOnce, currentProfileOnce, sdkScope, uiScope),
    LiveLikeLeaderBoardClient {

    var leaderboardDelegate: LeaderBoardDelegate? = null
    private var leaderBoardEntryResult: HashMap<String, PaginationResponse<LeaderBoardEntry>> =
        hashMapOf()
    private var lastLeaderboardPage: PaginationResponse<LeaderBoardEntry>? = null
    private var lastLeaderboardViewsPage: PaginationResponse<LeaderBoardEntry>? = null
    override fun getLeaderBoardsForProgram(
        programId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<LeaderBoard>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val program = networkApiClient.get(pair.second.programDetailUrlTemplate.replace(TEMPLATE_PROGRAM_ID, programId)).processResult<Program>()
            program.leaderboards
        }
    }


    override fun getLeaderBoardDetails(
        leaderBoardId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LeaderBoard>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val url = pair.second.leaderboardDetailUrlTemplate.replace(
                TEMPLATE_LEADER_BOARD_ID, leaderBoardId
            )
            networkApiClient.get(url).processResult()
        }
    }


    override fun getEntriesForLeaderBoard(
        leaderBoardId: String,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<LeaderBoardEntry>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            when (liveLikePagination) {
                LiveLikePagination.FIRST -> {
                    val url = pair.second.leaderboardDetailUrlTemplate.replace(
                        TEMPLATE_LEADER_BOARD_ID, leaderBoardId
                    )
                    networkApiClient.get(url)
                        .processResult<LeaderBoardResource>().entriesUrl
                }

                LiveLikePagination.NEXT -> leaderBoardEntryResult[leaderBoardId]?.next
                LiveLikePagination.PREVIOUS -> leaderBoardEntryResult[leaderBoardId]?.previous
            }?.let { url ->
                networkApiClient.get(url)
                    .processResult<PaginationResponse<LeaderBoardEntry>>()
                    .also {
                        leaderBoardEntryResult[leaderBoardId] = it
                    }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }


    override fun getLeaderBoardEntryForProfile(
        leaderBoardId: String,
        profileId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LeaderBoardEntry>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val url = pair.second.leaderboardDetailUrlTemplate.replace(
                TEMPLATE_LEADER_BOARD_ID,
                leaderBoardId
            )
            networkApiClient.get(url).processResult<LeaderBoardResource>().run {
                networkApiClient.get(
                    entryDetailUrlTemplate.replace(
                        TEMPLATE_PROFILE_ID, profileId
                    )
                ).processResult()
            }
        }
    }


    override fun getLeaderBoardEntryForCurrentUserProfile(
        leaderBoardId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LeaderBoardEntry>
    ) {
        sdkScope.launch {
            val pair = configurationProfilePairOnce()
            getLeaderBoardEntryForProfile(leaderBoardId, pair.first.id, liveLikeCallback)
        }
    }


    override fun getLeaderboardClients(
        leaderBoardId: List<String>,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LeaderboardClient>
    ) {
        for (i in leaderBoardId.indices) {
            safeCallBack(liveLikeCallback.map(uiScope) {
                it.also {
                    leaderboardDelegate?.leaderBoard(
                        LeaderBoardForClient(
                            it.id,
                            it.name,
                            it.rewardItem
                        ), it.currentUserPlacement
                    )
                }
            }) { pair ->
                val url = pair.second.leaderboardDetailUrlTemplate.replace(
                    TEMPLATE_LEADER_BOARD_ID, leaderBoardId[i]
                )
                networkApiClient.get(url).processResult<LeaderBoardResource>()
                    .let { leaderBoardResource ->
                        networkApiClient.get(
                            leaderBoardResource.entryDetailUrlTemplate.replace(
                                TEMPLATE_PROFILE_ID, pair.first.id
                            )
                        ).processResult<LeaderBoardEntry>().let { leaderboardEntry ->
                            LeaderboardClient(
                                leaderBoardResource.id,
                                leaderBoardResource.name,
                                leaderBoardResource.rewardItem,
                                LeaderboardPlacement(
                                    leaderboardEntry.rank,
                                    leaderboardEntry.percentileRank.toString(),
                                    leaderboardEntry.score
                                ),
                                leaderboardDelegate!!
                            )
                        }
                    }
            }
        }
    }


    override fun getProfileLeaderboards(
        profileId: String,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<LeaderBoardEntry>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val url =
                if (lastLeaderboardPage == null || liveLikePagination == LiveLikePagination.FIRST) {
                    pair.second.profileLeaderboardsUrlTemplate.replace(
                        TEMPLATE_PROFILE_ID,
                        profileId
                    )
                } else if (liveLikePagination == LiveLikePagination.PREVIOUS) {
                    lastLeaderboardPage?.previous
                } else {
                    lastLeaderboardPage?.next
                }
            url?.let { fetchUrl ->
                networkApiClient.get(fetchUrl, accessToken = pair.first.accessToken)
                    .processResult<PaginationResponse<LeaderBoardEntry>>()
                    .also { lastLeaderboardPage = it }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }



    override fun getProfileLeaderboardViews(
        profileId: String,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<LeaderBoardEntry>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val url =
                if (lastLeaderboardViewsPage == null || liveLikePagination == LiveLikePagination.FIRST) {
                    pair.second.profileLeaderboardsViewsUrlTemplate.replace(
                        TEMPLATE_PROFILE_ID, profileId
                    )
                } else if (liveLikePagination == LiveLikePagination.PREVIOUS) {
                    lastLeaderboardViewsPage?.previous
                } else {
                    lastLeaderboardViewsPage?.next
                }
            url?.let { fetchUrl ->
                networkApiClient.get(fetchUrl, accessToken = pair.first.accessToken)
                    .processResult<PaginationResponse<LeaderBoardEntry>>().also {
                        lastLeaderboardViewsPage = it
                    }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }


}