package com.livelike.engagementsdk.widget.viewModel

import com.livelike.common.DataStoreDelegate
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.utils.safeCallBack
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.AnalyticsWidgetInteractionInfo
import com.livelike.engagementsdk.AnalyticsWidgetSpecificInfo
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.WidgetInfos
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.core.data.models.RewardsType
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.data.models.ProgramGamificationProfile
import com.livelike.engagementsdk.widget.domain.GamificationManager
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.model.Resource
import com.livelike.engagementsdk.widget.repository.ProgramRepository
import com.livelike.engagementsdk.widget.utils.addGamificationAnalyticsData
import com.livelike.engagementsdk.widget.utils.toAnalyticsString
import com.livelike.network.NetworkApiClient
import com.livelike.realtime.RealTimeClientMessage
import com.livelike.utils.Once
import com.livelike.utils.logDebug
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId

// TODO inherit all widget viewModels from here and  add widget common code here.
abstract class WidgetViewModel<T : Resource>(
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    protected var onDismiss: (() -> Unit)?,
    analyticsService: AnalyticsService,
    networkApiClient: NetworkApiClient,
    rewardItemMapCache: Map<String, RewardItem>,
    userProfileDelegate: UserProfileDelegate?,
    dataStoreDelegate: DataStoreDelegate,
    viewModelDispatcher: CoroutineDispatcher,
    uiDispatcher: CoroutineDispatcher
) : BaseViewModel(
    configurationOnce,
    currentProfileOnce,
    analyticsService,
    networkApiClient,
    rewardItemMapCache,
    userProfileDelegate,
    dataStoreDelegate,
    viewModelDispatcher,
    uiDispatcher
) {

    var timeOutJob: Job? = null
    lateinit var widgetInfos: WidgetInfos
    private var widgetMessagingClient: ((RealTimeClientMessage) -> Unit)? = null
    var programRepository: ProgramRepository? = null

    constructor(
        widgetInfos: WidgetInfos,
        configurationOnce: Once<SdkConfiguration>,
        currentProfileOnce: Once<LiveLikeProfile>,
        programRepository: ProgramRepository? = null,
        widgetMessagingClient: ((RealTimeClientMessage) -> Unit)? = null,
        onDismiss: (() -> Unit)?,
        analyticsService: AnalyticsService,
        networkApiClient: NetworkApiClient,
        rewardItemMapCache: Map<String, RewardItem>,
        userProfileDelegate: UserProfileDelegate?,
        dataStoreDelegate: DataStoreDelegate,
        viewModelDispatcher: CoroutineDispatcher,
        uiDispatcher: CoroutineDispatcher
    ) : this(
        configurationOnce,
        currentProfileOnce,
        onDismiss,
        analyticsService,
        networkApiClient,
        rewardItemMapCache,
        userProfileDelegate,
        dataStoreDelegate,
        viewModelDispatcher,
        uiDispatcher
    ) {
        this.widgetInfos = widgetInfos
        this.programRepository = programRepository
        this.widgetMessagingClient = widgetMessagingClient
    }

    private var timeoutStarted = false
    private var interactiveUntilTimeoutStarted = false

    val dataFlow = MutableStateFlow<T?>(null)
    val resultsFlow = MutableStateFlow<T?>(null)
    val stateFlow = MutableStateFlow<WidgetState?>(null)
    val currentVoteFlow = MutableStateFlow<String?>(null)
    val disableInteractionFlow = MutableStateFlow<Boolean?>(null)

    val gamificationProfile: StateFlow<ProgramGamificationProfile?>
        get() = programRepository?.programGamificationProfileFlow ?: MutableStateFlow(null)
    val rewardsType: RewardsType
        get() = programRepository?.rewardType ?: RewardsType.NONE

    var animationEggTimerProgress = 0f

    var currentWidgetId: String = ""
    var programId: String = ""
    var currentWidgetType: WidgetType? = null

    val interactionData = AnalyticsWidgetInteractionInfo()
    val widgetSpecificInfo = AnalyticsWidgetSpecificInfo()

    open fun confirmInteraction() {
        if (currentVoteFlow.value != null) {
            currentWidgetType?.let {
                programRepository?.programId?.let { programId ->
                    analyticsService.trackWidgetInteraction(
                        it.toAnalyticsString(),
                        currentWidgetId,
                        programId,
                        interactionData,
                        dataFlow.value?.question?:""

                    )
                }
            } ?: logDebug { "current widget type is null" }
            safeCallBack(uiScope) {
                dataFlow.value?.rewardsUrl?.let {
                    getGamificationReward(
                        it, analyticsService, currentProfileOnce.invoke().accessToken
                    )?.let { pts ->
                        programRepository?.programGamificationProfileFlow?.value = pts
                        widgetMessagingClient?.let { widgetMessagingClient ->
                            GamificationManager.checkForNewBadgeEarned(
                                pts, widgetMessagingClient
                            )
                        }
                        interactionData.addGamificationAnalyticsData(pts)
                    }
                } ?: logDebug { "widget data or widget data reward url is null" }
                delay(2000)
                stateFlow.value = WidgetState.SHOW_RESULTS
                delay(2000)
                stateFlow.value = WidgetState.SHOW_GAMIFICATION
            }
        }
    }

    abstract fun vote(value: String)

    fun startInteractionTimeout(timeout: Long, function: (() -> Unit)? = null) {
        if (!timeoutStarted) {
            timeoutStarted = true
            timeOutJob = uiScope.launch {
                delay(timeout)
                onInteractionCompletion(function)
            }
        }
    }


    fun startInteractiveUntilTimeout(timeout: Long) {
        if (!interactiveUntilTimeoutStarted) {
            interactiveUntilTimeoutStarted = true
            uiScope.launch {
                val timeDiff = timeout - Instant.now().atZone(ZoneId.of("UTC")).toInstant().toEpochMilli()
                delay(timeDiff)
                disableInteractionFlow.value = true
            }
        }
    }

    /**
     * will be called after timer completion or can be called manually after interaction finished
     **/
    fun onInteractionCompletion(function: (() -> Unit)?) {
        if (currentVoteFlow.value == null) {
            dismissWidget(DismissAction.TIMEOUT)
            function?.invoke()
        } else {
            stateFlow.value = WidgetState.LOCK_INTERACTION
            widgetStateFlow.value = WidgetStates.RESULTS
        }
        timeoutStarted = false
    }

    // FYI Right now this Widgetmodel is inherited by tutorial and gamification widgets, so dismiss analytics should be added in more concrete class.
    open fun dismissWidget(action: DismissAction) {
        stateFlow.value = WidgetState.DISMISS
        onDismiss?.invoke()
        onClear()
    }

    override fun onClear() {
        super.onClear()
        timeoutStarted = false
        onDismiss = null
    }
}

enum class WidgetState {
    LOCK_INTERACTION, // It is to indicate current interaction is done.
    SHOW_RESULTS, // It is to tell view to show results
    SHOW_GAMIFICATION, DISMISS
}
