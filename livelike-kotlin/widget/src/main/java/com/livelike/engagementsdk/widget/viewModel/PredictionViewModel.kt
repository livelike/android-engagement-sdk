package com.livelike.engagementsdk.widget.viewModel

import com.livelike.common.DataStoreDelegate
import com.livelike.common.model.RequestType
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.utils.safeCallBack
import com.livelike.common.utils.toStream
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.AnalyticsWidgetInteractionInfo
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.Stream
import com.livelike.engagementsdk.WidgetInfos
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.core.data.models.RewardsType
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.data.models.PredictionWidgetUserInteraction
import com.livelike.engagementsdk.widget.data.models.ProgramGamificationProfile
import com.livelike.engagementsdk.widget.data.respository.WidgetInteractionRepository
import com.livelike.engagementsdk.widget.domain.GamificationManager
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.model.LiveLikeWidgetResult
import com.livelike.engagementsdk.widget.model.Option
import com.livelike.engagementsdk.widget.model.Resource
import com.livelike.engagementsdk.widget.model.WidgetImpressions
import com.livelike.engagementsdk.widget.repository.ProgramRepository
import com.livelike.engagementsdk.widget.utils.addGamificationAnalyticsData
import com.livelike.engagementsdk.widget.utils.toAnalyticsString
import com.livelike.engagementsdk.widget.widgetModel.FollowUpWidgetViewModel
import com.livelike.engagementsdk.widget.widgetModel.PredictionWidgetViewModel
import com.livelike.network.NetworkApiClient
import com.livelike.realtime.RealTimeClientMessage
import com.livelike.serialization.gson
import com.livelike.utils.LiveLikeException
import com.livelike.utils.Once
import com.livelike.utils.formatIsoZoned8601
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.livelike.widget.RECYCLER_NO_POSITION
import com.livelike.widget.parseDuration
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime

class PredictionWidget(
    val type: WidgetType,
    val resource: Resource
)

class PredictionViewModel(
    val widgetInfos: WidgetInfos,
    analyticsService: AnalyticsService,
    configurationOnce: Once<SdkConfiguration>,
    var onDismiss: (() -> Unit)?,
    currentProfileOnce: Once<LiveLikeProfile>,
    private val programRepository: ProgramRepository? = null,
    private val widgetMessagingClient: ((RealTimeClientMessage) -> Unit)? = null,
    val widgetInteractionRepository: WidgetInteractionRepository?,
    networkApiClient: NetworkApiClient,
    rewardItemMapCache: Map<String, RewardItem>,
    userProfileRewardDelegate: UserProfileDelegate?,
    dataStoreDelegate: DataStoreDelegate,
    private var lottieAnimationPath: ((String) -> String?)?,
    viewModelDispatcher: CoroutineDispatcher,
    uiDispatcher: CoroutineDispatcher
) : BaseViewModel(
    configurationOnce,
    currentProfileOnce,
    analyticsService,
    networkApiClient,
    rewardItemMapCache,
    userProfileRewardDelegate,
    dataStoreDelegate,
    viewModelDispatcher,
    uiDispatcher
), PredictionWidgetViewModel,
    FollowUpWidgetViewModel {
    var followUp: Boolean = false
    var points: Int? = null
    val gamificationProfile: StateFlow<ProgramGamificationProfile?>
        get() = programRepository?.programGamificationProfileFlow ?: MutableStateFlow(null)
    val rewardsType: RewardsType
        get() = programRepository?.rewardType ?: RewardsType.NONE
    val dataFlow = MutableStateFlow<PredictionWidget?>(null)
    val resultsFlow = MutableStateFlow<Resource?>(null)

    private var timeoutStarted = false
    var animationProgress = 0f
    var animationEggTimerProgress = 0f
    var animationPath = ""

    private var currentWidgetId: String = ""
    private var programId: String = ""
    private var currentWidgetType: WidgetType? = null
    private val interactionData = AnalyticsWidgetInteractionInfo()
    var selectedOptionId: String? = ""
    private val correctOptionIdFlow = MutableStateFlow<String?>(null)
    val disableInteractionFlow = MutableStateFlow<Boolean?>(null)
    private var interactiveUntilTimeout = false

    init {
        widgetObserver(widgetInfos)
    }

    private fun widgetObserver(widgetInfos: WidgetInfos?) {
        if (widgetInfos != null) {
            WidgetType.fromString(widgetInfos.type)?.let { type ->
                if (listOf(
                        WidgetType.IMAGE_PREDICTION,
                        WidgetType.IMAGE_PREDICTION_FOLLOW_UP,
                        WidgetType.TEXT_PREDICTION,
                        WidgetType.TEXT_PREDICTION_FOLLOW_UP
                    ).contains(type)
                ) {
                    val resource =
                        gson.fromJson(widgetInfos.payload.toString(), Resource::class.java) ?: null
                    resource?.apply {
                        if (resource.pubnubEnabled) {
                            logDebug { "WidgetType:${resource.kind},id:${resource.id}" }
                            subscribeChannel?.let { it1 ->
                                subscribeWidgetResults(
                                    it1,
                                    widgetInfos.widgetId,
                                    resultsFlow
                                )
                            } ?: logError { "subscribeChannel is null" }
                        } else {
                            logDebug { "Pubnub not enabled" }
                        }
                        dataFlow.value = PredictionWidget(type, resource)
                    } ?: logDebug { "prediction resource is null" }

                    currentWidgetId = widgetInfos.widgetId
                    programId = dataFlow.value?.resource?.programId.toString()
                    currentWidgetType = type
                    interactionData.widgetDisplayed()
                } else {
                    dataFlow.value = null
                }
            }
        }
    }


    fun startDismissTimout(
        timeout: String,
        isFollowup: Boolean
    ) {
        if (!timeoutStarted && timeout.isNotEmpty()) {
            timeoutStarted = true
            if (isFollowup) {
                uiScope.launch {
                    delay(parseDuration(timeout))
                    dismissWidget(DismissAction.TIMEOUT)
                }
            } else {
                uiScope.launch {
                    delay(parseDuration(timeout))
                    confirmationState()
                }
            }
        }
    }

    fun dismissWidget(action: DismissAction) {
        currentWidgetType?.let {
            analyticsService.trackWidgetDismiss(
                it.toAnalyticsString(),
                currentWidgetId,
                programId,
                interactionData,
                selectionLockedFlow.value,
                action
            )
        } ?: logDebug { "current widget type is null" }
        widgetStateFlow.value = WidgetStates.FINISHED
        logDebug { "dismiss Prediction Widget, reason:${action.name}" }
        onDismiss?.invoke()
        cleanUp()
    }

    fun onOptionClicked() {
        safeCallBack(uiScope) {
            vote()
        }
        interactionData.incrementInteraction()
    }

    fun followupState(
        selectedPredictionId: String,
        correctOptionId: String?,
        widgetWinAnimation: String,
        widgetLoseAnimation: String
    ) {
        if (followUp)
            return
        followUp = true
        correctOptionIdFlow.value = correctOptionId
        userSelectedOptionIdFlow.value = selectedPredictionId
        selectionLockedFlow.value = true
        claimPredictionRewards()
        dataFlow.value = dataFlow.value?.apply {
            for (opt in resource.getMergedOptions() ?: emptyList()) {
                opt.percentage = opt.getPercent(resource.getMergedTotal().toFloat())
            }
        }

        val isUserCorrect = dataFlow.value?.resource?.getMergedOptions()
            ?.find { it.id == selectedPredictionId }?.isCorrect ?: false
        val rootPath = if (isUserCorrect) widgetWinAnimation else widgetLoseAnimation
        animationPath = if (selectedPredictionId.isNotEmpty()) {
            lottieAnimationPath?.invoke(rootPath) ?: ""
        } else {
            ""
        }
        safeCallBack(uiScope) {
            dataFlow.value?.resource?.rewardsUrl?.let {
                getGamificationReward(
                    it,
                    analyticsService,
                    currentProfileOnce().accessToken
                )?.let { pts ->
                    programRepository?.programGamificationProfileFlow?.value = pts
                    points = pts.newPoints
                    widgetMessagingClient?.let { widgetMessagingClient ->
                        GamificationManager.checkForNewBadgeEarned(pts, widgetMessagingClient)
                    }
                    interactionData.pointEarned = points ?: 0
                } ?: logDebug { "Program gamification profile is null" }
            } ?: logError { "Reward Url is null" }
            widgetStateFlow.value = WidgetStates.RESULTS
        }
        logDebug { "Prediction Widget Follow Up isUserCorrect:$isUserCorrect" }
    }

    private fun confirmationState() {
        selectionLockedFlow.value = true
        widgetStateFlow.value = WidgetStates.RESULTS
        logDebug { "Prediction Widget selected Position:${selectedPositionFlow.value}" }
        safeCallBack(uiScope) {
            dataFlow.value?.resource?.rewardsUrl?.let {
                getGamificationReward(
                    it,
                    analyticsService,
                    currentProfileOnce().accessToken
                )?.let { pts ->
                    programRepository?.programGamificationProfileFlow?.value = pts
                    points = pts.newPoints
                    widgetMessagingClient?.let { widgetMessagingClient ->
                        GamificationManager.checkForNewBadgeEarned(pts, widgetMessagingClient)
                    }
                    interactionData.addGamificationAnalyticsData(pts)
                }
            } ?: logError { "Reward Url is null" }
            delay(6000)
            dismissWidget(DismissAction.TIMEOUT)
        }
    }

    /** track widget interacted event*/
    private fun trackWidgetInteractedEvent(){
        currentWidgetType?.let {
            analyticsService.trackWidgetInteraction(
                it.toAnalyticsString(),
                currentWidgetId,
                programId,
                interactionData,
                widgetPrompt = dataFlow.value?.resource?.question ?: ""
            )
        } ?: logDebug { "current widget type is null" }
    }

    private fun cleanUp() {
        unsubscribeWidgetResults()
        uiScope.cancel()
        timeoutStarted = false
        animationProgress = 0f
        animationPath = ""
        dataFlow.value = null
        animationEggTimerProgress = 0f
        currentWidgetType = null
        currentWidgetId = ""
        interactionData.reset()
    }

    override fun onClear() {
        cleanUp()
        lottieAnimationPath = null
    }

    private fun claimPredictionRewards() {
        safeCallBack({ result, error ->
            result?.let { logDebug { it } }
            error?.let { logError { it } }
        }) {
            val resources =
                dataFlow.value ?: throw LiveLikeException("No Prediction data found")
            val widgetId =
                if (resources.resource.textPredictionId.isNullOrEmpty()) (resources.resource.imagePredictionId) else (resources.resource.textPredictionId)
            widgetInfos.widgetId = widgetId ?: ""
            widgetInteractionRepository?.fetchRemoteInteractions(
                widgetId = widgetInfos.widgetId,
                widgetKind = widgetInfos.type,
            ) ?: logDebug { "widget interaction repository is null" }

            voteAsync(
                resources.resource.claimUrl ?: throw LiveLikeException("Claim Url is null"),
                useVoteUrl = false,
                body = mapOf("claim_token" to (getUserInteraction()?.claimToken ?: "")),
                type = RequestType.POST,
                widgetId = currentWidgetId,
                patchVoteUrl = getUserInteraction()?.url,
                rewardItemMapCache = rewardItemMapCache,
                userProfileRewardDelegate = userProfileRewardDelegate,
                currentProfileOnce = currentProfileOnce
            )
        }
    }


    override val widgetData: LiveLikeWidget
        get() = gson.fromJson(widgetInfos.payload, LiveLikeWidget::class.java)

    override val voteResults: Stream<LiveLikeWidgetResult>
        get() = resultsFlow.map { it?.toLiveLikeWidgetResult() }.toStream(uiScope)
    override val voteResultsFlow: Flow<LiveLikeWidgetResult?>
        get() = resultsFlow.map { it?.toLiveLikeWidgetResult() }

    override fun getPredictionVoteId(): String {
        val resource = dataFlow.value?.resource
        return dataStoreDelegate.getWidgetPredictionVotedAnswerIdOrEmpty(if (resource?.textPredictionId.isNullOrEmpty()) resource?.imagePredictionId else resource?.textPredictionId)
    }

    override fun claimRewards() {
        claimPredictionRewards()
    }

    override fun finish() {
        onDismiss?.invoke()
        cleanUp()
    }

    override fun markAsInteractive() {
        trackWidgetBecameInteractive(currentWidgetType, currentWidgetId, programId)
    }

    override fun registerImpression(
        url: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<WidgetImpressions>
    ) {
        registerImpressionApi(url, liveLikeCallback)
    }

    override fun lockInVote(optionID: String) {
        trackWidgetEngagedAnalytics(
            currentWidgetType = currentWidgetType,
            currentWidgetId = currentWidgetId,
            programId = programId,
            widgetPrompt = dataFlow.value?.resource?.question?:""
        )
        safeCallBack({ result, error ->
            error?.let { logError { it } }
            result?.let { logDebug { it } }
        }) {
            val widget = dataFlow.value ?: throw LiveLikeException("Prediction data is null")
            val option = widget.resource.getMergedOptions()?.find { it.id == optionID }
                ?: throw LiveLikeException("No Option is Found")
            saveInteraction(option)
            dataStoreDelegate.addWidgetPredictionVoted(widget.resource.id, option.id)
            voteAsync(
                option.getMergedVoteUrl() ?: throw LiveLikeException("No Vote Url"),
                option.id,
                currentProfileOnce = currentProfileOnce,
                patchVoteUrl = getUserInteraction()?.url,
                rewardItemMapCache = rewardItemMapCache,
                userProfileRewardDelegate = userProfileRewardDelegate
            )
        }
    }

    override fun getUserInteraction(): PredictionWidgetUserInteraction? {
        return widgetInteractionRepository?.getWidgetInteraction(
            widgetInfos.widgetId
        )
    }

    override fun loadInteractionHistory(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<PredictionWidgetUserInteraction>>) {
        safeCallBack(uiScope, liveLikeCallback) {
            val results =
                widgetInteractionRepository?.fetchRemoteInteractions(
                    widgetId = widgetInfos.widgetId,
                    widgetKind = widgetInfos.type,
                    programUrlTemplate = configurationOnce().programDetailUrlTemplate
                )
            if (WidgetType.fromString(widgetInfos.type) == WidgetType.TEXT_PREDICTION) {
                results?.interactions?.textPrediction ?: emptyList()
            } else if (WidgetType.fromString(widgetInfos.type) == WidgetType.IMAGE_PREDICTION) {
                results?.interactions?.imagePrediction ?: emptyList()
            } else {
                emptyList()
            }
        }
    }


    internal fun saveInteraction(option: Option) {
        selectedOptionId = option.id
        widgetInteractionRepository?.saveWidgetInteraction(
            PredictionWidgetUserInteraction(
                option.id ?: "",
                "",
                ZonedDateTime.now().formatIsoZoned8601(),
                getUserInteraction()?.url,
                false,
                "",
                null,
                widgetInfos.widgetId,
                widgetInfos.type
            )
        )
    }

    //this starts timer for interactive until set
    fun startInteractiveUntilTimeout(timeout: Long) {
        if (!interactiveUntilTimeout) {
            interactiveUntilTimeout = true
            uiScope.launch {
                val timeDiff =
                    timeout - Instant.now().atZone(ZoneId.of("UTC")).toInstant().toEpochMilli()
                delay(timeDiff)
                disableInteractionFlow.value = true
            }
        }
    }

    @Suppress("USELESS_ELVIS")
    private suspend fun vote() {
        val widget = dataFlow.value ?: throw LiveLikeException("Prediction Widget data not found")
        if (selectedPositionFlow.value != RECYCLER_NO_POSITION) { // User has selected an option
            //this is done to update vote count locally for responsive UI
            updateLocalVoteCount() //increments vote count locally
            uiScope.launch {
                delay(parseDuration(widget.resource.timeout))
                confirmationState() //this rendered with updated UI
                trackWidgetInteractedEvent() //widget interacted event
            }
            val selectedOption = widget.resource.getMergedOptions()?.get(selectedPositionFlow.value)
            if (selectedOption != null) {
                saveInteraction(selectedOption)
            }

            // Prediction widget votes on dismiss

            voteAsync(
                selectedOption?.getMergedVoteUrl()
                    ?: throw LiveLikeException("vote url is null"),
                selectedOption.id,
                widgetId = currentWidgetId,
                patchVoteUrl = getUserInteraction()?.url,
                rewardItemMapCache = rewardItemMapCache,
                userProfileRewardDelegate = userProfileRewardDelegate,
                currentProfileOnce = currentProfileOnce
            )

            // Save widget id and voted option for followup widget
            dataStoreDelegate.addWidgetPredictionVoted(
                widget.resource.id ?: "",
                selectedOption.id ?: ""
            )
        }
    }


    /** This updates the local vote count, for more responsiveness
     * once local vote count is updated, then ui is updated with percentage*/
    private fun updateLocalVoteCount() {
        (dataFlow.value?.resource)?.apply {
            val optionResults = this.getMergedOptions() ?: return

            if (selectedOptionId?.isNotEmpty() == true) {
                optionResults.find {
                    it.id == selectedOptionId
                }?.apply {
                    this.voteCount = this.voteCount?.minus(1) //decrements count
                    if (this.voteCount!! < 0) this.voteCount = 0
                }
            }

            // increment vote count for the option id
            userSelectedOptionIdFlow.value = selectedPositionFlow.value.let { it1 ->
                if (it1 > -1)
                    return@let this.getMergedOptions()?.get(it1)?.id
                return@let null
            } ?: ""
            optionResults.find {
                it.id == userSelectedOptionIdFlow.value
            }?.apply {
                this.voteCount = this.voteCount?.plus(1)
            }
        }
    }
}
