package com.livelike.engagementsdk.sponsorship

import com.livelike.BaseClient
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.utils.validateUuid
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.LiveLikeChatClient
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.widget.repository.ProgramRepository
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.processResult
import com.livelike.utils.LiveLikeException
import com.livelike.utils.NO_MORE_DATA
import com.livelike.utils.Once
import com.livelike.utils.PaginationResponse
import com.livelike.widget.INVALID_PROGRAM_ID
import com.livelike.widget.SPONSOR_URL_NOT_FOUND_ERROR
import kotlinx.coroutines.CoroutineScope
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

/**
 * Sponsor client allowing to fetch all sponsor related stuff from Livelike's CMS
 */

internal class Sponsor(
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    sdkScope: CoroutineScope,
    private val chatClient: LiveLikeChatClient,
    uiScope: CoroutineScope,
    private val networkApiClient: NetworkApiClient
) : BaseClient(configurationOnce, currentProfileOnce, sdkScope, uiScope), ISponsor {

    private val sponsorTypeListResponseMap =
        hashMapOf<SponsorListType, PaginationResponse<SponsorModel>>()

    override fun fetchByProgramId(
        programId: String,
        pagination: LiveLikePagination,
        callback: com.livelike.common.LiveLikeCallback<List<SponsorModel>>
    ) {
        safeCallBack { pair ->
            if (!validateUuid(programId)) {
                throw LiveLikeException(INVALID_PROGRAM_ID)
            }
            val programRepository =
                ProgramRepository(programId, currentProfileOnce, networkApiClient)
            val result = programRepository.getProgramData(pair.second.programDetailUrlTemplate)
            result.sponsorsUrl?.let {
                fetchSponsorDetails(
                    SponsorListType.Program, it, pagination, callback
                )
            } ?: throw LiveLikeException(SPONSOR_URL_NOT_FOUND_ERROR)
        }
    }



    override fun fetchForApplication(
        pagination: LiveLikePagination,
        callback: com.livelike.common.LiveLikeCallback<List<SponsorModel>>
    ) {
        fetchSponsorDetails(
            SponsorListType.Application, null, pagination, callback
        )
    }


    override fun fetchByChatRoomId(
        chatRoomId: String,
        pagination: LiveLikePagination,
        callback: com.livelike.common.LiveLikeCallback<List<SponsorModel>>
    ) {
        safeCallBack {
            val chatRoom = suspendCoroutine { cont ->
                chatClient.getChatRoom(chatRoomId){result,error->
                    result?.let { cont.resume(it) }
                    error?.let { cont.resumeWithException(Exception(it)) }
                }
            }
            fetchSponsorDetails(
                SponsorListType.ChatRoom, chatRoom.sponsorsUrl, pagination, callback
            )
        }
    }

    private fun fetchSponsorDetails(
        sponsorListType: SponsorListType,
        sponsorUrl: String?,
        liveLikePagination: LiveLikePagination,
        callback: com.livelike.common.LiveLikeCallback<List<SponsorModel>>
    ) {
        safeCallBack(callback) { pair ->
            val response = sponsorTypeListResponseMap[sponsorListType]
            when (liveLikePagination) {
                LiveLikePagination.FIRST -> sponsorUrl ?: pair.second.sponsorsUrl
                LiveLikePagination.NEXT -> response?.next
                LiveLikePagination.PREVIOUS -> response?.previous
            }?.let { url ->
                networkApiClient.get(url, pair.first.accessToken)
                    .processResult<PaginationResponse<SponsorModel>>().also {
                        sponsorTypeListResponseMap[sponsorListType] = it
                    }.results
            } ?: throw Exception(NO_MORE_DATA)
        }
    }

}

internal enum class SponsorListType { Program, Application, ChatRoom }