package com.livelike.engagementsdk.widget.viewModel

import com.livelike.common.DataStoreDelegate
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.AnalyticsWidgetInteractionInfo
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.WidgetInfos
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.model.WidgetImpressions
import com.livelike.engagementsdk.widget.utils.toAnalyticsString
import com.livelike.engagementsdk.widget.widgetModel.SocialEmbedWidgetModel
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.gson
import com.livelike.utils.Once
import com.livelike.utils.logDebug
import com.livelike.widget.parseDuration
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class SocialEmbedViewModel(
    val widgetInfos: WidgetInfos,
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    analyticsService: AnalyticsService,
    networkApiClient: NetworkApiClient,
    private var onDismiss: (() -> Unit)?,
    rewardItemMapCache: Map<String, RewardItem>,
    userProfileRewardDelegate: UserProfileDelegate?,
    dataStoreDelegate: DataStoreDelegate,
    viewModelDispatcher: CoroutineDispatcher,
    uiDispatcher: CoroutineDispatcher
) : BaseViewModel(
    configurationOnce,
    currentProfileOnce,
    analyticsService,
    networkApiClient,
    rewardItemMapCache,
    userProfileRewardDelegate,
    dataStoreDelegate,
    viewModelDispatcher,
    uiDispatcher
), SocialEmbedWidgetModel {

    private var timeoutStarted = false
    val dataFlow = MutableStateFlow<LiveLikeWidget?>(null)
    private var currentWidgetType: WidgetType? = null
    private var currentWidgetId: String = ""
    private var programId: String = ""
    private val interactionData = AnalyticsWidgetInteractionInfo()

    init {
        dataFlow.value =
            gson.fromJson(widgetInfos.payload.toString(), LiveLikeWidget::class.java) ?: null
        widgetStateFlow.value = WidgetStates.READY
        interactionData.widgetDisplayed()
        currentWidgetType = WidgetType.fromString(widgetInfos.type)
        currentWidgetId = widgetInfos.widgetId
        programId = dataFlow.value?.programId ?: ""
    }

    fun dismissWidget(action: DismissAction) {
        dataFlow.value?.let { data ->
            currentWidgetType?.let {
                analyticsService.trackWidgetDismiss(
                    it.toAnalyticsString(),
                    widgetInfos.widgetId,
                    data.programId ?: "",
                    interactionData,
                    false,
                    action
                )
            }
        } ?: logDebug { "social embed data is null" }
        logDebug { "dismiss Social embed Widget, reason:${action.name}" }
        onDismiss?.invoke()
        onClear()
    }

    fun startDismissTimout(timeout: String, onDismiss: () -> Unit) {
        if (!timeoutStarted && timeout.isNotEmpty()) {
            timeoutStarted = true
            uiScope.launch {
                delay(parseDuration(timeout))
                dismissWidget(DismissAction.TIMEOUT)
                onDismiss?.invoke()
                timeoutStarted = false
            }
        }
    }

    override fun onClear() {
        super.onClear()
        dataFlow.value = null
        timeoutStarted = false
        currentWidgetType = null
        interactionData.reset()
        onDismiss = null
    }

    override val widgetData: LiveLikeWidget
        get() = gson.fromJson(widgetInfos.payload, LiveLikeWidget::class.java)


    override fun finish() {
        onDismiss?.invoke()
    }

    override fun markAsInteractive() {
        trackWidgetBecameInteractive(currentWidgetType, currentWidgetId, programId)
    }

    override fun registerImpression(
        url: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<WidgetImpressions>
    ) {
        registerImpressionApi(url, liveLikeCallback)
    }
}
