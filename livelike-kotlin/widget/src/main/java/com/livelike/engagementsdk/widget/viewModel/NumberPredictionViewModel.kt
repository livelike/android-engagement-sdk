package com.livelike.engagementsdk.widget.viewModel

import com.livelike.common.DataStoreDelegate
import com.livelike.common.model.RequestType
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.utils.safeCallBack
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.AnalyticsWidgetInteractionInfo
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.WidgetInfos
import com.livelike.engagementsdk.core.data.models.NumberPredictionVotes
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.core.data.models.VoteResponse
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.data.models.NumberPredictionWidgetUserInteraction
import com.livelike.engagementsdk.widget.data.respository.WidgetInteractionRepository
import com.livelike.engagementsdk.widget.domain.GamificationManager
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.model.Option
import com.livelike.engagementsdk.widget.model.Resource
import com.livelike.engagementsdk.widget.model.WidgetImpressions
import com.livelike.engagementsdk.widget.repository.ProgramRepository
import com.livelike.engagementsdk.widget.utils.addGamificationAnalyticsData
import com.livelike.engagementsdk.widget.utils.toAnalyticsString
import com.livelike.engagementsdk.widget.widgetModel.NumberPredictionFollowUpWidgetModel
import com.livelike.engagementsdk.widget.widgetModel.NumberPredictionWidgetModel
import com.livelike.network.NetworkApiClient
import com.livelike.realtime.RealTimeClientMessage
import com.livelike.serialization.gson
import com.livelike.utils.LiveLikeException
import com.livelike.utils.Once
import com.livelike.utils.formatIsoZoned8601
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.livelike.widget.parseDuration
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime

class NumberPredictionWidget(
    val type: WidgetType, val resource: Resource
)

class NumberPredictionViewModel(
    val widgetInfos: WidgetInfos,
    analyticsService: AnalyticsService,
    configurationOnce: Once<SdkConfiguration>,
    var onDismiss: (() -> Unit)?,
    currentProfileOnce: Once<LiveLikeProfile>,
    private val programRepository: ProgramRepository? = null,
    private val widgetMessagingClient: ((RealTimeClientMessage) -> Unit)? = null,
    val widgetInteractionRepository: WidgetInteractionRepository?,
    networkApiClient: NetworkApiClient,
    rewardItemMapCache: Map<String, RewardItem>,
    userProfileRewardDelegate: UserProfileDelegate?,
    dataStoreDelegate: DataStoreDelegate,
    viewModelDispatcher: CoroutineDispatcher,
    uiDispatcher: CoroutineDispatcher
) : BaseViewModel(
    configurationOnce,
    currentProfileOnce,
    analyticsService,
    networkApiClient,
    rewardItemMapCache,
    userProfileRewardDelegate,
    dataStoreDelegate,
    viewModelDispatcher,
    uiDispatcher
), NumberPredictionWidgetModel, NumberPredictionFollowUpWidgetModel {

    val dataFlow = MutableStateFlow<NumberPredictionWidget?>(null)
    private val resultFlow = MutableStateFlow<Resource?>(null)
    val disableInteractionFlow = MutableStateFlow<Boolean?>(null)
    private val pointsFlow = MutableStateFlow<Int?>(null)
    var numberPredictionFollowUp: Boolean = false
    private var currentWidgetId: String = ""
    private var programId: String = ""
    private var currentWidgetType: WidgetType? = null
    private val interactionData = AnalyticsWidgetInteractionInfo()


    private var timeoutStarted = false
    private var animationProgress = 0f
    var animationEggTimerProgress = 0f
    private var animationPath = ""
    private var interactiveUntilTimeout = false
    private var isVoteSubmitted = false



    init {
        widgetObserver(widgetInfos)
    }


    private fun widgetObserver(widgetInfos: WidgetInfos?) {
        if (widgetInfos != null) {
            val resource =
                gson.fromJson(widgetInfos.payload.toString(), Resource::class.java) ?: null
            resource?.apply {
                if (resource.pubnubEnabled) {
                    subscribeChannel?.let { it1 ->
                        subscribeWidgetResults(
                            it1,
                            widgetInfos.widgetId,
                            resultFlow
                        )
                    } ?: logError { "subscribeChannel is null" }
                } else {
                    logDebug { "Pubnub not enabled" }
                }
                dataFlow.value = WidgetType.fromString(widgetInfos.type)
                    ?.let { NumberPredictionWidget(it, resource) }
            } ?: logDebug { "number prediction reosurce is null" }
            currentWidgetId = widgetInfos.widgetId
            programId = dataFlow.value?.resource?.programId.toString()
            currentWidgetType = WidgetType.fromString(widgetInfos.type)
            interactionData.widgetDisplayed()
        }
    }

    /**
     * submission of prediction votes (prediction for all options is mandatory)
     */
    override fun lockInVote(
        options: List<NumberPredictionVotes>,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<VoteResponse>?
    ) {
        if (options.isEmpty()) return
        safeCallBack(liveLikeCallback) {
            val widget = dataFlow.value ?: throw LiveLikeException("Number Prediction data is null")
            if (options.size < widget.resource.getMergedOptions()?.size!!) {
                throw LiveLikeException("submit prediction for all options")
            }
            // save interaction locally
            saveInteraction(options)
            // Save widget id and voted options (number and option id) for followup widget
            dataStoreDelegate.addWidgetNumberPredictionVoted(widget.resource.id, options)
            val body = arrayListOf<Map<String, Any?>>()
            for (item in options) {
                body.add(mapOf("option_id" to item.optionId, "number" to item.number))
            }
            logDebug { "body-vote-${body}" }
            voteAsync(
                dataFlow.value?.resource?.voteUrl
                    ?: throw LiveLikeException("number prediction data is null or vote url is null"),
                body = mapOf("votes" to body),
                type = RequestType.POST,
                useVoteUrl = false,
                widgetId = currentWidgetId,
                rewardItemMapCache = rewardItemMapCache,
                userProfileRewardDelegate = userProfileRewardDelegate,
                currentProfileOnce = currentProfileOnce
            )
        }
    }

    /**
     * Returns the votes submitted
     */
    override fun getPredictionVotes(): List<NumberPredictionVotes> {
        val resource = dataFlow.value?.resource
        return getWidgetNumberPredictionVotedAnswerList(if (resource?.textNumberPredictionId.isNullOrEmpty()) resource?.imageNumberPredictionId else resource?.textNumberPredictionId)
    }

    /**
     * Returns associated prediction id for followups
     */
    private fun getNumberPredictionId(it: NumberPredictionWidget): String {
        if (it.resource.textNumberPredictionId.isNullOrEmpty()) {
            return it.resource.imageNumberPredictionId ?: ""
        }
        return it.resource.textNumberPredictionId
    }


    override fun claimRewards() {
        claimPredictionRewards()
    }

    /**
     * claim rewards
     */
    private fun claimPredictionRewards() {
        safeCallBack({ result, error ->
            result?.let { logDebug { it } }
            error?.let { logError { it } }
        }) {
            val resources =
                dataFlow.value ?: throw LiveLikeException("Number Prediction data is null")
            val widgetId = getNumberPredictionId(resources)
            widgetInfos.widgetId = widgetId
            widgetInteractionRepository?.fetchRemoteInteractions(
                widgetId = widgetInfos.widgetId, widgetKind = widgetInfos.type
            )
            var claimToken = predictionWidgetVoteRepository.get(
                getNumberPredictionId(resources)
            )
            if (claimToken.isNullOrEmpty()) claimToken = getUserInteraction()?.claimToken ?: ""
            voteAsync(
                resources.resource.claimUrl
                    ?: throw LiveLikeException("number prediction resource is null or claimUrl is null"),
                useVoteUrl = false,
                body = mapOf("claim_token" to claimToken),
                type = RequestType.POST,
                widgetId = currentWidgetId,
                rewardItemMapCache = rewardItemMapCache,
                userProfileRewardDelegate = userProfileRewardDelegate,
                currentProfileOnce = currentProfileOnce
            )
        }

    }

    /**
     * Returns the last interaction
     */
    override fun getUserInteraction(): NumberPredictionWidgetUserInteraction? {
        return widgetInteractionRepository?.getWidgetInteraction(widgetInfos.widgetId)
    }

    override fun loadInteractionHistory(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<NumberPredictionWidgetUserInteraction>>) {
        safeCallBack(uiScope, liveLikeCallback) {
            val results = widgetInteractionRepository?.fetchRemoteInteractions(
                widgetId = widgetInfos.widgetId,
                widgetKind = widgetInfos.type,
                programUrlTemplate = configurationOnce().programDetailUrlTemplate
            )
            if (WidgetType.fromString(widgetInfos.type) == WidgetType.TEXT_NUMBER_PREDICTION) {
                results?.interactions?.textNumberPrediction ?: emptyList()
            } else if (WidgetType.fromString(widgetInfos.type) == WidgetType.IMAGE_NUMBER_PREDICTION) {
                results?.interactions?.imageNumberPrediction ?: emptyList()
            } else {
                emptyList()
            }
        }
    }


    override val widgetData: LiveLikeWidget
        get() = gson.fromJson(widgetInfos.payload, LiveLikeWidget::class.java)


    override fun finish() {
        onDismiss?.invoke()
        onClear()
    }

    override fun markAsInteractive() {
        trackWidgetBecameInteractive(currentWidgetType, currentWidgetId, programId)
    }

    override fun registerImpression(
        url: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<WidgetImpressions>
    ) {
        registerImpressionApi(url, liveLikeCallback)
    }

    internal fun saveInteraction(option: List<NumberPredictionVotes>) {
        widgetInteractionRepository?.saveWidgetInteraction(
            NumberPredictionWidgetUserInteraction(
                "",
                "",
                ZonedDateTime.now().formatIsoZoned8601(),
                getUserInteraction()?.url,
                option,
                widgetInfos.widgetId,
                widgetInfos.type
            )
        ) ?: logDebug { "widget interaction repository is null" }
    }

    override fun onClear() {
        super.onClear()
        currentWidgetType = null
        currentWidgetId = ""
        timeoutStarted = false
        isVoteSubmitted = false
        animationProgress = 0f
        animationPath = ""
        dataFlow.value = null
        resultFlow.value = null
        animationEggTimerProgress = 0f
        interactionData.reset()
        onDismiss = null
    }

    fun dismissWidget(action: DismissAction) {
        currentWidgetType?.let {
            analyticsService.trackWidgetDismiss(
                it.toAnalyticsString(), currentWidgetId, programId, interactionData, false, action
            )
        } ?: logDebug { "current widget type is null" }
        widgetStateFlow.value = WidgetStates.FINISHED
        logDebug { "dismiss Number Prediction Widget, reason:${action.name}" }
        onDismiss?.invoke()
        onClear()
    }

    fun startDismissTimeout(
        timeout: String,
        isFollowup: Boolean,
        selectedUserVotes: List<NumberPredictionVotes>?
    ) {
        if (!timeoutStarted && timeout.isNotEmpty()) {
            timeoutStarted = true
            if (isFollowup) {
                uiScope.launch {
                    delay(parseDuration(timeout))
                    dismissWidget(DismissAction.TIMEOUT)
                }
            } else {
                uiScope.launch {
                    delay(parseDuration(timeout))
                    if (!isVoteSubmitted) {
                        lockInteractionAndSubmitVote(selectedUserVotes)
                    }
                    resultsState()

                }
            }
        }
    }

    fun startInteractiveUntilTimeout(timeout: Long) {
        if (!interactiveUntilTimeout) {
            interactiveUntilTimeout = true
            uiScope.launch {
                val timeDiff =
                    timeout - Instant.now().atZone(ZoneId.of("UTC")).toInstant().toEpochMilli()
                delay(timeDiff)
                disableInteractionFlow.value = true
            }
        }
    }

    fun lockInteractionAndSubmitVote(selectedUserVotes: List<NumberPredictionVotes>?) {
        isVoteSubmitted = true
        selectedUserVotes?.let {
            lockInVote(it) { result, error ->
                result?.let { logDebug { it } }
                error?.let { logError { it } }
            }
        }
    }


    private fun resultsState() {
        widgetStateFlow.value = WidgetStates.RESULTS
        currentWidgetType?.let {
            analyticsService.trackWidgetInteraction(
                it.toAnalyticsString(),
                currentWidgetId,
                programId,
                interactionData,
                widgetPrompt = dataFlow.value?.resource?.question ?: ""
            )
        } ?: logDebug { "current widget type is null" }
        uiScope.launch {
            delay(3000)
            dismissWidget(DismissAction.TIMEOUT)
        }
    }


    fun followupState(
        selectedPredictionVotes: List<NumberPredictionVotes>?
    ) {
        if (numberPredictionFollowUp) return
        numberPredictionFollowUp = true
        claimPredictionRewards()
        val isUserCorrect =
            isUserCorrect(selectedPredictionVotes, dataFlow.value?.resource?.options)
        safeCallBack(uiScope) {
            dataFlow.value?.resource?.rewardsUrl?.let {
                getGamificationReward(
                    it,
                    analyticsService,
                    currentProfileOnce().accessToken
                )?.let { pts ->
                    programRepository?.programGamificationProfileFlow?.value = pts
                    publishPoints(pts.newPoints)
                    widgetMessagingClient?.let { widgetMessagingClient ->
                        GamificationManager.checkForNewBadgeEarned(pts, widgetMessagingClient)
                    }
                    interactionData.addGamificationAnalyticsData(pts)
                } ?: logDebug { "Program Gamification profile is null" }
            } ?: logError { "Rewards Url is null" }
        }

        widgetStateFlow.value = WidgetStates.RESULTS
        logDebug { "Number Prediction Widget Follow Up isUserCorrect:$isUserCorrect" }
    }

    private fun publishPoints(pts: Int) {
        this.pointsFlow.value = pts
    }


    fun isUserCorrect(
        selectedPredictionVotes: List<NumberPredictionVotes>?, correctVotes: List<Option?>?
    ): Boolean {
        var isCorrect = false
        if (selectedPredictionVotes?.isEmpty() == true) return false
        correctVotes?.let { option ->
            if (option.size == selectedPredictionVotes?.size) {
                for (i in selectedPredictionVotes.indices) {
                    val votedOption = selectedPredictionVotes[i]
                    val op = option.find { it?.id == votedOption.optionId }
                    isCorrect = op != null && votedOption.number == op.correctNumber
                    op?.isCorrect = isCorrect
                }
            }
        } ?: logDebug { "correct votes are null" }
        return isCorrect
    }


    fun getWidgetNumberPredictionVotedAnswerList(id: String?): List<NumberPredictionVotes> {
        val votedList = dataStoreDelegate.getWidgetNumberPredictionVoted()
        if (votedList.isNotEmpty()) {
            val optionList: MutableList<NumberPredictionVotes> = mutableListOf()
            for (item in votedList) {
                if (item.id == id) optionList.add(NumberPredictionVotes(item.optionId, item.number))
            }
            return optionList.toList()
        }
        return emptyList()
    }

}