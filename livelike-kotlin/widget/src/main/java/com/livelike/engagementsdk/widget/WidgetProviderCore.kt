package com.livelike.engagementsdk.widget

import com.livelike.common.DataStoreDelegate
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.ViewAnimationEvents
import com.livelike.engagementsdk.WidgetInfos
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.widget.data.models.Badge
import com.livelike.engagementsdk.widget.data.respository.WidgetInteractionRepository
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.repository.ProgramRepository
import com.livelike.engagementsdk.widget.viewModel.AlertWidgetViewModel
import com.livelike.engagementsdk.widget.viewModel.BaseViewModel
import com.livelike.engagementsdk.widget.viewModel.CheerMeterViewModel
import com.livelike.engagementsdk.widget.viewModel.CollectBadgeWidgetViewModel
import com.livelike.engagementsdk.widget.viewModel.EmojiSliderWidgetViewModel
import com.livelike.engagementsdk.widget.viewModel.NumberPredictionViewModel
import com.livelike.engagementsdk.widget.viewModel.PointTutorialWidgetViewModel
import com.livelike.engagementsdk.widget.viewModel.PollViewModel
import com.livelike.engagementsdk.widget.viewModel.PredictionViewModel
import com.livelike.engagementsdk.widget.viewModel.QuizViewModel
import com.livelike.engagementsdk.widget.viewModel.SocialEmbedViewModel
import com.livelike.engagementsdk.widget.viewModel.TextAskViewModel
import com.livelike.engagementsdk.widget.viewModel.VideoWidgetViewModel
import com.livelike.network.NetworkApiClient
import com.livelike.realtime.RealTimeClientMessage
import com.livelike.serialization.gson
import com.livelike.utils.Once
import com.livelike.utils.logDebug
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow

open class WidgetProviderCore {

    fun getWidgetModel(
        widgetMessagingClient: ((RealTimeClientMessage) -> Unit)? = null,
        widgetInfos: WidgetInfos,
        analyticsService: AnalyticsService,
        configurationOnce: Once<SdkConfiguration>,
        currentProfileOnce: Once<LiveLikeProfile>,
        onDismiss: (() -> Unit)?,
        programRepository: ProgramRepository? = null,
        animationEventsFlow: MutableStateFlow<ViewAnimationEvents?>,
        widgetInteractionRepository: WidgetInteractionRepository? = null,
        networkApiClient: NetworkApiClient,
        rewardItemMapCache: Map<String, RewardItem>,
        userProfileRewardDelagate: UserProfileDelegate?,
        dataStoreDelegate: DataStoreDelegate,
        predictionLottieAnimationPath: ((String) -> String?)?,
        viewModelDispatcher: CoroutineDispatcher,
        uiDispatcher: CoroutineDispatcher
    ): BaseViewModel? {
        val baseViewModel = when (WidgetType.fromString(widgetInfos.type)) {
            WidgetType.ALERT -> AlertWidgetViewModel(
                configurationOnce,
                currentProfileOnce,
                widgetInfos,
                analyticsService,
                networkApiClient,
                onDismiss,
                rewardItemMapCache,
                userProfileRewardDelagate,
                dataStoreDelegate,
                viewModelDispatcher,
                uiDispatcher
            )

            WidgetType.VIDEO_ALERT -> VideoWidgetViewModel(
                widgetInfos,
                configurationOnce,
                currentProfileOnce,
                analyticsService,
                networkApiClient,
                onDismiss,
                rewardItemMapCache,
                userProfileRewardDelagate,
                dataStoreDelegate,
                viewModelDispatcher, uiDispatcher
            )

            WidgetType.TEXT_QUIZ, WidgetType.IMAGE_QUIZ -> QuizViewModel(
                widgetInfos,
                analyticsService,
                configurationOnce,
                currentProfileOnce,
                onDismiss,
                programRepository,
                widgetMessagingClient,
                widgetInteractionRepository,
                networkApiClient, rewardItemMapCache,
                userProfileRewardDelagate,
                dataStoreDelegate, viewModelDispatcher, uiDispatcher
            )

            WidgetType.IMAGE_PREDICTION, WidgetType.IMAGE_PREDICTION_FOLLOW_UP,
            WidgetType.TEXT_PREDICTION, WidgetType.TEXT_PREDICTION_FOLLOW_UP -> PredictionViewModel(
                widgetInfos,
                analyticsService,
                configurationOnce,
                onDismiss,
                currentProfileOnce,
                programRepository,
                widgetMessagingClient,
                widgetInteractionRepository,
                networkApiClient,
                rewardItemMapCache, userProfileRewardDelagate,
                dataStoreDelegate,
                predictionLottieAnimationPath, viewModelDispatcher, uiDispatcher
            )

            WidgetType.TEXT_POLL, WidgetType.IMAGE_POLL -> PollViewModel(
                widgetInfos,
                analyticsService,
                configurationOnce,
                onDismiss,
                currentProfileOnce,
                programRepository,
                widgetMessagingClient,
                widgetInteractionRepository,
                networkApiClient, rewardItemMapCache,
                userProfileRewardDelagate,
                dataStoreDelegate, viewModelDispatcher, uiDispatcher
            )

            WidgetType.POINTS_TUTORIAL -> PointTutorialWidgetViewModel(
                configurationOnce,
                currentProfileOnce,
                onDismiss,
                analyticsService,
                //programRepository?.rewardType ?: RewardsType.NONE,
                programRepository?.programGamificationProfileFlow?.value,
                networkApiClient,
                rewardItemMapCache,
                userProfileRewardDelagate,
                dataStoreDelegate,
                viewModelDispatcher, uiDispatcher
            )

            WidgetType.COLLECT_BADGE -> CollectBadgeWidgetViewModel(
                gson.fromJson(
                    widgetInfos.payload,
                    Badge::class.java
                ),
                configurationOnce,
                currentProfileOnce,
                onDismiss, analyticsService, animationEventsFlow,
                networkApiClient, rewardItemMapCache,
                userProfileRewardDelagate,
                dataStoreDelegate,
                viewModelDispatcher, uiDispatcher
            )

            WidgetType.CHEER_METER -> CheerMeterViewModel(
                widgetInfos,
                analyticsService,
                configurationOnce,
                onDismiss,
                currentProfileOnce,
                widgetInteractionRepository,
                networkApiClient, rewardItemMapCache,
                userProfileRewardDelagate,
                dataStoreDelegate, viewModelDispatcher, uiDispatcher
            )

            WidgetType.IMAGE_SLIDER -> EmojiSliderWidgetViewModel(
                widgetInfos, analyticsService, configurationOnce, onDismiss,
                currentProfileOnce, programRepository, widgetMessagingClient,
                widgetInteractionRepository,
                networkApiClient, rewardItemMapCache,
                userProfileRewardDelagate,
                dataStoreDelegate, viewModelDispatcher, uiDispatcher
            )

            WidgetType.SOCIAL_EMBED -> SocialEmbedViewModel(
                widgetInfos,
                configurationOnce,
                currentProfileOnce,
                analyticsService,
                networkApiClient,
                onDismiss,
                rewardItemMapCache,
                userProfileRewardDelagate,
                dataStoreDelegate,
                viewModelDispatcher,
                uiDispatcher
            )

            WidgetType.TEXT_ASK -> TextAskViewModel(
                widgetInfos,
                analyticsService,
                onDismiss,
                currentProfileOnce,
                configurationOnce,
                widgetInteractionRepository,
                networkApiClient,
                rewardItemMapCache,
                userProfileRewardDelagate,
                dataStoreDelegate, viewModelDispatcher, uiDispatcher
            )

            WidgetType.TEXT_NUMBER_PREDICTION, WidgetType.TEXT_NUMBER_PREDICTION_FOLLOW_UP,
            WidgetType.IMAGE_NUMBER_PREDICTION, WidgetType.IMAGE_NUMBER_PREDICTION_FOLLOW_UP -> NumberPredictionViewModel(
                widgetInfos,
                analyticsService,
                configurationOnce,
                onDismiss,
                currentProfileOnce,
                programRepository,
                widgetMessagingClient,
                widgetInteractionRepository,
                networkApiClient,
                rewardItemMapCache, userProfileRewardDelagate,
                dataStoreDelegate, viewModelDispatcher, uiDispatcher
            )

            else -> null
        }
        logDebug { "WidgetModel created from provider, type: ${WidgetType.fromString(widgetInfos.type)}" }
        return baseViewModel
    }
}