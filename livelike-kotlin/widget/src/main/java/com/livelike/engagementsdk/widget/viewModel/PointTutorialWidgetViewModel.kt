package com.livelike.engagementsdk.widget.viewModel


import com.livelike.common.DataStoreDelegate
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.widget.data.models.ProgramGamificationProfile
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.model.Resource
import com.livelike.network.NetworkApiClient
import com.livelike.utils.Once
import kotlinx.coroutines.CoroutineDispatcher

class PointTutorialWidgetViewModel(
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    onDismiss: (() -> Unit)?,
    analyticsService: AnalyticsService,
    val programGamificationProfile: ProgramGamificationProfile?,
    networkApiClient: NetworkApiClient,
    rewardItemMapCache: Map<String, RewardItem>,
    userProfileRewardDelegate: UserProfileDelegate?,
    dataStoreDelegate: DataStoreDelegate,
    viewModelDispatcher: CoroutineDispatcher,
    uiDispatcher: CoroutineDispatcher
) : WidgetViewModel<Resource>(
    configurationOnce,
    currentProfileOnce,
    onDismiss,
    analyticsService,
    networkApiClient,
    rewardItemMapCache,
    userProfileRewardDelegate,
    dataStoreDelegate,
    viewModelDispatcher, uiDispatcher
) {

    override fun dismissWidget(action: DismissAction) {
        super.dismissWidget(action)
        analyticsService.trackPointTutorialSeen(action.name, 5000L)
    }

    //     Actually for this we need to have layers of WidgetViewModel one with interaction and one without interaction. This tutorial and gamification will not call this ideally, we can move this in later tech debt tickets.
    override fun vote(value: String) {
    }
}
