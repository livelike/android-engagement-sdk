package com.livelike.engagementsdk.gamification


import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.core.data.models.LeaderBoard
import com.livelike.engagementsdk.core.data.models.LeaderBoardEntry
import com.livelike.engagementsdk.core.data.models.LeaderboardClient
import com.livelike.network.NetworkApiClient
import com.livelike.utils.Once
import kotlinx.coroutines.CoroutineScope


interface LiveLikeLeaderBoardClient {

    /**
     * Fetch leaderboard associated with a specific program
     *
     * @param programId : id of the program for which leaderboards needed
     **/
    fun getLeaderBoardsForProgram(
        programId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<LeaderBoard>>
    )


    /**
     * Fetch leaderboard details via leaderboard id
     *
     * @param leaderBoardId : id of the leaderboard for which details needed
     **/
    fun getLeaderBoardDetails(
        leaderBoardId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LeaderBoard>
    )

    /**
     * Get all entries of a leaderboard with pagination
     *
     * @param leaderBoardId : id of the leaderboard for which details needed
     * @param liveLikePagination : pagination for the entries
     **/
    fun getEntriesForLeaderBoard(
        leaderBoardId: String,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<LeaderBoardEntry>>
    )

    /**
     * Get leaderboard entries of profile with leaderboard id
     *
     * @param leaderBoardId : id of the leaderboard for which details are needed
     * @param profileId : user profile id, for which leaderboard is needed
     **/
    fun getLeaderBoardEntryForProfile(
        leaderBoardId: String,
        profileId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LeaderBoardEntry>
    )

    /**
     * Get leaderboard entries of current user profile with leaderboard id
     *
     ** @param leaderBoardId : id of the leaderboard
     **/
    fun getLeaderBoardEntryForCurrentUserProfile(
        leaderBoardId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LeaderBoardEntry>
    )

    /**
     * Fetch leaderboard clients
     ** @param leaderBoardId : id of the leaderboard
     **/
    fun getLeaderboardClients(
        leaderBoardId: List<String>,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LeaderboardClient>
    )

    /**
     * Fetch leaderboards on which the profile is ranked
     ** @param profileId : user id, based on which the leaderboards will be fetched on which the profile is ranked
     **/
    fun getProfileLeaderboards(
        profileId: String,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<LeaderBoardEntry>>
    )


    /**
     * Fetch leaderboards views on which the profile is ranked
     ** @param profileId : user id, based on which the leaderboards views will be fetched on which the profile is ranked
     **/
    fun getProfileLeaderboardViews(
        profileId: String,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<LeaderBoardEntry>>
    )



    companion object {

        fun getInstance(
            configurationOnce: Once<SdkConfiguration>,
            currentProfileOnce: Once<LiveLikeProfile>,
            sdkScope: CoroutineScope,
            uiScope: CoroutineScope,
            networkApiClient: NetworkApiClient
        ): LiveLikeLeaderBoardClient = InternalLiveLikeLeaderBoardClient(
            configurationOnce,
            currentProfileOnce,
            sdkScope,
            uiScope,
            networkApiClient
        )
    }
}