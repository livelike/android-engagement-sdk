package com.livelike.engagementsdk.widget.viewModel


import com.livelike.common.DataStoreDelegate
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.ViewAnimationEvents
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.widget.data.models.Badge
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.model.Resource
import com.livelike.network.NetworkApiClient
import com.livelike.utils.Once
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow

class CollectBadgeWidgetViewModel(
    val badge: Badge,
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    onDismiss: (() -> Unit)?,
    analyticsService: AnalyticsService,
    private val animationEventsFlow: MutableStateFlow<ViewAnimationEvents?>,
    networkApiClient: NetworkApiClient,
    rewardItemMapCache: Map<String, RewardItem>,
    userProfileRewardDelegate: UserProfileDelegate?,
    dataStoreDelegate: DataStoreDelegate,
    viewModelDispatcher: CoroutineDispatcher,
    uiDispatcher: CoroutineDispatcher
) : WidgetViewModel<Resource>(
    configurationOnce,
    currentProfileOnce,
    onDismiss,
    analyticsService,
    networkApiClient,
    rewardItemMapCache,
    userProfileRewardDelegate,
    dataStoreDelegate,
    viewModelDispatcher, uiDispatcher
) {

    override fun dismissWidget(action: DismissAction) {
        animationEventsFlow.value = ViewAnimationEvents.BADGE_COLLECTED
        super.dismissWidget(action)
    }

    override fun vote(value: String) {
    }
}
