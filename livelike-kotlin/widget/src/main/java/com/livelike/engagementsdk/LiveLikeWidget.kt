package com.livelike.engagementsdk

import com.google.gson.annotations.SerializedName
import com.livelike.engagementsdk.core.data.models.Attribute
import com.livelike.engagementsdk.widget.model.Option
import com.livelike.engagementsdk.widget.model.Resource

typealias LiveLikeWidget = Resource

class WidgetAttribute(
    key: String,
    value: String
) : Attribute(key, value)

data class RewardSummary(
    @field:SerializedName("reward_action_key")
    var rewardActionKey: String? = null,

    @field:SerializedName("reward_item_name")
    var rewardItemName: String? = null,

    @field:SerializedName("reward_item_amount")
    var rewardItemAmount: Int? = null,
)

data class EarnableReward(
    @field:SerializedName("reward_action_key")
    var rewardActionKey: String,

    @field:SerializedName("reward_item_name")
    var rewardItemName: String,

    @field:SerializedName("reward_item_amount")
    var rewardItemAmount: Int,

    @field:SerializedName("reward_item_id")
    var rewardItemId: String,
)

data class CreatedBy(

    @field:SerializedName("image_url")
    val imageUrl: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: String
)

typealias OptionsItem = Option

data class OptionReward(
    @field:SerializedName("reward_item_id")
    var rewardItemId: String? = null,

    @field:SerializedName("reward_item_name")
    var rewardItemName: String? = null,

    @field:SerializedName("reward_item_amount")
    var rewardItemAmount: Int? = null,

    @field:SerializedName("reward_item")
    var rewardItem: String? = null,
)

data class ReactionsItem(

    @field:SerializedName("sequence")
    val sequence: Int? = null,

    @field:SerializedName("file")
    val file: String? = null,

    @field:SerializedName("react_url")
    val reactUrl: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("count")
    val count: Int? = null,

    @field:SerializedName("mimetype")
    val mimetype: String? = null,

    @field:SerializedName("id")
    val id: String? = null
)
