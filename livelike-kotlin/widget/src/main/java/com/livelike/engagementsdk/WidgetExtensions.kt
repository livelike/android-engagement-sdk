package com.livelike.engagementsdk


import com.livelike.common.LiveLikeKotlin
import com.livelike.common.TimeCodeGetter
import com.livelike.common.map
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.profile
import com.livelike.common.utils.safeCallBack
import com.livelike.engagementsdk.chat.ImageSize
import com.livelike.engagementsdk.chat.LiveLikeChatSession
import com.livelike.engagementsdk.core.data.models.Program
import com.livelike.engagementsdk.gamification.InternalLiveLikeLeaderBoardClient
import com.livelike.engagementsdk.gamification.LiveLikeLeaderBoardClient
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.domain.LeaderBoardDelegate
import com.livelike.serialization.processResult
import com.livelike.widget.InternalLiveLikeWidgetClient
import com.livelike.widget.LiveLikeWidgetClient
import com.livelike.widget.TEMPLATE_WIDGET_ID
import com.livelike.widget.TEMPLATE_WIDGET_KIND
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.InputStream

/**
 *  Returns leaderboard client
 */
fun LiveLikeKotlin.leaderboard(): LiveLikeLeaderBoardClient {
    val key = this.hashCode()
    return if (sdkInstanceWithLeaderboardClient.containsKey(key)) {
        sdkInstanceWithLeaderboardClient[key]!!
    } else {
        LiveLikeLeaderBoardClient.getInstance(
            sdkConfigurationOnce,
            profile().currentProfileOnce,
            sdkScope,
            uiScope,
            networkClient
        ).let {
            sdkInstanceWithLeaderboardClient[key] = it
            it
        }
    }
}


private val sdkInstanceWithLeaderboardClient = mutableMapOf<Int, LiveLikeLeaderBoardClient>()


/**
 *  Get leaderboard delegate
 *
 */
var LiveLikeKotlin.leaderBoardDelegate: LeaderBoardDelegate?
    get() {
        return (leaderboard() as InternalLiveLikeLeaderBoardClient).leaderboardDelegate
    }
    set(value) {
        (leaderboard() as InternalLiveLikeLeaderBoardClient).leaderboardDelegate = value
    }

/**
 *  Fetch all widget details.
 *  @param widgetId id of he widget
 *  @param widgetKind widget kind*/

fun LiveLikeKotlin.fetchWidgetDetails(
    widgetId: String,
    widgetKind: String,
    liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeWidget>
) {
    safeCallBack(sdkScope, liveLikeCallback.map(uiScope)) {
        val config = sdkConfigurationOnce()
        val getWidgetUrl = config.widgetDetailUrlTemplate.replace(TEMPLATE_WIDGET_ID, widgetId)
            .replace(TEMPLATE_WIDGET_KIND, widgetKind)
        networkClient.get(getWidgetUrl).processResult()
    }
}

/**
 *  Get program details
 *  @param customID custom Id to get the program
 */
fun LiveLikeKotlin.getProgramByCustomID(
    customID: String,
    liveLikeCallback: com.livelike.common.LiveLikeCallback<Program>
) {
    sdkScope.launch {
        safeCallBack(sdkScope, liveLikeCallback.map(uiScope)) {
            val config = sdkConfigurationOnce()
            val customUrl =
                config.programCustomIdUrlTemplate
            val formattedUrl = customUrl.replace("{custom_id}", customID)
            networkClient.get(formattedUrl).processResult()
        }
    }
}

/**
 *  Creates a content session. */

fun LiveLikeKotlin.createContentSession(
    programId: String,
    timeCodeGetter: TimeCodeGetter,
    errorDelegate: ErrorDelegate? = null,
    connectToDefaultChatRoom: Boolean = true,
    includeFilteredChatMessages: Boolean = false,
    preLoadImage: suspend (String) -> Boolean = { false },
    isLayoutTransitionEnabled: Boolean = false,
    lottieAnimationPath: (String) -> String? = { null },
    sessionDispatcher: CoroutineDispatcher = Dispatchers.Default,
    uiDispatcher: CoroutineDispatcher = Dispatchers.Main,
    quoteChatMessageDeletedMessage: String = "Message deleted.",
    chatMessageDeletedMessage: String = "This message has been removed.",
    isPlatformLocalContentImageUrl: (String) -> Boolean = { false },
    getInputStreamForImageUrl: (String) -> InputStream? = { null },
    getSizeOfImage: (ByteArray) -> ImageSize = { ImageSize(100, 100) },
    chatSessionDispatcher: CoroutineDispatcher = Dispatchers.Default,
    chatMainDispatcher: CoroutineDispatcher = Dispatchers.Main,
    chatEnforceUniqueMessageCallback: Boolean = false
): LiveLikeContentSession {
    return ContentSession(
        sdkConfigurationOnce,
        profile().currentProfileOnce,
        rewardTypeFlow,
        programId,
        analyticService,
        errorDelegate,
        connectToDefaultChatRoom,
        networkClient,
        dataStoreDelegate = dataStoreDelegate,
        chatSession = createChatSession(
            timeCodeGetter = timeCodeGetter,
            errorDelegate = errorDelegate,
            includeFilteredChatMessages = includeFilteredChatMessages,
            quoteChatMessageDeletedMessage = quoteChatMessageDeletedMessage,
            chatMessageDeletedMessage = chatMessageDeletedMessage,
            isPlatformLocalContentImageUrl = isPlatformLocalContentImageUrl,
            getInputStreamForImageUrl = getInputStreamForImageUrl,
            getSizeOfImage = getSizeOfImage,
            sessionDispatcher = chatSessionDispatcher,
            mainDispatcher = chatMainDispatcher,
            chatEnforceUniqueMessageCallback = chatEnforceUniqueMessageCallback
        ),
        isLayoutTransitionEnabled,
        preLoadImage = { preLoadImage(it) },
        lottieAnimationPath = lottieAnimationPath,
        leaderboardClient = leaderboard(),
        leaderBoardDelegateHandler = {
            leaderBoardDelegate = it
        },
        sessionDispatcher = sessionDispatcher,
        uiDispatcher = uiDispatcher
    ) { timeCodeGetter() }.also {
        addSession(it)
    }
}

@Deprecated(
    "Use createContentSession",
    ReplaceWith("createContentSession(String,TimeCodeGetter,ErrorDelegate,Boolean,Boolean,(String) -> Boolean,Boolean,(String) -> String?,CoroutineDispatcher,CoroutineDispatcher,String,String,(String) -> Boolean,(String) -> InputStream?,(ByteArray) -> ImageSize,CoroutineDispatcher,CoroutineDispatcher)")
)
fun LiveLikeKotlin.createContentSession(
    programId: String,
    timecodeGetter: LiveLikeKotlin.TimecodeGetterCore,
    errorDelegate: ErrorDelegate? = null,
    connectToDefaultChatRoom: Boolean = true,
    includeFilteredChatMessages: Boolean = false,
    preLoadImage: suspend (String) -> Boolean = { false },
    isLayoutTransitionEnabled: Boolean = false,
    lottieAnimationPath: (String) -> String? = { null },
    sessionDispatcher: CoroutineDispatcher = Dispatchers.Default,
    uiDispatcher: CoroutineDispatcher = Dispatchers.Main,
    quoteChatMessageDeletedMessage: String = "Message deleted.",
    chatMessageDeletedMessage: String = "This message has been removed.",
    isPlatformLocalContentImageUrl: (String) -> Boolean = { false },
    getInputStreamForImageUrl: (String) -> InputStream? = { null },
    getSizeOfImage: (ByteArray) -> ImageSize = { ImageSize(100, 100) },
    chatSessionDispatcher: CoroutineDispatcher = Dispatchers.Default,
    chatMainDispatcher: CoroutineDispatcher = Dispatchers.Main,
    chatEnforceUniqueMessageCallback: Boolean = false
): LiveLikeContentSession {
    return createContentSession(
        programId,
        timeCodeGetter = { timecodeGetter.getTimecode() },
        errorDelegate,
        connectToDefaultChatRoom,
        includeFilteredChatMessages,
        preLoadImage,
        isLayoutTransitionEnabled,
        lottieAnimationPath,
        sessionDispatcher,
        uiDispatcher,
        quoteChatMessageDeletedMessage,
        chatMessageDeletedMessage,
        isPlatformLocalContentImageUrl,
        getInputStreamForImageUrl,
        getSizeOfImage,
        chatSessionDispatcher,
        chatMainDispatcher,
        chatEnforceUniqueMessageCallback
    )
}


fun LiveLikeKotlin.createContentSession(
    programId: String,
    timecodeGetter: TimeCodeGetter,
    errorDelegate: ErrorDelegate? = null,
    connectToDefaultChatRoom: Boolean = true,
    chatSession: LiveLikeChatSession,
    preLoadImage: suspend (String) -> Boolean,
    isLayoutTransitionEnabled: Boolean = false,
    lottieAnimationPath: ((String) -> String?)?,
    sessionDispatcher: CoroutineDispatcher,
    mainDispatcher: CoroutineDispatcher,
): LiveLikeContentSession {
    return ContentSession(
        sdkConfigurationOnce,
        profile().currentProfileOnce,
        rewardTypeFlow,
        programId,
        analyticService,
        errorDelegate,
        connectToDefaultChatRoom,
        networkClient,
        dataStoreDelegate = dataStoreDelegate,
        chatSession = chatSession,
        isLayoutTransitionEnabled,
        preLoadImage = { preLoadImage(it) },
        lottieAnimationPath = lottieAnimationPath,
        leaderboardClient = leaderboard(),
        leaderBoardDelegateHandler = {
            leaderBoardDelegate = it
        },
        sessionDispatcher = sessionDispatcher,
        uiDispatcher = mainDispatcher
    ) { timecodeGetter() }.also {
        addSession(it)
    }
}


fun SdkConfiguration.getWidgetDetailUrl(widgetId: String, widgetType: WidgetType) =
    widgetDetailUrlTemplate.replace(
        TEMPLATE_WIDGET_ID, widgetId
    ).replace(TEMPLATE_WIDGET_KIND, widgetType.getKindName())

fun LiveLikeKotlin.widget(): LiveLikeWidgetClient {
    return InternalLiveLikeWidgetClient(
        sdkConfigurationOnce,
        profile().currentProfileOnce,
        sdkScope,
        uiScope,
        networkClient
    )
}