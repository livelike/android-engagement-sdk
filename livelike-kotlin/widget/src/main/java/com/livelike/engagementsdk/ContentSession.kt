package com.livelike.engagementsdk

import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.google.gson.JsonParseException
import com.google.gson.JsonParser
import com.livelike.common.DataStoreDelegate
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.utils.BaseSession
import com.livelike.common.utils.toStream
import com.livelike.common.utils.validateUuid
import com.livelike.engagementsdk.chat.LiveLikeChatSession
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.core.data.models.LeaderBoardForClient
import com.livelike.engagementsdk.core.data.models.LeaderboardClient
import com.livelike.engagementsdk.core.data.models.LeaderboardPlacement
import com.livelike.engagementsdk.core.data.models.Program
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.core.data.models.RewardsType
import com.livelike.engagementsdk.core.services.messaging.proxies.LiveLikeWidgetEntity
import com.livelike.engagementsdk.core.services.messaging.proxies.WidgetInterceptor
import com.livelike.engagementsdk.gamification.LiveLikeLeaderBoardClient
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.engagementsdk.widget.WidgetProviderCore
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.data.models.PredictionWidgetUserInteraction
import com.livelike.engagementsdk.widget.data.models.ProgramGamificationProfile
import com.livelike.engagementsdk.widget.data.models.PublishedWidgetListResponse
import com.livelike.engagementsdk.widget.data.models.WidgetUserInteractionBase
import com.livelike.engagementsdk.widget.data.respository.WidgetInteractionRepository
import com.livelike.engagementsdk.widget.domain.LeaderBoardDelegate
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.model.GetPublishedWidgetsRequestOptions
import com.livelike.engagementsdk.widget.repository.ProgramRepository
import com.livelike.engagementsdk.widget.viewModel.BaseViewModel
import com.livelike.network.NetworkApiClient
import com.livelike.realtime.RealTimeClientMessage
import com.livelike.realtime.RealTimeMessagingClient
import com.livelike.realtime.RealTimeMessagingClientConfig
import com.livelike.realtime.internal.syncTo
import com.livelike.serialization.gson
import com.livelike.serialization.processResult
import com.livelike.utils.Once
import com.livelike.utils.PaginationResponse
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.livelike.utils.logVerbose
import com.livelike.utils.suspendLazy
import com.livelike.widget.PROGRAM_WIDGETS_URL_ERROR
import com.livelike.widget.TEMPLATE_PROFILE_ID
import com.livelike.widget.TEMPLATE_PROGRAM_ID
import com.livelike.widget.WIDGET_ATTRIBUTES
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import java.util.PriorityQueue
import java.util.Queue

class ContentSession(
    val configurationOnce: Once<SdkConfiguration>,
    val currentProfileOnce: Once<LiveLikeProfile>,
    private val rewardTypeFlow: MutableStateFlow<String?>,
    private val programId: String,
    val analyticService: AnalyticsService,
    private val errorDelegate: ErrorDelegate? = null,
    private val connectToDefaultChatRoom: Boolean,
    val networkApiClient: NetworkApiClient,
    val dataStoreDelegate: DataStoreDelegate,
    override val chatSession: LiveLikeChatSession,
    val isLayoutTransitionEnabled: Boolean,
    private val preLoadImage: suspend (String) -> Boolean,
    private var lottieAnimationPath: ((String) -> String?)?,
    private val leaderboardClient: LiveLikeLeaderBoardClient,
    private val leaderBoardDelegateHandler: (LeaderBoardDelegate) -> Unit,
    private val sessionDispatcher: CoroutineDispatcher,
    private val uiDispatcher: CoroutineDispatcher,
    private val currentPlayheadTime: () -> EpochTime
) : BaseSession(
    configurationOnce, currentProfileOnce, sessionDispatcher, uiDispatcher, errorDelegate
), LiveLikeContentSession {


    private var isGamificationEnabled: Boolean = false
    val rewardItemMapCache: MutableMap<String, RewardItem> = mutableMapOf()

    private var realTimeMessagingClient: RealTimeMessagingClient? = null
    val currentWidgetViewFlow = MutableStateFlow<Pair<String, WidgetInfos>?>(null)

    private val _widgetFlow = MutableStateFlow<LiveLikeWidget?>(null)

    override val widgetStream = _widgetFlow.toStream(uiScope, false)
    val animationEventsFlow = MutableStateFlow<ViewAnimationEvents?>(null)

    private var publishedWidgetListResponse: PublishedWidgetListResponse? = null
    private var unclaimedInteractionResponse: PaginationResponse<PredictionWidgetUserInteraction>? =
        null

    private var isReceivingRealtimeUpdates = false
    var widgetInteractionRepository: WidgetInteractionRepository
    var userProfileRewardDelegate: UserProfileDelegate? = null

    val programRepository = ProgramRepository(
        programId, currentProfileOnce, networkApiClient
    )

    internal val programOnce: Once<Program> = suspendLazy {
        while (programRepository.program == null) {
            delay(1000)
        }
        programRepository.program!!
    }

    init {
        sessionErrorHandlerScope.launch {
            launch {
                programOnce().let { program ->
                    if (connectToDefaultChatRoom && program.defaultChatRoom != null) {
                        chatSession.connectToChatRoom(
                            program.defaultChatRoom.id,callback = { result, error ->
                                error?.let {
                                    uiScope.launch { errorDelegate?.onError(it) }
                                    logError { it }
                                }
                            }
                        )
                    }
                }
            }
            launch {
                currentProfileOnce.flow().filterNotNull().collect { user ->
                    analyticService.trackUsername(user.nickname)
                }
            }
            launch {
                val pair = configurationProfilePairOnce()
                val latestConfiguration = pair.second

                logDebug { "analyticService created" }
                analyticService.trackSession(pair.first.id)
                analyticService.trackUsername(pair.first.nickname)
                analyticService.trackConfiguration(latestConfiguration.name)

                if (programId.isNotEmpty()) {
                    val program = networkApiClient.get(
                        latestConfiguration.programDetailUrlTemplate.replace(
                            TEMPLATE_PROGRAM_ID, programId
                        )
                    ).processResult<Program>()
                    programRepository.program = program
                    rewardTypeFlow.value = program.rewardsType
                    updateRewardItemCache(program.rewardItems)
                    isGamificationEnabled = program.rewardsType != RewardsType.NONE.key
                    if (program.pubnubEnabled) {
                        initializeWidgetMessaging(
                            program.subscribeChannel,
                            latestConfiguration,
                            pair.first.id,
                            pair.first.accessToken
                        )
                    } else {
                        logDebug { "Pubnub not enabled" }
                    }

                    if (isGamificationEnabled) programRepository.fetchProgramRank()

                    startObservingForGamificationAnalytics(
                        analyticService,
                        programRepository.programGamificationProfileFlow,
                        programRepository.rewardType
                    )
                    isReceivingRealtimeUpdates = true
                }
            }
            launch {
                currentWidgetViewFlow.collect {
                    logDebug { "Recieved Widget $it" }
                }
            }
        }

        widgetInteractionRepository = WidgetInteractionRepository(
            programID = programId,
            currentProfileOnce = currentProfileOnce,
            configurationOnce = configurationOnce,
            networkApiClient,
        )
    }

    private val widgetQueue: Queue<RealTimeClientMessage> = PriorityQueue(10) { p0, p1 ->
        p0.priority.compareTo(p1.priority)
    }
    private var widgetOnScreen = false
    private var pendingMessage: RealTimeClientMessage? = null

    @OptIn(FlowPreview::class)
    private fun initializeWidgetMessaging(
        subscribeChannel: String, config: SdkConfiguration, uuid: String, accessToken: String
    ) {
        if (!validateUuid(uuid)) {
            logError { "Widget Initialization Failed due no uuid compliant user id received for user" }
            return
        }
        analyticService.trackLastWidgetStatus(true)

        config.pubNubKey?.let { subscribeKey ->
            realTimeMessagingClient = RealTimeMessagingClient.getInstance(
                RealTimeMessagingClientConfig(
                    subscribeKey,
                    accessToken,
                    uuid,
                    config.pubnubPublishKey,
                    config.pubnubOrigin,
                    config.pubnubHeartbeatInterval,
                    config.pubnubPresenceTimeout
                ), sessionScope
            )

            realTimeMessagingClient = realTimeMessagingClient?.syncTo(
                currentPlayheadTime, dispatcher = Dispatchers.Default, sessionScope = sessionScope
            )
            sessionScope.launch {
                realTimeMessagingClient!!.messageClientFlow.filter {
                    val widgetType = WidgetType.fromString(it.event)
                    widgetType != null
                }.flatMapConcat {
                    preLoadImagesFromJson(it)
                }.collect {
                    _widgetFlow.value = gson.fromJson(it.payload, LiveLikeWidget::class.java)
                    widgetQueue.add(it)
                    if (!widgetOnScreen) {
                        publishNextInQueue()
                    }
                }
            }
            realTimeMessagingClient?.subscribe(hashSetOf(subscribeChannel).toList())
        } ?: logDebug { "SDK Config pubnubkey is null" }

        logDebug { "initialized Widget Messaging" }
    }



    override fun subscribeToChannels(channels: List<String>): Flow<RealTimeClientMessage>? {
        realTimeMessagingClient?.subscribe(channels)

       return realTimeMessagingClient?.messageClientFlow?.filter { channels.contains(it.channel) }
    }


    override fun unsubscribeFromChannels(channels: List<String>) {
        realTimeMessagingClient?.unsubscribe(channels)
    }



    private fun publishNextInQueue() {
        logDebug { "publishNextInQueue ${widgetQueue.size}" }
        if (widgetQueue.isNotEmpty()) {
            widgetOnScreen = true
            notifyIntegrator(widgetQueue.remove())
        } else {
            widgetOnScreen = false
            currentWidgetViewFlow.value =
                null // sometimes widget view obscure the whole screen chat due t which app seems unresponsive
        }
    }

    private fun notifyIntegrator(message: RealTimeClientMessage) {
        val widgetType = WidgetType.fromString(message.event)
        if (widgetInterceptor == null || widgetType == WidgetType.POINTS_TUTORIAL || widgetType == WidgetType.COLLECT_BADGE) {
            showWidgetOnScreen(message)
        } else {
            uiScope.launch {
                // Need to assure we are on the main thread to communicated with the external activity
                try {
                    widgetInterceptor?.widgetWantsToShow(
                        gson.fromJson(message.payload, LiveLikeWidgetEntity::class.java)
                    )
                } catch (e: Exception) {
                    logError { "Widget interceptor encountered a problem: $e \n Dismissing the widget" }
                    dismissPendingMessage()
                }
            }
            pendingMessage = message
        }
    }

    private fun showPendingMessage() {
        pendingMessage?.let {
            showWidgetOnScreen(it)
        } ?: logDebug { "pendingMessage is null" }
    }

    private fun dismissPendingMessage() {
        publishNextInQueue()
    }

    private fun showWidgetOnScreen(message: RealTimeClientMessage) {
        val widgetType = message.event
        val payload = message.payload
        val widgetId = payload["id"].asString
        val pair = widgetType to WidgetInfos(widgetType, payload, widgetId) {
            logDebug { "Dismiss Click" }
            checkForPointTutorial()
            publishNextInQueue()
        }
        logDebug { "showWidgetOnScreen1 ${currentWidgetViewFlow.value}" }
        logDebug { "showWidgetOnScreen2 $pair" }
        logDebug { "Is Same Widget Pair : ${currentWidgetViewFlow.value?.equals(pair)}" }
        val check = currentWidgetViewFlow.tryEmit(pair)
        logDebug { "current:${currentWidgetViewFlow.value},is Set:$check" }
        // Register the impression on the backend
        payload.get("impression_url")?.asString?.let {
            registerImpression(it)
        } ?: logError { "Impression Url is Null" }
    }

    private fun checkForPointTutorial() {
        if (dataStoreDelegate.shouldShowPointTutorial()) {
            // Check if user scored points
            if (dataStoreDelegate.getTotalPoints() != 0) {
                val message = RealTimeClientMessage(
                    "points-tutorial",
                    JsonObject().apply {
                        addProperty("id", "gameification")
                    },
                    0L,
                    "",
                    "",
                    3,
                )
                notifyIntegrator(message)
            }
        }
    }

    private fun preLoadImagesFromJson(realTimeClientMessage: RealTimeClientMessage): Flow<RealTimeClientMessage> =
        flow {
            val imageList = getImagesFromJson(realTimeClientMessage.payload, mutableListOf())
            if (imageList.isNotEmpty()) {
                var index = 0
                var reloadThreshold = 0
                while (index < imageList.size) {
                    if (preLoadImage.invoke(imageList[index]) || reloadThreshold > 3) {
                        index++
                        reloadThreshold = 0
                    } else {
                        reloadThreshold++
                    }
                }
            }
            emit(realTimeClientMessage)
        }

    private fun getImagesFromJson(
        jsonObject: JsonObject, imagesList: MutableList<String>
    ): MutableList<String> {
        val elements = jsonObject.entrySet()
        for (element in elements) {
            when {
                element.key == "image_url" -> {
                    if (!element.value.isJsonNull && !imagesList.contains(element.value.asString)) {
                        imagesList.add(element.value.asString)
                    }
                }

                element.value.isJsonObject -> getImagesFromJson(
                    element.value.asJsonObject, imagesList
                )

                element.value.isJsonArray -> for (it in element.value.asJsonArray) {
                    if (it.isJsonObject) {
                        getImagesFromJson(it.asJsonObject, imagesList)
                    }
                }
            }
        }
        return imagesList
    }

    private fun updateRewardItemCache(rewardItems: List<RewardItem>) {
        for (it in rewardItems) {
            rewardItemMapCache[it.id] = it
        }
    }

    private fun registerImpression(url: String) {
        safeCallBack({ result, error ->
            result?.let { logDebug { "Register Impression Successfully" } }
            error?.let { logError { it } }
        }) {
            networkApiClient.post(url, accessToken = currentProfileOnce().accessToken)
                .processResult()
        }
    }

    private fun startObservingForGamificationAnalytics(
        analyticService: AnalyticsService,
        programGamificationProfileFlow: Flow<ProgramGamificationProfile?>,
        rewardType: RewardsType
    ) {
        if (rewardType != RewardsType.NONE) {
            uiScope.launch {
                programGamificationProfileFlow.collect {
                    it?.let {
                        analyticService.trackPointThisProgram(it.points)
                        if (rewardType == RewardsType.BADGES) {/*  if (it.points == 0 && it.currentBadge == null) {
                                  analyticService.registerSuperProperty(
                                      AnalyticsSuperProperties.TIME_LAST_BADGE_AWARD,
                                      null
                                  )
                                  analyticService.registerSuperProperty(
                                      AnalyticsSuperProperties.BADGE_LEVEL_THIS_PROGRAM,
                                      0
                                  )
                              } else if (it.currentBadge != null && it.newBadges?.isNotEmpty() == true) {
                                  analyticService.registerSuperProperty(
                                      AnalyticsSuperProperties.TIME_LAST_BADGE_AWARD,
                                      ZonedDateTime.now().formatIsoZoned8601()
                                  )
                                  analyticService.registerSuperProperty(
                                      AnalyticsSuperProperties.BADGE_LEVEL_THIS_PROGRAM,
                                      it.currentBadge.level
                                  )
                              }*/
                        }
                    }
                }
            }
        }
    }

    override var contentSessionleaderBoardDelegate: LeaderBoardDelegate? = null

    override val widgetFlow: Flow<LiveLikeWidget>
        get() = _widgetFlow.filterNotNull()


// ////// Global Session Controls ////////

    override fun pause() {
        logVerbose { "Pausing the Session" }
        realTimeMessagingClient?.stop()
        analyticService.trackLastChatStatus(false)
        analyticService.trackLastWidgetStatus(false)
    }

    override fun resume() {
        logVerbose { "Resuming the Session" }
        realTimeMessagingClient?.start()
        if (isGamificationEnabled) safeCallBack { programRepository.fetchProgramRank() }
        analyticService.trackLastChatStatus(true)
        analyticService.trackLastWidgetStatus(true)
    }

    override fun close() {
        logVerbose { "Closing the Session" }
        isReceivingRealtimeUpdates = false
        destroy()
        realTimeMessagingClient?.destroy()
        currentWidgetViewFlow.value = null
        chatSession.close()
        analyticService.trackLastChatStatus(false)
        analyticService.trackLastWidgetStatus(false)
    }

    override fun getPlayheadTime(): EpochTime {
        return currentPlayheadTime()
    }

    override fun contentSessionId() = programId

    override var widgetInterceptor: WidgetInterceptor? = null
        set(value) {
            field = value
            value?.let { wi ->
                uiScope.launch {
                    wi.eventsFlow.collect {
                        when (it) {
                            WidgetInterceptor.Decision.Show -> showPendingMessage()
                            WidgetInterceptor.Decision.Dismiss -> dismissPendingMessage()
                            else -> {}
                        }
                    }
                }
            }
        }


    override fun getPublishedWidgets(
        getPublishedWidgetsRequestOptions: GetPublishedWidgetsRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<LiveLikeWidget>>
    ) {
        safeCallBack(liveLikeCallback) {
            val program = programOnce()
            program.timelineUrl.let { url ->
                val innerUrl = when (getPublishedWidgetsRequestOptions.liveLikePagination) {
                    LiveLikePagination.FIRST -> url
                    LiveLikePagination.NEXT -> publishedWidgetListResponse?.next
                    LiveLikePagination.PREVIOUS -> publishedWidgetListResponse?.previous
                } ?: return@safeCallBack null
                val queryParams = arrayListOf<Pair<String, String>>()
                getPublishedWidgetsRequestOptions.let {
                    it.sincePlaybackTimeMs?.let { sincePlayback ->
                        queryParams.add("since_playback_time_ms" to sincePlayback.toString())
                    }
                    it.untilPlaybackTimeMs?.let { untilPlayback ->
                        queryParams.add("until_playback_time_ms" to untilPlayback.toString())
                    }
                    it.widgetAttributes?.let { attributes ->
                        for (entry in attributes) {
                            queryParams.add(WIDGET_ATTRIBUTES to "${entry.key}:${entry.value}")
                        }
                    }
                }
                buildWidgetList(innerUrl, queryParams)
            }
        }
    }

    override fun getRewardItems(): List<RewardItem> {
        return programRepository.program?.rewardItems ?: listOf()
    }

    override fun getLeaderboardClients(
        leaderBoardId: List<String>,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LeaderboardClient>
    ) {
        leaderboardClient.getLeaderboardClients(leaderBoardId, liveLikeCallback)
        val leaderBoardDelegate = object : LeaderBoardDelegate {
            override fun leaderBoard(
                leaderBoard: LeaderBoardForClient,
                currentUserPlacementDidChange: LeaderboardPlacement
            ) {
                contentSessionleaderBoardDelegate?.leaderBoard(
                    leaderBoard, currentUserPlacementDidChange
                )
            }
        }
        leaderBoardDelegateHandler.invoke(leaderBoardDelegate)
    }

    override fun getWidgetInteractionsWithUnclaimedRewards(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<PredictionWidgetUserInteraction>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val url = when (liveLikePagination) {
                LiveLikePagination.FIRST -> programOnce().unclaimedWidgetInteractionsUrlTemplate?.replace(
                    TEMPLATE_PROFILE_ID, pair.first.id
                )

                LiveLikePagination.NEXT -> unclaimedInteractionResponse?.next
                LiveLikePagination.PREVIOUS -> unclaimedInteractionResponse?.previous
            } ?: return@safeCallBack emptyList()
            networkApiClient.get(url, accessToken = pair.first.accessToken)
                .processResult<PaginationResponse<PredictionWidgetUserInteraction>>().also {
                    unclaimedInteractionResponse = it
                }.run {
                    results.filter {
                        var widgetType = it.widgetKind
                        widgetType = if (widgetType.contains("follow-up")) {
                            "$widgetType-updated"
                        } else {
                            "$widgetType-created"
                        }
                        return@filter WidgetType.fromString(widgetType) != null
                    }
                }
        }
    }


    override fun getWidgetInteraction(
        widgetId: String,
        widgetKind: String,
        widgetInteractionUrl: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<WidgetUserInteractionBase>
    ) {
        safeCallBack(liveLikeCallback) {
            val interactionResult = widgetInteractionRepository.fetchRemoteInteractions(
                widgetId = widgetId,
                widgetKind = widgetKind,
                widgetInteractionUrl = widgetInteractionUrl,
                programUrlTemplate = it.second.programDetailUrlTemplate
            )
            interactionResult?.interactions.let {
                var widgetType = widgetKind
                widgetType = if (widgetType.contains("follow-up")) {
                    "$widgetType-updated"
                } else {
                    "$widgetType-created"
                }
                when (WidgetType.fromString(widgetType)) {
                    WidgetType.TEXT_PREDICTION_FOLLOW_UP, WidgetType.TEXT_PREDICTION -> it?.textPrediction?.firstOrNull()
                    WidgetType.IMAGE_PREDICTION_FOLLOW_UP, WidgetType.IMAGE_PREDICTION -> it?.imagePrediction?.firstOrNull()
                    WidgetType.IMAGE_POLL -> it?.imagePoll?.firstOrNull()
                    WidgetType.TEXT_POLL -> it?.textPoll?.firstOrNull()
                    WidgetType.IMAGE_QUIZ -> it?.imageQuiz?.firstOrNull()
                    WidgetType.TEXT_QUIZ -> it?.textQuiz?.firstOrNull()
                    WidgetType.CHEER_METER -> it?.cheerMeter?.firstOrNull()
                    WidgetType.IMAGE_SLIDER -> it?.emojiSlider?.firstOrNull()
                    WidgetType.TEXT_NUMBER_PREDICTION_FOLLOW_UP, WidgetType.TEXT_NUMBER_PREDICTION -> it?.textNumberPrediction?.firstOrNull()
                    WidgetType.IMAGE_NUMBER_PREDICTION_FOLLOW_UP, WidgetType.IMAGE_NUMBER_PREDICTION -> it?.imageNumberPrediction?.firstOrNull()
                    else -> null
                }
            }
        }
    }

    override fun getWidgetInteractions(
        widgetKinds: List<String>,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<Map<String, List<WidgetUserInteractionBase>>>
    ) {
        safeCallBack(liveLikeCallback) {
            val interactionResult = widgetInteractionRepository.fetchRemoteInteractions(
                widgetKinds = widgetKinds,
                widgetId = null,
                widgetKind = null,
                programUrlTemplate = it.second.programDetailUrlTemplate
            )
            val map = mutableMapOf<String, List<WidgetUserInteractionBase>>()
            interactionResult?.interactions?.run {
                for (kind in widgetKinds) {
                    val list = when (WidgetType.findFromString(kind)) {
                        WidgetType.TEXT_PREDICTION_FOLLOW_UP, WidgetType.TEXT_PREDICTION -> textPrediction
                        WidgetType.IMAGE_PREDICTION_FOLLOW_UP, WidgetType.IMAGE_PREDICTION -> imagePrediction
                        WidgetType.IMAGE_POLL -> imagePoll
                        WidgetType.TEXT_POLL -> textPoll
                        WidgetType.IMAGE_QUIZ -> imageQuiz
                        WidgetType.TEXT_QUIZ -> textQuiz
                        WidgetType.CHEER_METER -> cheerMeter
                        WidgetType.IMAGE_SLIDER -> emojiSlider
                        WidgetType.TEXT_NUMBER_PREDICTION_FOLLOW_UP, WidgetType.TEXT_NUMBER_PREDICTION -> textNumberPrediction
                        WidgetType.IMAGE_NUMBER_PREDICTION_FOLLOW_UP, WidgetType.IMAGE_NUMBER_PREDICTION -> imageNumberPrediction
                        else -> emptyList()
                    }
                    map[kind] = list ?: emptyList()
                }
            }
            map
        }
    }

    override fun getWidgetModelFromJson(widgetResourceJson: JsonObject): BaseViewModel? {
        var widgetType = widgetResourceJson.get("kind").asString
        widgetType = if (widgetType.contains("follow-up")) {
            "$widgetType-updated"
        } else {
            "$widgetType-created"
        }
        val widgetId = widgetResourceJson["id"].asString
        return WidgetProviderCore().getWidgetModel(
            null,
            WidgetInfos(widgetType, widgetResourceJson, widgetId),
            analyticService,
            configurationOnce,
            currentProfileOnce,
            {
                //currentWidgetViewStream.onNext(null)
            },
            null,
            MutableStateFlow(null),
            widgetInteractionRepository,
            networkApiClient,
            rewardItemMapCache,
            userProfileRewardDelegate,
            dataStoreDelegate,
            lottieAnimationPath,
            viewModelDispatcher = sessionDispatcher,
            uiDispatcher = uiDispatcher
        )
    }

    override fun getWidgetModelFromLiveLikeWidget(liveLikeWidget: LiveLikeWidget): BaseViewModel? {
        try {
            val jsonObject = GsonBuilder().create().toJson(liveLikeWidget)
            return getWidgetModelFromJson(JsonParser.parseString(jsonObject).asJsonObject)
        } catch (ex: JsonParseException) {
            logDebug { "Invalid json passed for get WidgetModel" }
            ex.printStackTrace()
        }
        return null
    }

    override fun getWidgets(
        liveLikePagination: LiveLikePagination,
        requestParams: WidgetsRequestParameters?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<LiveLikeWidget>>
    ) {
        safeCallBack(liveLikeCallback) {
            programOnce().widgetsUrl?.let { url ->
                val innerUrl = when (liveLikePagination) {
                    LiveLikePagination.FIRST -> url
                    LiveLikePagination.NEXT -> publishedWidgetListResponse?.next
                    LiveLikePagination.PREVIOUS -> publishedWidgetListResponse?.previous
                } ?: return@safeCallBack emptyList()
                val params = arrayListOf<Pair<String, String>>()
                requestParams?.run {
                    widgetStatus?.let {
                        params.add("status" to it.parameterValue)
                    }
                    ordering?.let {
                        params.add("ordering" to it.parameterValue)
                    }
                    for (type in widgetTypeFilter) {
                        params.add("kind" to type.getKindName())
                    }
                    interactive?.let {
                        params.add("interactive" to it.toString())
                    }
                    since?.let {
                        params.add("since" to it)
                    }
                }
                buildWidgetList(innerUrl, params)
            } ?: throw Exception(PROGRAM_WIDGETS_URL_ERROR)
        }
    }

    override fun isReceivingRealtimeUpdates(): Boolean {
        return isReceivingRealtimeUpdates
    }


    private suspend fun buildWidgetList(
        innerUrl: String, params: List<Pair<String, String>> = emptyList()
    ): List<LiveLikeWidget> {
        publishedWidgetListResponse =
            networkApiClient.get(innerUrl, queryParameters = params).processResult()
        // widgetInteractionRepository.clearInteractionMap()
        // fetching widget interactions for widgets loaded
        publishedWidgetListResponse?.results?.let {
            if (it.isNotEmpty()) {
                currentProfileOnce().let { user ->
                    widgetInteractionRepository.fetchAndStoreWidgetInteractions(
                        publishedWidgetListResponse?.widgetInteractionsUrlTemplate?.replace(
                            TEMPLATE_PROFILE_ID, user.id
                        ) ?: "", user.accessToken
                    )
                }
            }
        }

        return publishedWidgetListResponse?.results?.filter {
            var widgetType = it.kind
            widgetType = if (widgetType?.contains("follow-up") == true) {
                "$widgetType-updated"
            } else {
                "$widgetType-created"
            }
            return@filter WidgetType.fromString(widgetType) != null
        } ?: emptyList()
    }
}