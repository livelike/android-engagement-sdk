package com.livelike.engagementsdk.widget.model

class ImageSliderEntity(
    clientId: String,
    createdAt: String,
    id: String,
    kind: String,
    programId: String,
    url: String
) : Resource(
    clientId = clientId,
    createdAt = createdAt,
    id = id,
    kind = kind,
    programId = programId,
    url = url
) {
    override fun toLiveLikeWidgetResult(): LiveLikeWidgetResult {
        return LiveLikeWidgetResult(getMergedOptions(), averageMagnitude)
    }
}
