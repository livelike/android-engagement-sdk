package com.livelike.engagementsdk.core.data.models

import com.google.gson.annotations.SerializedName
import com.livelike.engagementsdk.chat.data.remote.ChatRoom
import com.livelike.engagementsdk.publicapis.LiveLikeUserApi
import com.livelike.engagementsdk.sponsorship.SponsorModel
import com.livelike.engagementsdk.widget.domain.LeaderBoardDelegate

typealias Program = ProgramModel


data class LeaderBoardResource(
    @SerializedName("id") val id: String,
    @SerializedName("url") internal val url: String,
    @SerializedName("client_id") internal val clientId: String,
    @SerializedName("name") val name: String,
    @SerializedName("reward_item_id") internal val rewardItemId: String,
    @SerializedName("is_locked") internal val isLocked: Boolean,
    @SerializedName("entries_url") internal val entriesUrl: String,
    @SerializedName("entry_detail_url_template") internal val entryDetailUrlTemplate: String,
    @SerializedName("reward_item") val rewardItem: LeaderBoardReward
)


typealias LeaderBoard = LeaderBoardResource


data class LeaderBoardForClient(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("reward_item") val rewardItem: RewardItem
)


typealias LeaderBoardReward = RewardItem


data class LeaderBoardEntry(
    @SerializedName("percentile_rank") val percentileRank: Double,
    @SerializedName("profile_id") val profileId: String,
    @SerializedName("rank") val rank: Int,
    @SerializedName("score") val score: Int,
    @SerializedName("profile_nickname") val profileNickname: String,
    @SerializedName("client_id") val clientId: String,
    @SerializedName("name") val name: String,
    @SerializedName("profile") val profile: LiveLikeUserApi,
    @SerializedName("entry") val entry: Entry? = null,
    @SerializedName("leaderboard") val leaderboard: LeaderBoardResource? = null
)

data class Entry(
    @SerializedName("rank") val rank: Int,
    @SerializedName("score") val score: Int,
    @SerializedName("percentile_rank") val percentileRank: Double
)

data class LeaderboardClient(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("rewardItem") val rewardItem: RewardItem,
    @SerializedName("currentUserPlacement") val currentUserPlacement: LeaderboardPlacement,
    @SerializedName("leaderboardDelegate") val leaderBoardDelegate: LeaderBoardDelegate?
)

data class LeaderboardPlacement(
    @SerializedName("rank") val rank: Int,
    @SerializedName("rankPercentile") val rankPercentile: String,
    @SerializedName("score") val score: Int
)

data class ProgramModel(
    @SerializedName("url")
    internal val programUrl: String,
    @SerializedName("timeline_url")
    internal val timelineUrl: String,
    @SerializedName("rank_url")
    internal val rankUrl: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("widgets_enabled")
    val widgetsEnabled: Boolean,
    @SerializedName("chat_enabled")
    val chatEnabled: Boolean,
    @SerializedName("subscribe_channel")
    val subscribeChannel: String,
    @SerializedName("sendbird_channel")
    val chatChannel: String?,
    @SerializedName("analytics_properties")
    val analyticsProps: Map<String, String>?,
    @SerializedName("rewards_type")
    val rewardsType: String?, // none, points, bagdes
    @SerializedName("leaderboard_url")
    internal val leaderboardUrl: String?,
    @SerializedName("sticker_packs_url")
    internal val stickerPacksUrl: String?,
    @SerializedName("reaction_packs_url")
    internal val reactionPacksUrl: String?,
    @SerializedName("report_url")
    internal val reportUrl: String?,
    @SerializedName("default_chat_room")
    val defaultChatRoom: ChatRoom?,
    val leaderboards: List<LeaderBoardResource>,
    @SerializedName("reward_items")
    val rewardItems: List<RewardItem>,
    @field:SerializedName("sponsors")
    val sponsors: List<SponsorModel>,
    @field:SerializedName("sponsors_url")
    internal val sponsorsUrl: String?,
    @SerializedName("widget_interactions_url_template")
    internal val widgetInteractionUrl: String?,
    @SerializedName("unclaimed_widget_interactions_url_template")
    internal val unclaimedWidgetInteractionsUrlTemplate: String?,
    @SerializedName("widgets_url")
    internal val widgetsUrl: String?,
    @SerializedName("pubnub_enabled")
    val pubnubEnabled: Boolean = true
)

enum class RewardsType(val key: String) {
    NONE("none"),
    POINTS("points"),
    BADGES("badges");
}
