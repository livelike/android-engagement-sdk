package com.livelike.engagementsdk.core.services.messaging.proxies

import com.livelike.engagementsdk.widget.model.Resource
import com.livelike.engagementsdk.widget.viewModel.WidgetStates
import kotlinx.coroutines.flow.MutableStateFlow

/**
 * Integrator will pass implementation of this proxy to intercept widgets,
 * SDK will call widgetWantsToShow() and in same sequence/order sdk will listen on events stream for the decision on that widget.
 */
abstract class WidgetInterceptor {
    /** Called when a widget is received from the CMS */
    abstract fun widgetWantsToShow(widgetData: LiveLikeWidgetEntity)

    /** Unlock the widget and show it on screen */
    fun showWidget() {
        eventsFlow.value = Decision.Show
    }

    /** Dismiss the widget and won't show it on screen */
    fun dismissWidget() {
        eventsFlow.value = Decision.Dismiss
    }

    internal enum class Decision {
        Show,
        Dismiss
    }

    internal val eventsFlow = MutableStateFlow<Decision?>(null)
}

abstract class WidgetLifeCycleEventsListener {

    abstract fun onWidgetPresented(widgetData: LiveLikeWidgetEntity)
    abstract fun onWidgetInteractionCompleted(widgetData: LiveLikeWidgetEntity)
    abstract fun onWidgetDismissed(widgetData: LiveLikeWidgetEntity)
    abstract fun onWidgetStateChange(state: WidgetStates, widgetData: LiveLikeWidgetEntity)
    abstract fun onUserInteract(widgetData: LiveLikeWidgetEntity)
// TODO    abstract fun onWidgetCancelled(reason: WidgetCancelReason, widgetData: LiveLikeWidgetEntity)
}

typealias LiveLikeWidgetEntity = Resource
