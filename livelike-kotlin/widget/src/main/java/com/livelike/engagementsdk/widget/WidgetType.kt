package com.livelike.engagementsdk.widget

enum class WidgetType(val event: String) {
    CHEER_METER("cheer-meter-created"),
    TEXT_PREDICTION("text-prediction-created"),
    TEXT_PREDICTION_FOLLOW_UP("text-prediction-follow-up-updated"),
    IMAGE_PREDICTION("image-prediction-created"),
    IMAGE_PREDICTION_FOLLOW_UP("image-prediction-follow-up-updated"),
    TEXT_QUIZ("text-quiz-created"),
    IMAGE_QUIZ("image-quiz-created"),
    TEXT_POLL("text-poll-created"),
    IMAGE_POLL("image-poll-created"),
    POINTS_TUTORIAL("points-tutorial"),
    COLLECT_BADGE("collect-badge"),
    ALERT("alert-created"),
    IMAGE_SLIDER("emoji-slider-created"),
    SOCIAL_EMBED("social-embed-created"),
    VIDEO_ALERT("video-alert-created"),
    TEXT_ASK("text-ask-created"),
    RICH_POST("rich-post-created"),

    //@UnsupportedWidgetType
    TEXT_NUMBER_PREDICTION("text-number-prediction-created"),
    IMAGE_NUMBER_PREDICTION("image-number-prediction-created"),

   // @UnsupportedWidgetType
    TEXT_NUMBER_PREDICTION_FOLLOW_UP("text-number-prediction-follow-up-updated"),
    IMAGE_NUMBER_PREDICTION_FOLLOW_UP("image-number-prediction-follow-up-updated");


    companion object {
        private val map = values().associateBy(WidgetType::event)
        fun fromString(type: String) = map[type]
        fun findFromString(type: String): WidgetType? {
            return values().find { it.event.contains(type) }
        }
    }

    fun getType(): String {
        return event
            .replace("-created", "")
            .replace("-updated", "")
    }

    /*
        CAF 2021-11-24
        the original did not match the server side name
        and I was adverse to changing the existing function
        so I made a new one and made it internal.
     */
    fun getKindName(): String {
        return event
            .replace("-created", "")
            .replace("-updated", "")
    }
}