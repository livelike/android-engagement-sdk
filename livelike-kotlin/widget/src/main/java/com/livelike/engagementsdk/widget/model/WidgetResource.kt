package com.livelike.engagementsdk.widget.model

import com.google.gson.annotations.SerializedName
import com.livelike.engagementsdk.*
import com.livelike.engagementsdk.sponsorship.SponsorModel
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.data.models.SocialEmbedItem
import com.livelike.widget.parseDuration
import kotlin.math.roundToInt

open class Resource(
    /*  internal val follow_up_url: String = "",
      internal val testTag: String = "",*/
    @field:SerializedName("vote_url") val voteUrl: String? = null,
    @field:SerializedName("sponsors") var sponsors: List<SponsorModel>? = null,
    var height: Int? = null,
    @field:SerializedName("program_id") val programId: String,
    @field:SerializedName("client_id") val clientId: String,
    @field:SerializedName("created_at") val createdAt: String,
    @field:SerializedName("scheduled_at") val scheduledAt: String? = null,
    @field:SerializedName("engagement_count") val engagementCount: Int? = 0,
    @field:SerializedName("publish_delay") val publishDelay: String? = null,
    @field:SerializedName("rewards_url") val rewardsUrl: String? = null,
    @field:SerializedName("translatable_fields") val translatableFields: List<String>? = null,
    @field:SerializedName("schedule_url") val scheduleUrl: String? = null,
    @field:SerializedName("impression_count") val impressionCount: Int? = null,
    @field:SerializedName("engagement_percent") val engagementPercent: String? = null,
    @field:SerializedName("options") val options: List<OptionsItem>? = null,
    @field:SerializedName("choices") val choices: List<OptionsItem>? = null,
    @field:SerializedName("program_date_time") val programDateTime: String? = null,
    @field:SerializedName("id") val id: String,
    @field:SerializedName("published_at") val publishedAt: String? = null,
    @field:SerializedName("unique_impression_count") internal val uniqueImpressionCount: Int? = null,
    @field:SerializedName("question") val question: String? = null,
    @field:SerializedName("kind") val kind: String,
    @field:SerializedName("subscribe_channel") val subscribeChannel: String? = null,
    @field:SerializedName("created_by") val createdBy: CreatedBy? = null,
    @field:SerializedName("url") val url: String,
    @field:SerializedName("cheer_type") val cheerType: String? = null,
    @field:SerializedName("impression_url") val impressionUrl: String? = null,
    @field:SerializedName("reactions") val reactions: List<ReactionsItem>? = null,
    @field:SerializedName("interaction_url") val interactionUrl: String? = null,
    @field:SerializedName("custom_data") val customData: String? = null,
    @field:SerializedName("status") val status: String? = null,

    // The Date/time, until which the Widget will accept interactions
    @field:SerializedName("interactive_until") val interactiveUntil: String? = null,

    @field:SerializedName("text") val text: String? = null,
    @field:SerializedName("image_url") val imageUrl: String? = null,
    @field:SerializedName("video_url") val videoUrl: String? = null,
    @field:SerializedName("link_url") val linkUrl: String? = null,
    @field:SerializedName("link_label") val linkLabel: String? = null,
    @field:SerializedName("text_prediction_id") val textPredictionId: String? = null,
    @field:SerializedName("image_prediction_id") val imagePredictionId: String? = null,
    @field:SerializedName("text_prediction_url") val textPredictionUrl: String? = null,
    @field:SerializedName("text_number_prediction_id") val textNumberPredictionId: String? = null,
    @field:SerializedName("image_number_prediction_id") val imageNumberPredictionId: String? = null,
    @field:SerializedName("correct_option_id") val correctOptionId: String? = null,
    @field:SerializedName("title") val title: String? = null,
    @field:SerializedName("content") val content: String? = null,
    @field:SerializedName("prompt") val prompt: String? = null,
    @field:SerializedName("translations") val translations: Any? = null,
    @field:SerializedName("initial_magnitude") val initialMagnitude: Float? = null,
    @field:SerializedName("average_magnitude") var averageMagnitude: Float? = null,


    @field:SerializedName("claim_url") val claimUrl: String? = null,
    @field:SerializedName("reply_url") val replyUrl: String? = null,
    @field:SerializedName("confirmation_message") val confirmationMessage: String? = null,

    /**  fields related to social embed widget */
    @field:SerializedName("items") val socialEmbedItems: List<SocialEmbedItem>? = null,
    @field:SerializedName("comment") val comment: String? = null,
    @field:SerializedName("widget_interactions_url_template") val widgetInteractionUrl: String? = null,
    @field:SerializedName("rewards") val rewards: List<RewardSummary>? = null,
    @field:SerializedName("earnable_rewards") val earnableRewards: List<EarnableReward>? = null,
    @SerializedName("widget_attributes") val widgetAttributes: List<WidgetAttribute>? = null,
    @field:SerializedName("pubnub_enabled") val pubnubEnabled: Boolean = true,

    /** video on demand playback time */
    @field:SerializedName("playback_time_ms") val playbackTimeMs: Long? = null,

    @field:SerializedName("follow_ups") val followUps: List<Resource>? = null,
    @SerializedName("follow_up_url") val followUpUrl: String? = null,
    @SerializedName("reply_count") val replyCount: Int? = null,

) {

    fun copy(
        clientId: String = this.clientId,
        createdAt: String = this.createdAt,
        id: String = this.id,
        kind: String = this.kind,
        programId: String = this.programId,
        url: String = this.url,
        options: List<OptionsItem>? = this.options,
        choices: List<OptionsItem>? = this.choices
    ) = Resource(clientId = clientId, createdAt=createdAt, id=id, kind = kind, programId = programId, url=url, options = options, choices = choices)



    var timeout: String = ""
        set(value) {
            field = value
            interactionTime = parseDuration(value)
        }
    private var interactionTime: Long? = null

    /* override fun toString(): String {
         return "id:$id,question:$question,voteUrl:$voteUrl,options:${options?.joinToString { "$it<><>" }}\n"
     }*/

    init {
        for (it in getMergedOptions() ?: emptyList()) {
            it.percentage = it.getPercent(getMergedTotal().toFloat())
        }
    }

    fun getMergedOptions(): List<Option>? {
        return if (!choices.isNullOrEmpty()) {
            choices
        } else {
            options
        }
    }

    fun getMergedTotal(): Int {
        val totalAnswers = options?.sumOf { it.voteCount ?: 0 } ?: 0
        return if (totalAnswers == 0) {
            choices?.sumOf { it.answerCount ?: 0 } ?: 0
        } else {
            totalAnswers
        }
    }

    open fun toLiveLikeWidgetResult(): LiveLikeWidgetResult {
        return LiveLikeWidgetResult(getMergedOptions(), null)
    }

    /**
     * Added this method to get WidgetType for integrator understanding and they can use it for they implementation
     */
    fun getWidgetType(): WidgetType? {
        var widgetType = kind
        widgetType = if (widgetType?.contains("follow-up") == true) {
            "$widgetType-updated"
        } else {
            "$widgetType-created"
        }
        return WidgetType.fromString(widgetType)
    }
}

data class Alert(
    val id: String = "",
    val url: String = "",
    val kind: String = "",
    val timeout: String = "",
    val subscribe_channel: String = "",
    val program_id: String = "",
    val created_at: String = "",
    val published_at: String = "",
    val title: String = "",
    val text: String = "",
    val image_url: String = "",
    val link_url: String = "",
    val link_label: String = "",
    @SerializedName("impression_url") val impressionUrl: String = "",
    @SerializedName("video_url") val videoUrl: String = ""
)

data class Option(
    val url: String,
    @field:SerializedName("image_url") val imageUrl: String? = null,
    @field:SerializedName("vote_url") val voteUrl: String? = null,
    @field:SerializedName("description") val description: String? = null,
    @field:SerializedName("id") val id: String,
    @field:SerializedName("translatable_fields") val translatableFields: List<String>? = null,
    @field:SerializedName("translations") val translations: Any? = null,
    @field:SerializedName("vote_count") var voteCount: Int? = null,
    @field:SerializedName("is_correct") var isCorrect: Boolean? = null,
    @field:SerializedName("answer_url") val answerUrl: String? = null,
    @field:SerializedName("answer_count") var answerCount: Int? = null,
    @field:SerializedName("correct_number") val correctNumber: Int? = null,
    @field:SerializedName("number") var number: Int? = null,
    @field:SerializedName("earnable_rewards") var earnableRewards: List<OptionReward>,
    @field:SerializedName("reward_item_id") val rewardItemId: String?,
    @field:SerializedName("reward_item_amount") val rewardItemAmount: Double?,
) {

    /**
     * Copy function for the Option class, skipping the `url` field.
     * Allows all other fields to be customized while preserving defaults.
     */
    fun copy(
        imageUrl: String? = this.imageUrl,
        voteUrl: String? = this.voteUrl,
        description: String? = this.description,
        id: String = this.id,
        translatableFields: List<String>? = this.translatableFields,
        translations: Any? = this.translations,
        voteCount: Int? = this.voteCount,
        isCorrect: Boolean? = this.isCorrect,
        answerUrl: String? = this.answerUrl,
        answerCount: Int? = this.answerCount,
        correctNumber: Int? = this.correctNumber,
        number: Int? = this.number,
        rewardItemId: String? = this.rewardItemId,
        rewardItemAmount: Double? = this.rewardItemAmount
    ) = Option(
        url = "", // Do not allow overriding the URL
        imageUrl = imageUrl,
        voteUrl = voteUrl,
        description = description,
        id = id,
        translatableFields = translatableFields,
        translations = translations,
        voteCount = voteCount,
        isCorrect = isCorrect,
        answerUrl = answerUrl,
        answerCount = answerCount,
        correctNumber = correctNumber,
        number = number,
        earnableRewards = emptyList(),
        rewardItemId = rewardItemId,
        rewardItemAmount = rewardItemAmount
    )
    fun getPercent(total: Float): Int {
        if (total == 0F) return 0
        val nbVote: Int = answerCount ?: (voteCount ?: 0)
        return ((nbVote / total) * 100).roundToInt()
    }

    fun getMergedVoteCount(): Float {
        return (answerCount ?: (voteCount ?: 0)).toFloat()
    }

    fun getMergedVoteUrl(): String? {
        return if (voteUrl.isNullOrEmpty()) {
            answerUrl
        } else {
            voteUrl
        }
    }

    var percentage: Int? = 0

    fun updateCount(option: Option) {
        answerCount = option.answerCount
        voteCount = option.voteCount
    }
}