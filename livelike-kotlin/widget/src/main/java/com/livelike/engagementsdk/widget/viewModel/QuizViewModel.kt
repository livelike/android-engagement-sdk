package com.livelike.engagementsdk.widget.viewModel


import com.livelike.common.DataStoreDelegate
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.utils.safeCallBack
import com.livelike.common.utils.toStream
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.AnalyticsWidgetInteractionInfo
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.Stream
import com.livelike.engagementsdk.WidgetInfos
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.core.data.models.RewardsType
import com.livelike.engagementsdk.core.data.models.VoteResponse
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.data.models.ProgramGamificationProfile
import com.livelike.engagementsdk.widget.data.models.QuizWidgetUserInteraction
import com.livelike.engagementsdk.widget.data.respository.WidgetInteractionRepository
import com.livelike.engagementsdk.widget.domain.GamificationManager
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.model.LiveLikeWidgetResult
import com.livelike.engagementsdk.widget.model.Option
import com.livelike.engagementsdk.widget.model.Resource
import com.livelike.engagementsdk.widget.model.WidgetImpressions
import com.livelike.engagementsdk.widget.repository.ProgramRepository
import com.livelike.engagementsdk.widget.utils.addGamificationAnalyticsData
import com.livelike.engagementsdk.widget.utils.toAnalyticsString
import com.livelike.engagementsdk.widget.widgetModel.QuizWidgetModel
import com.livelike.network.NetworkApiClient
import com.livelike.realtime.RealTimeClientMessage
import com.livelike.serialization.gson
import com.livelike.utils.LiveLikeException
import com.livelike.utils.Once
import com.livelike.utils.formatIsoZoned8601
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.livelike.widget.RECYCLER_NO_POSITION
import com.livelike.widget.parseDuration
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime

class QuizWidget(
    val type: WidgetType,
    val resource: Resource
)

class QuizViewModel(
    private val widgetInfos: WidgetInfos,
    analyticsService: AnalyticsService,
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    var onDismiss:(() -> Unit)?,
    private val programRepository: ProgramRepository? = null,
    private val widgetMessagingClient: ((RealTimeClientMessage) -> Unit)? = null,
    val widgetInteractionRepository: WidgetInteractionRepository?,
    networkApiClient: NetworkApiClient,
    rewardItemMapCache: Map<String, RewardItem>,
    userProfileDelegate: UserProfileDelegate?,
    dataStoreDelegate: DataStoreDelegate,
    viewModelDispatcher: CoroutineDispatcher,
    uiDispatcher: CoroutineDispatcher
) : BaseViewModel(
    configurationOnce,
    currentProfileOnce,
    analyticsService,
    networkApiClient,
    rewardItemMapCache,
    userProfileDelegate,
    dataStoreDelegate,
    viewModelDispatcher, uiDispatcher
), QuizWidgetModel {

    var points: Int? = null
    val gamificationProfile: StateFlow<ProgramGamificationProfile?>
        get() = programRepository?.programGamificationProfileFlow ?: MutableStateFlow(null)
    val rewardsType: RewardsType
        get() = programRepository?.rewardType ?: RewardsType.NONE

    val dataFlow = MutableStateFlow<QuizWidget?>(null)
    val resultsFlow = MutableStateFlow<Resource?>(null)
    val currentVoteIdFlow = MutableStateFlow<String?>(null)
    val disableInteractionFlow = MutableStateFlow<Boolean?>(null)

    private var timeoutStarted = false
    var animationProgress = 0f
    var animationPath = ""
    private var voteUrl: String? = null
    var animationEggTimerProgress = 0f

    private var currentWidgetId: String = ""
    private var programId: String = ""
    private var currentWidgetType: WidgetType? = null
    private val interactionData = AnalyticsWidgetInteractionInfo()
    private var interactiveUntilTimeout = false
    var timeOutJob: Job? = null

    init {
        widgetObserver(widgetInfos)
    }

    internal fun vote() {
        logDebug { "Quiz Widget selectedPosition:${selectedPositionFlow.value}" }
        if (selectedPositionFlow.value == RECYCLER_NO_POSITION) return // Nothing has been clicked

        //this is done to update vote count locally for responsive UI
        getUserSelectedOptionIdAndUpdateCount() //increments decrements vote count locally
        resultsFlow.value = (dataFlow.value?.resource) // so that ui updates happen

        safeCallBack({ result, error ->
            result?.let { logDebug { it } }
            error?.let { logError { it } }
        }) {
            val myDataset = dataFlow.value?.resource?.getMergedOptions()
            val url = myDataset?.get(selectedPositionFlow.value)?.getMergedVoteUrl()
                ?: throw LiveLikeException("Vote Url is null")
            voteAsync(
                url,
                myDataset[selectedPositionFlow.value].id,
                rewardItemMapCache = rewardItemMapCache,
                userProfileRewardDelegate = userProfileRewardDelegate,
                currentProfileOnce = currentProfileOnce
            )

        }
    }

    private fun widgetObserver(widgetInfos: WidgetInfos?) {
        if (widgetInfos != null &&
            (WidgetType.fromString(widgetInfos.type) == WidgetType.IMAGE_QUIZ || WidgetType.fromString(
                widgetInfos.type
            ) == WidgetType.TEXT_QUIZ)
        ) {
            val resource =
                gson.fromJson(widgetInfos.payload.toString(), Resource::class.java) ?: null
            resource?.apply {
                if (resource.pubnubEnabled) {
                    subscribeChannel?.let { it1 ->
                        subscribeWidgetResults(
                            it1,
                            widgetInfos.widgetId,
                            resultsFlow
                        )
                    } ?: logError { "subscribeChannel is null" }
                } else {
                    logDebug { "Pubnub not enabled" }
                }
                dataFlow.value =
                    WidgetType.fromString(widgetInfos.type)?.let { QuizWidget(it, resource) }
            } ?: logDebug { "Quiz resource is null" }
            currentWidgetId = widgetInfos.widgetId
            programId = dataFlow.value?.resource?.programId.toString()
            currentWidgetType = WidgetType.fromString(widgetInfos.type)
            interactionData.widgetDisplayed()
        } else {
            onClear()
        }
    }

    fun startDismissTimout(
        timeout: String
    ) {
        if (!timeoutStarted && timeout.isNotEmpty()) {
            timeoutStarted = true
            timeOutJob = uiScope.launch {
                delay(parseDuration(timeout))
                lockInteractionAndSubmitVote()
            }
        }
    }

    fun lockInteractionAndSubmitVote() {
        selectionLockedFlow.value = true
        vote()
        widgetStateFlow.value = WidgetStates.RESULTS
        resultsState()
    }

    fun dismissWidget(action: DismissAction) {
        currentWidgetType?.let {
            analyticsService.trackWidgetDismiss(
                it.toAnalyticsString(),
                currentWidgetId,
                programId,
                interactionData,
                selectionLockedFlow.value,
                action
            )
        } ?: logDebug { "current widget type is null" }
        widgetStateFlow.value = WidgetStates.FINISHED
        logDebug { "dismiss Quiz Widget, reason:${action.name}" }
        onDismiss?.invoke()
        onClear()
    }

    private fun resultsState() {
        if (selectedPositionFlow.value == RECYCLER_NO_POSITION) {
            // If the user never selected an option dismiss the widget with no confirmation
            // dismissWidget(DismissAction.TIMEOUT)
            return
        }

        val isUserCorrect = selectedPositionFlow.value.let {
            dataFlow.value?.resource?.getMergedOptions()?.get(it)?.isCorrect
        } ?: false
        selectionLockedFlow.value = true
        logDebug { "Quiz View ,showing result isUserCorrect:$isUserCorrect" }
        safeCallBack(uiScope) {
            dataFlow.value?.resource?.rewardsUrl?.let {
                getGamificationReward(
                    it,
                    analyticsService,
                    currentProfileOnce().accessToken
                )?.let { pts ->
                    programRepository?.programGamificationProfileFlow?.value = pts
                    points = pts.newPoints
                    widgetMessagingClient?.let { widgetMessagingClient ->
                        GamificationManager.checkForNewBadgeEarned(pts, widgetMessagingClient)
                    }
                    interactionData.addGamificationAnalyticsData(pts)
                } ?: logDebug { "Program gamification profile is null" }
            } ?: logError { "Rewards Url is Null" }
            currentWidgetType?.let {
                analyticsService.trackWidgetInteraction(
                    it.toAnalyticsString(),
                    currentWidgetId,
                    programId,
                    interactionData,
                    widgetPrompt = dataFlow.value?.resource?.question ?: ""
                )
            } ?: logDebug { "current widget type is null" }
        }
    }

    override fun onClear() {
        super.onClear()
        vote() // Vote on dismiss
        unsubscribeWidgetResults()
        timeoutStarted = false
        animationProgress = 0f
        animationPath = ""
        voteUrl = null
        dataFlow.value = null
        resultsFlow.value = null
        animationEggTimerProgress = 0f

        currentWidgetType = null
        currentWidgetId = ""
        interactionData.reset()
        onDismiss = null
    }

    override val widgetData: LiveLikeWidget
        get() = gson.fromJson(widgetInfos.payload, LiveLikeWidget::class.java)

    override val voteResults: Stream<LiveLikeWidgetResult>
        get() = resultsFlow.map { it?.toLiveLikeWidgetResult() }.toStream(uiScope)
    override val voteResultFlow: Flow<LiveLikeWidgetResult?>
        get() = resultsFlow.map { it?.toLiveLikeWidgetResult() }

    override fun lockInAnswer(
        optionID: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<VoteResponse>?
    ) {
        trackWidgetEngagedAnalytics(
            currentWidgetType = currentWidgetType,
            currentWidgetId = currentWidgetId,
            programId = programId,
            widgetPrompt = dataFlow.value?.resource?.question?:""
        )
        safeCallBack(liveLikeCallback) {
            val widget = dataFlow.value ?: throw LiveLikeException("Quiz data is null")
            val option = widget.resource.getMergedOptions()?.find { it.id == optionID } ?: throw LiveLikeException("No Option is Found")
            saveInteraction(option)

            //this is done to update vote count locally for responsive UI
            updateLocalVoteCount(option.id) //increments decrements vote count locally
            resultsFlow.value = (dataFlow.value?.resource) // so that ui updates happen


            voteAsync(
                option.getMergedVoteUrl() ?: throw LiveLikeException("No Vote Url"),
                option.id,
                currentProfileOnce = currentProfileOnce,
                rewardItemMapCache = rewardItemMapCache,
                userProfileRewardDelegate = userProfileRewardDelegate
            )
        }
    }

    override fun getUserInteraction(): QuizWidgetUserInteraction? {
        return widgetInteractionRepository?.getWidgetInteraction(
            widgetInfos.widgetId
        )
    }

    override fun loadInteractionHistory(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<QuizWidgetUserInteraction>>) {
        safeCallBack(uiScope, liveLikeCallback) {
            val results =
                widgetInteractionRepository?.fetchRemoteInteractions(
                    widgetId = widgetInfos.widgetId,
                    widgetKind = widgetInfos.type,
                    programUrlTemplate = configurationOnce().programDetailUrlTemplate
                )
            if (WidgetType.fromString(widgetInfos.type) == WidgetType.TEXT_QUIZ) {
                results?.interactions?.textQuiz ?: emptyList()
            } else if (WidgetType.fromString(widgetInfos.type) == WidgetType.IMAGE_QUIZ) {
                results?.interactions?.imageQuiz ?: emptyList()
            } else {
                emptyList()
            }
        }
    }

    fun saveInteraction(option: Option) {
        widgetInteractionRepository?.saveWidgetInteraction(
            QuizWidgetUserInteraction(
                option.id,
                "",
                ZonedDateTime.now().formatIsoZoned8601(),
                option.getMergedVoteUrl(),
                widgetInfos.widgetId,
                widgetInfos.type
            )
        ) ?: logDebug { "widgetInteractionRepository is null" }
    }

    fun onOptionClicked() {
        interactionData.incrementInteraction()
    }

    private fun getUserSelectedOptionIdAndUpdateCount() {
        dataFlow.value?.resource?.apply {
            userSelectedOptionIdFlow.value = selectedPositionFlow.value.let { it1 ->
                if (it1 > -1)
                    return@let this.getMergedOptions()?.get(it1)?.id
                return@let null
            } ?: ""
            userSelectedOptionIdFlow.value?.let {
                updateLocalVoteCount(it)
            }
        } ?: logDebug { "quiz data is null" }
    }

     //this is used to update local vote counts
    private fun updateLocalVoteCount(optionID: String) {
        dataFlow.value?.resource?.apply {
            val optionResults = this.getMergedOptions() ?: return
                userSelectedOptionIdFlow.value = optionID
                optionResults.find {
                it.id == userSelectedOptionIdFlow.value
            }?.apply {
                this.answerCount = this.answerCount?.plus(1)
            }
        } ?: logDebug { "quiz data is null" }
    }

    //this starts timer for interactive until set
    fun startInteractiveUntilTimeout(timeout: Long) {
        if (!interactiveUntilTimeout) {
            interactiveUntilTimeout = true
            uiScope.launch {
                val timeDiff =
                    timeout - Instant.now().atZone(ZoneId.of("UTC")).toInstant().toEpochMilli()
                delay(timeDiff)
                disableInteractionFlow.value = true
            }
        }
    }

    override fun finish() {
        onDismiss?.invoke()
        onClear()
    }

    override fun markAsInteractive() {
        trackWidgetBecameInteractive(currentWidgetType, currentWidgetId, programId)
    }

    override fun registerImpression(
        url: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<WidgetImpressions>
    ) {
        registerImpressionApi(url, liveLikeCallback)
    }
}
