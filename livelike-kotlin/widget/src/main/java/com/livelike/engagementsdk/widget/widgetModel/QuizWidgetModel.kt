package com.livelike.engagementsdk.widget.widgetModel

import com.livelike.engagementsdk.Stream
import com.livelike.engagementsdk.core.data.models.VoteResponse
import com.livelike.engagementsdk.publicapis.LiveLikeCallback
import com.livelike.engagementsdk.widget.data.models.QuizWidgetUserInteraction
import com.livelike.engagementsdk.widget.model.LiveLikeWidgetResult
import com.livelike.engagementsdk.widget.viewModel.LiveLikeWidgetMediator
import kotlinx.coroutines.flow.Flow

interface QuizWidgetModel : LiveLikeWidgetMediator {

    /**
     * live stream for vote results
     */
    val voteResults: Stream<LiveLikeWidgetResult>
    val voteResultFlow: Flow<LiveLikeWidgetResult?>

    /**
     * lock the answer for quiz
     */
    fun lockInAnswer(
        optionID: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<VoteResponse>? = null
    )

    /**
     * it returns the latest user interaction for the widget
     */
    fun getUserInteraction(): QuizWidgetUserInteraction?

    /**
     * returns widget interactions from remote source
     */
    fun loadInteractionHistory(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<QuizWidgetUserInteraction>>)
}
