package com.livelike.engagementsdk.widget.viewModel

import com.livelike.common.DataStoreDelegate
import com.livelike.common.model.RequestType
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.core.data.models.VoteResponse
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.data.models.ProgramGamificationProfile
import com.livelike.engagementsdk.widget.data.respository.LocalPredictionWidgetVoteRepository
import com.livelike.engagementsdk.widget.data.respository.PredictionWidgetVote
import com.livelike.engagementsdk.widget.data.respository.PredictionWidgetVoteRepository
import com.livelike.engagementsdk.widget.domain.Reward
import com.livelike.engagementsdk.widget.domain.RewardSource
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.model.WidgetImpressions
import com.livelike.engagementsdk.widget.util.SingleRunner
import com.livelike.engagementsdk.widget.utils.toAnalyticsString
import com.livelike.network.NetworkApiClient
import com.livelike.realtime.RealTimeMessagingClient
import com.livelike.realtime.RealTimeMessagingClientConfig
import com.livelike.serialization.gson
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import com.livelike.utils.Once
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.livelike.utils.parseISODateTime
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId

abstract class BaseViewModel(
    protected val configurationOnce: Once<SdkConfiguration>,
    protected val currentProfileOnce: Once<LiveLikeProfile>,
    val analyticsService: AnalyticsService,
    protected val networkApiClient: NetworkApiClient,
    protected val rewardItemMapCache: Map<String, RewardItem>,
    protected val userProfileRewardDelegate: UserProfileDelegate?,
    val dataStoreDelegate: DataStoreDelegate,
    viewModelDispatcher: CoroutineDispatcher,
    uiDispatcher: CoroutineDispatcher
) : ViewModel(viewModelDispatcher, uiDispatcher) {

    var widgetResultClient: RealTimeMessagingClient? = null
    var subscribeResultJobs = arrayListOf<Job>()
    var subscribedWidgetChannelName: String? = null
    private var isMarkedInteractive: Boolean = false

    //TODO: check if WidgetState default value with READY can be used here current default is null
    val widgetStateFlow = MutableStateFlow<WidgetStates?>(null)
    var enableDefaultWidgetTransition = true
    var showTimer = true
    var showDismissButton: Boolean = true
    var timerStartTime: Long? = null

    private val lifeTimePointsFlow = MutableStateFlow<Int?>(null)
    private val rankFlow = MutableStateFlow<Int?>(null)

    private var rewardType = "none"
    val selectedPositionFlow = MutableStateFlow(-1)
    val selectionLockedFlow = MutableStateFlow(false)
    val userSelectedOptionIdFlow = MutableStateFlow<String?>(null)
    private var voteUrl = ""
    private val singleRunner = SingleRunner()
    protected var predictionWidgetVoteRepository: PredictionWidgetVoteRepository =
        LocalPredictionWidgetVoteRepository(dataStoreDelegate)

    fun registerImpressionApi(
        url: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<WidgetImpressions>
    ) {
        safeCallBack(liveLikeCallback) {
            val user = currentProfileOnce()
            networkApiClient.post(url, accessToken = user.accessToken).processResult()
        }
    }

    protected suspend fun voteAsync(
        widgetVotingUrl: String,
        voteId: String? = null,
        body: Map<String, Any>? = null,
        type: RequestType? = null,
        useVoteUrl: Boolean = true,
        currentProfileOnce: Once<LiveLikeProfile>,
        widgetId: String? = null,
        patchVoteUrl: String? = null,
        rewardItemMapCache: Map<String, RewardItem>,
        userProfileRewardDelegate: UserProfileDelegate?
    ): VoteResponse {
        return singleRunner.afterPrevious {
            logDebug { "WVU: $widgetVotingUrl, VoteId:$voteId, " }
            val voteResponse: VoteResponse
            if (!patchVoteUrl.isNullOrEmpty()) {
                voteUrl = patchVoteUrl
            }

            if (voteUrl.isEmpty() || !useVoteUrl) {
                voteResponse = if (type == RequestType.PATCH) {
                    networkApiClient.patch(
                        widgetVotingUrl,
                        accessToken = currentProfileOnce().accessToken,
                        body = body?.toJsonString()
                    )
                } else {
                    networkApiClient.post(
                        widgetVotingUrl,
                        accessToken = currentProfileOnce().accessToken,
                        body = body?.toJsonString()
                    )
                }.processResult()
            } else {
                voteResponse = if (type == RequestType.POST) {
                    networkApiClient.post(
                        voteUrl,
                        accessToken = currentProfileOnce().accessToken,
                        body = (body ?: voteId?.let {
                            mapOf("option_id" to it, "choice_id" to it)
                        })?.toJsonString()
                    )
                } else {
                    networkApiClient.patch(
                        voteUrl,
                        accessToken = currentProfileOnce().accessToken,
                        body = (body ?: voteId?.let {
                            mapOf("option_id" to it, "choice_id" to it)
                        })?.toJsonString()
                    )
                }.processResult()
            }
            voteUrl = voteResponse.url
            voteResponse.claimToken?.let {
                widgetId?.let { widgetId ->
                    predictionWidgetVoteRepository.add(
                        PredictionWidgetVote(
                            widgetId, it
                        )
                    ) {}
                }
            }
            voteResponse.rewards.let {
                for (reward in it) {
                    rewardItemMapCache[reward.rewardId]?.let {
                        currentProfileOnce().let { user ->
                            userProfileRewardDelegate?.userProfile(
                                user, Reward(it, reward.rewardItemAmount), RewardSource.WIDGETS
                            )
                        }
                    }
                }
            }
            return@afterPrevious voteResponse
        }
    }


    suspend fun getGamificationReward(
        rewardUrl: String,
        analyticsService: AnalyticsService,
        userAccessToken: String
    ): com.livelike.engagementsdk.widget.data.models.ProgramGamificationProfile? {
        if (rewardType == "none") {
            return null
        }
        val reward = networkApiClient.post(rewardUrl, accessToken = userAccessToken)
            .processResult<ProgramGamificationProfile>().also {
                dataStoreDelegate.addPoints(it.newPoints)
                analyticsService.registerSuperAndPeopleProperty(
                    "Lifetime Points" to it.points.toString()
                )
            }
        lifeTimePointsFlow.value = reward.points
        rankFlow.value = reward.rank
        return reward
    }

    protected inline fun <reified T : Any> subscribeWidgetResults(
        channelName: String,
        widgetId: String,
        flow: MutableStateFlow<T?>
    ) {
        logDebug { "BaseViewModel:subscribeWidgetResults:$channelName, $this" }
        subscribedWidgetChannelName = channelName
        subscribeResultJobs.add(viewModelScope.launch {
            val sdkConfiguration = configurationOnce()
            sdkConfiguration.pubNubKey?.let { subscribeKey ->
                val user = currentProfileOnce()
                widgetResultClient = RealTimeMessagingClient.getInstance(
                    RealTimeMessagingClientConfig(
                        subscribeKey,
                        user.accessToken,
                        user.id,
                        sdkConfiguration.pubnubPublishKey,
                        sdkConfiguration.pubnubOrigin,
                        sdkConfiguration.pubnubHeartbeatInterval,
                        sdkConfiguration.pubnubPresenceTimeout
                    ),
                    viewModelScope
                )
                launch {
                    widgetResultClient!!.messageClientFlow.map {
                        val widgetType = it.event
                        val payload = it.payload
                        return@map if (widgetType.contains("results") && payload.get("id").asString == widgetId) {
                            gson.fromJson(payload.toString(), T::class.java)
                        } else {
                            null
//                        throw Exception("Event recieved on different observer, widgetType:$widgetType, payload:$payload, currentWidgetId:$widgetId")
                        }
                    }.catch {
                        logError { it.message }
                    }.collect {
                        flow.value = it
                    }
                }
                widgetResultClient!!.subscribe(listOf(channelName))
            }
        })
    }

    fun unsubscribeWidgetResults() {
        subscribedWidgetChannelName?.let {
            widgetResultClient?.unsubscribe(listOf(it))
        }
        for (job in subscribeResultJobs) {
            job.cancel()
        }
    }

    fun trackWidgetEngagedAnalytics(
        currentWidgetType: WidgetType?,
        currentWidgetId: String,
        programId: String,
        widgetPrompt:String
    ) {
        currentWidgetType?.let {
            analyticsService.trackWidgetEngaged(
                currentWidgetType.toAnalyticsString(),
                currentWidgetId,
                programId,
                widgetPrompt

            )
        }
    }

    protected fun trackWidgetBecameInteractive(
        widgetType: WidgetType?,
        widgetId: String,
        programId: String,
    ) {
        if (!isMarkedInteractive) {
            isMarkedInteractive = true
            widgetType?.let { type ->
                analyticsService.trackWidgetBecameInteractive(
                    type.toAnalyticsString(),
                    widgetId,
                    programId
                )
            }
        }
    }


    /**
     * checks if widgets expired interactive_until
     **/
    fun isInteractivityExpired(interactiveUntil: String?): Boolean {
        interactiveUntil?.parseISODateTime()?.let {
            val epochTimeMs = it.toInstant().toEpochMilli()
            val currentTimeMs = Instant.now().atZone(ZoneId.of("UTC")).toInstant().toEpochMilli()
            if (currentTimeMs > epochTimeMs) {
                return false
            }
        }
        return true
    }


    /**
     * all models should override this to cleanup their resources
     **/
    open fun onClear() {
        viewModelScope.cancel()
        uiScope.cancel()
    }

    protected fun <T : Any> safeCallBack(
        liveLikeCallBack: com.livelike.common.LiveLikeCallback<T>? = null,
        call: suspend (Pair<LiveLikeProfile, SdkConfiguration>) -> T?
    ) {
        viewModelScope.launch {
            logDebug { "safeCallback invoke start" }
            try {
                val config = configurationOnce()
                val profile = currentProfileOnce()
                val data = call(profile to config)
                uiScope.launch {
                    liveLikeCallBack?.invoke(data, null)
                }
            } catch (e: Exception) {
                logError { e.message }
                uiScope.launch {
                    liveLikeCallBack?.invoke(null, e.message)
                }
            }
            logDebug { "safeCallback invoke end" }
        }
    }
}

enum class WidgetStates : Comparable<WidgetStates> {
    READY, // the data has received and ready to use to inject into view
    INTERACTING, // the data is injected into view and shown
    RESULTS, // interaction completed and result to be shown
    FINISHED, // dismiss the widget
}


