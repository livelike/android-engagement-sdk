package com.livelike.engagementsdk.core.data.models

import com.google.gson.annotations.SerializedName

class VoteResponse(
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("id") var id: String? = null,
    @SerializedName("widget_kind") var widgetKind: String? = null,
    @SerializedName("widget_id") var widgetId: String? = null,
    @SerializedName("rewards") var rewards: List<EarnedReward> = arrayListOf(),
    @SerializedName("option_id") var optionId: String? = null,
    @SerializedName("url") var url: String,
    @SerializedName("profile_id") var profileId: String? = null,
    @SerializedName("claim_token") var claimToken: String? = null,
)




