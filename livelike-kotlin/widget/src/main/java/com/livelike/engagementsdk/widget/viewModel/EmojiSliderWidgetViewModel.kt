package com.livelike.engagementsdk.widget.viewModel


import com.livelike.common.DataStoreDelegate
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.utils.safeCallBack
import com.livelike.common.utils.toStream
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.DismissAction
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.Stream
import com.livelike.engagementsdk.WidgetInfos
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.data.models.EmojiSliderUserInteraction
import com.livelike.engagementsdk.widget.data.respository.WidgetInteractionRepository
import com.livelike.engagementsdk.widget.domain.UserProfileDelegate
import com.livelike.engagementsdk.widget.model.ImageSliderEntity
import com.livelike.engagementsdk.widget.model.LiveLikeWidgetResult
import com.livelike.engagementsdk.widget.model.WidgetImpressions
import com.livelike.engagementsdk.widget.repository.ProgramRepository
import com.livelike.engagementsdk.widget.utils.toAnalyticsString
import com.livelike.engagementsdk.widget.widgetModel.ImageSliderWidgetModel
import com.livelike.network.NetworkApiClient
import com.livelike.realtime.RealTimeClientMessage
import com.livelike.serialization.gson
import com.livelike.utils.LiveLikeException
import com.livelike.utils.Once
import com.livelike.utils.formatIsoZoned8601
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.map
import org.threeten.bp.ZonedDateTime

class EmojiSliderWidgetViewModel(
    widgetInfos: WidgetInfos,
    analyticsService: AnalyticsService,
    configurationOnce: Once<SdkConfiguration>,
    onDismiss: (() -> Unit)?,
    currentProfileOnce: Once<LiveLikeProfile>,
    programRepository: ProgramRepository? = null,
    widgetMessagingClient: ((RealTimeClientMessage) -> Unit)? = null,
    val widgetInteractionRepository: WidgetInteractionRepository?,
    networkApiClient: NetworkApiClient,
    rewardItemMapCache: Map<String, RewardItem>,
    userProfileRewardDelegate: UserProfileDelegate?,
    dataStoreDelegate: DataStoreDelegate,
    viewModelDispatcher: CoroutineDispatcher,
    uiDispatcher: CoroutineDispatcher
) : WidgetViewModel<ImageSliderEntity>(
    widgetInfos,
    configurationOnce,
    currentProfileOnce,
    programRepository,
    widgetMessagingClient,
    onDismiss,
    analyticsService,
    networkApiClient, rewardItemMapCache,
    userProfileRewardDelegate,
    dataStoreDelegate,
    viewModelDispatcher, uiDispatcher
), ImageSliderWidgetModel {

    init {
        widgetObserver(widgetInfos)
        // restoring the emoji slider position from interaction history
        currentVoteFlow.value = getUserInteraction()?.magnitude?.toString()
    }

    override fun confirmInteraction() {
        currentVoteFlow.value?.let {
            vote(it)
        }
        super.confirmInteraction()
    }

    override fun vote(value: String) {
        safeCallBack({ result, error ->
            result?.let { logDebug { it } }
            error?.let { logError { it } }
        }) {
            voteAsync(
                dataFlow.value?.voteUrl ?: throw LiveLikeException("emoji data is null or vote url is null"),
                "",
                mapOf("magnitude" to value),
                rewardItemMapCache = rewardItemMapCache,
                userProfileRewardDelegate = userProfileRewardDelegate,
                currentProfileOnce = currentProfileOnce
            )
        }
    }

    private fun widgetObserver(widgetInfos: WidgetInfos) {
        val resource =
            gson.fromJson(widgetInfos.payload.toString(), ImageSliderEntity::class.java) ?: null
        resource?.apply {
            resource.subscribeChannel?.let { it1 ->
                subscribeWidgetResults(
                    it1,
                    widgetInfos.widgetId,
                    resultsFlow
                )
            } ?: logError { "subscribeChannel is Null" }

            dataFlow.value = resource
            widgetStateFlow.value = WidgetStates.READY
        } ?: logDebug { "emoji resource is null" }
        currentWidgetId = widgetInfos.widgetId
        programId = dataFlow.value?.programId.toString()
        currentWidgetType = WidgetType.fromString(widgetInfos.type)
        interactionData.widgetDisplayed()
    }

    override fun dismissWidget(action: DismissAction) {
        super.dismissWidget(action)
        currentWidgetType?.let {
            analyticsService.trackWidgetDismiss(
                it.toAnalyticsString(),
                currentWidgetId,
                programId,
                interactionData,
                false,
                action
            )
            logDebug { "dismiss EmojiSlider Widget, reason:${action.name}" }
        } ?: logDebug { "current widget type is null" }
    }

    override val widgetData: LiveLikeWidget
        get() = gson.fromJson(widgetInfos.payload, LiveLikeWidget::class.java)

    override val voteResults: Stream<LiveLikeWidgetResult>
        get() = resultsFlow.map { it?.toLiveLikeWidgetResult() }.toStream(uiScope)

    override fun finish() {
        onDismiss?.invoke()
        onClear()
    }

    override fun markAsInteractive() {
        trackWidgetBecameInteractive(currentWidgetType, currentWidgetId, programId)
    }

    override fun registerImpression(
        url: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<WidgetImpressions>
    ) {
        registerImpressionApi(url, liveLikeCallback)
    }

    override fun lockInVote(magnitude: Double) {
        dataFlow.value?.programId?.let { programId->
            trackWidgetEngagedAnalytics(
                currentWidgetType = currentWidgetType,
                currentWidgetId = currentWidgetId,
                programId = programId,
                widgetPrompt = dataFlow.value?.question?:""
            )
        }
        vote(magnitude.toString())
        saveInteraction(magnitude.toFloat(), dataFlow.value?.voteUrl)
    }

    override fun getUserInteraction(): EmojiSliderUserInteraction? {
        return widgetInteractionRepository?.getWidgetInteraction(
            widgetInfos.widgetId
        )
    }

    override fun loadInteractionHistory(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<EmojiSliderUserInteraction>>) {
        safeCallBack(uiScope, liveLikeCallback) {
            val results =
                widgetInteractionRepository?.fetchRemoteInteractions(
                    widgetId = widgetInfos.widgetId,
                    widgetKind = widgetInfos.type,
                    programUrlTemplate = configurationOnce().programDetailUrlTemplate
                )
            results?.interactions?.emojiSlider?: emptyList()
        }
    }

    fun saveInteraction(magnitude: Float, url: String?) {
        widgetInteractionRepository?.saveWidgetInteraction(
            EmojiSliderUserInteraction(
                magnitude,
                "",
                ZonedDateTime.now().formatIsoZoned8601(),
                url,
                widgetInfos.widgetId,
                widgetInfos.type
            )
        )
    }

    // this is used to calculate user's vote locally
    fun calculateMagnitude(): Double {
        var newAverage = 0.0
        currentVoteFlow.value?.let { magnitude ->
            val engagementCount = dataFlow.value?.engagementCount
            val total = engagementCount?.times(dataFlow.value?.averageMagnitude!!)
            newAverage = (total?.plus(magnitude.toDouble()))?.div((engagementCount.plus(1)))!!
            dataFlow.value?.averageMagnitude =
                newAverage.toFloat() //update with the locally calculated new average
        }
        return newAverage
    }

    override fun onClear() {
        super.onClear()
        unsubscribeWidgetResults()
        onDismiss = null
    }
}
