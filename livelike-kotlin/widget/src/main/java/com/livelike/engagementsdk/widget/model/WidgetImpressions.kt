package com.livelike.engagementsdk.widget.model

data class WidgetImpressions(
    val created_at: String,
    val id: String
)