package com.livelike.engagementsdk.widget.widgetModel


import com.livelike.engagementsdk.core.data.models.TextReply
import com.livelike.engagementsdk.publicapis.LiveLikeCallback
import com.livelike.engagementsdk.widget.data.models.TextAskUserInteraction
import com.livelike.engagementsdk.widget.viewModel.LiveLikeWidgetMediator

interface TextAskWidgetModel : LiveLikeWidgetMediator {

    /**
     * submit response entered
     */
    fun submitReply(response: String,liveLikeCallback: com.livelike.common.LiveLikeCallback<TextReply>)

    /**
     * it returns the latest user interaction for the widget
     */
    fun getUserInteraction(): TextAskUserInteraction?

    /**
     * returns widget interactions from remote source
     */
    fun loadInteractionHistory(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<TextAskUserInteraction>>)
}
