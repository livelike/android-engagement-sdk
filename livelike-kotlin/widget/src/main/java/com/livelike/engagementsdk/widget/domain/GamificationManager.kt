package com.livelike.engagementsdk.widget.domain

import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.data.models.ProgramGamificationProfile
import com.livelike.realtime.RealTimeClientMessage
import com.livelike.serialization.gson

/**
 * All domain logic and use-cases related to Gamification.
 * we can split it later based on usecases or not singleton. My heartly welcome for it.
 */
object GamificationManager {

    /** Check for latest badges and push the coolect badge widget into widget messaging client
     * @param program reward object that is typically fetched after widget interaction .
     * @param widgetManager last layer in widget pipeline where new badge collection widget may be published.
     */
    fun checkForNewBadgeEarned(
        programGamificationProfile: ProgramGamificationProfile,
        widgetManager: (RealTimeClientMessage) -> Unit
    ) {

        if (!programGamificationProfile.newBadges.isNullOrEmpty()) {
            val latestBadge = programGamificationProfile.newBadges.maxOrNull()
            val message = RealTimeClientMessage(
                WidgetType.COLLECT_BADGE.event,
                gson.toJsonTree(latestBadge).asJsonObject,
                0L,
                "gamification",
                "",
                2
//                TODO create generic to create this json message, really tech debt is increasing at fast rate now.
            )
            widgetManager.invoke(message)
        }
    }
}
