package com.livelike.engagementsdk.widget.widgetModel

import com.livelike.engagementsdk.Stream
import com.livelike.engagementsdk.core.data.models.VoteResponse
import com.livelike.engagementsdk.publicapis.LiveLikeCallback
import com.livelike.engagementsdk.widget.data.models.PollWidgetUserInteraction
import com.livelike.engagementsdk.widget.model.LiveLikeWidgetResult
import com.livelike.engagementsdk.widget.viewModel.LiveLikeWidgetMediator
import kotlinx.coroutines.flow.Flow

interface PollWidgetModel : LiveLikeWidgetMediator {

    val voteResults: Stream<LiveLikeWidgetResult>
    val voteResultFlow: Flow<LiveLikeWidgetResult?>

    fun submitVote(
        optionID: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<VoteResponse>? = null
    )

    /**
     * it returns the latest user interaction for the widget
     */
    fun getUserInteraction(): PollWidgetUserInteraction?

    /**
     * returns widget interactions from remote source
     */
    fun loadInteractionHistory(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<PollWidgetUserInteraction>>)

}
