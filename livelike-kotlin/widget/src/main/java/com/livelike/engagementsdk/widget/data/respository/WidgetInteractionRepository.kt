package com.livelike.engagementsdk.widget.data.respository


import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.data.models.UserWidgetInteractionApi
import com.livelike.engagementsdk.widget.data.models.WidgetUserInteractionBase
import com.livelike.engagementsdk.widget.repository.ProgramRepository
import com.livelike.network.NetworkApiClient
import com.livelike.utils.Once
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.livelike.widget.TEMPLATE_PROFILE_ID
import com.livelike.widget.TEMPLATE_PROGRAM_ID
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Repository that handles user's widget interaction data. It knows what data sources need to be
 * triggered to get widget interaction and where to store the data.
 **/
class WidgetInteractionRepository(
    val programID: String,
    private val currentProfileOnce: Once<LiveLikeProfile>,
    private val configurationOnce: Once<SdkConfiguration>,
    networkApiClient: NetworkApiClient
) {
    private val widgetInteractionRemoteSource: WidgetInteractionRemoteSource =
        WidgetInteractionRemoteSource(networkApiClient)

    private val programRepository = ProgramRepository(
        programID,
        currentProfileOnce,
        networkApiClient
    )

    val widgetInteractionMap = mutableMapOf<String, WidgetUserInteractionBase>()

    fun <T : WidgetUserInteractionBase> saveWidgetInteraction(widgetInteraction: T) {
        widgetInteractionMap[widgetInteraction.widgetId] = widgetInteraction
    }

    inline fun <reified T : WidgetUserInteractionBase> getWidgetInteraction(
        widgetId: String
    ): T? {
        logDebug { "AMA map ${widgetInteractionMap}" }
        return widgetInteractionMap[widgetId] as T?
    }

    /**
     * Responsible for fetching interactions and storing those locally
     **/
    suspend fun fetchAndStoreWidgetInteractions(
        url: String,
        accessToken: String
    ): UserWidgetInteractionApi = withContext(Dispatchers.IO) {
        val widgetInteractionsResult =
            widgetInteractionRemoteSource.getWidgetInteractions(url, accessToken)
        val interactionList = mutableListOf<WidgetUserInteractionBase>()
        val interactions = widgetInteractionsResult.interactions
        logDebug { "Total Interactions :${interactions.cheerMeter?.size},${interactions.emojiSlider?.size}" }
        interactions.cheerMeter?.let {
            logDebug { "Cheer Meter Interactions : ${it.size}" }
            interactionList.addAll(it)
        }
        interactions.emojiSlider?.let { interactionList.addAll(it) }
        interactions.textPoll?.let { interactionList.addAll(it) }
        interactions.textPrediction?.let { interactionList.addAll(it) }
        interactions.textQuiz?.let { interactionList.addAll(it) }
        interactions.imagePoll?.let { interactionList.addAll(it) }
        interactions.imagePrediction?.let { interactionList.addAll(it) }
        interactions.imageQuiz?.let { interactionList.addAll(it) }
        interactions.textAsk?.let { interactionList.addAll(it) }
        interactions.textNumberPrediction?.let { interactionList.addAll(it) }
        interactions.imageNumberPrediction?.let { interactionList.addAll(it) }
        //Causing error on total count for cheerMeter
        for (it in interactionList) {
//            if (it is CheerMeterUserInteraction && widgetInteractionMap[it.widgetId] != null) {
//                it.totalScore += (widgetInteractionMap[it.widgetId] as CheerMeterUserInteraction).totalScore
//            }
            widgetInteractionMap[it.widgetId] = it
        }

        widgetInteractionsResult
    }

    /**
     * Responsible for fetching program if program is null in program repository, since interaction url is
     * present in program resource. And fetch interaction based on that interaction url
     *
     **/
    suspend fun fetchRemoteInteractions(
        widgetInteractionUrl: String? = null,
        widgetId: String?,
        widgetKind: String?,
        widgetKinds: List<String> = emptyList(),
        programUrlTemplate: String? = null,
    ): UserWidgetInteractionApi? {
        var results: UserWidgetInteractionApi? = null
        if (programRepository.program == null) {
            (programUrlTemplate
                ?: this@WidgetInteractionRepository.configurationOnce().programDetailUrlTemplate).let { url ->
                try {
                    programRepository.getProgramData(
                        url.replace(
                            TEMPLATE_PROGRAM_ID,
                            programID
                        )
                    ).run {
                        results = fetchAndStoreWidgetInteractions(
                            getInteractionUrl(
                                widgetInteractionUrl,
                                widgetId,
                                widgetKind,
                                widgetKinds
                            ),
                            currentProfileOnce().accessToken
                        )
                    }
                } catch (e: Exception) {
                    logError { "Unable to fetch program details ${e.message}" }
                }
            }
        } else {
            results = fetchAndStoreWidgetInteractions(
                getInteractionUrl(widgetInteractionUrl, widgetId, widgetKind, widgetKinds),
                currentProfileOnce().accessToken
            )
        }
        return results
    }

    /**
     * get interaction url with appended widget kind and widget id
     **/
    private suspend fun getInteractionUrl(
        widgetInteractionUrl: String? = null,
        widgetId: String?,
        widgetKind: String?,
        widgetKinds: List<String>,
    ): String {
        var url = currentProfileOnce().id.let {
            (widgetInteractionUrl ?: programRepository.program?.widgetInteractionUrl)?.replace(
                TEMPLATE_PROFILE_ID,
                it
            )
        } ?: ""
        if (url.isNotEmpty() && widgetInteractionUrl == null) {
            if (widgetKind != null && widgetId != null) {
                url += "?${getWidgetKindBasedOnType(widgetKind)}_id=${widgetId}"
            }
            else if (widgetKinds.isNotEmpty()) {
                var params = ""
                for (it in widgetKinds) {
                    params += "widget_kind=${getWidgetTypeBasedOnKind(it.trim())}&"
                }
                url += "?${params.dropLast(1)}"
            }
        }
        return url
    }

    private fun getWidgetTypeBasedOnKind(widgetKind: String): String {
        return when (WidgetType.findFromString(widgetKind)) {
            WidgetType.TEXT_POLL -> "text-poll"
            WidgetType.IMAGE_POLL -> "image-poll"
            WidgetType.TEXT_QUIZ -> "text-quiz"
            WidgetType.IMAGE_QUIZ -> "image-quiz"
            WidgetType.TEXT_PREDICTION -> "text-prediction"
            WidgetType.IMAGE_PREDICTION -> "image-prediction"
            WidgetType.CHEER_METER -> "cheer-meter"
            WidgetType.IMAGE_SLIDER -> "emoji-slider"
            WidgetType.TEXT_PREDICTION_FOLLOW_UP -> "text-prediction"
            WidgetType.IMAGE_PREDICTION_FOLLOW_UP -> "image-prediction"
            WidgetType.TEXT_ASK -> "text-ask"
            WidgetType.TEXT_NUMBER_PREDICTION -> "text-number-prediction"
            WidgetType.IMAGE_NUMBER_PREDICTION -> "image-number-prediction"
            WidgetType.TEXT_NUMBER_PREDICTION_FOLLOW_UP -> "text-number-prediction"
            WidgetType.IMAGE_NUMBER_PREDICTION_FOLLOW_UP -> "image-number-prediction"
            else -> ""
        }
    }

    private fun getWidgetKindBasedOnType(widgetKind: String): String {
        return when (WidgetType.fromString(widgetKind)) {
            WidgetType.TEXT_POLL -> "text_poll"
            WidgetType.IMAGE_POLL -> "image_poll"
            WidgetType.TEXT_QUIZ -> "text_quiz"
            WidgetType.IMAGE_QUIZ -> "image_quiz"
            WidgetType.TEXT_PREDICTION -> "text_prediction"
            WidgetType.IMAGE_PREDICTION -> "image_prediction"
            WidgetType.CHEER_METER -> "cheer_meter"
            WidgetType.IMAGE_SLIDER -> "emoji_slider"
            WidgetType.TEXT_PREDICTION_FOLLOW_UP -> "text_prediction"
            WidgetType.IMAGE_PREDICTION_FOLLOW_UP -> "image_prediction"
            WidgetType.TEXT_ASK -> "text_ask"
            WidgetType.TEXT_NUMBER_PREDICTION -> "text_number_prediction"
            WidgetType.IMAGE_NUMBER_PREDICTION -> "image_number_prediction"
            WidgetType.TEXT_NUMBER_PREDICTION_FOLLOW_UP -> "text_number_prediction"
            WidgetType.IMAGE_NUMBER_PREDICTION_FOLLOW_UP -> "image_number_prediction"
            else -> ""
        }
    }

    fun clearInteractionMap() {
        widgetInteractionMap.clear()
    }
}
