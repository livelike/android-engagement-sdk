package com.livelike.engagementsdk.core.data.models

import com.google.gson.annotations.SerializedName


data class TextReply(
    @SerializedName("rewards") val rewards: List<EarnedReward>,
    @SerializedName("text") val text: String,
    val url: String,
    @SerializedName("created_at") val createdAt: String,
    val id: String,
    @SerializedName("widget_kind") val widgetKind: String,
    @SerializedName("profile_id") val profileId: String,
    @SerializedName("widget_id") val widgetId: String
)
