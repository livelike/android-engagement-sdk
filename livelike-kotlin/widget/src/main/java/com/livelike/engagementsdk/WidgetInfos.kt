package com.livelike.engagementsdk

import com.google.gson.JsonObject

/** A representation of a widget */
data class WidgetInfos(
    /** The type of the widget */
    val type: String,
    /** The data used to define the widget */
    val payload: JsonObject,
    /** The id of the widget */
    var widgetId: String,
    /** onDismiss Widget **/
    var onDismiss: (() -> Unit?)? = null
)