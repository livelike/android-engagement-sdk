package com.livelike.engagementsdk

import com.google.gson.JsonObject
import com.livelike.engagementsdk.chat.LiveLikeChatSession
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.core.data.models.LeaderboardClient
import com.livelike.engagementsdk.core.data.models.RewardItem
import com.livelike.engagementsdk.core.services.messaging.proxies.WidgetInterceptor
import com.livelike.engagementsdk.publicapis.LiveLikeCallback
import com.livelike.engagementsdk.widget.WidgetType
import com.livelike.engagementsdk.widget.data.models.PredictionWidgetUserInteraction
import com.livelike.engagementsdk.widget.data.models.WidgetUserInteractionBase
import com.livelike.engagementsdk.widget.domain.LeaderBoardDelegate
import com.livelike.engagementsdk.widget.model.GetPublishedWidgetsRequestOptions
import com.livelike.engagementsdk.widget.viewModel.BaseViewModel
import com.livelike.realtime.RealTimeClientMessage
import kotlinx.coroutines.flow.Flow

/**
 *  Represents a Content Session which LiveLike uses to deliver widgets and associate user with the Chat
 *  component.
 */
interface LiveLikeContentSession {
    /** The analytics services **/
    val chatSession: LiveLikeChatSession

    var contentSessionleaderBoardDelegate: LeaderBoardDelegate?

    val widgetStream: Stream<LiveLikeWidget>
    val widgetFlow: Flow<LiveLikeWidget>

//    /** All the new incoming widgets on current session will be published on this stream */
//    val widgetStream : Stream<LiveLikeWidget>

    /** Pause the current Chat and widget sessions. This generally happens when ads are presented */
    fun pause()

    /** Resume the current Chat and widget sessions. This generally happens when ads are completed */
    fun resume()

    /** Closes the current session.*/
    fun close()

    /** Return the playheadTime for this session.*/
    fun getPlayheadTime(): EpochTime

    /** Return the content Session Id (Program Id) for this session.*/
    fun contentSessionId(): String

    /** Intercepts the widgets and hold them until show() or dismiss() is being called */
    var widgetInterceptor: WidgetInterceptor?

    /**
     * Return the widget list which has been published
     * if the result is empty that means there is no data further and user reached end of list
     * **/
    fun getPublishedWidgets(
        getPublishedWidgetsRequestOptions: GetPublishedWidgetsRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<LiveLikeWidget>>
    )


    /** Returns list of reward item associated to entered program */
    fun getRewardItems(): List<RewardItem>

    /** Returns list of leaderboards associated to entered program */
    fun getLeaderboardClients(
        leaderBoardId: List<String>,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LeaderboardClient>
    )

    /**
     * Returns list of interactions for which rewards have not been claimed
     * @param liveLikePagination
     * */
    fun getWidgetInteractionsWithUnclaimedRewards(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<PredictionWidgetUserInteraction>>
    )

    /**
     * Returns interactions for widget
     * @param widgetId id if the widget
     * @param widgetKind kind of the widget
     * @param widgetInteractionUrl interaction url which needs to be called
     * */
    fun getWidgetInteraction(
        widgetId: String,
        widgetKind: String,
        widgetInteractionUrl: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<WidgetUserInteractionBase>
    )

    /**
     * Returns interactions for widget
     * @param widgetKinds kind for multiple widgets
     * */
    fun getWidgetInteractions(
        widgetKinds: List<String>,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<Map<String, List<WidgetUserInteractionBase>>>
    )

    /**
     * Returns the BaseViewModel object to utilize the apis with there own custom Widget
     */
    fun getWidgetModelFromJson(widgetResourceJson: JsonObject): BaseViewModel?
    fun getWidgetModelFromLiveLikeWidget(liveLikeWidget: LiveLikeWidget): BaseViewModel?

    /**
     * Returns widget models based on the options parameter provided
     * @param widgetTypeFilter widget kind
     * @param widgetStatus status of widget (published, queued)
     * @param ordering recent/oldest
     * @param interactive is widget interactive or not
     */
    fun getWidgets(
        liveLikePagination: LiveLikePagination,
        requestParams: WidgetsRequestParameters?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<LiveLikeWidget>>
    )

    fun isReceivingRealtimeUpdates(): Boolean

    /**
     * Returns  Flow and subscribe to specified pubnub channels
     */
    fun subscribeToChannels(channels:List<String>) : Flow<RealTimeClientMessage>?

    /**
     * Unsubscribe from specified pubnub channels
     */
    fun unsubscribeFromChannels(channels:List<String>)


}

data class WidgetsRequestParameters(
    val widgetTypeFilter: Set<WidgetType> = emptySet(),
    val widgetStatus: WidgetStatus? = null,
    val ordering: WidgetsRequestOrdering? = null,
    val interactive: Boolean? = null,
    /** since is expected in 2022-05-23T12:10:50.307Z format */
    val since: String? = null
)


enum class WidgetsRequestOrdering(val parameterValue: String) {
    RECENT("recent"),
    OLDEST("")
}

enum class WidgetStatus(val parameterValue: String) {
    PUBLISHED("published"),
    PENDING("pending"),
    SCHEDULED("scheduled")
}