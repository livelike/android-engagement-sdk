package com.livelike.engagementsdk


import com.livelike.common.LiveLikeKotlin
import com.livelike.common.profile
import com.livelike.engagementsdk.video.LiveLikeVideoClient

/**
 *  Creates a video client and returns that.
 */
fun LiveLikeKotlin.video(): LiveLikeVideoClient {
    val key = this.hashCode()
    return if (sdkInstanceWithVideoClient.containsKey(key)) {
        sdkInstanceWithVideoClient[key]!!
    } else {
        LiveLikeVideoClient.getInstance(
            sdkConfigurationOnce,
            profile().currentProfileOnce,
            sdkScope,
            uiScope,
            networkClient
        ).let {
            sdkInstanceWithVideoClient[key] = it
            it
        }
    }
}


private val sdkInstanceWithVideoClient = mutableMapOf<Int, LiveLikeVideoClient>()
