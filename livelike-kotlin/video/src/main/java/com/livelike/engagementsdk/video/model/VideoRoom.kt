package com.livelike.engagementsdk.video.model

import com.google.gson.annotations.SerializedName

data class VideoRoom(
    @SerializedName("id") var id: String,
    @SerializedName("created_by") var createdBy: String,
    @SerializedName("name") var name: String,
    @SerializedName("description") var description: String? = null,
    @SerializedName("video_room_session_identifier") var videoRoomSessionIdentifier: String,
    @SerializedName("client_id") var clientId: String
)