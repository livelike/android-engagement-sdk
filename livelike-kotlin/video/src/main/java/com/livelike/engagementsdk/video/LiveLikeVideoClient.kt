package com.livelike.engagementsdk.video


import com.google.gson.annotations.SerializedName
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.video.model.VideoRoom
import com.livelike.network.NetworkApiClient
import com.livelike.utils.Once
import kotlinx.coroutines.CoroutineScope

interface LiveLikeVideoClient {
    /**
     * Create a video room
     * @param name name of the video room to be created
     * @param description description of the room
     **/
    fun createVideoRoom(
        request: CreateVideoRoomRequest,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<VideoRoom>
    )

    /**
     * Get details of video room
     * @param id id of the video room for getting details
     *
     **/
    fun getVideoRoom(id: String, liveLikeCallback: com.livelike.common.LiveLikeCallback<VideoRoom>)


    companion object {

        fun getInstance(
            configurationOnce: Once<SdkConfiguration>,
            currentProfileOnce: Once<LiveLikeProfile>,
            sdkScope: CoroutineScope,
            uiScope: CoroutineScope,
            networkApiClient: NetworkApiClient
        ): LiveLikeVideoClient = InternalLiveLikeVideoClient(
            configurationOnce, currentProfileOnce, uiScope, sdkScope, networkApiClient
        )
    }

}

data class CreateVideoRoomRequest(
    @SerializedName("name") val name: String,
    @SerializedName("description") val description: String? = null
)