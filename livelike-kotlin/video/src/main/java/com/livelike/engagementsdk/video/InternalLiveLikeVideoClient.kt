package com.livelike.engagementsdk.video


import com.livelike.BaseClient
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.video.model.VideoRoom
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import com.livelike.utils.Once
import kotlinx.coroutines.CoroutineScope

internal class InternalLiveLikeVideoClient(
    configurationOnce: Once<SdkConfiguration>,
    currentProfileOnce: Once<LiveLikeProfile>,
    uiScope: CoroutineScope,
    sdkScope: CoroutineScope,
    private val networkApiClient: NetworkApiClient
) : BaseClient(configurationOnce, currentProfileOnce, sdkScope, uiScope),
    LiveLikeVideoClient {
    override fun createVideoRoom(
        request: CreateVideoRoomRequest,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<VideoRoom>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.videoRoomUrl.let { url ->
                networkApiClient.post(
                    url,
                    accessToken = pair.first.accessToken,
                    body = request.toJsonString()
                ).processResult()
            }
        }
    }

    override fun getVideoRoom(
        id: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<VideoRoom>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            pair.second.videoRoomDetailUrlTemplate.let { url ->
                networkApiClient.get(
                    url.replace(TEMPLATE_VIDEO_ROOM_ID, id),
                    accessToken = pair.first.accessToken
                ).processResult()
            }
        }
    }


}