@Suppress("DSL_SCOPE_VIOLATION")

plugins {
    `java-library`
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.detekt)
    `maven-publish`
//    alias(libs.plugins.dokka)
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}


publishing {
    publications {
        // creating a release publication
        create<MavenPublication>("maven") {
            groupId = "com.livelike.android-engagement-sdk"
            artifactId = "livelike-kotlin"
            version = "0.0.1"

            from(components["java"])
        }
    }
}

//subprojects.each { subproject -> evaluationDependsOn(subproject.path) }

tasks.jar {
    from(subprojects.map { it.sourceSets.main.get().output })
}

dependencies {
    api(project(":livelike-core:utils"))
    api(project(":livelike-core:network"))
    api(project(":livelike-core:serialization"))

    api(project(":livelike-kotlin:reaction"))
    api(project(":livelike-kotlin:common"))
    api(project(":livelike-kotlin:comment"))
    api(project(":livelike-kotlin:chat"))
    api(project(":livelike-kotlin:video"))
    api(project(":livelike-kotlin:gamification"))
    api(project(":livelike-kotlin:widget"))
    api(project(":livelike-kotlin:presence"))

    testImplementation(testFixtures(project(":livelike-kotlin:chat")))
}


