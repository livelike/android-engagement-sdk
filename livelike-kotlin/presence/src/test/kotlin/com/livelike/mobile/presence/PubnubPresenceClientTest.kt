package com.livelike.mobile.presence

import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive
import com.livelike.mobile.presence.models.JoinEvent
import com.livelike.mobile.presence.models.LeaveEvent
import com.livelike.mobile.presence.models.PresenceEvent
import com.livelike.mobile.presence.models.TimeoutEvent
import com.pubnub.api.PubNub
import com.pubnub.api.UserId
import com.pubnub.api.endpoints.presence.HereNow
import com.pubnub.api.endpoints.presence.SetState
import com.pubnub.api.models.consumer.PNStatus
import com.pubnub.api.models.consumer.presence.PNHereNowChannelData
import com.pubnub.api.models.consumer.presence.PNHereNowOccupantData
import com.pubnub.api.models.consumer.presence.PNHereNowResult
import com.pubnub.api.models.consumer.presence.PNSetStateResult
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNull
import org.junit.Test


internal class PubnubPresenceClientTest {

    @Test
    fun `simple sub unsub`() {

        val mockNub: PubNub = mockk()
        val hereNow: HereNow = mockk()

        every {
            mockNub.subscribe(listOf("demo_bob-pnpres"), emptyList(), false, 0)
        } returns Unit

        every {
            mockNub.unsubscribe(listOf("demo_bob-pnpres"), emptyList())
        } returns Unit

        every {
            mockNub.hereNow(listOf("demo_bob"), emptyList(), true, true)
        } returns hereNow

        every {
            hereNow.async(any())
        } returns Unit

        val dut = object : PubnubPresenceClient(mockk(), mockk(), mockk(), {  }) {
            override fun initialize() {
                clientID = "demo"
                pubnub = mockNub
            }
        }

        var lastEvent: PresenceEvent? = null
        val subscription = dut.subscribeForPresence(setOf("bob")) {
            lastEvent = it
        }

        verify(exactly = 1) { mockNub.hereNow(listOf("demo_bob"), includeState = true) }

        dut.unsubscribeForPresence(subscription)

        verify(exactly = 1) { mockNub.subscribe(channels = listOf("demo_bob-pnpres")) }
        verify(exactly = 1) { mockNub.unsubscribe(channels = listOf("demo_bob-pnpres")) }
        assertNull(lastEvent)

    }


    @Test
    fun `complex sub unsub`() {

        val mockNub: PubNub = mockk()
        val hereNow: HereNow = mockk()

        every {
            mockNub.subscribe(any(), emptyList(), false, 0)
        } returns Unit

        every {
            mockNub.unsubscribe(listOf("demo_dick-pnpres", "demo_harry-pnpres"), emptyList())
        } returns Unit

        every {
            mockNub.unsubscribe(listOf("demo_tom-pnpres", "demo_bob-pnpres"), emptyList())
        } returns Unit

        every {
            mockNub.unsubscribe(listOf("demo_guy-pnpres"), emptyList())
        } returns Unit

        every {
            mockNub.hereNow(any(), emptyList(), true, true)
        } returns hereNow

        every {
            hereNow.async(any())
        } returns Unit

        val dut = object : PubnubPresenceClient(mockk(), mockk(), mockk(), {
            //Results done
        }) {
            override fun initialize() {
                clientID = "demo"
                pubnub = mockNub
            }
        }

        var lastEvent: PresenceEvent? = null
        val subscriptionA = dut.subscribeForPresence(setOf("tom", "dick", "harry")) {
            lastEvent = it
        }
        val subscriptionB = dut.subscribeForPresence(setOf("tom", "bob", "guy")) {
            lastEvent = it
        }
        val subscriptionC = dut.subscribeForPresence(setOf("guy")) {
            lastEvent = it
        }

        verify(exactly = 1) {
            mockNub.subscribe(
                channels = listOf(
                    "demo_tom-pnpres",
                    "demo_dick-pnpres",
                    "demo_harry-pnpres"
                )
            )
        }
        verify(exactly = 1) {
            mockNub.subscribe(
                channels = listOf(
                    "demo_tom-pnpres",
                    "demo_bob-pnpres",
                    "demo_guy-pnpres"
                )
            )
        }
        verify(exactly = 1) { mockNub.subscribe(channels = listOf("demo_guy-pnpres")) }

        dut.unsubscribeForPresence(subscriptionA)
        verify(exactly = 1) {
            mockNub.unsubscribe(
                channels = listOf(
                    "demo_dick-pnpres",
                    "demo_harry-pnpres"
                )
            )
        }

        dut.unsubscribeForPresence(subscriptionB)
        verify(exactly = 1) {
            mockNub.unsubscribe(
                channels = listOf(
                    "demo_tom-pnpres",
                    "demo_bob-pnpres"
                )
            )
        }

        dut.unsubscribeForPresence(subscriptionC)
        verify(exactly = 1) { mockNub.unsubscribe(channels = listOf("demo_guy-pnpres")) }

        assertNull(lastEvent)

    }

    @Test
    fun `simple receive event`() {

        val mockNub: PubNub = mockk()
        val hereNow: HereNow = mockk()

        every {
            mockNub.subscribe(listOf("demo_bob-pnpres"), emptyList(), false, 0)
        } returns Unit

        every {
            mockNub.unsubscribe(listOf("demo_bob-pnpres"), emptyList())
        } returns Unit

        every {
            mockNub.hereNow(listOf("demo_bob"), emptyList(), true, true)
        } returns hereNow

        every {
            hereNow.async(any())
        } returns Unit

        val dut = object : PubnubPresenceClient(mockk(), mockk(), mockk(), {  }) {
            override fun initialize() {
                clientID = "demo"
                pubnub = mockNub
            }
        }

        var lastEvent: PresenceEvent? = null
        val subscription = dut.subscribeForPresence(setOf("bob")) {
            lastEvent = it
        }

        dut.onEvent(
            PNPresenceEventResult(
                event = "join",
                uuid = "some guy",
                channel = "demo_bob",
                state = null
            )
        )

        dut.unsubscribeForPresence(subscription)
        verify(exactly = 1) { mockNub.subscribe(channels = listOf("demo_bob-pnpres")) }
        verify(exactly = 1) { mockNub.unsubscribe(channels = listOf("demo_bob-pnpres")) }
        assertEquals(
            lastEvent, JoinEvent(
                userId = "some guy",
                channel = "bob",
                attributes = emptyMap()
            )
        )

    }

    @Test
    fun `interval receive event`() {

        val mockNub: PubNub = mockk()
        val hereNow: HereNow = mockk()

        every {
            mockNub.subscribe(listOf("demo_bob-pnpres"), emptyList(), false, 0)
        } returns Unit

        every {
            mockNub.unsubscribe(listOf("demo_bob-pnpres"), emptyList())
        } returns Unit

        every {
            mockNub.hereNow(listOf("demo_bob"), emptyList(), true, true)
        } returns hereNow

        every {
            hereNow.async(any())
        } returns Unit

        val dut = object : PubnubPresenceClient(mockk(), mockk(), mockk(), {  }) {
            override fun initialize() {
                clientID = "demo"
                pubnub = mockNub
            }
        }

        val events: MutableList<PresenceEvent> = mutableListOf()
        val subscription = dut.subscribeForPresence(setOf("bob")) {
            events.add(it)
        }

        dut.onEvent(
            PNPresenceEventResult(
                event = "interval",
                channel = "demo_bob",
                join = listOf("guy a", "guy b"),
                leave = listOf("guy c", "guy d"),
                timeout = listOf("guy e", "guy f"),
                hereNowRefresh = false
            )
        )

        dut.unsubscribeForPresence(subscription)
        verify(exactly = 1) { mockNub.subscribe(channels = listOf("demo_bob-pnpres")) }
        verify(exactly = 1) { mockNub.unsubscribe(channels = listOf("demo_bob-pnpres")) }
        //subscription calls this once, can call again if hereNowRefresh was true but it is not so 1 is correct
        verify(exactly = 1) { mockNub.hereNow(listOf("demo_bob"), includeState = true) }
        assertEquals(6, events.size)

        assertEquals(
            setOf(
                JoinEvent("guy a", "bob", emptyMap()),
                JoinEvent("guy b", "bob", emptyMap()),
                LeaveEvent("guy c", "bob", emptyMap()),
                LeaveEvent("guy d", "bob", emptyMap()),
                TimeoutEvent("guy e", "bob", emptyMap()),
                TimeoutEvent("guy f", "bob", emptyMap()),
            ), events.toSet()
        )

    }

    @Test
    fun `interval hereNow event`() {

        val mockNub: PubNub = mockk()
        val hereNow: HereNow = mockk()

        every {
            mockNub.subscribe(listOf("demo_bob-pnpres"), emptyList(), false, 0)
        } returns Unit

        every {
            mockNub.unsubscribe(listOf("demo_bob-pnpres"), emptyList())
        } returns Unit

        every {
            mockNub.hereNow(listOf("demo_bob"), emptyList(), true, true)
        } returns hereNow

        every {
            hereNow.async(any())
        } returns Unit

        val dut = object : PubnubPresenceClient(mockk(), mockk(), mockk(), {  }) {
            override fun initialize() {
                clientID = "demo"
                pubnub = mockNub
            }
        }

        val events: MutableList<PresenceEvent> = mutableListOf()
        val subscription = dut.subscribeForPresence(setOf("bob")) {
            events.add(it)
        }

        dut.onEvent(
            PNPresenceEventResult(
                event = "interval",
                channel = "demo_bob",
                hereNowRefresh = true
            )
        )

        dut.unsubscribeForPresence(subscription)
        verify(exactly = 1) { mockNub.subscribe(channels = listOf("demo_bob-pnpres")) }
        verify(exactly = 1) { mockNub.unsubscribe(channels = listOf("demo_bob-pnpres")) }
        //subscription calls this once, can call again if hereNowRefresh was true so 2 is correct
        verify(exactly = 2) { mockNub.hereNow(listOf("demo_bob"), includeState = true) }
        assertEquals(0, events.size)

    }

    @Test
    fun `setAttributes test`() {

        val mockNub: PubNub = mockk()
        val setState: SetState = mockk()
        val stateResult: PNSetStateResult = mockk()
        val hereNow: HereNow = mockk()

        every {
            mockNub.subscribe(listOf("demo_bob-pnpres"), emptyList(), false, 0)
        } returns Unit

        every {
            mockNub.subscribe(listOf("demo_bob"), emptyList(), false, 0)
        } returns Unit

        every {
            mockNub.unsubscribe(listOf("demo_bob-pnpres"), emptyList())
        } returns Unit

        every {
            mockNub.hereNow(listOf("demo_bob"), emptyList(), true, true)
        } returns hereNow

        every {
            hereNow.async(any())
        } returns Unit

        every {
            mockNub.setPresenceState(
                channels = listOf("demo_bob"),
                state = mapOf("bob" to "tom"),
                uuid = any()
            )
        } returns setState

        every {
            mockNub.configuration.userId
        } returns UserId("bob")

        every {
            setState.async(any())
        } answers {
            val arg = it.invocation.args[0] as (PNSetStateResult, PNStatus) -> Unit
            arg.invoke(stateResult, mockk<PNStatus>().also {
                every {
                    it.exception
                } returns null
            })
            Unit
        }

        every {
            stateResult.state
        } returns JsonObject().apply {
            add("bob", JsonPrimitive("tom"))
        }

        val dut = object : PubnubPresenceClient(mockk(), mockk(), mockk(), {  }) {
            override fun initialize() {
                clientID = "demo"
                pubnub = mockNub
            }
        }

        val events: MutableList<PresenceEvent> = mutableListOf()
        val subscription = dut.subscribeForPresence(setOf("bob")) {
            events.add(it)
        }

        dut.joinChannels(setOf("bob"))

        var wasRun = false
        dut.setAttributes(setOf("bob"), mapOf("bob" to "tom")) { result, error ->
            wasRun = true
            assertEquals(mapOf("bob" to "tom"), result)
        }

        assert(wasRun)

        dut.unsubscribeForPresence(subscription)
        verify(exactly = 1) { mockNub.subscribe(channels = listOf("demo_bob-pnpres")) }
        verify(exactly = 1) { mockNub.subscribe(channels = listOf("demo_bob")) }
        verify(exactly = 1) { mockNub.unsubscribe(channels = listOf("demo_bob-pnpres")) }
        verify(exactly = 1) {
            mockNub.setPresenceState(
                channels = listOf("demo_bob"),
                state = mapOf("bob" to "tom"),
                uuid = any()
            )
        }
        //subscription calls this once, can call again if hereNowRefresh was true so 2 is correct
        verify(exactly = 1) { mockNub.hereNow(listOf("demo_bob"), includeState = true) }
        assertEquals(0, events.size)
    }


    @Test
    fun `pull users on subscribe`() {

        val mockNub: PubNub = mockk()
        val hereNow: HereNow = mockk()
        val hereNowResult: PNHereNowResult = mockk()
        val status: PNStatus = mockk()
        val hereNowChannelData: PNHereNowChannelData = mockk()
        val hereNowOccupantData: PNHereNowOccupantData = mockk()

        every {
            mockNub.subscribe(listOf("demo_bob-pnpres"), emptyList(), false, 0)
        } returns Unit

        every {
            mockNub.unsubscribe(listOf("demo_bob-pnpres"), emptyList())
        } returns Unit

        every {
            hereNowResult.channels
        } returns HashMap<String, PNHereNowChannelData>().also {
            it["demo_bob"] = hereNowChannelData
        }

        every {
            hereNowChannelData.occupants
        } returns listOf(hereNowOccupantData)

        every {
            hereNowChannelData.channelName
        } returns "demo_bob"

        every {
            hereNowOccupantData.uuid
        } returns "some guy"

        every {
            hereNowOccupantData.state
        } returns null

        every {
            status.error
        } returns false

        every {
            mockNub.hereNow(channels = listOf("demo_bob"), includeState = true)
        } returns hereNow

        every {
            hereNow.async(any())
        } answers {
            val arg = it.invocation.args[0] as (PNHereNowResult?, PNStatus) -> Unit
            arg.invoke(hereNowResult, status)
            Unit
        }

        val dut = object : PubnubPresenceClient(mockk(), mockk(), mockk(), {  }) {
            override fun initialize() {
                clientID = "demo"
                pubnub = mockNub
            }
        }

        var lastEvent: PresenceEvent? = null
        val subscription = dut.subscribeForPresence(setOf("bob")) {
            lastEvent = it
        }

        verify(exactly = 1) { mockNub.hereNow(listOf("demo_bob"), includeState = true) }

        dut.unsubscribeForPresence(subscription)

        verify(exactly = 1) { mockNub.subscribe(channels = listOf("demo_bob-pnpres")) }
        verify(exactly = 1) { mockNub.unsubscribe(channels = listOf("demo_bob-pnpres")) }
        assertEquals(
            JoinEvent(
                "some guy",
                "bob",
                emptyMap()
            ), lastEvent
        )
    }


    @Test
    fun `remove user after hereNow`() {

        val mockNub: PubNub = mockk()
        val hereNow: HereNow = mockk()
        val hereNowResult: PNHereNowResult = mockk()
        val status: PNStatus = mockk()
        val hereNowChannelData: PNHereNowChannelData = mockk()
        val hereNowOccupantData: PNHereNowOccupantData = mockk()

        every {
            mockNub.subscribe(listOf("demo_bob-pnpres"), emptyList(), false, 0)
        } returns Unit

        every {
            mockNub.unsubscribe(listOf("demo_bob-pnpres"), emptyList())
        } returns Unit

        every {
            hereNowResult.channels
        } returns HashMap<String, PNHereNowChannelData>().also {
            it["demo_bob"] = hereNowChannelData
        }

        every {
            hereNowChannelData.occupants
        } returns listOf(hereNowOccupantData)

        every {
            hereNowChannelData.channelName
        } returns "demo_bob"

        var name = "some_guy"
        every {
            hereNowOccupantData.uuid
        } answers {
            name
        }

        every {
            hereNowOccupantData.state
        } returns null

        every {
            status.error
        } returns false

        every {
            mockNub.hereNow(channels = listOf("demo_bob"), includeState = true)
        } returns hereNow

        every {
            hereNow.async(any())
        } answers {
            val arg = it.invocation.args[0] as (PNHereNowResult?, PNStatus) -> Unit
            arg.invoke(hereNowResult, status)
            Unit
        }

        val dut = object : PubnubPresenceClient(mockk(), mockk(), mockk(), {

        }) {
            override fun initialize() {
                clientID = "demo"
                pubnub = mockNub
            }
        }

        var events: MutableList<PresenceEvent> = mutableListOf()
        val subscription = dut.subscribeForPresence(setOf("bob")) {
            events.add(it)
        }

        name = "other guy"

        dut.onEvent(
            PNPresenceEventResult(
                event = "interval",
                channel = "demo_bob",
                hereNowRefresh = true
            )
        )

        verify(exactly = 2) { mockNub.hereNow(listOf("demo_bob"), includeState = true) }

        dut.unsubscribeForPresence(subscription)

        verify(exactly = 1) { mockNub.subscribe(channels = listOf("demo_bob-pnpres")) }
        verify(exactly = 1) { mockNub.unsubscribe(channels = listOf("demo_bob-pnpres")) }

        assertEquals(
            listOf(
                JoinEvent("some_guy", "bob", emptyMap()),
                LeaveEvent("some_guy", "bob", emptyMap()),
                JoinEvent("other guy", "bob", emptyMap()),
            ), events
        )


    }


}