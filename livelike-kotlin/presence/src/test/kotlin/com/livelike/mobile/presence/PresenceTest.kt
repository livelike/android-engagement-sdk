package com.livelike.mobile.presence

import com.livelike.mobile.presence.models.JoinEvent
import com.livelike.mobile.presence.models.LeaveEvent
import com.livelike.mobile.presence.models.PresenceEvent
import com.livelike.mobile.presence.models.UpdateEvent
import org.junit.Test

class TestPresenceClient{

    @Test
    fun `can process join events`(){

        val presenceClient = BasePresenceClient()

        var event: PresenceEvent? = null
        val subscription = presenceClient.subscribeForPresence(
            channels = setOf("bob")
        ) {
            event = it
        }

        presenceClient.onEvent(
            event = "join",
            userId = "id",
            channel = "demo_bob",
            attributes = mapOf("tom" to "tom value")
        )

        assert( event != null )
        assert( event is JoinEvent )
        assert( event?.userId == "id" )
        assert( event?.channel == "bob" )
        assert( event?.attributes == mapOf( "tom" to "tom value") )

    }

    @Test
    fun `can process join events with empty attributes`(){

        val presenceClient = BasePresenceClient()

        var event: PresenceEvent? = null
        val subscription = presenceClient.subscribeForPresence(
            channels = setOf("bob")
        ) {
            event = it
        }

        presenceClient.onEvent(
            event = "join",
            userId = "id",
            channel = "demo_bob",
            attributes = null
        )

        assert( event != null )
        assert( event is JoinEvent )
        assert( event?.userId == "id" )
        assert( event?.channel == "bob" )
        assert( event?.attributes == emptyMap<String,String>() )

    }

    @Test
    fun `can process leave events`(){

        val presenceClient = BasePresenceClient()

        var event: PresenceEvent? = null
        val subscription = presenceClient.subscribeForPresence(
            channels = setOf("bob")
        ) {
            event = it
        }

        presenceClient.onEvent(
            event = "leave",
            userId = "id",
            channel = "demo_bob",
            attributes = null
        )

        assert( event != null )
        assert( event is LeaveEvent )
        assert( event?.userId == "id" )
        assert( event?.channel == "bob" )
        assert( event?.attributes == emptyMap<String,String>() )

    }

    @Test
    fun `can process state change events`(){

        val presenceClient = BasePresenceClient()

        var event: PresenceEvent? = null
        val subscription = presenceClient.subscribeForPresence(
            channels = setOf("bob")
        ) {
            event = it
        }

        presenceClient.onEvent(
            event = "state-change",
            userId = "id",
            channel = "demo_bob",
            attributes = mapOf("tom" to "tom value")
        )

        assert( event != null )
        assert( event is UpdateEvent)
        assert( event?.userId == "id" )
        assert( event?.channel == "bob" )
        assert( event?.attributes == mapOf( "tom" to "tom value") )

    }

    @Test
    fun `unsubscribe works`(){

        val presenceClient = BasePresenceClient()

        var event: PresenceEvent? = null
        val subscription = presenceClient.subscribeForPresence(
            channels = setOf("bob")
        ) {
            event = it
        }

        presenceClient.onEvent(
            event = "join",
            userId = "id",
            channel = "demo_bob",
            attributes = null
        )

        assert( event != null )
        assert( event is JoinEvent )
        assert( event?.userId == "id" )
        assert( event?.channel == "bob" )
        assert( event?.attributes == emptyMap<String,String>() )

        presenceClient.unsubscribeForPresence(subscription)

        presenceClient.onEvent(
            event = "leave",
            userId = "id-2",
            channel = "demo_bob",
            attributes = null
        )

        //we should not get new data on the closed channel
        assert( event != null )
        assert( event is JoinEvent )
        assert( event?.userId == "id" )
        assert( event?.channel == "bob" )
        assert( event?.attributes == emptyMap<String,String>() )

    }

    @Test
    fun `no bad delivery`(){

        val presenceClient = BasePresenceClient()

        var event: PresenceEvent? = null
        val subscription = presenceClient.subscribeForPresence(
            channels = setOf("bob")
        ) {
            event = it
        }

        presenceClient.onEvent(
            event = "join",
            userId = "id",
            channel = "demo_bob",
            attributes = null
        )

        assert( event != null )
        assert( event is JoinEvent )
        assert( event?.userId == "id" )
        assert( event?.channel == "bob" )
        assert( event?.attributes == emptyMap<String,String>() )

        presenceClient.onEvent(
            event = "join",
            userId = "id",
            channel = "demo_gob",
            attributes = null
        )

        //we should not get new data from a different channel
        assert( event != null )
        assert( event is JoinEvent )
        assert( event?.userId == "id" )
        assert( event?.channel == "bob" )
        assert( event?.attributes == emptyMap<String,String>() )

    }

}