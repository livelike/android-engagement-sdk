package com.livelike.mobile.presence

import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

val GSON_ATTRIBUTE_TYPE_DEF: Type = object : TypeToken<Map<String, String>>() {}.type

const val INTERVAL_EVENT = "interval"
const val JOIN_EVENT = "join"
const val LEAVE_EVENT = "leave"
const val TIMEOUT_EVENT = "timeout"
const val STATE_CHANGE_EVENT = "state-change"
