package com.livelike.mobile.presence

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.livelike.common.LiveLikeCallback
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.toNewCallback
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.mobile.presence.models.PresenceEvent
import com.livelike.mobile.presence.models.PresenceEventCallback
import com.livelike.utils.Once
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import com.pubnub.api.PNConfiguration
import com.pubnub.api.PubNub
import com.pubnub.api.UserId
import com.pubnub.api.callbacks.SubscribeCallback
import com.pubnub.api.enums.PNOperationType
import com.pubnub.api.enums.PNStatusCategory
import com.pubnub.api.models.consumer.PNStatus
import com.pubnub.api.models.consumer.presence.PNHereNowResult
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

internal open class PubnubPresenceClient(
    private val configurationOnce: Once<SdkConfiguration>,
    private val currentProfileOnce: Once<LiveLikeProfile>,
    private val sdkScope: CoroutineScope,
    private val completion: (Boolean) -> Unit,
) : BasePresenceClient() {

    lateinit var pubnub: PubNub
    lateinit var clientID: String
    private val gson = Gson()

    init {
        initialize()
    }

    open fun initialize() {
        sdkScope.launch {
            try {
                val configuration = configurationOnce.invoke()
                val user = currentProfileOnce.invoke()
                clientID = configuration.clientId
                val pubnubConfiguration = PNConfiguration(UserId(user.id))

                configuration.pubNubKey?.let {
                    pubnubConfiguration.subscribeKey = it
                }
                pubnubConfiguration.presenceTimeout = configuration.pubnubPresenceTimeout
                pubnubConfiguration.heartbeatInterval = configuration.pubnubHeartbeatInterval
                if (pubnubConfiguration.subscribeKey.isNotEmpty()) {
                    pubnub = PubNub(pubnubConfiguration)
                    addPubnubListener()
                    logDebug { "PubNub is being initialized!" }
                } else {
                    logDebug {
                        """
                        PubNub is not initialized for this application.
                        
                        Should you require Presence as a Service functionality 
                        please contact LiveLike customer support 
                    """.trimMargin()
                    }
                }
                completion(true)
            } catch (e: Exception) {
                logError { e.message }
                e.printStackTrace()
                completion(false)
            }

        }
    }

    private fun addPubnubListener() {
        pubnub.addListener(object : SubscribeCallback() {
            override fun status(pubnub: PubNub, pnStatus: PNStatus) {
                when (pnStatus.operation) {
                    PNOperationType.PNSubscribeOperation, PNOperationType.PNUnsubscribeOperation -> {
                        when (pnStatus.category) {
                            PNStatusCategory.PNTimeoutCategory, PNStatusCategory.PNUnexpectedDisconnectCategory -> pubnub.reconnect()

                            else -> {}
                        }
                    }

                    else -> {}
                }
            }

            override fun presence(pubnub: PubNub, pnPresenceEventResult: PNPresenceEventResult) {
                onEvent(pnPresenceEventResult)
            }
        })
    }

    internal fun onEvent(pnEvent: PNPresenceEventResult) {
        if (pnEvent.event == INTERVAL_EVENT) { //pubnub specific update type
            intervalUpdate(pnEvent)
        } else {
            onEvent(
                pnEvent.event ?: "", pnEvent.uuid, pnEvent.channel, toAttributes(pnEvent.state)
            )
        }
    }

    private fun intervalUpdate(pnPresenceEventResult: PNPresenceEventResult) {
        pnPresenceEventResult.channel?.let { channel ->
            pnPresenceEventResult.join?.let { list ->
                list.forEach { userId ->
                    onEvent(JOIN_EVENT, userId, channel, emptyMap())
                }
            } ?: logDebug { "interval Update join is null" }

            pnPresenceEventResult.leave?.let { list ->
                list.forEach { userId ->
                    onEvent(LEAVE_EVENT, userId, channel, emptyMap())
                }
            } ?: logDebug { "interval Update leave is null" }

            pnPresenceEventResult.timeout?.let { list ->
                list.forEach { userId ->
                    onEvent(TIMEOUT_EVENT, userId, channel, emptyMap())
                }
            } ?: logDebug { "interval Update timeout is null" }

            if (pnPresenceEventResult.hereNowRefresh == true) {
                pullCurrentUsers(setOf(channel))
            }
        } ?: logDebug { "interval presence update with null channel" }

    }

    override fun subscribeChannel(channels: List<String>) {
        super.subscribeChannel(channels)
        pubnub.subscribe(channels = channels)
    }

    override fun unsubscribeChannel(channels: List<String>) {
        super.unsubscribeChannel(channels)
        pubnub.unsubscribe(channels = channels)
    }

    override fun unsubscribeAllChannels() {
        super.unsubscribeAllChannels()
        pubnub.unsubscribeAll()
    }

    override fun setAttributes(
        channels: Set<String>,
        attributes: Map<String, String>,
        completion: LiveLikeCallback<Map<String, String>>?
    ) {
        super.setAttributes(channels, attributes, completion)
        pubnub.setPresenceState(
            channels = channels.toList().applyPrefix(), state = attributes
        ).async { output, status ->
            completion?.invoke(toAttributes(output?.state), status.exception?.message)
                ?: logDebug { "Completion is null" }
        }
    }

    override fun subscribeForPresence(
        channels: Set<String>, callback: (PresenceEvent) -> Unit
    ): PresenceEventCallback {
        return super.subscribeForPresence(channels, callback).also {
            if (it is InternalPresenceEventCallback) {
                pullCurrentUsers(it.channels)
            }
        }
    }

    private fun pullCurrentUsers(channels: Set<String>) {
        pubnub.hereNow(
            channels = channels.toList(),
            includeState = true,
        ).async(::processBulkPresenceUpdate)
    }

    private fun processBulkPresenceUpdate(result: PNHereNowResult?, status: PNStatus) {
        if (status.error) {
            return
        }
        result?.channels?.values?.forEach { channelData ->

            //find users who are in local cache but not in here now
            activePresenceSubscriptions.filter { it.channels.contains(channelData.channelName) }
                .forEach { eventCallback ->
                    eventCallback.cache.keys.minus(channelData.occupants.map { it.uuid }.toSet())
                        .forEach { userId ->
                            onEvent(
                                LEAVE_EVENT, userId, channelData.channelName, toAttributes(null)
                            )
                        }
                }

            //everyone who is here can join in
            channelData.occupants.forEach { occ ->
                onEvent(
                    JOIN_EVENT, occ.uuid, channelData.channelName, toAttributes(occ.state)
                )
            }
        }
    }

    override fun whereNow(
        userId: String, completion: LiveLikeCallback<List<String>>
    ) {
        pubnub.whereNow(userId).async { result, status ->
            completion.invoke(
                result?.channels?.map { it.rmPrefix() }, status.exception?.message
            )
        }
    }

    override fun whereNow(
        userId: String,
        completion: com.livelike.engagementsdk.publicapis.LiveLikeCallback<List<String>>
    ) {
        whereNow(userId, completion.toNewCallback())
    }

    override fun hereNow(
        channels: Set<String>, completion: LiveLikeCallback<Map<String, List<String>>>
    ) {
        pubnub.hereNow(
            channels = channels.applyPrefix()
        ).async { result, status ->
            if (status.error) {
                completion.invoke(null, status.exception?.message)
            }
            completion.invoke(
                result?.channels?.entries?.associate { entry ->
                    entry.key.rmPrefix() to entry.value.occupants.map { it.uuid }
                }, null
            )
        }
    }

    override fun hereNow(
        channels: Set<String>,
        completion: com.livelike.engagementsdk.publicapis.LiveLikeCallback<Map<String, List<String>>>
    ) {
        hereNow(channels, completion.toNewCallback())
    }

    override fun getAttributes(
        userId: String,
        channels: Set<String>,
        completion: LiveLikeCallback<Map<String, Map<String, String>>>
    ) {
        pubnub.getPresenceState(channels = channels.toList().applyPrefix(), uuid = userId)
            .async { result, status ->
                completion.invoke(
                    result?.stateByUUID?.entries?.associate {
                        it.key.rmPrefix() to toAttributes(it.value)
                    }, status.exception?.message
                )
            }
    }

    override fun getAttributes(
        userId: String,
        channels: Set<String>,
        completion: com.livelike.engagementsdk.publicapis.LiveLikeCallback<Map<String, Map<String, String>>>
    ) {
        getAttributes(userId, channels, completion.toNewCallback())
    }

    private fun toAttributes(state: JsonElement?): Map<String, String> = state?.let {
        gson.fromJson(state, GSON_ATTRIBUTE_TYPE_DEF)
    } ?: emptyMap()

    override fun getPrefix(): String {
        return clientID
    }

    override fun destroy() {
        super.destroy()
        pubnub.disconnect()
        pubnub.destroy()
    }

}