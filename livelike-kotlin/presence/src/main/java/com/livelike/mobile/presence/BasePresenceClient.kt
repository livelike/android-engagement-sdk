package com.livelike.mobile.presence


import com.livelike.common.LiveLikeCallback
import com.livelike.common.toNewCallback
import com.livelike.mobile.presence.models.JoinEvent
import com.livelike.mobile.presence.models.LeaveEvent
import com.livelike.mobile.presence.models.PresenceEvent
import com.livelike.mobile.presence.models.PresenceEventCallback
import com.livelike.mobile.presence.models.TimeoutEvent
import com.livelike.mobile.presence.models.UpdateEvent
import com.livelike.utils.logDebug
import java.util.Collections

internal open class BasePresenceClient : LiveLikePresenceClient {

    val activePresenceSubscriptions: MutableList<InternalPresenceEventCallback> =
        Collections.synchronizedList(mutableListOf())

    internal fun onEvent(
        event: String, userId: String?, channel: String?, attributes: Map<String, String>?
    ) {
        when (event) {
            JOIN_EVENT -> ::joinUpdate
            LEAVE_EVENT -> ::leaveUpdate
            TIMEOUT_EVENT -> ::timeoutUpdate
            STATE_CHANGE_EVENT -> ::stateUpdate
            else -> ::unknownUpdate
        }(userId, channel, attributes)
    }

    private fun joinUpdate(userId: String?, channel: String?, attributes: Map<String, String>?) {
        channel?.let {
            activePresenceSubscriptions.filter {
                it.channels.contains(channel)
            }.forEach { callbackHandle ->
                userId?.let { userId ->
                    callbackHandle.onCallback(
                        JoinEvent(
                            userId, channel.rmPrefix(), attributes ?: emptyMap()
                        )
                    )
                }
            }
        } ?: logDebug { "join Update channel is null" }
    }

    private fun leaveUpdate(userId: String?, channel: String?, attributes: Map<String, String>?) {
        channel?.let {
            activePresenceSubscriptions.filter {
                it.channels.contains(channel)
            }.forEach { callbackHandle ->
                userId?.let { userId ->
                    callbackHandle.onCallback(
                        LeaveEvent(
                            userId, channel.rmPrefix(), attributes ?: emptyMap()
                        )
                    )
                }
            }
        } ?: logDebug { "leave Update channel is null" }
    }

    private fun timeoutUpdate(userId: String?, channel: String?, attributes: Map<String, String>?) {
        channel?.let {
            activePresenceSubscriptions.filter {
                it.channels.contains(channel)
            }.forEach { callbackHandle ->
                userId?.let { userId ->
                    callbackHandle.onCallback(
                        TimeoutEvent(
                            userId, channel.rmPrefix(), attributes ?: emptyMap()
                        )
                    )
                }
            }
        } ?: logDebug { "timeout Update channel is null" }
    }

    private fun stateUpdate(userId: String?, channel: String?, attributes: Map<String, String>?) {
        channel?.let {
            activePresenceSubscriptions.filter {
                it.channels.contains(channel)
            }.forEach { callbackHandle ->
                userId?.let { userId ->
                    callbackHandle.onCallback(
                        UpdateEvent(
                            userId, channel.rmPrefix(), attributes ?: emptyMap()
                        )
                    )
                }
            }
        } ?: logDebug { "status Update channel is null" }
    }

    private fun unknownUpdate(userId: String?, channel: String?, attributes: Map<String, String>?) {
        logDebug { "unknown event from $userId, $channel, $attributes" }
    }

    protected open fun subscribeChannel(channels: List<String>) {
    }

    protected open fun unsubscribeChannel(channels: List<String>) {
    }

    protected open fun unsubscribeAllChannels() {
    }

    override fun joinChannels(
        channels: Set<String>
    ) {
        subscribeChannel(channels.toList().applyPrefix())
    }

    override fun leaveChannels(
        channels: Set<String>
    ) {
        unsubscribeChannel(channels.toList().applyPrefix())
    }

    override fun setAttributes(
        channels: Set<String>,
        attributes: Map<String, String>,
        completion: LiveLikeCallback<Map<String, String>>?
    ) {
    }

    override fun subscribeForPresence(
        channels: Set<String>, callback: (PresenceEvent) -> Unit
    ): PresenceEventCallback {
        return InternalPresenceEventCallback(channels.applyPrefix().toSet(), callback).also {
            activePresenceSubscriptions.add(it)
            subscribeChannel(it.presenceChannels)
        }

    }

    override fun unsubscribeForPresence(
        callback: PresenceEventCallback
    ) {
        if (callback is InternalPresenceEventCallback) {
            callback.active = false

            /*
             may have duplicate channels in other subscriptions
             if another subscription uses a channel remove it from the
             set of channels we unsub from.
             */
            val toUnSub = callback.presenceChannels.toSet()
                .subtract(activePresenceSubscriptions.filter { it != callback }
                    .fold(emptySet()) { acc, internal ->
                        acc.plus(internal.presenceChannels)
                    })

            unsubscribeChannel(toUnSub.toList())
            activePresenceSubscriptions.remove(callback)
        }
    }

    override fun unsubscribeAllPresence() {
        activePresenceSubscriptions.toList().forEach {
            unsubscribeForPresence(it)
        }
    }

    override fun whereNow(userId: String, completion: LiveLikeCallback<List<String>>) {
    }

    override fun whereNow(
        userId: String,
        completion: com.livelike.engagementsdk.publicapis.LiveLikeCallback<List<String>>
    ) {
        whereNow(userId, completion.toNewCallback())
    }

    override fun hereNow(
        channels: Set<String>, completion: LiveLikeCallback<Map<String, List<String>>>
    ) {
    }

    override fun hereNow(
        channels: Set<String>,
        completion: com.livelike.engagementsdk.publicapis.LiveLikeCallback<Map<String, List<String>>>
    ) {
        hereNow(channels, completion.toNewCallback())
    }

    override fun getAttributes(
        userId: String,
        channels: Set<String>,
        completion: LiveLikeCallback<Map<String, Map<String, String>>>
    ) {
    }

    override fun getAttributes(
        userId: String,
        channels: Set<String>,
        completion: com.livelike.engagementsdk.publicapis.LiveLikeCallback<Map<String, Map<String, String>>>
    ) {
       getAttributes(userId, channels, completion.toNewCallback())
    }

    override fun destroy() {
        unsubscribeAllPresence()
        unsubscribeAllChannels()
    }

    protected open fun getPrefix(): String {
        return "demo"
    }

    fun Collection<String>.applyPrefix(): List<String> {
        return map { channel -> "${getPrefix()}_$channel" }
    }

    protected fun String.rmPrefix(): String {
        return this.removePrefix("${getPrefix()}_")
    }

}