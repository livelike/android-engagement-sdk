package com.livelike.mobile.presence.models

/**
 * metadata associated with an existing presence callback
 */
interface PresenceEventCallback{

    /**
     * is this callback still actively backed by a feed
     */
    fun isActive(): Boolean

}