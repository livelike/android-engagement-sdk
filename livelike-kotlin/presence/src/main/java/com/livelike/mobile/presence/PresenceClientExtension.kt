package com.livelike.mobile.presence

import com.livelike.common.LiveLikeKotlin
import com.livelike.common.profile

fun LiveLikeKotlin.presenceClient(completion: (Boolean) -> Unit): LiveLikePresenceClient {
    val key = this.hashCode()
    return if (sdkInstanceWithPresenceClient.containsKey(key)) {
        completion(true)
        sdkInstanceWithPresenceClient[key]!!
    } else {
        LiveLikePresenceClient.getInstance(
            sdkConfigurationOnce,
            profile().currentProfileOnce,
            uiScope,
            completion
        ).let {
            sdkInstanceWithPresenceClient[key] = it
            it
        }
    }
}


private val sdkInstanceWithPresenceClient = mutableMapOf<Int, LiveLikePresenceClient>()
