package com.livelike.mobile.presence

import com.livelike.common.LiveLikeCallback
import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.mobile.presence.models.PresenceEvent
import com.livelike.mobile.presence.models.PresenceEventCallback
import com.livelike.utils.Once
import kotlinx.coroutines.CoroutineScope


interface LiveLikePresenceClient {

    /**
     * join channels to show your presence
     */
    fun joinChannels(
        channels: Set<String>
    )

    /**
     * leave channels to remove your presence
     */
    fun leaveChannels(
        channels: Set<String>
    )

    /**
     * update attributes associate with your presence on specific channels
     */
    fun setAttributes(
        channels: Set<String>,
        attributes: Map<String, String>,
        completion: LiveLikeCallback<Map<String, String>>? = null
    )

    /**
     * locate channels where a user has a presence
     */
    fun whereNow(
        userId: String,
        completion: LiveLikeCallback<List<String>>
    )

    @Deprecated(
        "Use whereNow",
        ReplaceWith("whereNow(String,com.livelike.common.LiveLikeCallback<List<String>>")
    )
    fun whereNow(
        userId: String,
        completion: com.livelike.engagementsdk.publicapis.LiveLikeCallback<List<String>>
    )

    /**
     * retrieve all users currently in listed channels
     */
    fun hereNow(
        channels: Set<String>,
        completion: LiveLikeCallback<Map<String, List<String>>>
    )

    @Deprecated(
        "Use hereNow",
        ReplaceWith("hereNow(Set<String>,com.livelike.common.LiveLikeCallback<Map<String, List<String>>>")
    )
    fun hereNow(
        channels: Set<String>,
        completion: com.livelike.engagementsdk.publicapis.LiveLikeCallback<Map<String, List<String>>>
    )

    /**
     * retrieve attributes for users on specific channels
     * @returns a map of channels and the attributes associated with the user in that channel
     */
    fun getAttributes(
        userId: String,
        channels: Set<String>,
        completion: LiveLikeCallback<Map<String, Map<String, String>>>
    )
    @Deprecated(
        "Use getAttributes",
        ReplaceWith("getAttributes(String,Set<String>,com.livelike.common.LiveLikeCallback<Map<String, Map<String, String>>>")
    )
    fun getAttributes(
        userId: String,
        channels: Set<String>,
        completion: com.livelike.engagementsdk.publicapis.LiveLikeCallback<Map<String, Map<String, String>>>
    )

    /**
     * starts feed of presence events and persists until unsubscribed
     */
    fun subscribeForPresence(
        channels: Set<String>,
        callback: (PresenceEvent) -> Unit
    ): PresenceEventCallback

    /**
     * stops the presence event feed associated to this callback
     */
    fun unsubscribeForPresence(callback: PresenceEventCallback)

    /**
     * stops all active presence event feeds
     *
     * unsubscribes all callbacks
     *
     * this will abandon existing callbacks
     */
    fun unsubscribeAllPresence()

    /**
     * stops all active presence event feeds
     *
     * unsubscribes all callbacks
     *
     * used to assure their are no active connections and cleans up any internal resources
     */
    fun destroy()

    companion object {

        fun getInstance(
            sdkConfigurationOnce: Once<SdkConfiguration>,
            currentProfileOnce: Once<LiveLikeProfile>,
            sdkScope: CoroutineScope,
            completion: (Boolean) -> Unit
        ): LiveLikePresenceClient = PubnubPresenceClient(
            sdkConfigurationOnce, currentProfileOnce, sdkScope, completion
        )

    }

}