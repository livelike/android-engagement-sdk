package com.livelike.mobile.presence.models

/**
 * base class for all presence events
 */
sealed class PresenceEvent(
    val userId: String,
    val channel: String,
    val attributes: Map<String, String>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PresenceEvent

        if (userId != other.userId) return false
        if (channel != other.channel) return false
        if (attributes != other.attributes) return false

        return true
    }

    override fun hashCode(): Int {
        var result = userId.hashCode()
        result = 31 * result + channel.hashCode()
        result = 31 * result + attributes.hashCode()
        return result
    }
}

/**
 * a user has joined the channel
 */
class JoinEvent(
    userId: String,
    channel: String,
    attributes: Map<String, String>,
) : PresenceEvent(userId, channel, attributes)

/**
 * a user has left the channel
 */
class LeaveEvent(
    userId: String,
    channel: String,
    attributes: Map<String, String>,
) : PresenceEvent(userId, channel, attributes)

/**
 * a user has timed out of the channel
 */
class TimeoutEvent(
    userId: String,
    channel: String,
    attributes: Map<String, String>,
) : PresenceEvent(userId, channel, attributes)

/**
 * a users metadata has changed
 */
class UpdateEvent(
    userId: String,
    channel: String,
    attributes: Map<String, String>,
) : PresenceEvent(userId, channel, attributes)