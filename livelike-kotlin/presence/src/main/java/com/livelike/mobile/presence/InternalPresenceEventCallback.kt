package com.livelike.mobile.presence

import com.livelike.mobile.presence.models.PresenceEvent
import com.livelike.mobile.presence.models.PresenceEventCallback

internal class InternalPresenceEventCallback(
    val channels: Set<String>,
    private val callback: (PresenceEvent) -> Unit
) : PresenceEventCallback {

    //use -pnpres here as we only want the presence channel
    val presenceChannels: List<String> = channels.map { "$it-pnpres" }
    var active: Boolean = true

    val cache: MutableMap<String, PresenceEvent> = mutableMapOf()

    override fun isActive(): Boolean {
        return active
    }

    fun onCallback(event: PresenceEvent) {
        cache[event.userId] = event
        callback(event)
    }

}