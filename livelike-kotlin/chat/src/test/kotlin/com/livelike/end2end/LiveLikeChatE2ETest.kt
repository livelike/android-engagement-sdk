package com.livelike.end2end

import com.livelike.common.AccessTokenDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.common.profile
import com.livelike.engagementsdk.chat
import com.livelike.engagementsdk.chat.LiveLikeChatClient
import com.livelike.engagementsdk.chat.Visibility
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.createChatSession
import com.livelike.engagementsdk.publicapis.ChatRoomRequestOptions
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.serialization.toJsonString
import com.livelike.utils.LiveLikeException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine
import kotlin.time.Duration.Companion.seconds

class LiveLikeChatE2ETest {

    private lateinit var chatClient: LiveLikeChatClient
    private lateinit var liveLikeKotlin: LiveLikeKotlin
    private val testDispatcher = StandardTestDispatcher()
    private lateinit var clientId: String
    private lateinit var producerToken: String


    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        producerToken = "kpq3LJ9X-0yD7sdPGKe59j8YwtUrOUeesJj9-kbEdtw4daCLdhb0Jg"
        clientId = "8PqSNDgIVHnXuJuGte1HdvOjOqhCFE1ZCR3qhqaS"
        /*producerToken = System.getenv("producerToken")
        clientId = System.getenv("clientId")*/
        liveLikeKotlin = LiveLikeKotlin(
            clientId,
            accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String? {
                    return producerToken
                }

                override fun storeAccessToken(accessToken: String?) {

                }

            },
            networkDispatcher = testDispatcher,
            sdkDispatcher = testDispatcher,
            mainDispatcher = testDispatcher
        )
        chatClient = liveLikeKotlin.chat()
    }

    @After
    fun tearDown() {
        testDispatcher.cancel()
    }

    @Test
    fun sendAndDeleteMessage() = runTest(timeout = 30.seconds) {
        liveLikeKotlin.profile().currentProfileOnce()
        //Create Chat Room
        val chatRoom = suspendCoroutine { cont ->
            chatClient.createChatRoom(
                title = "Test Chat Room",
                visibility = Visibility.everyone,
                tokenGates = null
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        // Fetch ChatRoom Details
        val chatRoomDetails = suspendCoroutine { cont ->
            chatClient.getChatRoom(chatRoom.id) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(chatRoomDetails.id.isNotEmpty())
        assert(chatRoomDetails.title == "Test Chat Room")
        assert(chatRoomDetails.visibility == Visibility.everyone)
        //Create chat Session
        val session = liveLikeKotlin.createChatSession(
            sessionDispatcher = testDispatcher,
            synchronizationDispatcher = testDispatcher,
            mainDispatcher = testDispatcher
        )
        suspendCoroutine { cont ->
            session.connectToChatRoom(chatRoomId = chatRoomDetails.id, callback = { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            })
        }
        assert(session.getCurrentChatRoom() == chatRoomDetails.id)



        //Send a Text Message
      val textMessage = suspendCoroutine { cont ->
            session.sendMessage(
                "My Test Message",
                liveLikePreCallback = { _, _ -> },
                callback = { result, error ->
                    result?.let { cont.resume(it) }
                    error?.let { cont.resumeWithException(LiveLikeException(it)) }
                })
        }
        assert(textMessage.message == "My Test Message")

        //Send a Custom Message(a)
        val customMessageA = suspendCoroutine<LiveLikeChatMessage> { cont ->
            session.sendCustomChatMessage("Test Custom Message") { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(customMessageA.customData == "Test Custom Message") {
            "Message not matched: ${customMessageA.customData}"
        }

        //Send a Custom Message(b)
        val customData = mapOf(
            "name" to "shivam",
            "rank" to 1
        ).toJsonString()
        val customMessageB = suspendCoroutine<LiveLikeChatMessage> { cont ->
            session.sendCustomChatMessage(customData) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(customMessageB.customData == customData) {
            "Message not matched: ${customMessageB.customData}"
        }

        //Delete Message
        suspendCoroutine<LiveLikeEmptyResponse> { cont ->
            session.deleteMessage(textMessage.id!!) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        assert(session.getDeletedMessages().contains(textMessage.id))

        suspendCoroutine<LiveLikeEmptyResponse> { cont ->
            session.deleteMessage(customMessageA.id!!) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(session.getDeletedMessages().contains(customMessageA.id))
        suspendCoroutine<LiveLikeEmptyResponse> { cont ->
            session.deleteMessage(customMessageB.id!!) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(session.getDeletedMessages().contains(customMessageB.id))

        session.close()
    }


    @Test
    fun test_start_thread_and_get_thread() = runTest(timeout = 30.seconds) {
        liveLikeKotlin.profile().currentProfileOnce()
        //Create Chat Room
        val chatRoom = suspendCoroutine { cont ->
            chatClient.createChatRoom(
                title = "Thread Test Chat Room",
                visibility = Visibility.everyone,
                tokenGates = null
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        // Fetch ChatRoom Details
        val chatRoomDetails = suspendCoroutine { cont ->
            chatClient.getChatRoom(chatRoom.id) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }

        //Create chat Session
        val session = liveLikeKotlin.createChatSession(
            sessionDispatcher = testDispatcher,
            synchronizationDispatcher = testDispatcher,
            mainDispatcher = testDispatcher
        )
        suspendCoroutine { cont ->
            session.connectToChatRoom(chatRoomId = chatRoomDetails.id, callback = { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            })
        }
        assert(session.getCurrentChatRoom() == chatRoomDetails.id)

        //parent message
        val parentMessage = suspendCoroutine { cont ->
            session.sendMessage(
                message = "parent message",
                liveLikePreCallback = { _, _ -> },
                callback = { result, error ->
                    result?.let { cont.resume(it) }
                    error?.let { cont.resumeWithException(LiveLikeException(it)) }
                })
        }
        assert(parentMessage.message == "parent message")

        // thread for the parent message
        val replyToParentMessage = suspendCoroutine { cont ->
            session.sendMessage(
                message = "Thread for parent message",
                parentMessageId = parentMessage.id,
                liveLikePreCallback = { _, _ -> },
                callback = { result, error ->
                    result?.let { cont.resume(it) }
                    error?.let { cont.resumeWithException(LiveLikeException(it)) }
                })
        }
        assert(replyToParentMessage.message == "Thread for parent message")
        assert(replyToParentMessage.parentMessageId == parentMessage.id)

        //get replies for the parent message
      /*  val getReplies = suspendCoroutine { cont ->
            session.getChatMessageReplies(parentMessage.id?.let { GetChatMessageRepliesRequestOption(parentMessageId = it) })
            {
              result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(getReplies[0].parentMessageId == parentMessage.id)*/

            session.close()
    }


    /// 1. Creates a members only chat room
    /// 2. Expected fail to join room
    /// 2. Adds self as chat room member
    /// 3. Joins room success
    /// 4. Deletes self as chat room member
    @Test
    fun chatMemberships() = runTest {
        val profile = liveLikeKotlin.profile().currentProfileOnce()
        // Create a chat room
        val chatRoom = suspendCoroutine { cont ->
            chatClient.createChatRoom(
                title = "Test Chat Room",
                visibility = Visibility.members,
                tokenGates = null
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        //TODO: Check that connectChatRoom fails when I am not a member
        val members = suspendCoroutine { cont ->
            chatClient.getChatRoomMemberships(
                ChatRoomRequestOptions(
                    chatRoomId = chatRoom.id,
                    liveLikePagination = LiveLikePagination.FIRST
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(members.isEmpty())
        val addMember = suspendCoroutine { cont ->
            chatClient.addCurrentProfileToChatRoom(chatRoomId = chatRoom.id) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(addMember.profile.id == profile.id)

        //TODO: Test that I can join the chat room as a member

        //Delete Membership
        suspendCoroutine<LiveLikeEmptyResponse> { cont ->
            chatClient.deleteCurrentProfileFromChatRoom(chatRoomId = chatRoom.id) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
    }

    @Test
    fun getChatRoomMembershipOptions() = runTest {
        val profile = liveLikeKotlin.profile().currentProfileOnce()
        // Create a chat room
        val chatRoom = suspendCoroutine { cont ->
            chatClient.createChatRoom(
                title = "Test Chat Room",
                visibility = Visibility.members,
                tokenGates = null
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        suspendCoroutine { cont ->
            chatClient.addCurrentProfileToChatRoom(chatRoomId = chatRoom.id) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        val members = suspendCoroutine { cont ->
            chatClient.getChatRoomMemberships(
                ChatRoomRequestOptions(
                    chatRoomId = chatRoom.id,
                    profileIds = listOf(profile.id),
                    LiveLikePagination.FIRST,
                )
            ) { result, error ->
                result?.let { cont.resume(it) }
                error?.let { cont.resumeWithException(LiveLikeException(it)) }
            }
        }
        assert(members.isNotEmpty())
    }

}

