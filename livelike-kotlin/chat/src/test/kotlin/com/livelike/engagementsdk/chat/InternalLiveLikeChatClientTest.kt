package com.livelike.engagementsdk.chat

import com.livelike.common.*
import com.livelike.engagementsdk.chat
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.Access
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage
import com.livelike.network.NetworkApiClient
import com.livelike.network.NetworkResult
import com.livelike.serialization.processResult
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

@OptIn(ExperimentalCoroutinesApi::class)
class InternalLiveLikeChatClientTest {

    private val testDispatcher = StandardTestDispatcher()

    @MockK
    lateinit var client: NetworkApiClient

    @Before
    fun setUp() {
        // do the setup
        MockKAnnotations.init(this)
        Dispatchers.setMain(testDispatcher)
    }

    private suspend fun setUpSDK(): LiveLikeKotlin {
//        if (this::sdk.isInitialized) return sdk
        coEvery { client.get("http://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4") } returns "application_success.json".getNetworkSuccessResultForFile(
            javaClass.classLoader
        )
        coEvery {
            client.get(
                "https://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4/profile/",
                accessToken = any()
            )
        } returns "profile_fetch_success.json".getNetworkSuccessResultForFile(javaClass.classLoader)
        val sdk =
            initSDK(client, testDispatcher, accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String {
                    return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJibGFzdHJ0IiwiaWQiOiJlZGFiMzNlZS0yODgwLTRkZWUtOTIxZC03ZjRkM2Q3YTQ1MDQiLCJjbGllbnRfaWQiOiJDZHNkTEtMM21TbGx5U3h5TzJValNSUkg0RXFGbVVGYmZTWFRaV1c0IiwiaWF0IjoxNjY5NzE2NjYxLCJhY2Nlc3NfdG9rZW4iOiIwNmMwNjMwZjJhY2UxYzliMmIyNWU5ZDk2NmRlOTcxZjUyODE1ODg0In0.OfxWOLCozRhL3aKzxwQMc2Fn7XmAcpuJhcdEReCwIBo"
                }

                override fun storeAccessToken(accessToken: String?) {

                }
            })
        sdk.user().currentProfileOnce()
        return sdk
    }

    @After
    fun tearDown() {
        testDispatcher.cancel()
    }

    @Test
    fun `test chatroom create success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.post("https://localhost/api/v1/chat-rooms/", accessToken = any(), body = any())
        } answers { "create-chat-room-success.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
        advanceUntilIdle()
        val res = suspendCoroutine { cont ->
            sdk.chat().createChatRoom { result, error ->
                result?.let { cont.resume(it) }
            }
        }
        advanceUntilIdle()
        assert(res.id == "5f642f1c-558d-4b31-919b-9d0bededc65d")
    }

    @Test
    fun `test chatroom details success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/chat-rooms/84f53947-efa5-4c8b-bbd3-ba14b825f7ed/",
                accessToken = null,
                queryParameters = any()
            )
        } answers { "fetch-chat-room-success.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
        println("before test")
        val res = suspendCoroutine { cont ->
            sdk.chat().getChatRoom("84f53947-efa5-4c8b-bbd3-ba14b825f7ed") { result, error ->
                result?.let { cont.resume(it) }
            }
        }
        advanceUntilIdle()
        assert(res.id == "84f53947-efa5-4c8b-bbd3-ba14b825f7ed")
    }

    @Test
    fun `test chatroom update success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.put(
                "https://localhost/api/v1/chat-rooms/84f53947-efa5-4c8b-bbd3-ba14b825f7ed/",
                accessToken = any(),
                body = any()
            )
        } returns "update-chat-room-success.json".getNetworkSuccessResultForFile(javaClass.classLoader)
        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.chat().updateChatRoom(
                "84f53947-efa5-4c8b-bbd3-ba14b825f7ed"
            ) { result, error ->
                result?.let { cont.resume(it) }
            }
        }
        advanceUntilIdle()
        assert(res.title == "new")
    }

    @Test
    fun `test chatroom update failure not found`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.put(//wrong Id given
                "https://localhost/api/v1/chat-rooms/84f53947-efa5-4c8b-bbd3-ba14b825f7ed/",
                accessToken = any(),
                body = any()
            )
        } returns "update-chat-room-failure.json".getNetworkErrorResultForFile(javaClass.classLoader)
        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.chat() //wrong Id given below
                .updateChatRoom(
                    "84f53947-efa5-4c8b-bbd3-ba14b825f7ed"
                ) { result, error ->
                    error?.let { cont.resume(it) }
                }
        }
        advanceUntilIdle()
        assert(res.isNotEmpty() && res == "update-chat-room-failure.json".readFromFile(javaClass.classLoader))
    }


    @Test
    fun `test add current user to chatroom success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/chat-rooms/52d9eada-bfdc-4638-8541-bbb45cb92bef/",
                accessToken = null,
                queryParameters = any()
            )
        } answers { "add_current_user_fetch_chatroom.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()
        coEvery {
            client.post(
                "https://localhost/api/v1/chat-rooms/52d9eada-bfdc-4638-8541-bbb45cb92bef/memberships/",
                accessToken = any(),
                body = any()
            )
        } answers {
            "add_current_user_to_chatroom_success.json".getNetworkSuccessResultForFile(
                javaClass.classLoader
            )
        }
        advanceUntilIdle()
        val res = suspendCoroutine { cont ->
            sdk.chat() //wrong Id given below
                .addCurrentProfileToChatRoom(
                    "52d9eada-bfdc-4638-8541-bbb45cb92bef"
                ) { result, error ->
                    result?.let { cont.resume(it) }
                }
        }
        advanceUntilIdle()
        assert(res.id == "71892bc6-74fc-41d9-8d32-c3b13923f9d7")
    }

    @Test
    fun `test add user to chatroom success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/chat-rooms/52d9eada-bfdc-4638-8541-bbb45cb92bef/",
                accessToken = null,
                queryParameters = any()
            )
        } answers { "add_current_user_fetch_chatroom.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()
        coEvery {
            client.post(
                "https://localhost/api/v1/chat-rooms/52d9eada-bfdc-4638-8541-bbb45cb92bef/memberships/",
                accessToken = any(),
                body = any()
            )
        } answers {
            "add_current_user_to_chatroom_success.json".getNetworkSuccessResultForFile(
                javaClass.classLoader
            )
        }
        advanceUntilIdle()
        val res = suspendCoroutine { cont ->
            sdk.chat() //wrong Id given below
                .addCurrentProfileToChatRoom(
                    "52d9eada-bfdc-4638-8541-bbb45cb92bef"
                ) { result, error ->
                    result?.let { cont.resume(it) }
                }
        }
        advanceUntilIdle()
        assert(res.id == "71892bc6-74fc-41d9-8d32-c3b13923f9d7")
    }

    @Test
    fun `test get members of chatRoom success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/chat-rooms/52d9eada-bfdc-4638-8541-bbb45cb92bef/",
                accessToken = null,
                queryParameters = any()
            )
        } answers { "add_current_user_fetch_chatroom.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()
        coEvery {
            client.get(
                "https://localhost/api/v1/chat-rooms/52d9eada-bfdc-4638-8541-bbb45cb92bef/memberships/",
                accessToken = any()
            )
        } answers {
            "fetch_members_of_chatroom_success.json".getNetworkSuccessResultForFile(
                javaClass.classLoader
            )
        }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.chat() //wrong Id given below
                .getMembersOfChatRoom(
                    "52d9eada-bfdc-4638-8541-bbb45cb92bef",
                    liveLikePagination = LiveLikePagination.FIRST
                ) { result, error ->
                    result?.let { cont.resume(it) }
                }
        }
        advanceUntilIdle()
        assert(res[0].nickname == "Dark Legend")
    }

    @Test
    fun `test delete current user from chatRoom success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/chat-rooms/52d9eada-bfdc-4638-8541-bbb45cb92bef/",
                accessToken = null,
                queryParameters = any()
            )
        } answers { "add_current_user_fetch_chatroom.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()
        coEvery {
            client.delete(
                "https://localhost/api/v1/chat-rooms/52d9eada-bfdc-4638-8541-bbb45cb92bef/memberships/",
                accessToken = any()
            )
        } answers { "delete_membership.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.chat() //wrong Id given below
                .deleteCurrentProfileFromChatRoom(
                    "52d9eada-bfdc-4638-8541-bbb45cb92bef"
                ) { result, error ->
                    result?.let { cont.resume(it) }
                }
        }
        advanceUntilIdle()
        assert(res.toString().contains("LiveLikeEmptyResponse"))
    }


    @Test
    fun `test send chatRoom invite to user success`() = runTest {
        val sdk = setUpSDK()

        advanceUntilIdle()
        coEvery {
            client.post(
                "https://localhost/api/v1/chat-room-invitations/",
                accessToken = any(),
                body = any()
            )
        } answers { "create_chatroom_invite.json".getNetworkSuccessResultForFile(javaClass.classLoader) }

        advanceUntilIdle()

        val res = suspendCoroutine { cont ->
            sdk.chat() //wrong Id given below
                .sendChatRoomInviteToProfile(
                    "52d9eada-bfdc-4638-8541-bbb45cb92bef",
                    "cb3bd893-00eb-486b-8f40-bb198aa1f595"
                ) { result, error ->
                    result?.let { cont.resume(it) }
                }
        }
        advanceUntilIdle()
        assert(res.invitedProfileId == "cb3bd893-00eb-486b-8f40-bb198aa1f595")
    }

    @Test
    fun `test check if gating passed success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/chatroom-token-gate-access/?chat_room_id=72e6a9c1-ebd4-42e8-8a51-cb4749d406b3" +
                        "&wallet_address=0xfa1e58c3c5349ca8E2C1E0975D1f3B49413e0171",
                accessToken = null,
                queryParameters = any()
            )
        } returns "check_token_gating_status.json".getNetworkSuccessResultForFile(javaClass.classLoader)
        coEvery {
            client.get(
                "https://localhost/api/v1/chat-rooms/72e6a9c1-ebd4-42e8-8a51-cb4749d406b3/",
                accessToken = null,
                queryParameters = any()
            )
        } answers { "fetch-chat-room-success.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
        advanceUntilIdle()
        val res = suspendCoroutine { cont ->
            sdk.chat()
                .getTokenGatedChatRoomAccessDetails(
                    "0xfa1e58c3c5349ca8E2C1E0975D1f3B49413e0171",
                    "72e6a9c1-ebd4-42e8-8a51-cb4749d406b3"
                ) { result, error ->
                    result?.let { cont.resume(it) }
                }
        }
        advanceUntilIdle()
        assert(res.access == Access.disallowed)
    }

    @Test
    fun `test fetch smartcontract names success`() = runTest {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/saved-contract-addresses/?client_id=32JZTq5Owh1ozORSJF9Mdxi4GJSRLNHgJWVtK0gC",
                accessToken = any()
            )
        } returns "get-smartcontract-names.json".getNetworkSuccessResultForFile(javaClass.classLoader)
        advanceUntilIdle()
        val res = suspendCoroutine { cont ->
            sdk.chat()
                .getSmartContracts(
                    LiveLikePagination.FIRST
                ) { result, error ->
                    result?.let { cont.resume(it) }
                }
        }
        advanceUntilIdle()
        assert(res[0].contractName == "LiveLike")


    }

    @Test
    fun `test same client instance from sdk`() = runTest {
        val sdk = setUpSDK()
        assert(sdk.chat() == sdk.chat())
    }

    @Test
    fun `test different client instance from different sdk instance`() = runTest {
        val sdk1 = setUpSDK()
        val sdk2 = setUpSDK()
        assert(sdk1.chat() != sdk2.chat())
        assert(sdk1.chat() == sdk1.chat())
        assert(sdk2.chat() == sdk2.chat())
    }

    @Test
    fun testParsingJsonToLiveLikeChatMessage() = runTest {
        val data =
            "{\"id\":\"b71ebeba-d2f8-4530-acac-4d36be057f59\",\"chat_room_id\":\"5b2d45c2-8810-43d1-8e20-33f599b7fd02\",\"sender_id\":\"3ffc0431-1ac1-441b-9c45-a28c15345f13\",\"sender_nickname\":\"Shivam Verma\",\"sender_image_url\":null,\"badge_image_url\":null,\"sender_profile_url\":\"https://cf-blast.livelikecdn.com/api/v1/profiles/3ffc0431-1ac1-441b-9c45-a28c15345f13/\",\"created_at\":\"2023-11-17T07:58:59.058628+00:00\",\"message_event\":\"custom-message-created\",\"custom_data\":\"Test Custom Message\",\"program_date_time\":null,\"pubnub_timetoken\":17002079391604167}"
        val message = NetworkResult.Success(data).processResult<LiveLikeChatMessage>()
        println("MM>>${message.customData}")
        assert(message.customData != null)
    }
}