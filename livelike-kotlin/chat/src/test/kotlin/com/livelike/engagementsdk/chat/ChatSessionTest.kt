package com.livelike.engagementsdk.chat

import com.livelike.common.AccessTokenDelegate
import com.livelike.common.LiveLikeKotlin
import com.livelike.common.getNetworkSuccessResultForFile
import com.livelike.common.initSDK
import com.livelike.common.profile
import com.livelike.engagementsdk.EpochTime
import com.livelike.engagementsdk.createChatSession
import com.livelike.network.NetworkApiClient
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

@OptIn(ExperimentalCoroutinesApi::class)
class ChatSessionTest {

    private val testDispatcher = StandardTestDispatcher()

    @MockK
    lateinit var client: NetworkApiClient

    @Before
    fun setUp() {
        // do the setup
        MockKAnnotations.init(this)
        Dispatchers.setMain(testDispatcher)
    }

    private suspend fun setUpSDK(): LiveLikeKotlin {
//        if (this::sdk.isInitialized) return sdk
        coEvery { client.get("http://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4") } returns "application_success.json".getNetworkSuccessResultForFile(
            javaClass.classLoader
        )
        coEvery {
            client.get(
                "https://localhost/api/v1/applications/CdsdLKL3mSllySxyO2UjSRRH4EqFmUFbfSXTZWW4/profile/",
                accessToken = any()
            )
        } returns "profile_fetch_success.json".getNetworkSuccessResultForFile(javaClass.classLoader)
        val sdk =
            initSDK(client, testDispatcher, accessTokenDelegate = object : AccessTokenDelegate {
                override fun getAccessToken(): String {
                    return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJibGFzdHJ0IiwiaWQiOiJlZGFiMzNlZS0yODgwLTRkZWUtOTIxZC03ZjRkM2Q3YTQ1MDQiLCJjbGllbnRfaWQiOiJDZHNkTEtMM21TbGx5U3h5TzJValNSUkg0RXFGbVVGYmZTWFRaV1c0IiwiaWF0IjoxNjY5NzE2NjYxLCJhY2Nlc3NfdG9rZW4iOiIwNmMwNjMwZjJhY2UxYzliMmIyNWU5ZDk2NmRlOTcxZjUyODE1ODg0In0.OfxWOLCozRhL3aKzxwQMc2Fn7XmAcpuJhcdEReCwIBo"
                }

                override fun storeAccessToken(accessToken: String?) {

                }
            })
        sdk.profile().currentProfileOnce()
        return sdk
    }

    @After
    fun tearDown() {
        testDispatcher.cancel()
    }

    @Test
    fun `test chat session connected to chatRoom`() = runTest(dispatchTimeoutMs = 2000) {
        val sdk = setUpSDK()
        coEvery {
            client.get(
                "https://localhost/api/v1/blocked-profile-ids/",
                accessToken = any(),
                queryParameters = any()
            )
        } answers { "block-profile-ids-success.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
        coEvery {
            client.get(
                "https://localhost/api/v1/chat-rooms/84f53947-efa5-4c8b-bbd3-ba14b825f7ed/",
                accessToken = null,
                queryParameters = any()
            )
        } answers { "fetch-chat-room-success.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
        coEvery {
            client.get(
                "https://localhost/api/v1/reaction-spaces/1ddee057-8a3d-44b2-81e7-2b021de488eb/",
                accessToken = any(),
            )
        } answers { "get_reaction_space.json".getNetworkSuccessResultForFile(javaClass.classLoader) }
        coEvery {
            client.get(
                "https://localhost/api/v1/chatroom-messages/",
                accessToken = any(),
                queryParameters = any()
            )
        } answers {
            "fetch_chat_messages_history_success.json".getNetworkSuccessResultForFile(
                javaClass.classLoader
            )
        }
        coEvery {
            client.get(
                "https://localhost/api/v1/chatroom-events/",
                accessToken = any(),
                queryParameters = any()
            )
        } answers {
            "chat_events_success.json".getNetworkSuccessResultForFile(
                javaClass.classLoader
            )
        }

        val session = sdk.createChatSession(
            {
                EpochTime(0)
            },
            sessionDispatcher = testDispatcher,
            synchronizationDispatcher = testDispatcher,
            mainDispatcher = testDispatcher,
        )
        println("connect to chatroom")
        val isConnectedToChatRoom = suspendCoroutine { cont ->

            session.connectToChatRoom(
                "84f53947-efa5-4c8b-bbd3-ba14b825f7ed",callback = { result, error ->
                    println("result>$result")
                    result?.let { cont.resume("84f53947-efa5-4c8b-bbd3-ba14b825f7ed") }
                    error?.let { cont.resumeWithException(Exception(it)) }
                }
            )
        }
        assert(isConnectedToChatRoom == "84f53947-efa5-4c8b-bbd3-ba14b825f7ed")
        session.close()
    }
}