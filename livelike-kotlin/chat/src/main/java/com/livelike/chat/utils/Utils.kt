package com.livelike.chat.utils

private val encodeMap = hashMapOf(
    "&amp;" to "&",
    "&lt;" to "<",
    "&gt;" to ">",
    "&quot;" to "\"",
    "&#47;" to "/"
)

fun String.decodeTextMessage(): String {
    return encodeMap.entries.fold(this) { acc, (key, value) -> acc.replace(key, value) }
}