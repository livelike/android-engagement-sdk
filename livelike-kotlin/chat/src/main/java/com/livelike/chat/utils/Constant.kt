package com.livelike.chat.utils

internal const val CHAT_PROVIDER = "pubnub"

const val EVENT_NEW_MESSAGE = "new-message"
const val EVENT_MESSAGE_DELETED = "deletion"
const val EVENT_LOADING_COMPLETE = "loading-complete"
const val EVENT_LOADING_STARTED = "loading-started"
const val EVENT_MESSAGE_CANNOT_SEND =
    "message_cannot_send" // case 0 : occurs when user is muted inside a room and sends a message

internal const val INTEGRATOR_USED_CHATROOM_DELEGATE_KEY = "IntegratorUsedChatRoomDelegate"
internal const val TEMPLATE_PROFILE_ID = "{profile_id}"
internal const val TEMPLATE_STATUS = "{status}"
internal const val TEMPLATE_INVITED_BY_ID = "{invited_by_id}"
internal const val TEMPLATE_WALLET_ADDRESS = "{wallet_address}"


internal const val NO_REACTIONS_AVAILABLE = "No List of Reactions in Reaction Pack"
internal const val CUSTOM_MESSAGE_URL_IS_NULL = "Custom Message Url is Null"

internal const val SESSION_CLOSED = "Session is closed"
internal const val CHAT_ROOM_ID_EMPTY = "ChatRoom Id cannot be Empty"
internal const val CHAT_ROOM_ID = "chat_room_id"
internal const val PAGE_SIZE = "page_size"
internal const val PARENT_MESSAGE_ID = "parent_message_id"
internal const val WALLET_ADDRESS_EMPTY = "Wallet Address cannot be Empty"
internal const val TOKEN_GATING_ACCESS_URL_NOT_FOUND = "Token gating access url not found."
internal const val CHATROOM_NOT_TOKEN_GATED = "ChatRoom is not Token Gated."
internal const val ALREADY_CONNECTED_TO_CHATROOM_ERROR = "Already Connected to ChatRoom"
internal const val CHATROOM_NOT_FOUND = "ChatRoom Not Found"
internal const val CHATSESSION_NOT_IDLE= "Chat Session not Idle"
internal const val MESSAGE_CANNOT_BE_EMPTY = "Message cannot be empty"
internal  const val STICKER_PACK_REPOSITORY_NULL = "StickerPack repository is null"
internal const val CHAT_REACTION_REPOSITORY_NULL = "Chat Reaction repository is null"
internal const val CHAT_ROOM_DETAILS_NULL = "Chat Room Details is Null"
internal const val NO_REACTION = "No Reaction found"
internal const val MESSAGE_NOT_FOUND = "Message not found"
internal const val MUTED_STATUS_URL_NOT_FOUND = "Muted Status Url Not found"
internal const val NO_ACTIVE_CHATROOM = "No Active Chat Room"
internal const val CHATROOM_MESSAGE_URL_NOT_FOUND = "Chat Room Message Url not found"
internal const val CHATRROM_MESSAGE_COUNT_URL_NOTFOUND ="Chat Room MessageCount Url not found"
internal const val CHAT_SESSION_NOT_IDLE ="Chat Session is not Idle"