package com.livelike.chat.repository

import com.livelike.chat.utils.NO_REACTIONS_AVAILABLE
import com.livelike.engagementsdk.chat.chatreaction.Reaction
import com.livelike.engagementsdk.chat.chatreaction.ReactionPack
import com.livelike.network.NetworkApiClient
import com.livelike.serialization.processResult
import com.livelike.utils.PaginationResponse

class ChatReactionRepository(
    private val remoteUrl: String,
    private val networkApiClient: NetworkApiClient
) {

    private var reactionList: List<Reaction>? = null
    private var reactionMap: Map<String, Reaction>? = null

    suspend fun getReactions(): List<Reaction> {
        val result =
            networkApiClient.get(remoteUrl).processResult<PaginationResponse<ReactionPack>>()
        return if (result.results.isNotEmpty()) {
            reactionList = result.results[0].emojis
            initReactionMap(reactionList)
            reactionList ?: throw Exception(NO_REACTIONS_AVAILABLE)
        } else {
            emptyList()
        }
    }

    private fun initReactionMap(reactionList: List<Reaction>?) {
        reactionList?.let { list ->
            reactionMap = list.associateBy { it.id }
        }
    }

}
