package com.livelike.engagementsdk.publicapis

import com.google.gson.annotations.SerializedName

data class ChatProfileMuteStatus(
    @SerializedName("is_muted") val isMuted: Boolean
)
typealias  ChatUserMuteStatus = ChatProfileMuteStatus


