package com.livelike.engagementsdk

import com.livelike.engagementsdk.chat.ChatRoomInfo

/**
 * Interface for receiving updates related to chat rooms.
 */
interface ChatRoomListener {
    /**
     * Called when a chat room is updated.
     *
     * @param chatRoom The updated information about the chat room.
     */
    fun onChatRoomUpdate(chatRoom: ChatRoomInfo)
}