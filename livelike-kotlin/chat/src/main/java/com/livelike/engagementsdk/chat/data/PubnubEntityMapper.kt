package com.livelike.engagementsdk.chat.data

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.livelike.common.model.PubnubChatEventType
import com.livelike.engagementsdk.chat.ChatMessageReaction
import com.livelike.engagementsdk.chat.data.remote.PubnubChatMessage
import com.livelike.engagementsdk.chat.data.remote.PubnubQuoteChatMessage
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage
import com.livelike.serialization.gson

internal fun LiveLikeChatMessage.toPubnubChatMessage(programDateTime: String?): PubnubChatMessage {
    return PubnubChatMessage(
        id,
        message,
        senderId ?: "",
        profilePic,
        nickname ?: "",
        programDateTime,
        imageUrl = imageUrl,
        imageWidth = imageWidth,
        imageHeight = imageHeight,
        customData = customData,
        messageMetadata = toJsonObject(messageMetadata),
        quoteMessage = quoteMessage?.toPubnubQuoteChatMessage(null),
        quoteMessageId = quoteMessageID,
        clientMessageId = clientMessageId,
        createdAt = createdAt,
        chatRoomId = chatRoomId,
        filteredMessage = filteredMessage,
        contentFilter = contentFilter,
        parentMessageId = parentMessageId,
        repliesCount = repliesCount,
        repliesUrl = repliesUrl
    )
}

internal fun LiveLikeChatMessage.toPubnubQuoteChatMessage(programDateTime: String?): PubnubQuoteChatMessage {
    return PubnubQuoteChatMessage(
        id,
        message,
        senderId ?: "",
        profilePic,
        nickname ?: "",
        programDateTime,
        imageUrl = imageUrl,
        imageWidth = imageWidth,
        imageHeight = imageHeight,
        customData = customData,
        messageMetadata = toJsonObject(messageMetadata),
        clientMessageId = clientMessageId,
        createdAt = createdAt,
        chatRoomId = chatRoomId,
        filteredMessage = filteredMessage,
        contentFilter = contentFilter,
        parentMessageId = parentMessageId,
        repliesCount = repliesCount,
        repliesUrl = repliesUrl
    )
}

internal fun PubnubQuoteChatMessage.toChatMessage(
    timetoken: Long,
    emojiCountMap: Map<String, List<ChatMessageReaction>>,
    event: PubnubChatEventType
): LiveLikeChatMessage {
    return LiveLikeChatMessage(
        message
    ).apply {
        this.messageEvent = event
        this.customData = this@toChatMessage.customData
        this.senderId = this@toChatMessage.senderId
        this.nickname = this@toChatMessage.senderNickname
        this.profilePic = this@toChatMessage.senderImageUrl
        this.id = this@toChatMessage.messageId
        this.filteredMessage = this@toChatMessage.filteredMessage
        this.contentFilter = this@toChatMessage.contentFilter
        this.profileReactionListForEmojiMap = emojiCountMap
        this.imageUrl = this@toChatMessage.imageUrl
        this.imageWidth = this@toChatMessage.imageWidth
        this.imageHeight = this@toChatMessage.imageHeight
        this.timetoken = timetoken
        this.createdAt = this@toChatMessage.createdAt
        this.clientMessageId = this@toChatMessage.clientMessageId
        this.chatRoomId = this@toChatMessage.chatRoomId
        this.messageMetadata = toMap(this@toChatMessage.messageMetadata)
        this.parentMessageId = this@toChatMessage.parentMessageId
        this.repliesUrl = this@toChatMessage.repliesUrl
        this.repliesCount = this@toChatMessage.repliesCount

    }
}

internal fun PubnubChatMessage.toChatMessage(
    timetoken: Long,
    emojiCountMap: Map<String, List<ChatMessageReaction>>,
    event: PubnubChatEventType
): LiveLikeChatMessage {
    return LiveLikeChatMessage(this.message).apply {
        this.messageEvent = event
        this.customData = this@toChatMessage.customData
        this.senderId = this@toChatMessage.senderId
        this.nickname = this@toChatMessage.senderNickname
        this.profilePic = this@toChatMessage.senderImageUrl
        this.id = this@toChatMessage.messageId
        this.filteredMessage = this@toChatMessage.filteredMessage
        this.contentFilter = this@toChatMessage.contentFilter
        this.profileReactionListForEmojiMap = emojiCountMap
        this.imageUrl = this@toChatMessage.imageUrl
        this.imageWidth = this@toChatMessage.imageWidth
        this.imageHeight = this@toChatMessage.imageHeight
        this.timetoken = timetoken
        this.createdAt = this@toChatMessage.createdAt
        this.quoteMessage = this@toChatMessage.quoteMessage?.toChatMessage(
            0L,
            emptyMap(),
            PubnubChatEventType.MESSAGE_CREATED
        )
        this.quoteMessageID = this@toChatMessage.quoteMessageId
        this.clientMessageId = this@toChatMessage.clientMessageId
        this.chatRoomId = this@toChatMessage.chatRoomId
        this.messageMetadata = toMap(this@toChatMessage.messageMetadata)
        this.parentMessageId = this@toChatMessage.parentMessageId
        this.repliesUrl = this@toChatMessage.repliesUrl
        this.repliesCount = this@toChatMessage.repliesCount
    }
}

fun toJsonObject(messageMetadata: Map<String, Any?>?): JsonElement? {
    messageMetadata?.let {
        return gson.fromJson(gson.toJson(it), JsonObject::class.java)
    }
    return null
}

fun toMap(messageMetadata: JsonElement?): Map<String, Any?>? {
    return gson.fromJson(
        messageMetadata, object : TypeToken<HashMap<String?, Any?>?>() {}.type
    )
}