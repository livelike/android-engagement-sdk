package com.livelike.engagementsdk.publicapis

import com.google.gson.annotations.SerializedName


data class TokenGatingStatus(

    @SerializedName("chat_room_id") val chatRoomId: String? = null,
    @SerializedName("access") val access: Access = Access.disallowed,
    @SerializedName("wallet_details") val walletDetails: WalletDetails? = WalletDetails(),
    @SerializedName("details") val details: String? = null
)

data class ContractBalances(

    @SerializedName("contract_address") val contractAddress: String? = null,
    @SerializedName("network_type") val networkType: String? = null,
    @SerializedName("balance") val balance: Int? = null,
    @SerializedName("token_type") val tokenType: String? = null,
    @SerializedName("metadata") val metadata: Metadata? = null,
    @SerializedName("failed_attributes") val failedAttributes : List<String> = emptyList(),
    @SerializedName("gate_passed") val gatePassed : Boolean = false

)

data class WalletDetails(

    @SerializedName("wallet_address") val walletAddress: String? = null,
    @SerializedName("contract_balances") val contractBalances: List<ContractBalances> = emptyList()
)

data class Attributes(

    @SerializedName("trait_type") val traitType: String? = null,
    @SerializedName("value") val value: String? = null

)

data class Metadata(

    @SerializedName("attributes") val attributes: List<Attributes> = emptyList(),
    @SerializedName("nft_with_attributes_count") val nftWithAttributesCount: Int? = null

)

enum class Access {
    allowed,
    disallowed
}