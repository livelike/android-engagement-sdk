package com.livelike.engagementsdk.chat.data.remote

import com.google.gson.annotations.SerializedName
import com.livelike.engagementsdk.LiveLikeProfile


data class ChatRoomMembership(
    @field:SerializedName("profile")
    val profile: LiveLikeProfile,
    @field:SerializedName("created_at")
    val createdAt: String,
    @field:SerializedName("id")
    val id: String,
    @field:SerializedName("url")
    val url: String
)
