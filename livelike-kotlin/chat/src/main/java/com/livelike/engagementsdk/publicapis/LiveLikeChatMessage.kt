package com.livelike.engagementsdk.publicapis

import com.google.gson.annotations.SerializedName
import com.livelike.common.model.PubnubChatEventType
import com.livelike.engagementsdk.chat.ChatMessageReaction
import com.livelike.utils.parseISODateTime


// this model is not changed since 1.2 release in hurry, we need to fix it may require to bump to major version.
data class LiveLikeChatMessage(
    var message: String?,
    /**
     * unique identifier
     */
    var id: String? = null,
    var clientMessageId: String? = null,

    @SerializedName("sender_nickname")
    var nickname: String? = null,

    @SerializedName("sender_id")
    var senderId: String? = null,

    @SerializedName("message_event")
    var messageEvent: PubnubChatEventType = PubnubChatEventType.MESSAGE_CREATED,

    @SerializedName("parent_message_id")
    var parentMessageId:String? = null,

    var imageWidth: Int? = null,
    var imageHeight: Int? = null,
    var imageUrl: String? = null
) {

    var timestamp: String? = null

    /**
     * message meta data to send custom messages
     */
    var messageMetadata: Map<String, Any?>? = null
    var isDeleted: Boolean = false
    var quoteMessage: LiveLikeChatMessage? = null
    var reactions: Map<String, List<LiveLikeChatReaction>>? = null
    var filteredMessage: String? = null
    var contentFilter: List<String>? = null

    @SerializedName("sender_image_url")
    var profilePic: String? = null
    /**
     * data for custom message
     */
    @SerializedName("custom_data")
    var customData: String? = null

    @SerializedName("created_at")
    var createdAt: String? = null

    @SerializedName("replies_url")
    var repliesUrl: String? = null

    @SerializedName("replies_count")
    var repliesCount: Int = 0

    @SerializedName("badge_image_url")
    private var badgeUrlImage: String? = null

    @SerializedName("chat_room_id")
    var chatRoomId: String? = null

    /**
     * type of the message, the enum class defines the types definition
     */
    val type: ChatMessageType?
        get() = messageEvent.toChatMessageType()

    var timeStamp: String? = null
    var isFromMe: Boolean = false
//    var myChatMessageReactions: ArrayList<ChatMessageReaction> = arrayListOf()
    var profileReactionListForEmojiMap: Map<String, List<ChatMessageReaction>> = mutableMapOf()

    // time of the message
    var timetoken: Long = 0L
    var isBlocked: Boolean = false
    internal var quoteMessageID: String? = null


    fun getUnixTimeStamp(): Long? {
        if (timetoken == 0L) {
            if (createdAt != null) {
                return createdAt?.parseISODateTime()?.toInstant()?.toEpochMilli()
            }
            return null
        }
        return try {
            timetoken / 10000
        } catch (_: ArithmeticException) {
            null
        }
    }

    private val isMessageFiltered
        get() = contentFilter?.isNotEmpty() ?: false

    fun convertedMessage(userId: String?): String {
        return if (isMessageFiltered && userId != senderId) {
            filteredMessage ?: message ?: ""
        } else {
            message ?: ""
        }
    }

    // Update the user_id to profile_id as required from backend
    internal fun toReportMessageJson(): String {
        return """{
                    "chat_room_id": "$chatRoomId",
                    "profile_id": "$senderId",
                    "nickname": "$nickname",
                    "message_id": "$id",
                    "message": "${message?.trim()}",
                    "pubnub_timetoken":$timetoken
                }
        """.trimIndent()
    }

    override fun toString(): String {
        return "message:$message, id:$id, imageUrl:$imageUrl, clientMessageId:$clientMessageId, timestamp:$timestamp, timeStamp:$timeStamp, timetoken:$timetoken, ${
            when (profileReactionListForEmojiMap.isNotEmpty()) {
                true -> profileReactionListForEmojiMap.entries.map { "Key:${it.key},Value:${it.value}" }
                    .reduce { s1, s2 -> "$s1\n$s2" }
                else -> ""
            }
        }"
    }

    fun copy(): LiveLikeChatMessage {
        return LiveLikeChatMessage(message).apply {
            this.message = this@LiveLikeChatMessage.message
            this.imageUrl = this@LiveLikeChatMessage.imageUrl
            this.imageWidth = this@LiveLikeChatMessage.imageWidth
            this.imageHeight = this@LiveLikeChatMessage.imageHeight
            this.quoteMessageID = this@LiveLikeChatMessage.quoteMessageID
            this.quoteMessage = this@LiveLikeChatMessage.quoteMessage
            this.messageEvent = this@LiveLikeChatMessage.messageEvent
            this.contentFilter = this@LiveLikeChatMessage.contentFilter
            this.filteredMessage = this@LiveLikeChatMessage.filteredMessage
            this.messageMetadata = this@LiveLikeChatMessage.messageMetadata
            this.chatRoomId = this@LiveLikeChatMessage.chatRoomId
            this.clientMessageId = this@LiveLikeChatMessage.clientMessageId
            this.timeStamp = this@LiveLikeChatMessage.timeStamp
            this.isFromMe = this@LiveLikeChatMessage.isFromMe
            this.profilePic = this@LiveLikeChatMessage.profilePic
            this.nickname = this@LiveLikeChatMessage.nickname
            this.senderId = this@LiveLikeChatMessage.senderId
            this.customData = this@LiveLikeChatMessage.customData
//            this.channel = this@LiveLikeChatMessage.channel
            this.createdAt = this@LiveLikeChatMessage.createdAt
            this.timetoken = this@LiveLikeChatMessage.timetoken
//            this.myChatMessageReactions = this@LiveLikeChatMessage.myChatMessageReactions
            this.profileReactionListForEmojiMap = this@LiveLikeChatMessage.profileReactionListForEmojiMap
            this.id = this@LiveLikeChatMessage.id
            this.timestamp = this@LiveLikeChatMessage.timestamp
            this.badgeUrlImage = this@LiveLikeChatMessage.badgeUrlImage
            this.isBlocked = this@LiveLikeChatMessage.isBlocked
            this.isDeleted = this@LiveLikeChatMessage.isDeleted
            //this.isMessageFiltered = this@LiveLikeChatMessage.isMessageFiltered
            this.reactions = this@LiveLikeChatMessage.reactions
            this.parentMessageId = this@LiveLikeChatMessage.parentMessageId
            this.repliesUrl = this@LiveLikeChatMessage.repliesUrl
            this.repliesCount = this@LiveLikeChatMessage.repliesCount
        }
    }
}

data class LiveLikeChatReaction(
    val uuid: String? = null,
    val actionTimeToken: String? = null
)

enum class ChatMessageType(val key: String) {
    MESSAGE_CREATED("message-created"),
    MESSAGE_DELETED("message-deleted"),
    IMAGE_CREATED("image-created"),
    IMAGE_DELETED("image-deleted"),
    CUSTOM_MESSAGE_CREATED("custom-message-created")
}

fun PubnubChatEventType.toChatMessageType(): ChatMessageType? {
    return when (this) {
        PubnubChatEventType.MESSAGE_DELETED -> ChatMessageType.MESSAGE_DELETED
        PubnubChatEventType.MESSAGE_CREATED -> ChatMessageType.MESSAGE_CREATED
        PubnubChatEventType.IMAGE_DELETED -> ChatMessageType.IMAGE_DELETED
        PubnubChatEventType.IMAGE_CREATED -> ChatMessageType.IMAGE_CREATED
        PubnubChatEventType.CUSTOM_MESSAGE_CREATED -> ChatMessageType.CUSTOM_MESSAGE_CREATED
        else -> null
    }
}

data class ChatRoomAdd(
    val id: String,
    @SerializedName("chat_room_id")
    val chatRoomID: String,
    @SerializedName("chat_room_title")
    val chatRoomTitle: String,
    @SerializedName("sender_id")
    val senderID: String,
    @SerializedName("sender_nickname")
    val senderNickname: String,
    @SerializedName("sender_image_url")
    val senderImageURL: String? = null,
    @SerializedName("badge_image_url")
    val badgeImageURL: String? = null,
    @SerializedName("sender_profile_url")
    val senderProfileURL: String,
    @SerializedName("created_at")
    val createdAt: String
)

