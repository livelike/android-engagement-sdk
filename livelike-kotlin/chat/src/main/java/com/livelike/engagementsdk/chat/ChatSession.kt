package com.livelike.engagementsdk.chat

import com.livelike.chat.repository.ChatReactionRepository
import com.livelike.chat.utils.ALREADY_CONNECTED_TO_CHATROOM_ERROR
import com.livelike.chat.utils.CHATROOM_NOT_FOUND
import com.livelike.chat.utils.CHATSESSION_NOT_IDLE
import com.livelike.chat.utils.CHAT_REACTION_REPOSITORY_NULL
import com.livelike.chat.utils.CHAT_ROOM_DETAILS_NULL
import com.livelike.chat.utils.CHAT_ROOM_ID
import com.livelike.chat.utils.CHAT_ROOM_ID_EMPTY
import com.livelike.chat.utils.CHAT_SESSION_NOT_IDLE
import com.livelike.chat.utils.CUSTOM_MESSAGE_URL_IS_NULL
import com.livelike.chat.utils.MESSAGE_CANNOT_BE_EMPTY
import com.livelike.chat.utils.MESSAGE_NOT_FOUND
import com.livelike.chat.utils.NO_ACTIVE_CHATROOM
import com.livelike.chat.utils.NO_REACTION
import com.livelike.chat.utils.PAGE_SIZE
import com.livelike.chat.utils.PARENT_MESSAGE_ID
import com.livelike.chat.utils.Queue
import com.livelike.chat.utils.SESSION_CLOSED
import com.livelike.chat.utils.STICKER_PACK_REPOSITORY_NULL
import com.livelike.chat.utils.decodeTextMessage
import com.livelike.common.DataStoreDelegate
import com.livelike.common.ErrorDetails
import com.livelike.common.ErrorSeverity
import com.livelike.common.LiveLikeCallbackWithSeverity
import com.livelike.common.clients.LiveLikeProfileClient
import com.livelike.common.model.PubnubChatEventType
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.model.toPubnubChatEventType
import com.livelike.common.utils.BaseSession
import com.livelike.engagementsdk.AnalyticsService
import com.livelike.engagementsdk.ChatRoomListener
import com.livelike.engagementsdk.EpochTime
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.MessageListener
import com.livelike.engagementsdk.MessageWithReactionListener
import com.livelike.engagementsdk.chat.chatreaction.Reaction
import com.livelike.engagementsdk.chat.data.remote.GetChatMessageRepliesRequestOption
import com.livelike.engagementsdk.chat.data.remote.ChatRoom
import com.livelike.engagementsdk.chat.data.remote.ImageResource
import com.livelike.engagementsdk.chat.data.remote.PinMessageInfo
import com.livelike.engagementsdk.chat.data.remote.PubnubChatMessage
import com.livelike.engagementsdk.chat.data.remote.PubnubChatReaction
import com.livelike.engagementsdk.chat.data.remote.PubnubPinMessageInfo
import com.livelike.engagementsdk.chat.data.remote.toPinMessageInfo
import com.livelike.engagementsdk.chat.data.toChatMessage
import com.livelike.engagementsdk.chat.data.toPubnubChatMessage
import com.livelike.engagementsdk.chat.stickerKeyboard.StickerPack
import com.livelike.engagementsdk.chat.stickerKeyboard.StickerPackRepository
import com.livelike.engagementsdk.findStickers
import com.livelike.engagementsdk.publicapis.BlockedInfo
import com.livelike.engagementsdk.publicapis.ChatRoomAdd
import com.livelike.engagementsdk.publicapis.ChatRoomDelegate
import com.livelike.engagementsdk.publicapis.ChatRoomInvitation
import com.livelike.engagementsdk.publicapis.ErrorDelegate
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.engagementsdk.reaction.LiveLikeReactionSession
import com.livelike.engagementsdk.reaction.UserReactionDelegate
import com.livelike.engagementsdk.reaction.models.UserReaction
import com.livelike.engagementsdk.toUTCTime
import com.livelike.network.NetworkApiClient
import com.livelike.realtime.RealTimeMessagingClient
import com.livelike.realtime.RealTimeMessagingClientConfig
import com.livelike.realtime.internal.LiveLikeMessagingClient
import com.livelike.realtime.internal.LiveLikeMessagingClientParam
import com.livelike.realtime.internal.syncTo
import com.livelike.serialization.gson
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import com.livelike.utils.LiveLikeException
import com.livelike.utils.Once
import com.livelike.utils.PaginationResponse
import com.livelike.utils.isoDateTimeFormat
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime
import java.io.InputStream
import java.util.Calendar
import java.util.Collections
import java.util.UUID
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class ChatSession(
    configurationOnce: Once<SdkConfiguration>,
    val currentProfileOnce: Once<LiveLikeProfile>,
    val isPublicRoom: Boolean = true,
    val chatEnforceUniqueMessageCallback: Boolean = false,
    val analyticsService: AnalyticsService,
    var errorDelegate: ErrorDelegate? = null,
    private val liveLikeChatClient: LiveLikeChatClient,
    val liveLikeProfileClient: LiveLikeProfileClient,
    val networkApiClient: NetworkApiClient,
    private val includeFilteredChatMessages: Boolean,
    val dataStoreDelegate: DataStoreDelegate,
    val quoteChatMessageDeletedMessage: String,
    val chatMessageDeletedMessage: String,
    private val isPlatformLocalContentImageUrl: (String) -> Boolean,
    private val getInputStreamForImageUrl: (String) -> InputStream?,
    private val getSizeOfImage: (ByteArray) -> ImageSize,
    val preLoadImage: suspend (String) -> Boolean,
    sessionDispatcher: CoroutineDispatcher,
    uiDispatcher: CoroutineDispatcher,
    private val synchronizationDispatcher: CoroutineDispatcher,
    private val getReactionSession: (String) -> LiveLikeReactionSession,
    private val currentPlayheadTime: () -> EpochTime
) : BaseSession(
    configurationOnce, currentProfileOnce, sessionDispatcher, uiDispatcher, errorDelegate
), LiveLikeChatSession {

    override fun getPlayheadTime(): EpochTime {
        return currentPlayheadTime()
    }

    //Flow used for updating url in viewModel
    private val _updateUrlFlow = MutableStateFlow<Pair<ChatRoom, Boolean>?>(null)
    val updateUrlFlow: StateFlow<Pair<ChatRoom, Boolean>?> = _updateUrlFlow.asStateFlow()

    val flushMessagesFlow = MutableStateFlow<String?>(null)

    private val _chatLoadedFlow = MutableStateFlow<String?>(null)
    val chatLoadedFlow = _chatLoadedFlow.asStateFlow()

    private var isClosed = false
    private var stickerPackRepository: StickerPackRepository? = null
    private var chatReactionRepository: ChatReactionRepository? = null

    override var getCurrentChatRoom: () -> String = { currentChatRoomId ?: "" }

    private var currentChatRoomId: String? = null
    private var isReceivingRealtimeUpdates = false
    private var realTimeChatMessagingClient: RealTimeMessagingClient? = null

    private val _chatSessionIdleFlow =
        MutableSharedFlow<Boolean>(replay = 1, onBufferOverflow = BufferOverflow.DROP_OLDEST)
    val chatSessionIdleFlow: SharedFlow<Boolean> = _chatSessionIdleFlow.asSharedFlow()

    private val _currentChatRoomFlow = MutableStateFlow<ChatRoom?>(null)
    val currentChatRoomFlow: StateFlow<ChatRoom?> = _currentChatRoomFlow.asStateFlow()

    private val messages = Collections.synchronizedList(ArrayList<LiveLikeChatMessage>())
    private val deletedMsgList = Collections.synchronizedList(arrayListOf<String>())
    private var reactionSessionFlow = MutableStateFlow<LiveLikeReactionSession?>(null)
    private var blockedProfileIds = hashSetOf<String>()

    init {
        logDebug { "Chat Session init :$this" }
        sessionErrorHandlerScope.launch {
            launch {
                configurationProfilePairOnce().let { pair ->
                    initializeChatMessaging(
                        currentPlayheadTime, pair.second.pubNubKey?.let {
                            RealTimeMessagingClientConfig(
                                it,
                                pair.first.accessToken,
                                pair.first.id,
                                pair.second.pubnubPublishKey,
                                pair.second.pubnubOrigin,
                                pair.second.pubnubHeartbeatInterval,
                                pair.second.pubnubPresenceTimeout
                            )
                        }, LiveLikeMessagingClientParam(
                            pair.second.chatRoomEventsUrl,
                            pair.second.apiPollingInterval,
                        )
                    )
                    isReceivingRealtimeUpdates = true
                    _chatSessionIdleFlow.emit(true)
                }
            }
            launch {
                reactionSessionFlow.collect { reactionSession ->
                    reactionSession?.let {
                        reactionSession.subscribeToUserReactionDelegate(
                            this@ChatSession.toString(),
                            object : UserReactionDelegate {
                                override fun onReactionAdded(reaction: UserReaction) {
                                    reaction.reactionId.let {
                                        proxyMsgListener.addMessageReaction(
                                            null, reaction.targetId, ChatMessageReaction(
                                                it, null, reaction.id, reaction.reactedById
                                            )
                                        )
                                    }
                                }

                                override fun onReactionRemoved(reaction: UserReaction) {
                                    reaction.reactionId.let { it1 ->
                                        proxyMsgListener.removeMessageReaction(
                                            null, reaction.targetId, it1, reaction.reactedById
                                        )
                                    }
                                }
                            })
                    } ?: logDebug { "reaction session is null" }
                }
            }
            liveLikeProfileClient.getProfileBlockIds { result, error ->
                error?.let {
                    logError { "Block Profile Ids Error: $it" }
                }
                result?.let {
                    blockedProfileIds.addAll(it)
                }
            }
            (liveLikeChatClient as InternalLiveLikeChatClient).subscribeToChatRoomInternalDelegate(
                this@ChatSession.hashCode().toString(),
                object : ChatRoomDelegate() {
                    override fun onNewChatRoomAdded(chatRoomAdd: ChatRoomAdd) {
                        //nothing needed here
                    }

                    override fun onReceiveInvitation(invitation: ChatRoomInvitation) {
                        //nothing needed here
                    }

                    override fun onBlockProfile(blockedInfo: BlockedInfo) {
                        blockedProfileIds.add(blockedInfo.blockedProfileID)
                    }

                    override fun onUnBlockProfile(blockInfoId: String, blockProfileId: String) {
                        blockedProfileIds.remove(blockProfileId)
                    }
                })
        }
    }

    private fun updatingURls(chatRoom: ChatRoom) {
        if (isClosed) {
            logError { SESSION_CLOSED }
            errorDelegate?.onError(SESSION_CLOSED)
            return
        }
        sessionErrorHandlerScope.launch {
            val pair = configurationProfilePairOnce()
            stickerPackRepository = StickerPackRepository(
                chatRoom.id, chatRoom.stickerPacksUrl, networkApiClient
            )
            chatReactionRepository = ChatReactionRepository(
                chatRoom.reactionPacksUrl, networkApiClient
            )
            _updateUrlFlow.value =
                chatRoom to (pair.second.pubNubKey.isNullOrEmpty() && pair.second.pubnubPublishKey.isNullOrEmpty()).not()
        }
    }

    override fun pause() {
        realTimeChatMessagingClient?.stop()
    }

    override fun resume() {
        realTimeChatMessagingClient?.start()
    }

    override fun close() {
        isReceivingRealtimeUpdates = false
        realTimeChatMessagingClient?.destroy()
        (liveLikeChatClient as InternalLiveLikeChatClient).unsubscribeToChatRoomDelegate(this.toString())
        destroy()
        isClosed = true
        errorDelegate = null
    }

    private val messageIdList = HashSet<String>()

    // TODO remove proxy message listener by having pipe in chat data layers/chain that tranforms pubnub channel to room
    private var proxyMsgListener: MessageWithReactionListener =
        object : MessageWithReactionListener {
            val cacheReactionsForMessageIdMap =
                hashMapOf<String, Map<String, Set<ChatMessageReaction>>>()
            val cacheReactionsForTimeTokenMap =
                hashMapOf<Long, Map<String, Set<ChatMessageReaction>>>()

            fun updateMessageWithReactions(message: LiveLikeChatMessage) {
                message.apply {
                    id?.let {
                        val reactionsMap1 = cacheReactionsForMessageIdMap[it] ?: emptyMap()
                        val reactionMap = profileReactionListForEmojiMap.toMutableMap()
                        reactionsMap1.forEach { entry ->
                            val list = HashSet(reactionMap[entry.key] ?: emptySet())
                            list.addAll(entry.value)
                            reactionMap[entry.key] = list.toList()
                        }
                        this.profileReactionListForEmojiMap = reactionMap
                        cacheReactionsForMessageIdMap.remove(it)
                    } ?: logDebug { "Message id is null" }
                    if (timetoken > 0L) {
                        val reactionsMap2 =
                            HashMap(cacheReactionsForTimeTokenMap[timetoken] ?: emptyMap())
                        val reactionMap = profileReactionListForEmojiMap.toMutableMap()
                        reactionsMap2.forEach { entry ->
                            val list = HashSet(reactionMap[entry.key] ?: emptySet())
                            list.addAll(entry.value)
                            reactionMap[entry.key] = list.toList()
                        }
                        this.profileReactionListForEmojiMap = reactionMap
                        cacheReactionsForTimeTokenMap.remove(timetoken)
                    }
                }
            }

            override fun onNewMessage(message: LiveLikeChatMessage) {
                if (blockedProfileIds.contains(message.senderId)) {
                    logDebug { "profile is blocked" }
                    return
                }


                val index =
                    this@ChatSession.messages.indexOfFirst { it.id == message.id || (it.clientMessageId == message.clientMessageId && message.messageEvent != PubnubChatEventType.CUSTOM_MESSAGE_CREATED) }
                logDebug { "proxy onNewMessage:$index , ${this@ChatSession.messages.size}" }
                message.apply {
                    this.message = this.message?.decodeTextMessage()
                    isBlocked = blockedProfileIds.contains(senderId)
                    quoteMessage?.apply {
                        isBlocked = blockedProfileIds.contains(senderId)
                        this.message = this.message?.decodeTextMessage()
                    }
                    updateMessageWithReactions(this)
                }
                if (index > -1 && index < this@ChatSession.messages.size) {
                    this@ChatSession.messages[index] = message
                } else {
                    this@ChatSession.messages.add(message)
                }

                if (chatEnforceUniqueMessageCallback) {
                    if (messageIdList.contains(message.id)) {
                        return
                    }
                }
                message.id?.let { messageIdList.add(it) }
                uiScope.launch {
                    msgListener?.onNewMessage(message)
                    messageWithReactionListener?.onNewMessage(message)
                }
            }

            override fun onHistoryMessage(messages: List<LiveLikeChatMessage>) {
                logDebug { "proxy onHistoryMessage: ${this@ChatSession.messages.size} ,new Messages:${messages.size} msgLister:$msgListener ,reactionListenr:${messageWithReactionListener}" }
                this@ChatSession.messages.addAll(0, messages.filter {
                    synchronized(deletedMsgList) {
                        !blockedProfileIds.contains(it.senderId) && !deletedMsgList.contains(it.id)
                    }
                }.map {
                    it.apply {
                        quoteMessage?.apply {
                            isBlocked = blockedProfileIds.contains(senderId)
                        }
                        isBlocked = blockedProfileIds.contains(senderId)
                        message = message?.decodeTextMessage()
                        quoteMessage?.message = quoteMessage?.message?.decodeTextMessage()
                        updateMessageWithReactions(this)
                    }
                })
                uiScope.launch {
                    msgListener?.onHistoryMessage(messages)
                    messageWithReactionListener?.onHistoryMessage(messages)
                }
            }

            override fun onDeleteMessage(messageId: String) {
                synchronized(deletedMsgList) {
                    deletedMsgList.add(messageId)
                }

                //commenting this as this will not show "message has been removed"
               /* val index = messages.indexOfFirst { it.id == messageId }
                if (index > -1 && index < messages.size) {
                    this@ChatSession.messages.removeAt(index)
                }*/

                uiScope.launch {
                    msgListener?.onDeleteMessage(messageId)
                    messageWithReactionListener?.onDeleteMessage(messageId)
                }
            }

            override fun onPinMessage(message: PinMessageInfo) {
                uiScope.launch {
                    msgListener?.onPinMessage(message)
                    messageWithReactionListener?.onPinMessage(message)
                }
            }

            override fun onUnPinMessage(pinMessageId: String) {
                uiScope.launch {
                    msgListener?.onUnPinMessage(pinMessageId)
                    messageWithReactionListener?.onUnPinMessage(pinMessageId)
                }
            }

            override fun onErrorMessage(error: String, clientMessageId: String?) {
                uiScope.launch {
                    msgListener?.onErrorMessage(error, clientMessageId)
                    messageWithReactionListener?.onErrorMessage(error, clientMessageId)
                }
            }

            private val processedUserReactionIds = HashSet<String>()
            override fun addMessageReaction(
                messagePubnubToken: Long?,
                messageId: String?,
                chatMessageReaction: ChatMessageReaction
            ) {
                val index =
                    this@ChatSession.messages.indexOfFirst { it.id == messageId || it.timetoken == messagePubnubToken }
                if (index > -1 && index < this@ChatSession.messages.size) {
                    val chatMessage = this@ChatSession.messages[index]
                    val emojiId = chatMessageReaction.emojiId
                    val reactionMap = chatMessage.profileReactionListForEmojiMap.toMutableMap()
                    val reactions = ArrayList(reactionMap[emojiId] ?: emptyList())
                    val reactionIndex =
                        reactions.indexOfFirst { it.emojiId == chatMessageReaction.emojiId && it.userId == chatMessageReaction.userId }
                    if (reactionIndex > -1) {
                        val reaction = reactions[reactionIndex]
                        reaction.apply {
                            if (chatMessageReaction.pubnubActionToken != null) pubnubActionToken =
                                chatMessageReaction.pubnubActionToken
                            if (chatMessageReaction.userReactionId != null) userReactionId =
                                chatMessageReaction.userReactionId
                        }
                    } else {
                        reactions.add(chatMessageReaction)
                    }
                    reactionMap[emojiId] = reactions
                    chatMessage.profileReactionListForEmojiMap = reactionMap
                    this@ChatSession.messages[index] = chatMessage
                } else {
                    if (messageId != null) {
                        val map = HashMap(cacheReactionsForMessageIdMap[messageId] ?: emptyMap())
                        val list = HashSet(map[chatMessageReaction.emojiId] ?: emptyList())
                        list.add(chatMessageReaction)
                        map[chatMessageReaction.emojiId] = list
                        cacheReactionsForMessageIdMap[messageId] = map
                    } else if (messagePubnubToken != null) {
                        val map =
                            HashMap(cacheReactionsForTimeTokenMap[messagePubnubToken] ?: emptyMap())
                        val list = HashSet(map[chatMessageReaction.emojiId] ?: emptyList())
                        list.add(chatMessageReaction)
                        map[chatMessageReaction.emojiId] = list
                        cacheReactionsForTimeTokenMap[messagePubnubToken] = map
                    } else {
                        logError { "Both message id and message time token are Null" }
                    }
                }

                if (chatEnforceUniqueMessageCallback) {
                    val userReactionId = chatMessageReaction.userReactionId
                    if (userReactionId == null || messageId == null || processedUserReactionIds.contains(
                            userReactionId
                        )
                    ) {
                        return
                    } else {
                        processedUserReactionIds.add(userReactionId)
                        uiScope.launch {
                            messageWithReactionListener?.addMessageReaction(
                                messagePubnubToken, messageId, chatMessageReaction
                            )
                        }
                    }
                } else {
                    uiScope.launch {
                        messageWithReactionListener?.addMessageReaction(
                            messagePubnubToken, messageId, chatMessageReaction
                        )
                    }
                }
            }

            override fun removeMessageReaction(
                messagePubnubToken: Long?, messageId: String?, emojiId: String, userId: String?
            ) {
                val index =
                    this@ChatSession.messages.indexOfFirst { it.id == messageId || it.timetoken == messagePubnubToken }
                if (index > -1 && index < this@ChatSession.messages.size) {

                    val chatMessage = this@ChatSession.messages[index]
                    val reactionMap = chatMessage.profileReactionListForEmojiMap.toMutableMap()
                    val reactions = ArrayList(reactionMap[emojiId] ?: emptyList())

                    val reactionUserIndex =
                        reactions.indexOfFirst { it.emojiId == emojiId && userId == it.userId }
                    if (reactionUserIndex > -1) {
                        processedUserReactionIds.remove(reactions[reactionUserIndex].userReactionId)
                        reactions.removeAt(reactionUserIndex)
                    }
                    reactionMap[emojiId] = reactions
                    chatMessage.profileReactionListForEmojiMap = reactionMap
                    this@ChatSession.messages[index] = chatMessage
                } else {
                    if (messageId != null) {
                        val map = HashMap(cacheReactionsForMessageIdMap[messageId] ?: emptyMap())
                        val list = HashSet(map[emojiId] ?: emptySet())
                        val findReactionIndex =
                            list.find { it.emojiId == emojiId && it.userId == userId }
                        if (findReactionIndex != null) {
                            list.remove(findReactionIndex)
                        }
                        map[emojiId] = list
                        cacheReactionsForMessageIdMap[messageId] = map
                    } else if (messagePubnubToken != null) {
                        val map =
                            HashMap(cacheReactionsForTimeTokenMap[messagePubnubToken] ?: emptyMap())
                        val list = HashSet(map[emojiId] ?: emptySet())
                        val findReactionIndex =
                            list.find { it.emojiId == emojiId && it.userId == userId }
                        if (findReactionIndex != null) {
                            list.remove(findReactionIndex)
                        }
                        map[emojiId] = list
                        cacheReactionsForTimeTokenMap[messagePubnubToken] = map
                    } else {
                        logError { "Both message id and message time token are Null" }
                    }
                }

                if (chatEnforceUniqueMessageCallback) {
                    if (messageId == null) {
                        return
                    }
                    uiScope.launch {
                        messageWithReactionListener?.removeMessageReaction(
                            messagePubnubToken, messageId, emojiId, userId
                        )
                    }
                } else {
                    uiScope.launch {
                        messageWithReactionListener?.removeMessageReaction(
                            messagePubnubToken, messageId, emojiId, userId
                        )
                    }
                }
            }
        }

    private var msgListener: MessageListener? = null
    private var messageWithReactionListener: MessageWithReactionListener? = null
    private var chatRoomListener: ChatRoomListener? = null

    private val proxyChatRoomListener = object : ChatRoomListener {
        override fun onChatRoomUpdate(chatRoom: ChatRoomInfo) {
            uiScope.launch {
                chatRoomListener?.onChatRoomUpdate(chatRoom)
            }
        }
    }

    private fun initializeChatMessaging(
        currentPlayheadTime: () -> EpochTime,
        realTimeMessagingClientConfig: RealTimeMessagingClientConfig?,
        liveLikeMessagingClientParam: LiveLikeMessagingClientParam
    ) {
        analyticsService.trackLastChatStatus(true)
        realTimeChatMessagingClient = if (realTimeMessagingClientConfig != null) {
            RealTimeMessagingClient.getInstance(realTimeMessagingClientConfig, sessionScope)
        } else {
            LiveLikeMessagingClient(liveLikeMessagingClientParam,
                networkApiClient,
                currentChatRoomFlow.map { it?.id }
                    .stateIn(sessionScope, SharingStarted.Eagerly, null),
                sessionScope)
        }
        currentPlayheadTime.let {
            realTimeChatMessagingClient = realTimeChatMessagingClient?.syncTo(
                it, dispatcher = synchronizationDispatcher, sessionScope = sessionScope
            )
        }
        realTimeChatMessagingClient?.let {
            sessionScope.launch {
                val user = currentProfileOnce()
                launch {
                    it.messageClientFlow.filter {
                        return@filter when (it.event.toPubnubChatEventType()) {
                            PubnubChatEventType.MESSAGE_CREATED, PubnubChatEventType.IMAGE_CREATED, PubnubChatEventType.CUSTOM_MESSAGE_CREATED -> true
                            else -> false
                        }
                    }.map {
                        return@map it.event.toPubnubChatEventType()!! to gson.fromJson<PubnubChatMessage>(
                            it.payload, PubnubChatMessage::class.java
                        ).toChatMessage(
                            it.timeToken, emptyMap(), it.event.toPubnubChatEventType()!!
                        )
                    }.map { it.second }.filter {
                        isMessageModerated(it, user.id).not()
                    }.map {
                        preLoadMessageImages(it)
                        it
                    }.collect {
                        proxyMsgListener.onNewMessage(it)
                    }
                }
                launch {
                    it.messageClientFlow.filter {
                        return@filter when (it.event.toPubnubChatEventType()) {
                            PubnubChatEventType.MESSAGE_DELETED, PubnubChatEventType.IMAGE_DELETED -> true
                            else -> false
                        }
                    }.map {
                        return@map it.event.toPubnubChatEventType()!! to it.payload.get("id")?.asString
                    }.filter { it.second != null && it.second.isNullOrEmpty().not() }
                        .map { it.second }.filterNotNull().collect {
                            proxyMsgListener.onDeleteMessage(it)
                        }
                }
                launch {
                    it.messageClientFlow.filter {
                        return@filter it.event.toPubnubChatEventType() == PubnubChatEventType.CHATROOM_UPDATED
                    }.map {
                        return@map gson.fromJson<ChatRoom>(
                            it.payload, ChatRoom::class.java
                        )
                    }.collect {
                        chatRoomListener?.onChatRoomUpdate(it)
                    }
                }
                launch {
                    it.messageClientFlow.filter {
                        return@filter when (it.event.toPubnubChatEventType()) {
                            PubnubChatEventType.MESSAGE_PINNED, PubnubChatEventType.MESSAGE_UNPINNED -> true
                            else -> false
                        }
                    }.map {
                        return@map it.event.toPubnubChatEventType()!! to gson.fromJson<PubnubPinMessageInfo>(
                            it.payload, PubnubPinMessageInfo::class.java
                        ).toPinMessageInfo()
                    }.collect {
                        when (it.first) {
                            PubnubChatEventType.MESSAGE_PINNED -> proxyMsgListener.onPinMessage(it.second)
                            PubnubChatEventType.MESSAGE_UNPINNED -> proxyMsgListener.onUnPinMessage(
                                it.second.id
                            )

                            else -> {}
                        }
                    }
                }
                launch {
                    it.messageClientFlow.filter {
                        return@filter when (it.event.toPubnubChatEventType()) {
                            PubnubChatEventType.CHATROOM_UPDATED -> true
                            else -> false
                        }
                    }.map {
                        return@map gson.fromJson<ChatRoom>(it.payload, ChatRoom::class.java)
                    }.collect {
                        proxyChatRoomListener.onChatRoomUpdate(it)
                    }
                }
                launch {
                    it.messageActionFlow.collect {
                        when (it.event) {
                            "added" -> {
                                it.payload.run {
                                    proxyMsgListener.addMessageReaction(
                                        get("messagePubnubToken").asLong, null, ChatMessageReaction(
                                            get("emojiId").asString,
                                            get("actionPubnubToken").asString.toLongOrNull(),
                                            null,
                                            get("userId").asString
                                        )
                                    )
                                }
                            }

                            "removed" -> {
                                it.payload.run {
                                    proxyMsgListener.removeMessageReaction(
                                        get("messagePubnubToken").asLong,
                                        null,
                                        get("emojiId").asString,
                                        get("userId").asString
                                    )
                                }
                            }

                            else -> {
                                logError { "Event not handled: ${it.event}" }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun isMessageModerated(chatMessage: LiveLikeChatMessage, userId: String): Boolean {
        return when {
            chatMessage.senderId == userId -> {
                // the message is yours, show it
                false
            }

            chatMessage.contentFilter?.contains("shadow-muted") ?: false -> {
                //never show a shadow muted message
                true
            }

            includeFilteredChatMessages -> {
                //new behavior, Show all filtered messages
                false
            }

            else -> {
                //old behavior, Do not show any filtered messages
                (chatMessage.contentFilter?.size ?: 0) > 0
            }
        }
    }

    private fun processReactionCounts(actions: Map<String, List<PubnubChatReaction>>?): MutableMap<String, List<ChatMessageReaction>> {
        val reactionCountMap = mutableMapOf<String, List<ChatMessageReaction>>()
        actions?.let {
            for (value in actions.keys) {
                val reactions = actions[value]?.map {
                    ChatMessageReaction(
                        value, it.actionTimeToken?.toLongOrNull(), it.userReactionId, it.uuid
                    )
                } ?: emptyList()
                reactionCountMap[value] = reactions
            }
        } ?: logDebug { "actions are null" }
        return reactionCountMap
    }

    override fun connectToChatRoom(
        chatRoomId: String,
        callback: com.livelike.common.LiveLikeCallback<Unit>?,
        callbackWithSeverity: LiveLikeCallbackWithSeverity<Unit>?
    ) {
        if (chatRoomId.isEmpty()) {
            callback?.invoke(null, CHAT_ROOM_ID_EMPTY)
            callbackWithSeverity?.invoke(null,ErrorDetails(CHAT_ROOM_ID_EMPTY,ErrorSeverity.LOW))
            uiScope.launch { errorDelegate?.onError(CHAT_ROOM_ID_EMPTY) }
            return
        }
        if (currentChatRoomFlow.value?.getChatChannel()?.contains(chatRoomId) == true) {
            callback?.invoke(null, ALREADY_CONNECTED_TO_CHATROOM_ERROR)
            callbackWithSeverity?.invoke(null,ErrorDetails(ALREADY_CONNECTED_TO_CHATROOM_ERROR,ErrorSeverity.LOW))
            return // Already in the room
        }
        currentChatRoomFlow.value?.let { chatRoom ->
            realTimeChatMessagingClient?.unsubscribe(
                listOf(
                    chatRoom.getChatChannel() ?: "", chatRoom.getControlChannel() ?: ""
                )
            )
        }
        flushMessagesFlow.value = chatRoomId
        messages.clear()
        synchronized(deletedMsgList) {
            deletedMsgList.clear()
        }
        this.currentChatRoomId = chatRoomId
        logDebug { "Connecting to ChatRoom: $chatRoomId" }
        sessionErrorHandlerScope.launch {
            chatSessionIdleFlow.collect { idle ->
                if (idle) {
                    safeCallBack(callback,callbackWithSeverity) {
                        val chatRoom =
                            (liveLikeChatClient as InternalLiveLikeChatClient).getChatRoomDetails(
                                chatRoomId
                            )
                        if (chatRoom.id == currentChatRoomId) {
                            flushMessagesFlow.value = chatRoom.id
                            if (currentChatRoomId == chatRoom.id) {
                                updatingURls(chatRoom)
                            } else {
                                logDebug { "Current ChatRoom id not matched:3" }
                                throw LiveLikeException("Current Chat Room id not matched")
                            }
                            delay(1000)
                            if (currentChatRoomId == chatRoom.id) {
                                _currentChatRoomFlow.value = chatRoom
                            } else {
                                logDebug { "Current ChatRoom id not matched:4" }
                                throw LiveLikeException("Current Chat Room id not matched")
                            }
                            _chatLoadedFlow.value = chatRoom.id
                            if (currentChatRoomId == chatRoom.id) {
                                reactionSessionFlow.value?.unSubscribeToUserReactionDelegate(
                                    this@ChatSession.toString()
                                )
                                reactionSessionFlow.value = chatRoom.reactionSpaceId?.let {
                                    getReactionSession(it)
                                }
                                this@ChatSession._currentChatRoomFlow.value = chatRoom
                                loadNextHistory()
                                chatRoom.getChatChannel()?.let {
                                    realTimeChatMessagingClient?.subscribe(listOf(it))
                                }
                                chatRoom.getControlChannel()?.let {
                                    realTimeChatMessagingClient?.subscribe(listOf(it))
                                }
                                Unit
                            } else {
                                logDebug { "Current ChatRoom id not matched:5" }
                                throw LiveLikeException("Current Chat Room id not matched")
                            }
                        } else {
                            logDebug { "Current ChatRoom id not matched:1" }
                            throw LiveLikeException("Current Chat Room id not matched")
                        }
                    }
                } else {
                    uiScope.launch {
                        callback?.invoke(null, CHAT_SESSION_NOT_IDLE)
                        callbackWithSeverity?.invoke(null, ErrorDetails(CHAT_SESSION_NOT_IDLE,ErrorSeverity.LOW))
                    }
                }
            }
        }
    }

    override fun connectToChatRoom(chatRoomId: String) {
        connectToChatRoom(chatRoomId) { result, error ->
            result?.let { logDebug { it.toString() } }
            error?.let { logError { it } }
        }
    }


    override fun getMessageCount(
        startTimestamp: Long,
        callback: com.livelike.common.LiveLikeCallback<Byte>
    ) {
        if (currentChatRoomId != null) {
            logDebug { "messageCount ${this.currentChatRoomId} , StartTime: $startTimestamp" }
            sessionScope.launch {
                chatSessionIdleFlow.collect { idle ->
                    if (idle) {
                        safeCallBack(callback) {
                            currentChatRoomFlow.value?.let { chatRoom ->
                                chatRoom.chatroomMessagesCountUrl.let { url ->
                                    val since = ZonedDateTime.ofInstant(
                                        Instant.ofEpochMilli(startTimestamp), ZoneId.of("UTC")
                                    ).isoDateTimeFormat()
                                    val until = ZonedDateTime.ofInstant(
                                        Instant.ofEpochMilli(Calendar.getInstance().timeInMillis),
                                        ZoneId.of("UTC")
                                    ).isoDateTimeFormat()

                                    networkApiClient.get(
                                        url,
                                        accessToken = it.first.accessToken,
                                        queryParameters = listOf("since" to since, "until" to until)
                                    ).processResult()
                                }
                            } ?: throw LiveLikeException(NO_ACTIVE_CHATROOM)
                        }
                    } else {
                        logError { "Chat Session not Idle" }
                        uiScope.launch { callback.invoke(null, CHATSESSION_NOT_IDLE) }
                    }
                }
            }
        } else {
            logError { "ChatRoom Not Found" }
            callback.invoke(null, CHATROOM_NOT_FOUND)
        }
    }

    override fun setMessageListener(
        messageListener: MessageListener?
    ) {
        msgListener = messageListener
    }

    override fun setMessageWithReactionListener(messageWithReactionListener: MessageWithReactionListener?) {
        this.messageWithReactionListener = messageWithReactionListener
    }

    override fun setChatRoomListener(chatRoomListener: ChatRoomListener?) {
        this.chatRoomListener = chatRoomListener
    }

    override var avatarUrl: String? = null

    /**
     * TODO: added it into default chat once all functionality related to chat is done
     */
    override fun sendMessage(
        message: String?,
        parentMessageId: String?,
        imageUrl: String?,
        imageWidth: Int?,
        imageHeight: Int?,
        messageMetadata: Map<String, Any?>?,
        liveLikePreCallback: com.livelike.common.LiveLikeCallback<LiveLikeChatMessage>,
        chatInterceptor: ChatInterceptor?,
        callback: com.livelike.common.LiveLikeCallback<LiveLikeChatMessage>?,
    ) {
        internalSendMessage(
            message = message,
            imageUrl = imageUrl,
            imageWidth = imageWidth,
            imageHeight = imageHeight,
            messageMetadata = messageMetadata,
            preLiveLikeCallback = liveLikePreCallback,
            chatInterceptor = chatInterceptor,
            callback = callback,
            parentChatMessageId = parentMessageId
        )
    }


    override fun quoteMessage(
        message: String?,
        parentMessageId: String?,
        imageUrl: String?,
        imageWidth: Int?,
        imageHeight: Int?,
        quoteMessageId: String,
        quoteMessage: LiveLikeChatMessage,
        messageMetadata: Map<String, Any?>?,
        liveLikePreCallback: com.livelike.common.LiveLikeCallback<LiveLikeChatMessage>,
        chatInterceptor: ChatInterceptor?,
        callback: com.livelike.common.LiveLikeCallback<LiveLikeChatMessage>?,
    ) {
        // Removing the parent message from parent message in order to avoid reply to reply in terms of data
        // and avoid data nesting

        if (quoteMessage.quoteMessage != null) {
            quoteMessage.quoteMessage = null
        }
        internalSendMessage(
            message = message,
            imageUrl = imageUrl,
            imageWidth = imageWidth,
            imageHeight = imageHeight,
            messageMetadata = messageMetadata,
            parentChatMessage = quoteMessage,
            preLiveLikeCallback = liveLikePreCallback,
            chatInterceptor = chatInterceptor,
            callback = callback,
            parentChatMessageId = parentMessageId
        )
    }



    private var lastPaginatedRepliesChatMessageResponse: PaginationResponse<PubnubChatMessage>? = null

    override fun getChatMessageReplies(
        getChatMessageRepliesRequestOption: GetChatMessageRepliesRequestOption?,
        callback: com.livelike.common.LiveLikeCallback<List<LiveLikeChatMessage>>?
    ) {
        safeCallBack(callback) {pair ->
            val user = pair.first
            currentChatRoomFlow.value?.let { chatRoom ->
                chatRoom.chatroomMessageUrl.let { url ->
                    logDebug { "History: $url \nroom:${chatRoom.id}\n" }
                    if (lastPaginatedRepliesChatMessageResponse != null && lastPaginatedRepliesChatMessageResponse!!.previous == null) {
                        emptyList()
                    } else {
                        val params = arrayListOf<Pair<String, Any>>()
                        params.add(CHAT_ROOM_ID to chatRoom.id)
                        getChatMessageRepliesRequestOption?.let {
                            params.add(PAGE_SIZE to it.limit)
                            params.add(PARENT_MESSAGE_ID to it.parentMessageId)

                        }
                        val messageList = networkApiClient.get(
                            lastPaginatedRepliesChatMessageResponse?.previous ?: url,
                            accessToken = user.accessToken,
                            queryParameters = when (lastPaginatedRepliesChatMessageResponse?.previous != null) {
                                true -> emptyList()
                                false -> params
                            }
                        ).processResult<PaginationResponse<PubnubChatMessage>>()
                            .also {
                                lastPaginatedRepliesChatMessageResponse = it
                            }
                            .run { this.results }.let { results ->
                                val list = if (results.isNotEmpty()) {
                                  /*  results.asSequence().map {
                                        when (it.messageEvent?.toPubnubChatEventType()) {
                                            PubnubChatEventType.MESSAGE_DELETED, PubnubChatEventType.IMAGE_DELETED -> {
                                                it.messageId?.let { it1 ->
                                                    proxyMsgListener.onDeleteMessage(it1)
                                                }
                                            }

                                            else -> {}
                                        }
                                        it
                                    }.filter {
                                        when (it.messageEvent?.toPubnubChatEventType()) {
                                            PubnubChatEventType.MESSAGE_DELETED, PubnubChatEventType.IMAGE_DELETED -> {
                                                false
                                            }

                                            else -> true
                                        }
                                    }*/
                                        results.map {
                                        it.toChatMessage(
                                            it.pubnubTimeToken?.toLong() ?: 0L,
                                            emptyMap() , //processReactionCounts(it.reactions), //since reactions will not be coming in threads response.Will have to fetch from user reaction count api
                                            it.messageEvent?.toPubnubChatEventType()
                                                ?: PubnubChatEventType.MESSAGE_CREATED
                                        )
                                    }.filter { chatMessage ->
                                        return@filter !isMessageModerated(chatMessage, user.id)
                                    }.toList()
                                } else {
                                    emptyList()
                                }
                                logDebug { "CHAT REPLIES----: ${list.size}" }
                                list
                            }
                         for (msg in messageList) {
                            preLoadMessageImages(msg)
                        }
                        messageList
                    }
                }
            } ?: throw java.lang.Exception(NO_ACTIVE_CHATROOM)
        }
    }



    private val messageSendingQueue =
        Queue<Pair<PubnubChatMessage, com.livelike.common.LiveLikeCallback<LiveLikeChatMessage>?>>()
    private var isPublishRunning = false

    private fun internalSendMessage(
        message: String?,
        imageUrl: String?,
        imageWidth: Int?,
        imageHeight: Int?,
        messageMetadata: Map<String, Any?>?,
        parentChatMessage: LiveLikeChatMessage? = null,
        preLiveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeChatMessage>,
        chatInterceptor: ChatInterceptor?,
        callback: com.livelike.common.LiveLikeCallback<LiveLikeChatMessage>?,
        parentChatMessageId: String? = null,

    ) {
        if (message?.isEmpty() == true) {
            preLiveLikeCallback.invoke(null, MESSAGE_CANNOT_BE_EMPTY)
            return
        }
        val timeData = getPlayheadTime()
        sessionErrorHandlerScope.launch {
            when (imageUrl != null) {
                true -> LiveLikeChatMessage(
                    message = null,
                    imageUrl = imageUrl,
                    imageWidth = imageWidth,
                    imageHeight = imageHeight
                )

                false -> LiveLikeChatMessage(message)
            }.apply {
                messageEvent = when (imageUrl != null) {
                    true -> PubnubChatEventType.IMAGE_CREATED
                    else -> PubnubChatEventType.MESSAGE_CREATED
                }
                this.customData = ""
                this.senderId = currentProfileOnce().id
                this.nickname = currentProfileOnce().nickname
                this.profilePic = avatarUrl
                this.isFromMe = true
                this.timeStamp = timeData.timeSinceEpochInMs.toString()
                this.quoteMessage = parentChatMessage
                this.clientMessageId = UUID.randomUUID().toString()
                this.chatRoomId = currentChatRoomId
                this.messageMetadata = messageMetadata
                this.filteredMessage = null
                this.contentFilter = null
                this.quoteMessageID = parentChatMessage?.id
                this.parentMessageId = parentChatMessageId
            }.let { chatMessage ->
                // TODO: need to update for error handling here if pubnub respond failure of message
                val allowChat = chatInterceptor?.invoke(chatMessage) ?: true
                if (allowChat) {
                    preLoadMessageImages(chatMessage)
                    preLiveLikeCallback.invoke(chatMessage, null)
                    if (currentChatRoomFlow.value?.chatroomMessageUrl != null) {
//                    val messageUrl = currentChatRoomFlow.value!!.chatroomMessageUrl!!
                        val hasExternalImage = imageUrl != null
                        if (hasExternalImage && chatMessage.imageUrl != null) {
                            val uploadImageUrl = chatMessage.imageUrl!!
                            logDebug { "IsPlatformLocalContentImageUrl: $isPlatformLocalContentImageUrl ,uploadUrl:$uploadImageUrl" }
                            when {
                                isPlatformLocalContentImageUrl(uploadImageUrl) -> {
                                    getInputStreamForImageUrl(uploadImageUrl)
                                }

                                else -> {
                                    networkApiClient.getInputStreamForDownloadFileFromUrl(
                                        uploadImageUrl
                                    )
                                }
                            }?.use { inputStream ->
                                val fileBytes = inputStream.readBytes()
                                val size = getSizeOfImage(fileBytes)

                                chatMessage.imageWidth = imageWidth ?: size.width
                                chatMessage.imageHeight = imageHeight ?: size.height
                                //TODO: adding the code to update image height/width with actual value instead of default
                                val result = networkApiClient.multipartFormData(
                                    currentChatRoomFlow.value!!.uploadUrl,
                                    "image",
                                    "image.png",
                                    fileBytes,
                                    "image/png",
                                    null,
                                    mapOf()
                                ).processResult<ImageResource>().image_url

                                logDebug { "Image Size:$size" }
                                result.let {
                                    chatMessage.messageEvent = PubnubChatEventType.IMAGE_CREATED
                                    chatMessage.imageUrl = it
                                    chatMessage.message = ""
                                    addMessageToQueue(chatMessage, timeData, callback)
                                }
                            } ?: logError { "InputStream is null" }
                        } else {
                            addMessageToQueue(chatMessage, timeData, callback)
                        }
                        currentChatRoomFlow.value?.id?.let { id ->
                            chatMessage.id?.let {
                                analyticsService.trackMessageSent(
                                    it, chatMessage.message, hasExternalImage, id
                                )
                            } ?: logDebug { "chat message id is null" }
                        } ?: logDebug { "current chat room is null or id is null" }
                    } else {
                        preLiveLikeCallback.invoke(null, "Current ChatRoom not connected")
                        errorDelegate?.onError("Current ChatRoom not connected")
                    }
                } else {
                    preLiveLikeCallback.invoke(
                        null, "Not allowed to send Message using Chat Interceptor"
                    )
                }
            }
        }
    }

    private fun addMessageToQueue(
        chatMessage: LiveLikeChatMessage,
        timeData: EpochTime,
        callback: com.livelike.common.LiveLikeCallback<LiveLikeChatMessage>?
    ) {
        sessionErrorHandlerScope.launch {
            messageSendingQueue.enqueue(chatMessage.toPubnubChatMessage(
                timeData.toUTCTime()
            ).apply {
                messageEvent = chatMessage.messageEvent.key
            } to callback)
            if (!isPublishRunning) {
                startPublishingFromQueue()
            }
        }
    }

    private suspend fun startPublishingFromQueue() {
        isPublishRunning = true
        while (!messageSendingQueue.isEmpty()) {
            messageSendingQueue.peek()?.let { pair ->
                try {
                    networkApiClient.post(
                        currentChatRoomFlow.value?.chatroomMessageUrl
                            ?: throw Exception("Current Chat Messaging URL is null"),
                        accessToken = currentProfileOnce().accessToken,
                        body = pair.first.toJsonString()
                    ).processResult<PubnubChatMessage>().also {
                        val msg = it.toChatMessage(
                            it.pubnubTimeToken?.toLong() ?: 0L,
                            mutableMapOf(),
                            it.messageEvent?.toPubnubChatEventType()
                                ?: PubnubChatEventType.MESSAGE_CREATED
                        )
                        preLoadMessageImages(msg)
                        proxyMsgListener.onNewMessage(msg)
                        pair.second?.invoke(msg, null)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    logError { e }
                    proxyMsgListener.onErrorMessage(
                        e.message ?: "Error while sending message",
                        pair.first.clientMessageId
                    )
                    pair.second?.invoke(null, e.message ?: "Error while sending message")
                }
                messageSendingQueue.dequeue()
                delay(100) // ensure messages not more than 5 per second as 100ms is pubnub latency
            }
        }
        isPublishRunning = false
    }

    //    private var firstUntil: String? = null
    private var lastPaginatedChatMessageResponse: PaginationResponse<PubnubChatMessage>? = null
    override fun loadNextHistory(
        limit: Int,
        sinceMessageId:String?,
        callbackWithSeverity: LiveLikeCallbackWithSeverity<List<LiveLikeChatMessage>>?
    ) {
        safeCallBack(liveLikeCallBack = { result, error ->
            result?.let { proxyMsgListener.onHistoryMessage(it) }
            error?.let {
                errorDelegate?.onError(it)
                proxyMsgListener.onErrorMessage(it, null)
            }
        },liveLikeCallbackWithSeverity = callbackWithSeverity) { pair ->
            val user = pair.first
            currentChatRoomFlow.value?.let { chatRoom ->
                chatRoom.chatroomMessageUrl.let { url ->
                    logDebug { "History: $url \nroom:${chatRoom.id}\n" }
                    if (lastPaginatedChatMessageResponse != null && lastPaginatedChatMessageResponse!!.previous == null) {
                        emptyList()
                    } else {
                        val params = arrayListOf<Pair<String, Any>>()
                        params.add(CHAT_ROOM_ID to chatRoom.id)
                        params.add(PAGE_SIZE to limit)
                        sinceMessageId?.let {
                            params.add("since_message_id" to sinceMessageId)
                        }
                        val messageList = networkApiClient.get(
                            lastPaginatedChatMessageResponse?.previous ?: url,
                            accessToken = user.accessToken,
                            queryParameters = when (lastPaginatedChatMessageResponse?.previous != null) {
                                true -> emptyList()
                                false -> params
                            }
                        ).processResult<PaginationResponse<PubnubChatMessage>>()
                            .also {
                                lastPaginatedChatMessageResponse = it
                            }
                            .run { this.results }.let { results ->
                                val list = if (results.isNotEmpty()) {
                                    results.asSequence().map {
                                        when (it.messageEvent?.toPubnubChatEventType()) {
                                            PubnubChatEventType.MESSAGE_DELETED, PubnubChatEventType.IMAGE_DELETED -> {
                                                it.messageId?.let { it1 ->
                                                    proxyMsgListener.onDeleteMessage(it1)
                                                }
                                            }

                                            else -> {}
                                        }
                                        it
                                    }.filter {
                                        when (it.messageEvent?.toPubnubChatEventType()) {
                                            PubnubChatEventType.MESSAGE_DELETED, PubnubChatEventType.IMAGE_DELETED -> {
                                                false
                                            }

                                            else -> true
                                        }
                                    }.map {
                                        it.toChatMessage(
                                            it.pubnubTimeToken?.toLong() ?: 0L,
                                            processReactionCounts(it.reactions),
                                            it.messageEvent?.toPubnubChatEventType()
                                                ?: PubnubChatEventType.MESSAGE_CREATED
                                        )
                                    }.filter { chatMessage ->
                                        return@filter !isMessageModerated(chatMessage, user.id)
                                    }.toList()
                                } else {
                                    emptyList()
                                }
                                logDebug { "Chat History: ${list.size}" }
                                list
                            }
                        for (msg in messageList) {
                            preLoadMessageImages(msg)
                        }
                        messageList
                    }
                }
            } ?: throw java.lang.Exception(NO_ACTIVE_CHATROOM)
        }
    }

    private suspend fun preLoadMessageImages(msg: LiveLikeChatMessage) {
        msg.run {
            imageUrl?.let { preLoadImage(it) }
            profilePic?.let { preLoadImage(it) }
            val matcher = message?.findStickers()
            if (matcher != null) {
                while (matcher.find()) {
                    val url =
                        stickerPackRepository?.getStickerPacks()?.map { it.stickers }?.flatten()
                            ?.find { it.shortcode == matcher.group().replace(":", "") }?.file
                    url?.let { preLoadImage(it) }
                }
            }
            quoteMessage?.let { preLoadMessageImages(it) }
        }
    }

    override fun getLoadedMessages(): ArrayList<LiveLikeChatMessage> {
        synchronized(messages) {
            return ArrayList(messages)
        }
    }

    override fun getDeletedMessages(): ArrayList<String> {
        synchronized(deletedMsgList) {
            return ArrayList(deletedMsgList)
        }
    }

    override fun clearMessages() {
        synchronized(deletedMsgList) {
            messages.clear()
            deletedMsgList.clear()
        }
    }

    override fun sendCustomChatMessage(
        customData: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeChatMessage>
    ) {
        val timeData = getPlayheadTime()
        safeCallBack(liveLikeCallback) { pair ->
            currentChatRoomFlow.value?.customMessagesUrl?.let { url ->
                networkApiClient.post(
                    url, body = mapOf(
                        "custom_data" to customData, "program_date_time" to timeData.toUTCTime()
                    ).toJsonString(), accessToken = pair.first.accessToken
                ).processResult()
            } ?: throw Exception(CUSTOM_MESSAGE_URL_IS_NULL)
        }
    }


    override fun deleteMessage(
        messageId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack({ result: LiveLikeEmptyResponse?, error: String? ->
            result?.let { emptyResponse ->
                currentChatRoomFlow.value?.chatroomMessageUrl?.let {
                    proxyMsgListener.onDeleteMessage(messageId)
                } ?: logError { "Chat Room message Url is Null" }
                uiScope.launch {
                    liveLikeCallback.invoke(emptyResponse, null)
                }
            }
            error?.let {
                logError { it }
                uiScope.launch {
                    liveLikeCallback.invoke(null, it)
                }
            }
        }) { pair ->
            currentChatRoomFlow.value?.let { chatRoom ->
                chatRoom.deleteMessageUrl.let {
                    networkApiClient.post(
                        it, body = mapOf(
                            "message_id" to messageId,
//                            "pubnub_timetoken" to pubnubTimeToken
                        ).toJsonString(), accessToken = pair.first.accessToken
                    ).processResult()
                }
            } ?: throw LiveLikeException("No Active Chat Room")
        }
    }


   /* override fun deleteMessage(
        messageId: String, liveLikeCallback: LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        deleteMessage(messageId, liveLikeCallback.toNewCallback())
    }*/

    override fun getStickerPacks(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<StickerPack>>) {
        safeCallBack(liveLikeCallback) {
            stickerPackRepository?.getStickerPacks()
                ?: throw Exception(STICKER_PACK_REPOSITORY_NULL)
        }
    }


    override fun getReactions(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<Reaction>>) {
        safeCallBack(liveLikeCallback) {
            chatReactionRepository?.getReactions() ?: throw Exception(
                CHAT_REACTION_REPOSITORY_NULL
            )
        }
    }


    override fun reportMessage(
        message: LiveLikeChatMessage,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            currentChatRoomFlow.value?.reportMessageUrl?.let { url ->
                networkApiClient.post(
                    url, accessToken = pair.first.accessToken, body = message.toReportMessageJson()
                ).processResult()
            } ?: throw Exception(CHAT_ROOM_DETAILS_NULL)
        }
    }

    override fun reportMessage(
        messageId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            currentChatRoomFlow.value?.reportMessageUrl?.let { url ->
                val message =
                    messages.find { it.id == messageId } ?: throw Exception(MESSAGE_NOT_FOUND)
                networkApiClient.post(
                    url, accessToken = pair.first.accessToken, body = message.toReportMessageJson()
                ).processResult()
            } ?: throw Exception(CHAT_ROOM_DETAILS_NULL)
        }
    }


    override fun sendMessageReaction(
        messageId: String,
        reactionId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack({ result, error ->
            result?.let { pair ->
                val message = pair.first
                val userReaction = pair.second
                proxyMsgListener.addMessageReaction(
                    message.timetoken, message.id, ChatMessageReaction(
                        userReaction.reactionId,
                        null,
                        userReaction.id,
                        userReaction.reactedById
                    )
                )
                liveLikeCallback.invoke(
                    LiveLikeEmptyResponse(), null
                )
            }
            error?.let { liveLikeCallback.invoke(null, it) }
        }) {
            val message = messages.find { it.id == messageId } ?: throw Exception(MESSAGE_NOT_FOUND)
            val userReaction = suspendCoroutine { cont ->
                reactionSessionFlow.value?.addUserReaction(
                    messageId,
                    reactionId,
                    null
                ) { result, error ->
                    result?.let { userReaction ->
                        userReaction.reactionId.let {
                            cont.resume(userReaction)
                        }
                    }
                    error?.let { liveLikeCallback.invoke(null, it) }
                } ?: throw LiveLikeException("Reaction Session not setup")
            }
            message to userReaction
        }
    }


    override fun removeMessageReactions(
        messageId: String,
        reactionId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val message = messages.find { it.id == messageId } ?: throw Exception(MESSAGE_NOT_FOUND)
            logDebug { "Message: $message, reaction:${message.profileReactionListForEmojiMap}" }
            val reactions = message.profileReactionListForEmojiMap[reactionId]
                ?: throw Exception("Reaction not found")
            val myReaction =
                reactions.find { it.emojiId == reactionId && it.userId == pair.first.id }
                    ?: throw Exception(NO_REACTION)
            if (myReaction.userReactionId != null) {
                suspendCoroutine { cont ->
                    reactionSessionFlow.value?.removeUserReaction(myReaction.userReactionId!!) { result, error ->
                        result?.let {
                            proxyMsgListener.removeMessageReaction(
                                null, messageId, myReaction.emojiId, myReaction.userId
                            )
                            cont.resume(LiveLikeEmptyResponse())
                        }
                        error?.let {
                            cont.resumeWithException(Exception(it))
                        }
                    }
                }
            } else if (myReaction.pubnubActionToken != null) {
                realTimeChatMessagingClient?.removeMessageAction(
                    currentChatRoomFlow.value?.getChatChannel()
                        ?: throw Exception("No Current ChatRoom or its channel is null"),
                    message.timetoken,
                    myReaction.pubnubActionToken!!
                )?.let { LiveLikeEmptyResponse() }
            } else {
                logError { "Both User Reaction Id and PubnubActionToken are null" }
                throw Exception("Unable to remove reaction")
            }
        }
    }


    override fun isReceivingRealtimeUpdates(): Boolean {
        return isReceivingRealtimeUpdates
    }

    override fun onBlockProfile(blockedInfo: com.livelike.common.model.BlockedInfo) {
        blockedProfileIds.add(blockedInfo.blockedProfileID)
    }

    override fun onUnBlockProfile(blockInfoId: String, blockProfileId: String) {
        TODO("Not yet implemented")
    }
}
