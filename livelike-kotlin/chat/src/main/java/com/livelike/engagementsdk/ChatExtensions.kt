package com.livelike.engagementsdk

import com.livelike.common.LiveLikeKotlin
import com.livelike.common.TimeCodeGetter
import com.livelike.common.profile
import com.livelike.engagementsdk.chat.ChatSession
import com.livelike.engagementsdk.chat.ImageSize
import com.livelike.engagementsdk.chat.LiveLikeChatClient
import com.livelike.engagementsdk.chat.LiveLikeChatSession
import com.livelike.engagementsdk.publicapis.*
import com.livelike.serialization.isoUTCDateTimeFormatter
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime
import java.io.InputStream

/**
 *  Creates a chat client and returns that.
 */


private val sdkInstanceWithChatClient = mutableMapOf<Int, LiveLikeChatClient>()

fun LiveLikeKotlin.chat(): LiveLikeChatClient {
    val key = this.hashCode()
    return if (sdkInstanceWithChatClient.containsKey(key)) {
        sdkInstanceWithChatClient[key]!!
    } else {
        LiveLikeChatClient.getInstance(
            sdkConfigurationOnce,
            uiScope,
            sdkScope,
            networkClient,
            profile().currentProfileOnce
        ).let {
            sdkInstanceWithChatClient[key] = it
            it
        }
    }
}

/**
 *  Creates a chat session.
 *  @param programId Backend generated identifier for current program
 *  @param timecodeGetter returns the video timecode
 */
fun LiveLikeKotlin.createChatSession(
    timeCodeGetter: TimeCodeGetter = { EpochTime(0L) },
    errorDelegate: ErrorDelegate? = null,
    includeFilteredChatMessages: Boolean = false,
    quoteChatMessageDeletedMessage: String = "Message deleted.",
    chatMessageDeletedMessage: String = "This message has been removed",
    isPlatformLocalContentImageUrl: (String) -> Boolean = { false },
    getInputStreamForImageUrl: (String) -> InputStream? = { null },
    getSizeOfImage: (ByteArray) -> ImageSize = { ImageSize(100, 100) },
    preLoadImage: suspend (String) -> Boolean = { false },
    sessionDispatcher: CoroutineDispatcher = Dispatchers.Default,
    mainDispatcher: CoroutineDispatcher = Dispatchers.Main,
    synchronizationDispatcher: CoroutineDispatcher = Dispatchers.Default,
    chatEnforceUniqueMessageCallback: Boolean = false
): LiveLikeChatSession {
    return ChatSession(
        sdkConfigurationOnce,
        profile().currentProfileOnce,
        false,
        chatEnforceUniqueMessageCallback,
        analyticService,
        errorDelegate,
        chat(),
        profile(),
        networkClient,
        includeFilteredChatMessages = includeFilteredChatMessages,
        dataStoreDelegate = dataStoreDelegate,
        quoteChatMessageDeletedMessage = quoteChatMessageDeletedMessage,
        chatMessageDeletedMessage = chatMessageDeletedMessage,
        isPlatformLocalContentImageUrl = isPlatformLocalContentImageUrl,
        getInputStreamForImageUrl = getInputStreamForImageUrl,
        getSizeOfImage = getSizeOfImage,
        preLoadImage = preLoadImage,
        sessionDispatcher = sessionDispatcher,
        uiDispatcher = mainDispatcher,
        synchronizationDispatcher = synchronizationDispatcher,
        getReactionSession = { id ->
            createReactionSession(
                reactionSpaceId = id,
                targetGroupId = null,
                errorDelegate = errorDelegate
            )
        }
    ) { timeCodeGetter() }.also {
        addSession(it)
    }
}

fun EpochTime.toUTCTime(): String? {
    return when (timeSinceEpochInMs) {
        0L -> null
        else -> ZonedDateTime.ofInstant(
            Instant.ofEpochMilli(timeSinceEpochInMs), ZoneId.of("UTC")
        ).format(isoUTCDateTimeFormatter)
    }
}