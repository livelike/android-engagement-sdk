package com.livelike.engagementsdk.publicapis

import com.google.gson.annotations.SerializedName
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.ChatRoomInfo

data class ChatRoomInvitation(
    @SerializedName("id") val id: String,
    @SerializedName("url") val url: String,
    @SerializedName("created_at") val createdAt: String,
    @SerializedName("status") val status: String,
    @SerializedName("invited_profile") val invitedProfile: LiveLikeProfile,
    @SerializedName("chat_room") val chatRoom: ChatRoomInfo,
    @SerializedName("invited_by") val invitedBy: LiveLikeProfile,
    @SerializedName("chat_room_id") val chatRoomId: String,
    @SerializedName("invited_profile_id") val invitedProfileId: String
)


enum class ChatRoomInvitationStatus(val key: String) {
    PENDING("pending"), ACCEPTED("accepted"), REJECTED("rejected")
}