package com.livelike.engagementsdk.chat

import com.livelike.common.LiveLikeCallbackWithSeverity
import com.livelike.common.clients.BlockEventListener
import com.livelike.engagementsdk.ChatRoomListener
import com.livelike.engagementsdk.EpochTime
import com.livelike.engagementsdk.MessageListener
import com.livelike.engagementsdk.MessageWithReactionListener
import com.livelike.engagementsdk.chat.chatreaction.Reaction
import com.livelike.engagementsdk.chat.data.remote.GetChatMessageRepliesRequestOption
import com.livelike.engagementsdk.chat.stickerKeyboard.StickerPack
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse


/**
 * Created by Shivansh Mittal on 2020-04-08.
 */
interface LiveLikeChatSession : BlockEventListener {

    /** Return the playheadTime for this session.*/
    fun getPlayheadTime(): EpochTime

    /** Pause the current Chat and widget sessions. This generally happens when ads are presented */
    fun pause()

    /** Resume the current Chat and widget sessions. This generally happens when ads are completed */
    fun resume()

    /** Closes the current session.*/
    fun close()

    /** The current chat room */
    var getCurrentChatRoom: () -> String

    /**
     * To connect to the chatRoom with provided chatRoomId, by default it will load initial messages
     */
    fun connectToChatRoom(
        chatRoomId: String,
        callback: com.livelike.common.LiveLikeCallback<Unit>? = null,
        callbackWithSeverity: LiveLikeCallbackWithSeverity<Unit>? = null
    )

    fun connectToChatRoom(chatRoomId: String)


    /** Returns the number of messages published on a chatroom since a given time*/
    fun getMessageCount(startTimestamp: Long, callback: com.livelike.common.LiveLikeCallback<Byte>)


    /** Register a message count listner for the specified Chat Room */
    fun setMessageListener(messageListener: MessageListener?)

    fun setMessageWithReactionListener(messageWithReactionListener: MessageWithReactionListener?)

    /** Register a chatRoom listener for the specified Chat Room */
    fun setChatRoomListener(chatRoomListener: ChatRoomListener?)

    /** Set the value of visibility of chat avatar **//*
    var shouldDisplayAvatar: Boolean
*/
    /** Avatar Image Url  **/
    var avatarUrl: String?

    /**
     * send Chat Message to the current ChatRoom
     *
     * @message : text message
     * @imageUrl: image message
     * @imageWidth: image width default is 100, if value is not null then the original width of image will not set
     * @imageHeight: image height default is 100, f value is not null then the original height of image will not set
     * @liveLikePreCallback : callback to provide the message object, this callback is not meant the message is sent, this when you want to add the message to message list before the message is sent/delivered
     * you can use clientMessageId to compare and update the message object
     * Note: For the very first for every message livelikeCallback return the ChatMessage object which contains the data added by the user,
     * then the #messageListener will recieve the same chatMessage with uploaded url and timetoken updated ,you can check it with the id in #ChatMessage
     *
     * **/
    fun sendMessage(
        message: String?,
        parentMessageId: String? = null,
        imageUrl: String? = null,
        imageWidth: Int? = null,
        imageHeight: Int? = null,
        messageMetadata: Map<String, Any?>? = null,
        liveLikePreCallback: com.livelike.common.LiveLikeCallback<LiveLikeChatMessage>,
        chatInterceptor: ChatInterceptor? = null,
        callback: com.livelike.common.LiveLikeCallback<LiveLikeChatMessage>? = null,
    )

    /**
     * Quotes a chat message with the provided details and sends it to the chat.
     *
     * @param message The text message to include in the quoted message. Can be null.
     * @param imageUrl The URL of an image to attach to the quoted message. Defaults to null.
     * @param imageWidth The width of the attached image. Can be null.
     * @param imageHeight The height of the attached image. Can be null.
     * @param quoteMessageId The unique identifier for the quoted message.
     * @param quoteMessage The original chat message to be quoted.
     * @param messageMetadata Additional metadata associated with the quoted message. Defaults to null.
     * @param liveLikePreCallback A callback to handle the result of quoting the message.
     * @param chatInterceptor An optional chat interceptor to apply before sending the quoted message. Defaults to null.
     */
    fun quoteMessage(
        message: String?,
        parentMessageId: String? = null,
        imageUrl: String? = null,
        imageWidth: Int?,
        imageHeight: Int?,
        quoteMessageId: String,
        quoteMessage: LiveLikeChatMessage,
        messageMetadata: Map<String, Any?>? = null,
        liveLikePreCallback: com.livelike.common.LiveLikeCallback<LiveLikeChatMessage>,
        chatInterceptor: ChatInterceptor? = null,
        callback: com.livelike.common.LiveLikeCallback<LiveLikeChatMessage>? = null,
    )


    /**
     * to get chat messages
     * @limit: default is 20,max is 100
     * @parentMessageId : id if the message whose replies need to be fetched
     */
    fun getChatMessageReplies(
        getChatMessageRepliesRequestOption: GetChatMessageRepliesRequestOption? = null,
        callback: com.livelike.common.LiveLikeCallback<List<LiveLikeChatMessage>>? = null)

    /**
     * to load Chat History
     * @limit: default is 20,max is 100
     */
    fun loadNextHistory(limit: Int = 20,
                        sinceMessageId:String? = null,
                        callbackWithSeverity: LiveLikeCallbackWithSeverity<List<LiveLikeChatMessage>>? = null)

    /**
     * To get the loaded message
     */
    fun getLoadedMessages(): ArrayList<LiveLikeChatMessage>

    /**
     * to get the deleted messages from the loaded message
     */
    fun getDeletedMessages(): ArrayList<String>

    /**
     * to clear chat messages
     */
    fun clearMessages()

    /**
     * send Custom Chat Message
     * @customData : json String data
     * @liveLikeCallback : callback to provide the message object
     */
    fun sendCustomChatMessage(
        customData: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeChatMessage>
    )


    /**
     * Deletes a chat message with the specified message ID.
     *
     * @param messageId The unique identifier of the message to be deleted.
     * @param liveLikeCallback A callback to handle the result of the delete operation,
     *                        which typically returns an empty response.
     */
    fun deleteMessage(
        messageId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    )


    /**
     * to get all the available sticker packs
     */
    fun getStickerPacks(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<StickerPack>>)


    /**
     * to get all the available reactions for use in chat
     */
    fun getReactions(liveLikeCallback: com.livelike.common.LiveLikeCallback<List<Reaction>>)


    /**
     * Reports a message
     *  @LiveLikeChatMessage : Chat message data which needs to be reported
     */
    fun reportMessage(
        message: LiveLikeChatMessage,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    )

    /**
     * Reports a message
     *  @LiveLikeChatMessage : Chat message data which needs to be reported
     */
    fun reportMessage(
        messageId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    )


    /**
     *  adds a reaction to a particular message.This supports multiple reaction also per user
     *  @messageId : Message Id of the message, on which reaction is to be added
     *  @reactionId : Reaction Id of the reaction to be added
     */
    fun sendMessageReaction(
        messageId: String,
        reactionId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    )


    /**
     *  removes a reaction from a particular message.
     *  @messageId : Message Id of the message, from which reaction  is to be removed
     *  @reactionId : Reaction Id of the reaction to be removed
     */
    fun removeMessageReactions(
        messageId: String,
        reactionId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    )

    /**
     * Checks whether the system is currently receiving real-time updates.
     *
     * @return `true` if the system is receiving real-time updates, otherwise `false`.
     */
    fun isReceivingRealtimeUpdates(): Boolean
}

typealias ChatInterceptor = (LiveLikeChatMessage) -> Boolean