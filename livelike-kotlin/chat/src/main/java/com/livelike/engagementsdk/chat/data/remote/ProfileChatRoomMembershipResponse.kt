package com.livelike.engagementsdk.chat.data.remote

import com.google.gson.annotations.SerializedName
import com.livelike.engagementsdk.LiveLikeUser

internal data class ProfileChatRoomMembershipResponse (
    @field:SerializedName("id")
    val id: String,
    @field:SerializedName("chat_room")
    val chatRoom: ChatRoom,
    @field:SerializedName("url")
    val url: String,
    @field:SerializedName("created_at")
    val createdAt: String,
    @field:SerializedName("profile")
    val profile: LiveLikeUser? = null,
    @field:SerializedName("added_by")
    val addedBy: LiveLikeUser? = null,
)
