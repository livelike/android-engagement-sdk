package com.livelike.engagementsdk.chat.data.remote

import com.google.gson.annotations.SerializedName

internal data class ProfileChatRoomListItem(
    @field:SerializedName("id")
    val id: String,
    @field:SerializedName("chat_room")
    val chatRoom: ChatRoom,
    @field:SerializedName("url")
    val url: String
)
