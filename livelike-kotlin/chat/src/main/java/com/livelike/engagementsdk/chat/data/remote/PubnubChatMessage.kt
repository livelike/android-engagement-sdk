package com.livelike.engagementsdk.chat.data.remote

import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName

internal data class PubnubChatMessage(

    @SerializedName("id")
    val messageId: String?,
    @SerializedName("message")
    val message: String?,
    @SerializedName("sender_id")
    val senderId: String,
    @SerializedName("sender_image_url")
    val senderImageUrl: String?,
    @SerializedName("sender_nickname")
    val senderNickname: String,
    @SerializedName("program_date_time")
    val programDateTime: String?,
    @SerializedName("messageToken")
    val messageToken: Long? = null,
    @SerializedName("image_url")
    val imageUrl: String?,
    @SerializedName("image_width")
    val imageWidth: Int?,
    @SerializedName("image_height")
    val imageHeight: Int?,
    @SerializedName("custom_data")
    val customData: String?,
    @SerializedName("message_metadata")
    val messageMetadata: JsonElement? = null,
    @SerializedName("quote_message")
    val quoteMessage: PubnubQuoteChatMessage? = null,
    @SerializedName("quote_message_id")
    val quoteMessageId: String? = null,
    @SerializedName("client_message_id")
    val clientMessageId: String? = null,
    @SerializedName("created_at")
    val createdAt: String? = null,
    @SerializedName("message_event")
    var messageEvent: String? = null,
    @SerializedName("chat_room_id")
    var chatRoomId: String? = null,
    @SerializedName("pubnub_timetoken")
    var pubnubTimeToken: String? = null,
    @SerializedName("reactions")
    var reactions: Map<String, List<PubnubChatReaction>>? = null,
    @SerializedName("content_filter")
    var contentFilter: List<String>? = null,
    @SerializedName("filtered_message")
    val filteredMessage: String?,
    @SerializedName("parent_message_id")
    var parentMessageId:String? = null,
    @SerializedName("replies_url")
    var repliesUrl: String? = null,
    @SerializedName("replies_count")
    var repliesCount: Int = 0

)

internal data class PubnubChatReaction(
    val uuid: String? = null,
    @SerializedName("actionTimetoken")
    val actionTimeToken: String? = null,
    @SerializedName("user_reaction_id")
    val userReactionId: String? = null
)

internal data class PubnubQuoteChatMessage(
    @SerializedName("id")
    val messageId: String?,
    @SerializedName("message")
    val message: String?,
    @SerializedName("sender_id")
    val senderId: String?,
    @SerializedName("sender_image_url")
    val senderImageUrl: String?,
    @SerializedName("sender_nickname")
    val senderNickname: String?,
    @SerializedName("program_date_time")
    val programDateTime: String?,
    @SerializedName("messageToken")
    val messageToken: Long? = null,
    @SerializedName("image_url")
    val imageUrl: String?,
    @SerializedName("image_width")
    val imageWidth: Int?,
    @SerializedName("image_height")
    val imageHeight: Int?,
    @SerializedName("custom_data")
    val customData: String?,
    @SerializedName("message_metadata")
    val messageMetadata: JsonElement? = null,
    @SerializedName("client_message_id")
    val clientMessageId: String? = null,
    @SerializedName("created_at")
    val createdAt: String? = null,
    @SerializedName("message_event")
    var messageEvent: String? = null,
    @SerializedName("chat_room_id")
    var chatRoomId: String? = null,
    @SerializedName("pubnub_timetoken")
    var pubnubTimeToken: Long? = null,
    @SerializedName("content_filter")
    var contentFilter: List<String>? = null,
    @SerializedName("filtered_message")
    val filteredMessage: String?,
    @SerializedName("parent_message_id")
    var parentMessageId:String? = null,
    @SerializedName("replies_url")
    var repliesUrl: String? = null,
    @SerializedName("replies_count")
    var repliesCount: Int = 0
)