package com.livelike.engagementsdk.publicapis

import com.google.gson.annotations.SerializedName

data class SmartContractDetails(
    @SerializedName("network_type") var networkType: String,
    @SerializedName("contract_name") var contractName: String? = null,
    @SerializedName("contract_address") var contractAddress: String
)
