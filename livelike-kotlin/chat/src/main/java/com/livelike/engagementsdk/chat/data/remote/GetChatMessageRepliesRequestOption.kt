package com.livelike.engagementsdk.chat.data.remote


data class GetChatMessageRepliesRequestOption(
    val limit: Int = 20,
    val parentMessageId: String
)
