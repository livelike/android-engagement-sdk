package com.livelike.engagementsdk.chat.data.remote

enum class LiveLikeOrdering {
    ASC, DESC
}