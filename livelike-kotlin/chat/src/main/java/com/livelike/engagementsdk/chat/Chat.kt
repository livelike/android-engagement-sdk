package com.livelike.engagementsdk.chat


import com.google.gson.annotations.SerializedName
import com.livelike.engagementsdk.chat.data.remote.ChatRoom
import com.livelike.engagementsdk.chat.data.remote.ChatRoomTokenGate
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage
import kotlinx.coroutines.flow.MutableStateFlow


interface ChatRenderer {
    fun displayChatMessage(message: LiveLikeChatMessage, currentProfileId: String)

    /**
     * called whenever messages are loaded from history call or at fist time load call
     **/
    fun displayChatMessages(messages: List<LiveLikeChatMessage>, currentProfileId: String)
    fun deleteChatMessage(messageId: String)

    fun loadingCompleted()
    fun addMessageReaction(
        messagePubnubToken: Long?,
        messageId: String?,
        chatMessageReaction: ChatMessageReaction
    )

    fun removeMessageReaction(
        messagePubnubToken: Long?,
        messageId: String?,
        emojiId: String,
        profileId: String?
    )

    fun errorSendingMessage(error: com.livelike.common.clients.Error)
}

data class ChatMessageReaction(
    val emojiId: String,
    var pubnubActionToken: Long? = null,
    var userReactionId: String?,
    var userId: String?
)

typealias ChatRoomInfo = ChatRoom

enum class Visibility { everyone, members }

const val CHAT_MESSAGE_IMAGE_TEMPLATE = ":message:"

internal data class ChatRoomRequest(
    @SerializedName("title")
    var title: String?,
    @SerializedName("visibility")
    var visibility: Visibility?,
    @SerializedName("token_gates")
    var tokenGates:List<ChatRoomTokenGate>? = null)

data class ImageSize(val height: Int, val width: Int)

abstract class DialogInterceptor {
    fun success() {
        dialogEvent.value = true
    }

    abstract fun dialogToShow(message: LiveLikeChatMessage)
    val dialogEvent = MutableStateFlow(false)
}