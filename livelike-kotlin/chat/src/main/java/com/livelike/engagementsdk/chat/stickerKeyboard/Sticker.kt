package com.livelike.engagementsdk.chat.stickerKeyboard

data class Sticker(val file: String, val shortcode: String, var chatRoomId: String)
