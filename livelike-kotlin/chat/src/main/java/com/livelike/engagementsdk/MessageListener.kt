package com.livelike.engagementsdk

import com.livelike.engagementsdk.chat.data.remote.PinMessageInfo
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage

/**
 * Returns the new message count whenever a unread message is being posted
 *
 */
/**
 * Interface for receiving chat message and event notifications.
 */
interface MessageListener {
    /**
     * Called when a new chat message is received.
     *
     * @param message The new chat message.
     */
    fun onNewMessage(message: LiveLikeChatMessage)

    /**
     * Called when a batch of historical chat messages is received.
     *
     * @param messages The list of historical chat messages.
     */
    fun onHistoryMessage(messages: List<LiveLikeChatMessage>)

    /**
     * Called when a chat message is deleted.
     *
     * @param messageId The unique identifier of the deleted message.
     */
    fun onDeleteMessage(messageId: String)

    /**
     * Called when a chat message is pinned.
     *
     * @param message The pinned message information.
     */
    fun onPinMessage(message: PinMessageInfo)

    /**
     * Called when a pinned chat message is unpinned.
     *
     * @param pinMessageId The unique identifier of the unpinned message.
     */
    fun onUnPinMessage(pinMessageId: String)

    /**
     * Called when an error related to chat messages is encountered.
     *
     * @param error The error message.
     * @param clientMessageId The optional client message identifier associated with the error.
     */
    fun onErrorMessage(error: String, clientMessageId: String?)
}