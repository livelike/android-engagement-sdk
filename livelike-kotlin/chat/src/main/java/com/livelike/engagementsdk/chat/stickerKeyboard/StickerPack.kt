package com.livelike.engagementsdk.chat.stickerKeyboard

data class StickerPack(val name: String, val file: String, val stickers: List<Sticker>)