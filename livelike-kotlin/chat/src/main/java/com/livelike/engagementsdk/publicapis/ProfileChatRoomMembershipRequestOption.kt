package com.livelike.engagementsdk.publicapis

import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination

data class ProfileChatRoomMembershipRequestOption(
    val profileIds: List<String>? = null,
    val chatRoomId:String? = null,
    val liveLikePagination: LiveLikePagination
)
