package com.livelike.engagementsdk.publicapis


import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination

data class ChatRoomRequestOptions (
    val chatRoomId: String,
    val profileIds:  List<String>? = null,
    val liveLikePagination: LiveLikePagination
)
