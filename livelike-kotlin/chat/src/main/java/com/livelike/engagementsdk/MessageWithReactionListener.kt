package com.livelike.engagementsdk

import com.livelike.engagementsdk.chat.ChatMessageReaction

/**
 * Interface for receiving chat message and event notifications, including message reactions.
 */
interface MessageWithReactionListener : MessageListener {
    /**
     * Called when a user adds a reaction to a chat message.
     *
     * @param messagePubnubToken The PubNub token associated with the message (if available).
     * @param messageId The unique identifier of the message to which the reaction is added.
     * @param chatMessageReaction The reaction added to the message.
     */
    fun addMessageReaction(
        messagePubnubToken: Long?,
        messageId: String?,
        chatMessageReaction: ChatMessageReaction
    )

    /**
     * Called when a user removes a reaction from a chat message.
     *
     * @param messagePubnubToken The PubNub token associated with the message (if available).
     * @param messageId The unique identifier of the message from which the reaction is removed.
     * @param emojiId The unique identifier of the reaction emoji being removed.
     * @param userId The unique identifier of the user whose reaction is being removed (if available).
     */
    fun removeMessageReaction(
        messagePubnubToken: Long?,
        messageId: String?,
        emojiId: String,
        userId: String?
    )
}
