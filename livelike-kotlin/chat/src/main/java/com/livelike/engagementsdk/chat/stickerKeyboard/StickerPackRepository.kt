package com.livelike.engagementsdk.chat.stickerKeyboard


import com.livelike.network.NetworkApiClient
import com.livelike.serialization.processResult
import com.livelike.utils.PaginationResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class StickerPackRepository(
    val chatRoomId: String,
    private val endpoint: String,
    private val networkApiClient: NetworkApiClient
) {
    private var stickerPackList: List<StickerPack>? = null

    suspend fun getStickerPacks(): List<StickerPack> {
        return withContext(Dispatchers.IO) {
            if (stickerPackList == null) {
                stickerPackList = try {
                    networkApiClient.get(endpoint)
                        .processResult<PaginationResponse<StickerPack>>()
                        .run {
                            return@run results.onEach {
                                for (st in it.stickers) {
                                    st.chatRoomId = chatRoomId
                                }
                            }
                        }
                } catch (_: Exception) {
                    listOf()
                }
            }
            return@withContext stickerPackList as List<StickerPack>
        }
    }

}
