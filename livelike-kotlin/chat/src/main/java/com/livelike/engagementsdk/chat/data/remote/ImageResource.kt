package com.livelike.engagementsdk.chat.data.remote

data class ImageResource(val id: String, val image_url: String)