package com.livelike.engagementsdk.chat.data.remote

import com.google.gson.annotations.SerializedName
import com.livelike.chat.utils.CHAT_PROVIDER
import com.livelike.engagementsdk.chat.Visibility


/**
 * Chat Rooms are abstraction over the chat providers in our infra
 **/
data class ChatRoom(
    @SerializedName("channels") internal val channels: Channels,
    @SerializedName("client_id") val clientId: String,
    @SerializedName("created_at")
    internal val createdAt: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    internal val url: String,
    @SerializedName("upload_url")
    internal val uploadUrl: String,
    @SerializedName("title")
    val title: String?,
    @SerializedName("content_filter")
    val contentFilter: ContentFilter?,
    @SerializedName("report_message_url")
    internal val reportMessageUrl: String,
    @SerializedName("reported_messages_url")
    internal val reportedMessagesUrl: String,
    @SerializedName("memberships_url")
    internal val membershipsUrl: String,
    @SerializedName("sticker_packs_url")
    internal val stickerPacksUrl: String,
    @SerializedName("reaction_packs_url")
    internal val reactionPacksUrl: String,
    @SerializedName("visibility")
    val visibility: Visibility? = null,
    @SerializedName("muted_status_url_template")
    internal val mutedStatusUrlTemplate: String,
    @SerializedName("custom_data")
    val customData: String? = null,
    @SerializedName("custom_messages_url")
    internal val customMessagesUrl: String,
    @SerializedName("sponsors_url") var sponsorsUrl: String,
    @SerializedName("chatroom_messages_url")
    internal val chatroomMessageUrl: String,
    @SerializedName("chatroom_messages_count_url")
    internal val chatroomMessagesCountUrl: String,
    @SerializedName("token_gates")
    val tokenGates: List<ChatRoomTokenGate>? = null,
    @SerializedName("delete_message_url")
    internal val deleteMessageUrl: String,
    @SerializedName("chatroom_token_gate_access_url")
    val chatroomTokenGateAccessUrl: String,
    @SerializedName("chatroom_token_gate_access_url_template")
    val chatroomTokenGateAccessUrlTemplate: String,
    @SerializedName("reaction_space_id")
    val reactionSpaceId: String? = null
) {

    fun getChatChannel(): String? {
        return channels.chat[CHAT_PROVIDER]
    }

    fun getControlChannel(): String? {
        return channels.control[CHAT_PROVIDER]
    }
}

enum class ContentFilter {
    filtered, none, producer
}

data class ChatRoomTokenGate(
    @SerializedName("contract_address")
    val contractAddress: String? = null,
    @SerializedName("network_type")
    val networkType: Networks = Networks.ethereum,
    @SerializedName("token_type")
    val tokenType: TokenTypes = TokenTypes.fungible,
    @SerializedName("attributes")
    val attributes: ArrayList<Attributes>? = null
)

data class Attributes(
    @SerializedName("trait_type")
    val traitType: String? = null,
    @SerializedName("value")
    val value: String? = null
)

enum class Networks {
    ethereum,
    polygon,
    chiliz,
    hedera
}

enum class TokenTypes(val value: String?) {
    @SerializedName("non-fungible")
    nonfungible("non-fungible"),
    @SerializedName("fungible")
    fungible("fungible")
}

data class Channels(
    @SerializedName("chat")
    val chat: Map<String, String>,
    @SerializedName("reactions")
    val reactions: Map<String, String>,
    @SerializedName("control")
    val control: Map<String, String>
)

