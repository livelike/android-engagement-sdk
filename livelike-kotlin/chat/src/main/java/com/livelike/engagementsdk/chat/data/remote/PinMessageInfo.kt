package com.livelike.engagementsdk.chat.data.remote

import com.google.gson.annotations.SerializedName
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage


data class PinMessageInfo(
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    internal val url: String,
    @SerializedName("message_id")
    val messageId: String,
    @SerializedName("message_payload")
    val messagePayload: LiveLikeChatMessage,
    @SerializedName("chat_room_id")
    val chatRoomId: String,
    @SerializedName("pinned_by_id")
    val pinnedById: String

)