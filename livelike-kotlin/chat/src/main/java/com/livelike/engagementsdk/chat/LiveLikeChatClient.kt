package com.livelike.engagementsdk.chat


import com.livelike.common.model.SdkConfiguration
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.data.remote.*
import com.livelike.engagementsdk.publicapis.*
import com.livelike.network.NetworkApiClient
import com.livelike.utils.Once
import kotlinx.coroutines.CoroutineScope

interface LiveLikeChatClient {

    var chatRoomDelegate: ChatRoomDelegate?

    /**
     * Creates a new chat room with the specified parameters.
     *
     * @param title The title or name of the chat room. Can be null.
     * @param visibility The visibility setting for the chat room (e.g., public, private, etc.). Can be null.
     * @param tokenGates A list of token gates that control access to the chat room. Can be null.
     * @param liveLikeCallback A callback to handle the result of the chat room creation,
     *                        typically returning the created chat room object.
     */
    fun createChatRoom(
        title: String? = null,
        visibility: Visibility? = null,
        tokenGates: List<ChatRoomTokenGate>? = null,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ChatRoom>,
    )



    /**
     * Updates an existing chat room with the specified parameters.
     *
     * @param chatRoomId The unique identifier of the chat room to be updated.
     * @param title The new title or name of the chat room. Can be null to keep the current title.
     * @param visibility The updated visibility setting for the chat room (e.g., public, private, etc.). Can be null to keep the current visibility.
     * @param liveLikeCallback A callback to handle the result of the chat room update operation.
     */
    fun updateChatRoom(
        chatRoomId: String,
        title: String? = null,
        visibility: Visibility? = null,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ChatRoom>
    )



    /**
     * Retrieves a chat room with the specified unique identifier.
     *
     * @param id The unique identifier of the chat room to retrieve.
     * @param liveLikeCallback A callback to handle the result of the chat room retrieval operation.
     */
    fun getChatRoom(
        id: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ChatRoom>
    )





    /**
     * Adds the current user's profile to the specified chat room, granting membership.
     *
     * @param chatRoomId The unique identifier of the chat room where the user's profile will be added.
     * @param liveLikeCallback A callback to handle the result of adding the user's profile to the chat room,
     *                        typically returning the updated chat room membership.
     */
    fun addCurrentProfileToChatRoom(
        chatRoomId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ChatRoomMembership>
    )

    /**
     * Adds a user profile to the specified chat room, granting membership.
     *
     * @param chatRoomId The unique identifier of the chat room where the user's profile will be added.
     * @param profileId The unique identifier of the user profile to be added to the chat room.
     * @param liveLikeCallback A callback to handle the result of adding the user's profile to the chat room,
     *                        typically returning the updated chat room membership.
     */
    fun addProfileToChatRoom(
        chatRoomId: String,
        profileId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ChatRoomMembership>
    )



    /**
     * Retrieves a list of chat rooms associated with the current user's profile.
     *
     * @param liveLikePagination Pagination parameters to control the result set.
     * @param liveLikeCallback A callback to handle the result of retrieving the list of chat rooms,
     *                        typically returning a list of chat rooms associated with the current user's profile.
     */
    fun getCurrentProfileChatRoomList(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ChatRoom>>
    )



    /**
     * Retrieves a list of members (profiles) in the specified chat room.
     *
     * @param chatRoomId The unique identifier of the chat room to retrieve members from.
     * @param liveLikePagination Pagination parameters to control the result set.
     * @param liveLikeCallback A callback to handle the result of retrieving the list of members,
     *                        typically returning a list of profiles that are members of the chat room.
     */
    fun getMembersOfChatRoom(
        chatRoomId: String,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<LiveLikeProfile>>
    )



    /**
     * Fetches chat room membership, provided chat room id
     *   @param chatRoomId chat room id for which membership is required
     *   @param profileIds List of profiles sharing the common chat room membership
     */
    fun getChatRoomMemberships(
        chatRoomRequestOptions: ChatRoomRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<LiveLikeProfile>>
    )

    /**
     *   Fetches chat room membership for a single or multiple profiles
     *   @param profileIds List of profiles sharing the common chat room membership
     */
    fun getProfileChatRoomMemberships(
        profileChatRoomMembershipRequestOption: ProfileChatRoomMembershipRequestOption,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ChatRoom>>
    )

    /**
     * Removes the current user's profile from the specified chat room, revoking membership.
     *
     * @param chatRoomId The unique identifier of the chat room from which the user's profile will be removed.
     * @param liveLikeCallback A callback to handle the result of removing the user's profile from the chat room,
     *                        typically returning an empty response indicating success.
     */
    fun deleteCurrentProfileFromChatRoom(
        chatRoomId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    )


    /**
     * Sends a chat room invitation to a specific user profile for the specified chat room.
     *
     * @param chatRoomId The unique identifier of the chat room for which the invitation is being sent.
     * @param profileId The unique identifier of the user profile to whom the invitation is being sent.
     * @param liveLikeCallback A callback to handle the result of sending the chat room invitation,
     *                        typically returning the created chat room invitation.
     */
    fun sendChatRoomInviteToProfile(
        chatRoomId: String,
        profileId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ChatRoomInvitation>
    )



    /**
     * Updates the status of a chat room invitation with the specified new status.
     *
     * @param chatRoomInvitation The chat room invitation to be updated.
     * @param invitationStatus The new status for the chat room invitation.
     * @param liveLikeCallback A callback to handle the result of updating the chat room invitation status,
     *                        typically returning the updated chat room invitation with the new status.
     */
    fun updateChatRoomInviteStatus(
        chatRoomInvitation: ChatRoomInvitation,
        invitationStatus: ChatRoomInvitationStatus,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ChatRoomInvitation>
    )



    /**
     * Retrieves a list of chat room invitations received by the current user's profile
     * with the specified invitation status.
     *
     * @param liveLikePagination Pagination parameters to control the result set.
     * @param invitationStatus The specific invitation status to filter the invitations.
     * @param liveLikeCallback A callback to handle the result of retrieving the list of invitations,
     *                        typically returning a list of chat room invitations with the specified status.
     */
    fun getInvitationsReceivedByCurrentProfileWithInvitationStatus(
        liveLikePagination: LiveLikePagination,
        invitationStatus: ChatRoomInvitationStatus,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ChatRoomInvitation>>
    )

    /**
     * Retrieves a list of chat room invitations sent by the current user's profile
     * with the specified invitation status.
     *
     * @param liveLikePagination Pagination parameters to control the result set.
     * @param invitationStatus The specific invitation status to filter the invitations.
     * @param liveLikeCallback A callback to handle the result of retrieving the list of invitations,
     *                        typically returning a list of chat room invitations with the specified status.
     */
    fun getInvitationsSentByCurrentProfileWithInvitationStatus(
        liveLikePagination: LiveLikePagination,
        invitationStatus: ChatRoomInvitationStatus,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ChatRoomInvitation>>
    )

    /**
     * Retrieves the muted status of the current user's profile for the specified chat room.
     *
     * @param chatRoomId The unique identifier of the chat room for which the mute status is being retrieved.
     * @param liveLikeCallback A callback to handle the result of retrieving the chat profile's mute status
     *                        for the specified chat room.
     */
    fun getProfileMutedStatus(
        chatRoomId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ChatProfileMuteStatus>
    )
    /**
     * Pins a specific chat message within a chat room, making it a pinned message.
     *
     * @param messageId The unique identifier of the message to be pinned.
     * @param chatRoomId The unique identifier of the chat room in which the message is to be pinned.
     * @param chatMessagePayload The chat message to be pinned.
     * @param liveLikeCallback A callback to handle the result of pinning the message,
     *                        typically returning information about the pinned message.
     */
    fun pinMessage(
        messageId: String,
        chatRoomId: String,
        chatMessagePayload: LiveLikeChatMessage,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<PinMessageInfo>
    )


    /**
     * Unpins a previously pinned chat message with the specified PinMessageInfo identifier.
     *
     * @param pinMessageInfoId The unique identifier of the PinMessageInfo associated with the pinned message.
     * @param liveLikeCallback A callback to handle the result of unpinning the message,
     *                        typically returning an empty response to indicate success.
     */
    fun unPinMessage(
        pinMessageInfoId: String,
        liveLiveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    )

    /**
     * Retrieves a list of pinned messages (PinMessageInfo) in the specified chat room.
     *
     * @param chatRoomId The unique identifier of the chat room from which to retrieve pinned messages.
     * @param order The ordering of the pinned messages (e.g., ascending, descending).
     * @param pagination Pagination parameters to control the result set.
     * @param liveLikeCallback A callback to handle the result of retrieving the list of pinned messages,
     *                        typically returning a list of PinMessageInfo objects.
     */
    fun getPinMessageInfoList(
        chatRoomId: String,
        order: LiveLikeOrdering,
        pagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<PinMessageInfo>>
    )


    /**
     *  Checks if the wallet passes the required token gating criteria
     *  @param walletAddress The web3 wallet address
     *  @param chatRoomId The ChatRoom id
     */
    fun getTokenGatedChatRoomAccessDetails(
        walletAddress: String,
        chatRoomId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<TokenGatingStatus>
    )



    /**
     * Retrieves a list of smart contracts with their details.
     *
     * @param liveLikePagination Pagination parameters to control the result set.
     * @param liveLikeCallback A callback to handle the result of retrieving the list of smart contracts
     *                        along with their details.
     */
    fun getSmartContracts(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<SmartContractDetails>>
    )



    companion object {

        fun getInstance(
            configurationOnce: Once<SdkConfiguration>,
            uiScope: CoroutineScope,
            sdkScope: CoroutineScope,
            networkApiClient: NetworkApiClient,
            profileOnce: Once<LiveLikeProfile>,
        ): LiveLikeChatClient = InternalLiveLikeChatClient(
            configurationOnce,
            uiScope,
            sdkScope,
            networkApiClient,
            profileOnce
        )
    }

}