package com.livelike.engagementsdk.chat.data.remote

import com.google.gson.annotations.SerializedName
import com.livelike.common.model.PubnubChatEventType
import com.livelike.engagementsdk.chat.ChatMessageReaction
import com.livelike.engagementsdk.chat.data.toChatMessage
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage

internal data class PubnubPinMessageInfo(
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    val url: String,
    @SerializedName("message_id")
    val messageId: String,
    @SerializedName("message_payload")
    val messagePayload: PubnubChatMessage,
    @SerializedName("chat_room_id")
    val chatRoomId: String,
    @SerializedName("pinned_by_id")
    val pinnedById: String
)


internal fun PubnubPinMessageInfo.toPinMessageInfo(): PinMessageInfo {
   val messageDetails = if (messagePayload == null) {
       LiveLikeChatMessage("",messageEvent =  PubnubChatEventType.MESSAGE_CREATED)
   } else {
       messagePayload.toChatMessage(
           0L,
           processReactionCounts(messagePayload.reactions),
           PubnubChatEventType.MESSAGE_CREATED
       )
   }

    val pinMessageInfo = if (messagePayload == null) {
        PinMessageInfo(id,"","",messageDetails,"","")
    }else{
        PinMessageInfo(
            id,
            url,
            messageId,
            messageDetails,
            chatRoomId, pinnedById
        )
    }
    return pinMessageInfo
}


private fun processReactionCounts(actions: Map<String, List<PubnubChatReaction>>?): MutableMap<String, List<ChatMessageReaction>> {
    val reactionCountMap = mutableMapOf<String, List<ChatMessageReaction>>()
    actions?.let {
        for (value in actions.keys) {
            val reactions = actions[value]?.map {
                ChatMessageReaction(
                    value, it.actionTimeToken?.toLongOrNull(), it.userReactionId, it.uuid
                )
            } ?: emptyList()
            reactionCountMap[value] = reactions
        }
    } ?: println("actions are null")
    return reactionCountMap
}