package com.livelike.engagementsdk.publicapis

abstract class ChatRoomDelegate {
    abstract fun onNewChatRoomAdded(chatRoomAdd: ChatRoomAdd)
    abstract fun onReceiveInvitation(invitation: ChatRoomInvitation)
    abstract fun onBlockProfile(blockedInfo: BlockedInfo)
    abstract fun onUnBlockProfile(blockInfoId: String, blockProfileId: String)
}