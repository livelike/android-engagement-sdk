package com.livelike.engagementsdk.chat

import com.google.gson.reflect.TypeToken
import com.livelike.BaseClient
import com.livelike.chat.utils.CHATROOM_NOT_TOKEN_GATED
import com.livelike.chat.utils.CHAT_ROOM_ID_EMPTY
import com.livelike.chat.utils.INTEGRATOR_USED_CHATROOM_DELEGATE_KEY
import com.livelike.chat.utils.TEMPLATE_INVITED_BY_ID
import com.livelike.chat.utils.TEMPLATE_PROFILE_ID
import com.livelike.chat.utils.TEMPLATE_STATUS
import com.livelike.chat.utils.TEMPLATE_WALLET_ADDRESS
import com.livelike.chat.utils.WALLET_ADDRESS_EMPTY
import com.livelike.common.model.PubnubChatEventType
import com.livelike.common.model.SdkConfiguration
import com.livelike.common.model.toPubnubChatEventType
import com.livelike.engagementsdk.LiveLikeProfile
import com.livelike.engagementsdk.chat.data.remote.ChatRoom
import com.livelike.engagementsdk.chat.data.remote.ChatRoomMembership
import com.livelike.engagementsdk.chat.data.remote.ChatRoomTokenGate
import com.livelike.engagementsdk.chat.data.remote.LiveLikeOrdering
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.chat.data.remote.PinMessageInfo
import com.livelike.engagementsdk.chat.data.remote.PinMessageInfoRequest
import com.livelike.engagementsdk.chat.data.remote.ProfileChatRoomListItem
import com.livelike.engagementsdk.chat.data.remote.ProfileChatRoomMembershipResponse
import com.livelike.engagementsdk.chat.data.remote.PubnubPinMessageInfo
import com.livelike.engagementsdk.chat.data.remote.toPinMessageInfo
import com.livelike.engagementsdk.chat.data.toPubnubChatMessage
import com.livelike.engagementsdk.publicapis.BlockedInfo
import com.livelike.engagementsdk.publicapis.ChatProfileMuteStatus
import com.livelike.engagementsdk.publicapis.ChatRoomAdd
import com.livelike.engagementsdk.publicapis.ChatRoomDelegate
import com.livelike.engagementsdk.publicapis.ChatRoomInvitation
import com.livelike.engagementsdk.publicapis.ChatRoomInvitationStatus
import com.livelike.engagementsdk.publicapis.ChatRoomRequestOptions
import com.livelike.engagementsdk.publicapis.LiveLikeChatMessage
import com.livelike.engagementsdk.publicapis.LiveLikeEmptyResponse
import com.livelike.engagementsdk.publicapis.ProfileChatRoomMembershipRequestOption
import com.livelike.engagementsdk.publicapis.SmartContractDetails
import com.livelike.engagementsdk.publicapis.TokenGatingStatus
import com.livelike.network.NetworkApiClient
import com.livelike.realtime.RealTimeMessagingClient
import com.livelike.realtime.RealTimeMessagingClientConfig
import com.livelike.serialization.gson
import com.livelike.serialization.processResult
import com.livelike.serialization.toJsonString
import com.livelike.utils.LiveLikeException
import com.livelike.utils.NO_MORE_DATA
import com.livelike.utils.Once
import com.livelike.utils.PaginationResponse
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class InternalLiveLikeChatClient(
    configurationOnce: Once<SdkConfiguration>,
    private val uiScope: CoroutineScope,
    private val sdkScope: CoroutineScope,
    private val networkApiClient: NetworkApiClient,
    currentProfileOnce: Once<LiveLikeProfile>,
) : BaseClient(configurationOnce, currentProfileOnce, sdkScope, uiScope),
    LiveLikeChatClient {
    private var profileChatRoomMembershipResponse: PaginationResponse<ProfileChatRoomMembershipResponse>? =
        null
    private var profileChatRoomListResponse: PaginationResponse<ProfileChatRoomListItem>? = null
    private var chatRoomMemberListMap: MutableMap<String, PaginationResponse<ChatRoomMembership>> =
        mutableMapOf()
    private val invitationForProfileMap =
        hashMapOf<String, PaginationResponse<ChatRoomInvitation>>()
    private val invitationByProfileMap = hashMapOf<String, PaginationResponse<ChatRoomInvitation>>()
    private var pinMessageInfoListResponse: PaginationResponse<PubnubPinMessageInfo>? = null
    private var smartContractNamesListResponse: PaginationResponse<SmartContractDetails>? = null
    private var internalChatRoomDelegate = hashMapOf<String, ChatRoomDelegate>()

    internal fun subscribeToChatRoomInternalDelegate(key: String, delegate: ChatRoomDelegate) {
        internalChatRoomDelegate[key] = delegate
        setUpPubNubClientForChatRoom()
    }

    internal fun unsubscribeToChatRoomDelegate(key: String) {
        internalChatRoomDelegate.remove(key)
    }

    override var chatRoomDelegate: ChatRoomDelegate? = null
        set(value) {
            field = value
            if (value != null) {
                subscribeToChatRoomInternalDelegate(INTEGRATOR_USED_CHATROOM_DELEGATE_KEY, value)
            } else {
                unsubscribeToChatRoomDelegate(INTEGRATOR_USED_CHATROOM_DELEGATE_KEY)
            }
        }

    override fun createChatRoom(
        title: String?,
        visibility: Visibility?,
        tokenGates: List<ChatRoomTokenGate>?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ChatRoomInfo>
    ) {
        createUpdateChatRoom(null, visibility, title, tokenGates, liveLikeCallback)
    }

    private fun setUpPubNubClientForChatRoom() {
        sdkScope.launch {
            try {
                val pair = configurationProfilePairOnce()
                pair.second.pubNubKey?.let {
                    RealTimeMessagingClient.getInstance(
                        RealTimeMessagingClientConfig(
                            it,
                            pair.first.accessToken,
                            pair.first.id,
                            pair.second.pubnubPublishKey,
                            pair.second.pubnubOrigin,
                            pair.second.pubnubHeartbeatInterval,
                            pair.second.pubnubPresenceTimeout
                        ), sdkScope
                    )
                }?.let { realTimeMessagingClient ->
                    launch {
                        realTimeMessagingClient.messageClientFlow.filter {
                            when (it.event.toPubnubChatEventType()) {
                                PubnubChatEventType.CHATROOM_ADDED, PubnubChatEventType.CHATROOM_INVITE, PubnubChatEventType.BLOCK_PROFILE, PubnubChatEventType.UNBLOCK_PROFILE -> true
                                else -> false
                            }
                        }.collect { realTimeClientMessage ->
                            when (val event = realTimeClientMessage.event.toPubnubChatEventType()) {
                                PubnubChatEventType.CHATROOM_ADDED -> {
                                    val chatRoomAdd: ChatRoomAdd = gson.fromJson(
                                        realTimeClientMessage.payload,
                                        object : TypeToken<ChatRoomAdd>() {}.type
                                    )
                                    uiScope.launch {
                                        for (it in internalChatRoomDelegate.values) {
                                            it.onNewChatRoomAdded(chatRoomAdd)
                                        }
                                    }
                                }

                                PubnubChatEventType.CHATROOM_INVITE -> {
                                    val invitation: ChatRoomInvitation = gson.fromJson(
                                        realTimeClientMessage.payload,
                                        object : TypeToken<ChatRoomInvitation>() {}.type
                                    )
                                    uiScope.launch {
                                        for (it in internalChatRoomDelegate.values) {
                                            it.onReceiveInvitation(invitation)
                                        }
                                    }
                                }

                                PubnubChatEventType.BLOCK_PROFILE -> {
                                    val blockedInfo: BlockedInfo = gson.fromJson(
                                        realTimeClientMessage.payload,
                                        object : TypeToken<BlockedInfo>() {}.type
                                    )
                                    uiScope.launch {
                                        for (it in internalChatRoomDelegate.values) {
                                            it.onBlockProfile(blockedInfo)
                                        }
                                    }
                                }

                                PubnubChatEventType.UNBLOCK_PROFILE -> {
                                    val blockedInfo: BlockedInfo = gson.fromJson(
                                        realTimeClientMessage.payload,
                                        object : TypeToken<BlockedInfo>() {}.type
                                    )
                                    uiScope.launch {
                                        for (it in internalChatRoomDelegate.values) {
                                            it.onUnBlockProfile(
                                                blockedInfo.id, blockedInfo.blockedProfileID
                                            )
                                        }
                                    }
                                }

                                else -> {
                                    logError { "Event: $event not supported" }
                                }
                            }
                        }
                    }
                    launch {
                        configurationProfilePairOnce().let { pair ->
                            realTimeMessagingClient.subscribe(arrayListOf(pair.first.subscribeChannel))
                        }
                    }
                } ?: logDebug { "Real time Messaging client is null or config pubnub key is null" }
            } catch (e: Exception) {
                e.printStackTrace()
                logError { e.message }
            }
        }
    }



    override fun updateChatRoom(
        chatRoomId: String,
        title: String?,
        visibility: Visibility?,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ChatRoomInfo>
    ) {
        createUpdateChatRoom(chatRoomId, visibility, title, liveLikeCallback = liveLikeCallback)
    }

    private fun createUpdateChatRoom(
        chatRoomId: String?,
        visibility: Visibility?,
        title: String?,
        tokenGates: List<ChatRoomTokenGate>? = null,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ChatRoomInfo>,
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            if (chatRoomId.isNullOrEmpty()) {
                networkApiClient.post(
                    pair.second.createChatRoomUrl,
                    accessToken = pair.first.accessToken,
                    body = ChatRoomRequest(title, visibility, tokenGates).toJsonString()
                )
            } else {
                networkApiClient.put(
                    pair.second.getChatRoomUrlFromTemplate(chatRoomId),
                    accessToken = pair.first.accessToken,
                    body = ChatRoomRequest(title, visibility, tokenGates).toJsonString()
                )
            }.processResult()
        }
    }


    override fun getChatRoom(
        id: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ChatRoomInfo>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            if (id.isEmpty()) {
                throw LiveLikeException(CHAT_ROOM_ID_EMPTY)
            }
            networkApiClient.get(pair.second.getChatRoomUrlFromTemplate(id)).processResult()
        }
    }


    override fun addCurrentProfileToChatRoom(
        chatRoomId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ChatRoomMembership>
    ) {
        addProfileToChatRoom(chatRoomId, "", liveLikeCallback)
    }

    override fun addProfileToChatRoom(
        chatRoomId: String,
        profileId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ChatRoomMembership>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val chatRoom = getChatRoomDetails(chatRoomId)
            networkApiClient.post(
                chatRoom.membershipsUrl, accessToken = pair.first.accessToken,
                body = when (profileId.isEmpty()) {
                    true -> mapOf()
                    else -> mapOf("profile_id" to profileId)
                }.toJsonString(),
            ).processResult()
        }
    }


    override fun getCurrentProfileChatRoomList(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ChatRoomInfo>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            when (liveLikePagination) {
                LiveLikePagination.FIRST -> pair.first.chatRoomMembershipsUrl
                LiveLikePagination.NEXT -> profileChatRoomListResponse?.next
                LiveLikePagination.PREVIOUS -> profileChatRoomListResponse?.previous
            }?.let { url ->
                networkApiClient.get(url, accessToken = pair.first.accessToken)
                    .processResult<PaginationResponse<ProfileChatRoomListItem>>()
                    .also {
                        profileChatRoomListResponse = it
                    }.results.map { it.chatRoom }
            } ?: throw LiveLikeException(NO_MORE_DATA)
        }
    }


    override fun getMembersOfChatRoom(
        chatRoomId: String,
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<LiveLikeProfile>>
    ) {
        getChatRoomMemberships(
            ChatRoomRequestOptions(
                chatRoomId,
                liveLikePagination = liveLikePagination
            ), liveLikeCallback
        )
    }



    override fun deleteCurrentProfileFromChatRoom(
        chatRoomId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val chatRoom = getChatRoomDetails(chatRoomId)
            networkApiClient.delete(
                chatRoom.membershipsUrl, accessToken = pair.first.accessToken
            ).processResult()
        }
    }

    internal suspend fun getChatRoomDetails(chatRoomId: String) = suspendCoroutine { cont ->
        getChatRoom(chatRoomId) { result, error ->
            result?.let { cont.resume(it) }
            error?.let { cont.resumeWithException(Exception(it)) }
        }
    }


    override fun getChatRoomMemberships(
        chatRoomRequestOptions: ChatRoomRequestOptions,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<LiveLikeProfile>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val url = when (chatRoomRequestOptions.liveLikePagination) {
                LiveLikePagination.FIRST -> getChatRoomDetails(chatRoomRequestOptions.chatRoomId).membershipsUrl
                LiveLikePagination.NEXT -> chatRoomMemberListMap[chatRoomRequestOptions.chatRoomId]?.next
                LiveLikePagination.PREVIOUS -> chatRoomMemberListMap[chatRoomRequestOptions.chatRoomId]?.previous
            } ?: throw Exception(NO_MORE_DATA)
            val queryParams = arrayListOf<Pair<String, Any>>()
            chatRoomRequestOptions.profileIds?.let {
                it.forEach { profileId ->
                    queryParams.add("profile_id" to profileId)
                }
            }
            val result = networkApiClient.get(
                url, pair.first.accessToken, queryParameters = queryParams
            ).processResult<PaginationResponse<ChatRoomMembership>>().also {
                chatRoomMemberListMap[chatRoomRequestOptions.chatRoomId] = it
            }
            result.results.map { it.profile }
        }
    }

    override fun getProfileChatRoomMemberships(
        profileChatRoomMembershipRequestOption: ProfileChatRoomMembershipRequestOption,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ChatRoom>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            when (profileChatRoomMembershipRequestOption.liveLikePagination) {
                LiveLikePagination.FIRST -> pair.second.chatRoomMembershipUrl
                LiveLikePagination.NEXT -> profileChatRoomMembershipResponse?.next
                LiveLikePagination.PREVIOUS -> profileChatRoomMembershipResponse?.previous
            }?.let { url ->
                val queryParams = arrayListOf<Pair<String, Any>>()
                profileChatRoomMembershipRequestOption.chatRoomId?.let { chatRoomId ->
                    queryParams.add("chat_room_id" to chatRoomId)
                }
                profileChatRoomMembershipRequestOption.profileIds?.let {
                    it.forEach { profileId ->
                        queryParams.add("profile_id" to profileId)
                    }
                }
                networkApiClient.get(url, accessToken = pair.first.accessToken, queryParams)
                    .processResult<PaginationResponse<ProfileChatRoomMembershipResponse>>()
                    .also {
                        profileChatRoomMembershipResponse = it
                    }.results.map { it.chatRoom }
            }
        }
    }



    override fun sendChatRoomInviteToProfile(
        chatRoomId: String,
        profileId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ChatRoomInvitation>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            networkApiClient.post(
                pair.second.createChatRoomInvitationUrl,
                accessToken = pair.first.accessToken,
                body = mapOf(
                    "chat_room_id" to chatRoomId, "invited_profile_id" to profileId
                ).toJsonString()
            ).processResult()
        }
    }


    override fun updateChatRoomInviteStatus(
        chatRoomInvitation: ChatRoomInvitation,
        invitationStatus: ChatRoomInvitationStatus,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ChatRoomInvitation>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            networkApiClient.patch(
                chatRoomInvitation.url,
                accessToken = pair.first.accessToken,
                body = mapOf("status" to invitationStatus.key).toJsonString()
            ).processResult()
        }
    }

    override fun getInvitationsReceivedByCurrentProfileWithInvitationStatus(
        liveLikePagination: LiveLikePagination,
        invitationStatus: ChatRoomInvitationStatus,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ChatRoomInvitation>>
    ) {
        safeCallBack(liveLikeCallback) {
            when (liveLikePagination) {
                LiveLikePagination.FIRST -> it.second.profileChatRoomReceivedInvitationsUrlTemplate.replace(
                    TEMPLATE_PROFILE_ID, it.first.id
                ).replace(TEMPLATE_STATUS, invitationStatus.key)

                LiveLikePagination.NEXT -> invitationForProfileMap[it.first.id]?.next
                LiveLikePagination.PREVIOUS -> invitationForProfileMap[it.first.id]?.previous
            }?.let { url ->
                networkApiClient.get(url, accessToken = it.first.accessToken)
                    .processResult<PaginationResponse<ChatRoomInvitation>>().also { res ->
                        invitationForProfileMap[it.first.id] = res
                    }.results
            } ?: throw LiveLikeException(NO_MORE_DATA)
        }
    }



    override fun getInvitationsSentByCurrentProfileWithInvitationStatus(
        liveLikePagination: LiveLikePagination,
        invitationStatus: ChatRoomInvitationStatus,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<ChatRoomInvitation>>
    ) {
        safeCallBack(liveLikeCallback) {
            when (liveLikePagination) {
                LiveLikePagination.FIRST -> it.second.profileChatRoomSentInvitationsUrlTemplate.replace(
                    TEMPLATE_INVITED_BY_ID, it.first.id
                ).replace(TEMPLATE_STATUS, invitationStatus.key)

                LiveLikePagination.NEXT -> invitationByProfileMap[it.first.id]?.next
                LiveLikePagination.PREVIOUS -> invitationByProfileMap[it.first.id]?.previous
            }?.let { url ->
                networkApiClient.get(url, accessToken = it.first.accessToken)
                    .processResult<PaginationResponse<ChatRoomInvitation>>().also { res ->
                        invitationByProfileMap[it.first.id] = res
                    }.results
            } ?: throw LiveLikeException(NO_MORE_DATA)
        }
    }



    override fun getProfileMutedStatus(
        chatRoomId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<ChatProfileMuteStatus>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            val chatRoom = getChatRoomDetails(chatRoomId)
            val url = chatRoom.mutedStatusUrlTemplate
            networkApiClient.get(
                url.replace(
                    TEMPLATE_PROFILE_ID, pair.first.id
                )
            ).processResult()
        }
    }



    override fun pinMessage(
        messageId: String,
        chatRoomId: String,
        chatMessagePayload: LiveLikeChatMessage,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<PinMessageInfo>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            networkApiClient.post(
                pair.second.pinnedMessageUrl,
                accessToken = pair.first.accessToken,
                body = PinMessageInfoRequest(
                    messageId, chatMessagePayload.toPubnubChatMessage(null), chatRoomId
                ).toJsonString()
            ).processResult<PubnubPinMessageInfo>().toPinMessageInfo()
        }
    }



    override fun unPinMessage(
        pinMessageInfoId: String,
        liveLiveLikeCallback: com.livelike.common.LiveLikeCallback<LiveLikeEmptyResponse>
    ) {
        safeCallBack(liveLiveLikeCallback) { pair ->
            networkApiClient.delete(
                "${pair.second.pinnedMessageUrl}$pinMessageInfoId/",
                accessToken = pair.first.accessToken
            ).processResult()
        }
    }


    override fun getPinMessageInfoList(
        chatRoomId: String,
        order: LiveLikeOrdering,
        pagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<PinMessageInfo>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            when (pagination) {
                LiveLikePagination.FIRST -> pair.second.pinnedMessageUrl
                LiveLikePagination.NEXT -> pinMessageInfoListResponse?.next
                LiveLikePagination.PREVIOUS -> pinMessageInfoListResponse?.previous
            }?.let { url ->
                val queryParams = arrayListOf<Pair<String, Any>>()
                if (pagination == LiveLikePagination.FIRST) {
                    queryParams.add("chat_room_id" to chatRoomId)
                }
                queryParams.add(
                    "ordering" to "${
                        when (order) {
                            LiveLikeOrdering.ASC -> ""
                            LiveLikeOrdering.DESC -> "-"
                        }
                    }pinned_at"
                )
                networkApiClient.get(
                    url, accessToken = pair.first.accessToken, queryParameters = queryParams
                ).processResult<PaginationResponse<PubnubPinMessageInfo>>()
                    .also {
                        pinMessageInfoListResponse = it
                    }.results.map { it.toPinMessageInfo() }
            } ?: throw LiveLikeException(NO_MORE_DATA)
        }
    }



    override fun getTokenGatedChatRoomAccessDetails(
        walletAddress: String,
        chatRoomId: String,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<TokenGatingStatus>
    ) {
        safeCallBack(liveLikeCallback) {
            val result = getChatRoomDetails(chatRoomId)
            if (result.tokenGates.isNullOrEmpty()) throw LiveLikeException(
                CHATROOM_NOT_TOKEN_GATED
            )
            if (walletAddress.isEmpty()) throw LiveLikeException(WALLET_ADDRESS_EMPTY)
            val tokenGatingStatusUrl = result.chatroomTokenGateAccessUrlTemplate.replace(
                TEMPLATE_WALLET_ADDRESS, walletAddress
            )
            networkApiClient.get(tokenGatingStatusUrl).processResult()
        }
    }



    override fun getSmartContracts(
        liveLikePagination: LiveLikePagination,
        liveLikeCallback: com.livelike.common.LiveLikeCallback<List<SmartContractDetails>>
    ) {
        safeCallBack(liveLikeCallback) { pair ->
            when (liveLikePagination) {
                LiveLikePagination.FIRST -> pair.second.savedContractAddressesUrl
                LiveLikePagination.NEXT -> smartContractNamesListResponse?.next
                LiveLikePagination.PREVIOUS -> smartContractNamesListResponse?.previous
            }?.let { url ->
                networkApiClient.get(url, accessToken = pair.first.accessToken)
                    .processResult<PaginationResponse<SmartContractDetails>>().also {
                        smartContractNamesListResponse = it
                    }.results
            } ?: throw LiveLikeException(NO_MORE_DATA)
        }
    }

}