@Suppress("DSL_SCOPE_VIOLATION")

plugins {
    `java-library`
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.detekt)
    `maven-publish`
//    alias(libs.plugins.dokka)
    `java-test-fixtures`
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

publishing {
    publications {
        // creating a release publication
        create<MavenPublication>("maven") {
            groupId = "com.livelike.android-engagement-sdk"
            artifactId = "livelike-kotlin-chat"
            version = "0.0.1"

            from(components["java"])
        }
    }
}


dependencies {
    implementation(project(":livelike-core:utils"))
    implementation(project(":livelike-core:network"))
    implementation(project(":livelike-core:serialization"))
    implementation(project(":livelike-kotlin:common"))
    implementation(project(":livelike-kotlin:reaction"))
    implementation(project(":livelike-kotlin:real-time"))

    testImplementation(testFixtures(project(":livelike-kotlin:common")))
    testImplementation(testFixtures(project(":livelike-kotlin:reaction")))
}