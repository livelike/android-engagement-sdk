package com.livelike.ui.comments

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView

internal class CustomDividerItemDecoration(context: Context, orientation: Int) :
    DividerItemDecoration(context, orientation) {

    private val dividerPaint: Paint = Paint()

    init {
        val color = ContextCompat.getColor(
            context,
            R.color.liveLike_comment_line_separator
        ) // Replace with your desired white color resource
        dividerPaint.color = color
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val left = parent.paddingLeft + 40
        val right = parent.width - parent.paddingRight - 45

        val childCount = parent.childCount
        for (i in 0 until childCount - 1) {
            val child = parent.getChildAt(i)
            val params = child.layoutParams as RecyclerView.LayoutParams
            val top = child.bottom + params.bottomMargin
            val bottom = top + drawable!!.intrinsicHeight
            c.drawRect(
                left.toFloat(),
                top.toFloat(),
                right.toFloat(),
                bottom.toFloat(),
                dividerPaint
            )
        }
    }
}
