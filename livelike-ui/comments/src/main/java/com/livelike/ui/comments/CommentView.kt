package com.livelike.ui.comments

import android.app.Activity
import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.livelike.comment.LiveLikeCommentSession
import com.livelike.comment.models.Comment
import com.livelike.comment.models.CommentSortingOptions
import com.livelike.engagementsdk.reaction.LiveLikeReactionSession
import com.livelike.ui.comments.databinding.CommentToastsBinding
import com.livelike.ui.comments.databinding.CommentViewBinding
import com.livelike.ui.comments.databinding.CustomSpinnerHeaderBinding
import com.livelike.ui.comments.databinding.CustomSpinnerItemBinding
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch


class CommentView : ConstraintLayout {

    private lateinit var binding: CommentViewBinding
    private var themeTypedArray: TypedArray? = null
    private var profileImageUrl: String? = null
    private lateinit var session: LiveLikeCommentSession
    private val uiScope = CoroutineScope(Dispatchers.Main.immediate + SupervisorJob())
    private lateinit var adapter: CommentAdapter
    private lateinit var itemDecoration: CustomDividerItemDecoration
    private var isLoading = false
    private var customTheme: TypedArray? = null
    private val drawableParentCommentTopId =
        ContextCompat.getDrawable(context, R.drawable.comment_rounded_corner)
    private val drawableReplyBox =
        ContextCompat.getDrawable(context, R.drawable.livelike_reply_round_background)
    private val deleteComment = context.getString(R.string.liveLike_delete_comment)
    private val unReportComment = context.getString(R.string.liveLike_unReport_comment)
    private val unBlockUser = context.getString(R.string.liveLike_unBlock_user)
    private val reportComment = context.getString(R.string.liveLike_report_comment)
    private val blockUser = context.getString(R.string.liveLike_block_user)

    constructor(context: Context) : super(context) {
        setupView(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setupView(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context, attrs, defStyle
    ) {
        setupView(attrs, defStyle)
    }


    private fun setupView(attrs: AttributeSet?, defStyle: Int) {
        binding = CommentViewBinding.inflate(LayoutInflater.from(context), this, true)
        itemDecoration = CustomDividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        customTheme = getCustomTheme(attrs, defStyle)
        binding.drawableVariable = drawableParentCommentTopId
        binding.replyBoxDrawableVariable = drawableReplyBox
        binding.commentCount = 0
        applyTheme(customTheme)
        loadSortingFilter(customTheme)
        themeTypedArray =
            context.obtainStyledAttributes(attrs, R.styleable.CommentView, defStyle, 0)
        profileImageUrl = themeTypedArray!!.getString(R.styleable.CommentView_profileImageUrl)

        (context as Activity).window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
    }


    private fun applyTheme(themeTypedArray: TypedArray?) {
        if (themeTypedArray == null) return

        val commentBubbleBackgroundDrawable = themeTypedArray.getDrawable(
            R.styleable.CommentViewTheme_commentBubbleBackgroundDrawable
        )
        if (commentBubbleBackgroundDrawable != null) {
            binding.drawableVariable = commentBubbleBackgroundDrawable
        } else {
            binding.drawableVariable = drawableParentCommentTopId
        }

        val replyBoxDrawable = themeTypedArray.getDrawable(
            R.styleable.CommentViewTheme_replyBoxDrawable
        )
        if (replyBoxDrawable != null) {
            binding.replyBoxDrawableVariable = replyBoxDrawable
        } else {
            binding.replyBoxDrawableVariable = drawableReplyBox
        }

        val commentBackground = themeTypedArray.getColor(
            R.styleable.CommentViewTheme_commentBackground,
            ContextCompat.getColor(context, R.color.liveLike_comment_background)
        )
        binding.liveLikeCommentBackground.setBackgroundColor(commentBackground)


        val profileNameTextColor = themeTypedArray.getColor(
            R.styleable.CommentViewTheme_profileNameParentCommentTextColor,
            ContextCompat.getColor(context, R.color.liveLike_comment_profile_text_color)
        )

        binding.commentParentContainer.includedCommentProfileHeader.tvProfileName.setTextColor(
            profileNameTextColor
        )

        val parentProfileTvTime = themeTypedArray.getColor(
            R.styleable.CommentViewTheme_profileTvParentTimeTextColor,
            ContextCompat.getColor(context, R.color.liveLike_comment_time_text_color)
        )
        binding.commentParentContainer.includedCommentProfileHeader.tvTime.setTextColor(
            parentProfileTvTime
        )

        val parentCommentTextColor = themeTypedArray.getColor(
            R.styleable.CommentViewTheme_parentCommentTextColor,
            ContextCompat.getColor(context, R.color.liveLike_comment_text_color)
        )
        binding.commentTextColor = parentCommentTextColor

        val commentTotalCountBackground = themeTypedArray.getColor(
            R.styleable.CommentViewTheme_commentTotalCountBackground,
            ContextCompat.getColor(context, R.color.liveLike_comment_count_background)
        )
        binding.commentTotalCountBackground = commentTotalCountBackground

        val commentTotalCountTextColor = themeTypedArray.getColor(
            R.styleable.CommentViewTheme_commentTotalCountTextColor,
            ContextCompat.getColor(context, R.color.liveLike_comment_count_text_color)
        )
        binding.commentTotalCountTextColor = commentTotalCountTextColor

        val parentCommentRepliesButtonColor = themeTypedArray.getColor(
            R.styleable.CommentViewTheme_parentCommentRepliesButtonColor,
            ContextCompat.getColor(context, R.color.liveLike_replies_button_text_color)
        )
        binding.commentParentContainer.repliesCount.setTextColor(parentCommentRepliesButtonColor)

        val emptyViewBackground = themeTypedArray.getColor(
            R.styleable.CommentViewTheme_emptyViewBackground,
            ContextCompat.getColor(context, R.color.liveLike_comment_empty_background_color)
        )
        binding.emptyScreenView.emptyView.setBackgroundColor(emptyViewBackground)

        val emptyViewTextColor = themeTypedArray.getColor(
            R.styleable.CommentViewTheme_emptyViewTextColor,
            ContextCompat.getColor(context, R.color.liveLike_comment_empty_background_text_color)
        )
        binding.emptyScreenView.tvEmptyView.setTextColor(emptyViewTextColor)

        val emptyViewTextColorConv = themeTypedArray.getColor(
            R.styleable.CommentViewTheme_emptyViewTextColorConv, ContextCompat.getColor(
                context, R.color.liveLike_comment_empty_background_text_color_conv
            )
        )
        binding.emptyScreenView.tvEmptyViewConv.setTextColor(emptyViewTextColorConv)

        val emptyViewImageDrawable = themeTypedArray.getDrawable(
            R.styleable.CommentViewTheme_emptyViewImageDrawable
        )

        if (emptyViewImageDrawable != null) {
            binding.emptyScreenView.emptyImageView.setImageDrawable(
                emptyViewImageDrawable
            )
        } else {
            binding.emptyScreenView.emptyImageView.setImageResource(R.drawable.empty_comments)
        }

        val replyTabLeftArrowImage = themeTypedArray.getDrawable(
            R.styleable.CommentViewTheme_replyTabLeftArrowImage
        )

        if (replyTabLeftArrowImage != null) {
            binding.replyTab.backArrowButton.setImageDrawable(
                replyTabLeftArrowImage
            )
        } else {
            binding.replyTab.backArrowButton.setImageResource(R.drawable.livelike_arrow_left)
        }

        val replyTabCrossImage = themeTypedArray.getDrawable(
            R.styleable.CommentViewTheme_replyTabCrossImage
        )

        if (replyTabCrossImage != null) {
            binding.replyTab.crossButton.setImageDrawable(
                replyTabCrossImage
            )
        } else {
            binding.replyTab.crossButton.setImageResource(R.drawable.livelike_close_cir)
        }

        val commentCountTabImage = themeTypedArray.getDrawable(
            R.styleable.CommentViewTheme_commentCountTabImage
        )

        if (commentCountTabImage != null) {
            binding.commentCountContainer.ivCommentIcon.setImageDrawable(
                commentCountTabImage
            )
        } else {
            binding.commentCountContainer.ivCommentIcon.setImageResource(R.drawable.livelike_comment_dots)
        }

        profileImageUrl = themeTypedArray.getString(
            R.styleable.CommentView_profileImageUrl
        )

        val sendViewBackground = themeTypedArray.getColor(
            R.styleable.CommentViewTheme_sendViewBackground,
            ContextCompat.getColor(context, R.color.liveLike_send_view_background_text_color)
        )
        binding.sendCommentView.sendViewBackground.setBackgroundColor(sendViewBackground)

        val editTextCommentDrawable = themeTypedArray.getDrawable(
            R.styleable.CommentViewTheme_editTextCommentDrawable
        )

        if (editTextCommentDrawable != null) {
            binding.sendCommentView.editTextComment.background = editTextCommentDrawable

        } else {
            binding.sendCommentView.editTextComment.setBackgroundResource(R.drawable.comment_edittext_rounded_corner)
        }

        val editTextCommentTextColor = themeTypedArray.getColor(
            R.styleable.CommentViewTheme_editTextCommentTextColor,
            ContextCompat.getColor(context, R.color.liveLike_send_view_text_color)
        )
        binding.sendCommentView.editTextComment.setTextColor(editTextCommentTextColor)

        val sendCommentDrawable = themeTypedArray.getDrawable(
            R.styleable.CommentViewTheme_sendCommentDrawable
        )

        if (sendCommentDrawable != null) {
            binding.sendCommentView.btnSendComment.background = sendCommentDrawable

        } else {
            binding.sendCommentView.btnSendComment.setBackgroundResource(R.drawable.comment_send_rounded_corner)
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        session.close()
        themeTypedArray?.recycle()
    }


    private fun getCustomTheme(attrs: AttributeSet?, defStyle: Int): TypedArray? {
        return context.obtainStyledAttributes(attrs, R.styleable.CommentView, defStyle, 0)
            .let { typedArray ->
                val theme = typedArray.getResourceId(R.styleable.CommentView_commentViewTheme, 0)
                typedArray.recycle()
                if (theme != 0) {
                    context.obtainStyledAttributes(theme, R.styleable.CommentViewTheme)
                } else {
                    null
                }
            }
    }

    //TODO: toast custom view is depreacted in android R , make sure to remove before that
    private fun showMessage(message: String) {
        val binding = CommentToastsBinding.inflate(LayoutInflater.from(context), null, false)
        binding.message = message
        val toast = Toast(context)
        toast.view = binding.root
        toast.duration = Toast.LENGTH_SHORT
        toast.setGravity(Gravity.START or Gravity.TOP, 2, 180)
        toast.show()
    }

    private fun loadSortingFilter(customTheme: TypedArray?) {
        val items = arrayOf(
            context.getString(R.string.livelike_newest),
            context.getString(R.string.livelike_oldest)
        )
        val spinnerAdapter = object : ArrayAdapter<String>(context, 0, items) {
            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val binding = convertView?.tag as? CustomSpinnerHeaderBinding
                    ?: CustomSpinnerHeaderBinding.inflate(
                        LayoutInflater.from(context), parent, false
                    )

                val filterHeaderTextColor = customTheme?.getColor(
                    R.styleable.CommentViewTheme_filterHeaderTextColor,
                    ContextCompat.getColor(context, R.color.liveLike_filter_tv_color)
                )

                if (filterHeaderTextColor != null) {
                    binding.tvCommentFilter.setTextColor(filterHeaderTextColor)
                } else {
                    binding.tvCommentFilter.setTextColor(
                        ContextCompat.getColor(
                            context, R.color.liveLike_filter_tv_color
                        )
                    )
                }

                val filterHeaderImage = customTheme?.getDrawable(
                    R.styleable.CommentViewTheme_filterHeaderImage
                )

                if (filterHeaderImage != null) {
                    binding.btnDropdown.setImageDrawable(
                        filterHeaderImage
                    )
                } else {
                    binding.btnDropdown.setImageResource(R.drawable.comment_filter)
                }

                binding.filter = this.getItem(position)
                binding.root.tag = binding
                return binding.root
            }

            override fun getDropDownView(
                position: Int, convertView: View?, parent: ViewGroup
            ): View {
                val binding = convertView?.tag as? CustomSpinnerItemBinding
                    ?: CustomSpinnerItemBinding.inflate(
                        LayoutInflater.from(context), parent, false
                    )
                binding.text = this.getItem(position)
                binding.root.tag = binding
                return binding.root
            }
        }
        binding.commentCountContainer.spinnerSort.adapter = spinnerAdapter
    }

    fun setSession(commentSession: LiveLikeCommentSession, reactionSession: LiveLikeReactionSession? = null) {
        session = commentSession
        adapter = CommentAdapter(
            customTheme,
            session::isReplyAllowed,
            session::openCommentReplies,
            session.profaneComment,
            session.topCommentFlow,
            this::getCommentOptionAdapter,
            uiScope
        )
        binding.commentDisplay.adapter = adapter
        adapter.setReactionSession(reactionSession)
        this@CommentView.uiScope.launch {
            launch {
                commentSession.commentListFlow.collect { comments ->
                    binding.loadingSpinner.visibility = View.VISIBLE
                    isLoading = true
                    adapter.submitList(comments)
                    binding.commentDisplay.layoutManager?.scrollToPosition(0)
                }
            }
            launch {
                session.commentsLoadedFlow.collect {
                    binding.commentDisplay.let { rv ->
                        if (it && commentSession.commentListFlow.value.isEmpty()) {
                            binding.loadingSpinner.visibility = View.GONE
                            rv.visibility = View.GONE
                            binding.emptyScreenView.root.visibility = View.VISIBLE
                        } else if (it && commentSession.commentListFlow.value.isNotEmpty()) {
                            binding.loadingSpinner.visibility = View.GONE
                            rv.visibility = View.VISIBLE
                            binding.emptyScreenView.root.visibility = View.GONE
                        }
                    }
                }
            }
            launch {
                commentSession.commentCountFlow.collect {
                    binding.commentCount = it
                }
            }
            launch {
                commentSession.topCommentFlow.collect { topComment ->
                    binding.parentComment = topComment
                    binding.profaneComment = commentSession.profaneComment
                    binding.parentTime = topComment?.getTimeForComment()
                    //To scroll to top everytime top comment changes. In future can be used to scroll to previous screen-specific comment
                    binding.commentDisplay.layoutManager?.scrollToPosition(0)
                    binding.nestedScrollView.run {
                        postDelayed({ scrollTo(0, 0) }, 200)
                    }

                    if (topComment != null) {
                        binding.isFromMe = topComment.isFromMe
                        topComment.isFromMe.let { isAuthor ->
                            binding.isFromMe = isAuthor
                            val spinnerAdapter = getCommentOptionAdapter(
                                topComment,
                                binding.commentParentContainer.includedCommentProfileHeader.btnCommentModeration
                            )
                            binding.commentParentContainer.includedCommentProfileHeader.btnCommentModeration.adapter =
                                spinnerAdapter
                            binding.commentParentContainer.includedCommentProfileHeader.btnCommentModeration.visibility =
                                View.VISIBLE
                        }


                        if (reactionSession != null) {
                            binding.commentParentContainer.reactionView.visibility = View.VISIBLE
                            val parentCommentReactionImageDrawable = customTheme?.getDrawable(
                                R.styleable.CommentViewTheme_parentCommentReactionButtonColor
                            )
                            binding.commentParentContainer.reactionView.setSession(
                                reactionSession, parentCommentReactionImageDrawable
                            )
                            binding.commentParentContainer.reactionView.setTargetId(topComment.id)
                        } else {
                            binding.commentParentContainer.reactionView.visibility = View.GONE
                        }

                    }

                    binding.parentTime = topComment?.getTimeForComment()
                    if (topComment != null && topComment.repliesCount > 0) {
                        binding.commentDisplay.addItemDecoration(itemDecoration)
                    } else {
                        while (binding.commentDisplay.itemDecorationCount > 0) {
                            binding.commentDisplay.removeItemDecorationAt(0)
                        }
                    }
                }
            }

            binding.replyTab.backArrowButton.setOnClickListener {
                commentSession.closeLastCommentReplies()
            }

            binding.replyTab.crossButton.setOnClickListener {
                commentSession.closeAllCommentReplies()
            }

            binding.sendCommentView.btnSendComment.setOnClickListener {
                val commentText = binding.sendCommentView.editTextComment.text.toString().trim()
                binding.sendCommentView.editTextComment.text.clear()
                if (commentText.isNotBlank()) {
                    val imm =
                        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(
                        binding.sendCommentView.editTextComment.windowToken, 0
                    )
                    commentSession.sendComment(commentText, profileImageUrl) { result, error ->
                        result?.let {
                            if (commentSession.topCommentFlow.value != null) {
                                binding.commentParentContainer.topParentLine.visibility =
                                    View.VISIBLE
                                binding.commentDisplay.addItemDecoration(itemDecoration)
                            }
                            binding.commentDisplay.layoutManager?.scrollToPosition(0)
                            binding.nestedScrollView.post {
                                binding.nestedScrollView.scrollTo(
                                    0, 0
                                )
                            }
                        }
                        error?.let {
                            showMessage(it)
                        }
                    }
                }
            }

            binding.commentDisplay.layoutManager = LinearLayoutManager(context)

            binding.nestedScrollView.run {
                setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { _, _, scrollY, _, oldScrollY ->
                    val directChild = getChildAt(0)
                    val contentHeight = directChild.measuredHeight
                    val scrollViewHeight = measuredHeight

                    if (scrollY == 0) {
                        // Reached the top of the scroll view
                    }

                    if (scrollY > oldScrollY) {
                        // Scrolling down
                    } else {
                        // Scrolling up
                    }

                    if ((scrollY >= contentHeight - scrollViewHeight) && !isLoading) {
                        // Reached the bottom of the scroll view
                        commentSession.loadNextHistory()
                    }
                    isLoading = false
                })
            }

            binding.commentCountContainer.spinnerSort.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>, view: View?, position: Int, id: Long
                    ) {
                        val sortingOption = when (parent.getItemAtPosition(position) as String) {
                            context.getString(R.string.livelike_newest) -> CommentSortingOptions.NEWEST
                            context.getString(R.string.livelike_oldest) -> CommentSortingOptions.OLDEST
                            else -> CommentSortingOptions.NEWEST
                        }
                        commentSession.setCommentSortingOption(sortingOption)
                        commentSession.reloadComments()
                    }

                    override fun onNothingSelected(p0: AdapterView<*>?) {
                        logDebug { "Nothing Selected" }
                    }
                }
        }
    }


    private fun getCommentOptionAdapter(comment: Comment, spinner: Spinner): CustomSpinnerAdapter {
        val items: Array<String> = if (comment.isFromMe) {
            arrayOf(deleteComment)
        } else {
            val matchingReport =
                session.commentsReportsListFlow.value.find { report -> report.comment?.id == comment.id }
            val matchingBlock =
                session.commentsBlockedListFlow.value.find { block -> block.blockedProfileID == comment.author.id }

            if (matchingReport != null && matchingBlock != null) {
                arrayOf(unReportComment, unBlockUser)
            } else if (matchingReport != null) {
                arrayOf(unReportComment, blockUser)
            } else if (matchingBlock != null) {
                arrayOf(reportComment, unBlockUser)
            } else {
                arrayOf(reportComment, blockUser)
            }
        }
        return CustomSpinnerAdapter(
            binding.root.context, items, spinner
        ) { _, selectedItem ->
            when (selectedItem) {
                deleteComment -> {
                    session.deleteComment(comment.id) { result, error ->
                        result?.let {
                            logDebug { "${comment.text}  deleted successfully" }
                        }
                        error?.let {
                            logDebug { "${comment.text}  not deleted" }
                        }
                    }
                }

                reportComment -> {
                    session.reportComment(comment.id) { result, error ->
                        result?.let {
                            showMessage(context.getString(R.string.liveLike_reported_message))
                        }
                        error?.let {
                            showMessage(it)
                        }
                    }
                }

                blockUser -> {
                    session.blockUser(comment.author.id) { result, error ->
                        result?.let {
                            session.getBlockedProfileList()
                            showMessage(
                                context.getString(
                                    R.string.livelike_was_blocked, comment.author.nickname
                                )
                            )
                        }

                        error?.let {
                            showMessage(it)
                        }
                    }
                }

                unReportComment -> {
                    val reportId =
                        session.commentsReportsListFlow.value.find { it.comment?.id == comment.id }?.id
                    if (reportId != null) {
                        session.unReportComment(reportId) { result, error ->
                            result?.let {
                                showMessage(context.getString(R.string.liveLike_unReported_message))
                            }
                            error?.let {
                                showMessage(it)
                            }
                        }
                    } else {
                        logError { "Report Id not found" }
                    }
                }

                unBlockUser -> {
                    session.unBlockUser(comment.author.id) { result, error ->
                        result?.let {
                            session.getBlockedProfileList()
                            showMessage(
                                context.getString(
                                    R.string.livelike_was_unblocked, comment.author.nickname
                                )
                            )
                        }

                        error?.let {
                            showMessage(it)
                        }
                    }
                }
            }
        }
    }
}

