package com.livelike.ui.comments

import android.content.Context
import android.graphics.Typeface
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Spinner
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.livelike.comment.models.Comment
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.TimeZone

internal fun Comment.getTimeForComment(): String? {
    val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    format.timeZone = TimeZone.getTimeZone("UTC")
    try {
        val date = format.parse(createdAt)
        return date?.let { DateUtils.getRelativeTimeSpanString(date.time).toString() }
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return null
}

@BindingAdapter("android:paddingHorizontal")
fun RecyclerView.bindPaddingHorizontal(marginHorizontal: Float) {
    this.updatePadding(left = marginHorizontal.toInt(), right = marginHorizontal.toInt())
}

@BindingAdapter("layoutMarginTop")
fun setLayoutMarginTop(view: View, dimen: Float) {
    val layoutParams = view.layoutParams as ViewGroup.MarginLayoutParams
    layoutParams.topMargin = dimen.toInt()
    view.layoutParams = layoutParams
}

@BindingAdapter("layoutMarginBottom")
fun setLayoutMarginBottom(view: View, dimen: Float) {
    val layoutParams = view.layoutParams as ViewGroup.MarginLayoutParams
    layoutParams.bottomMargin = dimen.toInt()
    view.layoutParams = layoutParams
}

@BindingAdapter("app:italicIfDeleted")
fun setItalicIfDeleted(textView: TextView, isDeleted: Boolean) {
    if (isDeleted) {
        textView.setTypeface(null, Typeface.ITALIC)
    } else {
        textView.setTypeface(null, Typeface.NORMAL)
    }
}

@BindingAdapter("app:textColorIfDeleted", "app:commentTextColor")
fun setTextColorIfDeleted(textView: TextView, isDeleted: Boolean, commentTextColor: Int?) {
    val textColor = if (isDeleted) {
        ContextCompat.getColor(textView.context, R.color.liveLike_delete_color)
    } else {
        commentTextColor ?: ContextCompat.getColor(
            textView.context,
            R.color.liveLike_comment_text_color
        )
    }
    textView.setTextColor(textColor)
}

@BindingAdapter("app:commentCountBackgroundColor")
fun setCommentCountBackgroundColor(layout: ConstraintLayout, colorResId: Int) {
    layout.setBackgroundColor(colorResId)
}

@BindingAdapter("app:commentReplyBackgroundColor")
fun setCommentReplyBackgroundColor(layout: LinearLayout, colorResId: Int) {
    layout.setBackgroundColor(colorResId)
}

@BindingAdapter("app:commentCountTextColor")
fun setCommentCountTextColor(textView: TextView, colorResId: Int?) {
    if (colorResId != null) {
        textView.setTextColor(colorResId)
    } else {
        textView.setTextColor(
            ContextCompat.getColor(
                textView.context,
                R.color.liveLike_comment_count_text_color
            )
        )
    }
}

@BindingAdapter("profileImageUrl")
fun loadProfileImage(imageView: ImageView, imageUrl: String?) {
    if (!imageUrl.isNullOrEmpty()) {
        Glide.with(imageView.context)
            .load(imageUrl)
            .centerCrop()
            .placeholder(R.drawable.livelike_profile_image)
            .error(R.drawable.livelike_profile_image)
            .into(imageView)
    } else {
        imageView.setImageResource(R.drawable.livelike_profile_image)
    }
}

@BindingAdapter("repliesCount")
fun setRepliesCount(textView: TextView, count: Int) {
    val context = textView.context
    val repliesText = if (count == 1) {
        "$count reply"
    } else {
        "$count replies"
    }
    textView.text = repliesText
}

@BindingAdapter("visibleIf")
fun setVisibleIf(view: View, condition: Boolean) {
    view.visibility = if (condition) View.VISIBLE else View.GONE
}

@BindingAdapter("app:dynamicRightMargin")
fun setDynamicRightMargin(view: View, isDeleted: Boolean) {
    val layoutParams = view.layoutParams as ConstraintLayout.LayoutParams
    val margin = if (isDeleted) 15 else 55
    layoutParams.rightMargin = dpToPx(view.context, margin)
    view.layoutParams = layoutParams
}

private fun dpToPx(context: Context, dp: Int): Int {
    val density = context.resources.displayMetrics.density
    return (dp * density).toInt()
}


class CustomSpinnerAdapter(
    context: Context,
    private val items: Array<String>,
    private val spinner: Spinner,
    private val onItemSelected: (position: Int, selectedItem: String) -> Unit
) : ArrayAdapter<String>(context, 0, items) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: LayoutInflater.from(context)
            .inflate(R.layout.custom_spinner_moderation, parent, false)
        view.visibility = View.GONE
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater = LayoutInflater.from(context)
        val view = layoutInflater.inflate(R.layout.custom_item_spinner_moderation, parent, false)

        val itemImage = view.findViewById<ImageView>(R.id.iBModeration)
        val itemText = view.findViewById<TextView>(R.id.tvModeration)

        when (position) {
            0 -> {
                if (items.size == 1) {
                    itemImage.setImageResource(R.drawable.livelike_trash)
                    itemText.text = "Delete Comment"
                } else {
                    itemImage.setImageResource(R.drawable.livelike_flag)
                    itemText.text = items[0]
                }
            }

            1 -> {
                itemImage.setImageResource(R.drawable.livelike_block)
                itemText.text = items[1]
            }
        }

        view.setOnClickListener {
            val selectedItem = getItem(position)
            onItemSelected(position, selectedItem!!)
            when (selectedItem) {
                "Report Comment" -> {
                    itemText.text = "Unreport Comment"
                    items[position] = "Unreport Comment"
                }

                "Block User" -> {
                    itemText.text = "Unblock User"
                    items[position] = "Unblock User"
                }

                "Unblock User" -> {
                    itemText.text = "Block User"
                    items[position] = "Block User"
                }

                "Unreport Comment" -> {
                    itemText.text = "Report Comment"
                    items[position] = "Report Comment"
                }
                // ... handle other cases ...
            }
            closeDropdown()
        }


        return view
    }

    private fun closeDropdown() {
        try {
            val method = Spinner::class.java.getDeclaredMethod("onDetachedFromWindow")
            method.isAccessible = true
            method.invoke(spinner)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
