package com.livelike.ui.comments

import android.app.Activity
import android.content.res.TypedArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.LinearLayout.HORIZONTAL
import android.widget.LinearLayout.VERTICAL
import android.widget.RelativeLayout
import android.widget.Spinner
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.livelike.comment.ProfaneComment
import com.livelike.comment.models.Comment
import com.livelike.engagementsdk.reaction.LiveLikeReactionSession
import com.livelike.ui.comments.databinding.DefaultCommentCellBinding
import com.livelike.utils.logDebug
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

internal class CommentAdapter(
    private val themeTypedArray: TypedArray?,
    private val isReplyAllowed: () -> Boolean,
    private val openCommentReplies: (Comment?) -> Unit,
    private val profaneComment: ProfaneComment,
    private var topComment: StateFlow<Comment?>,
    private val getCommentOptionAdapter: (Comment, Spinner) -> CustomSpinnerAdapter,
    private var uiScope: CoroutineScope
) :
    ListAdapter<Comment, CommentAdapter.CommentViewHolder>(CommentDiffCallback()) {
    private var reactionSession: LiveLikeReactionSession? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DefaultCommentCellBinding.inflate(inflater, parent, false)
        return CommentViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        val comment = getItem(position)
        holder.bind(comment)
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).id.hashCode().toLong()
    }


    fun setReactionSession(reactionSession: LiveLikeReactionSession?) {
        this.reactionSession = reactionSession
    }

    inner class CommentViewHolder(private val binding: DefaultCommentCellBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            (binding.root.context as Activity).window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        }

        fun bind(comment: Comment) {
            binding.comment = comment
            binding.profaneComment = profaneComment
            binding.time = comment.getTimeForComment()
            binding.isReplyAllowed = isReplyAllowed()
            uiScope.launch {
                topComment.collect {
                    binding.isParent = it != null
                }
            }

            comment.isFromMe.let { isAuthor ->
                binding.isFromMe = isAuthor
                val spinnerAdapter = getCommentOptionAdapter(
                    comment,
                    binding.includedCommentProfileHeader.btnCommentModeration
                )
                binding.includedCommentProfileHeader.btnCommentModeration.adapter = spinnerAdapter
            }

            binding.repliesCount.setOnClickListener {
                openCommentReplies(comment)
            }
            binding.includedCommentProfileHeader.btnReplyAction.setOnClickListener {
                openCommentReplies(comment)
            }
            val drawableCommentCell =
                ContextCompat.getDrawable(binding.root.context, R.drawable.comment_rounded_corner)
            val commentBubbleBackgroundDrawable = themeTypedArray?.getDrawable(
                R.styleable.CommentViewTheme_commentBubbleBackgroundDrawable
            )
            if (commentBubbleBackgroundDrawable != null) {
                binding.drawableVariable = commentBubbleBackgroundDrawable
            } else {
                binding.drawableVariable = drawableCommentCell
            }
            val drawableReplyBox = ContextCompat.getDrawable(
                binding.root.context,
                R.drawable.livelike_reply_round_background
            )
            val replyBoxDrawable = themeTypedArray?.getDrawable(
                R.styleable.CommentViewTheme_replyBoxDrawable
            )
            if (replyBoxDrawable != null) {
                binding.replyBoxDrawableVariable = replyBoxDrawable
            } else {
                binding.replyBoxDrawableVariable = drawableReplyBox
            }

            val profileNameTextColor = themeTypedArray?.getColor(
                R.styleable.CommentViewTheme_profileNameTextColor,
                ContextCompat.getColor(
                    binding.root.context,
                    R.color.liveLike_comment_profile_text_color
                )
            ) ?: ContextCompat.getColor(
                binding.root.context,
                R.color.liveLike_comment_profile_text_color
            )

            val profileTextViewTimeColor = themeTypedArray?.getColor(
                R.styleable.CommentViewTheme_profileTvTimeTextColor,
                ContextCompat.getColor(
                    binding.root.context,
                    R.color.liveLike_comment_time_text_color
                )
            ) ?: ContextCompat.getColor(
                binding.root.context,
                R.color.liveLike_comment_time_text_color
            )

            val commentTextColor = themeTypedArray?.getColor(
                R.styleable.CommentViewTheme_commentTextColor,
                ContextCompat.getColor(binding.root.context, R.color.liveLike_comment_text_color)
            ) ?: ContextCompat.getColor(binding.root.context, R.color.liveLike_comment_text_color)

            val commentRepliesButtonColor = themeTypedArray?.getColor(
                R.styleable.CommentViewTheme_commentRepliesButtonColor,
                ContextCompat.getColor(
                    binding.root.context,
                    R.color.liveLike_replies_button_text_color
                )
            ) ?: ContextCompat.getColor(
                binding.root.context,
                R.color.liveLike_replies_button_text_color
            )

            val viewOrder = themeTypedArray?.getInt(R.styleable.CommentViewTheme_viewOrder, 1) ?: 0
            val reactionButton = binding.reactionView
            val repliesCountContainer = binding.repliesCountContainer

            val reactionReplyButtonOrientation = themeTypedArray?.getInt(
                R.styleable.CommentViewTheme_reactionReplyButtonOrientation,
                VERTICAL
            ) ?: VERTICAL

            val reactionButtonParams = reactionButton.layoutParams as RelativeLayout.LayoutParams
            val repliesCountParams =
                repliesCountContainer.layoutParams as RelativeLayout.LayoutParams

            if (reactionSession != null) {
                binding.reactionView.visibility = View.VISIBLE
                logDebug { "reaction123 id:${comment.id} > ${comment.text}" }
                val commentReactionImageDrawable = themeTypedArray?.getDrawable(
                    R.styleable.CommentViewTheme_commentReactionButtonColor
                )
                binding.reactionView.setSession(reactionSession!!, commentReactionImageDrawable)
                binding.reactionView.setTargetId(comment.id)
            } else {
                binding.reactionView.visibility = View.GONE
            }

            if (reactionReplyButtonOrientation == HORIZONTAL) {

                if (viewOrder != 0) { // Reaction first and Horizontal
                    reactionButtonParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT)
                    repliesCountParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
                } else { // Replies first and Horizontal
                    reactionButtonParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
                    repliesCountParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT)
                }
                reactionButton.layoutParams = reactionButtonParams
                repliesCountContainer.layoutParams = repliesCountParams

            } else {
                if (viewOrder != 0) {
                    reactionButtonParams.addRule(RelativeLayout.ALIGN_PARENT_TOP)
                    repliesCountParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
                } else {
                    reactionButtonParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)

                    val layoutParams =
                        repliesCountContainer.layoutParams as ViewGroup.MarginLayoutParams
                    layoutParams.bottomMargin =
                        binding.root.context.resources.getDimensionPixelSize(R.dimen.comment_reply_bottom_margin)
                    repliesCountContainer.layoutParams = layoutParams
                    repliesCountParams.addRule(RelativeLayout.ALIGN_PARENT_TOP)
                }
                reactionButton.layoutParams = reactionButtonParams
                repliesCountContainer.layoutParams = repliesCountParams
            }

            binding.includedCommentProfileHeader.tvProfileName.setTextColor(profileNameTextColor)
            binding.includedCommentProfileHeader.tvTime.setTextColor(profileTextViewTimeColor)
            binding.commentTextColor = commentTextColor

            binding.repliesCount.setTextColor(commentRepliesButtonColor)
        }

    }

    private class CommentDiffCallback : DiffUtil.ItemCallback<Comment>() {
        override fun areItemsTheSame(oldItem: Comment, newItem: Comment): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Comment, newItem: Comment): Boolean {
            return oldItem == newItem
        }
    }
}
