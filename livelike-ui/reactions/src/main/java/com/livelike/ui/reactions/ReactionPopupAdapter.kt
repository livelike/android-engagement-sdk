package com.livelike.ui.reactions

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.livelike.engagementsdk.chat.chatreaction.Reaction
import com.livelike.ui.reactions.databinding.ReactionPopupItemBinding

internal class ReactionPopupAdapter(private val onReactionItemClick: (Reaction) -> Unit) :
    ListAdapter<Reaction, ReactionPopupAdapter.ReactionPopupViewHolder>(ItemDiffCallback()) {

    inner class ReactionPopupViewHolder(val binding: ReactionPopupItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    private class ItemDiffCallback : DiffUtil.ItemCallback<Reaction>() {
        override fun areItemsTheSame(oldItem: Reaction, newItem: Reaction): Boolean {
            // Implement this method to check if two items represent the same object
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Reaction, newItem: Reaction): Boolean {
            // Implement this method to check if two items have the same content
            return oldItem == newItem
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReactionPopupViewHolder {
        return ReactionPopupViewHolder(
            ReactionPopupItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ReactionPopupViewHolder, position: Int) {
        val reaction = getItem(position)

        Glide.with(holder.itemView.context.applicationContext)
            .load(reaction?.file)
            .into( holder.binding.reactionImageView)


        holder.binding.root.setOnClickListener { onReactionItemClick.invoke(reaction) }
    }
}