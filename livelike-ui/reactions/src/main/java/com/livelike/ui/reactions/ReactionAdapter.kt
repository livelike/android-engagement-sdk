package com.livelike.ui.reactions

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.livelike.engagementsdk.chat.chatreaction.Reaction
import com.livelike.engagementsdk.reaction.models.UserReactionCount
import com.livelike.ui.reactions.databinding.ReactionItemBinding
import com.livelike.ui.reactions.databinding.ReactionPlaceholderBinding
import kotlin.reflect.KFunction1

internal class ReactionAdapter(
    private val reactionPlaceHolderClickListener: KFunction1<View, Unit>,
    private val getReactionById: (String) -> Reaction?,
    private val onReactionClick: (Reaction) -> Unit,
    private val customTheme: Drawable?
) :
    ListAdapter<UserReactionCount, RecyclerView.ViewHolder>(ItemDiffCallback()) {

    inner class ReactionPlaceHolder(val binding: ReactionPlaceholderBinding) :
        RecyclerView.ViewHolder(binding.root)

    inner class ReactionViewHolder(val binding: ReactionItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_PLACEHOLDER -> {
                ReactionPlaceHolder(
                    ReactionPlaceholderBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }

            VIEW_TYPE_REACTION -> {
                ReactionViewHolder(
                    ReactionItemBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }

            else -> throw IllegalArgumentException("Invalid view type: $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ReactionPlaceHolder -> {
                if (customTheme != null) {
                    holder.binding.placeholderImage.setImageDrawable(customTheme)
                } else {
                    holder.binding.placeholderImage.setImageDrawable(ContextCompat.getDrawable(
                        holder.binding.root.context,
                        R.drawable.add_reaction_image
                    ))
                }
                holder.itemView.setOnClickListener(reactionPlaceHolderClickListener)
            }

            is ReactionViewHolder -> {
                val userReactionCount = getItem(position)
                val reaction = getReactionById(userReactionCount.reactionId)

                //reaction count without databinding
                if (userReactionCount.reactionId.isEmpty()) {
                    holder.binding.layoutReactionCount.background = null
                    holder.binding.imgReaction.setImageResource(R.drawable.add_reaction)
                    holder.binding.txtReactionCount.visibility =View.GONE

                } else {
                    holder.binding.layoutReactionCount.background =
                        ContextCompat.getDrawable(holder.itemView.context, R.drawable.reaction_item_background)

                    Glide.with(holder.itemView.context.applicationContext)
                        .load(reaction?.file)
                        .into( holder.binding.imgReaction)

                    holder.binding.txtReactionCount.visibility =View.VISIBLE
                    holder.binding.txtReactionCount.text = userReactionCount.count.toString()
                }

                // Handle self-reacted case
              if (userReactionCount.selfReactedUserReactionId != null) {
                    holder.binding.layoutReactionCount.background =
                        ContextCompat.getDrawable(holder.itemView.context, R.drawable.reaction_item_selected_background)
                }

                //click listener
                holder.binding.root.setOnClickListener {
                    reaction?.let {onReactionClick.invoke(it) }
                }
            }

            else -> {
                throw IllegalArgumentException("Invalid view holder")
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> VIEW_TYPE_PLACEHOLDER
            else -> VIEW_TYPE_REACTION
        }
    }

    private class ItemDiffCallback : DiffUtil.ItemCallback<UserReactionCount>() {
        override fun areItemsTheSame(
            oldItem: UserReactionCount,
            newItem: UserReactionCount
        ): Boolean {
            // Implement this method to check if two items represent the same object
            return oldItem.reactionId == newItem.reactionId
        }

        override fun areContentsTheSame(
            oldItem: UserReactionCount,
            newItem: UserReactionCount
        ): Boolean {
            // Implement this method to check if two items have the same content
            return oldItem == newItem
        }
    }

    companion object {
        private const val VIEW_TYPE_PLACEHOLDER = 1
        private const val VIEW_TYPE_REACTION = 2
    }
}