package com.livelike.ui.reactions

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.livelike.engagementsdk.chat.chatreaction.Reaction
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.reaction.LiveLikeReactionSession
import com.livelike.engagementsdk.reaction.models.UserReactionCount
import com.livelike.ui.reactions.databinding.ReactionPopupViewBinding
import com.livelike.ui.reactions.databinding.ReactionViewBinding
import com.livelike.utils.logDebug
import com.livelike.utils.logError
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


class ReactionView : ConstraintLayout {

    private lateinit var reactionViewBinding: ReactionViewBinding
    private lateinit var reactionPopupWindow: PopupWindow
    private lateinit var reactionPopupAdapter: ReactionPopupAdapter
    private var reactionAdapter: ReactionAdapter? = null
    private val placeHolderUserReaction = UserReactionCount("", 0)
    private val scope = CoroutineScope(SupervisorJob() + Dispatchers.Main.immediate)
    private val _targetIdFlow = MutableStateFlow<String?>(null)
    private val _sessionFlow = MutableStateFlow<LiveLikeReactionSession?>(null)
    private val _userReactionCountListFlow = MutableStateFlow<List<UserReactionCount>>(emptyList())
    private val _reactionsFlow = MutableStateFlow<List<Reaction>>(emptyList())
    private val _reactionPackAndUserReactionCountPair =
        _targetIdFlow.combine(_sessionFlow) { targetId, session ->
            logDebug { "reaction123 transform:$targetId > $session" }
            return@combine if (session != null && targetId != null) {
                val reactionPack = suspendCoroutine { cont ->
                    session.getReactionPacks { result, error ->
                        result?.let { reactionPacks ->
                            cont.resume(reactionPacks.map { it.emojis }.flatten())
                        }
                        error?.let {
                            cont.resumeWithException(Exception(it))
                        }
                    }
                }
                val reactionCountList = suspendCoroutine { cont ->
                    session.getUserReactionsCount(
                        listOf(targetId), LiveLikePagination.FIRST
                    ) { result, error ->
                        result?.let { targetUserReactionCounts ->
                            cont.resume(
                                targetUserReactionCounts.find { it.targetId == targetId }?.reactions
                                    ?: emptyList()
                            )
                        }
                        error?.let {
                            cont.resumeWithException(Exception(it))
                        }
                    }
                }
                reactionPack to reactionCountList
            } else null
        }


    constructor(context: Context) : super(context) {
        setupView(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setupView(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context, attrs, defStyle
    ) {
        setupView(attrs, defStyle)
    }

    private fun setupView(attrs: AttributeSet?, defStyle: Int) {
        reactionViewBinding = ReactionViewBinding.inflate(LayoutInflater.from(context), this, true)

        val layoutManager = FlexboxLayoutManager(context)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.FLEX_START // Adjust as needed
        reactionViewBinding.rcylReactions.layoutManager = layoutManager

        val spacing = resources.getDimensionPixelSize(R.dimen.reaction_item_spacing)
        reactionViewBinding.rcylReactions.addItemDecoration(ReactionItemDecoration(spacing))
        reactionViewBinding.rcylReactions.addItemDecoration(RowSpacingItemDecoration(spacing))


        val reactionPopupViewBinding =
            ReactionPopupViewBinding.inflate(LayoutInflater.from(context))
        reactionPopupWindow = PopupWindow(
            reactionPopupViewBinding.root,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            true
        ).apply {
            isOutsideTouchable = true
        }
        reactionPopupViewBinding.rcylReactionsPopup.layoutManager =
            LinearLayoutManager(context, RecyclerView.HORIZONTAL, false /*calculateSpanCount(70)*/)
        reactionPopupAdapter = ReactionPopupAdapter(::onReactionClick)
        reactionPopupViewBinding.rcylReactionsPopup.adapter = reactionPopupAdapter

        scope.launch {
            launch {
                _reactionPackAndUserReactionCountPair.collect {
                    if (it != null) {
                        logDebug { "reaction123 view: ${_targetIdFlow.value}" }
                        _reactionsFlow.value = it.first
                        _userReactionCountListFlow.value = it.second
                    }
                }
            }
            launch {
                _reactionsFlow.collect {
//                    logDebug { "reaction123 popup adapter: ${reactionAdapter.itemCount} > ${it.size} > ${_targetIdFlow.value}" }
                    reactionPopupAdapter.submitList(it)
                }
            }
            launch {
                _userReactionCountListFlow.collect {
                    logDebug { "reaction123 adapter:${reactionAdapter?.itemCount} > ${it.size} > ${_targetIdFlow.value}" }
                    reactionAdapter?.submitList(listOf(placeHolderUserReaction) + it)
                }
            }
        }
//        reactionPopupAdapter.submitList(_reactionsFlow.value)
//        reactionAdapter.submitList(listOf(placeHolderUserReaction) + _userReactionCountListFlow.value)
    }

    private fun onReactionClick(reaction: Reaction) {
        val session = _sessionFlow.value
        val targetId = _targetIdFlow.value
        if (session != null && targetId != null) {
            reactionPopupWindow.dismiss()
            val userReactionCount =
                _userReactionCountListFlow.value.find { it.reactionId == reaction.id }
            if (userReactionCount != null) {
                if (userReactionCount.selfReactedUserReactionId != null) {
                    session.removeUserReaction(userReactionCount.selfReactedUserReactionId!!) { result, error ->
                        result?.let {
                            if (_userReactionCountListFlow.value.any { it.reactionId == reaction.id && it.count == 1 }) {
                                _userReactionCountListFlow.value =
                                    _userReactionCountListFlow.value.filter { it.reactionId != reaction.id }
                            } else {
                                _userReactionCountListFlow.value =
                                    _userReactionCountListFlow.value.map {
                                        if (it.reactionId == reaction.id) {
                                            it.copy(
                                                count = it.count - 1,
                                                selfReactedUserReactionId = null
                                            )
                                        } else {
                                            it
                                        }
                                    }
                            }
                        }
                        error?.let {
                            logError { it }
                        }
                    }
                } else {
                    addReaction(reaction)
                }
            } else {
                addReaction(reaction)
            }
        } else {
            logError { "Session or Target Id is null, session:$session, targetId:$targetId" }
        }
    }

    private fun addReaction(reaction: Reaction) {
        val session = _sessionFlow.value
        val targetId = _targetIdFlow.value
        if (session != null && targetId != null) {
            session.addUserReaction(targetId, reaction.id) { result, error ->
                error?.let { logError { it } }
                result?.let { userReaction ->
                    if (_userReactionCountListFlow.value.any { it.reactionId == userReaction.reactionId }) {
                        _userReactionCountListFlow.value = _userReactionCountListFlow.value.map {
                            if (it.reactionId == userReaction.reactionId) it.copy(
                                count = it.count + 1,
                                selfReactedUserReactionId = userReaction.id
                            )
                            else it
                        }
                    } else {
                        _userReactionCountListFlow.value =
                            _userReactionCountListFlow.value + listOf(
                                UserReactionCount(
                                    userReaction.reactionId,
                                    1,
                                    userReaction.id
                                )
                            )
                    }
                }
            }
        } else {
            logError { "Session or Target Id is null, session:$session, targetId:$targetId" }
        }
    }

    private fun getReactionById(reactionId: String): Reaction? {
        return _reactionsFlow.value.find { it.id == reactionId }
    }

    private fun calculateSpanCount(itemWidthDp: Int): Int {
        val displayMetrics = context.resources.displayMetrics
        val screenWidthDp = displayMetrics.widthPixels / displayMetrics.density
        return (screenWidthDp / itemWidthDp).toInt()
    }

    private fun openReactionPopup(view: View) {
        val location = IntArray(2)
        view.getLocationOnScreen(location)
        reactionPopupWindow.showAtLocation(
            view,
            Gravity.NO_GRAVITY,
            location[0],
            location[1] - dpToPx(55f, context)
        )
    }

    private fun dpToPx(dp: Float, context: Context): Int {
        val scale = context.resources.displayMetrics.density
        return (dp * scale + 0.5f).toInt()
    }

    fun setTargetId(targetId: String) {
        logDebug { "reaction123 setTarget: $targetId >> ${_targetIdFlow.value}" }
        if (targetId != _targetIdFlow.value) {
            _userReactionCountListFlow.value = emptyList()
            _targetIdFlow.value = targetId
        }
    }

    fun setSession(session: LiveLikeReactionSession, reactionButtonDrawable: Drawable?) {
        logDebug { "reaction123 setSession: ${_targetIdFlow.value}>${_sessionFlow.value}>>$session" }
        if (session != _sessionFlow.value) {
            _userReactionCountListFlow.value = emptyList()
            _sessionFlow.value = session
            reactionAdapter = ReactionAdapter(
                ::openReactionPopup,
                ::getReactionById,
                ::onReactionClick,
                reactionButtonDrawable
            )
            reactionViewBinding.rcylReactions.adapter = reactionAdapter
            reactionAdapter!!.submitList(listOf(placeHolderUserReaction))
        }
    }
}