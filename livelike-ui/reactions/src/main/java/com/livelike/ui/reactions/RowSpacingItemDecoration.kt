package com.livelike.ui.reactions

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager

internal class RowSpacingItemDecoration(private val spacing: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view)

        // Check if it's not the first item in a row
        if (position > 0 && isFirstItemInRow(position, parent)) {
            outRect.top = spacing
        }
    }

    private fun isFirstItemInRow(position: Int, parent: RecyclerView): Boolean {
        if (position <= 0) {
            return true // First item is always in a new row
        }

        val layoutManager = parent.layoutManager
        if (layoutManager is FlexboxLayoutManager) {
            val currentRowStart = layoutManager.findFirstVisibleItemPosition()
            val currentRowEnd = currentRowStart + layoutManager.flexDirection

            // Check if the position is in the current row
            return position < currentRowStart || position >= currentRowEnd
        }

        return false
    }


}
